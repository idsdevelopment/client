﻿#nullable enable

using System.Diagnostics;
using System.Windows.Controls;
using ViewModels.MainWindow;

namespace ViewModels.StartPage;

internal class StartPageModel : ViewModelBase
{
	[Setting]
	public string Account
	{
		get { return Get( () => Account, "" ); }
		set { Set( () => Account, value ); }
	}

	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}

	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}

	[Setting]
	public double NewsChannelWidth
	{
		get { return Get( () => NewsChannelWidth, 0 ); }
		set { Set( () => NewsChannelWidth, value ); }
	}

	public bool ButtonEnable
	{
		get { return Get( () => ButtonEnable, false ); }
		set { Set( () => ButtonEnable, value ); }
	}


	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638124039/How+to+Login" ); }
	}

	public MainWindowModel.OnLoginEvent? OnLogin;

	public Page? StartPage;

	[DependsUpon( nameof( Account ), Delay = 200 )]
	[DependsUpon( nameof( UserName ), Delay = 200 )]
	[DependsUpon( nameof( Password ), Delay = 200 )]
	public void WhenFieldChanges()
	{
		ButtonEnable = !string.IsNullOrEmpty( Account.Trim() ) && !string.IsNullOrEmpty( UserName ) && ( Password.Length > 0 );
	}


	public void Execute_Login()
	{
		ButtonEnable = false;

		Task.Run( async () =>
		          {
			          try
			          {
				          switch( ( await Azure.LogIn( Globals.CurrentVersion.APP_VERSION, Account, UserName, Password ) ).Status )
				          {
				          case Azure.AZURE_CONNECTION_STATUS.ERROR:
					          Globals.Dialogues.Error.Show( "InvalidLogin" );

					          break;

				          case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
					          Globals.Dialogues.Error.Show( "TimeError" );

					          break;

				          case Azure.AZURE_CONNECTION_STATUS.OK:
					          try
					          {
						          if( Azure.DebugServer )
						          {
							          Globals.Dictionary.SetColor( "IdsColour", Globals.Dictionary.AsColor( "TestIdsColour" ) );
							          Globals.Dictionary.SetColor( "DefaultTitleBarColour", Globals.Dictionary.AsColor( "TestDefaultTitleBarColour" ) );
							          Globals.Dictionary.SetColor( "DefaultTitleBarTextColour", Globals.Dictionary.AsColor( "TestDefaultTitleBarTextColour" ) );
							          Globals.Dictionary.SetColor( "IdsForegroundColour", Globals.Dictionary.AsColor( "TestIdsForegroundColour" ) );
							          Globals.Dictionary.SetColor( "SecondaryTitleBarColour", Globals.Dictionary.AsColor( "TestSecondaryTitleBarColour" ) );
							          Globals.Dictionary.SetColor( "SecondaryTitleBarTextColour", Globals.Dictionary.AsColor( "TestSecondaryTitleBarTextColour" ) );

							          Globals.Dictionary.SetSolidBrush( "IdsBackgroundBrush", Globals.Dictionary.AsSolidColorBrush( "TestIdsBackgroundBrush" ) );
							          Globals.Dictionary.SetSolidBrush( "TitleBarBrush", Globals.Dictionary.AsSolidColorBrush( "TestTitleBarBrush" ) );
							          Globals.Dictionary.SetSolidBrush( "DefaultTitleBarTextBrush", Globals.Dictionary.AsSolidColorBrush( "TestDefaultTitleBarTextBrush" ) );
							          Globals.Dictionary.SetSolidBrush( "IdsForegroundBrush", Globals.Dictionary.AsSolidColorBrush( "TestIdsForegroundBrush" ) );
							          Globals.Dictionary.SetSolidBrush( "SecondaryTitleBarBrush", Globals.Dictionary.AsSolidColorBrush( "TestSecondaryTitleBarBrush" ) );
							          Globals.Dictionary.SetSolidBrush( "SecondaryTitleBarTextBrush", Globals.Dictionary.AsSolidColorBrush( "TestSecondaryTitleBarTextBrush" ) );

							          Globals.Windows.MainWindowColourChange?.Invoke();
						          }

						          var IsIds = await Azure.Client.RequestIsIds();
						          Globals.Security.IsIds = IsIds;

						          Globals.Security.IsNewCarrier = await Azure.Client.RequestIsNewCarrier();
						          var Carrier = Account.Split( '\\', '/' );
						          Globals.Security.CarrierId = Carrier.Length == 2 ? Carrier[ 1 ] : Carrier[ 0 ];

						          Globals.CurrentUser.UserName = UserName;
						          Globals.Menu.IsVisible       = true;
						          OnLogin?.Invoke( Account, UserName );
						          StartPage?.ClosePage();
					          }
					          catch( Exception E )
					          {
						          Logging.WriteLogLine( "Exception thrown: " + E );
					          }

					          break;
				          }
			          }
			          finally
			          {
				          Dispatcher.Invoke( () =>
				                             {
					                             ButtonEnable = true;
				                             } );
			          }
		          } );
	}

	public void Execute_ShowHelp()
	{
		Logging.WriteLogLine( "Opening " + HelpUri );
		var Uri = new Uri( HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	public delegate void OnLoginEvent( string companyName, string userName );
}