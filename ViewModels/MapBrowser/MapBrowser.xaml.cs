﻿#nullable enable

using System.Diagnostics;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Microsoft.Web.WebView2.Core;
using ViewModels.Trips.Boards.Common;
using File = System.IO.File;

namespace ViewModels.MapBrowser;

/// <summary>
///     Interaction logic for MapBrowser.xaml
/// </summary>
public partial class MapBrowser : Page, IDisposable
{
	private MapBrowserModel? Model;

	protected override async void OnInitialized( EventArgs e )
	{
		base.OnInitialized( e );

		var M = Model = (MapBrowserModel)DataContext;


		//Logging.WriteLogLine( "DEBUG Getting map keys" );
		await M.GetBingMapApiKey();

		//Logging.WriteLogLine( "DEBUG Loading drivers" );
		await M.LoadDrivers();
		await M.GetServiceLevels();

		M.SetView( this );

		if( !M.IsUpdateLiveMapRunning())
			M.StartUpdateLiveMap();

		M.CurrentMapLabelView = FindStringResource( "DriversMapLabelCurrentMapViewLiveNoAccelerator" );

		await LoadMap();

		M.SwitchToLiveLines();

		dtpFromDateTime.ShowDropDownButton = true;

		// Run this when new icons are used, to get their base64 strings
		//LoadIcons();
		webView.Visibility = Visibility.Visible;
		webView.InvalidateVisual();
	}

	public void Dispose()
	{
        Mouse.OverrideCursor = null;
        try
        {
            if (Model != null)
            {
                Logging.WriteLogLine($"Disposing");
                Model.StopUpdateLiveMap();
            }
        }
        catch { }
    }

	public void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	public void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}

	/// <summary>
	///     Note - have to make it async so that required pieces get loaded first.
	/// </summary>
	public async Task LoadMap()
	{
		await webView.EnsureCoreWebView2Async();

		if( Model is {} M )
		{
			webView.Visibility = Visibility.Hidden;
			var Html = M.GetMapHtml();
			webView.NavigateToString("<html><head></head><body></body></html>");
			webView.NavigateToString( Html );
			if (webView.IsLoaded)
			{
				Logging.WriteLogLine("webView isn't finished loading - sleeping for 1 second");
				Thread.Sleep(1000);
			}
			webView.Visibility = Visibility.Visible;
			webView.InvalidateVisual();
		}

		//var Centre = Model.Latitude + "," + Model.Longitude;
		//Logging.WriteLogLine( "DEBUG centre: " + centre );

		//webView.ExecuteScriptAsync
		webView.CoreWebView2.Settings.IsWebMessageEnabled = true;
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	//
	// 

	/// <summary>
	///     Use this to get the base64 for icons on map.
	/// </summary>
	/// <returns></returns>
	public string LoadIcons()
	{
		//byte[] imageArray  = System.IO.File.ReadAllBytes( @"c:\tmp\truck-48.png" );
		var ImageArray  = File.ReadAllBytes( @"c:\tmp\trucknew1.png" );
		var Base64Truck = Convert.ToBase64String( ImageArray );
		Logging.WriteLogLine( "DEBUG base64truck: " + Base64Truck );
		//imageArray = System.IO.File.ReadAllBytes( @"c:\tmp\TripPinPU.png" );
		ImageArray = File.ReadAllBytes( @"c:\tmp\TripPinPUnew1.png" );
		var Base64Pu = Convert.ToBase64String( ImageArray );
		Logging.WriteLogLine( "DEBUG base64PU: " + Base64Pu );
		//imageArray = System.IO.File.ReadAllBytes( @"c:\tmp\TripPinDel.png" );
		ImageArray = File.ReadAllBytes( @"c:\tmp\TripPinDelnew1.png" );
		var Base64Del = Convert.ToBase64String( ImageArray );
		Logging.WriteLogLine( "DEBUG base64Del: " + Base64Del );

		return Base64Truck;
	}


	public async void CentreMapOnPoint( GpsPoint point )
	{
		var Script = "CentreMapOnPoint(" + point.Latitude + "," + point.Longitude + ");";
		//Logging.WriteLogLine( "DEBUG script: " + script );
		await webView.CoreWebView2.ExecuteScriptAsync( Script );
	}

	public MapBrowser()
	{
		InitializeComponent();
	}

	//private void BtnLoadCurrentLocation_Click(object sender, RoutedEventArgs e)
	//{
	//    Logging.WriteLogLine("DEBUG Loading Current Location");

	//    if (DataContext is MapBrowserModel Model)
	//    {
	//        //Model.GetHomeLocation(DriversMap);
	//    }
	//}

	private void BtnOpenHelp_Click( object sender, RoutedEventArgs e )
	{
		if( Model is { } M )
		{
			Logging.WriteLogLine( "Opening " + M.HelpUri );
			var Uri = new Uri( M.HelpUri );
			Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
		}
	}

	private void CheckBox_Checked( object sender, RoutedEventArgs e )
	{
		if( sender is CheckBox Cb && Model is { } M )
		{
			//Logging.WriteLogLine("DEBUG Driver checked: " + cb.Content);
			// M.AddSelectedDriver( (string)Cb.Content );
			if (Cb.Parent is StackPanel sp)
			{
				foreach (var child in sp.Children)
				{
					if (child is TextBlock tb)
					{
						M.AddSelectedDriver(tb.Text);
						break;
					}
				}

			}
		}
	}

	private void CheckBox_Unchecked( object sender, RoutedEventArgs e )
	{
		if( sender is CheckBox Cb && Model is { } M )
		{
			//Logging.WriteLogLine("DEBUG Driver unchecked: " + cb.Content);
			//M.RemoveSelectedDriver( (string)Cb.Content );
			if (Cb.Parent is StackPanel sp)
			{
				foreach (var child in sp.Children)
				{
					if (child is TextBlock tb)
					{
						// M.RemoveSelectedDriver(tb.Text);
						Logging.WriteLogLine("DEBUG Removing selected " + tb.Text);
						var Found = (from D in M.AllDrivers
									 where D.DisplayName == tb.Text
									 select D).FirstOrDefault();


						if (Found?.StaffId is { } Id)
						{
							var Driver = (from D in M.Drivers
										  where D.StaffId == Found.StaffId
										  select D).FirstOrDefault();

							if (Driver is not null)
							{
								Driver.HasTrips = false;
								M.SelectedDrivers.Remove(Driver);
							}
							
							Found.HasTrips = false;
							M.SelectedDrivers.Remove(Found);

							if (M.ActiveMapView == MapBrowserModel.MAP_VIEW_LIVE)
							{
								if (M.TripsByDriverLive[Found.StaffId] is { } Tbd)
								{
									var TripIds = (from T in Tbd
												   select T.TripId).ToList();

									var PuTitles = (from T in Tbd
													select T.PickupCompanyName).ToList();

									var DelTitles = (from T in Tbd
													 select T.DeliveryCompanyName).ToList();

									MapHelper.RemoveTripsForDriver(TripIds, PuTitles, DelTitles, webView);
								}
								MapHelper.RemoveAllTrips(webView);
								MapHelper.RemoveEverything(webView);

								M.UpdateLiveMap_Tick(null, null);
								//M.UpdateLiveMap_Tick_Worker(null, null, true);
							}
							else if (M.ActiveMapView == MapBrowserModel.MAP_VIEW_HISTORY)
							{
								MapHelper.RemoveDriver(Found.StaffId, webView);
								if (M.TripsByDriverHistory.ContainsKey(Found.StaffId) && M.TripsByDriverHistory[Found.StaffId] is { } Tbd)
								{
									var TripIds = (from T in Tbd
												   select T.TripId).ToList();

									var PuTitles = (from T in Tbd
													select T.PickupCompanyName).ToList();

									var DelTitles = (from T in Tbd
													 select T.DeliveryCompanyName).ToList();

									//MapHelper.RemoveTripsForDriver(TripIds, PuTitles, DelTitles, webView);
									MapHelper.RemoveAllTrips(webView);
									MapHelper.RemoveEverything(webView);
									//M.MapTripsHistory();
								}

								M.Execute_SearchHistory();
							}
						}						

						break;
					}
				}

			}
		}
	}

	public void UncheckAllDrivers()
	{
		foreach (var item in lbDrivers.GetChildren())
		{
			if (item is StackPanel sp)
			{
				foreach (var child in sp.Children)
				{
					if (child is CheckBox cb)
					{
						cb.IsChecked = false;
						break;
					}
				}
			}
		}
	}

	public void CheckDriver(string displayName)
	{
		if (Model is { } M)
		{
			if (displayName.IsNotNullOrWhiteSpace())
			{

				foreach (var item in lbDrivers.GetChildren())
				{
					if (item is StackPanel sp)
					{
						CheckBox? check = null;
						foreach (var child in sp.Children)
						{
							if (child is CheckBox cb)
							{
								check = cb;
							}
							else if (child is TextBlock tb && tb.Text == displayName)
							{
								if (check != null)
								{
									check.IsChecked = true;
								}
								break;
							}
						}
					}
				}
			}
		}
	}

	private void DtpFromDateTime_ValueChanged( object sender, RoutedPropertyChangedEventArgs<object> e )
	{
		//Logging.WriteLogLine("DEBUG FromDateTime: " + dtpFromDateTime.Value);
		if( Model is { } M && dtpFromDateTime.Value is { } Time)
			M.FromDateTime = Time;
	}

	private void DtpToDateTime_ValueChanged( object sender, RoutedPropertyChangedEventArgs<object> e )
	{
		//Logging.WriteLogLine("DEBUG ToDateTime: " + dtpToDateTime.Value);		
		if ( Model is { } M && dtpToDateTime.Value is { } Time )
			M.ToDateTime = Time;
	}

	private void BtnApply_Click( object sender, RoutedEventArgs e )
	{
		if (Model is { } M)
		{
			M.HasSearchRun = true;
			M.Execute_SearchHistory();
		}
	}


	private void BtnLoadDriversGroup_Click( object sender, RoutedEventArgs e )
	{
		if( Model is { } M )
			Logging.WriteLogLine( "DEBUG Loading Drivers Group: " + M.SelectedDriverGroup );

		tcDrivers.SelectedIndex = 1;
	}

	private void BtnSaveSelectedDriversNewGroup_Click( object sender, RoutedEventArgs e )
	{
		if( Model is {SelectedDrivers.Count: > 0} M )
		{
			var DriversGroupName = M.NewDriversGroupName;

			var Exists = ( from N in M.DriversGroupsNames
			               where string.Equals( N, DriversGroupName, StringComparison.CurrentCultureIgnoreCase )
			               select N ).FirstOrDefault();

			if( Exists.IsNullOrWhiteSpace() )
			{
				Logging.WriteLogLine( "DEBUG Save selected drivers to group " + DriversGroupName );

				M.Execute_SaveSelectedDrivers();

				var Caption = FindStringResource( "DriversMapButtonSaveSelectedDriversTitle" );
				var Message = FindStringResource( "DriversMapButtonSaveSelectedDriversMessage" );
				Message = Message.Replace( "@1", DriversGroupName );
				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
			}
			else
			{
				// Duplicate name
				var Caption = FindStringResource( "DriversMapLabelSaveSelectedDriversErrorTitle" );
				var Message = FindStringResource( "DriversMapLabelSaveSelectedDriversError" );
				Message = Message.Replace( "@1", DriversGroupName );

				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
			}
		}
	}

	private void BtnUpdateGroup_Click( object sender, RoutedEventArgs e )
	{
		if( Model is {SelectedDrivers.Count: > 0} M )
		{
			if( M.SelectedDriverGroup.IsNotNullOrWhiteSpace() )
			{
				Logging.WriteLogLine( "DEBUG Updating group " + M.SelectedDriverGroup );

				// TODO Check for errors

				M.Execute_UpdateSelectedGroup();

				var Caption = FindStringResource( "DriversMapButtonUpdateSelectedGroupTitle" );
				var Message = FindStringResource( "DriversMapButtonUpdateSelectedGroupMessage" );
				Message = Message.Replace( "@1", M.SelectedDriverGroup );
				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
			}
			else
			{
				// No group selected
				var Caption = FindStringResource( "DriversMapButtonButtonUpdateErrorTitle" );
				var Message = FindStringResource( "DriversMapButtonButtonUpdateError" );

				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
			}
		}
	}

	private void TogHideOnMap_Click( object sender, RoutedEventArgs e )
	{
		if( sender is ToggleButton Tb && Model is { } M )
		{
			var Driver = Tb.DataContext.ToString();
			//Logging.WriteLogLine( "DEBUG Driver: " + driver );

			//if( Tb.IsChecked == true )
			if( Tb.Content.ToString() == FindStringResource( "DriversMapButtonHideOnMap" ))
			{
				Tb.Content = FindStringResource( "DriversMapButtonShowOnMap" );
				M.HideDriverOnMap( Driver );
			}
			else
			{
				Tb.Content = FindStringResource( "DriversMapButtonHideOnMap" );
				M.ShowDriverOnMap( Driver );
			}
		}
	}

	private void BtnSelectAll_Click( object sender, RoutedEventArgs e )
	{
		if( Model is { } M )
		{
			foreach( var Driver in M.SelectedDrivers )
			{
				Driver.HasTrips = true;
				M.ShowDriverOnMap( Driver.StaffId );
			}

			foreach( var Child in lbSelectedDrivers.GetChildren() )
			{
				if( Child is ToggleButton {Name: "TogHideOnMap"} Tb )
					Tb.Content = FindStringResource( "DriversMapButtonHideOnMap" );
			}
		}
	}

	private void BtnSelectNone_Click( object sender, RoutedEventArgs e )
	{
		if( Model is { } M )
		{
			foreach( var Driver in M.SelectedDrivers )
			{
				Driver.HasTrips = false;
				M.HideDriverOnMap( Driver.StaffId );
			}

			foreach( var Child in lbSelectedDrivers.GetChildren() )
			{
				if( Child is ToggleButton {Name: "TogHideOnMap"} Tb )
					Tb.Content = FindStringResource( "DriversMapButtonShowOnMap" );
			}
		}
	}

	private void TcSelectMap_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		//Logging.WriteLogLine( "DEBUG TcSelectMap tab changed " + TcSelectMap.SelectedIndex );

		if( Model is { } M )
		{
			switch( TcSelectMap.SelectedIndex )
			{
			case MapBrowserModel.MAP_VIEW_LIVE:
				M.ActiveMapView = MapBrowserModel.MAP_VIEW_LIVE;
				M.SwitchToLiveLines();
				break;

			case MapBrowserModel.MAP_VIEW_HISTORY:
				M.ActiveMapView = MapBrowserModel.MAP_VIEW_HISTORY;
				M.SwitchToHistoryLines();
				break;
			}
		}
	}

	private void BtnRemoveChecked_Click( object sender, RoutedEventArgs e )
	{
		List<Driver> ToRemove = new();

		foreach( var Child in lbSelectedDrivers.GetChildren() )
		{
			if( Child is CheckBox {IsChecked: true} Cb )
			{
				//Logging.WriteLogLine( "DEBUG Removing " + cb.DataContext );
				ToRemove.Add( (Driver)Cb.DataContext );

				Cb.IsChecked = false;
			}
		}

		if( ( ToRemove.Count > 0 ) && Model is { } M )
		{
			UncheckSelectedInDriversList( ToRemove );
			M.RemoveSelected( ToRemove );
		}
	}

	private void BtnRemoveAll_Click( object sender, RoutedEventArgs e )
	{
		List<Driver> ToRemove = new();

		foreach( var Child in lbSelectedDrivers.GetChildren() )
		{
			if( Child is CheckBox Cb )
			{
				//Logging.WriteLogLine( "DEBUG Removing " + cb.DataContext );
				ToRemove.Add( (Driver)Cb.DataContext );

				Cb.IsChecked = false;
			}
		}

		if( ( ToRemove.Count > 0 ) && Model is { } M )
		{
			UncheckSelectedInDriversList( ToRemove );
			M.RemoveSelected( ToRemove );
		}

		//RemoveAllDrivers();
		//MapHelper.RemoveAllDrivers(this);
		MapHelper.RemoveAllDrivers( webView );
		//RemoveAllTrips();
		//MapHelper.RemoveAllTrips(this);
		MapHelper.RemoveAllTrips( webView );
	}

	private void UncheckSelectedInDriversList( List<Driver> toRemove )
	{
		foreach( var Child in lbDrivers.GetChildren() )
		{
			//if( child is CheckBox cb && ( (Driver)cb.DataContext ).StaffId == driver.StaffId )
			if( Child is CheckBox Cb )
			{
				var StaffId = ( (Driver)Cb.DataContext ).StaffId;

				foreach( var Driver in toRemove )
				{
					//Logging.WriteLogLine("DEBUG cb.StaffId: " + ((Driver)cb.DataContext).StaffId + ", driver.StaffId: " + driver.StaffId);
					if( StaffId == Driver.StaffId )
					{
						Cb.IsChecked = false;
						break;
					}
				}
			}
		}
	}

	private void CbShowRouteOnLive_Checked( object sender, RoutedEventArgs e )
	{
		//Logging.WriteLogLine( "Showing driver's route on live" );
		if( Model is { } M )
		{
			M.ShowDriversRouteOnLive = true;
			//M.UpdateLiveMap_Tick( null, null );
		}
	}

	private void CbShowRouteOnLive_Unchecked( object sender, RoutedEventArgs e )
	{
		//Logging.WriteLogLine( "Hiding driver's route on live" );

		if( Model is { } M )
		{
			M.ShowDriversRouteOnLive = false;
			//M.UpdateLiveMap_Tick( null, null );
		}
	}

	private void CbShowDriverOnLive_Checked( object sender, RoutedEventArgs e )
	{
		if( CbShowRouteOnLive is { } Cb )
			Cb.IsEnabled = true;

		if( Model is { } M )
		{
			M.ShowDriverOnLive = true;
			//M.UpdateLiveMap_Tick( null, null );
		}
	}

	private void CbShowDriverOnLive_Unchecked( object sender, RoutedEventArgs e )
	{
		if( CbShowRouteOnLive is { } Cb )
			Cb.IsEnabled = false;

		//CbShowRouteOnLive.IsChecked  = false;
		if( Model is { } M )
			M.ShowDriverOnLive = false;
		//Model.ShowDriversRouteOnLive = false;

		//Model.UpdateLiveMap_Tick(null, null);
		//RemoveAllDrivers();
		//MapHelper.RemoveAllTrips(this);
		//MapHelper.RemoveAllTrips(webView);
	}

	private void CbShowTripsOnLive_Checked( object sender, RoutedEventArgs e )
	{
		//if( DataContext is MapBrowserModel Model )
		//{
		//	Model.ShowTripsOnLive = true;
		//	Model.UpdateLiveMap_Tick( null, null );
		//}
	}

	private void CbShowTripsOnLive_Unchecked( object sender, RoutedEventArgs e )
	{
		//if( DataContext is MapBrowserModel Model )
		//{
		//	Model.ShowTripsOnLive = false;
		//	Model.UpdateLiveMap_Tick( null, null );

		//	//RemoveAllTrips();
		//	//MapHelper.RemoveAllTrips(this);
		//	MapHelper.RemoveAllTrips(webView);
		//}
	}

	private void CbShowPUTripsOnLive_Checked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MapBrowserModel Model)
		//{
		//    Model.ShowPUTripsOnLive = true;
		//}
	}

	private void CbShowPUTripsOnLive_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MapBrowserModel Model)
		//{
		//    Model.ShowPUTripsOnLive = false;
		//}
	}

	private void CbShowDelTripsOnLive_Checked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MapBrowserModel Model)
		//{
		//    Model.ShowDelTripsOnLive = true;
		//}
	}

	private void CbShowDelTripsOnLive_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MapBrowserModel Model)
		//{
		//    Model.ShowDelTripsOnLive = false;
		//}
	}

	/*
	private void CbShowDriverOnHistory_Checked( object sender, RoutedEventArgs e )
	{
	}

	private void CbShowDriverOnHistory_Unchecked( object sender, RoutedEventArgs e )
	{
	}

	private void CbShowRouteOnHistory_Checked( object sender, RoutedEventArgs e )
	{
	}

	private void CbShowRouteOnHistory_Unchecked( object sender, RoutedEventArgs e )
	{
	}
	private void CbShowTripsOnHistory_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is MapBrowserModel Model )
		{
			Model.ShowTripsOnHistory = true;

			//Model.UpdateLiveMap_Tick(null, null);
		}
	}

	private void CbShowTripsOnHistory_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is MapBrowserModel Model )
		{
			Model.ShowTripsOnHistory = false;

			//Model.UpdateLiveMap_Tick(null, null);
		}
	}
	*/

	private void WebView_NavigationCompleted( object sender, CoreWebView2NavigationCompletedEventArgs e )
	{
	}

	private void WebView_NavigationStarting( object sender, CoreWebView2NavigationStartingEventArgs e )
	{
	}

	private void CbShowTraffic_Checked( object sender, RoutedEventArgs e )
	{
		ToggleTraffic( true );
	}

	private void CbShowTraffic_Unchecked( object sender, RoutedEventArgs e )
	{
		ToggleTraffic( false );
	}

	private async void ToggleTraffic( bool show )
	{
		if( Model is { } M )
			M.IsTrafficVisible = show;

		var Script = "ShowTraffic(" + show.ToString().ToLower() + ")";
		await webView.CoreWebView2.ExecuteScriptAsync( Script );
	}

	/*
	private async void BtnTestRemove_Click( object sender, RoutedEventArgs e )
	{
		var Script = "RemoveDriver('driver');";
		await webView.CoreWebView2.ExecuteScriptAsync( Script );
	}
	private async void BtnTestRemoveAll_Click( object sender, RoutedEventArgs e )
	{
		var Script = "RemoveAllDrivers();";
		await webView.CoreWebView2.ExecuteScriptAsync( Script );
	}
	*/

	private void WebView_WebMessageReceived( object sender, CoreWebView2WebMessageReceivedEventArgs e )
	{
		// JsonObject jsonObject = JsonConvert.DeserializeObject<jsonObject>(e.WebMessageAsJson);
		try
		{
			var Message = e.TryGetWebMessageAsString();
			Logging.WriteLogLine( "DEBUG WebMessage: " + Message );
		}
		catch( Exception )
		{
		}
	}

    private void CbShowShipmentTotals_Checked(object sender, RoutedEventArgs e)
    {

    }

    private void CbShowShipmentTotals_Unchecked(object sender, RoutedEventArgs e)
    {

    }

	private void CbShowDriversInfoBoxes_Unchecked(object sender, RoutedEventArgs e)
	{

	}

	private void CbShowDriversInfoBoxes_Checked(object sender, RoutedEventArgs e)
	{

	}

	private void BtnRedrawMap_Click(object sender, RoutedEventArgs e)
	{
		if ( Model is { }  M)
		{
			ObservableCollection<Driver> backup = new();
			foreach (var driver in M.SelectedDrivers)
			{
				backup.Add( driver );
			}
			M.Execute_RemoveAll();

			foreach (Driver driver in backup)
			{
				M.AddSelectedDriver(driver.DisplayName);
			}
		}
	}
}