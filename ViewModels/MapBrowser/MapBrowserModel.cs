﻿#nullable enable

using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Threading;
using System.Xml;
using Microsoft.Maps.MapControl.WPF;
using ViewModels.Trips.Boards.Common;
using File = System.IO.File;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

namespace ViewModels.MapBrowser;

public class DriverOnMap
{
	public MapPolyline? Line { get; set; }

	/// <summary>
	///     Holds the position for the driver's current position.
	/// </summary>
	public GpsPoint? Last { get; set; }

	public string? Latitude { get; set; }

	public string? Longitude { get; set; }

	private          GpsPoints        GpsPoints { get; set; } = new();
	private readonly MapBrowserModel? Model;
	public           string?          StaffId;

	public GpsPoints GetGpsPoints() => GpsPoints;

	public void SetGpsPoints( GpsPoints gps )
	{
		GpsPoints = gps;

		if( gps.Count > 0 )
		{
			Last      = gps[ gps.Count - 1 ];
			Latitude  = $"{Last.Latitude}";
			Longitude = $"{Last.Longitude}";
			Logging.WriteLogLine( "DEBUG Setting Last for driver: " + StaffId + " to " + Last.Latitude + "," + Last.Longitude + " , timestamp: " + Last.LocalDateTime );
		}
		else
		{
			//Last = new()
			//{
			//    Latitude = 49.317640851387331, // Vancouver
			//    Longitude = -123.11948939837492
			//    //Latitude  = -37.8084816, // Melbourne
			//    //Longitude = 144.8059559
			//};
			if( Model is {MostFrequentTownGps: { } L} )
			{
				Last      = L;
				Latitude  = $"{L.Latitude}";
				Longitude = $"{L.Longitude}";
				Logging.WriteLogLine( "DEBUG Setting default Last for driver: " + StaffId + " to " + Last.Latitude + "," + Last.Longitude );
			}
		}
	}

	public void AppendGpsPoints( GpsPoints gps )
	{
		var Before = GetGpsPoints();
		Before.AddRange( gps );
		SetGpsPoints( Before );
	}

	public DriverOnMap( MapBrowserModel? model )
	{
		Model = model;
	}

	public DriverOnMap( string? staffId, MapBrowserModel? model )
	{
		StaffId = staffId;
		Model   = model;
	}

	public DriverOnMap( string? staffId, MapPolyline? line = null )
	{
		StaffId = staffId;

		if( line is not null )
			Line = line;
	}
}

//internal class MapBrowserModel : ViewModelBase
public class MapBrowserModel : ViewModelBase
{
	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	//[DependsUpon(nameof(Loaded))]
	//public void WhenLoadedChanges()
	//{
	//    //if (Loaded)
	//    //{
	//    //    //GetCurrentLocation();
	//    //}
	//}

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1818984449/Drivers+GPS+Map" ); }
	}

	private DispatcherTimer? UpdateLiveMapTimer;
	public  MapBrowser?      View;

	public void SetView( MapBrowser mb )
	{
		View = mb;
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		LoadSettings();

		Loaded = true;

		SwitchToLiveLines();

		CurrentMapLabelView = FindStringResource( "DriversMapLabelCurrentMapViewLiveNoAccelerator" );

		//SelectedDrivers.CollectionChanged += SelectedDrivers_CollectionChanged;
	}

	private void SelectedDrivers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
	{
		Logging.WriteLogLine("DEBUG SelectedDriversChanged");
		Thread.Sleep(2000);
		WhenSelectedDriversChanges();
	}

	#region WebView2
	public readonly string Html = MapHelper.Html;

	public string ShowCurrentLocationHtml = @"
//Request the user's location
navigator.geolocation.getCurrentPosition(function (position) {
    var loc = new Microsoft.Maps.Location(
        position.coords.latitude,
        position.coords.longitude);

    //Add a pushpin at the user's location.
    //var pin = new Microsoft.Maps.Pushpin(loc, ""Current Location"");
    /*var pin = new Microsoft.Maps.Pushpin(loc, {
        title: ""Title"",
        subTitle: ""subTitle"",
        text: ""1""
    });*/
    var pin = new Microsoft.Maps.Pushpin(loc, {
            icon: '<svg xmlns:svg=""http://www.w3.org/2000/svg"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"" width=""25"" height=""40"" ><circle cx=""12.5"" cy=""14.5"" r=""10"" fill=""{color}"" /><image x=""0"" y=""0"" height=""40"" width=""25"" xlink:href=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAnCAYAAAGNntMoAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxMAAAsTAQCanBgAAAPBSURBVEhLtZY/SGRXFMaHpE0bSJVkt8oKAf+CKytispGIjY1NcGXF/QM2wlYLgyCC+bPYpUhSCgZiJhapxEAKC0HE3cUtBLETFrExG2zMkPHm+5059/nmvTG+bHY+OL57vvOd79775s4dSzmMjY297cP6uKur67Pz8/NPFYOMrXJ0dBQIS5qis7PzgQ9LJfXesEEI4baSTzCLxIVJQzI6OmpJJGOO8wsbZJDwh4eHQRM8VzxV8w65FSK6u7vvYMvTqUb09vY2dkRoji7C0zqwSsPI4+NjGyj/KG7fuESRgnH8EeL7sXcEUero6PgclYhnyncYR44VfYwKMDYywvn8Xnp6egLhaUHI/8f19XU3rYMc3iUXwB4w1sq/9/gujuFroXaxDB2uJ7HhKqBDz5J++y9N6C1ZWFiIS/smhupfExp/pfgyrUugaadWV1c5YS+dMpDDU3cqj6GhIVaRgNxL/461tTVr4OnU1dAhuVmr1QJPp1oAbgm91i8Ud51qDr2ZWxMTE+H09NT2AqrVaoCj5rI6RJTTX1zXJzk1NJawhN3d3aSozyJ31gAGdqlx6FSoOk/DjXQ4bbADurS0VPi9m/bg4KBwg2m3trYKN5h2enraGrSn24TWzSVNJPcHgca0el2PeQMiLz3yPNGgpbE0MDBAHi/UZ4qnMUTvUENj4oi+vj743H7gqHnaCC7dLC69iIE+mPc43mnAebk5ZmdnXRoCY6cvB5eb65tfdFlwwE5OTgJhh60IJicnA+Hp1dBGbxKethD6lRzQcfiJ78H4+Hgol8uBW3Fubs5yeOrovKUY1PRocHDQ7jSgI9Vwo2ZBHR16+uj3Uh78tg8PDwd+c4HTBhlxltPBubaxSwxn539ZPz65/xU0e3l+fj5nDmQ0Kfpus4g1lyYQF/DD1wgNbk1NTcEXPxYFgB+++DPJ79vb2y2ZBF/8+Qo939/fb8kk+OLPTp5w88ZJ9Liud30tHeI+zIb4D7Ih/n0iToIv/vbd5rzzv6+EVcVD1R+kQ9z9bJ6Ke9k4+ftVVTr7HjXcF9rWrxsbG9RsRxL/4lGJodLP2aePVwj6lAd88CPPQVt7NDIyEvb29tAmn5EM/iREvYqh/A/CJWZOH/34OH05tM12CX+oVFjs1UCHnj63KA41Pl5cXHSr5qCOzlteDzJY29zcdMtGwFN36eujra3tnf7+/nB2dubWF4Cn7tL/B52UkZmZGbeugxzeJW8GMvx2ZWXFJuBJ7qU3C13fleXlZa7xilMtwVvt7e3v+rggSqV/ABNOzES/IWvuAAAAAElFTkSuQmCC"" /></svg>',
            color: 'red'
        });
    pin.setOptions({color:'red', title:""Title""});
    map.entities.push(pin);

    //Center the map on the user's location.
    map.setView({ center: loc, zoom: 15 });
});";

	public string ShowDriverLocationHtml = @"
var loc = new Microsoft.Maps.Location(@latitude, @longitude);
var pin = new Microsoft.Maps.Pushpin(loc, {title: ""'@title'"", color: ""'@color'""});
map.entities.push(pin);
map.setView({center: loc, zoom: 15});
";

	public bool IsTrafficVisible
	{
		get { return Get( () => IsTrafficVisible, false ); }
		set { Set( () => IsTrafficVisible, value ); }
	}


	public bool IsCurrentLocationVisible
	{
		get { return Get( () => IsCurrentLocationVisible, true ); }
		set { Set( () => IsCurrentLocationVisible, value ); }
	}


	public string GetMapHtml()
	{
		// var strHtml = Html;
		var StrHtml = Html.Replace( "[BING_MAPS_KEY]", BingMapApiKey );

		return StrHtml;
	}

	private (int totalTrips, decimal totalWeight, int totalPieces) GetDriversLoadDetails( string staffId )
	{
		var     TotalTrips  = 0;
		decimal TotalWeight = 0;
		var     TotalPieces = 0;
		var     Trips       = new List<Trip>();

		if( TripsByDriverLive.Count == 0 )
			GetTripsForDriversLive();

		if( TripsByDriverHistory.Count == 0 )
			GetTripsForDriversHistory();

		switch( ActiveMapView )
		{
			case MAP_VIEW_LIVE when TripsByDriverLive.ContainsKey( staffId ):
				Trips = TripsByDriverLive[ staffId ];
				break;
			case MAP_VIEW_HISTORY when TripsByDriverHistory.ContainsKey( staffId ):
				Trips = TripsByDriverHistory[staffId];
				break;
		}

		if( Trips is {Count: > 0} )
		{
			TotalTrips = Trips.Count;

			foreach( var Trip in Trips )
			{
				TotalWeight += Trip.Weight;
				TotalPieces += (int)Trip.Pieces;
			}
		}

		return ( TotalTrips, TotalWeight, TotalPieces );
	}

	public string GetContentForDriver( string staffId, GpsPoint? lastPoint = null)
	{
		var (TotalTrips, TotalWeight, TotalPieces) = GetDriversLoadDetails( staffId );
		string lastPing = string.Empty;
		if (lastPoint != null)
		{
			DateTimeOffset last = lastPoint.LocalDateTime;
			lastPing = last.ToString("dd/MM/yyyy HH:mm:ss");
		}
		//GpsPoints? points = GetDriversGps(staffId, false);
		GpsPoints? points = null;
		if (ActiveMapView == MAP_VIEW_LIVE)
		{
			if (DictLiveDrivers.ContainsKey(staffId)) 
			{
				points = DictLiveDrivers[staffId].GetGpsPoints();
			}
		}
		else if (ActiveMapView == MAP_VIEW_HISTORY)
		{
			if (DictHistoryDrivers.ContainsKey(staffId))
			{
				points = DictHistoryDrivers[staffId].GetGpsPoints();
			}
		}
		if (points == null)
		{
			points = GetDriversGps(staffId, false);
		}
		if (points != null && lastPing == string.Empty)
		{
			GpsPoint point = points.LastOrDefault();
			lastPing = point.LocalDateTime.ToString("dd/MM/yyyy HH:mm:ss");
		}
		var Content = FindStringResource( "DriversMapLabelTripsCountPrefix" ) + " " + TotalTrips + "<br/>"
		              + FindStringResource( "DriversMapLabelTotalWeightPrefix" ) + " " + TotalWeight + "<br/>"
		              + FindStringResource( "DriversMapLabelTotalPiecesPrefix" ) + " " + TotalPieces + "<br/>"
					  + lastPing + "<br/>";



		return Content;
	}

	//public void TestDisplayDrivers()
	//{            
	//    var staffId = "driver";
	//    GpsPoint gps = new()
	//    {
	//        Latitude = double.Parse("-37.813938140869141"),
	//        Longitude = double.Parse("144.96342468261719")
	//    };
	//    //ShowDriverOnMap(staffId, gps);
	//    staffId = "driver2";
	//    gps = new()
	//    {
	//        Latitude = double.Parse("-37.913938140869141"),
	//        Longitude = double.Parse("144.86342468261719")
	//    };
	//    //ShowDriverOnMap(staffId, gps);

	//}
#endregion

#region Data & Helper methods
	public List<ServiceLevel> ServiceLevels
	{
		get { return Get( () => ServiceLevels, new List<ServiceLevel>() ); }
		set { Set( () => ServiceLevels, value ); }
	}


	public async Task GetServiceLevels()
	{
		//Logging.WriteLogLine("DEBUG Loading service levels");
		if( !IsInDesignMode )
		{
			try
			{
				var Sls = await Azure.Client.RequestGetServiceLevelsDetailed();

				if( Sls is not null )
				{
					var Levels = ( from S in Sls
					               orderby S.SortOrder, S.OldName
					               select S ).ToList();

					foreach( var Sl in Levels )
					{
						var NewSl = new ServiceLevel( Sl );
						ServiceLevels.Add( NewSl );
					}
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown: " + E );

				//MessageBox.Show("Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK);
			}
		}
	}

	public static string FindStringResource( string name )
	{
		var Current = Application.Current;

		if( Current is not null )
			return (string)Current.TryFindResource( name );

		return "";
	}


	//public MapLayer MapLayerLive
	//{
	//    get { return Get(() => MapLayerLive, new MapLayer()); }
	//    //set { Set(() => MapLayerLive, value); }
	//}


	//public MapLayer MapLayerHistory
	//{
	//    get { return Get(() => MapLayerHistory, new MapLayer()); }
	//    //set { Set(() => MapLayerHistory, value); }
	//}


	public string Latitude
	{
		//get { return Get(() => Latitude, "49.2765"); }
		get { return Get( () => Latitude, "0" ); }
		set { Set( () => Latitude, value ); }
	}


	public string Longitude
	{
		//get { return Get(() => Longitude, "-123.1030"); }
		get { return Get( () => Longitude, "0" ); }
		set { Set( () => Longitude, value ); }
	}

	//[DependsUpon(nameof(Latitude))]
	//[DependsUpon(nameof(Longitude))]
	//public void WhenLatitudeLongitudeChange()
	//{
	//    Centre = Latitude + "," + Longitude;
	//    Logging.WriteLogLine("Setting Centre: " + Centre);
	//}

	public string Centre
	{
		get { return Get( () => Centre, "" ); }
		set { Set( () => Centre, value ); }
	}


	public string BingMapApiKey
	{
		get { return Get( () => BingMapApiKey, "" ); }
		set { Set( () => BingMapApiKey, value ); }
	}

	//[DependsUpon(nameof(BingMapApiKey))]
	//public void WhenBingMapApiKeyChanges()
	//{
	//    if (BingMapApiKey != null && BingMapApiKey.Length > 0)
	//    {
	//        var loc = GetHomeLocation(DriversMap);

	//        //Logging.WriteLogLine("DEBUG: HomeLocation: " + loc);
	//    }
	//}


	public async Task GetBingMapApiKey()
	{
		//Logging.WriteLogLine("DEBUG Getting map api keys");

		if( !IsInDesignMode )
		{
			var Keys = await Azure.Client.RequestGetMapsApiKeys();

			if( Keys is not null )
			{
				var Key = Keys.BingMapsKey;

				//Logging.WriteLogLine("DEBUG BingMapsKey: " + key);
				BingMapApiKey = Key;
			}
		}
	}

	public ObservableCollection<Driver> AllDrivers
	{
		get { return Get( () => AllDrivers, new ObservableCollection<Driver>() ); }
		set { Set( () => AllDrivers, value ); }
	}

	public ObservableCollection<Driver> Drivers
	{
		get { return Get( () => Drivers, new ObservableCollection<Driver>() ); }
		set { Set( () => Drivers, value ); }
	}

	private readonly List<Driver> DriversWithTrips = new();

	public ObservableCollection<Driver> SelectedDrivers
	{
		get { return Get( () => SelectedDrivers, new ObservableCollection<Driver>() ); }
		set { Set( () => SelectedDrivers, value ); }
	}

	public int SelectedDriversCount
	{
		get { return SelectedDrivers.Count; }
		set { SelectedDriversCount = value; }
	}

	public ObservableCollection<Driver> SelectedDriversLive
	{
		get { return Get(() => SelectedDriversLive, new ObservableCollection<Driver>()); }
		set { Set(() => SelectedDriversLive, value); }
	}

	public ObservableCollection<Driver> SelectedDriversHistory
	{
		get { return Get(() => SelectedDriversHistory, new ObservableCollection<Driver>()); }
		set { Set(() => SelectedDriversHistory, value); }
	}

	//[DependsUpon500(nameof(SelectedDrivers))]
	public void WhenSelectedDriversChanges()
	{
		foreach (var d in SelectedDrivers)
		{
			Logging.WriteLogLine("SelectedDrivers: " + d.DisplayName);
		}
		if (ActiveMapView == MAP_VIEW_LIVE && SelectedDrivers.Count > 0 && View is { } V)
		{
			Driver driver = SelectedDrivers.Last();
			if (DictLiveDrivers.ContainsKey(driver.StaffId))
			{
				if (DictLiveDrivers[driver.StaffId].Last != null)
				{
					GpsPoint? last = DictLiveDrivers[driver.StaffId].Last;
					if (last?.Latitude > 0)
					{
						Dispatcher.Invoke(() =>
						{
							MapHelper.CentreMapOnPoint(last, V.webView);
						});
					}
				}
			}
		}
	}

	public void AddSelectedDriver( string displayName )
	{
		Logging.WriteLogLine("DEBUG Adding selected " + displayName);
		var Found = ( from D in AllDrivers
		              where D.DisplayName == displayName
		              select D ).FirstOrDefault();

		if( Found is { } F && View is { } V )
		{
			V.SetWaitCursor();
			F.HasTrips = true;
			bool alreadyIn = (from D in SelectedDrivers where D.DisplayName == displayName select D).Any();
			if( !alreadyIn )
			{
				SelectedDrivers.Add( Found );
			}

			if( ActiveMapView == MAP_VIEW_LIVE )
			{
				// MapTripsLive();
				UpdateLiveMap_Tick_Worker( null, null, true );

				//if (DictLiveDrivers.TryGetValue(Found.StaffId, out var DriverOnMap))
				//if (DictLiveDrivers.ContainsKey(Found.StaffId))
				//{
				//	var dom = DictLiveDrivers[Found.StaffId];
				//	if ( dom != null && dom.Last != null )
				//	{
				//		MapHelper.CentreMapOnPoint(dom.Last, V.webView);
				//	}
				//}
			}
			else
			{
				if (DictHistoryDrivers.TryGetValue(Found.StaffId, out var DriverOnMap))
					MapDriver(Found.StaffId, DriverOnMap.GetGpsPoints());

				MapTripsHistory();
			}

			V.ClearWaitCursor();


			//Thread.Sleep(500);

			//WhenSelectedDriversChanges();
		}
	}

	public void RemoveSelectedDriver( string displayName )
	{
		Logging.WriteLogLine("DEBUG Removing selected " + displayName);
		var Found = ( from D in AllDrivers
		              where D.DisplayName == displayName
		              select D ).FirstOrDefault();

		if( Found?.StaffId is { } Id )
		{
			SelectedDrivers.Remove( Found );
			HideDriverOnMap( Id );
		}
	}


	public async Task<ObservableCollection<Driver>> LoadDrivers()
	{
		var DriversCollection = new ObservableCollection<Driver>();

		if( !IsInDesignMode )
		{
			var Tmp = await Azure.Client.RequestGetDrivers();

			foreach( var Driver in Tmp )
			{
				//Logging.WriteLogLine("DEBUG Found driver " + driver.StaffId);
				Driver D = new()
				           {
					           StaffId     = Driver.StaffId,
							   DisplayName = Driver.StaffId + " (" +Driver.FirstName + " " + Driver.LastName + ")",
					           IsVisible   = false
				           };

				DriversCollection.Add( D );
			}

			var List = ( from D in DriversCollection
			             orderby D.DisplayName.ToLower()
			             select D ).ToList();

			if( !ShowDriversWithTripsOnLive )
				Drivers = new ObservableCollection<Driver>( List );
			else
			{
				switch( ActiveMapView )
				{
				case MAP_VIEW_LIVE:
					Drivers = new ObservableCollection<Driver>();

					if( DictLiveDrivers.Count == 0 )
						GetTripsForDriversLive();

					var Raw = DriversCollection.Where( driver => TripsByDriverLive.ContainsKey( driver.StaffId ) ).ToList();

					Raw = ( from D in Raw
					        orderby D.DisplayName.ToLower()
					        select D ).ToList();

					Drivers = new ObservableCollection<Driver>( Raw );
					DriversWithTrips.Clear();
					DriversWithTrips.AddRange( Raw );

					break;

				case MAP_VIEW_HISTORY:
					break;
				}
			}
			AllDrivers = new ObservableCollection<Driver>( List );

			foreach( var D in AllDrivers )
			{
				var Id = D.StaffId;

				if( !TripsByDriverLive.ContainsKey( Id ) )
					TripsByDriverLive.Add( Id, new List<Trip>() );

				if( !TripsByDriverHistory.ContainsKey( Id ) )
					TripsByDriverHistory.Add( Id, new List<Trip>() );
			}

			//Worker.DoWork             += Worker_DoWorkMapTrips;
			//Worker.RunWorkerCompleted += Worker_Completed;
			//Worker.RunWorkerAsync();
		}

		return DriversCollection;
	}


	public string AdhocFilter
	{
		get { return Get( () => AdhocFilter, "" ); }
		set { Set( () => AdhocFilter, value ); }
	}

	[DependsUpon( nameof( AdhocFilter ) )]
	public void WhenAdhocFilterChanges()
	{
		//Logging.WriteLogLine("DEBUG AdhocFilter: " + AdhocFilter);
		var                          Filter = AdhocFilter.TrimToLower();
		ObservableCollection<Driver> Oc     = new();

		if( ShowDriversWithTripsOnLive )
		{
			if( Filter.IsNotNullOrWhiteSpace() )
			{
				foreach( var D in Drivers )
				{
					if( D.DisplayName.TrimToLower().Contains( Filter ) )
						Oc.Add( D );
				}
			}
			else
				Oc = new ObservableCollection<Driver>( DriversWithTrips );
		}
		else
		{
			if( Filter.IsNotNullOrWhiteSpace() )
			{
				foreach( var D in AllDrivers )
				{
					if( D.DisplayName.TrimToLower().Contains( Filter ) )
						Oc.Add( D );
				}
			}
			else
				Oc = new ObservableCollection<Driver>( AllDrivers );
		}

		Drivers = Oc;
	}

	public DateTimeOffset FromDateTime
	{
		get { return Get( () => FromDateTime, DateTimeOffset.Now ); }
		set { Set( () => FromDateTime, value ); }
	}

	[DependsUpon( nameof( FromDateTime ) )]
	public void WhenFromDateTimeChanges()
	{
		//Logging.WriteLogLine("DEBUG FromDateTime: " + FromDateTime);
		var Tmp = new DateTimeOffset( FromDateTime.Year, FromDateTime.Month, FromDateTime.Day, FromDateTime.Hour, FromDateTime.Minute, FromDateTime.Second, FromDateTime.Offset );
		FromDateTime = Tmp;
	}

	public DateTimeOffset ToDateTime
	{
		get { return Get( () => ToDateTime, DateTimeOffset.Now ); }
		set { Set( () => ToDateTime, value ); }
	}

	[DependsUpon( nameof( ToDateTime ) )]
	public void WhenToDateTimeChanges()
	{
		//Logging.WriteLogLine("DEBUG ToDateTime: " + ToDateTime);
		var Tmp = new DateTimeOffset( ToDateTime.Year, ToDateTime.Month, ToDateTime.Day, ToDateTime.Hour, ToDateTime.Minute, ToDateTime.Second, ToDateTime.Offset );
		ToDateTime = Tmp;
	}

	public bool HasSearchRun
	{
		get { return Get(() => HasSearchRun, false); }
		set { Set(() => HasSearchRun, value); }
	}

	public string SelectedDriverGroup
	{
		get { return Get( () => SelectedDriverGroup, "" ); }
		set { Set( () => SelectedDriverGroup, value ); }
	}

	[DependsUpon( nameof( SelectedDriverGroup ) )]
	public bool IsSelectedDriverGroupLoaded
	{
		//get { return Get(() => IsSelectedDriverGroupLoaded, SelectedDriverGroup.IsNotNullOrWhiteSpace()); }
		get
		{
			Logging.WriteLogLine( "DEBUG SelectedDriverGroup.IsNotNullOrWhiteSpace(): " + SelectedDriverGroup.IsNotNullOrWhiteSpace() );
			return SelectedDriverGroup.IsNotNullOrWhiteSpace();
		}
	}


	public string NewDriversGroupName
	{
		get { return Get( () => NewDriversGroupName, "" ); }
		set { Set( () => NewDriversGroupName, value ); }
	}


	public ObservableCollection<string> DriversGroupsNames
	{
		get { return Get( () => DriversGroupsNames, new ObservableCollection<string>() ); }
		set { Set( () => DriversGroupsNames, value ); }
	}


	public Dictionary<string, DriverOnMap> DictHistoryDrivers
	{
		get { return Get( () => DictHistoryDrivers, new Dictionary<string, DriverOnMap>() ); }
		set { Set( () => DictHistoryDrivers, value ); }
	}

	public Dictionary<string, DriverOnMap> DictLiveDrivers
	{
		get { return Get( () => DictLiveDrivers, new Dictionary<string, DriverOnMap>() ); }
		set { Set( () => DictLiveDrivers, value ); }
	}


	public Dictionary<string, List<Trip>> TripsByDriverLive
	{
		get { return Get( () => TripsByDriverLive, new Dictionary<string, List<Trip>>() ); }
		set { Set( () => TripsByDriverLive, value ); }
	}

	public Dictionary<string, List<Trip>> TripsByDriverHistory
	{
		get { return Get( () => TripsByDriverHistory, new Dictionary<string, List<Trip>>() ); }
		set { Set( () => TripsByDriverHistory, value ); }
	}


	public double HistoryZoomLevel
	{
		get { return Get( () => HistoryZoomLevel, 12 ); }
		set { Set( () => HistoryZoomLevel, value ); }
	}

	public double LiveZoomLevel
	{
		get { return Get( () => LiveZoomLevel, 12 ); }
		set { Set( () => LiveZoomLevel, value ); }
	}


	public string CurrentMapLabelView
	{
		get { return Get( () => CurrentMapLabelView, FindStringResource( "DriversMapLabelCurrentMapViewLiveNoAccelerator" ) ); }
		set { Set( () => CurrentMapLabelView, value ); }
	}

	public const int MAP_VIEW_LIVE    = 0;
	public const int MAP_VIEW_HISTORY = 1;

	public int ActiveMapView
	{
		get { return Get( () => ActiveMapView, MAP_VIEW_LIVE ); }
		set { Set( () => ActiveMapView, value ); }
	}


	public void SwitchToHistoryLines()
	{
		Logging.WriteLogLine( "DEBUG Switching to History Lines" );
		CurrentMapLabelView = FindStringResource( "DriversMapLabelCurrentMapViewHistoryNoAccelerator" );

		Dispatcher.Invoke( () =>
		                   {
			                   //if( DriversMap != null )
			                   //{
			                   // DriversMap.Children.Clear();

			                   // LiveZoomLevel        = DriversMap.ZoomLevel;
			                   // DriversMap.ZoomLevel = HistoryZoomLevel;

			                   // foreach( var staffId in DictHistoryDrivers.Keys )
			                   // {
			                   //  Driver driver = ( from D in Drivers
			                   //				 where D.StaffId == staffId
			                   //				 select D ).FirstOrDefault();

			                   //  // Check that the driver isn't hidden
			                   //  if( driver?.HasTrips == true )
			                   //  {
			                   //   //Logging.WriteLogLine("DEBUG Adding driver: " + staffId);
			                   //   DriversMap.Children.Add( DictHistoryDrivers[ staffId ].Line );

			                   //   PutDriversLabelOnMap( driver.StaffId, DictHistoryDrivers[ staffId ].Last );
			                   //  }
			                   // }
			                   //}

			                   if( View is { } V )
			                   {
								   MapHelper.RemoveEverything(V.webView);
								   MapHelper.RemoveAllDrivers(V.webView);
								   MapHelper.RemoveAllTrips(V.webView);
								   SelectedDriversLive.Clear();
								   foreach (var driver in SelectedDrivers)
								   {
									   SelectedDriversLive.Add(driver);
								   }
								   SelectedDrivers.Clear();
								   V.UncheckAllDrivers();
								   foreach (var driver in SelectedDriversHistory)
								   {
									   SelectedDrivers.Add(driver);
									   V.CheckDriver(driver.DisplayName);
								   }

								   MapHelper.RemoveAllDrivers( V.webView );
				                   MapHelper.RemoveAllTrips(V.webView);
				                   //RemoveTripsFromMap();

				                   //Execute_SearchHistory();
				                   if( ShowDriverOnHistory && SelectedDrivers.Count > 0 )
				                   {
					                   //foreach( var Driver in SelectedDrivers )
					                   foreach( var Driver in SelectedDrivers )
					                   {
						                   var Id = Driver.StaffId;

						                   if( DictHistoryDrivers.TryGetValue( Id, out var DriverOnMap ) )
						                   {
							                   var Points = DriverOnMap.GetGpsPoints();

							                   if( Points.Count > 0 )
								                   MapDriver( Driver.StaffId, Points );
						                   }
					                   }
				                   }
				                   MapTripsHistory();
			                   }
		                   } );
	}

	public void SwitchToLiveLines()
	{
		Logging.WriteLogLine( "DEBUG Switching to Live Lines" );
		CurrentMapLabelView = FindStringResource( "DriversMapLabelCurrentMapViewLiveNoAccelerator" );

		Dispatcher.Invoke( () =>
		                   {
			                   //if( DriversMap != null )
			                   //{
			                   // DriversMap.Children.Clear();

			                   // HistoryZoomLevel     = DriversMap.ZoomLevel;
			                   // DriversMap.ZoomLevel = LiveZoomLevel;

			                   // foreach( var staffId in DictLiveDrivers.Keys )
			                   // {
			                   //  Driver driver = ( from D in Drivers
			                   //				 where D.StaffId == staffId
			                   //				 select D ).FirstOrDefault();

			                   //  // Check that the driver isn't hidden
			                   //  if( driver?.HasTrips == true )
			                   //  {
			                   //   //Logging.WriteLogLine("DEBUG Adding driver: " + staffId);
			                   //   DriversMap.Children.Add( DictLiveDrivers[ staffId ].Line );
			                   //  }
			                   // }
			                   //}
			                   if( View is { } V )
			                   {
								   MapHelper.RemoveEverything(V.webView);
								   SelectedDriversHistory.Clear();
								   foreach (var driver in SelectedDrivers)
								   {
									   SelectedDriversHistory.Add(driver);
								   }
								   SelectedDrivers.Clear();
								   V.UncheckAllDrivers();
								   MapHelper.RemoveAllDrivers( V.webView );
				                   MapHelper.RemoveAllTrips( V.webView );
								   foreach (var driver in SelectedDriversLive)
								   {
									   SelectedDrivers.Add(driver);
									   //if (driver.HasTrips)
									   //{
										  // V.CheckDriver(driver.DisplayName);
									   //}
									   V.CheckDriver(driver.DisplayName);
								   }								   
				                   UpdateLiveMap_Tick( null, null );
			                   }
		                   } );
	}

	public void HideDriverOnMap( string staffId )
	{
		Logging.WriteLogLine( "DEBUG Hiding driver on map " + staffId );

		if( View is { } V )
		{
			var Driver = ( from D in Drivers
			               where D.StaffId == staffId
			               select D ).FirstOrDefault();

			if( Driver is not null )
				Driver.HasTrips = false;

			Dispatcher.Invoke( () =>
			                   {
								   List<string> TripIds = new();
								   List<string> PuTitles = new();
								   List<string> DelTitles = new();
								   List<Trip>? trips = null;

				                   switch( ActiveMapView )
				                   {
									   case MAP_VIEW_LIVE:
										   if( DictLiveDrivers.ContainsKey( staffId ) )
										   {
											   MapHelper.RemoveDriver( staffId, V.webView );

											   if( TripsByDriverLive[ staffId ] is { } Tbd )
											   {
												   trips = Tbd;
											   }

											   // RemoveSelectedDriver(driver.DisplayName);

											   //if( TripsByDriverLive[ staffId ] is { } Tbd )
											   //{
												  // TripIds = ( from T in Tbd
														//		   select T.TripId ).ToList();

												  // PuTitles = ( from T in Tbd
														//			select T.PickupCompanyName ).ToList();

												  // DelTitles = ( from T in Tbd
														//			 select T.DeliveryCompanyName ).ToList();

												  // //MapHelper.RemoveTripsForDriver( TripIds, PuTitles, DelTitles, V.webView );
											   //}
										   }
										   break;

									   case MAP_VIEW_HISTORY:
										   if( DictHistoryDrivers.ContainsKey( staffId ) )
										   {
											   MapHelper.RemoveDriver(staffId, V.webView );
											   if (TripsByDriverHistory[staffId] is { } Tbd)
											   {
												   trips = Tbd;
											   }
											   //if (TripsByDriverHistory[staffId] is { } Tbd)
											   //{
											   // TripIds = (from T in Tbd
											   //		  select T.TripId).ToList();



											   //PuTitles = (from T in Tbd
											   //	   select T.PickupCompanyName).ToList();

											   //DelTitles = (from T in Tbd
											   //		select T.DeliveryCompanyName).ToList();

											   //MapHelper.RemoveTripsForDriver(TripIds, PuTitles, DelTitles, V.webView);
											   //}
										   }
										   break;
				                   }


								   //MapHelper.RemoveDriver( staffId, V.webView );
								   if (trips != null)
								   {
									   //TripIds = ( from T in dom
									   //select T.TripId ).ToList();

									   foreach (var trip in trips)
									   {
										   TripIds.Add(trip.TripId);
										   //PuTitles.Add(trip.PickupCompanyName + " " + FormatDateTimeOffset(trip.PickupTime));
										   PuTitles.Add(trip.PickupCompanyName);
										   //DelTitles.Add(trip.DeliveryCompanyName + " " + FormatDateTimeOffset(trip.DeliveryTime));
										   DelTitles.Add(trip.DeliveryCompanyName);
									   }

									   //foreach (var trip in trips)
									   //{
										  // DelTitles.Add(trip.DeliveryCompanyName + " " + FormatDateTimeOffset(trip.DeliveryTime));
									   //}

									   MapHelper.RemoveTripsForDriver(TripIds, PuTitles, DelTitles, V.webView);
								   }
			                   } );
		}
	}

	public void ShowDriverOnMap( string staffId )
	{
		Logging.WriteLogLine( "DEBUG Showing driver on map " + staffId );

		var Driver = ( from D in Drivers
		               where D.StaffId == staffId
		               select D ).FirstOrDefault();

		if( Driver is not null )
			Driver.HasTrips = true;

		if( DictLiveDrivers[ staffId ].Last is { } Point )
		{
			PutDriversLabelOnMap( staffId, Point );
			if (View is { } V)
			{
				MapHelper.CentreMapOnPoint(Point, V.webView);
			}
		}

		if( ShowTripsOnLive )
			MapTripsLiveForDriver( staffId, false );

		//var gps = GetDriversGps(staffId);
		//if (gps != null && gps.Count > 0)
		//{
		//    //view.CentreMapOnPoint(gps[gps.Count - 1]);
		//}
	}

	/*
			public Image LoadTruckImage()
			{
				var image = new Image
				{
					Source = new BitmapImage(new Uri("pack://application:,,,/ViewModels;component/Resources/Images/Buttons/truck-48.png")),
					Width  = 48,
					Height = 48
				};
	
				return image;
			}
	
			private Image LoadTripPUImage(string tripId)
			{
				var image = new Image
				{
					Source = new BitmapImage(new Uri("pack://application:,,,/ViewModels;component/Resources/Images/TripPinPU.png")),
					Width  = 48,
					Height = 48
				};
				image.ContextMenu = BuildContextMenuForTrip(tripId);
	
				return image;
			}
	
			private Image LoadTripDelImage(string tripId)
			{
				var image = new Image
				{
					Source = new BitmapImage(new Uri("pack://application:,,,/ViewModels;component/Resources/Images/TripPinDel.png")),
					Width  = 48,
					Height = 48
				};
				image.ContextMenu = BuildContextMenuForTrip(tripId);
	
				return image;
			}
	*/
	public bool ShowDriversRouteOnLive
	{
		get { return Get( () => ShowDriversRouteOnLive, true ); }
		set { Set( () => ShowDriversRouteOnLive, value ); }
	}

	[DependsUpon( nameof( ShowDriversRouteOnLive ) )]
	public void WhenShowDriversRouteOnLiveChanges()
	{
		Logging.WriteLogLine( "DEBUG ShowDriversRouteOnLive: " + ShowDriversRouteOnLive );

		if( SelectedDrivers.Count > 0 )
		{
			//foreach (var sd in SelectedDrivers)
			//{
			//    if (DictLiveDrivers.ContainsKey(sd.StaffId))
			//    {
			//        var dd = DictLiveDrivers[sd.StaffId];
			//        if (dd != null && ShowDriversRouteOnLive)
			//        {
			//            if (dd != null)
			//            {
			//                // DriversMap.Children.Add(DictLiveDrivers[sd.StaffId].Line);
			//                UpdateLiveMap_Tick(null, null);
			//            }
			//        }
			//        else
			//        {
			//            if (DriversMap != null)
			//            {
			//                // Just remove from map, but leave for timer to update
			//                if (DriversMap.Children.Contains(DictLiveDrivers[sd.StaffId].Line))
			//                {
			//                    DriversMap.Children.Remove(DictLiveDrivers[sd.StaffId].Line);
			//                }
			//            }
			//        }
			//    }
			//}

			//UpdateLiveMap_Tick( null, null);
			//Dispatcher.Invoke( MapTripsLive );
			Dispatcher.Invoke(() => UpdateLiveMap_Tick( null, null ));
		}
	}

	public bool ShowDriversInfo
	{
		get { return Get(() => ShowDriversInfo, false); }
		set { Set(() => ShowDriversInfo, value ); }
	}

	[DependsUpon(nameof(ShowDriversInfo))]
	public void WhenShowDriversInfoChanges()
	{
		//Dispatcher.Invoke( MapTripsLive );
		//WhenShowDriverOnLiveChanges();
		Dispatcher.Invoke(() => UpdateLiveMap_Tick( null, null ));
	}

	public bool ShowDriverOnLive
	{
		get { return Get( () => ShowDriverOnLive, true ); }
		set { Set( () => ShowDriverOnLive, value ); }
	}

	[DependsUpon( nameof( ShowDriverOnLive ) )]
	public void WhenShowDriverOnLiveChanges()
	{
		if( !ShowDriverOnLive )
		{
			// Can't be on if false
			//ShowDriversRouteOnLive = false;
			if( View is { } V )
			{
				Dispatcher.Invoke( () =>
				                   {
					                   MapHelper.RemoveAllDrivers( V.webView );
									   UpdateLiveMap_Tick( null, null );
				                   } );
			}
		}
		else
		{
			Dispatcher.Invoke( () =>
			                   {
				                   UpdateLiveMap_Tick( null, null );
			                   } );
		}
	}


	public bool ShowTripsOnLive
	{
		//get { return Get(() => ShowTripsOnLive, true); }
		get { return Get( () => ShowTripsOnLive, true ); }
		set { Set( () => ShowTripsOnLive, value ); }
	}

	[DependsUpon( nameof( ShowTripsOnLive ) )]
	public void WhenShowTripsOnLiveChanges()
	{
		//if( ShowTripsOnLive )
		//{
		//	Dispatcher.Invoke( () =>
		//					   {
		//						   MapTripsLive();
		//					   } );
		//}
		Dispatcher.Invoke( MapTripsLive );
	}

	public bool ShowShipmentTotals
    {
		get { return Get( () => ShowShipmentTotals, false ); }
		set { Set ( () => ShowShipmentTotals, value ); }
    }

	[DependsUpon(nameof(ShowShipmentTotals))]
	public void WhenShowShipmentTotalsChanges()
    {
		MapTripsLive();
    }

	// ReSharper disable once InconsistentNaming
	public bool ShowPUTripsOnLive
	{
		get { return Get( () => ShowPUTripsOnLive, true ); }
		set { Set( () => ShowPUTripsOnLive, value ); }
	}

	[DependsUpon( nameof( ShowPUTripsOnLive ) )]
	public void WhenShowPUTripsOnLiveChanges()
	{
		Dispatcher.Invoke( () =>
		                   {
			                   if( !ShowPUTripsOnLive && !ShowDelTripsOnLive )
				                   ShowTripsOnLive = false;
			                   //else
			                   //{
			                   // MapTripsLive();
			                   //}
			                   MapTripsLive();
		                   } );
	}

	public bool ShowDelTripsOnLive
	{
		get { return Get( () => ShowDelTripsOnLive, true ); }
		set { Set( () => ShowDelTripsOnLive, value ); }
	}

	[DependsUpon( nameof( ShowDelTripsOnLive ) )]
	public void WhenShowDelTripsOnLiveChanges()
	{
		Dispatcher.Invoke( () =>
		                   {
			                   if( !ShowPUTripsOnLive && !ShowDelTripsOnLive )
				                   ShowTripsOnLive = false;
			                   //else
			                   //{
			                   // MapTripsLive();
			                   //}
			                   MapTripsLive();
		                   } );
	}


	public bool ShowDriversWithTripsOnLive
	{
		get { return Get( () => ShowDriversWithTripsOnLive, true ); }
		set { Set( () => ShowDriversWithTripsOnLive, value ); }
	}

	[DependsUpon( nameof( ShowDriversWithTripsOnLive ) )]
	public void WhenShowDriversWithTripsOnLiveChanges()
	{
		// LoadDrivers();
		if( ShowDriversWithTripsOnLive )
			Drivers = new ObservableCollection<Driver>( DriversWithTrips );
		else
			Drivers = new ObservableCollection<Driver>( AllDrivers );
	}

	public bool ShowDriverOnHistory
	{
		get { return Get( () => ShowDriverOnHistory, true ); }
		set { Set( () => ShowDriverOnHistory, value ); }
	}

	[DependsUpon( nameof( ShowDriverOnHistory ) )]
	public void WhenShowDriverOnHistoryChanges()
	{
		if( ShowDriverOnHistory )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   foreach( var Driver in SelectedDrivers )
				                   {
					                   var Id = Driver.StaffId;

					                   if( DictHistoryDrivers.TryGetValue( Id, out var DriverOnMap ) )
						                   MapDriver( Id, DriverOnMap.GetGpsPoints() );
				                   }
			                   } );
		}
		else if( View is { } V )
			MapHelper.RemoveAllDrivers( V.webView );
	}

	public bool ShowDriversRouteOnHistory
	{
		get { return Get( () => ShowDriversRouteOnHistory, true ); }
		set { Set( () => ShowDriversRouteOnHistory, value ); }
	}

	[DependsUpon( nameof( ShowDriversRouteOnHistory ) )]
	public void WhenShowDriversRouteOnHistoryChanges()
	{
		// TODO
	}


	public bool ShowTripsOnHistory
	{
		get { return Get( () => ShowTripsOnHistory, true ); }
		set { Set( () => ShowTripsOnHistory, value ); }
	}

	[DependsUpon( nameof( ShowTripsOnHistory ) )]
	public void WhenShowTripsOnHistoryChanges()
	{
		if( ShowTripsOnHistory )
			Dispatcher.Invoke( MapTripsHistory );
		else if( View is { } V )
			MapHelper.RemoveAllTrips( V.webView );
	}


	/// <summary>
	///     TripId, Location
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public Dictionary<string, GpsPoint> TripPULocationsHistory
	{
		get { return Get( () => TripPULocationsHistory, new Dictionary<string, GpsPoint>() ); }
		set { Set( () => TripPULocationsHistory, value ); }
	}

	public Dictionary<string, GpsPoint> TripDelLocationsHistory
	{
		get { return Get( () => TripDelLocationsHistory, new Dictionary<string, GpsPoint>() ); }
		set { Set( () => TripDelLocationsHistory, value ); }
	}

	// ReSharper disable once InconsistentNaming
	public Dictionary<string, GpsPoint> TripPULocationsLive
	{
		get { return Get( () => TripPULocationsLive, new Dictionary<string, GpsPoint>() ); }
		set { Set( () => TripPULocationsLive, value ); }
	}

	public Dictionary<string, GpsPoint> TripDelLocationsLive
	{
		get { return Get( () => TripDelLocationsLive, new Dictionary<string, GpsPoint>() ); }
		set { Set( () => TripDelLocationsLive, value ); }
	}

	public MapLayer TripsHistoryLayer
	{
		get { return Get( () => TripsHistoryLayer, new MapLayer() ); }
		set { Set( () => TripsHistoryLayer, value ); }
	}

	public MapLayer TripsLiveLayer
	{
		get { return Get( () => TripsLiveLayer, new MapLayer() ); }
		set { Set( () => TripsLiveLayer, value ); }
	}


	/// <summary>
	///     Primary
	/// </summary>
	//public void DecideWhatToShow()
	//{
	//	switch( ActiveMapView )
	//	{
	//	case MAP_VIEW_LIVE:
	//		DecideWhatToShowOnLive();
	//		break;

	//	case MAP_VIEW_HISTORY:
	//		DecideWhatToShowOnHistory();
	//		break;
	//	}
	//}

	public void DecideWhatToShowOnLive()
	{
		if( ShowDriverOnLive && View is { } V)
		{
			var DriversList = ( from D in Drivers
			                    where D.HasTrips
			                    select D ).ToList();

			Dispatcher.Invoke( () =>
			{
				MapHelper.RemoveEverything(V.webView);
				foreach( var Driver in DriversList )
				{
					var Id = Driver.StaffId;

					if( !DictLiveDrivers.ContainsKey( Id ) )
					{
						var Points = GetGpsForDriver( Id, true );

						var Dom = new DriverOnMap( this )
						            {
							            StaffId = Id
						            };

						Dom.SetGpsPoints( Points );
						DictLiveDrivers.Add( Id, Dom );
					}
					else
					{
						var Points = GetGpsForDriver( Id, false );

						var Sorted = ( from Gp in Points
						                orderby Gp.LocalDateTime
						                select Gp ).ToList();

						Points.Clear();
						Points.AddRange( Sorted );
						DictLiveDrivers[ Id ].AppendGpsPoints( Points );
					}
					List<string> Gps = new();

					foreach( var Gp in DictLiveDrivers[ Id ].GetGpsPoints() )
					{
						var Line = Gp.Latitude + "," + Gp.Longitude;
						Gps.Add( Line );
					}

					var Content = ShowDriversInfo ? GetContentForDriver( Id ) : string.Empty;

					if( ShowDriversRouteOnLive )
					{
						//view.MapDriverWithLine( driver.StaffId, gps );
						//MapHelper.MapDriverWithLine( driver.StaffId, gps, view);
						MapHelper.MapDriverWithLine( Id, Gps, V.webView, Content, ShowDriversInfo );
					}
					else
					{
						//view.MapDriver( driver.StaffId, DictLiveDrivers[ driver.StaffId ].Last );
						//MapHelper.MapDriver( driver.StaffId, DictLiveDrivers[ driver.StaffId ].Last, view);
						if (ShowDriversInfo)
						{
							MapHelper.MapDriver( Id, DictLiveDrivers[ Id ].Last, V.webView, Content, ShowDriversInfo );
						}
					}
				}
			} );
		}

		if( ShowTripsOnLive )
			MapTripsLive();
	}

	public void DecideWhatToShowOnHistory()
	{
		// TODO 
	}

	private GpsPoints GetGpsForDriver( string staffId, bool allDay )
	{
		GpsPoints Gps = new();

		Task.WaitAll( Task.Run( async () =>
		                        {
			                        if( !IsInDesignMode )
			                        {
				                        GpsDriverDateRange Gddr;

				                        if( allDay )
				                        {
					                        // Hasn't been loaded yet - get all day
					                        Gddr = new GpsDriverDateRange
					                               {
						                               Driver       = staffId,
						                               FromDateTime = DateTimeOffset.Now.Date,
						                               ToDateTime   = DateTimeOffset.Now.Date.AddDays( 1 )
					                               };
				                        }
				                        else
				                        {
					                        Gddr = new GpsDriverDateRange
					                               {
						                               Driver       = staffId,
						                               FromDateTime = DateTimeOffset.Now.AddMinutes( -MINUTES_COUNT ),
						                               ToDateTime   = DateTimeOffset.Now
					                               };
				                        }
				                        var Points = await Azure.Client.RequestGetGpsPoints( Gddr );

				                        if( Points is {Count: > 0} )
					                        Gps.AddRange( Points );

				                        else if( MostFrequentTownGps is { } Most )
					                        Gps.Add( Most );
			                        }
		                        } )
		            );

		return Gps;
	}
#endregion

#region Location methods
	public (string latitude, string longitude) GeolocateAddress( string? addressLine1, string? city, string? region, string? country, string? postalCode )
	{
		var AddressQuery = BuildAddressQuery( country, region, city, postalCode, addressLine1 );

		//Logging.WriteLogLine("DEBUG addressQuery: " + addressQuery);
		var GeocodeRequest  = "http://dev.virtualearth.net/REST/v1/Locations?" + AddressQuery + "&o=xml&key=" + BingMapApiKey;
		var GeocodeResponse = GetXmlResponse( GeocodeRequest );

		return GetPointD( GeocodeResponse );

		//Logging.WriteLogLine("DEBUG latitude: " + latitude + ", longitude: " + longitude);
	}

	public GpsPoints? GetDriversGps( string staffId, bool mapDriver = true )
	{
		GpsPoints? Gps = null;

		//Logging.WriteLogLine("DEBUG Getting gps for " + staffId);

		var Driver = ( from D in Drivers
		               where D.StaffId == staffId
		               select D ).FirstOrDefault();

		// Check if it should be mapped
		if( Driver?.HasTrips == true )
		{
			var Gddr = ActiveMapView switch
			           {
				           MAP_VIEW_LIVE    => new GpsDriverDateRange {Driver = staffId, FromDateTime = DateTimeOffset.Now.Date, ToDateTime = DateTimeOffset.Now.Date.AddDays( 1 )},
				           MAP_VIEW_HISTORY => new GpsDriverDateRange {Driver = staffId, FromDateTime = FromDateTime, ToDateTime            = ToDateTime},
				           _                => null
			           };

			if( Gddr is { } G )
			{
				Task.Run( async () =>
				          {
							  if (!IsInDesignMode)
							  {
								  Gps = await Azure.Client.RequestGetGpsPoints(G);

								  if (Gps != null && Gps.Count > 0)
								  {
									  string times = string.Empty;
									  foreach (var gp in Gps)
									  {
										  times += gp.LocalDateTime + ",";
									  }
									  Logging.WriteLogLine("DEBUG Driver " + staffId + " times:\n" + times);
								  }
						          //if( gps != null )
						          //{
						          // Dispatcher.Invoke( () =>
						          //                    {
						          //                     MapDriver( staffId, gps );
						          //                    } );
						          //}

						          if( Gps is null || ( Gps.Count == 0 ) )
						          {
							          if( MostFrequentTownGps is { } Most )
							          {
								          Gps = new GpsPoints
								                {
									                Most
								                };
							          }
						          }

						          if( Gps is not null && mapDriver)
						          {
							          Dispatcher.Invoke( () =>
							                             {
								                             MapDriver( staffId, Gps );
							                             } );
						          }
					          }
				          } );
			}
		}

		return Gps;
	}

	private void MapDriver( string staffId, GpsPoints gps )
	{
		Logging.WriteLogLine( "DEBUG Mapping " + gps.Count + " points for " + staffId + " for timespan: " + FromDateTime + " to " + ToDateTime );

		// TODO Exception in History tab
		//var Driver = DictLiveDrivers[ staffId ];

		switch( ActiveMapView )
		{
			case MAP_VIEW_LIVE:
				if (!DictLiveDrivers.TryGetValue(staffId, out var Driver))
				{
					DictLiveDrivers.Add(staffId, Driver = new DriverOnMap(staffId));
				}
				Driver.SetGpsPoints( gps );

				Centre = $"{Driver.Latitude},{Driver.Longitude}";

				//Logging.WriteLogLine("DEBUG Setting centre to last location: " + Centre);
				if( Driver.Last is { } PointL )
				{
					Logging.WriteLogLine("DEBUG timestamp: " + PointL.LocalDateTime);
					if (ShowDriverOnLive)
					{
						Dispatcher.Invoke(() =>
						{
							PutDriversLabelOnMap(staffId, PointL);
							if (View is { } V)
							{
								MapHelper.CentreMapOnPoint(PointL, V.webView);
							}
						});
					}
				}

				break;

			case MAP_VIEW_HISTORY:
				if( !DictHistoryDrivers.TryGetValue( staffId, out var DonM ) )
					DictHistoryDrivers.Add( staffId, DonM = new DriverOnMap( staffId ) );
				DonM.SetGpsPoints( gps );

				Centre = $"{DonM.Latitude},{DonM.Longitude}";

				//Logging.WriteLogLine("DEBUG Setting centre to last location: " + Centre);

				//GetTripsForDriversHistory();
				if (DonM.Last is { } PointH)
				{
					Dispatcher.Invoke(() =>
					{
						PutDriversLabelOnMap(staffId, PointH);
						if (ShowTripsOnHistory)
						{
							MapTripsHistory();
						}
					});
				}

				break;
		}

		//if( Driver.Last is { } Point )
		//{
		//	Dispatcher.Invoke( () =>
		//	                   {
		//		                   switch( ActiveMapView )
		//		                   {
		//		                   case MAP_VIEW_LIVE:
		//			                   if( ShowDriverOnLive )
		//				                   PutDriversLabelOnMap( staffId, Point );
		//			                   break;

		//		                   case MAP_VIEW_HISTORY:
		//			                   PutDriversLabelOnMap( staffId, Point );
		//			                   break;
		//		                   }
		//	                   } );
		//}
	}


	public void MapTripsHistory()
	{
		Logging.WriteLogLine("DEBUG Mapping history trips for " + DictHistoryDrivers.Count + " drivers");
		if( HasSearchRun && TripsByDriverHistory.Count > 0 && View is { } V)
		{
			Dispatcher.Invoke(() =>
			{
				//RemoveHistoryTripsFromMap();
				MapHelper.RemoveAllTrips(V.webView);

				if (TripsByDriverHistory.Count == 0)
				{
					GetTripsForDriversHistory();
				}

				// Geolocate the addresses
				//foreach( var Driver in TripsByDriverHistory.Keys )
				foreach (var Driver in SelectedDrivers)
				{
					foreach( var Trip in TripsByDriverHistory[ Driver.StaffId ] )
					{
						Logging.WriteLogLine("DEBUG Trip: " + Trip.TripId + " status: " + Trip.Status1 + " driver: " + Trip.Driver + " pickupTime: " + Trip.PickupTime + " deliveryTime: " + Trip.DeliveryTime);

						var Id = Trip.TripId;

						GpsPoint Gp;

						if( !TripPULocationsHistory.ContainsKey( Id ) )
						{
							// Not located - do it
							Gp = GetPickupLocation( Trip );

							//Logging.WriteLogLine("DEBUG Adding trip to TripPULocationsHistory: " + Trip.TripId);
							TripPULocationsHistory.Add( Id, Gp );
						}
						else
						{
							Gp = TripPULocationsHistory[Trip.TripId];
						}
						//string puDescription = Trip.PickupCompanyName + "\n" + Trip.PickupTime.ToString();
						string puDescription = Trip.PickupCompanyName + " P:" + FormatDateTimeOffset(Trip.PickupTime);
						MapHelper.MapPUTrip(Id, puDescription, Gp, V.webView);

						if ( !TripDelLocationsHistory.ContainsKey( Id ) )
						{
							// Not located - do it
							Gp = GetDeliveryLocation( Trip );
							//Logging.WriteLogLine("DEBUG Adding trip to TripDelLocationsHistory: " + Trip.TripId);
							TripDelLocationsHistory.Add( Id, Gp );
						}
						else
						{
							Gp = TripDelLocationsHistory[Trip.TripId];
						}
						string delDescription = Trip.DeliveryCompanyName + " D:" + FormatDateTimeOffset(Trip.DeliveryTime);
						MapHelper.MapDelTrip(Id, delDescription, Gp, V.webView);
					}
				}
			});
		}
	}

	private string FormatDateTimeOffset(DateTimeOffset? date)
	{
		string formatted = string.Empty;
		if (date != null )
		{
			formatted = date.Value.Year + "-" + date.Value.Month + "-" + date.Value.Day + " " + date.Value.Hour + ":" + date.Value.Minute; // + ":" + date.Value.Second;
		}

		return formatted;
	}

	private void RemoveTripsFromMap( Dictionary<string, GpsPoint>.KeyCollection tripIds )
	{
		if( View is { } V )
		{
			foreach( var TripId in tripIds )
			{
				foreach( var Driver in TripsByDriverLive.Keys )
				{
					var Trip = ( from T in TripsByDriverLive[ Driver ]
					             where T.TripId == TripId
					             select T ).FirstOrDefault();

					if( Trip is { } Tr )
					{
						var Title = Tr.PickupCompanyName;

						if( Title.IsNotNullOrWhiteSpace() )
						{
							var Caption = Title;

							Dispatcher.Invoke( () =>
							                   {
								                   //view.RemoveTrip( tripId, title );
								                   //MapHelper.RemoveTrip( tripId, title, view);
								                   MapHelper.RemoveTrip( TripId, Caption, V.webView );
							                   } );
						}
					}
				}
			}
		}
	}

	private void RemoveHistoryTripsFromMap()
	{
		if( TripPULocationsHistory is {Keys: { } PUKeys} )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   //Logging.WriteLogLine("DEBUG Removing History PU trips from Map");
				                   RemoveTripsFromMap( PUKeys );
			                   } );
		}
		if (TripDelLocationsHistory is { Keys: { } DelKeys })
		{
			Dispatcher.Invoke(() =>
							   {
								   //Logging.WriteLogLine("DEBUG Removing History Del trips from Map");
								   RemoveTripsFromMap(DelKeys);
							   });
		}
	}

	private GpsPoint GetPickupLocation( TripUpdate trip )
	{
		string TripPickupAddressLatitude;
		string TripPickupAddressLongitude;

		// First, check to see if the address has gps
		if( trip.PickupAddressLatitude.IsNotNull() && ( trip.PickupAddressLatitude != 0 )
		                                           && trip.PickupAddressLongitude.IsNotNull() && ( trip.PickupAddressLongitude != 0 ) )
		{
			TripPickupAddressLatitude  = $"{trip.PickupAddressLatitude}";
			TripPickupAddressLongitude = $"{trip.PickupAddressLongitude}";
		}
		else
		{
			( TripPickupAddressLatitude, TripPickupAddressLongitude ) = GeolocateAddress( trip.PickupAddressAddressLine1, trip.PickupAddressCity,
			                                                                              trip.PickupAddressRegion, trip.PickupAddressCountry, trip.PickupAddressPostalCode );
		}

		var Gp = new GpsPoint
		         {
			         Latitude  = double.Parse( TripPickupAddressLatitude ),
			         Longitude = double.Parse( TripPickupAddressLongitude )
		         };

		return Gp;
	}

	private GpsPoint GetDeliveryLocation( Trip trip )
	{
		string TripDeliveryAddressLatitude;
		string TripDeliveryAddressLongitude;

		// First, check to see if the address has gps
		if( trip.DeliveryAddressLatitude.IsNotNull() && ( trip.DeliveryAddressLatitude != 0 )
		                                             && trip.DeliveryAddressLongitude.IsNotNull() && ( trip.DeliveryAddressLongitude != 0 ) )
		{
			TripDeliveryAddressLatitude  = $"{trip.DeliveryAddressLatitude}";
			TripDeliveryAddressLongitude = $"{trip.DeliveryAddressLongitude}";
		}
		else
		{
			( TripDeliveryAddressLatitude, TripDeliveryAddressLongitude ) = GeolocateAddress( trip.DeliveryAddressAddressLine1, trip.DeliveryAddressCity,
			                                                                                  trip.DeliveryAddressRegion, trip.DeliveryAddressCountry, trip.DeliveryAddressPostalCode );
		}

		var Gp = new GpsPoint
		         {
			         Latitude  = double.Parse( TripDeliveryAddressLatitude ),
			         Longitude = double.Parse( TripDeliveryAddressLongitude )
		         };

		return Gp;
	}

	/*
			/// <summary>
			/// TODO Add clickable link to load the trip into the Shipment Entry tab.
			/// </summary>
			/// <param name="trip"></param>
			/// <returns></returns>
			private TextBlock BuildTripTextBlock(Trip trip)
			{                        
				string content = trip.TripId + "\n" + FindStringResource("DriversMapLabelTotalWeightPrefix") + trip.Weight + " ";
				content += "\n" + FindStringResource("DriversMapLabelTotalPiecesPrefix") + trip.Pieces;
	
				TextBlock tb = new TextBlock
				{
					Text          = content,
					Background    = Brushes.White,
					Opacity       = 0.9,
					TextWrapping  = TextWrapping.Wrap,
					TextAlignment = TextAlignment.Left,
					MinWidth      = 60,
					ToolTip       = BuildTooltipForTrip(trip), // TODO Gord - TripId, Pickup and Delivery addresses, contents of shipment
					Name          = trip.TripId,
					
				};
							
				tb.ContextMenu = BuildContextMenuForTrip(trip.TripId);
	
				return tb;
			}
	
			private ContextMenu BuildContextMenuForTrip(string tripId)
			{
				ContextMenu cm = new ContextMenu();
				MenuItem mi = new MenuItem
				{
					Name   = tripId,
					Header = FindStringResource("DriversMapTripContextMenuOpenssTrip")
				};
				mi.Click += ClickHandler;
				cm.Items.Add(mi);
	
				return cm;
			}
	
			private void ClickHandler(object sender, RoutedEventArgs e)
			{
				if (sender is MenuItem mi)
				{
					//Logging.WriteLogLine("DEBUG Mouse click: " + mi.Name);
	
					Trip baseTrip = null;
					switch (ActiveMapView)
					{
						case MAP_VIEW_LIVE:
							foreach (var driver in TripsByDriverLive.Keys)
							{
								foreach (var trip in TripsByDriverLive[driver])
								{
									if (trip.TripId == mi.Name)
									{
										baseTrip = trip;
										break;
									}
								}
							}
	
							break;
						case MAP_VIEW_HISTORY:
							foreach (var driver in TripsByDriverHistory.Keys)
							{
								foreach (var trip in TripsByDriverHistory[driver])
								{
									if (trip.TripId == mi.Name)
									{
										baseTrip = trip;
										break;
									}
								}
							}
							break;
					}
	
					if (baseTrip != null)
					{
						Trips.Boards.Common.ServiceLevel sl = (from s in ServiceLevels
															   where s.RawServiceLevel.OldName == baseTrip.ServiceLevel
															   select s).FirstOrDefault();
	
						if (sl != null)
						{
							DisplayTrip selectedTrip = new DisplayTrip(baseTrip, sl);
	
							var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
							if (tis != null && tis.Count > 0)
							{
								PageTabItem pti = null;
								for (int i = 0; i < tis.Count; i++)
								{
									var ti = tis[i];
									if (ti.Content is TripEntry)
									{
										pti = ti;
										// Load the trip into the first TripEntry tab
										Logging.WriteLogLine("Displaying trip in extant " + Globals.TRIP_ENTRY + " tab");
	
										((TripEntryModel)pti.Content.DataContext).CurrentTrip = selectedTrip;
										((TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
										Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;
	
										break;
									}
								}
	
								if (pti == null)
								{
									// Need to create a new Trip Entry tab
									Logging.WriteLogLine("Opening new " + Globals.TRIP_ENTRY + " tab");
									Globals.RunProgram(Globals.TRIP_ENTRY, selectedTrip);
									Thread.Sleep(1000); // Give it time to load
	
									tis = Globals.DataContext.MainDataContext.ProgramTabItems;
									for (int i = 0; i < tis.Count; i++)
									{
										var ti = tis[i];
										if (ti.Content is TripEntry)
										{
											pti = ti;
											// Load the trip into the first TripEntry tab
											Logging.WriteLogLine("Displaying trip in extant " + Globals.TRIP_ENTRY + " tab");
	
											((TripEntryModel)pti.Content.DataContext).CurrentTrip = selectedTrip;
											((TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
											Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;
	
											break;
										}
									}
								}
							}
						}
					}
					else
					{
						Logging.WriteLogLine("DEBUG Can't find trip for " + mi.Name);
					}
				}
			}
	
			
			/// <summary>
			/// 
			/// </summary>
			/// <param name="trip"></param>
			private string BuildTooltipForTrip(Trip trip)
			{
				var tt = string.Empty;
	
	
				return tt;
			}
	*/


	//private const int DICT_TRIP_LAYERS_PU = 0;
	//private const int DICT_TRIP_LAYERS_DEL = 1;

	private void MapTripsLiveForDriver( string staffId, bool onlyGetLocations )
	{
		if( TripsByDriverLive.Count == 0 )
			GetTripsForDriversLive();

		if( TripsByDriverLive.Count > 0 )
		{
			var Trips = TripsByDriverLive[ staffId ];

			if( Trips is not null && View is { } V )
			{
				List<string>   PuTripIds       = new();
				List<string>   PuTitles        = new();
				List<string>   PuDescriptions  = new();
				List<GpsPoint> PuPoints        = new();
				List<string>   DelTripIds      = new();
				List<string>   DelTitles       = new();
				List<string>   DelDescriptions = new();
				List<GpsPoint> DelPoints       = new();

				//               Dispatcher.Invoke(() =>
				//{
				foreach( var Trip in Trips )
				{
					GpsPoint Gp;
					var      Id = Trip.TripId;

					if( ShowPUTripsOnLive )
					{
						PuTripIds.Add( Id );
						PuTitles.Add( Trip.PickupCompanyName );

						if( !TripPULocationsLive.ContainsKey( Id ) )
						{
							// Not located - do it
							Gp = GetPickupLocation( Trip );

							//Logging.WriteLogLine("DEBUG Adding trip to TripPULocationsLive: " + trip.TripId);
							if( !TripPULocationsLive.ContainsKey( Id ) )
								TripPULocationsLive.Add( Id, Gp );
						}
						else
							Gp = TripPULocationsLive[ Id ];

						PuPoints.Add( Gp );

						MapHelper.MapPUTrip(Trip.TripId, Trip.PickupCompanyName, Gp, V.webView);
						if (ShowShipmentTotals)
						{
							var Content = FindStringResource( "DriversMapLabelTotalWeightPrefix" ) + Trip.Weight + "<br/>"
										  + FindStringResource( "DriversMapLabelTotalPiecesPrefix" ) + Trip.Pieces + "<br/>"
										  + FindStringResource( "DriversMapLabelServiceLevelPrefix" ) + Trip.ServiceLevel + "<br/>";

							MapHelper.MapPUTripInfobox(Trip.TripId, Content, Gp, V.webView);
						
							PuDescriptions.Add( Content );
						}
					}

					if( ShowDelTripsOnLive )
					{
						DelTripIds.Add( Id );
						DelTitles.Add( Trip.DeliveryCompanyName );

						if( !TripDelLocationsLive.ContainsKey( Id ) )
						{
							// Not located - do it
							Gp = GetDeliveryLocation( Trip );

							//Logging.WriteLogLine("DEBUG Adding trip to TripDelLocationsLive: " + trip.TripId);
							if( !TripDelLocationsLive.ContainsKey( Id ) )
								TripDelLocationsLive.Add( Id, Gp );
						}
						else
							Gp = TripDelLocationsLive[ Id ];
						DelPoints.Add( Gp );

						MapHelper.MapDelTrip(Trip.TripId, Trip.DeliveryCompanyName, Gp, V.webView);
						if (ShowShipmentTotals)
						{
							var Content = FindStringResource( "DriversMapLabelTotalWeightPrefix" ) + Trip.Weight + "<br/>"
										  + FindStringResource( "DriversMapLabelTotalPiecesPrefix" ) + Trip.Pieces + "<br/>"
										  + FindStringResource( "DriversMapLabelServiceLevelPrefix" ) + Trip.ServiceLevel + "<br/>";

							MapHelper.MapDelTripInfobox(Trip.TripId, Content, Gp, V.webView);
							DelDescriptions.Add( Content );
						}

					}
				}

				//});
				//if( !onlyGetLocations && View is { } V )
				//{
				//	//view.MapTripsDel( delTripIds, delTitles, delDescriptions, delPoints );
				//	//MapHelper.MapTripsDel( delTripIds, delTitles, delDescriptions, delPoints, view);
				//	MapHelper.MapTripsDel( DelTripIds, DelTitles, DelDescriptions, DelPoints, V.webView, ShowShipmentTotals );
				//	//view.MapTripsPU( puTripIds, puTitles, puDescriptions, puPoints );
				//	//MapHelper.MapTripsPU( puTripIds, puTitles, puDescriptions, puPoints, view);
				//	MapHelper.MapTripsPU( PuTripIds, PuTitles, PuDescriptions, PuPoints, V.webView, ShowShipmentTotals );

				//	//for (int i = 0; i < delTripIds.Count; i++)
				//	//{
				//	//    MapHelper.MapDelTrip(delTripIds[i], delTitles[i], delPoints[i], view.webView);
				//	//    MapHelper.MapTripInfobox(delTripIds[i], delDescriptions[i], delPoints[i], view.webView);
				//	//}
				//	//for (int i = 0; i < puTripIds.Count; i++)
				//	//{
				//	//    MapHelper.MapPUTrip(puTripIds[i], puTitles[i], puPoints[i], view.webView);
				//	//    MapHelper.MapTripInfobox(puTripIds[i], puDescriptions[i], puPoints[i], view.webView);
				//	//}
				//}
			}
		}
	}

	private void MapTripsLive()
	{
		//Logging.WriteLogLine("DEBUG Mapping live trips: " + TripsByDriverLive.Count);
		if( TripsByDriverLive.Count == 0 )
			GetTripsForDriversLive();

		if( View is { } V && ( TripsByDriverLive.Count > 0 ) )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   //view.RemoveAllTrips();
				                   //MapHelper.RemoveAllTrips(view);
				                   MapHelper.RemoveAllTrips( V.webView );
				                   RemoveTripsFromMap();

				                   // Geolocate the addresses
				                   foreach( var Driver in TripsByDriverLive.Keys )
				                   {
					                   GpsPoint Gp;

					                   var Selected = ( from D in SelectedDrivers
					                                    where D.StaffId == Driver
					                                    select D ).FirstOrDefault();

					                   if( Selected != null )
					                   {
						                   Logging.WriteLogLine( "DEBUG driver " + Driver + " selected" );

						                   foreach( var Trip in TripsByDriverLive[ Driver ] )
						                   {
							                   if( ShowTripsOnLive && ShowPUTripsOnLive )
							                   {
								                   if( !TripPULocationsLive.ContainsKey( Trip.TripId ) )
								                   {
									                   // Not located - do it
									                   Gp = GetPickupLocation( Trip );

									                   Logging.WriteLogLine("DEBUG Adding trip to TripPULocationsLive: " + Trip.TripId);
									                   TripPULocationsLive.Add( Trip.TripId, Gp );
								                   }
								                   else
									                   Gp = TripPULocationsLive[ Trip.TripId ];

								                   MapHelper.MapPUTrip( Trip.TripId, Trip.PickupCompanyName, Gp, V.webView );

												   if (ShowShipmentTotals)
                                                   {
													   var Content = FindStringResource( "DriversMapLabelTotalWeightPrefix" ) + Trip.Weight + "<br/>"
																	 + FindStringResource( "DriversMapLabelTotalPiecesPrefix" ) + Trip.Pieces + "<br/>"
																	 + FindStringResource( "DriversMapLabelServiceLevelPrefix" ) + Trip.ServiceLevel + "<br/>";
													   MapHelper.MapPUTripInfobox( Trip.TripId, Content, Gp, V.webView );
                                                   }
							                   }

							                   if( ShowTripsOnLive && ShowDelTripsOnLive )
							                   {
								                   if( !TripDelLocationsLive.ContainsKey( Trip.TripId ) )
								                   {
									                   // Not located - do it
									                   Gp = GetDeliveryLocation( Trip );

									                   Logging.WriteLogLine("DEBUG Adding trip to TripDelLocationsLive: " + Trip.TripId);
									                   TripDelLocationsLive.Add( Trip.TripId, Gp );
								                   }
								                   else
									                   Gp = TripDelLocationsLive[ Trip.TripId ];

								                   MapHelper.MapDelTrip( Trip.TripId, Trip.DeliveryCompanyName, Gp, V.webView );

												   if (ShowShipmentTotals)
                                                   {
													   var Content = FindStringResource( "DriversMapLabelTotalWeightPrefix" ) + Trip.Weight + "<br/>"
																	 + FindStringResource( "DriversMapLabelTotalPiecesPrefix" ) + Trip.Pieces + "<br/>"
																	 + FindStringResource( "DriversMapLabelServiceLevelPrefix" ) + Trip.ServiceLevel + "<br/>";
													   MapHelper.MapDelTripInfobox( Trip.TripId, Content, Gp, V.webView );
                                                   }
							                   }
						                   }

						                   //if (gp != null)
						                   //{
						                   //    // Centre map on last trip
						                   //    //Dispatcher.Invoke(() =>
						                   //    //{
						                   //    //    view.CentreMapOnPoint(gp);
						                   //    //});
						                   //}
					                   }
				                   }
			                   } );
		}
	}

	private void RemoveTripsFromMap()
	{
		if( View is { } V )
		{
			Dispatcher.Invoke( () =>
			{
				foreach( var Driver in SelectedDrivers )
				{
					if( Driver.StaffId is { } Id && TripsByDriverLive[ Id ] is { } Tbd )
					{
						var TripIds = ( from T in Tbd
										select T.TripId ).ToList();

						var PuTitles = ( from T in Tbd
										 select T.PickupCompanyName ).ToList();

						var DelTitles = ( from T in Tbd
										  select T.DeliveryCompanyName ).ToList();

						//view.RemoveTripsForDriver( tripIds, puTitles, delTitles );
						//MapHelper.RemoveTripsForDriver( tripIds, puTitles, delTitles, view );
						MapHelper.RemoveTripsForDriver( TripIds, PuTitles, DelTitles, V.webView );
					}
				}
            } );
		}
	}

	//private void RemoveLiveTripsFromMap()
	//{
	//    Dispatcher.Invoke(() =>
	//    {
	//        Logging.WriteLogLine("DEBUG Removing Live PU trips from Map");
	//        RemoveTripsFromMap(TripPULocationsLive.Keys);
	//        Logging.WriteLogLine("DEBUG Removing Live Del trips from Map");
	//        RemoveTripsFromMap(TripDelLocationsLive.Keys);
	//    });
	//}


	//private void RemoveDriversLabelFromMap(string staffId)
	//{
	//    Dispatcher.Invoke(() =>
	//    {
	//        view.RemoveDriver(staffId);
	//    });
	//}

	private void PutDriversLabelOnMap( string staffId, GpsPoint last )
	{
		Logging.WriteLogLine( "Adding Driver to map for " + staffId );

		if( View is { } V )
		{
			var Points  = new List<string>();
			var Content = ShowDriversInfo ? GetContentForDriver( staffId, last ) : string.Empty;

			switch( ActiveMapView )
			{
			case MAP_VIEW_LIVE:
				//ShowDriverOnMap(staffId, last);
				if( ShowDriversRouteOnLive )
				{
					Logging.WriteLogLine( "DEBUG Also showing route" );

					var Driver = DictLiveDrivers[ staffId ];

					if( Driver is { } D )
					{
						foreach( var Gp in D.GetGpsPoints() )
						{
							var Point = Gp.Latitude + "," + Gp.Longitude;
							Points.Add( Point );
						}
					}
					else
					{
						// No points - use last
						Logging.WriteLogLine( "DEBUG No gps points - using last: " + last.Latitude + "," + last.Longitude );
						Points.Add( last.Latitude + "," + last.Longitude );
					}

					//view.MapDriverWithLine( staffId, points );
					//MapHelper.MapDriverWithLine( staffId, points, view);
					MapHelper.MapDriverWithLine( staffId, Points, V.webView, Content, ShowDriversInfo );
				}
				else
				{
					//view.MapDriver( staffId, last );
					//MapHelper.MapDriver( staffId, last, view);
					MapHelper.MapDriver( staffId, last, V.webView, Content, ShowDriversInfo );
				}
				break;

			case MAP_VIEW_HISTORY:
				//GetDriversGps(staffId);
				Content = GetContentForDriver( staffId );

				foreach( var P in DictHistoryDrivers[ staffId ].GetGpsPoints() )
					Points.Add( P.Latitude + "," + P.Longitude );
				MapHelper.MapDriverWithLine( staffId, Points, V.webView, Content, ShowDriversInfo );
				break;
			}
		}
	}

	/*
	private (int tripsCount, decimal totalWeight, decimal totalPieces) GetTotalsForDriverLive( string staffId )
	{
		var     TripsCount  = 0;
		decimal TotalWeight = 0;
		decimal TotalPieces = 0;

		switch( ActiveMapView )
		{
		case MAP_VIEW_LIVE:
			GetTripsForDriversLive();

			if( TripsByDriverLive.Count > 0 )
			{
				if( TripsByDriverLive.ContainsKey( staffId ) )
				{
					foreach( var Trip in TripsByDriverLive[ staffId ] )
					{
						++TripsCount;
						TotalWeight += Trip.Weight;
						TotalPieces += Trip.Pieces;
					}
				}
			}
			break;

		case MAP_VIEW_HISTORY:
			GetTripsForDriversHistory();

			if( TripsByDriverHistory.Count > 0 )
			{
				if( TripsByDriverHistory.ContainsKey( staffId ) )
				{
					foreach( var Trip in TripsByDriverHistory[ staffId ] )
					{
						++TripsCount;
						TotalWeight += Trip.Weight;
						TotalPieces += Trip.Pieces;
					}
				}
			}
			break;
		}

		return ( TripsCount, TotalWeight, TotalPieces );
	}

	*/

	private readonly BackgroundWorker Worker = new();

	private void Worker_DoWorkMapTrips( object sender, DoWorkEventArgs e )
	{
		Logging.WriteLogLine( "DEBUG Starting to get locations for trips" );

		//foreach (var staffId in TripsByDriverLive.Keys)
		foreach( var D in AllDrivers )
		{
			if( D.StaffId is { } Id )
			{
				//MapTripsLiveForDriver(staffId, true);
				MapTripsLiveForDriver( Id, true );

				//Logging.WriteLogLine("DEBUG TripsByDriverLive[" + staffId + "].Count: " + TripsByDriverLive[staffId].Count);
				Logging.WriteLogLine( "DEBUG TripsByDriverLive[" + D.StaffId + "].Count: " + TripsByDriverLive[ Id ].Count );
			}
		}
	}

	private static void Worker_Completed( object sender, RunWorkerCompletedEventArgs e )
	{
		Logging.WriteLogLine( "DEBUG worker completed" );
	}

	private void GetTripsForDriversLive()
	{
		var Dict = new Dictionary<string, List<Trip>>();

		if( !IsInDesignMode )
		{
			// First check for Drivers board.
			Logging.WriteLogLine( "Looking for drivers board" );

			const bool FOUND = false;

			//if (tis.Count > 0)
			//{
			//    for (var i = 0; i < tis.Count; i++)
			//    {
			//        var ti = tis[i];

			//        if (ti.Content is DriversBoard)
			//        {
			//            Logging.WriteLogLine("Found " + Globals.DRIVERS_BOARD + " tab");
			//            found = true;
			//            var tdb = ((DriversBoardViewModel)ti.Content.DataContext).TripsByDrivers;
			//            if (tdb?.Count > 0)
			//            {
			//                foreach (var tmp in tdb)
			//                {
			//                    if (!dict.ContainsKey(tmp.Driver))
			//                    {
			//                        dict.Add(tmp.Driver, new List<Trip>());
			//                    }
			//                    foreach (var trip in tmp.Trips)
			//                    {
			//                        dict[tmp.Driver].Add(trip);
			//                    }
			//                }

			//                TripsByDriverLive = dict;
			//            }

			//            break;
			//        }
			//    }
			//}

			if( !FOUND )
			{
				Logging.WriteLogLine( "Drivers board not found - getting trips from server..." );

				// Get from server
				Task.WaitAll(
				             Task.Run( async () =>
				                       {
					                       var Temp = await Azure.Client.RequestGetTripsByStatus( new StatusRequest {STATUS.DISPATCHED, STATUS.PICKED_UP, STATUS.DELIVERED} );

					                       var List = ( from T in Temp
					                                    orderby T.Driver
					                                    group T by T.Driver
					                                    into G
					                                    select G ).ToList();

					                       foreach( var Group in List )
					                       {
						                       var Key = Group.Key;

						                       if( Key != "" )
						                       {
							                       if( !Dict.TryGetValue( Key, out var Trips ) )
								                       Dict.Add( Key, Trips = new List<Trip>() );

							                       Trips.AddRange( Group );
						                       }
					                       }

					                       TripsByDriverLive = Dict;
				                       } )
				            );

				TripsByDriverLive = Dict;

				// Make sure that the TripsByDriverLive has entries for the drivers with no trips
				foreach( var Driver in AllDrivers )
				{
					if( !TripsByDriverLive.ContainsKey( Driver.StaffId ) )
						TripsByDriverLive.Add( Driver.StaffId, new List<Trip>() );
				}

				//Worker.DoWork += Worker_DoWorkMapTrips;
				//Worker.RunWorkerCompleted += Worker_Completed;
				//Worker.RunWorkerAsync();
			}
		}
	}

	//get { return Get(() => MostFrequentTownGps, GetMostFrequentTownGpsForDrivers()); }
	public GpsPoint? MostFrequentTownGps => Get( () => MostFrequentTownGps, GetMostFrequentTownGpsForDrivers() );

	public GpsPoint GetMostFrequentTownGpsForDrivers()
	{
		// Default to Vancouver
		GpsPoint Gps = new()
		               {
			               Latitude  = 49.317640851387331,
			               Longitude = -123.11948939837492
		               };

		Dictionary<string?, int>    DictTown          = new();
		Dictionary<string?, string> DictTownRegion    = new();
		Dictionary<string?, string> DictRegionCountry = new();

		foreach( var Driver in TripsByDriverLive.Keys )
		{
			var Tbd = TripsByDriverLive[ Driver ];

			if( Tbd.Count > 0 )
			{
				foreach( var Trip in Tbd )
				{
					if( !DictTown.ContainsKey( Trip.PickupAddressCity ) )
						DictTown.Add( Trip.PickupAddressCity, 0 );
					++DictTown[ Trip.PickupAddressCity ];

					if( !DictTownRegion.ContainsKey( Trip.PickupAddressCity ) )
						DictTownRegion.Add( Trip.PickupAddressCity, Trip.PickupAddressRegion );

					if( !DictRegionCountry.ContainsKey( Trip.PickupAddressRegion ) )
						DictRegionCountry.Add( Trip.PickupAddressRegion, Trip.PickupAddressCountry );
				}
			}
		}

		// Now find the most frequent town
		var Largest = 0;
		var Town    = string.Empty;

		foreach( var ValuePair in DictTown )
		{
			var Value = ValuePair.Value;

			if( Value > Largest )
			{
				Largest = Value;
				Town    = ValuePair.Key;
			}
		}

		var Region  = DictTownRegion[ Town ];
		var Country = DictRegionCountry[ Region ];
		var (Lat, Lo) = GeolocateAddress( "", Town, Region, Country, "" );
		Logging.WriteLogLine( "Setting default location to " + Town + ", " + Region + ", " + Country + " : latitude: " + Lat + ", longitude: " + Lo );

		if( Lat.IsNotNullOrWhiteSpace() && Lo.IsNotNullOrWhiteSpace() )
		{
			Gps = new GpsPoint
			      {
				      Latitude  = double.Parse( Lat ),
				      Longitude = double.Parse( Lo )
			      };
		}

		return Gps;
	}

	private void GetTripsForDriversHistory()
	{
		var Dict = new Dictionary<string, List<Trip>>();

		if( !IsInDesignMode )
		{
			Task.WaitAll(
			             Task.Run( async () =>
			                       {
									   Logging.WriteLogLine("DEBUG Fetching trips for driver history");
				                       var Temp = await Azure.Client.RequestSearchTrips( new SearchTrips
				                                                                         {
					                                                                         FromDate = FromDateTime,
					                                                                         ToDate   = ToDateTime,
																							 StartStatus = STATUS.DISPATCHED,
																							 EndStatus = STATUS.FINALISED
				                                                                         } );

				                       var List = ( from T in Temp
				                                    orderby T.Driver
				                                    group T by T.Driver
				                                    into G
				                                    select G ).ToList();
									   Logging.WriteLogLine("DEBUG Found trips: " + List.Count);

				                       if( List.Count > 0 )
				                       {
					                       foreach( var Group in List )
					                       {
						                       var Key = Group.Key;

						                       if( !Dict.TryGetValue( Key, out var Trips ) )
							                       Dict.Add( Key, Trips = new List<Trip>() );

						                       Trips.AddRange( Group );
					                       }
				                       }

				                       TripsByDriverHistory = Dict;

									   Logging.WriteLogLine("DEBUG Done");
			                       } )
			            );
		}
	}

	private static string BuildAddressQuery( string? country, string? region, string? city, string? postalCode, string? addressLine ) =>
		// "countryRegion=AU&adminDistrict=Vic&locality=Kyabram";
		"countryRegion=@1&adminDistrict=@2&locality=@3&postalCode=@4&addressLine=@5"
			.Replace( "@1", country.IsNotNullOrWhiteSpace() ? country : "" )
			.Replace( "@2", region.IsNotNullOrWhiteSpace() ? region : "" )
			.Replace( "@3", city.IsNotNullOrWhiteSpace() ? city : "" )
			.Replace( "@4", postalCode.IsNotNullOrWhiteSpace() ? postalCode : "" )
			.Replace( "@5", addressLine.IsNotNullOrWhiteSpace() ? addressLine : "" );

	/// <summary>
	///     Submit a REST Services or Spatial Data Services request and return the response
	/// </summary>
	/// <param
	///     name="requestUrl">
	/// </param>
	/// <returns></returns>
	private static XmlDocument GetXmlResponse( string requestUrl )
	{
		//System.Diagnostics.Trace.WriteLine("Request URL (XML): " + requestUrl);
		var XmlDoc = new XmlDocument();

		if( WebRequest.Create( requestUrl ) is HttpWebRequest Request )
		{
			try
			{
				if( Request.GetResponse() is HttpWebResponse Response )
				{
					using( Response )
					{
						if (Response.StatusCode != HttpStatusCode.OK)
							//throw new Exception( $"Server error (HTTP {Response.StatusCode}: {Response.StatusDescription})." );
							Logging.WriteLogLine( $"Server error (HTTP {Response.StatusCode}: {Response.StatusDescription})." );

						if( Response.GetResponseStream() is { } S )
							XmlDoc.Load( S );
					}
				}
			}
			catch (Exception e)
			{
				Logging.WriteLogLine("Exception: " + e.ToString());
			}
		}
		return XmlDoc;
	}

	/// <summary>
	///     Grabs the values directly from the document.
	/// </summary>
	/// <param
	///     name="xmlDoc">
	/// </param>
	/// <returns></returns>
	private static (string latitude, string longitude) GetPointD( XmlDocument xmlDoc )
	{
		var Latitude  = "0.0";
		var Longitude = "0.0";

		//Create namespace manager
		var Nsmgr = new XmlNamespaceManager( xmlDoc.NameTable );
		Nsmgr.AddNamespace( "rest", "http://schemas.microsoft.com/search/local/ws/rest/v1" );

		//Get all locations in the response and then extract the coordinates for the top location
		var LocationElements = xmlDoc.SelectNodes( "//rest:Point", Nsmgr );

		if( LocationElements is {Count: > 0} Le && Le[ 0 ] is { } Le0 )
		{
			Latitude  = Le0.SelectSingleNode( ".//rest:Latitude", Nsmgr )?.InnerText ?? "0";
			Longitude = Le0.SelectSingleNode( ".//rest:Longitude", Nsmgr )?.InnerText ?? "0";
		}
		else
			Logging.WriteLogLine( "Error: no location found" );

		return ( Latitude, Longitude );
	}

	/*
			private (float latitude, float longitude) GetPoint(XmlNodeList childNodes)
			{
				float latitude = 0.0f;
				float longitude = 0.0f;
	
				foreach (XmlNode node in childNodes)
				{
					foreach (XmlNode locNode in node)
					{
						if (locNode.Name == "ResourceSet")
						{
							foreach (XmlNode innerNode in locNode)
							{
								Logging.WriteLogLine("DEBUG Node: ResourceSet: " + innerNode.Name.ToString() + " InnerText: " + innerNode.InnerText);
	
								if (innerNode.Name == "Resources")
								{
									foreach (XmlNode rNode in innerNode)
									{
										Logging.WriteLogLine("\tDEBUG Node: ResourceSet: Resources: " + rNode.Name.ToString() + " InnerText: " + rNode.InnerText);
										if (rNode.Name == "Location")
										{
											foreach (XmlNode r1Node in rNode)
											{
												Logging.WriteLogLine("\t\tDEBUG Node: " + r1Node.Name.ToString() + " InnerText: " + r1Node.InnerText);
	
												if (r1Node.Name == "Point")
												{
													foreach (XmlNode r2Node in r1Node)
													{
														switch (r2Node.Name)
														{
															case "Latitude":
																Logging.WriteLogLine("\t\t\tDEBUG Node: " + r2Node.Name.ToString() + " InnerText: " + r2Node.InnerText);
																latitude = float.Parse(r2Node.InnerText);
																break;
															case "Longitude":
																Logging.WriteLogLine("\t\t\tDEBUG Node: " + r2Node.Name.ToString() + " InnerText: " + r2Node.InnerText);
																longitude = float.Parse(r2Node.InnerText);
																break;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
	
	
				return (latitude, longitude);
			}
	*/

/*
	private (string latitude, string longitude) GetPointR( XmlNodeList childNodes )
	{
		var LocalLatitude  = "0.0";
		var LocalLongitude = "0.0";

		foreach( XmlNode Node in childNodes )
		{
			if( Node.Name == "Point" )
			{
				foreach( XmlNode R1Node in Node )
				{
					switch( R1Node.Name )
					{
					case "Latitude":
						Logging.WriteLogLine( "\tDEBUG Node: " + R1Node.Name + " InnerText: " + R1Node.InnerText );

						//latitude = double.Parse(r1Node.InnerText);
						LocalLatitude = R1Node.InnerText;
						break;

					case "Longitude":
						Logging.WriteLogLine( "\tDEBUG Node: " + R1Node.Name + " InnerText: " + R1Node.InnerText );

						//longitude = double.Parse(r1Node.InnerText);
						LocalLongitude = R1Node.InnerText;
						break;
					}
				}

				return ( LocalLatitude, LocalLongitude );
			}
			( LocalLatitude, LocalLongitude ) = GetPointR( Node.ChildNodes );
		}

		return ( LocalLatitude, LocalLongitude );
	}
*/
	/*
			private void GetJsonResponse(string requestUrl)
			{
				HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
				using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
				{
					if (response.StatusCode != HttpStatusCode.OK)
						throw new Exception(String.Format("Server error (HTTP {0}: {1}).",
						response.StatusCode,
						response.StatusDescription));
					Logging.WriteLogLine("DEBUG Response: " + response.ToString());                
				}
			}
	*/
#endregion

#region Actions
	public void Execute_RemoveAll()
	{
		Logging.WriteLogLine( "Removing all selected drivers" );

		foreach( var Sd in Drivers )
			Sd.IsVisible = false;

		SelectedDrivers.Clear();

		if (View is { } V)
		{
			MapHelper.RemoveAllDrivers(V.webView);
			MapHelper.RemoveAllTrips(V.webView);			
			MapHelper.RemoveAllLines(V.webView);
		}
	}

	public void RemoveSelected( List<Driver> toRemove )
	{
		foreach( var Driver in toRemove )
		{
			//Driver remove = ( from D in Drivers
			var Remove = ( from D in AllDrivers
			               where D.StaffId == Driver.StaffId
			               select D ).FirstOrDefault();

			if( Remove is { } R )
			{
				R.IsVisible = false;
				SelectedDrivers.Remove( R );
			}
		}
	}

	public void Execute_SaveSelectedDrivers()
	{
		Logging.WriteLogLine( "Saving selected drivers" );

		DriversGroup Dg = new()
		                  {
			                  Name = NewDriversGroupName.Trim()
		                  };

		foreach( var Driver in SelectedDrivers )
			Dg.Drivers.Add( Driver.StaffId );

		if( DriversMapSettings is { } Dms )
		{
			Dms.DriversGroups.Remove( Dg.Name );
			Dms.DriversGroups.Add( Dg.Name, Dg );
			SaveSettings( Dms );
		}

		SdDriversGroups.Remove( Dg.Name );
		SdDriversGroups.Add( Dg.Name, Dg.Drivers );
		SelectedDriverGroup = NewDriversGroupName;
		NewDriversGroupName = string.Empty;
		WhenSdDriversGroupsChanges();
	}

	public void Execute_LoadSelectedDriversGroup()
	{
		Logging.WriteLogLine( "Loading Drivers Group: " + SelectedDriverGroup );

		if( View is { } V && SelectedDriverGroup.IsNotNullOrWhiteSpace() )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   V.SetWaitCursor();
				                   V.webView.Visibility = Visibility.Hidden;
				                   var Sd   = new List<Driver>();
				                   var List = SdDriversGroups[ SelectedDriverGroup ];

				                   if( List != null )
				                   {
					                   foreach( var Tmp in AllDrivers )
						                   Tmp.IsVisible = false;

					                   foreach( var Name in List )
					                   {
						                   var Driver = ( from D in AllDrivers
						                                  where D.StaffId == Name
						                                  select D ).FirstOrDefault();

						                   if( Driver is not null )
						                   {
							                   Driver.IsVisible = true;
							                   Sd.Add( Driver );
						                   }
					                   }
				                   }

				                   if( Sd.Count > 0 )
				                   {
					                   foreach( var Driver in Sd )
						                   AddSelectedDriver( Driver.DisplayName );
				                   }

				                   SelectedDrivers = new ObservableCollection<Driver>( Sd );

				                   View.webView.Visibility = Visibility.Visible;
				                   View.webView.InvalidateVisual();

				                   View.ClearWaitCursor();
			                   } );
		}
	}

	public void Execute_SelectAllSelectedDrivers()
	{
		foreach( var Driver in SelectedDrivers )
			Driver.HasTrips = true;
	}

	public void Execute_SelectNoneSelectedDrivers()
	{
		foreach( var Driver in SelectedDrivers )
			Driver.HasTrips = false;
	}

	public void Execute_UpdateSelectedGroup()
	{
		if( SelectedDriverGroup.IsNotNullOrWhiteSpace() )
		{
			Logging.WriteLogLine( "DEBUG Updating " + SelectedDriverGroup );

			DriversGroup Dg = new()
			                  {
				                  Name = SelectedDriverGroup
			                  };

			foreach( var Driver in SelectedDrivers )
				Dg.Drivers.Add( Driver.StaffId );

			if( DriversMapSettings is { } Dms )
			{
				Dms.DriversGroups.Remove( Dg.Name );
				Dms.DriversGroups.Add( Dg.Name, Dg );

				SaveSettings( Dms );
			}

			SdDriversGroups.Remove( Dg.Name );
			SdDriversGroups.Add( Dg.Name, Dg.Drivers );
			WhenSdDriversGroupsChanges();
		}
		else
			Logging.WriteLogLine( "DEBUG SelectedDriverGroup is empty " );
	}

	public void Execute_DeleteSelectedGroup()
	{
		Logging.WriteLogLine( "DEBUG Deleting " + SelectedDriverGroup );

		if( SdDriversGroups.ContainsKey( SelectedDriverGroup ) )
		{
			foreach( var Driver in SdDriversGroups[ SelectedDriverGroup ] )
			{
				Logging.WriteLogLine( "DEBUG Removing driver from map: " + Driver );

				var DisplayName = ( from D in AllDrivers
				                    where D.StaffId == Driver
				                    select D.DisplayName ).FirstOrDefault();

				if( DisplayName is not null )
					RemoveSelectedDriver( DisplayName );
			}

			SdDriversGroups.Remove( SelectedDriverGroup );

			if( DriversMapSettings is { } Dms )
			{
				Dms.DriversGroups.Remove( SelectedDriverGroup );
				SaveSettings( Dms );
			}
		}

		if( DriversGroupsNames.Contains( SelectedDriverGroup ) )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   DriversGroupsNames.Remove( SelectedDriverGroup );
			                   } );
		}
		SelectedDriverGroup = string.Empty;
	}

	public void Execute_SearchHistory()
	{
		Logging.WriteLogLine( "DEBUG Searching for " + FromDateTime + " to " + ToDateTime );

		//HasSearchRun = true;

		MapHelper.RemoveEverything(View?.webView);
		MapHelper.RemoveAllDrivers(View?.webView);
		MapHelper.RemoveAllTrips(View?.webView);

		if( ShowDriverOnHistory && HasSearchRun )
		{
			foreach( var Driver in SelectedDrivers )
				GetDriversGps( Driver.StaffId );
		}

		//if( ShowTripsOnHistory && SelectedDrivers.Count > 0 )
		if( ShowTripsOnHistory && HasSearchRun )
		{
			GetTripsForDriversHistory();
			MapTripsHistory();
		}
	}
#endregion

#region Settings Persistence
	private static Settings? DriversMapSettings { get; set; } = new();

	public SortedDictionary<string, List<string>> SdDriversGroups
	{
		get { return Get( () => SdDriversGroups, new SortedDictionary<string, List<string>>() ); }
		set { Set( () => SdDriversGroups, value ); }
	}

	[DependsUpon( nameof( SdDriversGroups ) )]
	public void WhenSdDriversGroupsChanges()
	{
		var List = ( from D in SdDriversGroups.Keys
		             orderby D.ToLower()
		             select D ).ToList();

		DriversGroupsNames = new ObservableCollection<string>( List );
	}

	public class DriversGroup
	{
		public string       Name    { get; set; } = string.Empty;
		public List<string> Drivers { get; set; } = new();
	}

	public class Settings
	{
		public Dictionary<string, DriversGroup> DriversGroups { get; set; } = new();

		//public List<DriversGroup> DriversGroups { get; set; } = new List<DriversGroup>();
	}


	private string MakeSettingsFileName()
	{
		var AppData    = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		return $"{BaseFolder}\\DriversMap.{SETTINGS_FILE_EXTENSION}";
	}

	public new void LoadSettings()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var FileName = MakeSettingsFileName();

				if( File.Exists( FileName ) )
				{
					Logging.WriteLogLine( "Loading settings from " + FileName );
					var SerializedObject = File.ReadAllText( FileName );

					//SerializedObject = Encryption.Decrypt(SerializedObject);
					DriversMapSettings = JsonConvert.DeserializeObject<Settings>( SerializedObject );

					if( DriversMapSettings is not null )
					{
						foreach( var Key in DriversMapSettings.DriversGroups.Keys )
						{
							if( DriversMapSettings is { } Dms && Dms.DriversGroups[ Key ] is {Drivers: { } Dvrs} )
								SdDriversGroups.Add( Key, Dvrs );
						}
						WhenSdDriversGroupsChanges();
					}
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to load settings: " + E );

				// Doing this to ensure that the board will load if the file is corrupted
				Logging.WriteLogLine( "Deleting bad settings file for Drivers Map" );
				DriversMapSettings = new Settings();
				SaveSettings( DriversMapSettings );
			}
		}
	}

	private void SaveSettings( Settings? settings )
	{
		if( !IsInDesignMode )
		{
			try
			{
				var SerializedObject = JsonConvert.SerializeObject( settings );

				//Logging.WriteLogLine("Saved settings:\n" + SerializedObject);
				//SerializedObject = Encryption.Encrypt(SerializedObject);
				var FileName = MakeSettingsFileName();
				Logging.WriteLogLine( "Saving settings to " + FileName );
				File.WriteAllText( FileName, SerializedObject );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to save settings: " + E );
			}
		}
	}
	#endregion

	#region Live Map Timer
	private const int MINUTES_COUNT = 2; // Changed from 1;

	//public GpsPoint? LastPoint { get; set; } = null;
	public GpsPoint LastPoint
	{
	  //get { return Get( () => TripsHistoryLayer, new MapLayer() ); }
		get { return Get( () => LastPoint, new GpsPoint()); }
		set { Set(() => LastPoint, value); }
	}

	//[DependsUpon500(nameof(LastPoint))]
	public void WhenLastPointChanges()
	{
		if (LastPoint != null && LastPoint.Latitude > 0 && View is { } V)
		{
			Logging.WriteLogLine("DEBUG Setting LastPoint for CentreMapOnPoint " + LastPoint.Latitude + "," + LastPoint.Longitude + ", " + LastPoint.LocalDateTime);
			Dispatcher.Invoke(() =>
					{
						MapHelper.CentreMapOnPoint(LastPoint, V.webView);
					});
		}
	}

	public bool IsUpdateLiveMapRunning() => UpdateLiveMapTimer is {IsEnabled: true};

	public void StopUpdateLiveMap()
	{
		if( UpdateLiveMapTimer is not null )
		{
			Logging.WriteLogLine( "Stopping update live map timer" );
			UpdateLiveMapTimer.Stop();
		}
	}

	public void StartUpdateLiveMap()
	{
		Logging.WriteLogLine( "Starting update live map timer" );

		UpdateLiveMapTimer          =  new DispatcherTimer();
		UpdateLiveMapTimer.Tick     += UpdateLiveMap_Tick;
		UpdateLiveMapTimer.Interval =  new TimeSpan( 0, MINUTES_COUNT, 0 );
		UpdateLiveMapTimer.Start();
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	public void UpdateLiveMap_Tick( object? sender, EventArgs? e )
	{
		Logging.WriteLogLine("DEBUG UpdateLiveMap_Tick fired - SelectedDrivers.Count: " + SelectedDrivers.Count);
		//DecideWhatToShowOnLive();
		UpdateLiveMap_Tick_Worker(sender, e);
	}

	public void UpdateLiveMap_Tick_Worker( object? sender, EventArgs? e, bool centreMap = false)
	{
		//Logging.WriteLogLine("DEBUG UpdateLiveMap_Tick fired - SelectedDrivers.Count: " + SelectedDrivers.Count);

		//if( ( SelectedDrivers.Count > 0 ) && ShowDriverOnLive )
		if( ShowDriverOnLive && View != null && ActiveMapView == MAP_VIEW_LIVE )
		{
			Dispatcher.Invoke(() =>
			{
				//MapHelper.RemoveAllDrivers(View.webView);
				MapHelper.RemoveEverything(View.webView);
				MapHelper.RemoveAllDrivers(View.webView);
				MapHelper.RemoveAllLines(View.webView);
				MapHelper.RemoveAllTrips(View.webView);
			});

			var now = DateTimeOffset.Now;
			//GpsPoint? lastPoint = null;

			Driver? last = null;
			if (SelectedDrivers.Count > 0)
			{
				last = SelectedDrivers.Last();
				Logging.WriteLogLine("DEBUG Setting last to " + last.StaffId);
			}
			foreach( var Driver in SelectedDrivers )
			{
				GpsDriverDateRange Gddr;
				var                StaffId = Driver.StaffId;

				//if (DictLiveDrivers.ContainsKey(StaffId))
				//{
				//	Gddr = new GpsDriverDateRange
				//	{
				//		Driver = StaffId,
				//		FromDateTime = now.AddMinutes(-MINUTES_COUNT),
				//		ToDateTime = now
				//	};
				//}
				//else // First time - get all points for the day
				//{
				//	Gddr = new GpsDriverDateRange
				//	{
				//		Driver = StaffId,
				//		FromDateTime = now.Date,
				//		ToDateTime = now.Date.AddDays(1)
				//	};
				//}
				Gddr = new GpsDriverDateRange
				{
					Driver = StaffId,
					FromDateTime = now.Date,
					ToDateTime = now.Date.AddDays(1)
				};

				if (!IsInDesignMode && ShowDriverOnLive)
				{
					Task.Run(async () =>
							  {
								  //Logging.WriteLogLine("DEBUG FromDateTime: " + Gddr.FromDateTime + ", ToDateTime: " + Gddr.ToDateTime);
								  var Gps = await Azure.Client.RequestGetGpsPoints(Gddr);

								  if (Gps is not null)
								  {
									  // Sometimes the query returns points from before the start time
									  Gps = RemovePointsBeforeStart(Gps, Gddr.FromDateTime);

									  Dispatcher.Invoke(() =>
														 {
															 if (!DictLiveDrivers.TryGetValue(StaffId, out var DonM))
																 DictLiveDrivers.Add(StaffId, DonM = new DriverOnMap(StaffId));
															 DonM.SetGpsPoints(Gps);
															 //DonM.AppendGpsPoints( Gps );  // Turned off - Loading all points each time

															 foreach (var Gp in Gps)
															 {
																 //Logging.WriteLogLine("\tDEBUG UpdateLiveMap_Tick gp: driver: " + driver + ", Latitude: " + gp.La + ", Longitude: " + gp.Lo + ", Date: " + gp.LocalDateTime);

																 var MapPolyLine = DonM.Line;

																 if (MapPolyLine is { } Pl)
																 {
																	 Pl.Locations ??= new LocationCollection();
																	 Pl.Locations.Add(new Location(Gp.Latitude, Gp.Longitude));
																 }
															 }

															 // Update the view if visible
															 if (ActiveMapView == MAP_VIEW_LIVE)
															 {
																 foreach (var ValuePair in DictLiveDrivers)
																 {
																	 var Key = ValuePair.Key;

																	 var Default = (from D in Drivers
																					where D.StaffId == Key
																					select D).FirstOrDefault();

																	 // Check that the driver isn't hidden
																	 if (Default?.HasTrips == true)
																	 {
																		 var GpsPoint = ValuePair.Value.Last;

																		 if (GpsPoint is { } Point)
																		 {
																			 if (centreMap && last?.StaffId == StaffId)
																			 {
																				 Logging.WriteLogLine("DEBUG Setting centre on driver: " + StaffId + " : " + Point.Latitude + "," + Point.Longitude);
																				 MapHelper.CentreMapOnPoint(Point, View.webView);
																			 }
																			 //Logging.WriteLogLine("DEBUG Adding driver: " + Key + " timestamp: " + Point.LocalDateTime);
																			 PutDriversLabelOnMap(Key, Point);
																		 }
																	 }
																 }

																 //if (ShowTripsOnLive)
																 //{
																	// MapTripsLive();
																 //}
															 }
														 });
								  }
							  });				   
				}
			}

			if (ShowTripsOnLive)
			{
				Dispatcher.Invoke(() =>
				{
					MapTripsLive();
				});
			}
		}
		else if (ShowTripsOnLive && ActiveMapView == MAP_VIEW_LIVE )
		{
			Dispatcher.Invoke(() =>
			{
				MapTripsLive();
			});
		}


	}

	private GpsPoints RemovePointsBeforeStart(GpsPoints gps, DateTimeOffset beginning)
	{
		GpsPoints sanatized = new();

		foreach (var gp in gps)
		{
			if (gp.LocalDateTime >= beginning)
			{
				sanatized.Add(gp);
			}
		}

		return sanatized;
	}
	private bool AreGpsPointsValid(GpsPoints gps, DateTimeOffset beginning)
	{
		bool yes = true;
		foreach (var gp in gps)
		{
			if (gp.LocalDateTime < beginning)
			{
				yes = false; 
				break;
			}
		}

		return yes;
	}
	#endregion
}