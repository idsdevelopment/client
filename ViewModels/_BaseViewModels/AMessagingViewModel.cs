﻿#nullable enable

using System.Threading;

namespace ViewModels.BaseViewModels;

public abstract class AMessagingViewModel : ViewModelBase, IDisposable
{
	protected const int ERROR_TIMEOUT_MS              = 10_000,
	                    DEFAULT_TIME_TO_LIVE_IN_HOURS = 1;

	protected static readonly STATUS[] AllStatusFilters =
	{
		STATUS.NEW,
		STATUS.ACTIVE,
		STATUS.DISPATCHED,
		STATUS.PICKED_UP,
		STATUS.DELIVERED,
		STATUS.VERIFIED,
		STATUS.POSTED,
		STATUS.INVOICED,
		STATUS.SCHEDULED,
		STATUS.FINALISED,
		STATUS.DELETED
	};

	private CancellationTokenSource? Token;

	protected readonly string Board;

	protected readonly STATUS[] Filters;

	protected readonly string UserName;

	protected abstract void OnUpdateTrip( Trip trip );

	protected abstract void OnRemoveTrip( string tripId );

	protected abstract void OnUpdateStatus( string tripId, STATUS status, STATUS1 status1, STATUS2 status2, bool receivedByDevice, bool readByDriver );

	protected virtual void Dispose( bool disposing )
	{
		if( Token is not null )
		{
			Azure.Client.CancelTripUpdateListen( Token );
			Token = null;
		}
	}

	~AMessagingViewModel()
	{
		Dispose( false );
	}


	protected async Task UpdateStatus( string program,
	                                   STATUS status, STATUS1 status1, STATUS2 status2, List<string> tripIds, string driver = "",
	                                   TripUpdateStatus.BROADCAST broadcast = TripUpdateStatus.BROADCAST.NONE,
	                                   TripUpdateStatusBase.DEVICE_STATUS deviceStatus = TripUpdateStatusBase.DEVICE_STATUS.UNSET )
	{
		if( tripIds.Count > 0 )
		{
			while( true )
			{
				try
				{
					await Azure.Client.RequestUpdateTripStatus( new TripUpdateStatus
					                                            {
						                                            Program      = program,
						                                            DeviceStatus = deviceStatus,
						                                            Status       = status,
						                                            Status1      = status1,
						                                            Status2      = status2,
						                                            Broadcast    = broadcast,
						                                            Driver       = driver,
						                                            TripIdList   = tripIds
					                                            } );

					var TripIds = string.Join( ", ", tripIds );

					switch( status )
					{
					case STATUS.DELETED:
						Logging.WriteLogLine( $"DELETED Trips: {TripIds}" );
						break;

					case STATUS.DISPATCHED:
						Logging.WriteLogLine( $"Trips: {TripIds} - Assigned to driver: {driver}" );
						break;
					}

					break;
				}
				catch
				{
					Thread.Sleep( ERROR_TIMEOUT_MS );
				}
			}
		}
	}

	protected AMessagingViewModel( string board, STATUS[]? filters = null, int timeToLiveInHours = DEFAULT_TIME_TO_LIVE_IN_HOURS )
	{
		Board   = board;
		Filters = filters ?? AllStatusFilters;
		var TimeToLiveHours = timeToLiveInHours.ToString();

		UserName = $"{Globals.CurrentUser.UserName}-{DateTime.UtcNow:ddHHmmss}";

		if( !IsInDesignMode )
		{
			Azure.Client.ListenTripUpdate( board, UserName, TimeToLiveHours, async tripUpdateMessages =>
			                                                                 {
				                                                                 if( tripUpdateMessages is not null )
				                                                                 {
					                                                                 foreach( var UpdateMessage in tripUpdateMessages )
					                                                                 {
						                                                                 if( !Filters.Contains( UpdateMessage.Status ) )
							                                                                 continue;

						                                                                 switch( UpdateMessage.Action )
						                                                                 {
						                                                                 case TripUpdateMessage.ACTION.UPDATE_STATUS:
							                                                                 Dispatcher.Invoke( () =>
							                                                                                    {
								                                                                                    foreach( var TripId in UpdateMessage.TripIdList )
								                                                                                    {
									                                                                                    switch( UpdateMessage.Status )
									                                                                                    {
									                                                                                    case STATUS.DELETED:
									                                                                                    case STATUS.FINALISED:
										                                                                                    OnRemoveTrip( TripId );
										                                                                                    break;

									                                                                                    default:
										                                                                                    OnUpdateStatus( TripId, UpdateMessage.Status, UpdateMessage.Status1, UpdateMessage.Status2,
										                                                                                                    UpdateMessage.ReceivedByDevice,
										                                                                                                    UpdateMessage.ReadByDriver );
										                                                                                    break;
									                                                                                    }
								                                                                                    }
							                                                                                    } );

							                                                                 break;

						                                                                 case TripUpdateMessage.ACTION.UPDATE_TRIP:
							                                                                 while( true )
							                                                                 {
								                                                                 try
								                                                                 {
									                                                                 switch( UpdateMessage.Status )
									                                                                 {
									                                                                 case STATUS.FINALISED:
									                                                                 case STATUS.DELETED:
										                                                                 foreach( var TripId in UpdateMessage.TripIdList )
											                                                                 OnRemoveTrip( TripId );
										                                                                 break;

									                                                                 default:
										                                                                 var Trips = await Azure.Client.RequestGetTrips( new TripIdList( UpdateMessage.TripIdList ) );

										                                                                 if( Trips is not null )
										                                                                 {
											                                                                 var Trips1 = Trips;

											                                                                 Dispatcher.Invoke( () =>
											                                                                                    {
												                                                                                    foreach( var Trip in Trips1 )
												                                                                                    {
													                                                                                    switch( Trip.Status1 )
													                                                                                    {
													                                                                                    case STATUS.DELETED:
													                                                                                    case STATUS.FINALISED:
														                                                                                    OnRemoveTrip( Trip.TripId );
														                                                                                    break;

													                                                                                    default:
														                                                                                    OnUpdateTrip( Trip );
														                                                                                    break;
													                                                                                    }
												                                                                                    }
											                                                                                    } );
										                                                                 }
										                                                                 break;
									                                                                 }
									                                                                 break;
								                                                                 }
								                                                                 catch( Exception E )
								                                                                 {
									                                                                 Console.WriteLine( E );
									                                                                 Thread.Sleep( ERROR_TIMEOUT_MS );
								                                                                 }
							                                                                 }
							                                                                 break;
						                                                                 }
					                                                                 }
				                                                                 }
			                                                                 } );
		}
	}


	public void Dispose()
	{
		Dispose( true );
		GC.SuppressFinalize( this );
	}
}