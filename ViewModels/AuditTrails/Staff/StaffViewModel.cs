﻿namespace ViewModels.AuditTrails;

public class StaffViewModel : ViewModelBase
{
	public bool SearchByUser
	{
		get { return Get( () => SearchByUser, false ); }
		set { Set( () => SearchByUser, value ); }
	}


	public bool SearchEnabled
	{
		get { return Get( () => SearchEnabled, true ); }
		set { Set( () => SearchEnabled, value ); }
	}


	public string User
	{
		get { return Get( () => User, "" ); }
		set { Set( () => User, value ); }
	}

	[DependsUpon250( nameof( User ) )]
	[DependsUpon( nameof( SearchByUser ) )]
	public void EnableSearch()
	{
		SearchEnabled = !SearchByUser || ( SearchByUser && User.IsNotNullOrWhiteSpace() );
	}
}