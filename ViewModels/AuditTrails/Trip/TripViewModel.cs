﻿namespace ViewModels.AuditTrails;

public class TripViewModel : ViewModelBase
{
	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	public bool EnableSearch
	{
		get { return Get( () => EnableSearch, false ); }
		set { Set( () => EnableSearch, value ); }
	}

	public Action ExecuteAuditTrip;

	public override void OnArgumentChange( object arg )
	{
		base.OnArgumentChange( arg );

		if( arg is string Id )
		{
			TripId = Id;
			WhenTripIdChanges();
			ExecuteAuditTrip?.Invoke();
		}
	}


	[DependsUpon250( nameof( TripId ) )]
	public void WhenTripIdChanges()
	{
		EnableSearch = TripId.Trim().IsNotNullOrWhiteSpace();
	}
}