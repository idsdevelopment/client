﻿using System.Windows.Controls;

namespace ViewModels.AuditTrails;

/// <summary>
///     Interaction logic for Trip.xaml
/// </summary>
public partial class Trip : Page
{
	public Trip()
	{
		Initialized += ( _, _ ) =>
		               {
			               if( DataContext is TripViewModel Model )
			               {
				               Model.ExecuteAuditTrip = () =>
				                                        {
					                                        if( !ViewModelBase.IsInDesignMode )
						                                        Viewer.GetLog();
				                                        };
			               }
		               };
		InitializeComponent();
	}
}