﻿#nullable enable

using System.Collections;
using System.IO;
using System.Timers;
using static ViewModels.Reporting.DataSets;
using File = System.IO.File;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using Role = IdsRemoteServiceControlLibraryV2.Role;

namespace ViewModels.Staff.ShiftRates;

public class MaintainStaffViewModel : ViewModelBase
{
	private const string PROGRAM_NAME = "MaintainStaff";

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638910469/Adding+new+Staff+Member" ); }
	}

	public bool ShowZones
	{
		get { return Get( () => ShowZones, IsInDesignMode ); }
		set { Set( () => ShowZones, value ); }
	}


	public StaffLookupSummary StaffLookupSummary
	{
		get { return Get( () => StaffLookupSummary, new StaffLookupSummary() ); }
		set { Set( () => StaffLookupSummary, value ); }
	}


	public bool ShowUpdated
	{
		get { return Get( () => ShowUpdated, IsInDesignMode ); }
		set { Set( () => ShowUpdated, value ); }
	}

	public bool InAdd
	{
		get { return Get( () => InAdd, IsInDesignMode ); }
		set { Set( () => InAdd, value ); }
	}


	public bool InEdit
	{
		get { return Get( () => InEdit, true ); }
		set { Set( () => InEdit, value ); }
	}


	public bool EnableSave
	{
		get { return Get( () => EnableSave, false ); }
		set { Set( () => EnableSave, value ); }
	}


	public bool EnableCancel
	{
		get { return Get( () => EnableCancel, false ); }
		set { Set( () => EnableCancel, value ); }
	}


	public string LookupStaffId
	{
		get { return Get( () => LookupStaffId, "" ); }
		set { Set( () => LookupStaffId, value ); }
	}

	public string StaffId
	{
		get { return Get( () => StaffId, "" ); }
		set { Set( () => StaffId, value ); }
	}


	public string NewStaffId
	{
		get { return Get( () => NewStaffId, "" ); }
		set { Set( () => NewStaffId, value ); }
	}


	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}

	public string PasswordConfirmation
	{
		get { return Get( () => PasswordConfirmation, "" ); }
		set { Set( () => PasswordConfirmation, value ); }
	}

	public bool PasswordOk
	{
		get { return Get( () => PasswordOk, !IsInDesignMode ); }
		set { Set( () => PasswordOk, value ); }
	}


	[DependsUpon( nameof( PasswordOk ) )]
	public string IsPasswordOk
	{
		//get
		//{
		//	string visibility = "Hidden";
		//	if (!PasswordOk)
		//             {
		//		visibility = "Visible";
		//             }
		//	Logging.WriteLogLine("Setting PasswordProblem visibility to " + visibility);
		//	return visibility;
		//}
		get { return Get( () => IsPasswordOk, "Hidden" ); }
		set { Set( () => IsPasswordOk, value ); }
	}


	public bool Enabled
	{
		get { return Get( () => Enabled, true ); }
		set { Set( () => Enabled, value ); }
	}

	public string FirstName
	{
		get { return Get( () => FirstName, "" ); }
		set { Set( () => FirstName, value ); }
	}

	public string LastName
	{
		get { return Get( () => LastName, "" ); }
		set { Set( () => LastName, value ); }
	}


	public string MiddleNames
	{
		get { return Get( () => MiddleNames, "" ); }
		set { Set( () => MiddleNames, value ); }
	}

	public string Phone
	{
		get { return Get( () => Phone, "" ); }
		set { Set( () => Phone, value ); }
	}

	public string Mobile
	{
		get { return Get( () => Mobile, "" ); }
		set { Set( () => Mobile, value ); }
	}

	public string Email
	{
		get { return Get( () => Email, "" ); }
		set { Set( () => Email, value ); }
	}


	public string Suite
	{
		get { return Get( () => Suite, "" ); }
		set { Set( () => Suite, value ); }
	}


	public string Street
	{
		get { return Get( () => Street, "" ); }
		set { Set( () => Street, value ); }
	}


	public string Street1
	{
		get { return Get( () => Street1, "" ); }
		set { Set( () => Street1, value ); }
	}

	public string City
	{
		get { return Get( () => City, "" ); }
		set { Set( () => City, value ); }
	}

	//public string Region
	//{
	//	get { return Get( () => Region, "" ); }
	//	set { Set( () => Region, value ); }
	//}

	//public string Country
	//{
	//	get { return Get( () => Country, "" ); }
	//	set { Set( () => Country, value ); }
	//}
	public IList Countries
	{
		get => CountriesRegions.Countries;
		set { Set( () => Countries, value ); }
	}


	public IList Regions
	{
		get { return Get( () => Regions, new List<object>() ); }
		set { Set( () => Regions, value ); }
	}

	public string AddressRegion
	{
		get { return Get( () => AddressRegion, "" ); }
		set { Set( () => AddressRegion, value ); }
	}

	public string AddressCountry
	{
		get { return Get( () => AddressCountry, "" ); }
		set { Set( () => AddressCountry, value ); }
	}


	public string PreviousCountry
	{
		get { return Get( () => PreviousCountry, "" ); }
		set { Set( () => PreviousCountry, value ); }
	}

	public string PostalCode
	{
		get { return Get( () => PostalCode, "" ); }
		set { Set( () => PostalCode, value ); }
	}


	public string EmergencyFirstName
	{
		get { return Get( () => EmergencyFirstName, "" ); }
		set { Set( () => EmergencyFirstName, value ); }
	}

	public string EmergencyLastName
	{
		get { return Get( () => EmergencyLastName, "" ); }
		set { Set( () => EmergencyLastName, value ); }
	}

	public string EmergencyPhone
	{
		get { return Get( () => EmergencyPhone, "" ); }
		set { Set( () => EmergencyPhone, value ); }
	}

	public string EmergencyMobile
	{
		get { return Get( () => EmergencyMobile, "" ); }
		set { Set( () => EmergencyMobile, value ); }
	}


	public string EmergencyEmail
	{
		get { return Get( () => EmergencyEmail, "" ); }
		set { Set( () => EmergencyEmail, value ); }
	}


	public decimal MainCommission1
	{
		get { return Get( () => MainCommission1, 0 ); }
		set { Set( () => MainCommission1, value ); }
	}

	public decimal MainCommission2
	{
		get { return Get( () => MainCommission2, 0 ); }
		set { Set( () => MainCommission2, value ); }
	}

	public decimal MainCommission3
	{
		get { return Get( () => MainCommission3, 0 ); }
		set { Set( () => MainCommission3, value ); }
	}


	public decimal OverrideCommission1
	{
		get { return Get( () => OverrideCommission1, 0 ); }
		set { Set( () => OverrideCommission1, value ); }
	}

	public decimal OverrideCommission2
	{
		get { return Get( () => OverrideCommission2, 0 ); }
		set { Set( () => OverrideCommission2, value ); }
	}

	public decimal OverrideCommission3
	{
		get { return Get( () => OverrideCommission3, 0 ); }
		set { Set( () => OverrideCommission3, value ); }
	}

	public decimal OverrideCommission4
	{
		get { return Get( () => OverrideCommission4, 0 ); }
		set { Set( () => OverrideCommission4, value ); }
	}

	public decimal OverrideCommission5
	{
		get { return Get( () => OverrideCommission5, 0 ); }
		set { Set( () => OverrideCommission5, value ); }
	}

	public decimal OverrideCommission6
	{
		get { return Get( () => OverrideCommission6, 0 ); }
		set { Set( () => OverrideCommission6, value ); }
	}

	public decimal OverrideCommission7
	{
		get { return Get( () => OverrideCommission7, 0 ); }
		set { Set( () => OverrideCommission7, value ); }
	}

	public decimal OverrideCommission8
	{
		get { return Get( () => OverrideCommission8, 0 ); }
		set { Set( () => OverrideCommission8, value ); }
	}

	public decimal OverrideCommission9
	{
		get { return Get( () => OverrideCommission9, 0 ); }
		set { Set( () => OverrideCommission9, value ); }
	}

	public decimal OverrideCommission10
	{
		get { return Get( () => OverrideCommission10, 0 ); }
		set { Set( () => OverrideCommission10, value ); }
	}

	public decimal OverrideCommission11
	{
		get { return Get( () => OverrideCommission11, 0 ); }
		set { Set( () => OverrideCommission11, value ); }
	}

	public decimal OverrideCommission12
	{
		get { return Get( () => OverrideCommission12, 0 ); }
		set { Set( () => OverrideCommission12, value ); }
	}


	public bool AlreadyCreated
	{
		get { return Get( () => AlreadyCreated, IsInDesignMode ); }
		set { Set( () => AlreadyCreated, value ); }
	}


	public bool EnableStaffNotes
	{
		get { return Get( () => EnableStaffNotes, false ); }
		set { Set( () => EnableStaffNotes, value ); }
	}

	public Role ChangedRole
	{
		get { return Get( () => ChangedRole, new Role() ); }
		set { Set( () => ChangedRole, value ); }
	}


	public List<Protocol.Data.Role> SelectedRoles
	{
		get { return Get( () => SelectedRoles, new List<Protocol.Data.Role>() ); }
		set { Set( () => SelectedRoles, value ); }
	}

	public LOG Log
	{
		get { return Get( () => Log, LOG.STAFF ); }

		set { Set( () => Log, value ); }
	}

	protected override async void OnInitialised()
	{
		DoEnableStaffNotes();

		LoadCachedStaffData();

		if( StaffMembersRoles.Count == 0 )
			await LoadStaff();
		else
		{
			// Run in background
			_ = Task.Run( async () =>
						  {
							  await LoadStaff();
						  } );
		}
	}

	public Func<string, string> FormatData = data => data;

	private bool                 InCheckId;
	public  Protocol.Data.Staff? OriginalStaff;

	public new Action? Refresh;
	private    bool    ReloadOld;

	public Func<LogLookup, LogLookup> RequestOverride = lookup => lookup;
	public Action?                    SetNewStaffFocus;

	private Timer? ShowUpdateTimer;

	private Protocol.Data.Staff BuildStaffMember()
	{
		Protocol.Data.Staff Sd = new()
								 {
									 StaffId  = InAdd ? NewStaffId : StaffId,
									 Enabled  = Enabled,
									 Password = Encryption.ToTimeLimitedToken( Password ),

									 Address = new Address
											   {
												   Suite        = Suite,
												   AddressLine1 = Street,
												   AddressLine2 = Street1,
												   City         = City,
												   Country      = AddressCountry,
												   PostalCode   = PostalCode,
												   EmailAddress = Email,
												   Phone        = Phone,
												   Mobile       = Mobile,
												   Region       = AddressRegion
											   },

									 EmergencyEmail     = EmergencyEmail,
									 EmergencyFirstName = EmergencyFirstName,
									 EmergencyLastName  = EmergencyLastName,
									 EmergencyMobile    = EmergencyMobile,
									 EmergencyPhone     = EmergencyPhone,
									 FirstName          = FirstName,
									 LastName           = LastName,
									 MiddleNames        = MiddleNames,

									 MainCommission1 = MainCommission1,
									 MainCommission2 = MainCommission2,
									 MainCommission3 = MainCommission3,

									 OverrideCommission1  = OverrideCommission1,
									 OverrideCommission2  = OverrideCommission2,
									 OverrideCommission3  = OverrideCommission3,
									 OverrideCommission4  = OverrideCommission4,
									 OverrideCommission5  = OverrideCommission5,
									 OverrideCommission6  = OverrideCommission6,
									 OverrideCommission7  = OverrideCommission7,
									 OverrideCommission8  = OverrideCommission8,
									 OverrideCommission9  = OverrideCommission9,
									 OverrideCommission10 = OverrideCommission10,
									 OverrideCommission11 = OverrideCommission11,
									 OverrideCommission12 = OverrideCommission12
								 };

		return Sd;
	}

	private static StaffAndRoles BuildStaffAndRoles( Protocol.Data.Staff staff, List<Protocol.Data.Role> roles )
	{
		StaffAndRoles Sar = new()
							{
								StaffId  = staff.StaffId,
								Enabled  = staff.Enabled,
								Password = staff.Password,

								Address = new Address
										  {
											  Suite        = staff.Address.Suite,
											  AddressLine1 = staff.Address.AddressLine1,
											  AddressLine2 = staff.Address.AddressLine2,
											  City         = staff.Address.City,
											  Country      = staff.Address.Country,
											  PostalCode   = staff.Address.PostalCode,
											  EmailAddress = staff.Address.EmailAddress,
											  Phone        = staff.Address.Phone,
											  Mobile       = staff.Address.Mobile,
											  Region       = staff.Address.Region
										  },

								EmergencyEmail     = staff.EmergencyEmail,
								EmergencyFirstName = staff.EmergencyFirstName,
								EmergencyLastName  = staff.EmergencyLastName,
								EmergencyMobile    = staff.EmergencyMobile,
								EmergencyPhone     = staff.EmergencyPhone,
								FirstName          = staff.FirstName,
								LastName           = staff.LastName,
								MiddleNames        = staff.MiddleNames,

								MainCommission1 = staff.MainCommission1,
								MainCommission2 = staff.MainCommission2,
								MainCommission3 = staff.MainCommission3,

								OverrideCommission1  = staff.OverrideCommission1,
								OverrideCommission2  = staff.OverrideCommission2,
								OverrideCommission3  = staff.OverrideCommission3,
								OverrideCommission4  = staff.OverrideCommission4,
								OverrideCommission5  = staff.OverrideCommission5,
								OverrideCommission6  = staff.OverrideCommission6,
								OverrideCommission7  = staff.OverrideCommission7,
								OverrideCommission8  = staff.OverrideCommission8,
								OverrideCommission9  = staff.OverrideCommission9,
								OverrideCommission10 = staff.OverrideCommission10,
								OverrideCommission11 = staff.OverrideCommission11,
								OverrideCommission12 = staff.OverrideCommission12
							};

		//sar.Roles = roles;
		foreach( var Role in roles )
			Sar.Roles.Add( Role );

		return Sar;
	}

	private void Clear()
	{
		SelectedStaff = null;

		Enabled = true;

		StaffId              = "";
		Password             = "";
		PasswordConfirmation = "";

		FirstName   = "";
		LastName    = "";
		MiddleNames = "";

		Phone  = "";
		Mobile = "";
		Email  = "";

		Suite   = "";
		Street  = "";
		Street1 = "";
		City    = "";

		//Region     = "";
		//Country    = "";

		if( PreviousCountry.IsNotNullOrWhiteSpace() )
			AddressCountry = PreviousCountry;
		else
		{
			// Get the most common country
			//AddressCountry = await GetMostCommonCountry();
			AddressCountry = GetMostCommonCountry();

			// If nothing, select the first
			if( AddressCountry.IsNullOrWhiteSpace() )
				AddressCountry = (string)Countries[ 0 ];
		}
		AddressRegion = "";
		PostalCode    = "";

		EmergencyFirstName = "";
		EmergencyLastName  = "";
		EmergencyPhone     = "";
		EmergencyMobile    = "";
		EmergencyEmail     = "";

		MainCommission1 = 0;
		MainCommission2 = 0;
		MainCommission3 = 0;

		OverrideCommission1  = 0;
		OverrideCommission2  = 0;
		OverrideCommission3  = 0;
		OverrideCommission4  = 0;
		OverrideCommission5  = 0;
		OverrideCommission6  = 0;
		OverrideCommission7  = 0;
		OverrideCommission8  = 0;
		OverrideCommission9  = 0;
		OverrideCommission10 = 0;
		OverrideCommission11 = 0;
		OverrideCommission12 = 0;

		IsPasswordOk = "Hidden";

		NoteName  = "";
		NoteText  = "";
		NoteNames = new List<string>();

		IsAddEditDeleteEnabled = true;
		IsSaveCancelEnabled    = false;
		IsReadOnly             = true;
		IsAddNote              = false;
		IsEditNote             = false;

		OldNoteName = NoteName = string.Empty;
		OldNoteText = NoteText = string.Empty;

		OriginalStaff = null;

		SelectedRoles.Clear();
	}

	//private async Task<string> GetMostCommonCountry()
	private string GetMostCommonCountry()
	{
		var Chosen = "";

		var Dict = new Dictionary<string, int>();

		foreach( var Sm in StaffMembers )
		{
			var Country = Sm.Address.Country;

			if( Country.IsNotNullOrWhiteSpace() )
			{
				if( !Dict.ContainsKey( Country ) )
					Dict.Add( Country, 1 );
				else
					++Dict[ Country ];
			}

			//var S = await Azure.Client.RequestGetStaffMember( Sm.StaffId );

			//if( S is not null )
			//{
			//	//string country = sm.Address.Country;
			//	var Country = S.Address.Country;

			//	if( Country.IsNotNullOrWhiteSpace() )
			//	{
			//		if( !Dict.ContainsKey( Country ) )
			//			Dict.Add( Country, 1 );
			//		else
			//			++Dict[ Country ];
			//	}
			//}
		}

		// Simple case - only 1 country
		if( Dict.Count == 1 )
		{
			foreach( var Tmp in Dict.Keys )
			{
				Chosen = Tmp;
				break;
			}
		}
		else
		{
			var HighestCount = -1;
			var HighestKey   = string.Empty;

			foreach( var Tmp in Dict.Keys )
			{
				if( HighestCount == -1 )
				{
					HighestCount = Dict[ Tmp ];
					HighestKey   = Tmp;
				}
				else
				{
					if( Dict[ Tmp ] > HighestCount )
					{
						HighestCount = Dict[ Tmp ];
						HighestKey   = Tmp;
					}
				}
			}
			Chosen = HighestKey;
		}

		Logging.WriteLogLine( "Found " + Chosen );

		return Chosen;
	}

	[DependsUpon( nameof( Loaded ) )]
	public void WhenLoadedChanges()
	{
		Logging.WriteLogLine( "DEBUG Loaded: " + Loaded );
	}


	[DependsUpon( nameof( AddressCountry ) )]
	public void WhenAddressCountryChanges()
	{
		List<string> Region = new();

		if( AddressCountry.IsNotNullOrWhiteSpace() )
			PreviousCountry = AddressCountry;

		Region = AddressCountry switch
				 {
					 "Australia"     => CountriesRegions.AustraliaRegions,
					 "Canada"        => CountriesRegions.CanadaRegions,
					 "United States" => CountriesRegions.USRegions,
					 _               => Region
				 };

		Regions = Region;
	}

	[DependsUpon350( nameof( StaffLookupSummary ) )]
	public async Task WhenStaffLookupSummaryChanges()
	{
		if( !IsInDesignMode )
		{
			EnableSave = false;

			var Rec = StaffLookupSummary;
			FirstName = Rec.FirstName;
			LastName  = Rec.LastName;
			Street    = Rec.AddressLine1;

			if( !ReloadOld || OriginalStaff.IsNull() )
				OriginalStaff = await Azure.Client.RequestGetStaffMember( StaffId );

			ReloadOld = false;
			var S = OriginalStaff;

			if( S is not null && S.IsNotNull() && S.Ok )
			{
				var Ok = await WhenPasswordChanges();

				if( Ok )
				{
					var A = S.Address;

					Dispatcher.Invoke( () =>
									   {
										   Enabled = S.Enabled;

										   Password    = Password;
										   MiddleNames = S.MiddleNames;

										   Phone  = A.Phone;
										   Mobile = A.Mobile;
										   Email  = A.EmailAddress;

										   Suite   = A.Suite;
										   Street  = A.AddressLine1;
										   Street1 = A.AddressLine2;
										   City    = A.City;

										   //Region     = A.Region;
										   //Country    = A.Country;
										   AddressRegion  = A.Region;
										   AddressCountry = A.Country;
										   PostalCode     = A.PostalCode;

										   EmergencyFirstName = S.EmergencyFirstName;
										   EmergencyLastName  = S.EmergencyLastName;
										   EmergencyPhone     = S.EmergencyPhone;
										   EmergencyMobile    = S.EmergencyMobile;
										   EmergencyEmail     = S.EmergencyEmail;

										   MainCommission1 = S.MainCommission1;
										   MainCommission2 = S.MainCommission2;
										   MainCommission3 = S.MainCommission3;

										   OverrideCommission1  = S.OverrideCommission1;
										   OverrideCommission2  = S.OverrideCommission2;
										   OverrideCommission3  = S.OverrideCommission3;
										   OverrideCommission4  = S.OverrideCommission4;
										   OverrideCommission5  = S.OverrideCommission5;
										   OverrideCommission6  = S.OverrideCommission6;
										   OverrideCommission7  = S.OverrideCommission7;
										   OverrideCommission8  = S.OverrideCommission8;
										   OverrideCommission9  = S.OverrideCommission9;
										   OverrideCommission10 = S.OverrideCommission10;
										   OverrideCommission11 = S.OverrideCommission11;
										   OverrideCommission12 = S.OverrideCommission12;
									   } );
				}
			}
		}
	}

	[DependsUpon( nameof( ShowUpdated ) )]
	public void WhenShowUpdatedChanged()
	{
		if( ShowUpdated )
		{
			if( ShowUpdateTimer is null )
			{
				ShowUpdateTimer = new Timer( 5000 ) {AutoReset = false};

				ShowUpdateTimer.Elapsed += ( _, _ ) =>
										   {
											   Dispatcher.Invoke( () =>
															      {
																      ShowUpdated = false;
															      } );
										   };
			}

			ShowUpdateTimer.Start();
		}
	}

	[DependsUpon( nameof( StaffId ) )]
	public void WhenStaffIdChanges()
	{
		LookupStaffId = StaffId;
	}

	[DependsUpon350( nameof( NewStaffId ) )]
	public async Task WhenNewStaffIdChanges()
	{
		if( !IsInDesignMode && !InCheckId )
		{
			InCheckId = true;

			try
			{
				var HasId = await Azure.Client.RequestStaffMemberExists( NewStaffId );

				Dispatcher.Invoke( () =>
								   {
									   AlreadyCreated = HasId;
								   } );
			}
			finally
			{
				InCheckId = false;
			}
		}
	}

	[DependsUpon350( nameof( Password ) )]
	public async Task<bool> WhenPasswordChanges()
	{
		if( !IsInDesignMode )
		{
			var Ok = await Azure.Client.RequestIsPasswordValid( Encryption.ToTimeLimitedToken( Password ) );
			PasswordOk = Ok;

			var Visibility = "Hidden";

			if( !PasswordOk && ( Password.Trim().Length > 0 ) )
				Visibility = "Visible";
			IsPasswordOk = Visibility;

			return Ok;
		}

		return false;
	}

	[DependsUpon250( nameof( StaffId ) )]
	[DependsUpon( nameof( InEdit ) )]
	public void DoEnableStaffNotes()
	{
		EnableStaffNotes = InEdit && StaffId.IsNotNullOrWhiteSpace();
	}


	public void Execute_AddStaff()
	{
		InAdd  = true;
		InEdit = false;
		Clear();
		SetNewStaffFocus?.Invoke();
	}

	public async Task Execute_Cancel()
	{
		InAdd          = false;
		InEdit         = true;
		AlreadyCreated = false;
		EnableSave     = false;
		IsPasswordOk   = "Hidden";

		if( StaffId.IsNotNullOrWhiteSpace() )
		{
			ReloadOld = true;
			await WhenStaffLookupSummaryChanges();
			Refresh?.Invoke();
		}
		else
			Clear();
	}

	public void Execute_Clear()
	{
		Clear();
	}

	//
	//
	//	Enable save
	//
	[DependsUpon( nameof( InAdd ) )]
	[DependsUpon( nameof( InEdit ) )]
	[DependsUpon( nameof( AlreadyCreated ) )]
	[DependsUpon( nameof( ChangedRole ) )]

	//
	[DependsUpon250( nameof( Enabled ) )] // Delay applies to all
	[DependsUpon( nameof( PasswordOk ) )]
	[DependsUpon( nameof( FirstName ) )]
	[DependsUpon( nameof( LastName ) )]
	[DependsUpon( nameof( MiddleNames ) )]
	[DependsUpon( nameof( Phone ) )]
	[DependsUpon( nameof( Mobile ) )]
	[DependsUpon( nameof( Email ) )]

	//
	[DependsUpon( nameof( Suite ) )]
	[DependsUpon( nameof( Street ) )]
	[DependsUpon( nameof( Street1 ) )]
	[DependsUpon( nameof( City ) )]

	//[DependsUpon( nameof( Region ) )]
	//[DependsUpon( nameof( Country ) )]
	[DependsUpon( nameof( AddressRegion ) )]
	[DependsUpon( nameof( AddressCountry ) )]
	[DependsUpon( nameof( PostalCode ) )]

	//
	[DependsUpon( nameof( EmergencyFirstName ) )]
	[DependsUpon( nameof( EmergencyLastName ) )]
	[DependsUpon( nameof( EmergencyPhone ) )]
	[DependsUpon( nameof( EmergencyMobile ) )]
	[DependsUpon( nameof( EmergencyEmail ) )]

	//
	[DependsUpon( nameof( MainCommission1 ) )]
	[DependsUpon( nameof( MainCommission2 ) )]
	[DependsUpon( nameof( MainCommission3 ) )]

	//
	[DependsUpon( nameof( OverrideCommission1 ) )]
	[DependsUpon( nameof( OverrideCommission2 ) )]
	[DependsUpon( nameof( OverrideCommission3 ) )]
	[DependsUpon( nameof( OverrideCommission4 ) )]
	[DependsUpon( nameof( OverrideCommission5 ) )]
	[DependsUpon( nameof( OverrideCommission6 ) )]
	[DependsUpon( nameof( OverrideCommission7 ) )]
	[DependsUpon( nameof( OverrideCommission8 ) )]
	[DependsUpon( nameof( OverrideCommission9 ) )]
	[DependsUpon( nameof( OverrideCommission10 ) )]
	[DependsUpon( nameof( OverrideCommission11 ) )]
	[DependsUpon( nameof( OverrideCommission12 ) )]
	public void DoEnableSave()
	{
		if( PasswordOk )
		{
			EnableSave = ( InEdit && !StaffId.IsNullOrEmpty() )
						 || ( InAdd && !AlreadyCreated && !NewStaffId.IsNullOrEmpty() && !FirstName.IsNullOrEmpty() && !LastName.IsNullOrEmpty() );

			EnableCancel = EnableSave || InAdd;
		}
		else
		{
			EnableSave   = false;
			EnableCancel = true;
		}
	}

	/// <summary>
	///     Weird behaviour.  StaffId is the new id if the right-hand save button is clickec,
	///     and the old id if the toolbar button is clicked, even though both buttons call
	///     the same method in the code behind.
	/// </summary>
	/// <param
	///     name="id">
	/// </param>
	/// <returns></returns>

	//public async Task Execute_Save()
	public async Task Execute_Save( string id )
	{
		if( !IsInDesignMode )
		{
			EnableSave = false;

			StaffId = id;
			var Sd = BuildStaffMember();

			UpdateStaffMemberWithRolesAndZones UpdateData = new( PROGRAM_NAME, Sd )
															{
																Roles = SelectedRoles
															};

			// Update the roles
			if( StaffMembersRoles.ContainsKey( id ) )
			{
				StaffMembersRoles[ id ].Clear();

				foreach( var Role in SelectedRoles )
					StaffMembersRoles[ id ].Add( Role );

				//await Azure.Client.RequestAddUpdateRole(Role);
				UpdateData.Roles = StaffMembersRoles[ id ];
			}
			//else
			//    UpdateData.Roles = new List<Protocol.Data.Role>();

			await Azure.Client.RequestUpdateStaffMember( UpdateData );

			// If the staffId has changed, treat it as a new StaffMember

			// TODO If the staffId has changed, delete the original 
			if( ( OriginalStaff != null ) && ( OriginalStaff.StaffId != StaffId ) )
			{
				// Need to copy any associated StaffNotes - must be first as they are deleted with the StaffMember
				var StaffNotes = await Azure.Client.RequestGetStaffNotes( OriginalStaff.StaffId );

				if( StaffNotes is not null && ( StaffNotes.Count > 0 ) )
				{
					foreach( var Note in StaffNotes )
					{
						Logging.WriteLogLine( "Copying note " + Note.NoteId + " to StaffMember: " + StaffId );
						await Azure.Client.RequestAddUpdateStaffNote( PROGRAM_NAME, StaffId, Note.NoteId, Note.Text );
					}
				}
				Logging.WriteLogLine( "StaffId has changed - deleting the original" );
				await Azure.Client.RequestDeleteStaffMember( PROGRAM_NAME, OriginalStaff.StaffId );
			}

			//LoadStaff();

			//await Dispatcher.Invoke(async () =>
			Dispatcher.Invoke( () =>
							   {
								   // Updated
								   var Title   = (string)Application.Current.TryFindResource( "StaffUpdatedTitle" );
								   var Message = (string)Application.Current.TryFindResource( "StaffUpdatedMessage" );

								   // MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);

								   Message = Message.Replace( "@1", StaffId );
								   _       = MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );

								   //StaffId = string.Empty;
								   //Password = string.Empty;
								   //PasswordConfirmation = string.Empty;

								   //await LoadStaff();
								   // Add or update staff member locally
								   if( OriginalStaff != null )
								   {
									   // Edit - Roles have already been updated
									   Sd.Password = Password;
									   var Sar = BuildStaffAndRoles( Sd, SelectedRoles );

									   if( OriginalStaff.StaffId != StaffId )
									   {
										   var Found = ( from S in StaffMembers
													     where S.StaffId == OriginalStaff.StaffId
													     select S ).FirstOrDefault();

										   if( Found != null )
										   {
											   Logging.WriteLogLine( "Removing OriginalStaff " + OriginalStaff.StaffId );
											   _ = StaffMembersRoles.Remove( OriginalStaff.StaffId );
											   _ = StaffMembers.Remove( Found );
										   }
									   }
									   else
									   {
										   var Found = ( from S in StaffMembers
													     where S.StaffId == StaffId
													     select S ).FirstOrDefault();

										   if( Found != null )
										   {
											   Logging.WriteLogLine( "Removing Staff " + StaffId );
											   _ = StaffMembersRoles.Remove( StaffId );
											   _ = StaffMembers.Remove( Found );
										   }
									   }

									   // Add back
									   StaffMembers.Add( Sar );
									   StaffMembersRoles.Add( StaffId, SelectedRoles );
								   }
								   else
								   {
									   // Add
									   //bool ok = true;
									   //string was = sd.Password;
									   //(sd.Password, ok) = Encryption.FromTimeLimitedToken(sd.Password);
									   //if (!ok)
									   //                                    {
									   //	Logging.WriteLogLine("DEBUG Failed to decrypt password: " + sd.Password + " setting back to " + was);
									   //	sd.Password = was;
									   //                                    }
									   Sd.Password = Password;
									   StaffMembersRoles.Add( Sd.StaffId, new List<Protocol.Data.Role>() );

									   // Don't forget the Roles
									   foreach( var Role in SelectedRoles )
										   StaffMembersRoles[ id ].Add( Role );
									   var Sar = BuildStaffAndRoles( Sd, SelectedRoles );

									   var Found = ( from S in StaffMembers
												     where S.StaffId == Sd.StaffId
												     select S ).FirstOrDefault();

									   if( Found == null )
										   StaffMembers.Add( Sar );
								   }

								   StaffMembers = ( from S in StaffMembers
											        orderby S.StaffId.ToLower()
											        select S ).ToList();
								   FilteredStaffList = StaffMembers;

								   //StaffAndRoles? yes = (from S in StaffMembers where S.StaffId == "IDStest" select S).FirstOrDefault();
								   //                                 Logging.WriteLogLine("DEBUG IDStest present in StaffMembers: " + (yes != null ? "yes" : "no"));
								   //Logging.WriteLogLine("DEBUG IDStest Roles: " + yes?.Roles.Count);
								   //                                 yes = (from S in FilteredStaffList where S.StaffId == "IDStest" select S).FirstOrDefault();
								   //                                 Logging.WriteLogLine("DEBUG IDStest present in FilteredStaffList: " + (yes != null ? "yes" : "no"));
								   //                                 Logging.WriteLogLine("DEBUG IDStest Roles in FilteredStaffList: " + yes?.Roles.Count);
								   //Logging.WriteLogLine("DEBUG StaffMembersRoles[].Roles.Count: " + StaffMembersRoles[id].Count);

								   StaffData Data = new( StaffMembersRoles, StaffMembers );
								   SaveStaffData( Data );

								   WhenFilterRadioButtonsChange();
								   Clear();
							   } );
			ShowUpdated = true;
		}
	}

	public async Task Execute_Save_V1( string id )
	{
		if( !IsInDesignMode )
		{
			EnableSave = false;

			StaffId = id;

			var UpdateData = new UpdateStaffMemberWithRolesAndZones( PROGRAM_NAME, new Protocol.Data.Staff
																			       {
																				       StaffId  = InAdd ? NewStaffId : StaffId,
																				       Enabled  = Enabled,
																				       Password = Encryption.ToTimeLimitedToken( Password ),

																				       Address = new Address
																						         {
																							         Suite        = Suite,
																							         AddressLine1 = Street,
																							         AddressLine2 = Street1,
																							         City         = City,
																							         Country      = AddressCountry,
																							         PostalCode   = PostalCode,
																							         EmailAddress = Email,
																							         Phone        = Phone,
																							         Mobile       = Mobile,
																							         Region       = AddressRegion
																						         },

																				       EmergencyEmail     = EmergencyEmail,
																				       EmergencyFirstName = EmergencyFirstName,
																				       EmergencyLastName  = EmergencyLastName,
																				       EmergencyMobile    = EmergencyMobile,
																				       EmergencyPhone     = EmergencyPhone,
																				       FirstName          = FirstName,
																				       LastName           = LastName,
																				       MiddleNames        = MiddleNames,

																				       MainCommission1 = MainCommission1,
																				       MainCommission2 = MainCommission2,
																				       MainCommission3 = MainCommission3,

																				       OverrideCommission1  = OverrideCommission1,
																				       OverrideCommission2  = OverrideCommission2,
																				       OverrideCommission3  = OverrideCommission3,
																				       OverrideCommission4  = OverrideCommission4,
																				       OverrideCommission5  = OverrideCommission5,
																				       OverrideCommission6  = OverrideCommission6,
																				       OverrideCommission7  = OverrideCommission7,
																				       OverrideCommission8  = OverrideCommission8,
																				       OverrideCommission9  = OverrideCommission9,
																				       OverrideCommission10 = OverrideCommission10,
																				       OverrideCommission11 = OverrideCommission11,
																				       OverrideCommission12 = OverrideCommission12
																			       }
																   )
							 {
								 Roles = SelectedRoles
							 };

			// Update the roles
			if( StaffMembersRoles.ContainsKey( id ) )
			{
				StaffMembersRoles[ id ].Clear();

				foreach( var Role in SelectedRoles )
					StaffMembersRoles[ id ].Add( Role );

				//await Azure.Client.RequestAddUpdateRole(Role);
				UpdateData.Roles = StaffMembersRoles[ id ];
			}
			else
				UpdateData.Roles = new List<Protocol.Data.Role>();

			await Azure.Client.RequestUpdateStaffMember( UpdateData );

			// TODO If the staffId has changed,
			//		delete the original 
			if( ( OriginalStaff != null ) && ( OriginalStaff.StaffId != StaffId ) )
			{
				// Need to copy any associated StaffNotes - must be first as they are deleted with the StaffMember
				var StaffNotes = await Azure.Client.RequestGetStaffNotes( OriginalStaff.StaffId );

				if( StaffNotes is not null && ( StaffNotes.Count > 0 ) )
				{
					foreach( var Note in StaffNotes )
					{
						Logging.WriteLogLine( "Copying note " + Note.NoteId + " to StaffMember: " + StaffId );
						await Azure.Client.RequestAddUpdateStaffNote( PROGRAM_NAME, StaffId, Note.NoteId, Note.Text );
					}
				}
				Logging.WriteLogLine( "StaffId has changed - deleting the original" );
				await Azure.Client.RequestDeleteStaffMember( PROGRAM_NAME, OriginalStaff.StaffId );
			}

			//LoadStaff();

			await Dispatcher.Invoke( async () =>
								     {
									     // Updated
									     var Title   = (string)Application.Current.TryFindResource( "StaffUpdatedTitle" );
									     var Message = (string)Application.Current.TryFindResource( "StaffUpdatedMessage" );

									     // MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);

									     Message = Message.Replace( "@1", StaffId );
									     MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );

									     StaffId              = string.Empty;
									     Password             = string.Empty;
									     PasswordConfirmation = string.Empty;

									     Clear();

									     await LoadStaff();
								     } );

			ShowUpdated = true;
		}
	}

	public void Execute_Delete()
	{
		if( SelectedStaff is not null )
		{
			SelectedStaff.Enabled = false;
			var Id = SelectedStaff.StaffId;
			Logging.WriteLogLine( "Deleting StaffMember: " + Id );

			if( !IsInDesignMode )
			{
				Task.Run( async () =>
						  {
							  await Azure.Client.RequestDeleteStaffMember( PROGRAM_NAME, Id );
							  //await LoadStaff();
						  } );

				Dispatcher.Invoke( () =>
								   {
									   StaffId              = string.Empty;
									   Password             = string.Empty;
									   PasswordConfirmation = string.Empty;

									   StaffMembersRoles.Remove( Id );

									   var Found = ( from S in StaffMembers
												     where S.StaffId == Id
												     select S ).FirstOrDefault();

									   if( Found != null )
										   StaffMembers.Remove( Found );

									   StaffData Data = new( StaffMembersRoles, StaffMembers );
									   SaveStaffData( Data );

									   Clear();

									   WhenFilterRadioButtonsChange();
									   ApplyFilter( CurrentAdhocFilter );
								   } );
			}
		}
	}

	public List<AuditLine> GetAuditLines()
	{
		var NewItems = new List<AuditLine>();

		if( SelectedStaff is not null )
		{
			var UserId = SelectedStaff.StaffId;

			//if( CurrentTrip != null )
			if( UserId.IsNotNullOrWhiteSpace() )
			{
				Logging.WriteLogLine( "Loading log for " + SelectedStaff.StaffId );

				if( !IsInDesignMode )
				{
					// From LogViewerModel
					var Lookup = new LogLookup
								 {
									 Log      = Log,
									 FromDate = DateTimeOffset.MinValue,
									 ToDate   = DateTimeOffset.Now,

									 //Key      = CurrentTrip.TripId
									 Key = UserId
								 };

					Task.WaitAll( Task.Run( async () =>
										    {
											    try
											    {
												    var Lg = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );
												    Logging.WriteLogLine( "Found " + Lg.Count + " log lines" );

												    //var LogItems = new ObservableCollection<String>();

												    foreach( var Entry in Lg )
												    {
													    Entry.Data = FormatData( Entry.Data );

													    // var L = Log;

													    //LogItems.Add(new DisplayEntry(L, Entry));
														var    Row = Entry.RowKey;
													    var    P   = Row.LastIndexOf( "--", StringComparison.Ordinal );
													    var User = P < 0 ? "????" : Row.Substring( P + 2 );

													    var Operation = Entry.Operation;
													    var Data      = Entry.Data;

													    var Line = new AuditLine( Entry.Timestamp, User, Operation, Data );

													    //Logging.WriteLogLine("Found " + line.Description + ", " + line.ToString());
													    // LogItems.Add(L.ToString());
													    NewItems.Add( Line );
												    }

												    //Dispatcher.Invoke( () => { AuditLines = NewItems; } );

												    //Dispatcher.Invoke(() =>
												    //               {
												    //                   //NothingFound = LogItems.Count == 0;

												    //                   //Items = LogItems;
												    //                   //SearchButtonEnabled = true;
												    //                   //NewItems = LogItems;
												    //               });
											    }
											    catch( Exception Exception )
											    {
												    Console.WriteLine( Exception );
											    }
										    } ) );
				}
			}
		}

		return NewItems;
	}

#region StaffList
	//public          List<Protocol.Data.Staff>                  StaffMembers      = new();
	public List<StaffAndRoles>                                StaffMembers      = new();
	public SortedDictionary<string, List<Protocol.Data.Role>> StaffMembersRoles = new();

	//public List<DisplayStaff> FilteredStaffList
	//public List<Protocol.Data.Staff> FilteredStaffList
	public List<StaffAndRoles> FilteredStaffList
	{
		//get { return Get( () => FilteredStaffList, LoadStaff ); }
		//get { return Get( () => FilteredStaffList, new List<Protocol.Data.Staff>() ); }
		get { return Get( () => FilteredStaffList, new List<StaffAndRoles>() ); }
		set { Set( () => FilteredStaffList, value ); }
	}

	public async Task<List<Protocol.Data.Staff>> LoadStaff()
	{
		//List<DisplayStaff> staff = new List<DisplayStaff>();
		var Staff = new List<Protocol.Data.Staff>();

		if( !IsInDesignMode )
		{
			Logging.WriteLogLine( "DEBUG Starting LoadStaff" );
			var Response = await Azure.Client.RequestGetStaffAndRoles();

			//Logging.WriteLogLine("DEBUG After GetStaff");

			if( Response is not null )
			{
				StaffMembersRoles.Clear();
				List<StaffAndRoles> ToDelete = new();
				ToDelete.AddRange( Response );

				foreach( var St in Response )
				{
					//Logging.WriteLogLine( "Found staff member: " + St.StaffId );

					if( !IsStaffWebAdmin( St ) && ( St.StaffId != Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE ) )
					{
						// Decrypt and store in the staff object
						var (Value, Ok) = Encryption.FromTimeLimitedToken( St.Password );

						if( Ok )
						{
							St.Password = Value;

							//Logging.WriteLogLine("with pwd: " + st.Password);
							//if (St.Roles != null && St.Roles.Count > 0)
							//Logging.WriteLogLine("DEBUG Loading roles for " + St.StaffId);
							//List<Protocol.Data.Role>? found = new();

							//foreach (Protocol.Data.Role? role in St.Roles)
							//    found.Add(role);						
							if( !StaffMembersRoles.ContainsKey( St.StaffId ) )
							{
								//StaffMembersRoles.Add(St.StaffId, found);
								StaffMembersRoles.Add( St.StaffId, St.Roles );
							}
							else
							{
								//StaffMembersRoles[St.StaffId] = found;
								StaffMembersRoles[ St.StaffId ] = St.Roles;
							}
							//if (!StaffMembers.Contains(St))
							//                     {
							//	lock (StaffMembers)
							//                         {
							//		StaffMembers.Add(St);
							//                         }
							//                     }

							lock( ToDelete )
								ToDelete.Remove( St );
						}
					}
					else
					{
						//if( St.StaffId != Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
						//	Logging.WriteLogLine( "Skipping Customer account's admin user " + St.StaffId );
						//else
						//	Logging.WriteLogLine( "Skipping none-dispatched route user " + St.StaffId );

						lock( ToDelete )
							ToDelete.Remove( St );
					}

					if( ToDelete.Count == 0 )
					{
						StaffMembers.Clear();

						foreach( var Sid in StaffMembersRoles.Keys )
						{
							var Sar = ( from S in Response
									    where S.StaffId == Sid
									    select S ).FirstOrDefault();

							if( Sar != null )
								StaffMembers.Add( Sar );
						}

						StaffMembers = ( from S in StaffMembers
									     orderby S.StaffId.ToLower()
									     select S ).ToList();

						StaffData Sd = new( StaffMembersRoles, StaffMembers );

						SaveStaffData( Sd );
					}
				}

				//StaffMembers = (from S in Response
				//				where S.StaffId.IsNotNullOrWhiteSpace()
				//                               orderby S.StaffId.ToLower()
				//                               select S ).ToList();

				Logging.WriteLogLine( "DEBUG StaffMembers has " + StaffMembers.Count + " items" );
				Logging.WriteLogLine( "DEBUG StaffMembersRoles has " + StaffMembersRoles.Count + " items" );

				IsFilterRoleShowAllSelected    = true;
				IsFilterEnabledShowAllSelected = true;

				//ApplyFilter("");
				FilteredStaffList = StaffMembers;

				Clear();

				Loaded = true;
			}
			Loaded = true;

			Logging.WriteLogLine( "DEBUG Done LoadStaff" );
		}

		return Staff;
	}

	private bool IsStaffWebAdmin( StaffAndRoles sar )
	{
		var Yes = false;

		if( sar.Roles.Count > 0 )
		{
			var Role = ( from R in sar.Roles
					     where string.Equals( R.Name, WebAdminRole, StringComparison.CurrentCultureIgnoreCase )
					     select R ).FirstOrDefault();

			// Only exclude if WEB_ADMIN_ROLE is the only one
			if( ( Role != null ) && ( sar.Roles.Count == 1 ) )
				Yes = true;
		}

		return Yes;
	}

	public async Task LoadStaffAndUpdateCurrentCollectionAsync()
	{
		if( !IsInDesignMode )
		{
			Logging.WriteLogLine( "DEBUG Starting LoadStaff" );
			var Response = await Azure.Client.RequestGetStaffAndRoles();

			if( Response != null )
			{
			}
		}
	}

	public SortedDictionary<string, List<Protocol.Data.Role>> LoadRolesForStaff()
	{
		if( !IsInDesignMode )
		{
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   Logging.WriteLogLine( "Loading Roles" );
									   StaffMembersRoles.Clear();

									   foreach( var St in StaffMembers )
									   {
										   var Roles = await Azure.Client.RequestStaffMemberRoles( St.StaffId );

										   if( Roles != null )
										   {
											   var Found = new List<Protocol.Data.Role>();

											   foreach( var Role in Roles )
												   Found.Add( Role );

											   if( !StaffMembersRoles.ContainsKey( St.StaffId ) )
												   StaffMembersRoles.Add( St.StaffId, Found );
											   else
												   StaffMembersRoles[ St.StaffId ] = Found;
										   }
									   }
									   Logging.WriteLogLine( "Finished loading Roles" );
								   } )
						);
		}

		return StaffMembersRoles;
	}

	public Protocol.Data.Staff? SelectedStaff
	{
		get { return Get( () => SelectedStaff, (Protocol.Data.Staff?)null ); }
		set { Set( () => SelectedStaff, value ); }
	}

	[DependsUpon( nameof( SelectedStaff ) )]
	public void WhenSelectedStaffChanges()
	{
		if( SelectedStaff != null )
			Logging.WriteLogLine( "Selected Staff: " + SelectedStaff.StaffId );
	}


	public string SelectedFilterRole
	{
		get { return Get( () => SelectedFilterRole, "" ); }
		set { Set( () => SelectedFilterRole, value ); }
	}


	[DependsUpon( nameof( SelectedFilterRole ) )]
	public void WhenSelectedFilterRoleChanges()
	{
		Logging.WriteLogLine( "Selected filter role: " + SelectedFilterRole );
	}

	private readonly string WebAdminRole = "Web Admin";

	public ObservableCollection<string> AvailableRoles
	{
		get { return Get( () => AvailableRoles, LoadRoles ); }
		set { Set( () => AvailableRoles, value ); }
	}

	private ObservableCollection<string> LoadRoles()
	{
		var Roles = new ObservableCollection<string>();

		if( !IsInDesignMode )
		{
			Task.Run( async () =>
					  {
						  var RolesResponse = await Azure.Client.RequestRoles();

						  if( RolesResponse != null )
						  {
							  var Sl         = new SortedSet<string>();
							  var Collection = new ObservableCollection<string>();

							  foreach( var Role in RolesResponse )
							  {
								  Logging.WriteLogLine( "Found role: " + Role.Name );

								  lock( Sl )
								  {
									  if( Role.Name != WebAdminRole )
										  Sl.Add( Role.Name );
									  else
										  Logging.WriteLogLine( "DEBUG Skipping " + Role.Name );
								  }
							  }

							  foreach( var Role in Sl )
								  Collection.Add( Role );
							  AvailableRoles = Collection;
						  }
					  } );
		}

		return Roles;
	}

	public async Task Execute_LoadStaffMember()
	{
		if( SelectedStaff is not null )
		{
			StaffId  = SelectedStaff.StaffId;
			Password = SelectedStaff.Password;
			//(Password, _) = Encryption.FromTimeLimitedToken(SelectedStaff.Password);

			OriginalStaff = await Azure.Client.RequestGetStaffMember( StaffId );

			if( OriginalStaff is not null )
			{
				Logging.WriteLogLine( "Address: " + OriginalStaff.Address.AddressLine1 + ", " + OriginalStaff.Address.City );

				Enabled = OriginalStaff.Enabled;

				// PasswordConfirmation = SelectedStaff.Password;
				FirstName   = OriginalStaff.FirstName;
				LastName    = OriginalStaff.LastName;
				MiddleNames = OriginalStaff.MiddleNames;

				Suite   = OriginalStaff.Address.Suite;
				Street  = OriginalStaff.Address.AddressLine1;
				Street1 = OriginalStaff.Address.AddressLine2;
				City    = OriginalStaff.Address.City;

				//Region = OriginalStaff.Address.Region;
				//Country = OriginalStaff.Address.Country;
				AddressRegion  = OriginalStaff.Address.Region;
				AddressCountry = OriginalStaff.Address.Country;
				PostalCode     = OriginalStaff.Address.PostalCode;

				Phone  = OriginalStaff.Address.Phone;
				Mobile = OriginalStaff.Address.Mobile;
				Email  = OriginalStaff.Address.EmailAddress;

				EmergencyFirstName = OriginalStaff.EmergencyFirstName;
				EmergencyLastName  = OriginalStaff.EmergencyLastName;
				EmergencyPhone     = OriginalStaff.EmergencyPhone;
				EmergencyMobile    = OriginalStaff.EmergencyMobile;
				EmergencyEmail     = OriginalStaff.EmergencyEmail;

				MainCommission1      = OriginalStaff.MainCommission1;
				MainCommission2      = OriginalStaff.MainCommission2;
				MainCommission3      = OriginalStaff.MainCommission3;
				OverrideCommission1  = OriginalStaff.OverrideCommission1;
				OverrideCommission2  = OriginalStaff.OverrideCommission2;
				OverrideCommission3  = OriginalStaff.OverrideCommission3;
				OverrideCommission4  = OriginalStaff.OverrideCommission4;
				OverrideCommission5  = OriginalStaff.OverrideCommission5;
				OverrideCommission6  = OriginalStaff.OverrideCommission6;
				OverrideCommission7  = OriginalStaff.OverrideCommission7;
				OverrideCommission8  = OriginalStaff.OverrideCommission8;
				OverrideCommission9  = OriginalStaff.OverrideCommission9;
				OverrideCommission10 = OriginalStaff.OverrideCommission10;
				OverrideCommission11 = OriginalStaff.OverrideCommission11;
				OverrideCommission12 = OriginalStaff.OverrideCommission12;

				LoadNotes( StaffId );

				if( !StaffMembersRoles.ContainsKey( StaffId ) )
				{
					Logging.WriteLogLine( "Loading roles for " + StaffId );
					var Roles = await Azure.Client.RequestStaffMemberRoles( StaffId );

					if( Roles != null )
					{
						var Found = new List<Protocol.Data.Role>();

						foreach( var Role in Roles )
							Found.Add( Role );
						StaffMembersRoles.Add( StaffId, Found );
					}
				}

				EnableSave   = true;
				EnableCancel = true;
			}
		}
	}
#endregion

#region StaffList filter
	public bool IsFilterRoleShowAllSelected
	{
		get { return Get( () => IsFilterRoleShowAllSelected, true ); }
		set { Set( () => IsFilterRoleShowAllSelected, value ); }
	}


	public bool IsFilterRoleSelectSelected
	{
		get { return Get( () => IsFilterRoleSelectSelected, false ); }
		set { Set( () => IsFilterRoleSelectSelected, value ); }
	}


	public bool IsFilterEnabledShowAllSelected
	{
		get { return Get( () => IsFilterEnabledShowAllSelected, true ); }
		set { Set( () => IsFilterEnabledShowAllSelected, value ); }
	}


	public bool IsFilterEnabledShowEnabledSelected
	{
		get { return Get( () => IsFilterEnabledShowEnabledSelected, false ); }
		set { Set( () => IsFilterEnabledShowEnabledSelected, value ); }
	}

	public bool IsFilterEnabledShowDisabledSelected
	{
		get { return Get( () => IsFilterEnabledShowDisabledSelected, false ); }
		set { Set( () => IsFilterEnabledShowDisabledSelected, value ); }
	}

	[DependsUpon( nameof( IsFilterRoleShowAllSelected ) )]
	[DependsUpon( nameof( IsFilterRoleSelectSelected ) )]
	[DependsUpon( nameof( IsFilterEnabledShowAllSelected ) )]
	[DependsUpon( nameof( IsFilterEnabledShowEnabledSelected ) )]
	[DependsUpon( nameof( IsFilterEnabledShowDisabledSelected ) )]
	public void WhenFilterRadioButtonsChange()
	{
		Logging.WriteLogLine( "Changing filter" );

		if( IsFilterRoleShowAllSelected )
			SelectedFilterRole = string.Empty;
		else
		{
			if( SelectedFilterRole.IsNullOrWhiteSpace() )
				SelectedFilterRole = AvailableRoles[ 0 ];
		}

		List<StaffAndRoles> Staff = new();

		foreach( var Sm in StaffMembers )
		{
			//if (Sm.StaffId.ToLower() == "idstest")
			//{
			//    Logging.WriteLogLine("DEBUG found IDStest");

			//    //Logging.WriteLogLine("DEBUG IDStest.Roles.Count: " + Sm.Roles.Count);
			//}
			if( TestMatch( Sm ) )
			{
				if( CurrentAdhocFilter.IsNotNullOrWhiteSpace() )
				{
					if( DoesStaffMemberMatchAdhocFilter( Sm ) )
						Staff.Add( Sm );
				}
				else
					Staff.Add( Sm );
			}
		}

		FilteredStaffList = Staff;
	}

	public bool DoesStaffMemberMatchAdhocFilter( StaffAndRoles sar )
	{
		var Yes = false;

		if( sar.StaffId.ToLower().Contains( CurrentAdhocFilter ) || sar.FirstName.ToLower().Contains( CurrentAdhocFilter )
															     || sar.LastName.ToLower().Contains( CurrentAdhocFilter ) )
			Yes = true;

		return Yes;
	}

	public string CurrentAdhocFilter { get; set; } = string.Empty;

	public void ApplyFilter( string filter )
	{
		Logging.WriteLogLine( "Filtering list for " + filter );
		CurrentAdhocFilter = filter;

		List<StaffAndRoles> Staff = new();

		if( filter.IsNotNullOrWhiteSpace() )
		{
			foreach( var Sm in StaffMembers )
			{
				//if( Sm.StaffId.ToLower().Contains( filter ) || Sm.FirstName.ToLower().Contains( filter  ) || Sm.LastName.ToLower().Contains( filter ))
				if( DoesStaffMemberMatchAdhocFilter( Sm ) )
				{
					if( TestMatch( Sm ) )
						Staff.Add( Sm );
				}
			}
		}
		else
			Staff = StaffMembers;

		FilteredStaffList = Staff;
	}

	private bool TestMatch( Protocol.Data.Staff staff )
	{
		var MatchesRole    = false;
		var MatchesEnabled = false;

		if( staff.Enabled )
		{
			if( IsFilterEnabledShowAllSelected || IsFilterEnabledShowEnabledSelected )
				MatchesEnabled = true;
		}
		else
		{
			if( IsFilterEnabledShowAllSelected || IsFilterEnabledShowDisabledSelected )
				MatchesEnabled = true;
		}

		if( IsFilterRoleShowAllSelected )
			MatchesRole = true;
		else
		{
			// What role is selected?
			// What roles does the user have?
			var Sar = ( from S in StaffMembers
					    where S.StaffId == staff.StaffId
					    select S ).FirstOrDefault();

			if( Sar != null )
			{
				foreach( var Role in Sar.Roles )
				{
					if( SelectedFilterRole.IsNotNullOrWhiteSpace() && ( Role.Name == SelectedFilterRole ) )
					{
						MatchesRole = true;

						break;
					}
				}
			}
			else
				Logging.WriteLogLine( "ERROR Can't find " + staff.StaffId + " in StaffMembers" );
			//if( StaffMembersRoles.ContainsKey( staff.StaffId ) )
			//{
			//	foreach( var Role in StaffMembersRoles[ staff.StaffId ] )
			//	{
			//		if( SelectedFilterRole.IsNotNullOrWhiteSpace() && ( Role.Name == SelectedFilterRole ) )
			//		{
			//			MatchesRole = true;

			//			break;
			//		}
			//	}
			//}
		}

		var Matches = MatchesRole && MatchesEnabled;
		//Logging.WriteLogLine( staff.StaffId + " matches: " + Matches );

		return Matches;
	}
#endregion

#region Notes
	public IDictionary<string, Note> Notes
	{
		get { return Get( () => Notes, new Dictionary<string, Note>() ); }
		set { Set( () => Notes, value ); }
	}


	public List<string> NoteNames
	{
		get { return Get( () => NoteNames, new List<string>() ); }
		set { Set( () => NoteNames, value ); }
	}


	public string SelectedNote
	{
		get { return Get( () => SelectedNote, "" ); }
		set { Set( () => SelectedNote, value ); }
	}


	[DependsUpon( nameof( SelectedNote ) )]
	public void WhenSelectedNoteChanges()
	{
		Logging.WriteLogLine( "Displaying note: " + SelectedNote );

		/*
		 * var sm = (from m in Model.StaffMembers
						  where m.StaffId.ToLower() == tbStaffId.Text.Trim().ToLower()
						  select m).First();
		*/
		//var note = (from n in Notes
		//			where n.NoteId == SelectedNote
		//			select n).First();
		if( Notes.ContainsKey( SelectedNote ) )
		{
			var Note               = Notes[ SelectedNote ];
			OldNoteName = NoteName = Note.NoteId;
			OldNoteText = NoteText = Note.Text;
		}
	}


	public string NoteName
	{
		get { return Get( () => NoteName, "" ); }
		set { Set( () => NoteName, value ); }
	}


	public string OldNoteName
	{
		get { return Get( () => OldNoteName, "" ); }
		set { Set( () => OldNoteName, value ); }
	}


	public string OldNoteText
	{
		get { return Get( () => OldNoteText, "" ); }
		set { Set( () => OldNoteText, value ); }
	}


	public string NoteText
	{
		get { return Get( () => NoteText, "" ); }
		set { Set( () => NoteText, value ); }
	}


	public bool IsReadOnly
	{
		get { return Get( () => IsReadOnly, true ); }
		set { Set( () => IsReadOnly, value ); }
	}


	public bool IsEditNote
	{
		get { return Get( () => IsEditNote, false ); }
		set { Set( () => IsEditNote, value ); }
	}


	public bool IsAddNote
	{
		get { return Get( () => IsAddNote, false ); }
		set { Set( () => IsAddNote, value ); }
	}


	public bool IsAddEditDeleteEnabled
	{
		get { return Get( () => IsAddEditDeleteEnabled, true ); }
		set { Set( () => IsAddEditDeleteEnabled, value ); }
	}


	//[DependsUpon(nameof(IsAddEditDeleteEnabled))]
	public bool IsSaveCancelEnabled
	{
		get { return Get( () => IsSaveCancelEnabled, false ); }
		set { Set( () => IsSaveCancelEnabled, value ); }
	}

	public bool DoesNoteNameExist( string name ) => Notes.ContainsKey( name );

	/// <summary>
	///     Has to be InEdit.
	/// </summary>
	/// <returns></returns>
	public bool IsNoteBeingRenamed() => NoteName != OldNoteName;

	public bool HasNoteTextChanged() => NoteText != OldNoteText;

	public void Execute_AddNote()
	{
		Logging.WriteLogLine( "Adding new note" );
		IsAddEditDeleteEnabled = false;
		IsSaveCancelEnabled    = true;
		IsReadOnly             = false;
		IsAddNote              = true;
		IsEditNote             = false;

		NoteName = string.Empty;
		NoteText = string.Empty;
	}

	public void Execute_EditNote()
	{
		Logging.WriteLogLine( "Editing " + SelectedNote );
		IsAddEditDeleteEnabled = false;
		IsSaveCancelEnabled    = true;
		IsReadOnly             = false;
		IsAddNote              = false;
		IsEditNote             = true;
	}

	public async void Execute_RenameNote()
	{
		Logging.WriteLogLine( "Renaming " + OldNoteName + " to " + NoteName );

		await Azure.Client.RequestRenameStaffNote( PROGRAM_NAME, StaffId, OldNoteName, NoteName );

		if( HasNoteTextChanged() )
			Execute_SaveNote();

		IsAddEditDeleteEnabled = true;
		IsSaveCancelEnabled    = false;
		IsReadOnly             = true;
		IsAddNote              = false;
		IsEditNote             = false;

		OldNoteName = NoteName = string.Empty;
		OldNoteText = NoteText = string.Empty;

		Dispatcher.Invoke( () =>
						   {
							   LoadNotes( StaffId );
						   } );
	}

	public async void Execute_DeleteNote()
	{
		Logging.WriteLogLine( "Deleting " + SelectedNote );
		var Name = SelectedNote;

		await Azure.Client.RequestDeleteStaffNote( PROGRAM_NAME, StaffId, Name );

		IsAddEditDeleteEnabled = true;
		IsSaveCancelEnabled    = false;

		OldNoteName = NoteName = string.Empty;
		OldNoteText = NoteText = string.Empty;

		Dispatcher.Invoke( () =>
						   {
							   LoadNotes( StaffId );
						   } );
	}

	/// <summary>
	///     Validation has been done in MaintainStaff.xaml.cs.
	/// </summary>
	public async void Execute_SaveNote()
	{
		Logging.WriteLogLine( "Saving " + NoteName );

		// Making copies so that they aren't overwritten
		var Name = NoteName;
		var Body = NoteText;

		await Azure.Client.RequestAddUpdateStaffNote( PROGRAM_NAME, StaffId, Name, Body );

		IsAddEditDeleteEnabled = true;
		IsSaveCancelEnabled    = false;
		IsReadOnly             = true;
		IsAddNote              = false;
		IsEditNote             = false;

		OldNoteName = NoteName = string.Empty;
		OldNoteText = NoteText = string.Empty;

		Dispatcher.Invoke( () =>
						   {
							   LoadNotes( StaffId );
						   } );
	}

	public void Execute_CancelNote()
	{
		Logging.WriteLogLine( "Canceling " + SelectedNote );
		IsAddEditDeleteEnabled = true;
		IsSaveCancelEnabled    = false;
		IsReadOnly             = true;
		IsAddNote              = false;
		IsEditNote             = false;

		OldNoteName = NoteName = string.Empty;
		OldNoteText = NoteText = string.Empty;

		LoadNotes( StaffId );
	}


	/// <summary>
	/// </summary>
	/// <param
	///     name="staffId">
	/// </param>
	private void LoadNotes( string staffId )
	{
		Logging.WriteLogLine( "Loading notes for " + staffId );

		Task.Run( async () =>
				  {
					  var Dict       = new Dictionary<string, Note>();
					  var List       = new List<string>();
					  var StaffNotes = await Azure.Client.RequestGetStaffNotes( staffId );

					  if( StaffNotes.IsNotNull() )
					  {
						  foreach( var Note in StaffNotes )
						  {
							  Logging.WriteLogLine( "Found note: " + Note.NoteId + ", text: " + Note.Text );
							  Dict.Add( Note.NoteId, Note );
							  List.Add( Note.NoteId );
						  }
					  }

					  Dispatcher.Invoke( () =>
									     {
										     Notes     = Dict;
										     NoteNames = List;
									     } );
				  } );
	}

	public void LoadNote( string noteId )
	{
		Logging.WriteLogLine( "Loading note for " + noteId );

		Dispatcher.Invoke( () =>
						   {
						   } );
	}
#endregion


#region Caching of Staff Data
	public class StaffData
	{
		public SortedDictionary<string, List<Protocol.Data.Role>> StaffMembersRoles { get; set; } = new();
		public List<StaffAndRoles>                                StaffMembers      { get; set; } = new();

		public StaffData( SortedDictionary<string, List<Protocol.Data.Role>>? sdStaffAndRoles, List<StaffAndRoles>? staffMembers )
		{
			if( ( sdStaffAndRoles != null ) && ( sdStaffAndRoles.Count > 0 ) )
				StaffMembersRoles = sdStaffAndRoles;

			if( ( staffMembers != null ) && ( staffMembers.Count > 0 ) )
				StaffMembers = staffMembers;
		}
	}

	private void SaveStaffData( StaffData staffData )
	{
		if( !IsInDesignMode )
		{
			try
			{
				var SerializedObject = JsonConvert.SerializeObject( staffData );

				//Logging.WriteLogLine("DEBUG Saved staff data:\n" + serializedObject);
				SerializedObject = Encryption.Encrypt( SerializedObject );
				var FileName = MakeCompanyDataFileName();
				Logging.WriteLogLine( "Saving staff data to " + FileName );
				File.WriteAllText( FileName, SerializedObject );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to save staff data: " + E );
			}
		}
	}

	private void LoadCachedStaffData()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var FileName = MakeCompanyDataFileName();

				if( File.Exists( FileName ) )
				{
					Logging.WriteLogLine( "Loading staff data from " + FileName );
					var SerializedObject = File.ReadAllText( FileName );
					SerializedObject = Encryption.Decrypt( SerializedObject );
					var Sd = JsonConvert.DeserializeObject<StaffData>( SerializedObject );

					if( Sd != null )
					{
						Dispatcher.Invoke( () =>
										   {
											   if( Sd.StaffMembersRoles.Count > 0 )
												   StaffMembersRoles = Sd.StaffMembersRoles;

											   if( Sd.StaffMembers.Count > 0 )
												   StaffMembers = Sd.StaffMembers;
										   } );
					}
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to load staff data: " + E );

				// Doing this to ensure that this tab will load if the file is corrupted
				Logging.WriteLogLine( "Deleting bad company data file" );
				StaffData Sd = new( null, null );
				SaveStaffData( Sd );
			}
		}
	}

	private string MakeCompanyDataFileName()
	{
		var AppData    = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		var Id = Globals.Company.CarrierId;

		return $"{BaseFolder}\\{Id}-staffdata.json";
	}
#endregion
}

/// <summary>
///     Utility class for displaying in Staff tab list.
/// </summary>
public class DisplayStaff
{
	public DisplayStaff()
	{
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="st">
	/// </param>
	public DisplayStaff( Protocol.Data.Staff st )
	{
		Staff   = st;
		StaffId = st.StaffId;

		if( st.FirstName.IsNotNullOrWhiteSpace() )
			FullName = st.FirstName.Trim() + " ";

		if( st.LastName.IsNotNullOrWhiteSpace() )
			FullName += st.LastName.Trim();
		Logging.WriteLogLine( "Created new DisplayStaff: StaffId: " + StaffId + ", FullName: " + FullName );
	}

	public string FullName = "FullName";

	public Protocol.Data.Staff? Staff;
	public string               StaffId = "StaffId";
}