﻿#nullable enable

using System.Threading;
using System.Windows.Data;
using System.Windows.Input;

namespace ViewModels.Staff.ShiftRates;

public class DisplayShiftToColourConvertor : IValueConverter
{
	private static SolidColorBrush? DefaultRowColor,
	                                DeletedRowColor,
	                                ModifiedRowColor,
	                                NewRowColor;

	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is DisplayShift Shift )
		{
			if( Shift.IsDeleted )
				return DeletedRowColor ??= Globals.Dictionary.AsSolidColorBrush( "DataGridRowDeletedBrush" );

			if( Shift.IsNew )
				return NewRowColor ??= Globals.Dictionary.AsSolidColorBrush( "DataGridRowMNewBrush" );

			if( Shift.IsModified )
				return ModifiedRowColor ??= Globals.Dictionary.AsSolidColorBrush( "DataGridRowModifiedBrush" );

			return DefaultRowColor ??= Globals.Dictionary.AsSolidColorBrush( "DataGridRowBackgroundBrush" );
		}
		return Brushes.Transparent;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class DisplayShift : INotifyPropertyChanged
{
	// ReSharper disable once InconsistentNaming
	public static List<string> DAYS_OF_WEEK => ShiftExtensions.DAY_OF_WEEK_AS_LIST;

	public DisplayShift Self => this;

	public string OriginalName { get; }

	public string Name
	{
		get => _Name;
		set
		{
			if( _Name != value )
			{
				_Name = value;
				Modified();
			}
		}
	}

	public string Formula
	{
		get => _Formula;
		set
		{
			if( _Formula != value )
			{
				_Formula = value;
				Modified();
			}
		}
	}

	public string ServiceLevel
	{
		get => _ServiceLevel;
		set
		{
			if( _ServiceLevel != value )
			{
				_ServiceLevel = value;
				Modified();
			}
		}
	}

	public bool IsDeleted
	{
		get => _IsDeleted;
		set
		{
			if( _IsDeleted != value )
			{
				_IsDeleted = value;
				Modified();
			}
		}
	}

	public bool IsModified { get; private set; }

	public bool Loaded
	{
		get => _Loaded;
		set
		{
			_Loaded = value;
			OnPropertyChanged( nameof( Self ) );
		}
	}

	private static readonly DateTime MinDate = new( 1, 1, 1, 0, 0, 0 ),
	                                 MaxDate = new( 1, 1, 1, 23, 59, 59 );


	private bool _IsDeleted;


	private string _Name         = null!,
	               _Formula      = null!,
	               _ServiceLevel = null!;

	private bool _Loaded;


	public readonly bool IsNew;

	public Action<DisplayShift>? OnChange;

	protected internal void Modified( bool modified = true, [CallerMemberName] string? propertyName = null )
	{
		if( Loaded && OnChange is not null )
		{
			IsModified = modified;
			OnPropertyChanged( nameof( Self ) );
			OnPropertyChanged( propertyName );
		}
	}


	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		if( Loaded && OnChange is not null && PropertyChanged is not null )
		{
			PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
			OnChange( this );
		}
	}

	public DisplayShift( StaffShift s )
	{
		Name         = s.ShiftName;
		OriginalName = s.ShiftName;
		StartDay     = (int)s.StartDay + 1;
		Start        = new DateTime( s.Start.Ticks );
		EndDay       = (int)s.EndDay + 1;
		End          = new DateTime( s.End.Ticks );
		Formula      = s.Formula;
		ServiceLevel = s.ServiceLevel;
	}

	public DisplayShift( StaffShift s, Action<DisplayShift> onChange ) : this( s )
	{
		OnChange = onChange;
	}

	public DisplayShift( DisplayShift s )
	{
		Name         = s.Name;
		OriginalName = s.OriginalName;
		StartDay     = s.StartDay;
		Start        = new DateTime( s.Start.Ticks );
		EndDay       = s.EndDay;
		End          = new DateTime( s.End.Ticks );
		Formula      = s.Formula;
		ServiceLevel = s.ServiceLevel;
		IsModified   = s.IsModified;
		Loaded       = s.Loaded;
		OnChange     = s.OnChange;
	}

	public DisplayShift( Action<DisplayShift> onChange )
	{
		Name         = "";
		OriginalName = "";
		Formula      = "";
		ServiceLevel = "";
		IsNew        = true;
		Start        = MinDate;
		End          = MaxDate;
		OnChange     = onChange; // Must be last
	}

	public event PropertyChangedEventHandler? PropertyChanged;

#region Duration
	public string Duration => $"{ShiftExtensions.Duration( StartDay, Start.TimeOfDay, EndDay, End.TimeOfDay ):dd\\:hh\\:mm\\:ss}";

	private void DurationChanged()
	{
		OnPropertyChanged( nameof( Duration ) );
	}
#endregion

#region Start Period
	private int      _StartDay;
	private DateTime _Start;

	public int StartDay
	{
		get => _StartDay;
		set
		{
			_StartDay = value;
			Modified();
			DurationChanged();
		}
	}

	public DateTime Start
	{
		get => _Start;
		set
		{
			if( _Start != value )
			{
				_Start = value;
				Modified();
				DurationChanged();
			}
		}
	}
#endregion

#region End Period
	private int      _EndDay;
	private DateTime _End;

	public int EndDay
	{
		get => _EndDay;
		set
		{
			_EndDay = value;
			Modified();
			DurationChanged();
		}
	}

	public DateTime End
	{
		get => _End;
		set
		{
			if( _End != value )
			{
				_End = value;
				Modified();
				DurationChanged();
			}
		}
	}
#endregion
}

public class StaffShiftsViewModel : ViewModelBase
{
	private const string HELP_URI = "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1726349317/Staff+Shift+Rates";

	public List<string> ServiceLevels
	{
		get { return Get( () => ServiceLevels, new List<string>() ); }
		set { Set( () => ServiceLevels, value ); }
	}

	private List<DisplayShift> OriginalShifts = new();

	protected override async void OnInitialised()
	{
		base.OnInitialised();
		await Reload();
	}

	private async Task Reload()
	{
		if( !IsInDesignMode )
		{
			ServiceLevels = await Azure.Client.RequestGetServiceLevels();

			var DisplayShifts = ( from S in await Azure.Client.RequestGetStaffShifts()
			                      select new DisplayShift( S, OnEdit ) ).ToList();
			DoCancel( DisplayShifts );

			OriginalShifts = DisplayShifts;
		}
	}

#region Actions
	public Action? OnShowSavedDialogue;

	public ICommand Save => Commands[ nameof( Save ) ];

	public async void Execute_Save()
	{
		var Updates = new StaffShiftsUpdate();

		Updates.AddRange( from S in Shifts
		                  where S.IsDeleted || S.IsNew || S.IsModified
		                  select new StaffShiftUpdate
		                         {
			                         ShiftName    = S.Name,
			                         OriginalName = S.OriginalName,
			                         Delete       = S.IsDeleted,
			                         Formula      = S.Formula,
			                         StartDay     = (DAY_OF_WEEK)( S.StartDay - 1 ),
			                         Start        = S.Start.TimeOfDay,
			                         EndDay       = (DAY_OF_WEEK)( S.EndDay - 1 ),
			                         End          = S.End.TimeOfDay,
			                         ServiceLevel = S.ServiceLevel
		                         } );

		await Azure.Client.RequestUpdateStaffShifts( Updates );
		Thread.Sleep( 500 );
		await Reload();
		Execute_Cancel();
		OnShowSavedDialogue?.Invoke();
	}

	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public void DoCancel( List<DisplayShift> shifts )
	{
		Shifts = new ObservableCollection<DisplayShift>( from Shift in shifts
		                                                 select new DisplayShift( Shift ) );
		EnableAdd    = true;
		EnableCancel = false;
		EnableSave   = false;
	}

	public void Execute_Cancel()
	{
		DoCancel( OriginalShifts );
	}

	public ICommand Help => Commands[ nameof( Help ) ];

	public void Execute_Help()
	{
		Globals.Uri.Execute( HELP_URI );
	}
#endregion

#region Formula
	public string Formula
	{
		get { return Get( () => Formula, "" ); }
		set { Set( () => Formula, value ); }
	}

	[DependsUpon250( nameof( Formula ) )]
	public void WhenFormulaChanges()
	{
		var Item = SelectedItem;

		if( Item is not null )
			Item.Formula = Formula;
	}
#endregion


#region Buttons
	public bool EnableSave
	{
		get { return Get( () => EnableSave, false ); }
		set { Set( () => EnableSave, value ); }
	}


	public bool EnableCancel
	{
		get { return Get( () => EnableCancel, false ); }
		set { Set( () => EnableCancel, value ); }
	}


	public bool EnableAdd
	{
		get { return Get( () => EnableAdd, true ); }
		set { Set( () => EnableAdd, value ); }
	}

	public bool EnableDelete
	{
		get { return Get( () => EnableDelete, true ); }
		set { Set( () => EnableDelete, value ); }
	}

	[DependsUpon( nameof( Shifts ) )]
	[DependsUpon( nameof( EnableAdd ) )]
	public void WhenShiftsOrAddChanges()
	{
		EnableDelete = Shifts.Count > 0;
	}
#endregion

#region Shifts
	private void OnEdit( DisplayShift item )
	{
		EnableCancel = true;
		EnableSave   = true;
	}


	public ObservableCollection<DisplayShift> Shifts
	{
		get { return Get( () => Shifts, new ObservableCollection<DisplayShift>() ); }
		set { Set( () => Shifts, value ); }
	}

	[DependsUpon350( nameof( Shifts ) )]
	public void WhenShiftsChange()
	{
		foreach( var Shift in Shifts )
			Shift.Loaded = true;
	}

	public ICommand NewShift => Commands[ nameof( NewShift ) ];

	public void Execute_NewShift()
	{
		var Temp = new DisplayShift( OnEdit )
		           {
			           Loaded = true
		           };
		Shifts.Add( Temp );
		SelectedItem = Temp;
		EnableAdd    = false;
	}

	public ICommand DeleteShift => Commands[ nameof( DeleteShift ) ];

	public void Execute_DeleteShift()
	{
		var Item = SelectedItem;

		if( Item is not null )
			Item.IsDeleted = true;
	}

	public DisplayShift? SelectedItem
	{
		get { return Get( () => SelectedItem, (DisplayShift?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ) )]
	public void WhenSelectedItemChanges()
	{
		var Item = SelectedItem;

		if( Item is not null )
			Formula = Item.Formula;
	}
#endregion
}