﻿#nullable enable

using System.Windows.Controls;

namespace ViewModels.Staff.ShiftRates.ShiftRates;

/// <summary>
///     Interaction logic for ShiftRates.xaml
/// </summary>
public partial class ShiftRates : Page
{
	public ShiftRates()
	{
		InitializeComponent();

		if( Root.DataContext is StaffShiftsViewModel Model )
		{
			Model.OnShowSavedDialogue = () =>
			                            {
				                            Globals.Dialogues.Inform.Show( "ShiftSaved" );
			                            };
		}
	}
}