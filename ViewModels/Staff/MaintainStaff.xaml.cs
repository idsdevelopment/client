﻿#nullable enable

using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ViewModels.Staff.ShiftRates;

/// <summary>
///     Interaction logic for MaintainStaff.xaml
/// </summary>
public partial class MaintainStaff : Page
{
	private readonly MaintainStaffViewModel Model;

	public MaintainStaff()
	{
		//Logging.WriteLogLine("DEBUG ");
		Mouse.OverrideCursor = Cursors.Wait;

		Initialized += ( _, _ ) =>
		               {
			               if( DataContext is MaintainStaffViewModel M )
			               {
				               //M.Loaded = false;

				               M.SetNewStaffFocus = () =>
				                                    {
					                                    tbStaffId.Focus();
				                                    };

				               M.Refresh = () =>
				                           {
					                           RoleListBox.Refresh();
				                           };
			               }
		               };

		InitializeComponent();

		Model = (MaintainStaffViewModel)DataContext;

		//Model.ApplyFilter("");

		//Model.Loaded = true;

		rbStaffListShowAll_Checked( null, null );
		rbStaffListShowEnabledAll_Checked( null, null );
		rbStaffListShowAll.IsChecked        = true;
		rbStaffListShowEnabledAll.IsChecked = true;

		Mouse.OverrideCursor = Cursors.Arrow;

		//Logging.WriteLogLine("DEBUG ");
	}

	private void BtnHelp_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening " + Model.HelpUri );
		var Uri = new Uri( Model.HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void rbStaffListShowAll_Checked( object? sender, RoutedEventArgs? e )
	{
		Model.IsFilterRoleShowAllSelected = true;
		Model.IsFilterRoleSelectSelected  = false;
	}

	private void rbStaffListShowAll_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MaintainStaffViewModel Model)
		//{
		//	Model.IsFilterRoleShowAllSelected = false;
		//	Model.IsFilterRoleSelectSelected = true;
		//}
	}

	private void StaffListShowSelectRole_Checked( object sender, RoutedEventArgs e )
	{
		// Check to see if the roles have been loaded
		//if( Model.StaffMembers.Count > Model.StaffMembersRoles.Count )
		//{
		//	var Dlg = new DialogLoadingRoles
		//	          {
		//		          Owner = Application.Current.MainWindow
		//	          };
		//	Dlg.Show(); // Non-modal

		//	var Smr = Model.LoadRolesForStaff();
		//	Logging.WriteLogLine( "Loaded roles for " + Smr.Count + " staff members" );

		//	Dlg.Close();
		//}

		Model.IsFilterRoleShowAllSelected = false;
		Model.IsFilterRoleSelectSelected  = true;
	}

	private void rbStaffListShowSelectRole_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MaintainStaffViewModel Model)
		//{
		//	Model.IsFilterRoleShowAllSelected = true;
		//	Model.IsFilterRoleSelectSelected = false;
		//}
	}

	private void rbStaffListShowEnabledAll_Checked( object? sender, RoutedEventArgs? e )
	{
		Model.IsFilterEnabledShowAllSelected      = true;
		Model.IsFilterEnabledShowEnabledSelected  = false;
		Model.IsFilterEnabledShowDisabledSelected = false;
	}


	private void rbStaffListShowEnabledAll_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MaintainStaffViewModel Model)
		//{
		//	Model.IsFilterEnabledShowAllSelected = false;
		//}
	}


	private void rbStaffListShowEnabledEnabled_Checked( object sender, RoutedEventArgs e )
	{
		Model.IsFilterEnabledShowEnabledSelected  = true;
		Model.IsFilterEnabledShowAllSelected      = false;
		Model.IsFilterEnabledShowDisabledSelected = false;
	}


	private void rbStaffListShowEnabledEnabled_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MaintainStaffViewModel Model)
		//{
		//	Model.IsFilterEnabledShowEnabledSelected = false;
		//}
	}


	private void rbStaffListShowEnabledDisabled_Checked( object sender, RoutedEventArgs e )
	{
		Model.IsFilterEnabledShowDisabledSelected = true;
		Model.IsFilterEnabledShowAllSelected      = false;
		Model.IsFilterEnabledShowEnabledSelected  = false;
	}


	private void rbStaffListShowEnabledDisabled_Unchecked( object sender, RoutedEventArgs e )
	{
		//if (DataContext is MaintainStaffViewModel Model)
		//{
		//	Model.IsFilterEnabledShowDisabledSelected = false;
		//}
	}

	private void tbStaffListFilter_KeyUp( object sender, KeyEventArgs e )
	{
		Model.ApplyFilter( tbStaffListFilter.Text.Trim() );
	}

	private void tbStaffListFilter_LostFocus( object sender, RoutedEventArgs e )
	{
		Model.ApplyFilter( tbStaffListFilter.Text.Trim().ToLower() );
	}

	private void btnStaffEdit_Click( object sender, RoutedEventArgs e )
	{
		lvStaff_MouseDoubleClick( null, null );
	}

	private void btnDeleteStaff_Click( object sender, RoutedEventArgs e )
	{
		var Caption = (string)Application.Current.TryFindResource( "StaffDeleteConfirmationTitle" );
		var Message = (string)Application.Current.TryFindResource( "StaffDeleteConfirmation" );

		if( Model.SelectedStaff is not null )
		{
			var SelectedStaffStaffId = $"{Model.StaffId} ({Model.SelectedStaff.StaffId})";
			Message = Message.Replace( "@1", SelectedStaffStaffId );
			var Mbr = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

			if( Mbr == MessageBoxResult.Yes )
			{
				Logging.WriteLogLine( "Deleting name: " + SelectedStaffStaffId );
				Model.Execute_Delete();

				Caption = (string)Application.Current.TryFindResource( "StaffDeletedTitle" );
				Message = (string)Application.Current.TryFindResource( "StaffDeleted" );

				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
			}
		}
	}

	private async void lvStaff_MouseDoubleClick( object? sender, MouseButtonEventArgs? e )
	{
		await Model.Execute_LoadStaffMember();
		pbStaffPassword.Password = Model.Password;

		//var (Value, Ok) = Encryption.FromTimeLimitedToken(Model.Password);
		//if (Ok)
		//{
		//	pbStaffPassword.Password = Value;
		//	//tbStaffPassword.Text = Model.Password.ToString();
		//	pbStaffPasswordConfirmation.Password = Value;
		//	//tbStaffPasswordConfirmation.Text = Model.Password.ToString();
		//}

		// TODO Testing to see if i can get the audit trail
		//var lines = Model.GetAuditLines();
	}

	private void cbRoles_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		Model.WhenFilterRadioButtonsChange();
	}

	private void imageRevealPassword_MouseDown( object sender, MouseButtonEventArgs e )
	{
		imageRevealPassword.Source = new BitmapImage( new Uri( "eye-open-24.png", UriKind.RelativeOrAbsolute ) );
		tbStaffPassword.Text       = pbStaffPassword.Password;
		pbStaffPassword.Visibility = Visibility.Hidden;
		tbStaffPassword.Visibility = Visibility.Visible;

		tbStaffPasswordConfirmation.Text       = pbStaffPasswordConfirmation.Password;
		pbStaffPasswordConfirmation.Visibility = Visibility.Hidden;
		tbStaffPasswordConfirmation.Visibility = Visibility.Visible;
	}

	private void imageRevealPassword_MouseLeave( object? sender, MouseEventArgs? e )
	{
		imageRevealPassword.Source = new BitmapImage( new Uri( "eye-closed-24.png", UriKind.RelativeOrAbsolute ) );
		tbStaffPassword.Text       = pbStaffPassword.Password;
		pbStaffPassword.Visibility = Visibility.Visible;
		tbStaffPassword.Visibility = Visibility.Hidden;

		tbStaffPasswordConfirmation.Text       = pbStaffPasswordConfirmation.Password;
		pbStaffPasswordConfirmation.Visibility = Visibility.Visible;
		tbStaffPasswordConfirmation.Visibility = Visibility.Hidden;
	}

	private void imageRevealPassword_MouseUp( object sender, MouseButtonEventArgs e )
	{
		imageRevealPassword_MouseLeave( null, null );
	}

	private void pbStaffPassword_PasswordChanged( object sender, RoutedEventArgs e )
	{
		Model.Password = pbStaffPassword.Password;
	}

	private void pbStaffPasswordConfirmation_PasswordChanged( object sender, RoutedEventArgs e )
	{
		Model.PasswordConfirmation = pbStaffPasswordConfirmation.Password;
	}

	private void btnCancel1_Click( object sender, RoutedEventArgs e )
	{
		btnCancel_Click( sender, e );
	}

	private string ValidateStaffMember()
	{
		var Errors = new StringBuilder();

		var CheckPwConf      = false;
		var DuplicateStaffId = false;

		//string newId = tbStaffId.Text.TrimToLower();
		var NewId = tbStaffId.Text.Trim();

		if( ( Model.SelectedStaff == null ) || Model.SelectedStaff.StaffId.IsNullOrEmpty() )
			CheckPwConf = true;
		else if( Model.InEdit )
		{
			// Check to see if the password has changed - if yes, need confirmation
			//var sm = (from m in Model.StaffMembers
			//		  where m.StaffId.ToLower() == tbStaffId.Text.Trim().ToLower()
			//		  select m).First();
			if( Model.StaffMembers is not null && ( Model.StaffMembers.Count > 0 ) )
			{
				foreach( var Sm in Model.StaffMembers )
				{
					if( Sm.StaffId.TrimToLower() == NewId.ToLower() )
					{
						// Is the staffid changing?
						if( ( Model.OriginalStaff != null ) && ( Model.OriginalStaff.StaffId.Trim() != NewId.Trim() ) )
						{
							Logging.WriteLogLine( "Changing staffId from " + Model.OriginalStaff.StaffId.Trim() + " to " + NewId.Trim() );
							DuplicateStaffId = true;
							break;
						}

						if( pbStaffPassword.Password.Trim() != Sm.Password )
						{
							Logging.WriteLogLine( "Editing: password has changed" );
							CheckPwConf = true;
							break;
						}
					}
				}
			}
		}

		if( tbStaffId.Text.IsNullOrWhiteSpace() )
		{
			Errors.Append( (string)Application.Current.TryFindResource( "StaffValidationStaffIdEmpty" ) ).Append( "\r\n" );
			tbStaffId.Background = Brushes.Yellow;
		}

		if( DuplicateStaffId )
		{
			var Message = (string)Application.Current.TryFindResource( "StaffValidationDuplicateStaffId" );
			Message = Message.Replace( "@1", Model.OriginalStaff?.StaffId );
			Message = Message.Replace( "@2", NewId );
			Errors.Append( Message ).Append( "\r\n" );
			tbStaffId.Background = Brushes.Yellow;
		}

		if( CheckPwConf )
		{
			if( pbStaffPassword.Password.IsNullOrWhiteSpace() )
			{
				Errors.Append( (string)Application.Current.TryFindResource( "StaffValidationPasswordEmpty" ) ).Append( "\r\n" );
				pbStaffPassword.Background = Brushes.Yellow;
			}

			if( pbStaffPasswordConfirmation.Password.IsNullOrWhiteSpace() )
			{
				Errors.Append( (string)Application.Current.TryFindResource( "StaffValidationPasswordConfirmationEmpty" ) ).Append( "\r\n" );
				pbStaffPasswordConfirmation.Background = Brushes.Yellow;
			}

			if( pbStaffPassword.Password.IsNotNullOrWhiteSpace() && pbStaffPasswordConfirmation.Password.IsNotNullOrWhiteSpace() )
			{
				if( pbStaffPassword.Password.Trim() != pbStaffPasswordConfirmation.Password.Trim() )
				{
					Errors.Append( (string)Application.Current.TryFindResource( "StaffValidationPasswordAndPasswordConfirmationDontMatch" ) ).Append( "\r\n" );
					pbStaffPassword.Background             = Brushes.Yellow;
					pbStaffPasswordConfirmation.Background = Brushes.Yellow;
				}
			}
		}

		return Errors.ToString();
	}

	private void ClearErrors()
	{
		tbStaffId.Background                   = Brushes.White;
		pbStaffPassword.Background             = Brushes.White;
		pbStaffPasswordConfirmation.Background = Brushes.White;
	}

#region Cancel
	private void btnCancel_Click( object sender, RoutedEventArgs e )
	{
		Model.Execute_Clear();
		tbStaffId.Text                       = "";
		pbStaffPassword.Password             = "";
		pbStaffPasswordConfirmation.Password = "";

		ClearErrors();

		// Seems to be necessary to ensure that the SelectedRoles are cleared
		Model.StaffId = string.Empty;

		//Model.StaffId = null;

		Model.IsPasswordOk = "Hidden";

		Model.Execute_Clear();

		//if (RoleListBox.DataContext is RoleListBoxModel rlbm)
		//            {
		//	rlbm.Items.Clear();
		//            }
	}
#endregion

#region Save
	private async void btnSave1_Click( object sender, RoutedEventArgs e )
	{
		var Errors = ValidateStaffMember();

		if( Errors.IsNullOrWhiteSpace() )
		{
			// Seems to be necessary to ensure that the SelectedRoles are cleared
			// Model.StaffId = string.Empty;
			await Model.Execute_Save( tbStaffId.Text.Trim() );

			ClearErrors();
			tbStaffId.Text                       = "";
			pbStaffPassword.Password             = "";
			pbStaffPasswordConfirmation.Password = "";
		}
		else
		{
			Logging.WriteLogLine( "Errors found: " + Errors );

			var Caption = (string)Application.Current.TryFindResource( "StaffValidationTitle" );

			var Message = (string)Application.Current.TryFindResource( "StaffValidationMessage" ) + "\r\n"
			                                                                                      + (string)Application.Current.TryFindResource( "StaffValidationMessage1" ) + "\r\n"
			                                                                                      + Errors;

			MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
		}
	}
#endregion

#region Notes
	private void btnAddNote_Click( object sender, RoutedEventArgs e )
	{
	}

	private void btnEditNote_Click( object sender, RoutedEventArgs e )
	{
	}

	private void btnDeleteNote_Click( object sender, RoutedEventArgs e )
	{
		var Caption = (string)Application.Current.TryFindResource( "StaffNotesMessageDeletingTitle" );
		var Message = (string)Application.Current.TryFindResource( "StaffNotesMessageDeleting" );
		var Mbr     = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

		if( Mbr == MessageBoxResult.Yes )
		{
			Model.Execute_DeleteNote();

			Caption = (string)Application.Current.TryFindResource( "StaffNotesMessageDeletedTitle" );
			Message = (string)Application.Current.TryFindResource( "StaffNotesMessageDeleted" );
			MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
		}
		else
			Logging.WriteLogLine( "Deletion cancelled" );
	}

	private void lbNoteNames_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
	}

	private void lbNoteNames_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e )
	{
	}

	private void btnSaveNote_Click( object sender, RoutedEventArgs e )
	{
		var Errors  = false;
		var Caption = (string)Application.Current.TryFindResource( "StaffNotesValidationDuplicateNameTitle" );
		var Message = string.Empty;

		var NoteName = tbNoteName.Text.Trim();

		// string noteText = tbNoteText.Text.Trim();

		if( tbNoteName.Text.IsNotNullOrWhiteSpace() )
		{
			if( Model.IsAddNote )
			{
				if( Model.DoesNoteNameExist( NoteName ) )
				{
					Message = (string)Application.Current.TryFindResource( "StaffNotesValidationDuplicateName" );
					Message = Message.Replace( "@1", NoteName );
					Errors  = true;
					tbNoteName.Focus();
				}
			}
		}
		else
		{
			Message = (string)Application.Current.TryFindResource( "StaffNotesValidationEmptyName" );
			Errors  = true;
			tbNoteName.Focus();
		}

		if( !Errors )
		{
			// Check for renaming
			if( Model.IsNoteBeingRenamed() )
				Model.Execute_RenameNote();
			else
				Model.Execute_SaveNote();

			Caption = (string)Application.Current.TryFindResource( "StaffNotesMessageSavedTitle" );
			Message = (string)Application.Current.TryFindResource( "StaffNotesMessageSaved" );
			MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
		}
		else
			MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
	}

	private void btnCancelNote_Click( object sender, RoutedEventArgs e )
	{
	}
#endregion
}