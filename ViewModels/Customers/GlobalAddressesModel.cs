﻿#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using ViewModels.Trips.TripEntry;
using File = System.IO.File;

namespace ViewModels.Customers
{
    public class GlobalAddressesModel : ViewModelBase
    {
        public static string GlobalAddressBookId = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;

        private static GlobalAddressesModel? instance = null;
        private static long instanceCounter = 0;
        // Holding references to make updates easier
        private readonly static List<object> Callers = new();

        private GlobalAddressesModel() 
        {
            Logging.WriteLogLine("Creating GlobalAddressModel");
            Task.WaitAll(
                Task.Run(async() =>
                {
                    LoadCachedCompanyData();
                    GetCompaniesAndUpdateCurrentCollectionAsync();
                    await GetPreferences();
                })
            );

            Task.WaitAll(
                Task.Run(async () =>
                {
                    await LoadGlobalAddressBook();
                })
            );

            if (!IsUpdateGlobalAddressesTimerRunning())
            {
                StartUpdateGlobalAddressesTimer();
                UpdateGlobalAddresses_Tick(null, null);

            }

            // Try cache
            LoadCachedCompanyAddressData();
            // Run to ensure that latest data are loaded
            Worker.DoWork += Worker_DoWorkLoadAddressesForCompanies;
            Worker.RunWorkerCompleted += Worker_Completed;
            Worker.RunWorkerAsync();
        }



        public static GlobalAddressesModel GetInstance(object caller)
        {
            ++instanceCounter;
            Logging.WriteLogLine($"DEBUG instanceCounter: {instanceCounter} Caller: '{caller}'");
            if (!Callers.Contains(caller))
            {
                if (caller is TripEntryModel model)
                {
                    Logging.WriteLogLine($"Caller is TripEntryModel with a GUID of '{model.GUID}'");
                }
                Callers.Add(caller);
            }
            Logging.WriteLogLine($"DEBUG Callers.Count: {Callers.Count}");

            if (instance == null)
            {
                instance = new GlobalAddressesModel();
            }

            return instance;
        }

        #region Data
                
        public SortedDictionary<string, CompanyByAccountSummary> GlobalAddresses
        {
            get => Get(() => GlobalAddresses, new SortedDictionary<string, CompanyByAccountSummary>());
            set => Set(() => GlobalAddresses, value);
        }

        public SortedDictionary<string, CompanyDetail> GlobalDetailedAddresses
        {
            get => Get(() => GlobalDetailedAddresses, new SortedDictionary<string, CompanyDetail>()); 
            set => Set(() => GlobalDetailedAddresses, value);
        }

        // Used to hold these details to prevent having to load everything again.
        // Required for PML because all addresses are in the PMLOz account, not the global address book.
        public Dictionary<string, CompanySummaryList> DictCompanySummaryListsByAccountId { get; set; } = new();
        public Dictionary<string, CompanyDetailList> DictCompanyDetailListsByAccountId { get; set; } = new();

        public IList CompanyNames
        {
            get { return Get(() => CompanyNames, new List<string>()); }
            set { Set(() => CompanyNames, value); }
        }


        public List<CompanyAddress> Companies
        {
            get { return Get(() => Companies, new List<CompanyAddress>()); }
            set { Set(() => Companies, value); }
        }

        public SortedDictionary<string, CompanyAddress> CompanyFullNames
        {
            get { return Get(() => CompanyFullNames, new SortedDictionary<string, CompanyAddress>(StringComparer.OrdinalIgnoreCase)); }
            set { Set(() => CompanyFullNames, value); }
        }

        #endregion

        #region Public Actions

        // TODO Need to check TripEntryModel for places where this should be called
        public void UpdateGlobalAddresses(object caller, SortedDictionary<string, CompanyByAccountSummary> cas, SortedDictionary<string, CompanyDetail> cds)
        {
            Logging.WriteLogLine($"Updating {cas.Count} global addresses");
            GlobalAddresses = cas;
            GlobalDetailedAddresses = cds;

            UpdateCallersWithNewGlobalAddressBook(caller);
        }

        #endregion

        #region Private Actions
        private async Task LoadGlobalAddressBook()
        {
            SortedDictionary<string, CompanyByAccountSummary> addressBook = new();
            var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
            var loaded = false;
                        

            if (!loaded)
            {
                Logging.WriteLogLine("Loading global addresses from server");

                var Tasks = new Task[]
                            {
                            Azure.Client.RequestGetCustomerCompaniesSummary( GlobalAddressBookId ),
                            Azure.Client.RequestGetCustomerCompaniesDetailed( GlobalAddressBookId )
                            };
                await Task.WhenAll(Tasks);

                var csl = ((Task<CompanySummaryList>)Tasks[0]).Result;
                var cdl = ((Task<CompanyDetailList>)Tasks[1]).Result;

                if (cdl is not null)
                {
                    // Already created and populated
                    foreach (var Detail in cdl)
                    {
                        // Don't include the artifacts of the global address book
                        if (Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && (Detail.Company.CompanyNumber != GlobalAddressBookId)
                                                                                         && (Detail.Company.CompanyName != GlobalAddressBookId) 
                                                                                         && !GlobalDetailedAddresses.ContainsKey(Detail.Company.CompanyName.ToLower()))
                            GlobalDetailedAddresses.Add(Detail.Company.CompanyName.ToLower(), Detail);
                    }
                }

                if (csl is not null)
                {
                    foreach (var Cs in csl)
                    {
                        //Logging.WriteLogLine("DEBUG Looking at Cs: " + Cs.CompanyName);
                        if (DoesCustomerCompaniesDetailedContainCompanySummaryListItem(cdl, Cs))
                        {
                            // Don't include the artifacts of the global address book
                            if (Cs.CompanyName.IsNotNullOrWhiteSpace() && (Cs.CompanyName != GlobalAddressBookId) && !addressBook.ContainsKey(Cs.CompanyName.ToLower()))
                                addressBook.Add(Cs.CompanyName.ToLower(), Cs);
                        }
                    }
                }

                if ((addressBook.Keys.Count == 1) && (cdl != null))
                {
                    foreach (var detail in cdl)
                    {
                        CompanyByAccountSummary cbas = new()
                        {
                            AddressLine1 = detail.Address.AddressLine1,
                            AddressLine2 = detail.Address.AddressLine2,
                            City = detail.Address.City,
                            CompanyName = detail.Address.CompanyName,
                            CountryCode = detail.Address.CountryCode,
                            CustomerCode = detail.Address.CompanyNumber,
                            DisplayCompanyName = detail.Address.CompanyName,
                            Enabled = detail.Company.Enabled,
                            Found = false, // TODO
                            LocationBarcode = detail.Address.LocationBarcode,
                            PostalCode = detail.Address.PostalCode,
                            Region = detail.Address.Region,
                            Suite = detail.Address.Suite
                        };

                        if (!addressBook.ContainsKey(cbas.CompanyName.ToLower()))
                            addressBook.Add(cbas.CompanyName.ToLower(), cbas);
                    }
                }

                Logging.WriteLogLine("GlobalAddressBook has " + GlobalAddresses.Count + " addresses");

                GlobalAddresses = addressBook;
            }
        }

        private static bool DoesCustomerCompaniesDetailedContainCompanySummaryListItem(CompanyDetailList? cdl, CompanyByAccountAndCompanyName cbas)
        {
            var Yes = false;

            if (cdl is not null)
            {
                foreach (var Cd in cdl)
                {
                    if (Cd.Company.CompanyNumber == cbas.CustomerCode)
                    {
                        Yes = true;
                        break;
                    }
                }
            }
            return Yes;
        }

        #endregion

        #region Preferences and Defaults
        public Preferences Prefs
        {
            get => Get(() => Prefs, new Preferences());
            set => Set(() => Prefs, value);
        }
    
        private async Task GetPreferences()
        {
            if (!IsInDesignMode)
            {
                Logging.WriteLogLine("Getting Preferences");
                Prefs = await Azure.Client.RequestPreferences();

                Logging.WriteLogLine("Found " + Prefs?.Count + " preferences;");

                // 
                var pref = (from P in Prefs
                            where P.Description.Contains("Reload Global Address Book from Server period (default 120 mins)")
                            select P).FirstOrDefault();
                if (pref != null || pref?.IntValue == 0)
                {
                    TimerInterval = pref.IntValue;
                }
                else
                {
                    TimerInterval = 120;
                }
            }
        }
        #endregion

        #region Global Address Timer
        private DispatcherTimer? UpdateGlobalAddressesTimer;

        private int TimerInterval = 120;

        public void StartUpdateGlobalAddressesTimer()
        {
            Logging.WriteLogLine("Starting UpdateGlobalAddressesTimer...");
            UpdateGlobalAddressesTimer = new DispatcherTimer();
            UpdateGlobalAddressesTimer.Tick += UpdateGlobalAddresses_Tick;
            UpdateGlobalAddressesTimer.Interval = new TimeSpan(0, TimerInterval, 0);
            UpdateGlobalAddressesTimer.Start();
        }

        public void StopUpdateGlobalAddressesTimer()
        {
            Logging.WriteLogLine("Stopping UpdateGlobalAddressesTimer...");
            UpdateGlobalAddressesTimer?.Stop();
        }

        private void UpdateGlobalAddresses_Tick(object? sender, EventArgs? e)
        {
            Logging.WriteLogLine("Reloading the Global Address Book...");
            Task.Run(async () =>
                      {
                          await LoadGlobalAddressBook();
                          Logging.WriteLogLine("Reloading the Global Address Book - Done");
                          UpdateCallersWithNewGlobalAddressBook();
                      });
        }

        private void UpdateCallersWithNewGlobalAddressBook(object? caller = null)
        {
            if (Callers.Count > 0)
            {
                foreach (var registered in Callers)
                {
                    if (registered != null && registered != caller && registered is TripEntryModel tm)
                    {
                        Logging.WriteLogLine("Updating TripEntryModel");
                        tm.GlobalAddresses = GlobalAddresses;
                        tm.GlobalDetailedAddresses = GlobalDetailedAddresses;
                    }
                }
            }
        }

        public bool IsUpdateGlobalAddressesTimerRunning() => UpdateGlobalAddressesTimer is { IsEnabled: true };
        #endregion

        #region Caching of Company Addresses
        public class CompanyAddressData
        {
            public Dictionary<string, CompanySummaryList> DictCompanySummarysList { get; set; } = new();
            public Dictionary<string, CompanyDetailList> DictCompanyDetailsList { get; set; } = new();
        }

        private void SaveCompanyAddressData(CompanyAddressData data)
        {
            if (!IsInDesignMode)
            {
                try
                {
                    var serializedObject = JsonConvert.SerializeObject(data);

                    //Logging.WriteLogLine( "DEBUG Saved company data:\n" + serializedObject );
                    serializedObject = Encryption.Encrypt(serializedObject);
                    var fileName = MakeCompanyAddressDataFileName();
                    Logging.WriteLogLine("Saving company address data to " + fileName);
                    File.WriteAllText(fileName, serializedObject);
                }
                catch (Exception E)
                {
                    Logging.WriteLogLine("ERROR: unable to save company data: " + E);
                }
            }
        }

        private void LoadCachedCompanyAddressData()
        {
            if (!IsInDesignMode)
            {
                try
                {
                    var fileName = MakeCompanyDataFileName();

                    if (File.Exists(fileName))
                    {
                        Logging.WriteLogLine("Loading company address data from " + fileName);
                        var serializedObject = File.ReadAllText(fileName);
                        serializedObject = Encryption.Decrypt(serializedObject);
                        var cad = JsonConvert.DeserializeObject<CompanyAddressData>(serializedObject);

                        if (cad != null)
                        {
                            if (cad.DictCompanySummarysList.Count > 0)
                                DictCompanySummaryListsByAccountId = cad.DictCompanySummarysList;

                            if (cad.DictCompanyDetailsList.Count > 0)
                                DictCompanyDetailListsByAccountId = cad.DictCompanyDetailsList;
                        }
                    }
                }
                catch (Exception e)
                {
                    Logging.WriteLogLine("ERROR: unable to load company address data: " + e);

                    // Doing this to ensure that this tab will load if the file is corrupted
                    Logging.WriteLogLine("Deleting bad company address data file");
                    CompanyAddressData cad = new();
                    SaveCompanyAddressData(cad);
                }
            }
        }

        private static readonly SemaphoreSlim MultiEntrySem = new( 1, 1 );
        private readonly CancellationTokenSource CancellationToken = new();

        private async void GetCompaniesAndUpdateCurrentCollectionAsync()
        {
            if (!IsInDesignMode)
            {
                try
                {
                    var Token = CancellationToken.Token;

                    if (!Token.IsCancellationRequested)
                    {
                        await MultiEntrySem.WaitAsync(Token);

                        try
                        {
                            Logging.WriteLogLine("Loading Company Names");
                            var Names = await Azure.Client.RequestGetCustomerCodeList();

                            if (!Token.IsCancellationRequested && Names is not null)
                            {
                                var CoNames = (from N in Names
                                               where N != GlobalAddressBookId
                                               orderby N.ToLower()
                                               select N).ToList();

                                List<CompanyAddress>? CompanyAddresses = new();
                                List<string> EnabledNames = new();

                                foreach (var CoName in CoNames)
                                {
                                    if (Token.IsCancellationRequested)
                                        return;

                                    var Company = Azure.Client.RequestGetResellerCustomerCompany(CoName).Result;

                                    if (Company != null)
                                    {
                                        if (Company.CompanyNumber.IsNullOrWhiteSpace())
                                            Company.CompanyNumber = CoName;

                                        if (Company.Enabled)
                                        {
                                            CompanyAddresses.Add(Company);
                                            EnabledNames.Add(CoName);
                                        }
                                    }
                                    else
                                        Logging.WriteLogLine("Can't get Company for " + CoName);
                                }

                                Logging.WriteLogLine("Finished getting companies (found " + CompanyAddresses.Count + ")");

                                EnabledNames = (from E in EnabledNames
                                                orderby E.ToLower()
                                                select E).ToList();

                                //Dispatcher.Invoke(() =>
                                //                   {
                                                       Companies = CompanyAddresses;
                                                       CompanyNames = EnabledNames;
                                                   //});
                                Logging.WriteLogLine("Finished updating Companies");

                                // Save locally
                                Logging.WriteLogLine("Saving updated companies locally");
                                CompanyData Cd = new();

                                Cd.CompanyAddresses.AddRange(Companies);
                                Cd.EnabledNames.AddRange((List<string>)CompanyNames);
                                SaveCompanyData(Cd);
                                //_ = Task.Run(async () =>
                                //{
                                //	object value = await SaveCompanyData(cd);

                                //});
                            }
                        }
                        catch (Exception E)
                        {
                            //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
                            Logging.WriteLogLine("Exception thrown: " + E);
                            MessageBox.Show("Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK);
                        }
                    }
                }
                catch // Probably closed tab druing initialisation
                {
                }
            }

            Logging.WriteLogLine("DEBUG Done");
        }

        private string MakeCompanyAddressDataFileName()
        {
            var AppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd('\\');

            if (!Directory.Exists(BaseFolder))
                Directory.CreateDirectory(BaseFolder);

            var id = Globals.Company.CarrierId;

            return $"{BaseFolder}\\{id}-companyaddressdata.json";
        }

        private string MakeCompanyDataFileName()
        {
            var AppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd('\\');

            if (!Directory.Exists(BaseFolder))
                Directory.CreateDirectory(BaseFolder);

            var id = Globals.Company.CarrierId;

            return $"{BaseFolder}\\{id}-companydata.json";
        }

        #endregion

        #region Load Addresses for Companies
        private readonly BackgroundWorker Worker = new();

        private async void Worker_DoWorkLoadAddressesForCompanies(object? sender, DoWorkEventArgs? e)
        {
            Logging.WriteLogLine("Starting to load addresses for companies");

            var Names = await Azure.Client.RequestGetCustomerCodeList();

            if (Names is not null)
            {
                var CoNames = (from N in Names
                               where N != GlobalAddressBookId
                               orderby N.ToLower()
                               select N).ToList();

                // TODO Arbitrary limit - PML only has 2 and all the addresses are in PmlOz
                if (CoNames.Count < 10)
                {
                    await new Tasks.Task<string>().RunAsync(CoNames, async coName =>
                                                                      {
                                                                          Logging.WriteLogLine("DEBUG Loading addresses for " + coName);

                                                                          bool HasName;

                                                                          lock (DictCompanySummaryListsByAccountId)
                                                                              HasName = DictCompanySummaryListsByAccountId.ContainsKey(coName);

                                                                          if (!HasName)
                                                                          {
                                                                              //Logging.WriteLogLine("DEBUG Loading CompanySummaryList from server");

                                                                              if (await Azure.Client.RequestGetCustomerCompaniesSummary(coName) is { } Csl)
                                                                              {
                                                                                  lock (DictCompanySummaryListsByAccountId)
                                                                                  {
                                                                                      DictCompanySummaryListsByAccountId.Add(coName, Csl);
                                                                                      Logging.WriteLogLine("DEBUG Found " + Csl.Count + " summary addresses for " + coName);
                                                                                  }
                                                                              }
                                                                          }

                                                                          lock (DictCompanyDetailListsByAccountId)
                                                                              HasName = DictCompanyDetailListsByAccountId.ContainsKey(coName);

                                                                          if (!HasName)
                                                                          {
                                                                              if (await Azure.Client.RequestGetCustomerCompaniesDetailed(coName) is { } Cdl)
                                                                              {
                                                                                  lock (DictCompanyDetailListsByAccountId)
                                                                                  {
                                                                                      DictCompanyDetailListsByAccountId.Add(coName, Cdl);
                                                                                      Logging.WriteLogLine("DEBUG Found " + Cdl.Count + " detailed addresses for " + coName);
                                                                                  }
                                                                              }
                                                                          }
                                                                      }, 5);

                    //Logging.WriteLogLine("DEBUG DictCompanySummaryListsByAccountId.Count == CoNames.Count");
                    CompanyAddressData cad = new()
                    {
                        DictCompanySummarysList = DictCompanySummaryListsByAccountId,
                        DictCompanyDetailsList = DictCompanyDetailListsByAccountId
                    };
                    SaveCompanyAddressData(cad);
                }
                else
                    Logging.WriteLogLine("NOT caching addresses for companies - too many");
            }
        }

        private static void Worker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            Logging.WriteLogLine("Worker completed");
        }
        #endregion

        #region Caching of Company Data
        public class CompanyData
        {
            public List<CompanyAddress> CompanyAddresses { get; set; } = new();
            public List<string> EnabledNames { get; set; } = new();
        }

        //private readonly BackgroundWorker Worker = new();

        //private void Worker_SaveCompanyData( object sender, DoWorkEventArgs e )
        //{

        //}

        private static void Worker_SaveCompanyDataCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Logging.WriteLogLine("DEBUG worker completed");
        }

        private void SaveCompanyData(CompanyData companyData)
        {
            if (!IsInDesignMode)
            {
                try
                {
                    var serializedObject = JsonConvert.SerializeObject(companyData);

                    //Logging.WriteLogLine( "DEBUG Saved company data:\n" + serializedObject );
                    serializedObject = Encryption.Encrypt(serializedObject);
                    var fileName = MakeCompanyDataFileName();
                    Logging.WriteLogLine("Saving company data to " + fileName);
                    File.WriteAllText(fileName, serializedObject);
                }
                catch (Exception E)
                {
                    Logging.WriteLogLine("ERROR: unable to save company data: " + E);
                }
            }
        }

        private void LoadCachedCompanyData()
        {
            if (!IsInDesignMode)
            {
                try
                {
                    var fileName = MakeCompanyDataFileName();

                    if (File.Exists(fileName))
                    {
                        Logging.WriteLogLine("Loading company data from " + fileName);
                        var serializedObject = File.ReadAllText(fileName);
                        serializedObject = Encryption.Decrypt(serializedObject);
                        var cd = JsonConvert.DeserializeObject<CompanyData>(serializedObject);

                        if (cd != null)
                        {
                            if ((cd.CompanyAddresses != null) && (cd.CompanyAddresses.Count > 0))
                                Companies = cd.CompanyAddresses;

                            if ((cd.EnabledNames != null) && (cd.EnabledNames.Count > 0))
                                CompanyNames = cd.EnabledNames;                            
                        }

                        CompanyFullNames.Clear();

                        foreach (var ca in Companies)
                        {
                            if (!CompanyFullNames.ContainsKey(ca.CompanyName))
                                CompanyFullNames.Add(ca.CompanyName, ca);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logging.WriteLogLine("ERROR: unable to load company data: " + e);

                    // Doing this to ensure that this tab will load if the file is corrupted
                    Logging.WriteLogLine("Deleting bad company data file");
                    CompanyData cd = new();
                    SaveCompanyData(cd);
                }
            }
        }
        private void ReloadPrimaryCompanies()
	{
		Logging.WriteLogLine( "Reloading Primary Companies" );

		if( !IsInDesignMode )
		{
			Tasks.RunVoid( async () =>
						   {
							   var Names = await Azure.Client.RequestGetCustomerCodeList();

							   if( Names is not null )
							   {
								   var CoNames = ( from N in Names
											       let Ln = N.ToLower()
											       orderby Ln
											       select Ln ).ToList();

								   var CompanyAddresses = new List<CompanyAddress>();

								   await new Tasks.Task<string>().RunAsync( CoNames, async coName =>
																			         {
																				         try
																				         {
																					         var Company = await Azure.Client.RequestGetResellerCustomerCompany( coName );

																					         lock( CompanyAddresses )
																						         CompanyAddresses.Add( Company );
																				         }
																				         catch( Exception Exception )
																				         {
																					         Logging.WriteLogLine( "Exception thrown: " + Exception );
																				         }
																			         }, 5 );

								   //Dispatcher.Invoke( () =>
											//	      {
													      CompanyNames = CoNames;
													      Companies    = CompanyAddresses;
												      //} );
							   }
						   } );
		}
	}

	// [DependsUpon(nameof(ReloadAccounts))]
	private async Task GetCompanyNames()
	{
		if( !IsInDesignMode )
		{
			try
			{
				Logging.WriteLogLine( "Loading Company Names" );
				var Names = await Azure.Client.RequestGetCustomerCodeList();

				if( Names is not null )
				{
					// Don't include the Global Address Book
					var CoNames = ( from N in Names
								    where N != GlobalAddressBookId
								    orderby N.ToLower()
								    select N ).ToList();

					var NamesToDelete = ( from N in Names
										  where N != GlobalAddressBookId
										  orderby N.ToLower()
										  select N ).ToList();

					List<CompanyAddress>? CompanyAddresses = new();
					List<string>?         EnabledNames     = new();

					var CoNamesLock = new object();

					await new Tasks.Task<string>().RunAsync( CoNames, async coName =>
																      {
																	      try
																	      {
																		      var Company = await Azure.Client.RequestGetResellerCustomerCompany( coName );

																		      lock( CoNamesLock )
																		      {
																			      if( Company?.Enabled == true )
																			      {
																				      if( coName != GlobalAddressBookId )
																				      {
																					      if( Company.CompanyNumber.IsNullOrWhiteSpace() )
																					      {
																						      //Logging.WriteLogLine( "DEBUG Setting CompanyNumber to " + coName );
																						      Company.CompanyNumber = coName;
																					      }

																					      if( !EnabledNames.Contains( coName ) )
																						      EnabledNames.Add( coName );

																					      //Logging.WriteLogLine( "DEBUG Loading Company: " + Company.CompanyName );

																					      if( !CompanyAddresses.Contains( Company ) )
																						      CompanyAddresses.Add( Company );
																				      }
																			      }
																			      //else
																			      // Logging.WriteLogLine( "DEBUG - skipping disabled account: " + coName );

																			      lock( NamesToDelete )
																			      {
																				      NamesToDelete.Remove( coName );

																				      if( NamesToDelete.Count == 0 )
																				      {
																					      // Save locally
																					      CompanyData Cd = new();

																					      Cd.CompanyAddresses.AddRange( CompanyAddresses );
																					      //Cd.EnabledNames.AddRange((List<string>)CompanyNames);
																					      Cd.EnabledNames.AddRange( EnabledNames );
																					      SaveCompanyData( Cd );
																				      }
																			      }
																		      }
																	      }
																	      catch( Exception Exception )
																	      {
																		      Logging.WriteLogLine( "Exception thrown: " + Exception );
																	      }
																      }, 5 );

					EnabledNames = ( from Tmp in EnabledNames
								     where Tmp != GlobalAddressBookId
								     orderby Tmp.ToLower()
								     select Tmp ).ToList();

					//Dispatcher.Invoke( () =>
					//				   {
										   CompanyNames = EnabledNames;
										   Companies    = CompanyAddresses;
										   CompanyFullNames.Clear();

										   foreach( var ca in Companies )
										   {
											   if( !CompanyFullNames.ContainsKey( ca.CompanyName ) && ( ca.CompanyNumber != GlobalAddressBookId ) && ( ca.CompanyName != GlobalAddressBookId ) )
												   CompanyFullNames.Add( ca.CompanyName, ca );
										   }
									   //} );
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK );
			}
		}
		Logging.WriteLogLine( "DEBUG Done" );
	}
        #endregion

    }
}
