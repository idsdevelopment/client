﻿using Utils.Csv;
using ViewModels.Trips.TripEntry;

namespace ViewModels.Customers.Accounts;

public class CustomerImportModel : ViewModelBase
{
	protected override void OnInitialised()
	{
		base.OnInitialised();

		Task.Run( () =>
		          {
			          Task.WaitAll( GetAccountNamesAsync(),
			                        GetAdminUsers(),
			                        GetRoles()
			                      );
		          } );
	}

	private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

#region Data
	public string SelectedImportFile
	{
		get { return Get( () => SelectedImportFile, "" ); }
		set { Set( () => SelectedImportFile, value ); }
	}


	public bool IsUpdateAddressesChecked
	{
		get { return Get( () => IsUpdateAddressesChecked, false ); }
		set { Set( () => IsUpdateAddressesChecked, value ); }
	}


	public bool IsImportButtonEnabled
	{
		get { return Get( () => IsImportButtonEnabled, false ); }
		set { Set( () => IsImportButtonEnabled, value ); }
	}

	public string Results
	{
		get { return Get( () => Results, "" ); }
		set { Set( () => Results, value ); }
	}


	public bool IsImportRunning
	{
		get { return Get( () => IsImportRunning, false ); }
		set { Set( () => IsImportRunning, value ); }
	}

	// Offsets
	private static readonly int CsvAccountname        = 0;
	private static readonly int CsvAccount            = 1;
	private static readonly int CsvPhone              = 2;
	private static readonly int CsvSalesrep           = 3;
	private static readonly int CsvAccountnotes       = 4;
	private static readonly int CsvBillingperiod      = 5;
	private static readonly int CsvBillingmethod      = 6;
	private static readonly int CsvInvoiceemail       = 7;
	private static readonly int CsvCreditlimit        = 8;
	private static readonly int CsvBillingcompany     = 9;
	private static readonly int CsvBillingname        = 10;
	private static readonly int CsvBillingphone       = 11;
	private static readonly int CsvBillingsuite       = 12;
	private static readonly int CsvBillingstreet      = 13;
	private static readonly int CsvBillingcity        = 14;
	private static readonly int CsvBillingprovstate   = 15;
	private static readonly int CsvBillingcountry     = 16;
	private static readonly int CsvBillingpczip       = 17;
	private static readonly int CsvBillingZone		  = 18;
	private static readonly int CsvShippingcompany    = 19;
	private static readonly int CsvShippingname       = 20;
	private static readonly int CsvShippingphone      = 21;
	private static readonly int CsvShippingsuite      = 22;
	private static readonly int CsvShippingstreet     = 23;
	private static readonly int CsvShippingcity       = 24;
	private static readonly int CsvShippingprovstate  = 25;
	private static readonly int CsvShippingcountry    = 26;
	private static readonly int CsvShippingpczip      = 27;
	private static readonly int CsvShippingZone		  = 28;
	private static readonly int CsvAdminuser          = 29;
	private static readonly int CsvAdminpw            = 30;
	private static readonly int CsvAdminfirstname     = 31;
	private static readonly int CsvAdminlastname      = 32;
	private static readonly int CsvAdminemail         = 33;
	private static readonly int CsvAdminph            = 34;
	private static readonly int CsvAdmincell          = 35;
	private static readonly int CsvReferenceMandatory = 36;

	private readonly Dictionary<int, string> DictFieldNames = new()
	                                                          {
		                                                          {CsvAccountname, "AccountName"},
		                                                          {CsvAccount, "Account"},
		                                                          {CsvPhone, "Phone"},
		                                                          {CsvSalesrep, "SalesRep"},
		                                                          {CsvAccountnotes, "AccountNotes"},
		                                                          {CsvBillingperiod, "BillingPeriod"},
		                                                          {CsvBillingmethod, "BillingMethod"},
		                                                          {CsvInvoiceemail, "InvoiceEmail"},
		                                                          {CsvCreditlimit, "CreditLimit"},
		                                                          {CsvBillingcompany, "BillingCompany"},
		                                                          {CsvBillingname, "BillingName"},
		                                                          {CsvBillingphone, "BillingPhone"},
		                                                          {CsvBillingsuite, "BillingSuite"},
		                                                          {CsvBillingstreet, "BillingStreet"},
		                                                          {CsvBillingcity, "BillingCity"},
		                                                          {CsvBillingprovstate, "BillingProvState"},
		                                                          {CsvBillingcountry, "BillingCountry"},
		                                                          {CsvBillingpczip, "BillingPCZip"},
																  {CsvBillingZone, "BillingZone"},
		                                                          {CsvShippingcompany, "ShippingCompany"},
		                                                          {CsvShippingname, "ShippingName"},
		                                                          {CsvShippingphone, "ShippingPhone"},
		                                                          {CsvShippingsuite, "ShippingSuite"},
		                                                          {CsvShippingstreet, "ShippingStreet"},
		                                                          {CsvShippingcity, "ShippingCity"},
		                                                          {CsvShippingprovstate, "ShippingProvState"},
		                                                          {CsvShippingcountry, "ShippingCountry"},
		                                                          {CsvShippingpczip, "ShippingPCZip"},
																  {CsvShippingZone, "ShippingZone"},
		                                                          {CsvAdminuser, "AdminUser"},
		                                                          {CsvAdminpw, "AdminPW"},
		                                                          {CsvAdminfirstname, "AdminFirstName"},
		                                                          {CsvAdminlastname, "AdminLastName"},
		                                                          {CsvAdminemail, "AdminEmail"},
		                                                          {CsvAdminph, "AdminPh"},
		                                                          {CsvAdmincell, "AdminCell"},
																  {CsvReferenceMandatory, "ReferenceMandatory"},
	                                                          };

	private readonly Dictionary<int, string> DictMandatoryFields = new()
	                                                               {
		                                                               {CsvAccountname, "AccountName"},
		                                                               {CsvAccount, "Account"},
		                                                               {CsvPhone, "Phone"},

		                                                               //{ CSV_SALESREP, "SalesRep" },
		                                                               //{ CSV_ACCOUNTNOTES, "AccountNotes" },
		                                                               //{ CSV_BILLINGPERIOD, "BillingPeriod" },
		                                                               //{ CSV_BILLINGMETHOD, "BillingMethod" },
		                                                               //{ CSV_INVOICEEMAIL, "InvoiceEmail" },
		                                                               //{ CSV_CREDITLIMIT, "CreditLimit" },
		                                                               {CsvBillingcompany, "BillingCompany"},

		                                                               //{ CSV_BILLINGNAME, "BillingName" },
		                                                               {CsvBillingphone, "BillingPhone"},

		                                                               //{ CSV_BILLINGSUITE, "BillingSuite" },
		                                                               {CsvBillingstreet, "BillingStreet"},
		                                                               {CsvBillingcity, "BillingCity"},
		                                                               {CsvBillingprovstate, "BillingProvState"},
		                                                               {CsvBillingcountry, "BillingCountry"},
		                                                               {CsvBillingpczip, "BillingPCZip"},
		                                                               {CsvShippingcompany, "ShippingCompany"},

		                                                               //{ CSV_SHIPPINGNAME, "ShippingName" },
		                                                               {CsvShippingphone, "ShippingPhone"},

		                                                               //{ CSV_SHIPPINGSUITE, "ShippingSuite" },
		                                                               {CsvShippingstreet, "ShippingStreet"},
		                                                               {CsvShippingcity, "ShippingCity"},
		                                                               {CsvShippingprovstate, "ShippingProvState"},
		                                                               {CsvShippingcountry, "ShippingCountry"},
		                                                               {CsvShippingpczip, "ShippingPCZip"},
		                                                               {CsvAdminuser, "AdminUser"},
		                                                               {CsvAdminpw, "AdminPW"},
		                                                               {CsvAdminfirstname, "AdminFirstName"},
		                                                               {CsvAdminlastname, "AdminLastName"},
		                                                               {CsvAdminemail, "AdminEmail"},
		                                                               {CsvAdminph, "AdminPh"}

		                                                               //{ CSV_ADMINCELL, "AdminCell" },
																	   //{CsvReferenceMandatory, "ReferenceMandatory"}
	                                                               };

	private Dictionary<string, Protocol.Data.Staff> DictAdmins = new();

	private List<string> AccountNames = new();

	public Role WebAdminRole { get; set; }
#endregion

#region Actions
	public (bool isCorrect, List<string> errors) CheckHeaders()
	{
		var          IsCorrect = true;
		List<string> Errors    = new();

		try
		{
			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			// Just the first line
			var Header = CsvObj[ 0 ];

			if( Header.Count == DictFieldNames.Count )
			{
				for( var I = 0; I < Header.Count; I++ )
					CheckHeaderColumn( Header[ I ], DictFieldNames[ I ], ref Errors );
			}
			else
			{
				// Wrong number of headers 
				var Error = FindStringResource( "CustomerImportHeadersWrongNumberError" );
				Error = Error.Replace( "@1", DictFieldNames.Count + "" );
				Error = Error.Replace( "@2", Header.Count + "" );
				Errors.Add( Error );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );
			IsCorrect = false;
		}

		if( Errors.Count > 0 )
			IsCorrect = false;

		return ( IsCorrect, Errors );
	}

	private static void CheckHeaderColumn( string got, string shouldBe, ref List<string> errors )
	{
		if( !string.Equals( got.Trim(), shouldBe, StringComparison.CurrentCultureIgnoreCase ) )
		{
			var Error = FindStringResource( "CustomerImportHeadersError" );
			Error = Error.Replace( "@1", got );
			Error = Error.Replace( "@2", shouldBe );
			errors.Add( Error );
		}
	}

	private void CheckLineForMandatoryFields( int lineNumber, List<string> line, ref List<string> errors )
	{
		foreach( var Key in DictMandatoryFields.Keys )
		{
			var Value = line[ Key ];

			if( Value.Length == 0 )
			{
				var Error = FindStringResource( "ImportShipmentsMissingFieldError" );
				Error = Error.Replace( "@1", lineNumber + "" );
				Error = Error.Replace( "@2", DictFieldNames[ Key ] );
				errors.Add( Error );
			}
		}
	}

	public (bool isCorrect, List<string> errors) CheckFileContents()
	{
		var          IsCorrect = true;
		List<string> Errors    = new();

		try
		{
			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			// Skip the header
			for( var I = 1; I < CsvObj.RowCount; I++ )
			{
				var Line   = CsvObj[ I ];
				var Values = ProcessLine( Line );

				CheckLineForMandatoryFields( I, Values, ref Errors );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );
			IsCorrect = false;
		}

		if( Errors.Count > 0 )
			IsCorrect = false;

		return ( IsCorrect, Errors );
	}

	private static List<string> ProcessLine( Row line )
	{
		List<string> Values = new()
		                      {
			                      line[ CsvAccountname ],

			                      //Logging.WriteLogLine("AccountName: " + line[CSV_ACCOUNTNAME]);
			                      line[ CsvAccount ],

			                      //Logging.WriteLogLine("Account: " + line[CSV_ACCOUNT]);
			                      line[ CsvPhone ],

			                      //Logging.WriteLogLine("Phone: " + line[CSV_PHONE]);
			                      line[ CsvSalesrep ],

			                      //Logging.WriteLogLine("SalesRep: " + line[CSV_SALESREP]);
			                      line[ CsvAccountnotes ],

			                      //Logging.WriteLogLine("AccountNotes: " + line[CSV_ACCOUNTNOTES]);
			                      line[ CsvBillingperiod ],

			                      //Logging.WriteLogLine("BillingPeriod: " + line[CSV_BILLINGPERIOD]);
			                      line[ CsvBillingmethod ],

			                      //Logging.WriteLogLine("BillingMethod: " + line[CSV_BILLINGMETHOD]);
			                      line[ CsvInvoiceemail ],

			                      //Logging.WriteLogLine("InvoiceEmail: " + line[CSV_INVOICEEMAIL]);
			                      line[ CsvCreditlimit ],

			                      //Logging.WriteLogLine("CreditLimit: " + line[CSV_CREDITLIMIT]);
			                      line[ CsvBillingcompany ],

			                      //Logging.WriteLogLine("BillingCompany: " + line[CSV_BILLINGCOMPANY]);
			                      line[ CsvBillingname ],

			                      //Logging.WriteLogLine("BillingName: " + line[CSV_BILLINGNAME]);
			                      line[ CsvBillingphone ],

			                      //Logging.WriteLogLine("BillingPhone: " + line[CSV_BILLINGPHONE]);
			                      line[ CsvBillingsuite ],

			                      //Logging.WriteLogLine("BillingSuite: " + line[CSV_BILLINGSUITE]);
			                      line[ CsvBillingstreet ],

			                      //Logging.WriteLogLine("BillingStreet: " + line[CSV_BILLINGSTREET]);
			                      line[ CsvBillingcity ],

			                      //Logging.WriteLogLine("BillingCity: " + line[CSV_BILLINGCITY]);
			                      line[ CsvBillingprovstate ],

			                      //Logging.WriteLogLine("BillingProvState: " + line[CSV_BILLINGPROVSTATE]);
			                      line[ CsvBillingcountry ],

			                      //Logging.WriteLogLine("BillingCountry: " + line[CSV_BILLINGCOUNTRY]);
			                      line[ CsvBillingpczip ],

								  line[CsvBillingZone],

			                      //Logging.WriteLogLine("BillingPcZip: " + line[CSV_BILLINGPCZIP]);
			                      line[ CsvShippingcompany ],

			                      //Logging.WriteLogLine("ShippingCompany: " + line[CSV_SHIPPINGCOMPANY]);
			                      line[ CsvShippingname ],

			                      //Logging.WriteLogLine("ShippingName: " + line[CSV_SHIPPINGNAME]);
			                      line[ CsvShippingphone ],

			                      //Logging.WriteLogLine("ShippingPhone: " + line[CSV_SHIPPINGPHONE]);
			                      line[ CsvShippingsuite ],

			                      //Logging.WriteLogLine("ShippingSuite: " + line[CSV_SHIPPINGSUITE]);
			                      line[ CsvShippingstreet ],

			                      //Logging.WriteLogLine("ShippingStreet: " + line[CSV_SHIPPINGSTREET]);
			                      line[ CsvShippingcity ],

			                      //Logging.WriteLogLine("ShippingCity: " + line[CSV_SHIPPINGCITY]);
			                      line[ CsvShippingprovstate ],

			                      //Logging.WriteLogLine("ShippingProvState: " + line[CSV_SHIPPINGPROVSTATE]);
			                      line[ CsvShippingcountry ],

			                      //Logging.WriteLogLine("ShippingCountry: " + line[CSV_SHIPPINGCOUNTRY]);
			                      line[ CsvShippingpczip ],

								  line[CsvShippingZone],

			                      //Logging.WriteLogLine("ShippingPcZip: " + line[CSV_SHIPPINGPCZIP]);
			                      line[ CsvAdminuser ],

			                      //Logging.WriteLogLine("AdminUser: " + line[CSV_ADMINUSER]);
			                      line[ CsvAdminpw ],

			                      //Logging.WriteLogLine("AdminPw: " + line[CSV_ADMINPW]);
			                      line[ CsvAdminfirstname ],

			                      //Logging.WriteLogLine("AdminFirstName: " + line[CSV_ADMINFIRSTNAME]);
			                      line[ CsvAdminlastname ],

			                      //Logging.WriteLogLine("AdminLastName: " + line[CSV_ADMINLASTNAME]);
			                      line[ CsvAdminemail ],

			                      //Logging.WriteLogLine("AdminEmail: " + line[CSV_ADMINEMAIL]);
			                      line[ CsvAdminph ],

			                      //Logging.WriteLogLine("AdminPh: " + line[CSV_ADMINPH]);
			                      line[ CsvAdmincell ],

								  line[CsvReferenceMandatory]
		                      };

		//Logging.WriteLogLine("AdminCell: " + line[CSV_ADMINCELL]);

		return Values;
	}

	public (int numberImported, int numberSkipped, List<string> errors) Import()
	{
		Logging.WriteLogLine( "Importing accounts from file " + SelectedImportFile );

		//bool isCorrect = true;
		List<string> Errors = new();
		IsImportRunning = true;
		var NumberImported = 0;
		var NumberSkipped  = 0;

		try
		{
			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			// Skip the header
			for( var I = 1; I < CsvObj.RowCount; I++ )
			{
				Logging.WriteLogLine("Processing line " + I);
				List<string> RowErrors;
				var          Line        = CsvObj[ I ];
				string       CompanyName = Line[ CsvAccountname ];

				var Exists = ( from A in AccountNames
				               where A.TrimToLower() == CompanyName.TrimToLower()
				               select A ).FirstOrDefault();

				if( Exists.IsNullOrWhiteSpace() )
				{
					++NumberImported;
					Logging.WriteLogLine("Adding new account: " + CompanyName);
					Task.WaitAll(
					             _ = Task.Run( async () =>
					                           {
						                           RowErrors = await AddAccount( Line, true );

						                           if( RowErrors.Count > 0 )
							                           Errors.AddRange( RowErrors );
					                           } )
					            );
				}
				else
				{
					Logging.WriteLogLine("Udating account: " + CompanyName);
					// ++NumberSkipped;
					// var Message = FindStringResource( "CustomerImportCompanyNameExistsError" );
					// Message = Message.Replace( "@1", CompanyName );
					// Logging.WriteLogLine( Message );
					// Errors.Add( Message );
					++NumberImported;

					Task.WaitAll(
								 _ = Task.Run(async () =>
											   {
												   RowErrors = await AddAccount(Line, false);

												   if (RowErrors.Count > 0)
													   Errors.AddRange(RowErrors);
											   })
								);
				}
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );

			//isCorrect = false;
		}

		//if (errors.Count > 0)
		//{
		//    isCorrect = false;
		//}
		IsImportRunning = false;

		//return (isCorrect, errors);
		return ( NumberImported, NumberSkipped, Errors );
	}

	private async Task<List<string>> AddAccount( Row line, bool isNew )
	{
		List<string> Errors = new();

		//Task.Run(() =>
		//{
		//    Task.WaitAll(GetAccountNamesAsync(),
		//                 GetAdminUsers(),
		//                 GetRoles()
		//                );
		//});

		try
		{
			Company BillingCompany  = new();
			Company ShippingCompany = new();
			PrimaryCompany PrimaryCompany = new();
			CompanyAddress ResellerCompany = new();


			var     AccountId       = ( (string)line[ CsvAccount ] ).Trim();
			var     AccountName     = ( (string)line[ CsvAccountname ] ).Trim();

			// Only update if NOT editing - new Account
			if (isNew)
			{
				PrimaryCompany = new PrimaryCompany
									 {
										 Company =
										 {
											 //CompanyNumber = SelectedAccountId.Trim()
											 CompanyNumber = AccountId
										 }
									 };

				ResellerCompany = new CompanyAddress
									  {
										  //CompanyName = SelectedAccountName.Trim(),
										  CompanyName = AccountName,

										  //CompanyNumber = SelectedAccountId.Trim()
										  CompanyNumber = AccountId
									  };

			}
			else
			{
				Task.WaitAll(
					Task.Run(async () =>
					{
						PrimaryCompany = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(AccountId);
						Logging.WriteLogLine("DEBUG Got PrimaryCompany: " + PrimaryCompany.CustomerCode);
						ResellerCompany = await Azure.Client.RequestGetResellerCustomerCompany(PrimaryCompany.CustomerCode);
						Logging.WriteLogLine("DEBUG Got ResellerCompany: " + ResellerCompany.CompanyName);
					})
				);
			}

			#region Primary Company
			var Company = PrimaryCompany.Company;

			//var AccountId = SelectedAccountId.Trim();
			//var PWord = Password.Password.Trim();
			var PWord = ( (string)line[ CsvAdminpw ] ).Trim();

			//company.CompanyName = SelectedAccountName.Trim();
			Company.CompanyNumber = AccountId;

			//Company.CompanyName = SelectedAccountName.Trim();
			Company.CompanyName = AccountName;

			//Company.UserName = SelectedAccountAdminUserName.Trim();
			Company.UserName = ( (string)line[ CsvAdminuser ] ).Trim();
			Company.Password = PWord;

			//Company.ContactName = SelectedAccountSalesRep.Trim();
			Company.ContactName = ( (string)line[ CsvSalesrep ] ).Trim();

			//Company.Phone = SelectedAccountPhone.Trim();
			Company.Phone = ( (string)line[ CsvPhone ] ).Trim();

			//Company.Notes = SelectedAccountNotes.Trim();
			Company.Notes = ( (string)line[ CsvAccountnotes ] ).Trim();

			//Company.Enabled = SelectedAccountIsEnabled;
			Company.Enabled = true;

			//ResellerCompany.Notes = SelectedAccountNotes.Trim();
			ResellerCompany.Notes          = ( (string)line[ CsvAccountnotes ] ).Trim();
			PrimaryCompany.BillingCompany  = BillingCompany;
			PrimaryCompany.ShippingCompany = ShippingCompany;

			//PrimaryCompany.CustomerCode = SelectedAccountId.Trim();
			PrimaryCompany.CustomerCode = AccountId;

			//PrimaryCompany.LoginCode = SelectedAccountId.Trim();
			PrimaryCompany.LoginCode = AccountId;

			//PrimaryCompany.UserName = SelectedAccountAdminUserName.Trim();
			PrimaryCompany.UserName = ( (string)line[ CsvAdminuser ] ).Trim();

			//PrimaryCompany.Password = SelectedAccountAdminUserPassword.Trim();
			PrimaryCompany.Password = PWord;
		#endregion

		#region Billing Company
			BillingCompany.CompanyNumber = AccountId;

			//BillingCompany.CompanyName = SelectedAccountBillingCompanyName.Trim();
			BillingCompany.CompanyName = ( (string)line[ CsvBillingcompany ] ).Trim();

			//BillingCompany.UserName = SelectedAccountBillingAdminName.Trim();
			BillingCompany.UserName = ( (string)line[ CsvBillingname ] ).Trim();
			BillingCompany.Password = PWord;

			//BillingCompany.Phone = SelectedAccountBillingAdminPhone.Trim();
			BillingCompany.Phone = ( (string)line[ CsvBillingphone ] ).Trim();

			//BillingCompany.Suite = SelectedAccountBillingSuite.Trim();
			BillingCompany.Suite = ( (string)line[ CsvBillingsuite ] ).Trim();

			//BillingCompany.AddressLine1 = SelectedAccountBillingStreet.Trim();
			BillingCompany.AddressLine1 = ( (string)line[ CsvBillingstreet ] ).Trim();

			//BillingCompany.City = SelectedAccountBillingCity.Trim();
			BillingCompany.City = ( (string)line[ CsvBillingcity ] ).Trim();

			//BillingCompany.Region = SelectedAccountBillingProvState.Trim();
			BillingCompany.Region = ( (string)line[ CsvBillingprovstate ] ).Trim();

			//BillingCompany.PostalCode = SelectedAccountBillingPostalZip.Trim();
			BillingCompany.PostalCode = ( (string)line[ CsvBillingpczip ] ).Trim();

			//BillingCompany.Country = SelectedAccountBillingCountry.Trim();
			BillingCompany.Country = ( (string)line[ CsvBillingcountry ] ).Trim();

			//BillingCompany.Notes = SelectedAccountBillingNotes.Trim();
			BillingCompany.Notes = ( (string)line[ CsvAccountnotes ] ).Trim(); //((string)line[CSV_BILLINGNOTES]).Trim();

			//BillingCompany.ZoneName = SelectedAccountBillingZone.Trim();
			BillingCompany.ZoneName = ( (string)line[ CsvBillingZone ] ).Trim(); //((string)line[CSV_BILLINGZONE]).Trim();
		#endregion

		#region Shipping Address
			ShippingCompany.CompanyNumber = AccountId;

			//ShippingCompany.CompanyName = ShippingAccountCompanyName.Trim();
			ShippingCompany.CompanyName = ( (string)line[ CsvShippingcompany ] ).Trim();

			//ShippingCompany.UserName = ShippingAccountUserName.Trim();
			ShippingCompany.UserName = ( (string)line[ CsvShippingname ] ).Trim();
			ShippingCompany.Password = PWord;

			//ShippingCompany.Phone = SelectedAccountShippingAdminPhone.Trim();
			ShippingCompany.Phone = ( (string)line[ CsvShippingphone ] ).Trim();

			//ShippingCompany.Suite = SelectedAccountShippingSuite.Trim();
			ShippingCompany.Suite = ( (string)line[ CsvShippingsuite ] ).Trim();

			//ShippingCompany.AddressLine1 = SelectedAccountShippingStreet.Trim();
			ShippingCompany.AddressLine1 = ( (string)line[ CsvShippingstreet ] ).Trim();

			//ShippingCompany.City = SelectedAccountShippingCity.Trim();
			ShippingCompany.City = ( (string)line[ CsvShippingcity ] ).Trim();

			//ShippingCompany.Region = SelectedAccountShippingProvState.Trim();
			ShippingCompany.Region = ( (string)line[ CsvShippingprovstate ] ).Trim();

			//ShippingCompany.PostalCode = SelectedAccountShippingPostalZip.Trim();
			ShippingCompany.PostalCode = ( (string)line[ CsvShippingpczip ] ).Trim();

			//ShippingCompany.Country = SelectedAccountShippingCountry.Trim();
			ShippingCompany.Country = ( (string)line[ CsvShippingcountry ] ).Trim();

			//ShippingCompany.Notes = SelectedAccountShippingAddressNotes.Trim();
			ShippingCompany.Notes = ( (string)line[ CsvAccountnotes ] ).Trim(); //((string)line[CSV_SHIPPINGNOTES]).Trim();

			//ShippingCompany.ZoneName = SelectedAccountShippingZone.Trim();
			ShippingCompany.ZoneName = ( (string)line[ CsvShippingZone ] ).Trim(); //((string)line[CSV_SHIPPINGZONE]).Trim();
		#endregion

			AddUpdatePrimaryCompany UpdatePrimaryCompany = new( "CustomerImportModel" )
			                                               {
				                                               //Password = Encryption.ToTimeLimitedToken(SelectedAccountAdminUserPassword),
				                                               Password        = Encryption.ToTimeLimitedToken( PWord ),
				                                               Company         = Company,
				                                               BillingCompany  = BillingCompany,
				                                               ShippingCompany = ShippingCompany,

				                                               //UserName = SelectedAccountAdminUserName.Trim(),
				                                               UserName           = ( (string)line[ CsvAdminuser ] ).Trim(),
				                                               CustomerCode       = BillingCompany.CompanyNumber.Trim(),
				                                               SuggestedLoginCode = ""
			                                               };
			// TODO Turned off for testing
			var Tmp = await Azure.Client.RequestAddUpdatePrimaryCompany( UpdatePrimaryCompany );
			//Logging.WriteLogLine( "Response: " + Tmp );

			var RCompany = BuildCompany( ResellerCompany );
			string isMandatory = (string)line[CsvReferenceMandatory];
			if (isMandatory.Trim().ToLower() == "true")
			{
				RCompany.EmailAddress2 = Company.EmailAddress2 = BillingCompany.EmailAddress2 = ShippingCompany.EmailAddress2 = "RequireReferenceField";
			}
			RCompany.Notes = ( (string)line[ CsvAccountnotes ] ).Trim();

			if (isNew)
			{
				AddCustomerCompany Acc = new( "CustomerImportModel" )
										 {
											 Company = RCompany,

											 //CustomerCode = SelectedAccountId
											 CustomerCode = AccountId
										 };
				// TODO Turned off for testing
				await Azure.Client.RequestAddCustomerCompany( Acc );
			}
			else
			{
				var Ucc = new UpdateCustomerCompany("CustomerImportModel")
				{
					Company = RCompany,
					CustomerCode = AccountId
				};
				await Azure.Client.RequestUpdateCustomerCompany(Ucc);
			}

			List<Role> Roles = new();

			if( DictAdmins.TryGetValue( AccountId, out var AdminUser ) )
			{
				Logging.WriteLogLine( "Updating " + AccountId );

				//AdminUser.Address.EmailAddress = SelectedAccountAdminUserEmail.Trim();
				AdminUser.Address.EmailAddress = ( (string)line[ CsvAdminemail ] ).Trim();

				//AdminUser.Address.Phone = SelectedAccountAdminUserPhone.Trim();
				AdminUser.Address.Phone = ( (string)line[ CsvAdminph ] ).Trim();

				//AdminUser.Address.Mobile = SelectedAccountAdminUserCell.Trim();
				AdminUser.Address.Mobile = ( (string)line[ CsvAdmincell ] ).Trim();

				//AdminUser.Password = SelectedAccountAdminUserPassword;
				AdminUser.Password = PWord;

				//AdminUser.Enabled = SelectedAccountAdminUserEnabled;
				AdminUser.Enabled = true;

				//AdminUser.FirstName = SelectedAccountAdminUserFirstName.Trim();
				AdminUser.FirstName = ( (string)line[ CsvAdminfirstname ] ).Trim();

				//AdminUser.LastName = SelectedAccountAdminUserLastName.Trim();
				AdminUser.LastName = ( (string)line[ CsvAdminlastname ] ).Trim();

				if( WebAdminRole is not null )
					Roles.Add( WebAdminRole );
			}
			else
			{
				Logging.WriteLogLine( "Adding " + AccountId );

				Address Address = new()
				                  {
					                  //EmailAddress = SelectedAccountAdminUserEmail.Trim(),
					                  EmailAddress = ( (string)line[ CsvAdminemail ] ).Trim(),

					                  //Phone = SelectedAccountAdminUserPhone.Trim(),
					                  Phone = ( (string)line[ CsvAdminph ] ).Trim(),

					                  //Mobile = SelectedAccountAdminUserCell.Trim()
					                  Mobile = ( (string)line[ CsvAdmincell ] ).Trim()
				                  };

				AdminUser = new Protocol.Data.Staff
				            {
					            Address = Address,
					            StaffId = AccountId.Trim(),

					            //Password = SelectedAccountAdminUserPassword,
					            Password = PWord,

					            //Enabled = SelectedAccountAdminUserEnabled,
					            Enabled = true,

					            //FirstName = SelectedAccountAdminUserFirstName.Trim(),
					            FirstName = ( (string)line[ CsvAdminfirstname ] ).Trim(),

					            //LastName = SelectedAccountAdminUserLastName.Trim()
					            LastName = ( (string)line[ CsvAdminlastname ] ).Trim()
				            };

				if( WebAdminRole is not null )
					Roles.Add( WebAdminRole );
			}
			AdminUser.Password = Encryption.ToTimeLimitedToken( AdminUser.Password );

			UpdateStaffMemberWithRolesAndZones UpdateData = new( "CustomerImportModel", AdminUser )
			                                                {
				                                                Roles = Roles
			                                                };
			// TODO Turned off for testing
			await Azure.Client.RequestUpdateStaffMember( UpdateData );

			// TODO Turned off - hangs
			//UpdateTripEntryPagesWithNewAccounts();
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown when saving Account Details: " + E );
		}

		return Errors;
	}

	private async Task<List<string>> AddAccount_V1( Row line )
	{
		List<string> Errors = new();

		//Task.Run(() =>
		//{
		//    Task.WaitAll(GetAccountNamesAsync(),
		//                 GetAdminUsers(),
		//                 GetRoles()
		//                );
		//});

		try
		{
			Company BillingCompany  = new();
			Company ShippingCompany = new();
			var     AccountId       = ( (string)line[ CsvAccount ] ).Trim();
			var     AccountName     = ( (string)line[ CsvAccountname ] ).Trim();

			// Only update if NOT editing - new Account
			var PrimaryCompany = new PrimaryCompany
			                     {
				                     Company =
				                     {
					                     //CompanyNumber = SelectedAccountId.Trim()
					                     CompanyNumber = AccountId
				                     }
			                     };

			var ResellerCompany = new CompanyAddress
			                      {
				                      //CompanyName = SelectedAccountName.Trim(),
				                      CompanyName = AccountName,

				                      //CompanyNumber = SelectedAccountId.Trim()
				                      CompanyNumber = AccountId
			                      };

		#region Primary Company
			var Company = PrimaryCompany.Company;

			//var AccountId = SelectedAccountId.Trim();
			//var PWord = Password.Password.Trim();
			var PWord = ( (string)line[ CsvAdminpw ] ).Trim();

			//company.CompanyName = SelectedAccountName.Trim();
			Company.CompanyNumber = AccountId;

			//Company.CompanyName = SelectedAccountName.Trim();
			Company.CompanyName = AccountName;

			//Company.UserName = SelectedAccountAdminUserName.Trim();
			Company.UserName = ( (string)line[ CsvAdminuser ] ).Trim();
			Company.Password = PWord;

			//Company.ContactName = SelectedAccountSalesRep.Trim();
			Company.ContactName = ( (string)line[ CsvSalesrep ] ).Trim();

			//Company.Phone = SelectedAccountPhone.Trim();
			Company.Phone = ( (string)line[ CsvPhone ] ).Trim();

			//Company.Notes = SelectedAccountNotes.Trim();
			Company.Notes = ( (string)line[ CsvAccountnotes ] ).Trim();

			//Company.Enabled = SelectedAccountIsEnabled;
			Company.Enabled = true;

			//ResellerCompany.Notes = SelectedAccountNotes.Trim();
			ResellerCompany.Notes          = ( (string)line[ CsvAccountnotes ] ).Trim();
			PrimaryCompany.BillingCompany  = BillingCompany;
			PrimaryCompany.ShippingCompany = ShippingCompany;

			//PrimaryCompany.CustomerCode = SelectedAccountId.Trim();
			PrimaryCompany.CustomerCode = AccountId;

			//PrimaryCompany.LoginCode = SelectedAccountId.Trim();
			PrimaryCompany.LoginCode = AccountId;

			//PrimaryCompany.UserName = SelectedAccountAdminUserName.Trim();
			PrimaryCompany.UserName = ( (string)line[ CsvAdminuser ] ).Trim();

			//PrimaryCompany.Password = SelectedAccountAdminUserPassword.Trim();
			PrimaryCompany.Password = PWord;
		#endregion

		#region Billing Company
			BillingCompany.CompanyNumber = AccountId;

			//BillingCompany.CompanyName = SelectedAccountBillingCompanyName.Trim();
			BillingCompany.CompanyName = ( (string)line[ CsvBillingcompany ] ).Trim();

			//BillingCompany.UserName = SelectedAccountBillingAdminName.Trim();
			BillingCompany.UserName = ( (string)line[ CsvBillingname ] ).Trim();
			BillingCompany.Password = PWord;

			//BillingCompany.Phone = SelectedAccountBillingAdminPhone.Trim();
			BillingCompany.Phone = ( (string)line[ CsvBillingphone ] ).Trim();

			//BillingCompany.Suite = SelectedAccountBillingSuite.Trim();
			BillingCompany.Suite = ( (string)line[ CsvBillingsuite ] ).Trim();

			//BillingCompany.AddressLine1 = SelectedAccountBillingStreet.Trim();
			BillingCompany.AddressLine1 = ( (string)line[ CsvBillingstreet ] ).Trim();

			//BillingCompany.City = SelectedAccountBillingCity.Trim();
			BillingCompany.City = ( (string)line[ CsvBillingcity ] ).Trim();

			//BillingCompany.Region = SelectedAccountBillingProvState.Trim();
			BillingCompany.Region = ( (string)line[ CsvBillingprovstate ] ).Trim();

			//BillingCompany.PostalCode = SelectedAccountBillingPostalZip.Trim();
			BillingCompany.PostalCode = ( (string)line[ CsvBillingpczip ] ).Trim();

			//BillingCompany.Country = SelectedAccountBillingCountry.Trim();
			BillingCompany.Country = ( (string)line[ CsvBillingcountry ] ).Trim();

			//BillingCompany.Notes = SelectedAccountBillingNotes.Trim();
			BillingCompany.Notes = ( (string)line[ CsvAccountnotes ] ).Trim(); //((string)line[CSV_BILLINGNOTES]).Trim();

			//BillingCompany.ZoneName = SelectedAccountBillingZone.Trim();
			BillingCompany.ZoneName = ( (string)line[ CsvBillingZone ] ).Trim(); //((string)line[CSV_BILLINGZONE]).Trim();
		#endregion

		#region Shipping Address
			ShippingCompany.CompanyNumber = AccountId;

			//ShippingCompany.CompanyName = ShippingAccountCompanyName.Trim();
			ShippingCompany.CompanyName = ( (string)line[ CsvShippingcompany ] ).Trim();

			//ShippingCompany.UserName = ShippingAccountUserName.Trim();
			ShippingCompany.UserName = ( (string)line[ CsvShippingname ] ).Trim();
			ShippingCompany.Password = PWord;

			//ShippingCompany.Phone = SelectedAccountShippingAdminPhone.Trim();
			ShippingCompany.Phone = ( (string)line[ CsvShippingphone ] ).Trim();

			//ShippingCompany.Suite = SelectedAccountShippingSuite.Trim();
			ShippingCompany.Suite = ( (string)line[ CsvShippingsuite ] ).Trim();

			//ShippingCompany.AddressLine1 = SelectedAccountShippingStreet.Trim();
			ShippingCompany.AddressLine1 = ( (string)line[ CsvShippingstreet ] ).Trim();

			//ShippingCompany.City = SelectedAccountShippingCity.Trim();
			ShippingCompany.City = ( (string)line[ CsvShippingcity ] ).Trim();

			//ShippingCompany.Region = SelectedAccountShippingProvState.Trim();
			ShippingCompany.Region = ( (string)line[ CsvShippingprovstate ] ).Trim();

			//ShippingCompany.PostalCode = SelectedAccountShippingPostalZip.Trim();
			ShippingCompany.PostalCode = ( (string)line[ CsvShippingpczip ] ).Trim();

			//ShippingCompany.Country = SelectedAccountShippingCountry.Trim();
			ShippingCompany.Country = ( (string)line[ CsvShippingcountry ] ).Trim();

			//ShippingCompany.Notes = SelectedAccountShippingAddressNotes.Trim();
			ShippingCompany.Notes = ( (string)line[ CsvAccountnotes ] ).Trim(); //((string)line[CSV_SHIPPINGNOTES]).Trim();

			//ShippingCompany.ZoneName = SelectedAccountShippingZone.Trim();
			ShippingCompany.ZoneName = ( (string)line[ CsvShippingZone ] ).Trim(); //((string)line[CSV_SHIPPINGZONE]).Trim();
		#endregion

			AddUpdatePrimaryCompany UpdatePrimaryCompany = new( "CustomerImportModel" )
			                                               {
				                                               //Password = Encryption.ToTimeLimitedToken(SelectedAccountAdminUserPassword),
				                                               Password        = Encryption.ToTimeLimitedToken( PWord ),
				                                               Company         = Company,
				                                               BillingCompany  = BillingCompany,
				                                               ShippingCompany = ShippingCompany,

				                                               //UserName = SelectedAccountAdminUserName.Trim(),
				                                               UserName           = ( (string)line[ CsvAdminuser ] ).Trim(),
				                                               CustomerCode       = BillingCompany.CompanyNumber.Trim(),
				                                               SuggestedLoginCode = ""
			                                               };
			// TODO Turned off for testing
			var Tmp = await Azure.Client.RequestAddUpdatePrimaryCompany( UpdatePrimaryCompany );
			//Logging.WriteLogLine( "Response: " + Tmp );

			var RCompany = BuildCompany( ResellerCompany );
			string isMandatory = (string)line[CsvReferenceMandatory];
			if (isMandatory.Trim().ToLower() == "true")
			{
				RCompany.EmailAddress2 = Company.EmailAddress2 = BillingCompany.EmailAddress2 = ShippingCompany.EmailAddress2 = "RequireReferenceField";
			}
			RCompany.Notes = ( (string)line[ CsvAccountnotes ] ).Trim();

			AddCustomerCompany Acc = new( "CustomerImportModel" )
			                         {
				                         Company = RCompany,

				                         //CustomerCode = SelectedAccountId
				                         CustomerCode = AccountId
			                         };
			// TODO Turned off for testing
			await Azure.Client.RequestAddCustomerCompany( Acc );

			List<Role> Roles = new();

			if( DictAdmins.TryGetValue( AccountId, out var AdminUser ) )
			{
				Logging.WriteLogLine( "Updating " + AccountId );

				//AdminUser.Address.EmailAddress = SelectedAccountAdminUserEmail.Trim();
				AdminUser.Address.EmailAddress = ( (string)line[ CsvAdminemail ] ).Trim();

				//AdminUser.Address.Phone = SelectedAccountAdminUserPhone.Trim();
				AdminUser.Address.Phone = ( (string)line[ CsvAdminph ] ).Trim();

				//AdminUser.Address.Mobile = SelectedAccountAdminUserCell.Trim();
				AdminUser.Address.Mobile = ( (string)line[ CsvAdmincell ] ).Trim();

				//AdminUser.Password = SelectedAccountAdminUserPassword;
				AdminUser.Password = PWord;

				//AdminUser.Enabled = SelectedAccountAdminUserEnabled;
				AdminUser.Enabled = true;

				//AdminUser.FirstName = SelectedAccountAdminUserFirstName.Trim();
				AdminUser.FirstName = ( (string)line[ CsvAdminfirstname ] ).Trim();

				//AdminUser.LastName = SelectedAccountAdminUserLastName.Trim();
				AdminUser.LastName = ( (string)line[ CsvAdminlastname ] ).Trim();

				if( WebAdminRole is not null )
					Roles.Add( WebAdminRole );
			}
			else
			{
				Logging.WriteLogLine( "Adding " + AccountId );

				Address Address = new()
				                  {
					                  //EmailAddress = SelectedAccountAdminUserEmail.Trim(),
					                  EmailAddress = ( (string)line[ CsvAdminemail ] ).Trim(),

					                  //Phone = SelectedAccountAdminUserPhone.Trim(),
					                  Phone = ( (string)line[ CsvAdminph ] ).Trim(),

					                  //Mobile = SelectedAccountAdminUserCell.Trim()
					                  Mobile = ( (string)line[ CsvAdmincell ] ).Trim()
				                  };

				AdminUser = new Protocol.Data.Staff
				            {
					            Address = Address,
					            StaffId = AccountId.Trim(),

					            //Password = SelectedAccountAdminUserPassword,
					            Password = PWord,

					            //Enabled = SelectedAccountAdminUserEnabled,
					            Enabled = true,

					            //FirstName = SelectedAccountAdminUserFirstName.Trim(),
					            FirstName = ( (string)line[ CsvAdminfirstname ] ).Trim(),

					            //LastName = SelectedAccountAdminUserLastName.Trim()
					            LastName = ( (string)line[ CsvAdminlastname ] ).Trim()
				            };

				if( WebAdminRole is not null )
					Roles.Add( WebAdminRole );
			}
			AdminUser.Password = Encryption.ToTimeLimitedToken( AdminUser.Password );

			UpdateStaffMemberWithRolesAndZones UpdateData = new( "CustomerImportModel", AdminUser )
			                                                {
				                                                Roles = Roles
			                                                };
			// TODO Turned off for testing
			await Azure.Client.RequestUpdateStaffMember( UpdateData );

			// TODO Turned off - hangs
			//UpdateTripEntryPagesWithNewAccounts();
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown when saving Account Details: " + E );
		}

		return Errors;
	}

	private static void UpdateTripEntryPagesWithNewAccounts()
	{
		Dispatcher.Invoke(() =>
		{
			var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

			foreach (var Ti in Tis)
			{
				if (Ti.Content is TripEntry)
				{
					Logging.WriteLogLine("Updating Accounts in extant " + Globals.TRIP_ENTRY + " tabs");

					// Reload
					var Ti1 = Ti;

					((TripEntryModel)Ti1.Content.DataContext).ReloadPrimaryCompanies();
				}
			}
		});
	}

	private static Company BuildCompany( CompanyBase ca ) =>

		// Used by Execute_Delete
		new()
		{
			LocationBarcode = ca.LocationBarcode,
			Suite           = ca.Suite,
			AddressLine1    = ca.AddressLine1,
			City            = ca.City,
			Country         = ca.Country,
			CompanyName     = ca.CompanyName,
			PostalCode      = ca.PostalCode,
			Region          = ca.Region,

			Phone        = ca.Phone,
			EmailAddress = ca.EmailAddress,
			Notes        = ca.Notes
		};

	public async Task GetRoles()
	{
		var Roles = await Azure.Client.RequestRoles();

		WebAdminRole = ( from R in Roles
		                 where R.Name == "Web Admin"
		                 select R ).FirstOrDefault();

		if( WebAdminRole == null )
		{
			Logging.WriteLogLine( "Web Admin role is null - Creating and saving" );

			WebAdminRole = new Role
			               {
				               Name            = "Web Admin",
				               IsAdministrator = false,
				               IsDriver        = false,
				               IsWebService    = false,
				               Mandatory       = false
			               };

			await Azure.Client.RequestAddUpdateRole( WebAdminRole );

			Roles = await Azure.Client.RequestRoles();

			WebAdminRole = ( from R in Roles
			                 where R.Name == "Web Admin"
			                 select R ).FirstOrDefault();

			if( WebAdminRole != null )
				Logging.WriteLogLine( "Added Web Admin Role: " + WebAdminRole.Name );
			else
				Logging.WriteLogLine( "ERROR Failed adding Web Admin Role" );
		}
	}

	private async Task<List<string>> GetAccountNamesAsync()
	{
		try
		{
			if( !IsInDesignMode )
			{
				Logging.WriteLogLine( "Loading Companies" );

				AccountNames = ( from Company in await Azure.Client.RequestGetCustomerCodeCompanyNameList()
				                 let CompanyName = Company.CompanyName.Trim().ToLower()
				                 orderby CompanyName
				                 group Company by CompanyName
				                 into G
				                 select G.First().CompanyName ).ToList();

				//SelectedAccountBillingCountry = SelectedAccountShippingCountry = Names.Count > 0 ? Names[0] : "";
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( $"Exception thrown fetching customers:\n{E}" );
		}
		return new List<string>();
	}

	public async Task GetAdminUsers()
	{
		try
		{
			if( !IsInDesignMode )
			{
				Logging.WriteLogLine( "Loading Admin Users" );
				List<Protocol.Data.Staff> StaffList = new();

				foreach( var A in await Azure.Client.RequestGetAdministrators() )
				{
					var S = await Azure.Client.RequestGetStaffMember( A.StaffId );

					if( S is not null )
					{
						S.Password = Encryption.FromTimeLimitedToken( S.Password ).Value;
						StaffList.Add( S );
					}
				}

				DictAdmins = ( from S in StaffList
				               select S ).ToDictionary( s => s.StaffId, s => s );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( $"Exception thrown fetching Administrators:\n{E}" );
		}
	}
#endregion
}