﻿#nullable enable

using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using IdsControlLibraryV2.TabControl;
using ViewModels.Customers.AddressBook;
using ViewModels.Staff.ShiftRates;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Customers.Accounts;

/// <summary>
///     Interaction logic for Accounts.xaml
/// </summary>
public partial class Accounts : Page
{
	public Accounts()
	{
		InitializeComponent();

		Model = (AccountsModel)DataContext;
		// Trying to force loading into sidebar
		// var tmp = Model.AccountNames;
		//Model.AccountNames = Model.GetAccountNames();

		//this.lvAccounts.SelectedIndex = 0;
		//this.LvAccounts_MouseDoubleClick(this.lvAccounts, null);

		// this.BtnCollapse_Click(null, null);
		Model.Password        = pbAccountAdminPassword;
		Model.ConfirmPassword = pbAccountAdminConfirmPassword;
		Model.View            = this;

		Model.OnShowSaved = () =>
							{
								Globals.Dialogues.Inform.Show( "AccountsShiftsSaved" );
							};
	}

	public readonly Dictionary<string, Control> DictControls = new();

	private readonly AccountsModel Model;

	private void LoadMappingDictionary()
	{
		foreach( var Ctrl in this.GetChildren() )
		{
			var Binding = Ctrl switch
						  {
							  TextBox Tb         => BindingOperations.GetBinding( Tb, TextBox.TextProperty ),
							  ComboBox Cb        => BindingOperations.GetBinding( Cb, ItemsControl.ItemsSourceProperty ),
							  DateTimePicker Dtp => BindingOperations.GetBinding( Dtp, DateTimePicker.ValueProperty ),
							  _                  => null
						  };

			if( Binding is not null )
			{
				var Path = Binding.Path.Path;

				if( !DictControls.ContainsKey( Path ) )
					DictControls.Add( Path, (Control)Ctrl );
				else
					Logging.WriteLogLine( "ERROR " + Path + " already exists" );
			}
		}
	}

	private static void DisplayNotImplementedMessage()
	{
		MessageBox.Show( "Not implemented yet", "Not Implemented", MessageBoxButton.OK, MessageBoxImage.Information );
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void BtnSave_Click( object sender, RoutedEventArgs e )
	{
		//this.DisplayNotImplementedMessage();
		if( DictControls.Count == 0 )
			LoadMappingDictionary();
		Model.SelectedAccountAdminUserPassword = pbAccountAdminPassword.Password;
		Model.SelectedAccountAdminUserName     = tbAccountAdminUser.Text;
		//var Errors = await Model.SaveAccountDetails( DictControls );
		var Errors = Model.SaveAccountDetails( DictControls );

		if( Errors.Count > 0 )
		{
			var Caption = (string)Application.Current.TryFindResource( "AccountsValidationTitle" );
			var Sb      = new StringBuilder();
			Sb.Append( (string)Application.Current.TryFindResource( "AccountsValidationErrors" ) ).Append( "\n" );

			foreach( var Error in Errors )
				Sb.Append( "\t" ).Append( Error ).Append( "\n" );
			Sb.Append( (string)Application.Current.TryFindResource( "AccountsValidationErrors1" ) );
			MessageBox.Show( Sb.ToString(), Caption, MessageBoxButton.OK, MessageBoxImage.Error );
		}
		else
		{
			var Caption = (string)Application.Current.TryFindResource( "AccountsAccountSavedTitle" );
			var Message = (string)Application.Current.TryFindResource( "AccountsAccountSaved" );
			MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Asterisk );

			// Reload the account to lock the accountid field
			//var SelectedAccountName = Model.SelectedAccountName;
			BtnClear_Click( null, null );
			//Model.SelectedAccountName = SelectedAccountName;
		}
	}

	private void BtnFind_Click( object sender, RoutedEventArgs e )
	{
		DisplayNotImplementedMessage();
	}

	private void BtnAudit_Click( object sender, RoutedEventArgs e )
	{
		DisplayNotImplementedMessage();
	}

	private void BtnAddressBook_Click( object sender, RoutedEventArgs e )
	{
		//this.DisplayNotImplementedMessage();
		// This opens the main address book without an account selected
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( Tis.Count > 0 )
		{
			PageTabItem? Pti = null;

			for( var I = 0; I < Tis.Count; I++ )
			{
				var Ti = Tis[ I ];

				if( Ti.Content is AddressBook.AddressBook )
				{
					Pti = Ti;
					Logging.WriteLogLine( "Switching to extant " + Globals.ADDRESS_BOOK + " tab" );

					//((AddressBook.AddressBookModel)pti.Content.DataContext).CurrentTrip = selectedTrip;
					Globals.DataContext.MainDataContext.TabControl.SelectedIndex = I;
					break;
				}
			}

			if( Pti is null )
			{
				// Need to create a new Trip Entry tab
				Logging.WriteLogLine( "Opening new " + Globals.ADDRESS_BOOK + " tab" );
				Globals.RunProgram( Globals.ADDRESS_BOOK );
			}
		}
	}

	private void BtnUsers_Click( object sender, RoutedEventArgs e )
	{
		//this.DisplayNotImplementedMessage();
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( Tis.Count > 0 )
		{
			PageTabItem? Pti = null;

			for( var I = 0; I < Tis.Count; I++ )
			{
				var Ti = Tis[ I ];

				if( Ti.Content is MaintainStaff )
				{
					Pti = Ti;
					Logging.WriteLogLine( "Switching to extant " + Globals.MAINTAIN_STAFF + " tab" );

					//((AddressBook.AddressBookModel)pti.Content.DataContext).CurrentTrip = selectedTrip;
					Globals.DataContext.MainDataContext.TabControl.SelectedIndex = I;
					break;
				}
			}

			if( Pti == null )
			{
				Logging.WriteLogLine( "Opening new " + Globals.MAINTAIN_STAFF + " tab" );
				Globals.RunProgram( Globals.MAINTAIN_STAFF );
			}
		}
	}

	private void BtnCollapse_Click( object sender, RoutedEventArgs e )
	{
		spSidePanel.Visibility = Visibility.Collapsed;
		btnExpand.Visibility   = Visibility.Visible;
	}

	private void BtnExpand_Click( object sender, RoutedEventArgs e )
	{
		spSidePanel.Visibility = Visibility.Visible;
		btnExpand.Visibility   = Visibility.Collapsed;
	}

	private void BtnAddressBookForAccount_MouseUp( object sender, MouseButtonEventArgs e )
	{
		Logging.WriteLogLine( "Show Address Book for Account clicked" );

		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( Tis.Count > 0 )
		{
			PageTabItem? Pti = null;

			for( var I = 0; I < Tis.Count; I++ )
			{
				var Ti = Tis[ I ];

				if( Ti.Content is AddressBook.AddressBook )
				{
					Pti = Ti;

					// Load the trip into the first TripEntry tab
					Logging.WriteLogLine( "Switching to extant " + Globals.ADDRESS_BOOK + " tab" );
					( (AddressBookModel)Pti.Content.DataContext ).SelectedAccountName = Model.SelectedAccountName;
					Globals.DataContext.MainDataContext.TabControl.SelectedIndex      = I;
					break;
				}
			}

			if( Pti is null )
			{
				Logging.WriteLogLine( "Opening new " + Globals.ADDRESS_BOOK + " tab" );

				// TODO this does open a new tab but it doesn't load the desired account's addresses
				Globals.RunProgram( Globals.ADDRESS_BOOK, Model.SelectedAccountName );
			}
		}
	}

	private void CbDrivers_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		DisplayNotImplementedMessage();
	}

	private void BtnSendWelcomeEmail_MouseUp( object sender, MouseButtonEventArgs e )
	{
		Logging.WriteLogLine( "Send Welcome Email clicked" );
		// TODO Send welcome email
	}

	private void BtnUserCallersList_MouseUp( object sender, MouseButtonEventArgs e )
	{
		Logging.WriteLogLine( "Show User Callers List clicked" );
		// TODO Show User callers list
	}

	private void imageRevealPassword_MouseDown( object sender, MouseButtonEventArgs e )
	{
		imageRevealPassword.Source               = new BitmapImage( new Uri( "/ViewModels;component/Resources/Images/eye-open-24.png", UriKind.RelativeOrAbsolute ) );
		ViewPassword.Text                        = pbAccountAdminPassword.Password;
		pbAccountAdminPassword.Visibility        = Visibility.Hidden;
		ViewPassword.Visibility                  = Visibility.Visible;
		ViewConfirmPassword.Text                 = pbAccountAdminConfirmPassword.Password;
		pbAccountAdminConfirmPassword.Visibility = Visibility.Hidden;
		ViewConfirmPassword.Visibility           = Visibility.Visible;
	}

	private void imageRevealPassword_MouseLeave( object sender, MouseEventArgs e )
	{
		imageRevealPassword.Source               = new BitmapImage( new Uri( "/ViewModels;component/Resources/Images/eye-closed-24.png", UriKind.RelativeOrAbsolute ) );
		ViewPassword.Text                        = string.Empty;
		pbAccountAdminPassword.Visibility        = Visibility.Visible;
		ViewPassword.Visibility                  = Visibility.Hidden;
		ViewConfirmPassword.Text                 = string.Empty;
		pbAccountAdminConfirmPassword.Visibility = Visibility.Visible;
		ViewConfirmPassword.Visibility           = Visibility.Hidden;
	}

	private void imageRevealPassword_MouseUp( object sender, MouseButtonEventArgs e )
	{
		//Utils.Logging.WriteLogLine("Hiding password");
		imageRevealPassword.Source               = new BitmapImage( new Uri( "/ViewModels;component/Resources/Images/eye-closed-24.png", UriKind.RelativeOrAbsolute ) );
		ViewPassword.Text                        = string.Empty;
		pbAccountAdminPassword.Visibility        = Visibility.Visible;
		ViewPassword.Visibility                  = Visibility.Hidden;
		ViewConfirmPassword.Text                 = string.Empty;
		pbAccountAdminConfirmPassword.Visibility = Visibility.Visible;
		ViewConfirmPassword.Visibility           = Visibility.Hidden;
	}

	private void LvAccounts_MouseDoubleClick( object? sender, MouseButtonEventArgs? e )
	{
		var SelectedAccountName = (string)lvAccounts.SelectedItem;
		tbAccountId.IsReadOnly   = true;
		tbAccountName.IsReadOnly = true;
		Logging.WriteLogLine( "Double-clicked Account in list: " + SelectedAccountName );
		//BtnClear_Click( null, null );
		Model.SelectedAccountName = SelectedAccountName;
	}

	private void CbAccountBillingCountry_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		Model.SelectedBillingCountry            = (string)cbAccountBillingCountry.SelectedItem;
		cbAccountBillingProvState.SelectedIndex = 0;
	}

	private void CbAccountShippingCountry_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		Model.SelectedShippingCountry            = (string)cbAccountShippingCountry.SelectedItem;
		cbAccountShippingProvState.SelectedIndex = 0;
	}

	private void TbFilter_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( sender is TextBox Tb )
			Model.Execute_Filter( Tb.Text.Trim() );
	}

	private void BtnHelp_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening " + Model.HelpUri );
		var Uri = new Uri( Model.HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	private void BtnEdit_Click( object sender, RoutedEventArgs e )
	{
		if( lvAccounts.SelectedIndex > -1 )
			LvAccounts_MouseDoubleClick( null, null );
	}

	private void BtnAddPackageOverride_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Adding package override" );

		if( Model.SelectedAccountName.IsNotNullOrWhiteSpace() && Model.SelectedOverrideBasePackageType.IsNotNullOrWhiteSpace() && Model.SelectedOverrideNewPackageType.IsNotNullOrWhiteSpace() )
		{
			var Exists = Model.DoesPackageOverrideExist( Model.SelectedOverrideBasePackageType, Model.SelectedOverrideNewPackageType );

			if( !Exists )
			{
				Logging.WriteLogLine( "BasePackageType: " + Model.SelectedOverrideBasePackageType + " NewPackageType: " + Model.SelectedOverrideNewPackageType );
				Model.AddUpdatePackageOverride();
			}
			else
			{
				var Caption = FindStringResource( "AccountsValidationDuplicateOverrideTitle" );
				var Message = FindStringResource( "AccountsValidationDuplicateOverrideMessage" );
				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
			}
		}
	}

	private void BtnRemovePackageOverride_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Removing package override" );

		if( Model.SelectedAccountName.IsNotNullOrWhiteSpace() )
			Model.DeletePackageOverride( LvOverrides.SelectedIndex );
	}

	private void LvOverrides_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		if( Model.SelectedAccountName.IsNotNullOrWhiteSpace() )
			Model.LoadPackageOverride( LvOverrides.SelectedIndex );
	}

	private void BtnAddAccountDiscount_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Adding account discount" );

		if( Model.SelectedAccountName.IsNotNullOrWhiteSpace() )
			Model.AddUpdatePackageRedirect();
	}

	private void BtnRemoveAccountDiscount_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Removing account discount" );

		if( Model.SelectedAccountName.IsNotNullOrWhiteSpace() )
			Model.DeletePackageRedirect( LvDiscounts.SelectedIndex );
	}

	private void LvDiscounts_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		if( Model.SelectedAccountName.IsNotNullOrWhiteSpace() )
			Model.LoadPackageRedirect( LvDiscounts.SelectedIndex );
	}

	private void CbDiscountType_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( TbDiscountValue != null )
		{
			switch( CbDiscountType.SelectedIndex )
			{
			case 0:
				if( TbDiscountValue.Text is "" or "0.1" )
				{
					TbDiscountValue.Text    = "0";
					TbDiscountValue.ToolTip = FindStringResource( "AccountsPricingDiscountsGridLabelsValueTooltipOverride" );
					CbDiscountType.ToolTip  = FindStringResource( "AccountsPricingDiscountsGridLabelsDiscountTypeTooltipOverride" );
				}
				break;

			case 1:
				if( TbDiscountValue.Text is "" or "0" )
				{
					TbDiscountValue.Text    = "0.1";
					TbDiscountValue.ToolTip = FindStringResource( "AccountsPricingDiscountsGridLabelsValueTooltipPercentage" );
					CbDiscountType.ToolTip  = FindStringResource( "AccountsPricingDiscountsGridLabelsDiscountTypeTooltipPercentage" );
				}
				break;
			}
		}
	}

	/*
	private void BtnDefaultChargesUpdate_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is AccountsModel Model )
		{
			// Walk through the screen

			Model.Execute_UpdateDefaultCharges();
		}
	}
	*/
	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public void BtnClear_Click( object? sender, RoutedEventArgs? e )
	{
		//this.DisplayNotImplementedMessage();
		Model.ClearAccountDetails();
		Model.ClearUserDetails();
		Model.ClearErrorConditions();
		tbAccountId.IsReadOnly                 = false;
		tbAccountName.IsReadOnly               = false;
		cbAccountBillingCountry.SelectedIndex  = -1;
		cbAccountShippingCountry.SelectedIndex = -1;
		cbAccountBillingZone.SelectedIndex     = -1;
		cbAccountShippingZone.SelectedIndex    = -1;
	}

	public void UpdateGrid()
	{
		DgDefaultCharges.ItemsSource = null;

		DgDefaultCharges.ItemsSource = Model.RawCharges;

		//foreach (AccountsModel.DisplayCharge item in Model.RawCharges)
		//{
		//}
	}
}