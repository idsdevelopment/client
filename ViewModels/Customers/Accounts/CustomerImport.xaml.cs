﻿using Microsoft.Win32;

namespace ViewModels.Customers.Accounts;

/// <summary>
///     Interaction logic for CustomerImport.xaml
/// </summary>
public partial class CustomerImport : Window
{
	private readonly CustomerImportModel Model;

	public CustomerImport( Window owner )
	{
		InitializeComponent();

		if( DataContext is CustomerImportModel model )
		{
			Model = model;

			_ = ShowDialog();
		}
	}

	private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	private void BtnBrowse_Click( object sender, RoutedEventArgs e )
	{
		var filter = "CSV Files (*.csv)|*.csv|Text Files (*.txt)|*txt";

		var ofd = new OpenFileDialog
		          {
			          Filter = filter
		          };
		ofd.Multiselect = false;
		var result = ofd.ShowDialog();

		if( result == true )
		{
			Logging.WriteLogLine( "Selected " + ofd.FileName );
			var file = ofd.FileName;
			TbResults.Text = string.Empty;

			Model.SelectedImportFile = file;
			tbFileName.Text          = file;
			var (isCorrect, errors)  = Model.CheckHeaders();

			if( !isCorrect )
			{
				//Model.SelectedImportFile    = string.Empty;
				Model.IsImportButtonEnabled = false;
				var title   = FindStringResource( "AddressBookImportAddressesFormatErrorsInFileHeaderTitle" );
				var message = FindStringResource( "AddressBookImportAddressesFormatErrorsInFileHeader" );
				message = message.Replace( "@1", file );

				foreach( var error in errors )
					message += "\n\t" + error;
				message += "\n" + FindStringResource( "AddressBookImportAddressesFormatErrorsInFileHeaderEnd" );

				//Model.Results = message;
				TbResults.Text = message;

				NotepadHelper.ShowMessage( message, "Errors" );

				MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
			}
			else
			{
				( isCorrect, errors ) = Model.CheckFileContents();

				if( !isCorrect )
				{
					//Model.SelectedImportFile    = string.Empty;
					Model.IsImportButtonEnabled = false;
					var title   = FindStringResource( "AddressBookImportAddressesErrorsInFileTitle" );
					var message = FindStringResource( "AddressBookImportAddressesErrorsInFile" );
					message = message.Replace( "@1", file );

					foreach( var error in errors )
						message += "\n" + error;
					message += "\n" + FindStringResource( "AddressBookImportAddressesErrorsInFileEnd" );

					//Model.Results = message;
					TbResults.Text = message;

					NotepadHelper.ShowMessage( message, "Errors" );

					MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
				}
				else
				{
					Model.IsImportButtonEnabled = true;
					BtnImport.IsEnabled         = true;
				}
			}
		}
	}

	private void BtnImport_Click( object sender, RoutedEventArgs e )
	{
		var (numberImported, numberSkipped, errors) = Model.Import();

		if( errors.Count > 0 )
			Model.Results = string.Join( "\n", errors.ToArray() );
		var title   = FindStringResource( "CustomerImportAccountsImportedTitle" );
		var message = FindStringResource( "CustomerImportAccountsImportedMessage" );
		message = message.Replace( "@1", numberImported + "" );

		if( numberSkipped > 0 )
		{
			var message2 = FindStringResource( "CustomerImportAccountsSkippedMessage" );
			message2 =  message2.Replace( "@1", numberSkipped + "" );
			message  += "\n" + message2;
		}
		MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );

		Model.SelectedImportFile    = string.Empty;
		Model.IsImportButtonEnabled = false;
	}

	private void BtnClose_Click( object sender, RoutedEventArgs e )
	{
		var close = false;

		if( Model.IsImportRunning )
		{
			// TODO Say something
		}
		else
			close = true;

		if( close )
			Close();
	}
}