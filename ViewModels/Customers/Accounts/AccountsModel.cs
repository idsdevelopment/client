﻿#nullable enable

using System.Collections;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModels.Staff.ShiftRates;
using ViewModels.Trips.TripEntry;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;
using MessageBox = System.Windows.MessageBox;
using Selector = System.Windows.Controls.Primitives.Selector;

namespace ViewModels.Customers.Accounts;

public class SelectedShift : DisplayShift
{
	public bool Selected
	{
		get => _Selected1;
		set
		{
			_Selected1 = value;
			Modified();
		}
	}

	public string SelectedStartDay => DAYS_OF_WEEK[ StartDay ];
	public string SelectStart      => ToTime( Start.TimeOfDay );

	public string SelectedEndDay => DAYS_OF_WEEK[ EndDay ];
	public string SelectEnd      => ToTime( End.TimeOfDay );

	public string SelectedDuration => ToTime( ShiftExtensions.Duration( StartDay, Start.TimeOfDay, EndDay, End.TimeOfDay ) );

	public SelectedShift( StaffShift s, StaffShifts companyShifts ) : base( s )
	{
		var N = Name;

		foreach( var Cs in companyShifts )
		{
			if( Cs.ShiftName == N )
			{
				Selected = true;
				break;
			}
		}
	}

	private bool _Selected1;

	private static string ToTime( TimeSpan t ) => $"{t:hh\\:\\:mm\\:ss}";
}

public class SelectedShifts : ObservableCollection<SelectedShift>
{
}

public class DisplayAccountDiscount
{
	public string Service      { get; set; } = string.Empty;
	public string Package      { get; set; } = string.Empty;
	public string DiscountType { get; set; } = string.Empty;
	public string FromZone     { get; set; } = string.Empty;
	public string ToZone       { get; set; } = string.Empty;
	public string StringValue  { get; set; } = string.Empty;
}

public class AccountsModel : ViewModelBase
{
	private const string PROGRAM = "Account Details";

	public class DisplayCharge : Charge
	{
		public bool IsOverrideApplied { get; set; }
		public bool IsChargeApplied   { get; set; }

		public DisplayCharge( Charge c )
		{
			Id                       = c.Id;
			AccountId                = c.AccountId;
			AlwaysApplied            = c.AlwaysApplied;
			ChargeId                 = c.ChargeId;
			ChargeType               = c.ChargeType;
			DisplayOnDriversScreen   = c.DisplayOnDriversScreen;
			DisplayOnEntry           = c.DisplayOnEntry;
			DisplayOnInvoice         = c.DisplayOnInvoice;
			DisplayOnWaybill         = c.DisplayOnWaybill;
			DisplayType              = c.DisplayType;
			ExcludeFromFuelSurcharge = c.ExcludeFromFuelSurcharge;
			ExcludeFromTaxes         = c.ExcludeFromTaxes;
			Formula                  = c.Formula;
			IsDiscountable           = c.IsDiscountable;
			IsFormula                = c.IsFormula;
			IsFuelSurcharge          = c.IsFuelSurcharge;
			IsMultiplier             = c.IsMultiplier;
			IsOverride               = c.IsOverride;
			IsTax                    = c.IsTax;
			IsText                   = c.IsText;
			IsVolumeCharge           = c.IsVolumeCharge;
			IsWeightCharge           = c.IsWeightCharge;
			Label                    = c.Label;
			Max                      = c.Max;
			Min                      = c.Min;
			OverridenPackage         = c.OverridenPackage;
			SortIndex                = c.SortIndex;
			Text                     = c.Text;
			Value                    = c.Value;

			if( c.SortIndex <= -1 )
				IsChargeApplied = true;

			if( c.SortIndex <= -2 )
				IsOverrideApplied = true;

			if( c is DisplayCharge Dc )
			{
				IsOverrideApplied = Dc.IsOverrideApplied;
				IsChargeApplied   = Dc.IsChargeApplied;
			}
		}

		public Charge GetAsCharge()
		{
			Charge Charge = new()
							{
								Id                       = Id,
								AccountId                = AccountId,
								AlwaysApplied            = AlwaysApplied,
								ChargeId                 = ChargeId,
								ChargeType               = ChargeType,
								DisplayOnDriversScreen   = DisplayOnDriversScreen,
								DisplayOnEntry           = DisplayOnEntry,
								DisplayOnInvoice         = DisplayOnInvoice,
								DisplayOnWaybill         = DisplayOnWaybill,
								DisplayType              = DisplayType,
								ExcludeFromFuelSurcharge = ExcludeFromFuelSurcharge,
								ExcludeFromTaxes         = ExcludeFromTaxes,
								Formula                  = Formula,
								IsDiscountable           = IsDiscountable,
								IsFormula                = IsFormula,
								IsFuelSurcharge          = IsFuelSurcharge,
								IsMultiplier             = IsMultiplier,
								IsOverride               = IsOverride,
								IsTax                    = IsTax,
								IsText                   = IsText,
								IsVolumeCharge           = IsVolumeCharge,
								IsWeightCharge           = IsWeightCharge,
								Label                    = Label,
								Max                      = Max,
								Min                      = Min,
								OverridenPackage         = OverridenPackage,
								SortIndex                = SortIndex,
								Text                     = Text,
								Value                    = Value
							};

			return Charge;
		}
	}

	public List<string> Drivers { get; set; } = new();

	public Role? AdminRole,
				 DriveRole,
				 WebAdminRole;

	private Dictionary<string, Protocol.Data.Staff> DictAdmins = new();

	private Dictionary<string, CustomerCodeCompanyName> DictCustomerNamesIds = new();

#region Pricing Shifts
#region Common
	public ICommand PricingShiftSave => Commands[ nameof( PricingShiftSave ) ];

	public Action? OnShowSaved;

	public async void Execute_PricingShiftSave()
	{
		var Update = new UpdateCompanyShifts
					 {
						 ProgramName = PROGRAM,
						 CompanyName = SelectedAccountId
					 };

		Update.Shifts.AddRange( from S in Shifts
							    where S.Selected
							    select S.Name );

		await Azure.Client.RequestUpdateCompanyShifts( Update );

		foreach( var Shift in Shifts )
			Shift.Modified( false );

		OnShowSaved?.Invoke();

		PricingShiftSaveEnable = false;
	}

	public bool PricingShiftSaveEnable
	{
		get { return Get( () => PricingShiftSaveEnable, false ); }
		set { Set( () => PricingShiftSaveEnable, value ); }
	}
#endregion

#region Pricing
	public bool PricingVisible
	{
		get { return Get( () => PricingVisible, false ); }
		set { Set( () => PricingVisible, value ); }
	}

	[DependsUpon( nameof( PricingVisible ) )]
	public void WhenPricingVisibilityChanges()
	{
		if( !InChange )
		{
			InChange = true;

			try
			{
				var Vis = PricingVisible;
				PricingOrStaffVisible = Vis || EmployeeVisible;

				Vis             = !Vis;
				AccountVisible  = Vis;
				EmployeeVisible = Vis;
			}
			finally
			{
				InChange = false;
			}
		}
	}

#region Package Overrides
	public string SelectedOverrideBasePackageType
	{
		get { return Get( () => SelectedOverrideBasePackageType, "" ); }
		set { Set( () => SelectedOverrideBasePackageType, value ); }
	}

	public string SelectedOverrideNewPackageType
	{
		get { return Get( () => SelectedOverrideNewPackageType, "" ); }
		set { Set( () => SelectedOverrideNewPackageType, value ); }
	}

	public OverridePackage? SelectedPackageOverride { get; set; }

	public ObservableCollection<OverridePackage> PackageOverrides
	{
		get => Get( () => PackageOverrides, new ObservableCollection<OverridePackage>() );
		set { Set( () => PackageOverrides, value ); }
	}

	public ObservableCollection<string> DisplayPackageOverrides
	{
		get => Get( () => DisplayPackageOverrides, new ObservableCollection<string>() );
		set { Set( () => DisplayPackageOverrides, value ); }
	}

	public ObservableCollection<string> GetDisplayPackageOverrides()
	{
		ObservableCollection<string> Pos = new();

		foreach( var Override in PackageOverrides )
		{
			var BasePt = ( from P in PackageTypes
						   where P.PackageTypeId == Override.FromPackageTypeId
						   select P ).FirstOrDefault();

			var NewPt = ( from P in PackageTypes
						  where P.PackageTypeId == Override.ToPackageTypeId
						  select P ).FirstOrDefault();

			if( BasePt is not null && NewPt is not null )
				DisplayPackageOverrides.Add( FormatPackageOverride( BasePt.Description, NewPt.Description ) );
		}

		return Pos;
	}

	public string FormatPackageOverride( string basePackage, string newPackage ) => $"{basePackage} -> {newPackage}";
#endregion

#region Package Discounts
	// NOTE These are called PackageRedirects
	public string SelectedDiscountServiceLevelName
	{
		get { return Get( () => SelectedDiscountServiceLevelName, "" ); }
		set { Set( () => SelectedDiscountServiceLevelName, value ); }
	}

	public string SelectedDiscountPackageType
	{
		get { return Get( () => SelectedDiscountPackageType, "" ); }
		set { Set( () => SelectedDiscountPackageType, value ); }
	}

	public string SelectedDiscountType
	{
		get { return Get( () => SelectedDiscountType, "" ); }
		set { Set( () => SelectedDiscountType, value ); }
	}

	public List<string> DiscountTypes
	{
		get { return Get( () => DiscountTypes, GetDiscountTypes() ); }
	}

	public List<string> GetDiscountTypes()
	{
		List<string> Strings = new()
							   {
								   FindStringResource( "AccountsPricingDiscountsDiscountTypeReplace" ),
								   FindStringResource( "AccountsPricingDiscountsDiscountTypePercentage" )
							   };

		return Strings;
	}

	public List<string> DiscountZones
	{
		get { return Get( () => DiscountZones, GetDiscountZones() ); }
	}

	public List<string> GetDiscountZones()
	{
		List<string> ZonesList = new();

		// zones.AddRange(Zones);
		foreach( var Zone in Zones )
		{
			// Weed out any blanks
			if( Zone.IsNotNullOrWhiteSpace() )
				ZonesList.Add( Zone );
		}
		ZonesList.Insert( 0, FindStringResource( "AccountsPricingDiscountsAll" ) );

		return ZonesList;
	}

	public string SelectedFromZone
	{
		get { return Get( () => SelectedFromZone, "" ); }
		set { Set( () => SelectedFromZone, value ); }
	}

	public string SelectedToZone
	{
		get { return Get( () => SelectedToZone, "" ); }
		set { Set( () => SelectedToZone, value ); }
	}

	public decimal Value
	{
		get { return Get( () => Value, 0 ); }
		set { Set( () => Value, value ); }
	}

	public RedirectPackage? SelectedPackageRedirect { get; set; }

	public List<RedirectPackage> AccountDiscounts
	{
		get => Get( () => AccountDiscounts, new List<RedirectPackage>() );
		set => Set( () => AccountDiscounts, value );
	}

	public ObservableCollection<DisplayAccountDiscount> DisplayAccountDiscounts
	{
		get { return Get( () => DisplayAccountDiscounts, GetDisplayAccountDiscounts() ); }
		set { Set( () => DisplayAccountDiscounts, value ); }
	}

	public ObservableCollection<DisplayAccountDiscount> GetDisplayAccountDiscounts()
	{
		ObservableCollection<DisplayAccountDiscount> Oc = new();

		return Oc;
	}
#endregion
#endregion

#region Shifts
	public SelectedShifts Shifts
	{
		get { return Get( () => Shifts, new SelectedShifts() ); }
		set { Set( () => Shifts, value ); }
	}
#endregion
#endregion

#region ToolBar Visibility
	public bool PricingOrStaffVisible
	{
		get { return Get( () => PricingOrStaffVisible, false ); }
		set { Set( () => PricingOrStaffVisible, value ); }
	}

#region Employee
#region Actions
	public ICommand EmployeeSelectAll => Commands[ nameof( EmployeeSelectAll ) ];

	public void Execute_EmployeeSelectAll()
	{
		foreach( var Shift in Shifts )
			Shift.Selected = true;
	}

	public ICommand EmployeeClearAll => Commands[ nameof( EmployeeClearAll ) ];

	public void Execute_EmployeeClearAll()
	{
		foreach( var Shift in Shifts )
			Shift.Selected = false;
	}
#endregion

	public bool EmployeeVisible
	{
		get { return Get( () => EmployeeVisible, false ); }
		set { Set( () => EmployeeVisible, value ); }
	}

	private StaffShifts? BufferedShifts;

	[DependsUpon( nameof( SelectedAccountName ) )]
	public async Task WhenSelectedAccountNameChanges()
	{
		PricingShiftSaveEnable = false;
		var Account = SelectedAccountId;

		if( Account.IsNotNullOrWhiteSpace() )
		{
			var StaffShifts   = BufferedShifts ??= await Azure.Client.RequestGetStaffShifts();
			var CompanyShifts = await Azure.Client.RequestGetCompanyShifts( Account );

			var S = new SelectedShifts();

			foreach( var StaffShift in StaffShifts )
				S.Add( new SelectedShift( StaffShift, CompanyShifts ) );

			Shifts = S;

			foreach( var Shift in S )
			{
				Shift.OnChange = _ =>
								 {
									 PricingShiftSaveEnable = true;
								 };

				Shift.Loaded = true;
			}
		}
	}

	[DependsUpon( nameof( EmployeeVisible ) )]
	public async Task WhenEmployeeChanges()
	{
		if( !InChange )
		{
			InChange = true;

			try
			{
				var Vis = EmployeeVisible;

				PricingOrStaffVisible = Vis || PricingVisible;

				Vis            = !Vis;
				PricingVisible = Vis;
				AccountVisible = Vis;

				BufferedShifts = null;
				await WhenSelectedAccountNameChanges();
			}
			finally
			{
				InChange = false;
			}
		}
	}
#endregion

	private bool InChange;

#region Account
	public bool AccountVisible
	{
		get { return Get( () => AccountVisible, true ); }
		set { Set( () => AccountVisible, value ); }
	}

	[DependsUpon( nameof( AccountVisible ) )]
	public void WhenAccountVisibilityChanges()
	{
		if( !InChange )
		{
			InChange = true;

			try
			{
				var Vis = !AccountVisible;
				PricingVisible  = Vis;
				EmployeeVisible = Vis;
			}
			finally
			{
				InChange = false;
			}
		}
	}
#endregion
#endregion

#region Data
	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public string GlobalAddressBookId { get; set; } = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		Task.Run( () =>
				  {
					  Task.WaitAll( GetAccountNamesAsync(),
								    GetAdminUsers(),
								    GetDrivers(),
								    GetRoles(),
								    GetPackageTypes(),
								    GetServiceLevels(),
								    GetRawCharges()
								  );
					  Loaded = true;
				  } );
		Loaded = true;
	}

	// public ObservableCollection<string> AccountNames = new ObservableCollection<string>();

	public Accounts? View { get; set; }

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/649691328/How+to+Manage+Customer+Accounts" ); }
	}

	public ObservableCollection<string> AccountNames
	{
		//get { return Get(() => AccountNames, GetAccountNames); }
		get { return Get( () => AccountNames, new ObservableCollection<string>() ); }
		set { Set( () => AccountNames, value ); }
	}

	public async Task GetPreferences()
	{
		if( !IsInDesignMode )
		{
			Logging.WriteLogLine( "Getting Preferences" );
			var Prefs = await Azure.Client.RequestPreferences();

			Logging.WriteLogLine( "Found " + Prefs?.Count + " preferences;" );

			if( Prefs?.Count > 0 )
			{
				var Pref = ( from P in Prefs
						     where P.Description.Contains( "Invoice Billing Type: Additional groups (Monthly 2 etc)" )
						     select P ).FirstOrDefault();
				InvoiceBillingTypeAdditionalGroups = Pref is {Enabled: true};
				Logging.WriteLogLine( $"{Pref?.Description} is {InvoiceBillingTypeAdditionalGroups}" );

				SelectedAccountBillingPeriods = InvoiceBillingTypeAdditionalGroups ? GetBillingPeriodsWithAdditional() : GetBillingPeriods();
			}
		}
	}

	public bool InvoiceBillingTypeAdditionalGroups
	{
		get => Get( () => InvoiceBillingTypeAdditionalGroups, false );
		set => Set( () => InvoiceBillingTypeAdditionalGroups, value );
	}

	public List<string> Zones
	{
		get { return Get( () => Zones, GetZones ); }
		set { Set( () => Zones, value ); }
	}

	public List<Zone> RawZones
	{
		get => Get( () => RawZones, new List<Zone>() );
		set => Set( () => RawZones, value );
	}

	public List<string> GetZones()
	{
		Logging.WriteLogLine( "Loading Zones" );
		var StringZones = new List<string>();

		if( !IsInDesignMode )
		{
			Task.Run( async () =>
					  {
						  var RequestZones = await Azure.Client.RequestZones();
						  Logging.WriteLogLine( "Found " + RequestZones.Count + " Zones" );

						  StringZones = ( from Z in RequestZones
									      orderby Z.Name.ToLower()
									      select Z.Name ).ToList();

						  RawZones.Clear();

						  foreach( var Zone in RequestZones )
							  RawZones.Add( Zone );
					  } ).Wait();
		}
		return StringZones;
	}

	public string SelectedAccountName
	{
		get { return Get( () => SelectedAccountName, "" ); }
		set { Set( () => SelectedAccountName, value ); }
	}

	public string OriginalSelectedAccountName
	{
		get { return Get( () => OriginalSelectedAccountName, "" ); }
		set { Set( () => OriginalSelectedAccountName, value ); }
	}

	public Company? SelectedAccountCompany
	{
		get { return Get( () => SelectedAccountCompany, new Company() ); }
		set { Set( () => SelectedAccountCompany, value ); }
	}

#region SelectedAccountId
	public string SelectedAccountId
	{
		get { return Get( () => SelectedAccountId, "" ); }
		set { Set( () => SelectedAccountId, value ); }
	}

	//[DependsUpon250( nameof( SelectedAccountId ) )]
	public void WhenSelectedAccountIdChanges()
	{
		//ClearAccountDetails();
	}
#endregion

	[DependsUpon( nameof( SelectedAccountCompany ) )]
	public bool IsAccountIdLocked
	{
		get => !( ( SelectedAccountCompany == null ) || SelectedAccountCompany.CompanyName.IsNullOrWhiteSpace() );
		set { Set( () => IsAccountIdLocked, value ); }
	}

	public string SelectedAccountPhone
	{
		get { return Get( () => SelectedAccountPhone, "" ); }
		set { Set( () => SelectedAccountPhone, value ); }
	}

	public string SelectedAccountSalesRep
	{
		get { return Get( () => SelectedAccountSalesRep, "" ); }
		set { Set( () => SelectedAccountSalesRep, value ); }
	}

	public string SelectedAccountNotes
	{
		get { return Get( () => SelectedAccountNotes, "" ); }
		set { Set( () => SelectedAccountNotes, value ); }
	}

	public string SelectedAccountBillingType
	{
		get { return Get( () => SelectedAccountBillingType, "" ); }
		set { Set( () => SelectedAccountBillingType, value ); }
	}

	public List<string> SelectedAccountBillingMethods
	{
		get { return Get( () => SelectedAccountBillingMethods, GetBillingMethods ); }

		//set { Set( () => SelectedAccountBillingMethod, value ); }
	}

	public string SelectedAccountBillingMethod
	{
		get => Get( () => SelectedAccountBillingMethod, string.Empty );
		set => Set( () => SelectedAccountBillingMethod, value );
	}

	public List<string> GetBillingMethods() => new()
											   {
												   FindStringResource( "AccountsBillingMethodPrint" ),
												   FindStringResource( "AccountsBillingMethodEmail" )
											   };

	public List<string> SelectedAccountBillingPeriods
	{
		get { return Get( () => SelectedAccountBillingPeriods, GetBillingPeriods ); }
		set => Set( () => SelectedAccountBillingPeriods, value );
	}

	public string SelectedAccountBillingPeriod
	{
		get => Get( () => SelectedAccountBillingPeriod, string.Empty );
		set => Set( () => SelectedAccountBillingPeriod, value );
	}

	public List<string> GetBillingPeriods() => new()
											   {
												   FindStringResource( "AccountsBillingPeriodDaily" ),
												   FindStringResource( "AccountsBillingPeriodWeekly" ),
												   FindStringResource( "AccountsBillingPeriodBiWeekly" ),
												   FindStringResource( "AccountsBillingPeriodMonthly" ),
												   FindStringResource( "AccountsBillingPeriodSingle" ),
												   FindStringResource( "AccountsBillingPeriodIndividual" )
											   };

	public List<string> GetBillingPeriodsWithAdditional() => new()
															 {
																 FindStringResource( "AccountsBillingPeriodDaily" ),
																 FindStringResource( "AccountsBillingPeriodWeekly" ),
																 FindStringResource( "AccountsBillingPeriodBiWeekly" ),
																 FindStringResource( "AccountsBillingPeriodMonthly" ),
																 FindStringResource( "AccountsBillingPeriodSingle" ),

																 FindStringResource( "AccountsBillingPeriodDaily2" ),
																 FindStringResource( "AccountsBillingPeriodDaily3" ),
																 FindStringResource( "AccountsBillingPeriodWeekly2" ),
																 FindStringResource( "AccountsBillingPeriodWeekly3" ),
																 FindStringResource( "AccountsBillingPeriodBiWeekly2" ),
																 FindStringResource( "AccountsBillingPeriodBiWeekly3" ),
																 FindStringResource( "AccountsBillingPeriodMonthly2" ),
																 FindStringResource( "AccountsBillingPeriodMonthly3" ),
																 FindStringResource( "AccountsBillingPeriodSingle2" ),
																 FindStringResource( "AccountsBillingPeriodSingle3" ),
																 FindStringResource( "AccountsBillingPeriodIndividual" )
															 };

	public string SelectedAccountInvoiceOverride
	{
		get { return Get( () => SelectedAccountInvoiceOverride, "" ); }
		set { Set( () => SelectedAccountInvoiceOverride, value ); }
	}

	public List<string> SelectedAccountInvoiceOverrides
	{
		get { return Get( () => SelectedAccountInvoiceOverrides, GetInvoiceOverrides ); }
		set { Set( () => SelectedAccountInvoiceOverrides, value ); }
	}

	public List<string> GetInvoiceOverrides() => new()
												 {
													 FindStringResource( "AccountsBillingInvoiceOverrideNone" )
												 };

	public string SelectedAccountInvoiceEmail
	{
		get { return Get( () => SelectedAccountInvoiceEmail, "" ); }
		set { Set( () => SelectedAccountInvoiceEmail, value ); }
	}

	public string SelectedAccountOutstandingFormatted
	{
		get { return Get( () => SelectedAccountOutstandingFormatted, GetOutstandingForSelectedAccountFormatted ); }
		set { Set( () => SelectedAccountOutstandingFormatted, value ); }
	}

	public bool SelectedAccountIsEnabled
	{
		get { return Get( () => SelectedAccountIsEnabled, true ); }
		set { Set( () => SelectedAccountIsEnabled, value ); }
	}

	public bool SelectedAccountRequireReferenceField
	{
		get { return Get( () => SelectedAccountRequireReferenceField, false ); }
		set { Set( () => SelectedAccountRequireReferenceField, value ); }
	}

	public string SelectedAccountCreditLimit
	{
		get { return Get( () => SelectedAccountCreditLimit, "" ); }
		set { Set( () => SelectedAccountCreditLimit, value ); }
	}

	public string SelectedAccountAdminName
	{
		get { return Get( () => SelectedAccountAdminName, "" ); }
		set { Set( () => SelectedAccountAdminName, value ); }
	}

	public string SelectedAccountAdminPhone
	{
		get { return Get( () => SelectedAccountAdminPhone, "" ); }
		set { Set( () => SelectedAccountAdminPhone, value ); }
	}

	public string SelectedAccountBillingCompanyName
	{
		get { return Get( () => SelectedAccountBillingCompanyName, "" ); }
		set { Set( () => SelectedAccountBillingCompanyName, value ); }
	}

	public string SelectedAccountBillingAdminName
	{
		get { return Get( () => SelectedAccountBillingAdminName, "" ); }
		set { Set( () => SelectedAccountBillingAdminName, value ); }
	}

	public string SelectedAccountBillingAdminPhone
	{
		get { return Get( () => SelectedAccountBillingAdminPhone, "" ); }
		set { Set( () => SelectedAccountBillingAdminPhone, value ); }
	}

	public string SelectedAccountBillingSuite
	{
		get { return Get( () => SelectedAccountBillingSuite, "" ); }
		set { Set( () => SelectedAccountBillingSuite, value ); }
	}

	public string SelectedAccountBillingStreet
	{
		get { return Get( () => SelectedAccountBillingStreet, "" ); }
		set { Set( () => SelectedAccountBillingStreet, value ); }
	}

	public string SelectedAccountBillingCity
	{
		get { return Get( () => SelectedAccountBillingCity, "" ); }
		set { Set( () => SelectedAccountBillingCity, value ); }
	}

	public string SelectedAccountBillingProvState
	{
		get { return Get( () => SelectedAccountBillingProvState, "" ); }
		set { Set( () => SelectedAccountBillingProvState, value ); }
	}

	public string SelectedAccountBillingCountry
	{
		get { return Get( () => SelectedAccountBillingCountry, "" ); }
		set { Set( () => SelectedAccountBillingCountry, value ); }
	}

	public string SelectedAccountBillingPostalZip
	{
		get { return Get( () => SelectedAccountBillingPostalZip, "" ); }
		set { Set( () => SelectedAccountBillingPostalZip, value ); }
	}

	public string SelectedAccountBillingZone
	{
		get { return Get( () => SelectedAccountBillingZone, "" ); }
		set { Set( () => SelectedAccountBillingZone, value ); }
	}

	public string SelectedAccountBillingNotes
	{
		get { return Get( () => SelectedAccountBillingNotes, "" ); }
		set { Set( () => SelectedAccountBillingNotes, value ); }
	}

#region Shipping Account
	public string ShippingAccountCompanyName
	{
		get { return Get( () => ShippingAccountCompanyName, "" ); }
		set { Set( () => ShippingAccountCompanyName, value ); }
	}

	public string ShippingAccountUserName
	{
		get { return Get( () => ShippingAccountUserName, "" ); }
		set { Set( () => ShippingAccountUserName, value ); }
	}

	public string SelectedAccountShippingAdminPhone
	{
		get { return Get( () => SelectedAccountShippingAdminPhone, "" ); }
		set { Set( () => SelectedAccountShippingAdminPhone, value ); }
	}

	public string SelectedAccountShippingSuite
	{
		get { return Get( () => SelectedAccountShippingSuite, "" ); }
		set { Set( () => SelectedAccountShippingSuite, value ); }
	}

	public string SelectedAccountShippingStreet
	{
		get { return Get( () => SelectedAccountShippingStreet, "" ); }
		set { Set( () => SelectedAccountShippingStreet, value ); }
	}

	public string SelectedAccountShippingCity
	{
		get { return Get( () => SelectedAccountShippingCity, "" ); }
		set { Set( () => SelectedAccountShippingCity, value ); }
	}

	public string SelectedAccountShippingProvState
	{
		get { return Get( () => SelectedAccountShippingProvState, "" ); }
		set { Set( () => SelectedAccountShippingProvState, value ); }
	}

	public string SelectedAccountShippingCountry
	{
		get { return Get( () => SelectedAccountShippingCountry, "" ); }
		set { Set( () => SelectedAccountShippingCountry, value ); }
	}

	public string SelectedAccountShippingPostalZip
	{
		get { return Get( () => SelectedAccountShippingPostalZip, "" ); }
		set { Set( () => SelectedAccountShippingPostalZip, value ); }
	}

	public string SelectedAccountShippingZone
	{
		get { return Get( () => SelectedAccountShippingZone, "" ); }
		set { Set( () => SelectedAccountShippingZone, value ); }
	}

	public string SelectedAccountShippingAddressNotes
	{
		get { return Get( () => SelectedAccountShippingAddressNotes, "" ); }
		set { Set( () => SelectedAccountShippingAddressNotes, value ); }
	}
#endregion

	public string SelectedAccountAdminUserId
	{
		get { return Get( () => SelectedAccountAdminUserId, "" ); }
		set { Set( () => SelectedAccountAdminUserId, value ); }
	}

	public string SelectedAccountAdminUserName
	{
		get { return Get( () => SelectedAccountAdminUserName, "" ); }
		set { Set( () => SelectedAccountAdminUserName, value ); }
	}

	public string SelectedAccountAdminUserPassword
	{
		get { return Get( () => SelectedAccountAdminUserPassword, "" ); }
		set { Set( () => SelectedAccountAdminUserPassword, value ); }
	}

	public PasswordBox Password        = null!;
	public PasswordBox ConfirmPassword = null!;

	public bool SelectedAccountAdminUserEnabled
	{
		get { return Get( () => SelectedAccountAdminUserEnabled, true ); }
		set { Set( () => SelectedAccountAdminUserEnabled, value ); }
	}

	public string SelectedAccountAdminUserFirstName
	{
		get { return Get( () => SelectedAccountAdminUserFirstName, "" ); }
		set { Set( () => SelectedAccountAdminUserFirstName, value ); }
	}

	public string SelectedAccountAdminUserLastName
	{
		get { return Get( () => SelectedAccountAdminUserLastName, "" ); }
		set { Set( () => SelectedAccountAdminUserLastName, value ); }
	}

	public string SelectedAccountAdminUserEmail
	{
		get { return Get( () => SelectedAccountAdminUserEmail, "" ); }
		set { Set( () => SelectedAccountAdminUserEmail, value ); }
	}

	public string SelectedAccountAdminUserPhone
	{
		get { return Get( () => SelectedAccountAdminUserPhone, "" ); }
		set { Set( () => SelectedAccountAdminUserPhone, value ); }
	}

	public string SelectedAccountAdminUserCell
	{
		get { return Get( () => SelectedAccountAdminUserCell, "" ); }
		set { Set( () => SelectedAccountAdminUserCell, value ); }
	}

	public IList Countries
	{
		get => CountriesRegions.Countries;
		set { Set( () => Countries, value ); }
	}

	public string SelectedBillingCountry
	{
		get { return Get( () => SelectedBillingCountry, "" ); }
		set { Set( () => SelectedBillingCountry, value ); }
	}

	public string SelectedShippingCountry
	{
		get { return Get( () => SelectedShippingCountry, "" ); }
		set { Set( () => SelectedShippingCountry, value ); }
	}

	[DependsUpon( nameof( SelectedBillingCountry ) )]
	public IList BillingRegions
	{
		get { return Get( () => BillingRegions, new List<object>() ); }
		set { Set( () => BillingRegions, value ); }
	}

	[DependsUpon( nameof( SelectedShippingCountry ) )]
	public IList ShippingRegions
	{
		get { return Get( () => ShippingRegions, new List<object>() ); }
		set { Set( () => ShippingRegions, value ); }
	}

	public string AdhocFilter
	{
		get { return Get( () => AdhocFilter, "" ); }
		set { Set( () => AdhocFilter, value ); }
	}

	//
	// Private methods for Data
	//

	private static string GetOutstandingForSelectedAccountFormatted()
	{
		const decimal RAW         = 0.0M; // TODO 
		var           Outstanding = $"{RAW:c}";
		return Outstanding;
	}

	public async Task<ObservableCollection<string>> GetAccountNamesAsync()
	{
		try
		{
			if( !IsInDesignMode )
			{
				var Companies = ( from Company in await Azure.Client.RequestGetCustomerCodeCompanyNameList()
								  let CompanyName = Company.CompanyName.Trim().ToLower()
								  orderby CompanyName
								  group Company by CompanyName
								  into G
								  select G.First() ).ToList();
				DictCustomerNamesIds = Companies.ToDictionary( a => a.CompanyName, a => a );

				// Don't include the Global Address Book in the list
				var Names = new ObservableCollection<string>( from Company in Companies
															  where Company.CompanyName != GlobalAddressBookId
															  select Company.CompanyName );
				AccountNames = Names;
				Logging.WriteLogLine( "Loaded " + Names.Count + " companies" );
				SelectedAccountBillingCountry = SelectedAccountShippingCountry = Names.Count > 0 ? Names[ 0 ] : "";

				//TestGetAllPackageOverridesAndRedirects();
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( $"Exception thrown fetching customers:\n{E}" );
		}
		return new ObservableCollection<string>();
	}

	public async Task GetRoles()
	{
		var Roles = await Azure.Client.RequestRoles();

		AdminRole = ( from R in Roles
					  where R.IsAdministrator
					  select R ).FirstOrDefault();

		DriveRole = ( from R in Roles
					  where R.IsDriver
					  select R ).FirstOrDefault();

		WebAdminRole = ( from R in Roles
					     where R.Name == "Web Admin"
					     select R ).FirstOrDefault();

		if( WebAdminRole == null )
		{
			Logging.WriteLogLine( "Web Admin role is null - Creating and saving" );

			WebAdminRole = new Role
						   {
							   Name            = "Web Admin",
							   IsAdministrator = false,
							   IsDriver        = false,
							   IsWebService    = false,
							   Mandatory       = false
						   };

			await Azure.Client.RequestAddUpdateRole( WebAdminRole );

			Roles = await Azure.Client.RequestRoles();

			WebAdminRole = ( from R in Roles
						     where R.Name == "Web Admin"
						     select R ).FirstOrDefault();

			if( WebAdminRole != null )
				Logging.WriteLogLine( "Added Web Admin Role: " + WebAdminRole.Name );
			else
				Logging.WriteLogLine( "ERROR Failed adding Web Admin Role" );
		}
	}

	public async Task GetAdminUsers()
	{
		try
		{
			if( !IsInDesignMode )
			{
				Logging.WriteLogLine( "DEBUG Starting to get Admin Users" );
				List<Protocol.Data.Staff> StaffList = new();

				if( WebAdminRole != null )
				{
					_ = Task.Run( async () =>
								  {
									  await GetRoles();
								  } );
				}

				foreach( var A in await Azure.Client.RequestGetStaffAndRoles() )
				{
					//if (WebAdminRole != null && A.Roles.Contains(WebAdminRole))
					var S = await Azure.Client.RequestGetStaffMember( A.StaffId );

					if( S is not null )
					{
						S.Password = Encryption.FromTimeLimitedToken( S.Password ).Value;
						StaffList.Add( S );
					}
					else
						Logging.WriteLogLine( "Can't find StaffMember: " + A.StaffId );
				}

				//DictAdmins = ( from S in StaffList
				//               select S ).ToDictionary( s => s.StaffId, s => s );
				DictAdmins = new Dictionary<string, Protocol.Data.Staff>();

				foreach( var S in StaffList )
				{
					if( !DictAdmins.ContainsKey( S.StaffId ) )
						DictAdmins[ S.StaffId ] = S;
					else
						Logging.WriteLogLine( "DictAdmins already contains the StaffId " + S.StaffId );
				}

				Logging.WriteLogLine( "DEBUG Done - Loaded " + StaffList.Count() + " Admin Users" );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( $"Exception thrown fetching Administrators:\n{E}" );
		}
	}

	public async Task GetAdminUsers_V1()
	{
		try
		{
			if( !IsInDesignMode )
			{
				List<Protocol.Data.Staff> StaffList = new();

				foreach( var A in await Azure.Client.RequestGetAdministrators() )
				{
					var S = await Azure.Client.RequestGetStaffMember( A.StaffId );

					if( S is not null )
					{
						S.Password = Encryption.FromTimeLimitedToken( S.Password ).Value;
						StaffList.Add( S );
					}
				}

				DictAdmins = ( from S in StaffList
							   select S ).ToDictionary( s => s.StaffId, s => s );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( $"Exception thrown fetching Administrators:\n{E}" );
		}
	}

	public async Task GetDrivers()
	{
		if( !IsInDesignMode )
		{
			try
			{
				if( !IsInDesignMode )
				{
					Drivers = ( from D in await Azure.Client.RequestGetDrivers()
							    orderby D.StaffId
							    select D.StaffId ).ToList();
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( $"Exception while getting staff list: {E}" );
			}
		}
	}

	public ObservableCollection<string> PackageTypeNames
	{
		get => Get( () => PackageTypeNames, new ObservableCollection<string>() );
		set => Set( () => PackageTypeNames, value );
	}

	public List<PackageType> PackageTypes
	{
		get => Get( () => PackageTypes, new List<PackageType>() );
		set => Set( () => PackageTypes, value );
	}

	public async Task GetPackageTypes()
	{
		if( !IsInDesignMode )
		{
			//pts = await Azure.Client.RequestGetPackageTypes();
			var Pts = await Azure.Client.RequestGetPackageTypes();

			Dispatcher.Invoke( () =>
							   {
								   PackageTypeNames.Clear();
								   PackageTypes.Clear();

								   foreach( var Pt in Pts )
								   {
									   PackageTypeNames.Add( Pt.Description );
									   PackageTypes.Add( Pt );
								   }
							   } );
		}
	}

	public async Task GetPackageRedirectsAndOverrides()
	{
		if( !IsInDesignMode && SelectedAccountName.IsNotNullOrWhiteSpace() && ( SelectedCompanyInternalId > -1 ) )
		{
			var Prs = await Azure.Client.RequestGetPackageRedirectsByCompanyId( SelectedCompanyInternalId );
			var Pos = await Azure.Client.RequestGetPackageOverridesByCompanyId( SelectedCompanyInternalId );

			Logging.WriteLogLine( "Found " + Pos?.Count + " overrides and " + Prs?.Count + " redirects" );

			PackageOverrides        = new ObservableCollection<OverridePackage>();
			DisplayPackageOverrides = new ObservableCollection<string>();
			//RaisePropertyChanged(nameof(DisplayPackageOverrides));
			AccountDiscounts        = new List<RedirectPackage>();
			DisplayAccountDiscounts = new ObservableCollection<DisplayAccountDiscount>();
			//RaisePropertyChanged(nameof(DisplayAccountDiscounts));

			/*			Dispatcher.Invoke(() =>
                        {
                            if (View != null)
                            {
                                //View.LvOverrides.Visibility = Visibility.Hidden;
                                //View.LvDiscounts.Visibility = Visibility.Hidden;

                                PackageOverrides = new();
                                DisplayPackageOverrides = new();
                                //RaisePropertyChanged(nameof(DisplayPackageOverrides));
                                AccountDiscounts = new();
                                DisplayAccountDiscounts = new();
                                //RaisePropertyChanged(nameof(DisplayAccountDiscounts));

                                //View.LvOverrides.Visibility = Visibility.Visible;
                                //View.LvDiscounts.Visibility = Visibility.Visible;
                                //View.LvOverrides.InvalidateVisual();
                                //View.LvDiscounts.InvalidateVisual();
                            }

                        });
            */

			SelectedPackageOverride = null;
			SelectedPackageRedirect = null;

			if( Pos?.Count > 0 )
			{
				foreach( var Po in Pos )
				{
					PackageOverrides.Add( Po );

					var BasePt = ( from P in PackageTypes
								   where P.PackageTypeId == Po.FromPackageTypeId
								   select P ).FirstOrDefault();

					var NewPt = ( from P in PackageTypes
								  where P.PackageTypeId == Po.ToPackageTypeId
								  select P ).FirstOrDefault();

					if( BasePt is not null && NewPt is not null )
						DisplayPackageOverrides.Add( FormatPackageOverride( BasePt.Description, NewPt.Description ) );
				}
			}

			if( Prs?.Count > 0 )
			{
				foreach( var Pr in Prs )
				{
					var Pt = ( from P in PackageTypes
							   where P.PackageTypeId == Pr.PackageTypeId
							   select P ).FirstOrDefault();

					var Sl = ( from S in ServiceLevels
							   where S.ServiceLevelId == Pr.ServiceLevelId
							   select S ).FirstOrDefault();
					var DiscountType = Pr.OverrideId;

					var Fz = ( from Z in RawZones
							   where Z.ZoneId == Pr.FromZoneId
							   select Z ).FirstOrDefault();

					var Tz = ( from Z in RawZones
							   where Z.ZoneId == Pr.ToZoneId
							   select Z ).FirstOrDefault();
					var Charge = Pr.Charge;

					if( Pt is not null && Sl is not null )
					{
						DisplayAccountDiscount Item = new()
													  {
														  DiscountType = DiscountTypes[ DiscountType ],
														  //FromZone = fz.Name,
														  //ToZone = tz.Name,
														  Package     = Pt.Description,
														  Service     = Sl.OldName,
														  StringValue = Charge + ""
													  };

						if( Fz != null )
							Item.FromZone = Fz.Name;
						else
						{
							Item.FromZone = Pr.FromZoneId switch
											{
												0  => "",
												-1 => FindStringResource( "AccountsPricingDiscountsAll" ),
												_  => ""
											};
						}

						if( Tz != null )
							Item.ToZone = Tz.Name;
						else
						{
							Item.ToZone = Pr.ToZoneId switch
										  {
											  0  => "",
											  -1 => FindStringResource( "AccountsPricingDiscountsAll" ),
											  _  => ""
										  };
						}

						AccountDiscounts.Add( Pr );
						DisplayAccountDiscounts.Add( Item );
					}
				}
			}
		}
	}

	//private void TestGetAllPackageOverridesAndRedirects()
	//{
	//	if (DictCustomerNamesIds?.Count > 0)
	//	{
	//		Task.Run(async () =>
	//		{
	//			List<string> found = new();
	//			foreach (string key in DictCustomerNamesIds.Keys) 
	//			{
	//				// SelectedAccountId = DictCustomerNamesIds[ SelectedAccount ].CustomerCode;
	//				var prs = await Azure.Client.RequestGetPackageRedirectsByCompanyCode(key);
	//				var pos = await Azure.Client.RequestGetPackageOverridesByCompanyCode(key);

	//				Logging.WriteLogLine(key + " Found " + pos?.Count + " overrides and " + prs?.Count + " redirects");
	//				if (prs?.Count > 0)
	//				{
	//					found.Add(key + " prs.Count: " + prs.Count);
	//				}
	//                   if (pos?.Count > 0)
	//                   {
	//                       found.Add(key + " pos.Count: " + pos.Count);
	//                   }

	//                   prs = await Azure.Client.RequestGetPackageRedirectsByCompanyCode(DictCustomerNamesIds[key].CustomerCode);
	//                   pos = await Azure.Client.RequestGetPackageOverridesByCompanyCode(DictCustomerNamesIds[key].CustomerCode);

	//                   Logging.WriteLogLine(DictCustomerNamesIds[key].CustomerCode + " Found " + pos?.Count + " overrides and " + prs?.Count + " redirects");
	//                   if (prs?.Count > 0)
	//                   {
	//                       found.Add(key + " prs.Count: " + prs.Count);
	//                   }
	//                   if (pos?.Count > 0)
	//                   {
	//                       found.Add(key + " pos.Count: " + pos.Count);
	//                   }
	//               }

	//			if (found.Count > 0)
	//			{
	//				foreach (var item in found)
	//				{
	//					Logging.WriteLogLine("DEBUG " + item);
	//				}
	//			}
	//		});
	//	}
	//}

	public ObservableCollection<string> ServiceLevelNames
	{
		get => Get( () => ServiceLevelNames, new ObservableCollection<string>() );
		set => Set( () => ServiceLevelNames, value );
	}

	public List<ServiceLevel> ServiceLevels
	{
		get => Get( () => ServiceLevels, new List<ServiceLevel>() );
		set => Set( () => ServiceLevels, value );
	}

	public async Task GetServiceLevels()
	{
		if( !IsInDesignMode )
		{
			var Sls = await Azure.Client.RequestGetServiceLevelsDetailed();

			Dispatcher.Invoke( () =>
							   {
								   ServiceLevelNames.Clear();
								   ServiceLevels.Clear();

								   var Names = ( from S in Sls
											     orderby S.SortOrder
											     select S.OldName ).ToList();

								   foreach( var Name in Names )
									   ServiceLevelNames.Add( Name );

								   foreach( var Sl in Sls )
									   ServiceLevels.Add( Sl );
							   } );
		}
	}

	public long SelectedCompanyInternalId
	{
		get => Get( () => SelectedCompanyInternalId, -1 );
		set => Set( () => SelectedCompanyInternalId, value );
	}
#endregion

#region Actions
	[DependsUpon( nameof( SelectedBillingCountry ) )]
	public void WhenSelectedBillingCountryChanges()
	{
		BillingRegions = SelectedBillingCountry switch
						 {
							 "Australia"     => CountriesRegions.AustraliaRegions,
							 "Canada"        => CountriesRegions.CanadaRegions,
							 "United States" => CountriesRegions.USRegions,
							 _               => new List<string>()
						 };
	}

	[DependsUpon( nameof( SelectedShippingCountry ) )]
	public void WhenSelectedShippingCountryChanges()
	{
		ShippingRegions = SelectedShippingCountry switch
						  {
							  "Australia"     => CountriesRegions.AustraliaRegions,
							  "Canada"        => CountriesRegions.CanadaRegions,
							  "United States" => CountriesRegions.USRegions,
							  _               => new List<string>()
						  };
	}

#region WhenSelectedAccountChanges
	private bool InWhenSelectedAccountChanges { get; set; }

	[DependsUpon( nameof( SelectedAccountName ) )]
	public async void WhenSelectedAccountChanges()
	{
		if( SelectedAccountName.IsNotNullOrWhiteSpace() && !InWhenSelectedAccountChanges )
		{
			InWhenSelectedAccountChanges = true;
			IsAccountIdLocked            = true;

			try
			{
				var SelectedAccount = SelectedAccountName;

				//Logging.WriteLogLine( "Selected Account Name has changed: " + SelectedAccount );
				//View?.BtnClear_Click(null, null);
				//ClearAccountDetails();
				//ClearUserDetails();
				//ClearErrorConditions();

				if( !string.IsNullOrEmpty( SelectedAccount ) )
				{
					Loaded = false;

					if( DictCustomerNamesIds.ContainsKey( SelectedAccount ) && !IsInDesignMode )
					{
						SelectedAccountId = DictCustomerNamesIds[ SelectedAccount ].CustomerCode;
						UpdateDefaultChargesForAccount();
						OriginalSelectedAccountName = SelectedAccountName = SelectedAccount;

						try
						{
							var Company         = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccountId );
							var ResellerCompany = await Azure.Client.RequestGetResellerCustomerCompany( Company.CustomerCode );
							//CustomerLookupSummary?  cls = await Azure.Client.RequestCustomerSummaryLookup(SelectedAccountId);

							// ReSharper disable once ConditionIsAlwaysTrueOrFalse
							if( Company is not null && ResellerCompany is not null )
							{
								await Dispatcher.Invoke( async () =>
													     {
														     SelectedCompanyInternalId    = Company.Company.CompanyId;
														     SelectedAccountBillingPeriod = SelectedAccountBillingPeriods[ Company.Company.BillingPeriod ];
														     SelectedAccountBillingMethod = Company.Company.EmailInvoice ? SelectedAccountBillingMethods[ 1 ] : SelectedAccountBillingMethods[ 0 ];
														     SelectedAccountInvoiceEmail  = Company.Company.EmailAddress1;
														     SelectedAccountCreditLimit   = Company.Company.CreditLimit.ToString( "F2" );

														     await GetPackageRedirectsAndOverrides();

													     #region Primary Company
														     SelectedAccountIsEnabled = ResellerCompany.Enabled;
														     SelectedAccountNotes     = ResellerCompany.Notes;

														     SelectedAccountAdminUserId       = Company.LoginCode;
														     SelectedAccountAdminName         = Company.UserName;
														     SelectedAccountAdminUserPassword = Company.Password;

														     var Co = Company.Company;

														     SelectedAccountAdminUserName     = Co.UserName;
														     SelectedAccountSalesRep          = Co.ContactName;
														     SelectedAccountShippingProvState = Co.Region;
														     SelectedAccountPhone             = Co.Phone;

														     Password.Password        = SelectedAccountAdminUserPassword;
														     ConfirmPassword.Password = SelectedAccountAdminUserPassword;

														     if( DictAdmins.TryGetValue( SelectedAccountId, out var Admin ) )
														     {
															     SelectedAccountAdminUserEnabled   = Admin.Enabled;
															     SelectedAccountAdminUserPassword  = Admin.Password;
															     SelectedAccountAdminUserFirstName = Admin.FirstName;
															     SelectedAccountAdminUserLastName  = Admin.LastName;
															     SelectedAccountAdminUserEmail     = Admin.Address.EmailAddress;
															     SelectedAccountAdminUserPhone     = Admin.Address.Phone;
															     SelectedAccountAdminUserCell      = Admin.Address.Mobile;
														     }
														     else
															     Logging.WriteLogLine( $"Can't find admin user: {SelectedAccountId}" );

														     SelectedAccountCompany = Company.BillingCompany;

														     Logging.WriteLogLine( "SelectedAccountAdminUserName: " + SelectedAccountAdminUserName );
														     Logging.WriteLogLine( "SelectedAccountAdminUserPassword: " + SelectedAccountAdminUserPassword );

														     //if (cls != null)
														     //                                    {
														     // SelectedAccountRequireReferenceField = cls.BoolOption1;
														     //}
														     if( ( ResellerCompany.EmailAddress2 == "RequireReferenceField" ) || ( Company.Company.EmailAddress2 == "RequireReferenceField" )
																													          || ( Company.BillingCompany.EmailAddress2 == "RequireReferenceField" ) || ( Company.ShippingCompany.EmailAddress2 == "RequireReferenceField" ) )
															     SelectedAccountRequireReferenceField = true;
														     else
															     SelectedAccountRequireReferenceField = false;
													     #endregion

													     #region Billing Company
														     var Bc = Company.BillingCompany;

														     SelectedAccountBillingCompanyName = Bc.CompanyName;
														     SelectedAccountBillingAdminName   = Bc.UserName;
														     SelectedAccountBillingAdminPhone  = Bc.Phone;

														     SelectedAccountBillingSuite     = Bc.Suite;
														     SelectedAccountBillingStreet    = Bc.AddressLine1 + " " + Bc.AddressLine2;
														     SelectedAccountBillingCity      = Bc.City;
														     SelectedAccountBillingPostalZip = Bc.PostalCode;

														     if( Bc.Region.IsNotNullOrWhiteSpace() )
															     SelectedAccountBillingProvState = Bc.Region;
														     else
														     {
															     SelectedAccountBillingProvState = string.Empty;

															     if( View != null )
																     View.cbAccountBillingProvState.SelectedIndex = -1;
														     }

														     if( Bc.Country.IsNotNullOrWhiteSpace() )
															     SelectedAccountBillingCountry = Bc.Country;
														     else
														     {
															     SelectedAccountBillingCountry = string.Empty;

															     if( View != null )
																     View.cbAccountBillingCountry.SelectedIndex = -1;
														     }
														     SelectedAccountBillingZone = Bc.ZoneName;

														     SelectedAccountBillingNotes = Bc.Notes;
													     #endregion

													     #region Shipping Address
														     var Sc = Company.ShippingCompany;

														     ShippingAccountCompanyName        = Sc.CompanyName;
														     ShippingAccountUserName           = Sc.UserName;
														     SelectedAccountShippingAdminPhone = Sc.Phone;

														     //SelectedAccountShippingCountry      = Sc.Country;
														     if( Sc.Country.IsNotNullOrWhiteSpace() )
															     SelectedAccountShippingCountry = Sc.Country;
														     else
														     {
															     SelectedAccountShippingCountry = string.Empty;

															     if( View != null )
																     View.cbAccountShippingCountry.SelectedIndex = -1;
														     }

														     // SelectedAccountShippingProvState    = Sc.Region;
														     if( Sc.Region.IsNotNullOrWhiteSpace() )
															     SelectedAccountShippingProvState = Sc.Region;
														     else
														     {
															     SelectedAccountShippingProvState = string.Empty;

															     if( View != null )
																     View.cbAccountShippingProvState.SelectedIndex = -1;
														     }
														     SelectedAccountShippingCity         = Sc.City;
														     SelectedAccountShippingPostalZip    = Sc.PostalCode;
														     SelectedAccountShippingStreet       = Sc.AddressLine1 + " " + Sc.AddressLine2;
														     SelectedAccountShippingSuite        = Sc.Suite;
														     SelectedAccountShippingAddressNotes = Sc.Notes;
														     SelectedAccountShippingZone         = Sc.ZoneName;
													     #endregion
													     } );
							}
						}
						catch( Exception E )
						{
							Logging.WriteLogLine( "Exception thrown fetching Account: " + E );
						}
					}
				}
				else
				{
					View?.BtnClear_Click( null, null );
					ClearAccountDetails();
					ClearUserDetails();
					ClearErrorConditions();
				}
			}
			finally
			{
				InWhenSelectedAccountChanges = false;
				Loaded                       = true;
			}
		}
	}

	private void UpdateDefaultChargesForAccount()
	{
		//string id = DictCustomerNamesIds[SelectedAccountName].CustomerCode;
		//DefaultCharges = DictAccountIdDefaultCharges[id];
		if( DictAccountIdDefaultCharges.ContainsKey( SelectedAccountId ) )
			DefaultCharges = DictAccountIdDefaultCharges[ SelectedAccountId ];

		foreach( var Dc in RawCharges )
		{
			//DisplayCharge found = (from D in DefaultCharges where D.AccountId == SelectedAccountId && D.ChargeId == dc.ChargeId select D).FirstOrDefault();
			var Found = ( from D in DefaultCharges
						  where ( D.AccountId == SelectedAccountId ) && ( D.ChargeId == PriceTripHelper.MakeDefaultChargeId( SelectedAccountId, Dc.ChargeId ) )
						  select D ).FirstOrDefault();

			if( Found != null )
			{
				switch( Found.SortIndex )
				{
				case -3:
					{
						Dc.IsOverrideApplied = true;
						Dc.IsChargeApplied   = true;
						break;
					}

				case -2:
					{
						Dc.IsOverrideApplied = true;
						Dc.IsChargeApplied   = false;
						break;
					}

				case -1:
					{
						Dc.IsOverrideApplied = false;
						Dc.IsChargeApplied   = true;
						break;
					}

				default:
					{
						Dc.IsOverrideApplied = false;
						Dc.IsChargeApplied   = false;
						break;
					}
				}
			}
			else
			{
				Dc.IsOverrideApplied = false;
				Dc.IsChargeApplied   = false;
			}
		}

		//RaisePropertyChanged(nameof(RawCharges));
		if( View != null )
		{
			Dispatcher.Invoke( () =>
							   {
								   View.UpdateGrid();
							   } );
		}
	}

	public async void WhenSelectedAccountChanges_V1()
	{
		if( SelectedAccountName.IsNotNullOrWhiteSpace() && !InWhenSelectedAccountChanges )
		{
			InWhenSelectedAccountChanges = true;

			try
			{
				var SelectedAccount = SelectedAccountName;

				Logging.WriteLogLine( "Selected Account Name has changed: " + SelectedAccount );
				View?.BtnClear_Click( null, null );
				ClearAccountDetails();
				ClearUserDetails();
				ClearErrorConditions();

				if( !string.IsNullOrEmpty( SelectedAccount ) )
				{
					Loaded = false;

					if( DictCustomerNamesIds.ContainsKey( SelectedAccount ) && !IsInDesignMode )
					{
						SelectedAccountId   = DictCustomerNamesIds[ SelectedAccount ].CustomerCode;
						SelectedAccountName = SelectedAccount;

						try
						{
							var Company         = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccountId );
							var ResellerCompany = await Azure.Client.RequestGetResellerCustomerCompany( Company.CustomerCode );
							//CustomerLookupSummary?  cls = await Azure.Client.RequestCustomerSummaryLookup(SelectedAccountId);

							if( ResellerCompany is not null )
							{
								Dispatcher.Invoke( () =>
												   {
												   #region Primary Company
													   SelectedAccountIsEnabled = ResellerCompany.Enabled;
													   SelectedAccountNotes     = ResellerCompany.Notes;

													   SelectedAccountAdminUserId       = Company.LoginCode;
													   SelectedAccountAdminName         = Company.UserName;
													   SelectedAccountAdminUserPassword = Company.Password;

													   var Co = Company.Company;

													   SelectedAccountAdminUserName     = Co.UserName;
													   SelectedAccountSalesRep          = Co.ContactName;
													   SelectedAccountShippingProvState = Co.Region;
													   SelectedAccountPhone             = Co.Phone;

													   Password.Password        = SelectedAccountAdminUserPassword;
													   ConfirmPassword.Password = SelectedAccountAdminUserPassword;

													   if( DictAdmins.TryGetValue( SelectedAccountId, out var Admin ) )
													   {
														   SelectedAccountAdminUserEnabled   = Admin.Enabled;
														   SelectedAccountAdminUserPassword  = Admin.Password;
														   SelectedAccountAdminUserFirstName = Admin.FirstName;
														   SelectedAccountAdminUserLastName  = Admin.LastName;
														   SelectedAccountAdminUserEmail     = Admin.Address.EmailAddress;
														   SelectedAccountAdminUserPhone     = Admin.Address.Phone;
														   SelectedAccountAdminUserCell      = Admin.Address.Mobile;
													   }
													   else
														   Logging.WriteLogLine( $"Can't find admin user: {SelectedAccountId}" );

													   SelectedAccountCompany = Company.BillingCompany;

													   Logging.WriteLogLine( "SelectedAccountAdminUserName: " + SelectedAccountAdminUserName );
													   Logging.WriteLogLine( "SelectedAccountAdminUserPassword: " + SelectedAccountAdminUserPassword );

													   //if (cls != null)
													   //                                    {
													   // SelectedAccountRequireReferenceField = cls.BoolOption1;
													   //}
													   if( ( ResellerCompany.EmailAddress2 == "RequireReferenceField" ) || ( Company.Company.EmailAddress2 == "RequireReferenceField" )
																												        || ( Company.BillingCompany.EmailAddress2 == "RequireReferenceField" ) || ( Company.ShippingCompany.EmailAddress2 == "RequireReferenceField" ) )
														   SelectedAccountRequireReferenceField = true;
													   else
														   SelectedAccountRequireReferenceField = false;
												   #endregion

												   #region Billing Company
													   var Bc = Company.BillingCompany;

													   SelectedAccountBillingCompanyName = Bc.CompanyName;
													   SelectedAccountBillingAdminName   = Bc.UserName;
													   SelectedAccountBillingAdminPhone  = Bc.Phone;

													   SelectedAccountBillingSuite     = Bc.Suite;
													   SelectedAccountBillingStreet    = Bc.AddressLine1 + " " + Bc.AddressLine2;
													   SelectedAccountBillingCity      = Bc.City;
													   SelectedAccountBillingPostalZip = Bc.PostalCode;
													   SelectedAccountBillingCountry   = Bc.Country;
													   SelectedAccountBillingProvState = Bc.Region;
													   SelectedAccountBillingZone      = Bc.ZoneName;

													   SelectedAccountBillingNotes = Bc.Notes;
												   #endregion

												   #region Shipping Address
													   var Sc = Company.ShippingCompany;

													   ShippingAccountCompanyName        = Sc.CompanyName;
													   ShippingAccountUserName           = Sc.UserName;
													   SelectedAccountShippingAdminPhone = Sc.Phone;

													   SelectedAccountShippingCountry      = Sc.Country;
													   SelectedAccountShippingProvState    = Sc.Region;
													   SelectedAccountShippingCity         = Sc.City;
													   SelectedAccountShippingPostalZip    = Sc.PostalCode;
													   SelectedAccountShippingStreet       = Sc.AddressLine1 + " " + Sc.AddressLine2;
													   SelectedAccountShippingSuite        = Sc.Suite;
													   SelectedAccountShippingAddressNotes = Sc.Notes;
													   SelectedAccountShippingZone         = Sc.ZoneName;
												   #endregion
												   } );
							}
						}
						catch( Exception E )
						{
							Logging.WriteLogLine( "Exception thrown fetching Account: " + E );
						}
					}
				}
			}
			finally
			{
				InWhenSelectedAccountChanges = false;
				Loaded                       = true;
			}
		}
	}
#endregion

	public void ClearAccountDetails()
	{
		Logging.WriteLogLine( "Clearing Account Details" );
		IsAccountIdLocked                 = false;
		SelectedCompanyInternalId         = -1;
		SelectedAccountId                 = string.Empty;
		SelectedAccountName               = string.Empty;
		OriginalSelectedAccountName       = string.Empty;
		SelectedAccountAdminName          = string.Empty;
		SelectedAccountAdminPhone         = string.Empty;
		SelectedAccountAdminUserCell      = string.Empty;
		SelectedAccountAdminUserEmail     = string.Empty;
		SelectedAccountAdminUserEnabled   = false; //true;
		SelectedAccountAdminUserFirstName = string.Empty;
		SelectedAccountAdminUserId        = string.Empty;
		SelectedAccountAdminUserLastName  = string.Empty;
		SelectedAccountAdminUserPassword  = string.Empty;
		SelectedAccountAdminUserPhone     = string.Empty;
		SelectedAccountBillingCity        = string.Empty;

		SelectedAccountBillingCountry = string.Empty;
		//SelectedAccountBillingMethod         = string.Empty;
		SelectedAccountBillingCompanyName = string.Empty;
		SelectedAccountBillingAdminName   = string.Empty;
		SelectedAccountBillingNotes       = string.Empty;
		SelectedAccountBillingType        = string.Empty;
		SelectedAccountBillingPostalZip   = string.Empty;
		SelectedAccountBillingProvState   = string.Empty;
		SelectedAccountBillingStreet      = string.Empty;
		SelectedAccountBillingSuite       = string.Empty;
		SelectedAccountBillingZone        = string.Empty;
		SelectedAccountBillingAdminPhone  = string.Empty;
		//		SelectedAccountId                    = string.Empty;
		SelectedAccountInvoiceEmail    = string.Empty;
		SelectedAccountInvoiceOverride = string.Empty;
		SelectedAccountIsEnabled       = true;
		//		SelectedAccountName                  = string.Empty;
		SelectedAccountNotes                 = string.Empty;
		SelectedAccountOutstandingFormatted  = string.Empty;
		SelectedAccountPhone                 = string.Empty;
		SelectedAccountRequireReferenceField = false;
		SelectedAccountSalesRep              = string.Empty;
		SelectedAccountShippingAddressNotes  = string.Empty;
		SelectedAccountShippingAdminPhone    = string.Empty;
		SelectedAccountShippingCity          = string.Empty;

		SelectedAccountShippingCountry   = string.Empty;
		SelectedAccountShippingPostalZip = string.Empty;
		SelectedAccountShippingProvState = string.Empty;
		SelectedAccountShippingStreet    = string.Empty;
		SelectedAccountShippingSuite     = string.Empty;
		SelectedAccountShippingZone      = string.Empty;
		SelectedAccountCompany           = null;
		ShippingAccountCompanyName       = string.Empty;
		ShippingAccountUserName          = string.Empty;

		if( View != null )
		{
			View.cbAccountBillingCountry.SelectedIndex  = -1;
			View.cbAccountShippingCountry.SelectedIndex = -1;
		}

		SelectedOverrideBasePackageType = string.Empty;
		SelectedOverrideNewPackageType  = string.Empty;
		PackageOverrides                = new ObservableCollection<OverridePackage>();
		DisplayPackageOverrides         = new ObservableCollection<string>();
		RaisePropertyChanged( nameof( DisplayPackageOverrides ) );
		SelectedPackageOverride = null;
		SelectedPackageRedirect = null;

		SelectedDiscountServiceLevelName = string.Empty;
		SelectedDiscountPackageType      = string.Empty;
		SelectedDiscountType             = string.Empty;
		SelectedFromZone                 = string.Empty;
		SelectedToZone                   = string.Empty;
		AccountDiscounts                 = new List<RedirectPackage>();
		DisplayAccountDiscounts          = new ObservableCollection<DisplayAccountDiscount>();
	}

	public void ClearUserDetails()
	{
		SelectedAccountAdminUserCell      = string.Empty;
		SelectedAccountAdminUserEmail     = string.Empty;
		SelectedAccountAdminUserEnabled   = true;
		SelectedAccountAdminUserFirstName = string.Empty;
		SelectedAccountAdminUserId        = string.Empty;
		SelectedAccountAdminUserLastName  = string.Empty;
		SelectedAccountAdminUserName      = string.Empty;
		SelectedAccountAdminUserPassword  = string.Empty;
		SelectedAccountAdminUserPhone     = string.Empty;
		Password.Password                 = string.Empty;
		ConfirmPassword.Password          = string.Empty;
	}

	public void ClearErrorConditions()
	{
		if( View != null )
		{
			foreach( var Name in View.DictControls.Keys )
				View.DictControls[ Name ].Background = Brushes.White;
		}
	}

	//public async Task<List<string>> SaveAccountDetails( Dictionary<string, Control> controls )
	public List<string> SaveAccountDetails( Dictionary<string, Control> controls )
	{
		var Errors = Validate( controls );

		if( Errors.Count == 0 )
		{
			Logging.WriteLogLine( "Saving Account: " + SelectedAccountName + " (" + SelectedAccountId + ")" );

			try
			{
				PrimaryCompany PrimaryCompany  = new();
				Company        BillingCompany  = new();
				Company        ShippingCompany = new();
				CompanyAddress ResellerCompany = new();
				var            IsNew           = false;

				if( SelectedAccountCompany is not null && SelectedAccountCompany.CompanyName.IsNotNullOrWhiteSpace() )
				{
					// Updating
					Task.WaitAll(
								 Task.Run( async () =>
										   {
											   //PrimaryCompany  = await Azure.Client.RequestGetPrimaryCompanyByCompanyName( SelectedAccountName );
											   if( SelectedAccountName == OriginalSelectedAccountName )
												   PrimaryCompany = await Azure.Client.RequestGetPrimaryCompanyByCompanyName( SelectedAccountName );
											   else
												   PrimaryCompany = await Azure.Client.RequestGetPrimaryCompanyByCompanyName( OriginalSelectedAccountName );
											   BillingCompany  = PrimaryCompany.BillingCompany;
											   ShippingCompany = PrimaryCompany.ShippingCompany;
											   ResellerCompany = await Azure.Client.RequestGetResellerCustomerCompany( PrimaryCompany.CustomerCode );
										   } )
								);
				}
				else
				{
					IsNew = true;

					// Only update if NOT editing - new Account
					PrimaryCompany = new PrimaryCompany
									 {
										 Company =
										 {
											 CompanyNumber = SelectedAccountId.Trim()
										 }
									 };

					ResellerCompany = new CompanyAddress
									  {
										  CompanyName   = SelectedAccountName.Trim(),
										  CompanyNumber = SelectedAccountId.Trim()
									  };
				}

			#region Primary Company
				var Company   = PrimaryCompany.Company;
				var AccountId = SelectedAccountId.Trim();
				var PWord     = Password.Password.Trim();

				Company.BillingPeriod = -1;

				for( short I = 0; I < SelectedAccountBillingPeriods.Count; I++ )
				{
					if( SelectedAccountBillingPeriods[ I ] == SelectedAccountBillingPeriod )
					{
						Company.BillingPeriod = I;
						break;
					}
				}
				Company.EmailInvoice  = SelectedAccountBillingMethod == FindStringResource( "AccountsBillingMethodEmail" );
				Company.EmailAddress1 = SelectedAccountInvoiceEmail;
				Company.CreditLimit   = decimal.TryParse( SelectedAccountCreditLimit, out var Convert ) ? Convert : 0;

				//company.CompanyName = SelectedAccountName.Trim();
				ResellerCompany.CompanyNumber = Company.CompanyNumber = AccountId;
				ResellerCompany.CompanyName   = Company.CompanyName   = SelectedAccountName.Trim();
				Company.UserName              = SelectedAccountAdminUserName.Trim();
				Company.Password              = PWord;
				Company.ContactName           = SelectedAccountSalesRep.Trim();

				Company.Phone        = SelectedAccountBillingAdminPhone.Trim();
				Company.Suite        = BillingCompany.Suite        = SelectedAccountBillingSuite.Trim();
				Company.AddressLine1 = BillingCompany.AddressLine1 = SelectedAccountBillingStreet.Trim();
				Company.City         = BillingCompany.City         = SelectedAccountBillingCity.Trim();
				Company.Region       = BillingCompany.Region       = SelectedAccountBillingProvState.Trim();
				Company.PostalCode   = BillingCompany.PostalCode   = SelectedAccountBillingPostalZip.Trim();
				Company.Country      = BillingCompany.Country      = SelectedAccountBillingCountry.Trim();
				Company.Notes        = SelectedAccountBillingNotes.Trim();
				Company.ZoneName     = SelectedAccountBillingZone.Trim();

				ResellerCompany.Phone = Company.Phone = SelectedAccountPhone.Trim();
				Company.Notes         = SelectedAccountNotes.Trim();

				Company.Enabled                = SelectedAccountIsEnabled;
				ResellerCompany.Notes          = SelectedAccountNotes.Trim();
				ResellerCompany.EmailAddress2  = Company.EmailAddress2 = SelectedAccountRequireReferenceField ? "RequireReferenceField" : "";
				PrimaryCompany.BillingCompany  = BillingCompany;
				PrimaryCompany.ShippingCompany = ShippingCompany;
				PrimaryCompany.CustomerCode    = SelectedAccountId.Trim();
				PrimaryCompany.LoginCode       = SelectedAccountId.Trim();
				PrimaryCompany.UserName        = SelectedAccountAdminUserName.Trim();
				PrimaryCompany.Password        = SelectedAccountAdminUserPassword.Trim();
				BillingCompany.EmailAddress2   = SelectedAccountRequireReferenceField ? "RequireReferenceField" : "";
			#endregion

			#region Billing Company
				BillingCompany.CompanyNumber = AccountId;
				BillingCompany.CompanyName   = SelectedAccountBillingCompanyName.Trim();
				BillingCompany.UserName      = SelectedAccountBillingAdminName.Trim();
				BillingCompany.Password      = PWord;

				BillingCompany.Phone         = SelectedAccountBillingAdminPhone.Trim();
				ResellerCompany.Suite        = BillingCompany.Suite        = SelectedAccountBillingSuite.Trim();
				ResellerCompany.AddressLine1 = BillingCompany.AddressLine1 = SelectedAccountBillingStreet.Trim();
				ResellerCompany.City         = BillingCompany.City         = SelectedAccountBillingCity.Trim();
				ResellerCompany.Region       = BillingCompany.Region       = SelectedAccountBillingProvState.Trim();
				ResellerCompany.PostalCode   = BillingCompany.PostalCode   = SelectedAccountBillingPostalZip.Trim();
				ResellerCompany.Country      = BillingCompany.Country      = SelectedAccountBillingCountry.Trim();
				BillingCompany.Notes         = SelectedAccountBillingNotes.Trim();
				BillingCompany.ZoneName      = SelectedAccountBillingZone.Trim();
				BillingCompany.EmailAddress2 = SelectedAccountRequireReferenceField ? "RequireReferenceField" : "";
			#endregion

			#region Shipping Address
				ShippingCompany.CompanyNumber = AccountId;
				ShippingCompany.CompanyName   = ShippingAccountCompanyName.Trim();
				ShippingCompany.UserName      = ShippingAccountUserName.Trim();
				ShippingCompany.Password      = PWord;

				ShippingCompany.Phone         = SelectedAccountShippingAdminPhone.Trim();
				ShippingCompany.Suite         = SelectedAccountShippingSuite.Trim();
				ShippingCompany.AddressLine1  = SelectedAccountShippingStreet.Trim();
				ShippingCompany.City          = SelectedAccountShippingCity.Trim();
				ShippingCompany.Region        = SelectedAccountShippingProvState.Trim();
				ShippingCompany.PostalCode    = SelectedAccountShippingPostalZip.Trim();
				ShippingCompany.Country       = SelectedAccountShippingCountry.Trim();
				ShippingCompany.Notes         = SelectedAccountShippingAddressNotes.Trim();
				ShippingCompany.ZoneName      = SelectedAccountShippingZone.Trim();
				ShippingCompany.EmailAddress2 = SelectedAccountRequireReferenceField ? "RequireReferenceField" : "";
			#endregion

				var UpdatePrimaryCompany = new AddUpdatePrimaryCompany( "AccountsModel" )
										   {
											   Password           = Encryption.ToTimeLimitedToken( SelectedAccountAdminUserPassword ),
											   Company            = Company,
											   BillingCompany     = BillingCompany,
											   ShippingCompany    = ShippingCompany,
											   UserName           = SelectedAccountAdminUserName.Trim(),
											   CustomerCode       = BillingCompany.CompanyNumber.Trim(),
											   SuggestedLoginCode = ""
										   };

				Task.WaitAll(
							 Task.Run( async () =>
									   {
										   var Tmp = await Azure.Client.RequestAddUpdatePrimaryCompany( UpdatePrimaryCompany );
										   Logging.WriteLogLine( "Response: " + Tmp );

										   // 20201119 This had been commented out for some reason
										   var RCompany = BuildCompany( ResellerCompany );

										   if( !IsNew )
										   {
											   var Ucc = new UpdateCustomerCompany( "AccountsModel" )
													     {
														     Company      = RCompany,
														     CustomerCode = SelectedAccountId
													     };
											   await Azure.Client.RequestUpdateCustomerCompany( Ucc );
										   }
										   else
										   {
											   var Acc = new AddCustomerCompany( "AccountsModel" )
													     {
														     Company      = RCompany,
														     CustomerCode = SelectedAccountId
													     };
											   await Azure.Client.RequestAddCustomerCompany( Acc );
										   }
									   } )
							);

				//CustomerLookupSummary? cls = await Azure.Client.RequestCustomerSummaryLookup(SelectedAccountId);
				//if (cls != null)
				//               {
				//	cls.BoolOption1 = SelectedAccountRequireReferenceField;
				//               }

				var Name  = SelectedAccountId;
				var Roles = new List<Role>();

				if( DictAdmins.TryGetValue( Name, out var AdminUser ) )
				{
					Logging.WriteLogLine( "Updating " + Name );
					AdminUser.Address.EmailAddress = SelectedAccountAdminUserEmail.Trim();
					AdminUser.Address.Phone        = SelectedAccountAdminUserPhone.Trim();
					AdminUser.Address.Mobile       = SelectedAccountAdminUserCell.Trim();
					AdminUser.Password             = SelectedAccountAdminUserPassword;
					AdminUser.Enabled              = SelectedAccountAdminUserEnabled;
					AdminUser.FirstName            = SelectedAccountAdminUserFirstName.Trim();
					AdminUser.LastName             = SelectedAccountAdminUserLastName.Trim();

					//if( AdminRole is not null )
					//	Roles.Add( AdminRole );
					if( WebAdminRole != null )
						Roles.Add( WebAdminRole );
				}
				else
				{
					Logging.WriteLogLine( "Adding " + Name );

					Address Address = new()
									  {
										  EmailAddress = SelectedAccountAdminUserEmail.Trim(),
										  Phone        = SelectedAccountAdminUserPhone.Trim(),
										  Mobile       = SelectedAccountAdminUserCell.Trim()
									  };

					AdminUser = new Protocol.Data.Staff
								{
									Address   = Address,
									StaffId   = Name.Trim(),
									Password  = SelectedAccountAdminUserPassword,
									Enabled   = SelectedAccountAdminUserEnabled,
									FirstName = SelectedAccountAdminUserFirstName.Trim(),
									LastName  = SelectedAccountAdminUserLastName.Trim()
								};

					//if( AdminRole is not null )
					//	Roles.Add( AdminRole );
					if( WebAdminRole != null )
						Roles.Add( WebAdminRole );
				}
				AdminUser.Password = Encryption.ToTimeLimitedToken( AdminUser.Password );

				var UpdateData = new UpdateStaffMemberWithRolesAndZones( "AccountsModel", AdminUser )
								 {
									 Roles = Roles
								 };

				Task.Run( async () =>
						  {
							  await Azure.Client.RequestUpdateStaffMember( UpdateData );
						  } );

				Task.Run( async () =>
						  {
							  await GetAccountNamesAsync();
						  } );

				Task.Run( async () =>
						  {
							  await GetAdminUsers();
						  } );

				Task.Run( async () =>
						  {
							  await GetRoles();
						  } );

				Task.Run( () =>
						  {
							  Dispatcher.Invoke( () =>
											     {
												     UpdateTripEntryPagesWithNewAccounts();
											     } );
						  } );

				//Task.WaitAll(
				//Task.Run(async () =>
				//		  {
				//			  await Azure.Client.RequestUpdateStaffMember(UpdateData);
				//			  await GetAccountNamesAsync();
				//			  await GetAdminUsers();
				//			  await GetRoles();
				//			  Dispatcher.Invoke(() =>
				//			  {
				//				  UpdateTripEntryPagesWithNewAccounts();
				//			  });
				//		  });
				//);
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown when saving Account Details: " + E );
			}
		}
		return Errors;
	}

	//private static Company BuildCompany( CompanyBase ca ) =>
	private static Company BuildCompany( CompanyAddress ca ) =>

		// Used by Execute_Delete
		new()
		{
			Phone1          = ca.Phone,
			LocationBarcode = ca.LocationBarcode,
			Suite           = ca.Suite,
			AddressLine1    = ca.AddressLine1,
			City            = ca.City,
			Country         = ca.Country,
			CompanyName     = ca.CompanyName,
			PostalCode      = ca.PostalCode,
			Region          = ca.Region,

			Phone         = ca.Phone,
			EmailAddress  = ca.EmailAddress,
			EmailAddress2 = ca.EmailAddress2,
			Notes         = ca.Notes
		};

	private static void UpdateTripEntryPagesWithNewAccounts()
	{
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		foreach( var Ti in Tis )
		{
			if( Ti.Content is TripEntry )
			{
				Logging.WriteLogLine( "Updating Accounts in extant " + Globals.TRIP_ENTRY + " tabs" );

				// Reload
				var Ti1 = Ti;

				Dispatcher.Invoke( () =>
								   {
									   ( (TripEntryModel)Ti1.Content.DataContext ).ReloadPrimaryCompanies();
								   } );
			}
		}
	}

	private List<string> Validate( Dictionary<string, Control> controls )
	{
		var Errors = new List<string>();

		foreach( var FieldName in controls.Keys )
		{
			string Error;

			switch( FieldName )
			{
			case "SelectedAccountId":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationAccountId" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountName":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationAccountName" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountPhone":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationAccountPhone" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountBillingStreet":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationBillingStreet" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountBillingCity":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationBillingCity" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountBillingProvState":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationBillingProvState" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountBillingCountry":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationBillingCountry" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountBillingPostalZip":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationBillingPostalZip" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountShippingStreet":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationShippingStreet" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountShippingCity":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationShippingCity" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountShippingProvState":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationShippingProvState" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountShippingCountry":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationShippingCountry" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountShippingPostalZip":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationShippingPostalZip" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;

			case "SelectedAccountAdminUserId":
				Error = SetErrorState( controls[ FieldName ], FindStringResource( "AccountsValidationAdminUserId" ) );

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
				break;
			}
		}
		return Errors;
	}

	private static string SetErrorState( Control ctrl, string message )
	{
		var Error = string.Empty;

		if( IsControlEmpty( ctrl ) )
		{
			ctrl.Background = Brushes.Yellow;
			Error           = message;
		}
		else
			ctrl.Background = Brushes.White;
		return Error;
	}

	private static bool IsControlEmpty( Control ctrl )
	{
		return ctrl switch
			   {
				   TextBox Tb         => IsTextBoxEmpty( Tb ),
				   ComboBox Cb        => IsComboBoxSelected( Cb ),
				   DateTimePicker Dtp => IsDateTimePickerEmpty( Dtp ),
				   _                  => true
			   };
	}

	private static bool IsTextBoxEmpty( TextBox tb ) => string.IsNullOrEmpty( tb.Text );

	private static bool IsComboBoxSelected( Selector cb ) => !( cb.SelectedIndex > -1 );

	private static bool IsDateTimePickerEmpty( InputBase dtp ) => string.IsNullOrEmpty( dtp.Text );

	public void Execute_Filter( string filter )
	{
		AdhocFilter = filter;
		var Names = new ObservableCollection<string>();

		if( string.IsNullOrEmpty( filter ) )
		{
			//GetAccountNames();
			foreach( var Name in DictCustomerNamesIds.Keys )
				Names.Add( Name );
		}
		else
		{
			foreach( var Name in AccountNames )
			{
				if( Name.ToLower().Contains( filter.ToLower() ) )
					Names.Add( Name );
			}
		}
		AccountNames = Names;
	}

#region Package Overrides and Discounts
	public void AddUpdatePackageOverride()
	{
		var BasePt = ( from P in PackageTypes
					   where P.Description == SelectedOverrideBasePackageType
					   select P ).FirstOrDefault();

		var NewPt = ( from P in PackageTypes
					  where P.Description == SelectedOverrideNewPackageType
					  select P ).FirstOrDefault();

		if( BasePt is not null && NewPt is not null )
		{
			OverridePackage Po;

			if( SelectedPackageOverride != null )
			{
				Logging.WriteLogLine( "Updating package override" );
				Po = SelectedPackageOverride;

				Po.FromPackageTypeId = BasePt.PackageTypeId;
				Po.ToPackageTypeId   = NewPt.PackageTypeId;
			}
			else
			{
				Logging.WriteLogLine( "Adding package override" );

				Po = new OverridePackage
					 {
						 CompanyId         = SelectedCompanyInternalId,
						 FromPackageTypeId = BasePt.PackageTypeId,
						 ToPackageTypeId   = NewPt.PackageTypeId,
						 SortIndex         = -1
					 };
			}

			Logging.WriteLogLine( "DEBUG Adding/Updating PackageOverride:\nCompanyId: " + Po.CompanyId + " FromPackageTypeId: " + Po.FromPackageTypeId
								  + " ToPackageTypeId: " + Po.ToPackageTypeId + " SortIndex: " + Po.SortIndex + "\n" );

			if( SelectedPackageOverride != null )
			{
				Logging.WriteLogLine( "Updating so deleting original" );

				Task.Run( () =>
						  {
							  _ = Azure.Client.RequestDeletePackageOverride( Po.Id );
						  } );
			}

			Task.Run( () =>
					  {
						  _ = Azure.Client.RequestAddUpdatePackageOverride( Po );
					  } );

			Task.Run( WhenSelectedAccountChanges );
		}
	}

	public bool DoesPackageOverrideExist( string basePackage, string newPackage )
	{
		var Yes = false;

		var BasePt = ( from P in PackageTypes
					   where P.Description == basePackage
					   select P ).FirstOrDefault();

		var NewPt = ( from P in PackageTypes
					  where P.Description == newPackage
					  select P ).FirstOrDefault();

		if( BasePt is not null && NewPt is not null )
		{
			//var item = (from P in PackageOverrides where P.FromPackageTypeId == basePt.PackageTypeId && P.ToPackageTypeId == newPt.PackageTypeId select P).FirstOrDefault();
			var Item = ( from P in PackageOverrides
					     where ( P.FromPackageTypeId == BasePt.PackageTypeId ) && ( P.ToPackageTypeId == NewPt.PackageTypeId )
					     select P ).Any();

			if( Item && ( SelectedPackageOverride == null ) ) // Not editing
				Yes = true;
		}
		return Yes;
	}

	public void DeletePackageOverride( int selectedIndex )
	{
		if( ( selectedIndex > -1 ) && ( selectedIndex < PackageOverrides.Count ) )
		{
			var Po = PackageOverrides[ selectedIndex ];

			Logging.WriteLogLine( "DEBUG Deleting PackageOverride:\nCompanyId: " + Po.CompanyId + " FromPackageTypeId: " + Po.FromPackageTypeId
								  + " ToPackageTypeId: " + Po.ToPackageTypeId + " SortIndex: " + Po.SortIndex + "\n" );

			Task.Run( () =>
					  {
						  _ = Azure.Client.RequestDeletePackageOverride( Po.Id );
					  } );

			Task.Run( WhenSelectedAccountChanges );
		}
	}

	public void LoadPackageOverride( int selectedIndex )
	{
		if( ( selectedIndex > -1 ) && ( PackageOverrides.Count > selectedIndex ) )
		{
			var Po = PackageOverrides[ selectedIndex ];
			SelectedPackageOverride = Po;

			var BasePt = ( from P in PackageTypes
						   where P.PackageTypeId == Po.FromPackageTypeId
						   select P ).FirstOrDefault();

			var NewPt = ( from P in PackageTypes
						  where P.PackageTypeId == Po.ToPackageTypeId
						  select P ).FirstOrDefault();

			if( BasePt is not null && NewPt is not null )
			{
				SelectedOverrideBasePackageType = BasePt.Description;
				SelectedOverrideNewPackageType  = NewPt.Description;
			}
		}
	}

	public void AddUpdatePackageRedirect()
	{
		var Pt = ( from P in PackageTypes
				   where P.Description == SelectedDiscountPackageType
				   select P ).FirstOrDefault();

		var Sl = ( from S in ServiceLevels
				   where S.OldName == SelectedDiscountServiceLevelName
				   select S ).FirstOrDefault();
		Zone? Fz = null;

		if( SelectedFromZone.IsNotNullOrWhiteSpace() )
		{
			Fz = ( from Z in RawZones
				   where Z.Name == SelectedFromZone
				   select Z ).FirstOrDefault();
		}
		Zone? Tz = null;

		if( SelectedToZone.IsNotNullOrWhiteSpace() )
		{
			Tz = ( from Z in RawZones
				   where Z.Name == SelectedToZone
				   select Z ).FirstOrDefault();
		}

		var OverrideId = 0;

		for( var I = 0; I < DiscountTypes.Count; I++ )
		{
			if( SelectedDiscountType == DiscountTypes[ I ] )
			{
				OverrideId = I;
				break;
			}
		}

		RedirectPackage Pr;

		if( Pt is not null && Sl is not null )
		{
			if( SelectedPackageRedirect != null )
			{
				Logging.WriteLogLine( "Updating package redirect" );
				Pr                = SelectedPackageRedirect;
				Pr.Charge         = Value;
				Pr.OverrideId     = OverrideId;
				Pr.PackageTypeId  = Pt.PackageTypeId;
				Pr.ServiceLevelId = Sl.ServiceLevelId;
			}
			else
			{
				Logging.WriteLogLine( "Adding package redirect" );

				Pr = new RedirectPackage
					 {
						 CompanyId      = SelectedCompanyInternalId,
						 Charge         = Value,
						 OverrideId     = OverrideId,
						 PackageTypeId  = Pt.PackageTypeId,
						 ServiceLevelId = Sl.ServiceLevelId
					 };
			}

			if( Fz != null )
				Pr.FromZoneId = Fz.ZoneId;
			else if( SelectedFromZone == FindStringResource( "AccountsPricingDiscountsAll" ) )
				Pr.FromZoneId = -1;

			if( Tz != null )
				Pr.ToZoneId = Tz.ZoneId;
			else if( SelectedToZone == FindStringResource( "AccountsPricingDiscountsAll" ) )
				Pr.ToZoneId = -1;

			Logging.WriteLogLine( "DEBUG Adding/Updating PackageRedirect:\nCompanyId: " + Pr.CompanyId + " Charge: " + Pr.Charge + " FromZoneId: " + Pr.FromZoneId + " ToZoneId: " + Pr.ToZoneId
								  + " OverrideId: " + Pr.OverrideId + " PackageTypeId: " + Pr.PackageTypeId + " ServiceLevelId: " + Pr.ServiceLevelId + "\n" );

			if( SelectedPackageRedirect != null )
			{
				Logging.WriteLogLine( "Updating so deleting original" );

				Task.Run( () =>
						  {
							  _ = Azure.Client.RequestDeletePackageRedirect( Pr.Id );
						  } );
			}

			Task.Run( () =>
					  {
						  _ = Azure.Client.RequestAddUpdatePackageRedirect( Pr );
					  } );

			Task.Run( WhenSelectedAccountChanges );
		}
	}

	public bool DoesPackageRedirectExist() => false;

	public void DeletePackageRedirect( int selectedIndex )
	{
		if( AccountDiscounts.Count > selectedIndex )
		{
			var Pr = AccountDiscounts[ selectedIndex ];

			Logging.WriteLogLine( "DEBUG Deleting PackageRedirect:\nCompanyId: " + Pr.CompanyId + " Charge: " + Pr.Charge + " FromZoneId: " + Pr.FromZoneId + " ToZoneId: " + Pr.ToZoneId
								  + " OverrideId: " + Pr.OverrideId + " PackageTypeId: " + Pr.PackageTypeId + " ServiceLevelId: " + Pr.ServiceLevelId + "\n" );

			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   await Azure.Client.RequestDeletePackageRedirect( Pr.Id );
								   } ),
						 Task.Run( WhenSelectedAccountChanges )
						);
		}
	}

	public void LoadPackageRedirect( int selectedIndex )
	{
		if( ( selectedIndex > -1 ) && ( AccountDiscounts.Count > selectedIndex ) )
		{
			var Pr = AccountDiscounts[ selectedIndex ];
			SelectedPackageRedirect = Pr;

			var Pt = ( from P in PackageTypes
					   where P.PackageTypeId == Pr.PackageTypeId
					   select P ).FirstOrDefault();

			var Sl = ( from S in ServiceLevels
					   where S.ServiceLevelId == Pr.ServiceLevelId
					   select S ).FirstOrDefault();
			var DiscountType = Pr.OverrideId;

			var Fz = ( from Z in RawZones
					   where Z.ZoneId == Pr.FromZoneId
					   select Z ).FirstOrDefault();

			var Tz = ( from Z in RawZones
					   where Z.ZoneId == Pr.ToZoneId
					   select Z ).FirstOrDefault();
			var Charge = Pr.Charge;

			SelectedDiscountServiceLevelName = Sl?.OldName ?? string.Empty;
			SelectedDiscountPackageType      = Pt?.Description ?? string.Empty;
			SelectedDiscountType             = DiscountTypes[ DiscountType ];

			if( Fz != null )
				SelectedFromZone = Fz.Name;
			else
			{
				SelectedFromZone = Pr.FromZoneId switch
								   {
									   0  => "",
									   -1 => FindStringResource( "AccountsPricingDiscountsAll" ),
									   _  => ""
								   };
			}

			if( Tz != null )
				SelectedToZone = Tz.Name;
			else
			{
				SelectedToZone = Pr.ToZoneId switch
								 {
									 0  => "",
									 -1 => FindStringResource( "AccountsPricingDiscountsAll" ),
									 _  => ""
								 };
			}
			Value = Charge;
		}
	}
#endregion
#endregion

#region Default Charges Data
	// ReSharper disable once InconsistentNaming
	public readonly string ACCOUNT_CHARGE_PREFIX = Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX;

	public ObservableCollection<DisplayCharge> RawCharges
	{
		//get { return Get(() => Charges, GetCharges()); }
		get { return Get( () => RawCharges, new ObservableCollection<DisplayCharge>() ); }
		set { Set( () => RawCharges, value ); }
	}

	public ObservableCollection<DisplayCharge> Overrides
	{
		//get { return Get(() => Charges, GetCharges()); }
		get { return Get( () => Overrides, new ObservableCollection<DisplayCharge>() ); }
		set { Set( () => Overrides, value ); }
	}

	public List<DisplayCharge> DefaultCharges
	{
		get => Get( () => DefaultCharges, new List<DisplayCharge>() );
		set => Set( () => DefaultCharges, value );
	}

	public Dictionary<string, List<DisplayCharge>> DictAccountIdDefaultCharges
	{
		get => Get( () => DictAccountIdDefaultCharges, new Dictionary<string, List<DisplayCharge>>() );
		set => Set( () => DictAccountIdDefaultCharges, value );
	}

	//public List<Charge> OverridesCharges
	//{
	//    get { return Get(() => OverridesCharges, new List<Charge>()); }
	//    set { Set(() => OverridesCharges, value); }
	//}
#endregion

#region Default Charges Actions
	public async Task GetRawCharges()
	{
		if( !IsInDesignMode )
		{
			try
			{
				List<DisplayCharge> Charges = new();
				Logging.WriteLogLine( "Getting charges..." );
				var Result = await Azure.Client.RequestGetAllCharges();
				Logging.WriteLogLine( $"Found {Result.Count} charges" );

				// Exclude both account and standard charge overrides
				List<Charge> Tmp = ( from C in Result
								     orderby C.SortIndex
								     where !C.ChargeId.StartsWith( ACCOUNT_CHARGE_PREFIX ) && !C.IsOverride
								     select C ).ToList();

				foreach( var C in Tmp )
				{
					DisplayCharge Dc = new( C );
					Charges.Add( Dc );
				}
				RawCharges = new ObservableCollection<DisplayCharge>( Charges );

				// Now get the overrides
				Charges.Clear();

				Tmp = ( from C in Result
					    orderby C.SortIndex
					    where C.IsOverride
					    select C ).ToList();

				foreach( var C in Tmp )
				{
					DisplayCharge Dc = new( C );
					Charges.Add( Dc );
				}
				Overrides = new ObservableCollection<DisplayCharge>( Charges );

				// Now get the default overrides
				Charges.Clear();

				// Load the default charges for each account
				foreach( var Key in DictCustomerNamesIds.Keys )
				{
					var Id = DictCustomerNamesIds[ Key ].CustomerCode;

					Tmp = ( from C in Result
						    orderby C.SortIndex
						    where C.ChargeId.StartsWith( ACCOUNT_CHARGE_PREFIX ) && !C.IsOverride && ( C.AccountId == Id )
						    select C ).ToList();

					if( Tmp.Count > 0 )
					{
						foreach( var C in Tmp )
						{
							DisplayCharge Dc = new( C );
							Charges.Add( Dc );
						}
					}

					if( !DictAccountIdDefaultCharges.ContainsKey( Id ) )
						DictAccountIdDefaultCharges.Add( Id, Charges );
					else
						DictAccountIdDefaultCharges[ Id ] = Charges;
				}
				DefaultCharges = new List<DisplayCharge>( Charges );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown: " + E );

				Dispatcher.Invoke( () =>
								   {
									   MessageBox.Show( "Error fetching Charges\n" + E, "Error", MessageBoxButton.OK );
								   } );
			}
		}
		//return charges;
	}

	public void Execute_UpdateDefaultCharges()
	{
		if( SelectedAccountName.IsNotNullOrWhiteSpace() )
		{
			//Charges toSave = new();
			Charges ToDelete = new();

			DefaultCharges = new List<DisplayCharge>();
			List<DisplayCharge> OldDcs = new();
			OldDcs.AddRange( DictAccountIdDefaultCharges[ SelectedAccountId ] );

			foreach( var Dc in RawCharges )
			{
				Logging.WriteLogLine( $"DEBUG ChargeId: {Dc.ChargeId} IsOverrideApplied: {Dc.IsOverrideApplied} IsChargeApplied: {Dc.IsChargeApplied}" );

				if( Dc.IsOverrideApplied || Dc.IsChargeApplied )
				{
					// Make default
					var Found = ( from D in OldDcs
								  where D.ChargeId == PriceTripHelper.MakeDefaultChargeId( SelectedAccountId, Dc.ChargeId )
								  select D ).FirstOrDefault();

					if( Found != null )
					{
						var Si = MakeSortIndex( Dc );

						if( Si < 0 )
							Found.SortIndex = Si;

						if( Found.SortIndex != Dc.SortIndex )
						{
							// Changed
							Logging.WriteLogLine( $"Saving modified Default Charge: {Dc.ChargeId}" );
							//toSave.Add(dc);
						}
						DefaultCharges.Add( Found );
					}
					else
					{
						DisplayCharge NewDefault = new( Dc )
												   {
													   ChargeId  = PriceTripHelper.MakeDefaultChargeId( SelectedAccountId, Dc.ChargeId ),
													   AccountId = SelectedAccountId
												   };
						var Si = MakeSortIndex( Dc );

						if( Si < 0 )
							NewDefault.SortIndex = Si;
						Logging.WriteLogLine( $"Adding Default Charge: {NewDefault.ChargeId}" );
						DefaultCharges.Add( NewDefault );
						//toSave.Add(newDefault.GetAsCharge());
					}
				}
				else
				{
					// Check for turning off of the flags
					var Found = ( from D in OldDcs
								  where D.ChargeId == PriceTripHelper.MakeDefaultChargeId( SelectedAccountId, Dc.ChargeId )
								  select D ).FirstOrDefault();

					if( Found != null )
					{
						Logging.WriteLogLine( $"Deleting Default Charge: {Found.ChargeId}" );
						ToDelete.Add( Found );
						DefaultCharges.Remove( Found );
					}
				}
			}

			if( !DictAccountIdDefaultCharges.ContainsKey( SelectedAccountId ) )
				DictAccountIdDefaultCharges.Add( SelectedAccountId, DefaultCharges );
			else
				DictAccountIdDefaultCharges[ SelectedAccountId ] = DefaultCharges;

			// TODO Perform the save - updates and deletions
			Charges ToSave = new();
			ToSave.AddRange( DefaultCharges );

			if( ToSave.Count > 0 )
			{
				foreach( var Dc in RawCharges )
					ToSave.Add( Dc.GetAsCharge() );

				if( Overrides.Count > 0 )
				{
					foreach( var Dc in Overrides )
						ToSave.Add( Dc.GetAsCharge() );
				}
				Logging.WriteLogLine( $"Adding/Updating {ToSave.Count} Default Charges" );
				Logging.WriteLogLine( $"Deleting {ToDelete.Count} Default Charges" );

				Task.WaitAll(
							 Task.Run( async () =>
									   {
										   await Azure.Client.RequestAddUpdateCharges( ToSave );
									   } )
							);
			}

			// Inform the user
			var Title   = FindStringResource( "AccountsDefaultChargesUpdatedTitle" );
			var Message = FindStringResource( "AccountsDefaultChargesUpdatedMessage" );
			Message = Message.Replace( "@1", SelectedAccountName );

			Dispatcher.Invoke( () =>
							   {
								   Xceed.Wpf.Toolkit.MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );
							   } );
		}
		else
		{
			var Title   = FindStringResource( "AccountsDefaultChargesUpdatedTitle" );
			var Message = FindStringResource( "AccountsDefaultChargesUpdatedSelectAccountMessage" );
			Message = Message.Replace( "@1", SelectedAccountName );

			Dispatcher.Invoke( () =>
							   {
								   Xceed.Wpf.Toolkit.MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
							   } );
		}
	}

	private static short MakeSortIndex( DisplayCharge dc )
	{
		short Index = 0;

		switch( dc.IsOverrideApplied )
		{
		case true when dc.IsChargeApplied:
			Index = -3;
			break;

		case true:
			Index = -2;
			break;

		default:
			if( dc.IsChargeApplied )
				Index = -1;
			break;
		}
		return Index;
	}
#endregion
}