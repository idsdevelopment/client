﻿#nullable enable

using System.Collections;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using Utils.Csv;
using ViewModels.Trips.SearchTrips;
using ViewModels.Trips.TripEntry;
using ViewModels.Uplift.Shipments;
using Xceed.Wpf.Toolkit;
using File = System.IO.File;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using SearchTrips = ViewModels.Trips.SearchTrips.SearchTrips;

namespace ViewModels.Customers.AddressBook;

public class AddressBookModel : ViewModelBase
{
	/// <summary>
	///     Used to restore the filter after saving.
	/// </summary>
	public enum WHICH_FILTER
	{
		NONE,
		COMPANY_NAME,
		LOCATION_BARCODE,
		CITY,
		REGION
	}


	public WHICH_FILTER CurrentFilter
	{
		get { return Get( () => CurrentFilter, WHICH_FILTER.NONE ); }
		set { Set( () => CurrentFilter, value ); }
	}


	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/649756883/Address+Book" ); }
	}

	// Use for global address book
	public string GlobalAddressBookId { get; set; } = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		Task.Run( () =>
				  {
					  Task.WaitAll(
								   GetAccountNames(),
								   LoadGroups()
								  );

					  Execute_ClearAddress();
					  Loaded = true;
				  } );
	}

	private readonly List<ExtendedCompanyAddress> CurrentCompanyAddresses = new();

	public override void OnArgumentChange( object? selectedAccountName )
	{
		if( selectedAccountName is string San )
		{
			Task.Run( async () =>
					  {
						  SelectedAccountName = San;

						  var Name = ( from D in AccountNames
									   where D.StartsWith( San )
									   select D ).FirstOrDefault();

						  if( Name is not null )
						  {
							  var Start = Name.LastIndexOf( '(' ) + 1;
							  var Len   = Name.LastIndexOf( ')' ) - Start;

							  if( ( Start + Len ) <= Name.Length )
							  {
								  var Id = Name.Substring( Start, Len );
								  SelectedAccountId = Id;
								  GroupNames        = new AddressIdList();

								  await LoadAddresses();
								  await LoadGroups();
							  }
						  }

						  // await WhenSelectedAccountNameChanges();
					  } );
		}
	}

#region Filter Addresses
	public string FilterCompanyName
	{
		get { return Get( () => FilterCompanyName, "" ); }
		set { Set( () => FilterCompanyName, value ); }
	}

	//[DependsUpon(nameof(FilterCompanyName))]
	public void WhenFilterCompanyNameChanges( string text )
	{
		Logging.WriteLogLine( "Filtering by Company Name: " + FilterCompanyName + ", text: " + text );
		CurrentFilter = WHICH_FILTER.COMPANY_NAME;
		//FilterCompanyName = text.ToLower();
		FilterCompanyName = text;

		if( FilterCompanyName.IsNotNullOrWhiteSpace() )
		{
			var Ecas = ( from Eca in CurrentCompanyAddresses
						 where Eca.Ca.CompanyName.ToLower().Contains( FilterCompanyName.ToLower() )
						 select Eca ).ToList();

			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( Ecas );
							   } );
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );
							   } );
		}
	}


	public string FilterLocationBarcode
	{
		get { return Get( () => FilterLocationBarcode, "" ); }
		set { Set( () => FilterLocationBarcode, value ); }
	}

	//[DependsUpon(nameof(FilterLocationBarcode))]
	public void WhenFilterLocationBarcodeChanges( string text )
	{
		Logging.WriteLogLine( "Filtering by Location Barcode: " + FilterLocationBarcode + ", text: " + text );
		CurrentFilter = WHICH_FILTER.LOCATION_BARCODE;
		//FilterLocationBarcode = text.ToLower();
		FilterLocationBarcode = text;

		if( FilterLocationBarcode.IsNotNullOrWhiteSpace() )
		{
			var Ecas = ( from Eca in CurrentCompanyAddresses
						 where Eca.Ca.LocationBarcode.ToLower().Contains( FilterLocationBarcode.ToLower() )
						 select Eca ).ToList();

			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( Ecas );
							   } );
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );
							   } );
		}
	}


	public List<string> FilterCities
	{
		get { return Get( () => FilterCities, GetCities ); }
		set { Set( () => FilterCities, value ); }
	}


	/// <summary>
	///     Walks the currently used cities and builds a sorted list.
	/// </summary>
	/// <returns></returns>
	[DependsUpon( nameof( FilterCities ) )]
	private List<string> GetCities()
	{
		Logging.WriteLogLine( "Loading currently used cities in the current account's addresses" );

		var Cities = ( from A in ExtendedAddresses
					   let City = A.Ca.City.Trim().ToLower()
					   orderby City
					   group City by City
					   into G
					   select G.First() ).ToList();

		if( ( Cities.Count == 0 ) || Cities[ 0 ].IsNotNullOrWhiteSpace() )
			Cities.Insert( 0, "" ); // Make sure that there is an empty line

		FilterCities = Cities;

		return Cities;
	}


	public string FilterSelectedCity
	{
		get { return Get( () => FilterSelectedCity, "" ); }
		set { Set( () => FilterSelectedCity, value ); }
	}

	[DependsUpon( nameof( FilterSelectedCity ) )]
	public void WhenFilterSelectedCityChanges()
	{
		Logging.WriteLogLine( "FilterSelectedCity: " + FilterSelectedCity );
		CurrentFilter = WHICH_FILTER.CITY;

		if( FilterSelectedCity.IsNotNullOrWhiteSpace() )
		{
			ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );

			if( ExtendedAddresses.Count > 0 )
			{
				// TODO there may be a problem with sorting
				var Distinct = from Eca in CurrentCompanyAddresses
							   where Eca.Ca.City == FilterSelectedCity
							   select Eca;
				var Ecas = Distinct.Distinct();

				//var sd = new SortedDictionary<string, ExtendedCompanyAddress>();

				//foreach( var eca in ExtendedAddresses )
				//{
				//	if( eca.Ca.City == FilterSelectedCity )
				//	{
				//		if( !sd.ContainsKey( eca.Ca.CompanyName.ToLower() ) )
				//			sd.Add( eca.Ca.CompanyName.ToLower(), eca );
				//	}
				//}

				//var ecas = new List<ExtendedCompanyAddress>();

				//foreach( var key in sd.Keys )
				//	ecas.Add( sd[ key ] );

				Dispatcher.Invoke( () =>
								   {
									   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( Ecas );
								   } );
			}
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );
							   } );
		}
	}


	public List<string> FilterRegions
	{
		get { return Get( () => FilterRegions, new List<string>() ); }
		set { Set( () => FilterRegions, value ); }
	}


	public string FilterSelectedRegion
	{
		get { return Get( () => FilterSelectedRegion, "" ); }
		set { Set( () => FilterSelectedRegion, value ); }
	}

	[DependsUpon( nameof( FilterSelectedRegion ) )]
	public void WhenFilterSelectedRegionChanges()
	{
		Logging.WriteLogLine( "FilterSelectedRegion: " + FilterSelectedRegion );
		CurrentFilter = WHICH_FILTER.REGION;

		if( FilterSelectedRegion.IsNotNullOrWhiteSpace() )
		{
			ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );

			if( ExtendedAddresses.Count > 0 )
			{
				var Sd = new SortedDictionary<string, ExtendedCompanyAddress>();

				foreach( var Eca in ExtendedAddresses )
				{
					if( Eca.Ca.Region == FilterSelectedRegion )
					{
						if( !Sd.ContainsKey( Eca.Ca.CompanyName.ToLower() ) )
						{
							Logging.WriteLogLine( "Found " + Eca.Ca.CompanyName + ", Region: " + Eca.Ca.Region );
							Sd.Add( Eca.Ca.CompanyName.ToLower(), Eca );
						}
					}
					else
					{
						// Check to see if the region is an abbreviation
						// FilterSelectedRegion == "New South Wales", eca.Ca.Region == "NSW"
						var Ras = CountriesRegions.GetRegionAndAbbreviationsForCountry( Eca.Ca.Country );

						foreach( var Key in Ras.Keys )
						{
							Logging.WriteLogLine( "Comparing " + Ras[ Key ].ToLower() + " to " + Eca.Ca.Region.ToLower() );

							if( ( Key == FilterSelectedRegion ) && string.Equals( Ras[ Key ], Eca.Ca.Region, StringComparison.CurrentCultureIgnoreCase ) )
							{
								if( !Sd.ContainsKey( Eca.Ca.CompanyName.ToLower() ) )
								{
									Logging.WriteLogLine( "Found by abbreviation: " + Eca.Ca.CompanyName + ", Region: " + Eca.Ca.Region );
									Sd.Add( Eca.Ca.CompanyName.ToLower(), Eca );

									break;
								}
							}
						}
					}
				}

				var Ecas = new List<ExtendedCompanyAddress>();

				foreach( var Key in Sd.Keys )
					Ecas.Add( Sd[ Key ] );

				Dispatcher.Invoke( () =>
								   {
									   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( Ecas );
								   } );
			}
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );
							   } );
		}
	}

	public void Execute_ClearFilter()
	{
		Logging.WriteLogLine( "Clearing Filter" );
		FilterCompanyName     = string.Empty;
		FilterLocationBarcode = string.Empty;
		FilterSelectedCity    = string.Empty;
		FilterSelectedRegion  = string.Empty;
		CurrentFilter         = WHICH_FILTER.NONE;

		// Reload unfiltered list
		Dispatcher.Invoke( () =>
						   {
							   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );
						   } );
	}
#endregion


#region Groups
	public int NextGroupNumber
	{
		get { return Get( () => NextGroupNumber, -1 ); }
		set { Set( () => NextGroupNumber, value ); }
	}


	public List<CompanyAddressGroup> Groups
	{
		get { return Get( () => Groups, new List<CompanyAddressGroup>() ); }
		set { Set( () => Groups, value ); }
	}

	public List<CompanyAddressGroup> AllGroups
	{
		get { return Get( () => AllGroups, new List<CompanyAddressGroup>() ); }
		set { Set( () => AllGroups, value ); }
	}

	public List<string> GroupNames
	{
		get { return Get( () => GroupNames, new List<string>() ); }
		set { Set( () => GroupNames, value ); }
	}

	[DependsUpon( nameof( GroupNames ) )]
	public void WhenGroupNamesChanges()
	{
		var Gns = string.Empty;

		foreach( var Gn in GroupNames )
			Gns += Gn + ", ";
		Logging.WriteLogLine( "GroupNames has changed: " + Gns );
	}

	/// <summary>
	///     Holds a map of group names and their companies.
	/// </summary>
	private readonly Dictionary<string, List<string>> GroupsAndMembers = new();

	private async Task<List<CompanyAddressGroup>> LoadGroups()
	{
		Logging.WriteLogLine( "Loading groups" );
		var AddressGroups = new List<CompanyAddressGroup>();

		try
		{
			var Cags = await Azure.Client.RequestGetCompanyAddressGroups();

			//Logging.WriteLogLine("DEBUG cags.Count: " + cags?.Count);
			if( Cags is not null )
			{
				var Members = await PopulateGroupsAndMembers( Cags );

				//Logging.WriteLogLine("DEBUG SelectedAccountId: " + SelectedAccountId);
				//var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( SelectedAccountId );

				//AddressGroups = AddressesHelper.GetGroupsForAccount( Cags, Members, Details, SelectedAccountId, true );
				AddressGroups = AddressesHelper.GetGroupsForAccount( Cags, Members, CurrentDetailList, SelectedAccountId, true );

				AllGroups = new List<CompanyAddressGroup>();
				AllGroups.AddRange( Cags );

				//
				// TODO Deletions are going to leave this out of sync
				//
				NextGroupNumber = Cags.Count;
				Logging.WriteLogLine( "Found " + AddressGroups.Count + " groups ; next GroupNumber: " + NextGroupNumber );

				Groups = AddressGroups;

				var Grps = ( from A in AddressGroups
							 select A.Description ).ToList();

				var FilteredGroups = ( from M in Members
									   where Grps.Contains( M.Key )
									   orderby M.Key
									   select M ).ToDictionary( m => m.Key, m => m.Value );

				LoadGroupNames( FilteredGroups );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown: " + E );
		}

		return AddressGroups;
	}


	private async Task<Dictionary<string, List<string>>> PopulateGroupsAndMembers( IEnumerable<CompanyAddressGroup> cags )
	{
		try
		{
			var CompanyAddressGroupList = new CompanyAddressGroupList();

			CompanyAddressGroupList.AddRange( from C in cags
											  select C.GroupNumber );

			var Companies = await Azure.Client.RequestGetCompaniesWithinAddressGroups( CompanyAddressGroupList );

			GroupsAndMembers.Clear();

			foreach( var Comp in Companies )
			{
				GroupsAndMembers[ Comp.Description ] = ( from Company in Comp.Companies
														 orderby Company
														 select Company ).ToList();
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown: " + E );
		}

		return GroupsAndMembers;
	}

	private void LoadGroupNames( Dictionary<string, List<string>> members )
	{
		//var members        = (string)Application.Current.TryFindResource( "AddressBookGroupsMembers" );
		var LabelSingle    = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountSingle" );
		var LabelNotSingle = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountNotSingle" );
		var List           = new List<string>();

		foreach( var Member in members )
		{
			var Key   = Member.Key;
			var Value = Member.Value;

			Logging.WriteLogLine( $"key: {Key}" );

			var ValueCount = Value.Count;

			var Line = $"{Key} \t( {ValueCount} ";

			if( ValueCount != 1 )
			{
				//line = sd[key] + " \t(" + counts[key] + " members)"; 
				Line += LabelNotSingle + ")";
			}
			else
				Line += LabelSingle + ")";

			Logging.WriteLogLine( "line: " + Line );
			List.Add( Line );
		}

		GroupNames = List;
		Loaded     = true;
	}


	public string NewGroupDescription
	{
		get { return Get( () => NewGroupDescription, "" ); }

		set { Set( () => NewGroupDescription, value ); }

		//set
		//{
		//	NewGroupDescription = value;
		//	IsCreateGroupEnabled = NewGroupDescription.IsNotNullOrWhiteSpace();
		//	Logging.WriteLogLine("IsCreateGroupEnabled: " + IsCreateGroupEnabled);
		//}			
	}


	//[DependsUpon(nameof(NewGroupDescription))]
	public bool IsCreateGroupEnabled
	{
		get { return Get( () => IsCreateGroupEnabled, false ); }

		set { Set( () => IsCreateGroupEnabled, value ); }

		//get => NewGroupDescription.IsNotNullOrWhiteSpace();
		//get
		//{
		//	bool enable = false;
		//	if (NewGroupDescription.IsNotNullOrWhiteSpace())
		//	{
		//		enable = true;
		//	}
		//	Logging.WriteLogLine("IsCreateGroupEnabled: " + enable);

		//	return enable;
		//}
	}


	public void Execute_CreateGroup()
	{
		if( NewGroupDescription.IsNotNullOrWhiteSpace() )
		{
			Logging.WriteLogLine( "Creating group: " + NewGroupDescription );

			var Exists = CreateGroup( NewGroupDescription );

			if( Exists )
				NewGroupDescription = string.Empty;
		}
	}

	private bool CreateGroup( string groupName )
	{
		var Exists = false;

		//foreach( var ag in Groups )
		foreach( var Ag in AllGroups )
		{
			if( string.Equals( Ag.Description, groupName, StringComparison.CurrentCultureIgnoreCase ) )
			{
				Exists = true;
				break;
			}
		}

		if( !Exists )
		{
			//Task.WaitAll(Task.Run(() =>
			Task.Run( async () =>
					  {
						  try
						  {
							  NextGroupNumber = await GetNextGroupNumber();

							  var Cag = new CompanyAddressGroup
										{
											Description = groupName,
											GroupNumber = NextGroupNumber
										};
							  await Azure.Client.RequestAddUpdateCompanyAddressGroup( Cag );

							  var Title   = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupCreatedTitle" );
							  var Message = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupCreated" );
							  Message = Message.Replace( "@1", groupName );

							  await Dispatcher.Invoke( async () =>
													   {
														   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );
														   groupName = string.Empty;
														   await LoadGroups();
													   } );
						  }
						  catch( Exception E )
						  {
							  Logging.WriteLogLine( "Exception thrown: " + E );
						  }
					  } );
		}
		else
		{
			var Title   = (string)Application.Current.TryFindResource( "AddressBookGroupsErrorGroupAlreadyExistsTitle" );
			var Message = (string)Application.Current.TryFindResource( "AddressBookGroupsErrorGroupAlreadyExists" );
			Message = Message.Replace( "@1", groupName );

			Dispatcher.Invoke( () =>
							   {
								   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
							   } );
		}

		return Exists;
	}

	private async Task<int> GetNextGroupNumber()
	{
		var GroupNumber = -1;

		var HsLocal = new HashSet<int>();

		foreach( var Cag in Groups )
			HsLocal.Add( Cag.GroupNumber );

		CompanyAddressGroups? Cags = null;

		try
		{
			Cags = await Azure.Client.RequestGetCompanyAddressGroups();
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown: " + E );
		}

		if( Cags is not null )
		{
			var HsRemote = new HashSet<int>();

			foreach( var Cag in Cags )
				HsRemote.Add( Cag.GroupNumber );

			for( var I = 0; I < int.MaxValue; I++ )
			{
				// Make sure that it isn't used locally
				if( !HsRemote.Contains( I ) && !HsLocal.Contains( I ) )
				{
					GroupNumber = I;

					break;
				}
			}
		}

		return GroupNumber;
	}

	public List<ExtendedCompanyAddress> SelectedAddresses

	{
		get { return Get( () => SelectedAddresses, new List<ExtendedCompanyAddress>() ); }
		set { Set( () => SelectedAddresses, value ); }
	}

	public void Execute_AddSelectedToGroup()
	{
		if( SelectedAddresses.Count > 0 )
		{
			var Names = new List<string>();
			Names.AddRange( GroupAddressNames );

			foreach( var Tmp in SelectedAddresses )
			{
				if( !Names.Contains( Tmp.Ca.CompanyName ) )
					Names.Add( Tmp.Ca.CompanyName );
				else
					Logging.WriteLogLine( "Skipping " + Tmp.Ca.CompanyName + " - already in list" );
			}

			GroupAddressNames = Names;
		}
	}

	public void Execute_RemoveSelectedFromGroup()
	{
		if( SelectedGroupMembers.Count > 0 )
		{
			//var keep = (from gan in SelectedGroupMembers
			//		select gan).Distinct().ToList();

			var Keep = new List<string>();

			foreach( var Gan in GroupAddressNames )
			{
				if( !SelectedGroupMembers.Contains( Gan ) )
					Keep.Add( Gan );
			}

			GroupAddressNames = Keep;
		}
	}


	public List<string> SelectedGroupMembers

	{
		get { return Get( () => SelectedGroupMembers, new List<string>() ); }
		set { Set( () => SelectedGroupMembers, value ); }
	}


	public List<string> GroupAddressNames

	{
		get { return Get( () => GroupAddressNames, new List<string>() ); }
		set { Set( () => GroupAddressNames, value ); }
	}

	//[DependsUpon(nameof(GroupAddressNames))]
	public void WhenGroupAddressNamesChanges()
	{
		StringBuilder Sb = new();

		foreach( var Name in GroupAddressNames )
			Sb.Append( Name ).Append( "\n" );
		Logging.WriteLogLine( "DEBUG\n" + Sb );
	}

	public string SelectedGroup

	{
		get { return Get( () => SelectedGroup, "" ); }
		set { Set( () => SelectedGroup, value ); }
	}

	[DependsUpon( nameof( SelectedGroup ) )]
	public bool IsSelectedGroupPopulated() => SelectedGroup.IsNotNullOrWhiteSpace();


	public int SelectedGroupTab

	{
		get { return Get( () => SelectedGroupTab, 0 ); }
		set { Set( () => SelectedGroupTab, value ); }
	}

	public void Execute_ModifyGroup()
	{
		if( SelectedGroup.IsNotNullOrWhiteSpace() )
		{
			Logging.WriteLogLine( "Modifying " + SelectedGroup );

			SelectedGroupTab = 1;

			// Load the addresses for the group
			Task.Run( async () =>
					  {
						  var Cag = GetGroupForName( SelectedGroup );

						  GroupAddressNames.Clear();

						  if( Cag is not null )
						  {
							  var AddressGroups = await Azure.Client.RequestGetCompaniesWithinAddressGroup( Cag.GroupNumber );

							  if( AddressGroups is not null && ( AddressGroups.Count > 0 ) && ( AddressGroups[ 0 ].Companies.Count > 0 ) )
							  {
								  GroupAddressNames = ( from C in AddressGroups[ 0 ].Companies
														orderby C
														select C ).ToList();
							  }
						  }
					  } );
		}
	}

	public async void Execute_SaveGroup()
	{
		await SaveGroup( SelectedGroup, GroupAddressNames );
		SelectedGroup = string.Empty;
		GroupAddressNames.Clear();
		NewGroupDescription = string.Empty;
		SelectedGroupTab    = 0;
	}

	private async Task SaveGroup( string group, List<string> names, bool showMessage = true )
	{
		IsImporting = true;

		try
		{
			var CopyOfNames = new List<string>();
			CopyOfNames.AddRange( names );

			Logging.WriteLogLine( "Saving Group " + group + " with " + CopyOfNames.Count + " Address(es)" );

			var Cag = GetGroupForName( group );

			// First, empty the group by deleting and adding it
			//await Azure.Client.RequestDeleteCompanyAddressGroup(cag.GroupNumber);
			//await Azure.Client.RequestAddUpdateCompanyAddressGroup( cag );

			if( CopyOfNames.Count > 0 )
			{
				if( Cag is null )
				{
					Cag = new CompanyAddressGroup
						  {
							  Description = group,
							  GroupNumber = await GetNextGroupNumber()
						  };
					await Azure.Client.RequestAddUpdateCompanyAddressGroup( Cag );
				}
				var GroupNumber = Cag.GroupNumber;

				var AddressGroups = await Azure.Client.RequestGetCompaniesWithinAddressGroup( GroupNumber );

				if( AddressGroups is {Count: > 0} )
				{
					List<string> Current = new( AddressGroups[ 0 ].Companies );

					var (ToRemove, ToAdd) = GetDifferencesForGroupMembership( Current, CopyOfNames );

					if( ToRemove.Count > 0 )
					{
						foreach( var Remove in ToRemove )
						{
							Logging.WriteLogLine( "Removing " + Remove + " from group " + group );

							ImportingCompanyName = $"(Removing From Group) {Remove}";

							var Cage = new CompanyAddressGroupEntry
									   {
										   CompanyName = Remove,
										   GroupNumber = GroupNumber
									   };
							await Azure.Client.RequestDeleteCompanyAddressGroupEntry( Cage );
						}
					}

					if( ToAdd.Count > 0 )
					{
						foreach( var Add in ToAdd )
						{
							Logging.WriteLogLine( "Adding " + Add + " to group " + group );

							ImportingCompanyName = $"(Adding To Group) {Add}";

							var Cage = new CompanyAddressGroupEntry
									   {
										   CompanyName = Add,
										   GroupNumber = GroupNumber
									   };
							await Azure.Client.RequestAddCompanyAddressGroupEntry( Cage );
						}
					}
				}
			}

			if( showMessage )
			{
				var Title   = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupSavedTitle" );
				var Message = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupSaved" );

				//message = message.Replace("@1", group);
				var Tmp = group;

				if( Tmp.Contains( "\t" ) )
					Tmp = Tmp.Substring( 0, Tmp.IndexOf( "\t", StringComparison.Ordinal ) );

				Message = Message.Replace( "@1", Tmp );

				await Dispatcher.Invoke( async () =>
										 {
											 MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );
											 NewGroupDescription = string.Empty;

											 //group = string.Empty;
											 CopyOfNames.Clear();

											 await LoadGroups();
										 } );
			}
		}
		finally
		{
			IsImporting = false;
		}
	}

	private static (List<string> toRemove, List<string> toAdd) GetDifferencesForGroupMembership( IReadOnlyCollection<string> currentMembers, IReadOnlyCollection<string> importNames )
	{
		List<string> ToRemove = new();
		List<string> ToAdd    = new();

		// Case for new group
		if( currentMembers.Count == 0 )
		{
			Logging.WriteLogLine( "New group - adding all newMembers" );
			ToAdd.AddRange( importNames );
		}
		else
		{
			ToRemove.AddRange( from Current in currentMembers
							   let Found = ( from Tmp in importNames
											 where Tmp == Current
											 select Tmp ).FirstOrDefault()
							   where Found is null
							   select Current );

			ToAdd.AddRange( from Name in importNames
							let Found = ( from Tmp in currentMembers
										  where Tmp == Name
										  select Tmp ).FirstOrDefault()
							where Found is null
							select Name );
		}

		return ( ToRemove, ToAdd );
	}

	public IList GroupAddresses

	{
		//get { return Get(() => GroupAddresses, new List<CompaniesWithinAddressGroup>()); }
		//get { return Get(() => GroupAddresses, new List<CompanyAddress>()); }
		get { return Get( () => GroupAddresses, new List<string>() ); }
		set { Set( () => GroupAddresses, value ); }
	}

	private CompanyAddressGroup? GetGroupForName( string name )
	{
		CompanyAddressGroup? Cag = null;

		// "test group 0 \t(0 members)"
		if( name.IndexOf( "\t", StringComparison.Ordinal ) > -1 )
			name = name.Substring( 0, name.IndexOf( "\t", StringComparison.Ordinal ) ).TrimEnd();

		foreach( var Tmp in Groups )
		{
			if( Tmp.Description == name )
			{
				Cag = Tmp;

				break;
			}
		}

		return Cag;
	}

	public void Execute_DeleteGroup()
	{
		Logging.WriteLogLine( "Deleting " + SelectedGroup );

		Task.Run( async () =>
				  {
					  var GroupName = SelectedGroup.Substring( 0, SelectedGroup.IndexOf( "\t", StringComparison.Ordinal ) ).TrimEnd();

					  foreach( var Cag in Groups )
					  {
						  if( Cag.Description == GroupName )
						  {
							  await Azure.Client.RequestDeleteCompanyAddressGroup( Cag.GroupNumber );

							  await Dispatcher.Invoke( async () =>
													   {
														   await LoadGroups();
													   } );

							  break;
						  }
					  }
				  } );
	}

	//
	// Modify
	//

	public void Execute_CancelModifyGroup()
	{
		//Logging.WriteLogLine( "Modify Group cancelled: " + SelectedGroup );

		//SelectedGroup = string.Empty;
		SelectedGroupTab = 0;
	}


	public List<string> MemberOfGroups

	{
		get { return Get( () => MemberOfGroups, new List<string>() ); }
		set { Set( () => MemberOfGroups, value ); }
	}


	public List<string> AvailableGroups

	{
		get { return Get( () => AvailableGroups, new List<string>() ); } // LoadAddressesGroupNames()
		set { Set( () => AvailableGroups, value ); }
	}

	private void LoadAddressesGroupNames()
	{
		var MogList = new List<string>();

		MemberOfGroups.Clear();
		AvailableGroups.Clear();

		var SdMog = new SortedDictionary<string, string>();
		var SdAg  = new SortedDictionary<string, string>();

		foreach( var Key in GroupsAndMembers.Keys )
		{
			var Found = false;

			foreach( var Ad in GroupsAndMembers[ Key ] )
			{
				// Case insensitive
				//Logging.WriteLogLine("Comparing: " + ad.Trim().ToLower() + " to " + AddressDescription.Trim().ToLower());
				if( string.Equals( Ad.Trim(), AddressDescription.Trim(), StringComparison.CurrentCultureIgnoreCase ) )
				{
					Found = true;

					break;
				}
			}

			if( Found )
				SdMog.Add( Key.ToLower(), Key );
			else
				SdAg.Add( Key.ToLower(), Key );

			//if( GroupsAndMembers[ key ].Contains( AddressDescription.Trim() ) )
			//	sdMog.Add( key.ToLower(), key );
			//else
			//	sdAg.Add( key.ToLower(), key );
		}

		if( SdMog.Count > 0 )
		{
			foreach( var Mog in SdMog.Keys )
				MogList.Add( SdMog[ Mog ] );
		}

		MemberOfGroups = MogList;

		var AgList = new List<string>();

		if( SdAg.Count > 0 )
		{
			foreach( var Ag in SdAg.Keys )
				AgList.Add( SdAg[ Ag ] );
		}

		AvailableGroups = AgList;

		//foreach (string name in GroupNames)
		//{
		//    // "test group 0 \t(0 members)"
		//    string item = name.Substring(0, name.IndexOf("\t")).TrimEnd();

		//    if (MemberOfGroups.Count > 0)
		//    {
		//        if (!MemberOfGroups.Contains(item))
		//        {
		//            list.Add(item);
		//        }
		//    }
		//    else
		//    {
		//        list.Add(item);
		//    }
		//}

		//return list;
	}


	public string MemberOfGroupsSelected

	{
		get { return Get( () => MemberOfGroupsSelected, "" ); }
		set { Set( () => MemberOfGroupsSelected, value ); }
	}


	public string AvailableGroupsSelected

	{
		get { return Get( () => AvailableGroupsSelected, "" ); }
		set { Set( () => AvailableGroupsSelected, value ); }
	}

	public void Execute_RemoveFromMemberOfGroups()
	{
		if( MemberOfGroupsSelected.IsNotNullOrWhiteSpace() )
		{
			var Sd = new SortedDictionary<string, string>
					 {
						 {MemberOfGroupsSelected.ToLower(), MemberOfGroupsSelected}
					 };

			foreach( var Group in AvailableGroups )
				Sd.Add( Group.ToLower(), Group );

			var List = new List<string>();

			foreach( var Key in Sd.Keys )
				List.Add( Sd[ Key ] );

			var Mogs = new List<string>();

			foreach( var Key in MemberOfGroups )
			{
				if( Key != MemberOfGroupsSelected )
					Mogs.Add( Key );
			}

			Dispatcher.Invoke( () =>
							   {
								   AvailableGroups = List;

								   //MemberOfGroups.Remove(MemberOfGroupsSelected);
								   MemberOfGroups = Mogs;
							   } );
		}
	}

	public void Execute_AddToMemberOfGroups()
	{
		if( AvailableGroupsSelected.IsNotNullOrWhiteSpace() )
		{
			var Sd = new SortedDictionary<string, string>
					 {
						 {AvailableGroupsSelected.ToLower(), AvailableGroupsSelected}
					 };

			foreach( var Group in MemberOfGroups )
				Sd.Add( Group.ToLower(), Group );

			var List = new List<string>();

			foreach( var Key in Sd.Keys )
				List.Add( Sd[ Key ] );

			var Ags = new List<string>();

			foreach( var Key in AvailableGroups )
			{
				if( Key != AvailableGroupsSelected )
					Ags.Add( Key );
			}

			Dispatcher.Invoke( () =>
							   {
								   MemberOfGroups = List;

								   // AvailableGroups.Remove(AvailableGroupsSelected);
								   AvailableGroups = Ags;
							   } );
		}
	}
#endregion

#region Data
	public bool IsDescriptionReadOnly

	{
		get { return Get( () => IsDescriptionReadOnly, false ); }
		set { Set( () => IsDescriptionReadOnly, value ); }
	}

	[DependsUpon( nameof( IsDescriptionReadOnly ) )]
	public bool IsDescriptionEnabled =>
		!IsDescriptionReadOnly;

	//get { return Get(() => !IsDescriptionReadOnly, true); }
	//set { Set(() => IsDescriptionReadOnly, value); }
	public IList Countries

	{
		get => CountriesRegions.Countries;
		set { Set( () => Countries, value ); }
	}


	//public IList? Regions
	public List<string> Regions

	{
		//get { return Get( () => Regions, new List<object>() ); }
		get { return Get( () => Regions, new List<string>() ); }
		set { Set( () => Regions, value ); }
	}

	public List<string> AccountNames

	{
		get { return Get( () => AccountNames, new List<string>() ); }

		//get { return Get(() => AccountNames); }
		set { Set( () => AccountNames, value ); }
	}


	public ObservableCollection<ExtendedCompanyAddress> ExtendedAddresses

	{
		get { return Get( () => ExtendedAddresses, new ObservableCollection<ExtendedCompanyAddress>() ); }
		set { Set( () => ExtendedAddresses, value ); }
	}


	public string SelectedAccountName

	{
		get { return Get( () => SelectedAccountName, "" ); }
		set { Set( () => SelectedAccountName, value ); }
	}


	public string SelectedAccountId

	{
		get { return Get( () => SelectedAccountId, "" ); }

		//get
		//{
		//    // Load the Account

		//    return SelectedAccountId;
		//}
		set { Set( () => SelectedAccountId, value ); }
	}


	public string SelectedAccountOutstandingFormatted

	{
		get { return Get( () => SelectedAccountOutstandingFormatted, GetOutstandingForSelectedAccountFormatted ); }
		set { Set( () => SelectedAccountOutstandingFormatted, value ); }
	}


	public ObservableCollection<CompanyAddress> Addresses

	{
		get { return Get( () => Addresses, new ObservableCollection<CompanyAddress>() ); }
		set { Set( () => Addresses, value ); }
	}


	public bool IsGlobalAddressBook

	{
		get { return Get( () => IsGlobalAddressBook, false ); }
		set { Set( () => IsGlobalAddressBook, value ); }
	}


	public bool IsAccountAddressBook

	{
		get { return Get( () => IsAccountAddressBook, true ); }
		set { Set( () => IsAccountAddressBook, value ); }
	}


	public string AddressFilter

	{
		get { return Get( () => AddressFilter, "" ); }
		set { Set( () => AddressFilter, value ); }
	}


	public ExtendedCompanyAddress? SelectedAddress

	{
		get { return Get( () => SelectedAddress, new ExtendedCompanyAddress( new CompanyAddress(), "" ) ); }
		set { Set( () => SelectedAddress, value ); }
	}


	// Address Details section


	public string AddressDescription

	{
		get { return Get( () => AddressDescription, "" ); }
		set { Set( () => AddressDescription, value ); }
	}

	//[DependsUpon(nameof(AddressDescription))]
	//private void WhenAddressDescriptionChanges()
	//{
	//    // Check that the description is unique
	//}


	public string AddressLocationBarcode

	{
		get { return Get( () => AddressLocationBarcode, "" ); }
		set { Set( () => AddressLocationBarcode, value ); }
	}


	public string AddressSuite

	{
		get { return Get( () => AddressSuite, "" ); }
		set { Set( () => AddressSuite, value ); }
	}


	public string AddressStreet

	{
		get { return Get( () => AddressStreet, "" ); }
		set { Set( () => AddressStreet, value ); }
	}


	public string AddressCity

	{
		get { return Get( () => AddressCity, "" ); }
		set { Set( () => AddressCity, value ); }
	}


	public string AddressZipPostal

	{
		get { return Get( () => AddressZipPostal, "" ); }
		set { Set( () => AddressZipPostal, value ); }
	}


	public string AddressProvState

	{
		get { return Get( () => AddressProvState, "" ); }
		set { Set( () => AddressProvState, value ); }
	}


	public string AddressCountry

	{
		get { return Get( () => AddressCountry, "" ); }
		set { Set( () => AddressCountry, value ); }
	}

	[DependsUpon( nameof( AddressCountry ) )]
	public void WhenAddressCountryChanges()
	{
#pragma warning disable CS8601 // Possible null reference assignment.
		Regions = AddressCountry switch
				  {
					  "Australia"     => CountriesRegions.AustraliaRegions,
					  "Canada"        => CountriesRegions.CanadaRegions,
					  "United States" => CountriesRegions.USRegions,
					  _               => new List<string>()
				  };
#pragma warning restore CS8601 // Possible null reference assignment.
	}


	public string AddressLatitude

	{
		get { return Get( () => AddressLatitude, "0" ); }
		set { Set( () => AddressLatitude, value ); }
	}


	public string AddressLongitude

	{
		get { return Get( () => AddressLongitude, "0" ); }
		set { Set( () => AddressLongitude, value ); }
	}


	public string AddressContactName

	{
		get { return Get( () => AddressContactName, "" ); }
		set { Set( () => AddressContactName, value ); }
	}


	public string AddressContactPhone

	{
		get { return Get( () => AddressContactPhone, "" ); }
		set { Set( () => AddressContactPhone, value ); }
	}


	public string AddressContactEmail

	{
		get { return Get( () => AddressContactEmail, "" ); }
		set { Set( () => AddressContactEmail, value ); }
	}


	public string AddressNotes

	{
		get { return Get( () => AddressNotes, "" ); }
		set { Set( () => AddressNotes, value ); }
	}


	// Export Addresses section


	public string ExportDescription

	{
		get { return Get( () => ExportDescription, "" ); }
		set { Set( () => ExportDescription, value ); }
	}


	public bool ExportIsExclude

	{
		get { return Get( () => ExportIsExclude, false ); }
		set { Set( () => ExportIsExclude, value ); }
	}

	//
	// Data Methods
	//

#region Account Name
	private Dictionary<string, string> DisplayNameToCodeDictionary = new();

	[DependsUpon( nameof( SelectedAccountName ) )]
	public async Task WhenSelectedAccountNameChanges()
	{
		Execute_CancelModifyGroup();

		var Sel = SelectedAccountName;

		if( Sel.IsNotNullOrWhiteSpace() && DisplayNameToCodeDictionary.TryGetValue( Sel, out var Lookup ) )
		{
			SelectedAccountId = Lookup;
			GroupNames        = new AddressIdList();

			await LoadAddresses();
			await LoadGroups();
		}
	}
#endregion

	private static string GetOutstandingForSelectedAccountFormatted()
	{
		const decimal RAW = 0.0M; // TODO 

		var Outstanding = $"{RAW:c}";

		return Outstanding;
	}

	private async Task GetAccountNames()
	{
		try
		{
			if( Addresses.Count == 0 )
			{
				if( !IsInDesignMode )
				{
					AccountNames.Clear();
					ExtendedAddresses.Clear();

					var Ccl = await Azure.Client.RequestGetCustomerCodeCompanyNameList();

					var ResellerName = ToDisplayName( GlobalAddressBookId, GlobalAddressBookId );

					Logging.WriteLogLine( "Found " + Ccl.Count + " Customers" );

					DisplayNameToCodeDictionary = ( from C in Ccl
													select C ).ToDictionary( c => ToDisplayName( c.CustomerCode, c.CompanyName ), c => c.CustomerCode );

					AccountNames = ( from C in Ccl
									 where C.CustomerCode != GlobalAddressBookId
									 orderby C.CompanyName.ToLower()
									 select ToDisplayName( C.CustomerCode, C.CompanyName ) ).ToList();

					AccountNames.Remove( ResellerName );
				}
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown fetching customers:\n" + E );
		}
	}

	public CompanyDetailList? CurrentDetailList;

	private async Task LoadAddresses( bool loadFromServer = true )
	{
		ExtendedAddresses.Clear();
		CurrentCompanyAddresses.Clear();

		//CurrentDetailList = null;

		try
		{
			Loaded = false;

			if( !IsInDesignMode )
			{
				Logging.WriteLogLine( "Loading addresses for " + SelectedAccountName + " (" + SelectedAccountId + ")..." );

				//CompanyDetailList? Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( SelectedAccountId );
				//CurrentDetailList = Details;
				CompanyDetailList? Details;

				if( loadFromServer )
				{
					Logging.WriteLogLine( "Loading addresses from server" );
					Details           = await Azure.Client.RequestGetCustomerCompaniesDetailed( SelectedAccountId );
					CurrentDetailList = Details;
				}
				else
				{
					Logging.WriteLogLine( "Not loading addresses from server" );
					Details = CurrentDetailList;
				}

				var SdExtended = ( from D in Details
								   let CompanyName = D.Company.CompanyName.Trim().ToLower()
								   where CompanyName.IsNotNullOrWhiteSpace() && !CompanyName.Contains( GlobalAddressBookId )
								   orderby CompanyName
								   group new
										 {
											 CompanyName,
											 Address = new ExtendedCompanyAddress( D.Address, D.Company.UserName )
										 } by CompanyName
								   into CoNames // Remove Duplicates
								   select CoNames.First() ).ToDictionary( d => d.CompanyName, d => d.Address );

				Dispatcher.Invoke( () =>
								   {
									   ExtendedAddresses.Clear();
									   CurrentCompanyAddresses.Clear();

									   foreach( var E in SdExtended )
									   {
										   var Value = E.Value;

										   if( !Value.Ca.CompanyName.Contains( GlobalAddressBookId ) )
										   {
											   //Logging.WriteLogLine( "eca: " + Value.Ca.CompanyName + ", LocationBarcode: " + Value.Ca.LocationBarcode );
											   CurrentCompanyAddresses.Add( Value );
											   ExtendedAddresses.Add( Value );
										   }
									   }

									   if( ExtendedAddresses.Count > 0 )
									   {
										   foreach( var Eca in ExtendedAddresses )
										   {
											   var Country = Eca.Ca.Country;
											   Logging.WriteLogLine( "DEBUG Loading regions for Country: " + Country );

											   if( Country.IsNotNullOrWhiteSpace() )
											   {
												   AddressCountry = Country;
												   Regions        = CountriesRegions.GetRegionsForCountry( Country );

												   if( Regions != null )
												   {
													   FilterRegions = CountriesRegions.GetRegionsForCountry( Country );

													   if( ( FilterRegions.Count > 0 ) && FilterRegions[ 0 ].IsNotNullOrWhiteSpace() )
														   FilterRegions.Insert( 0, "" );

													   break;
												   }
											   }
										   }

										   GetCities();
									   }

									   switch( CurrentFilter )
									   {
									   case WHICH_FILTER.CITY:
										   WhenFilterSelectedCityChanges();

										   break;

									   case WHICH_FILTER.COMPANY_NAME:
										   WhenFilterCompanyNameChanges( FilterCompanyName );

										   break;

									   case WHICH_FILTER.LOCATION_BARCODE:
										   WhenFilterLocationBarcodeChanges( FilterLocationBarcode );

										   break;

									   case WHICH_FILTER.REGION:
										   WhenFilterSelectedRegionChanges();

										   break;
									   }

									   Loaded = true;
								   } );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown fetching addresses:\n" + E );
		}
	}


	// [DependsUpon(nameof(AddressFilter))]
	public void WhenAddressFilterChanges( string filter = "" )
	{
		if( !string.IsNullOrEmpty( filter ) )
			AddressFilter = filter;

		Logging.WriteLogLine( "Filter changed: " + AddressFilter );

		if( !string.IsNullOrEmpty( AddressFilter ) )
		{
			var Ecas = new List<ExtendedCompanyAddress>();

			//foreach (ExtendedCompanyAddress eca in ExtendedAddresses)
			foreach( var Eca in CurrentCompanyAddresses )
			{
				if( Eca.Ca.CompanyName.ToLower().Contains( AddressFilter ) || Eca.AddressString.ToLower().Contains( AddressFilter ) )
					Ecas.Add( Eca );
			}

			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( Ecas );
							   } );
		}
		else
		{
			// Reload unfiltered list
			//LoadAddresses();
			Dispatcher.Invoke( () =>
							   {
								   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( CurrentCompanyAddresses );
							   } );
		}
	}


	public bool IsSelectAccountFromListSelected

	{
		get { return Get( () => IsSelectAccountFromListSelected, true ); }
		set { Set( () => IsSelectAccountFromListSelected, value ); }
	}


	public bool IsGlobalSelected

	{
		get { return Get( () => IsGlobalSelected, false ); }
		set { Set( () => IsGlobalSelected, value ); }
	}


	public string SelectedImportFile

	{
		get { return Get( () => SelectedImportFile, "" ); }
		set { Set( () => SelectedImportFile, value ); }
	}


	public bool IsSelectGroupFromListSelected

	{
		get { return Get( () => IsSelectGroupFromListSelected, true ); }
		set { Set( () => IsSelectGroupFromListSelected, value ); }
	}


	public bool IsNewGroupSelected

	{
		get { return Get( () => IsNewGroupSelected, false ); }
		set { Set( () => IsNewGroupSelected, value ); }
	}


	public string SelectedGroupName

	{
		get { return Get( () => SelectedGroupName, "" ); }
		set { Set( () => SelectedGroupName, value ); }
	}


	public string NewImportGroupName

	{
		get { return Get( () => NewImportGroupName, "" ); }
		set { Set( () => NewImportGroupName, value ); }
	}

	[DependsUpon( nameof( SelectedImportFile ) )]
	[DependsUpon( nameof( IsSelectGroupFromListSelected ) )]
	[DependsUpon( nameof( SelectedGroupName ) )]
	[DependsUpon( nameof( NewImportGroupName ) )]
	[DependsUpon( nameof( IsNewGroupSelected ) )]
	public bool IsImportButtonEnabled

	{
		get { return Get( () => IsImportButtonEnabled, false ); }
		set { Set( () => IsImportButtonEnabled, value ); }
	}

	public bool IsGlobalAddressBookSelected
	{
		get => Get( () => IsGlobalAddressBookSelected, false );
		set { Set( () => IsGlobalAddressBookSelected, value ); }
	}

	private static string ToDisplayName( string code, string name ) => $"{name}   ({code})";

	public string LastAccountNameSelected { get; set; } = string.Empty;

	[DependsUpon( nameof( IsGlobalAddressBookSelected ) )]
	public void WhenIsGlobalAddressBookSelectedChanges()
	{
		if( IsGlobalAddressBookSelected )
		{
			LastAccountNameSelected = SelectedAccountName;
			Logging.WriteLogLine( "Selecting Global address book - Setting SelectedAccountId to " + GlobalAddressBookId );
			SelectedAccountId   = GlobalAddressBookId;
			SelectedAccountName = ToDisplayName( GlobalAddressBookId, GlobalAddressBookId );
		}
		else
		{
			SelectedAccountName = LastAccountNameSelected;
			Logging.WriteLogLine( "Selecting Account address book for " + SelectedAccountName );
		}

		Task.Run( async () =>
				  {
					  await WhenSelectedAccountNameChanges();
				  } );
	}
#endregion

#region Actions
	public async Task<List<string>> AddUpdateAddress( Dictionary<string, Control> controls )
	{
		Logging.WriteLogLine( "Add Update Address clicked" );

		var Errors = ValidateAddress( controls );

		if( Errors.Count == 0 )
		{
			Logging.WriteLogLine( "Address is valid - saving" );

			// If the AddressDescription is in the list of ExtendedAddresses, 
			// we are updating an existing Address
			var Address = ( from Eca in ExtendedAddresses
							where Eca.Ca.CompanyName.Trim() == AddressDescription.Trim()
							select Eca ).FirstOrDefault();

			if( !IsInDesignMode )
			{
				try
				{
					string Title,
						   Message;
					var Company = BuildCompany();

					if( Address is null )
					{
						Logging.WriteLogLine( "Adding new address: " + AddressDescription );

						var Acc = new AddCustomerCompany( "AddressBookModel" )
								  {
									  Company      = Company,
									  CustomerCode = SelectedAccountId
								  };
						await Azure.Client.RequestAddCustomerCompany( Acc );

						// Now update the address's groups
						foreach( var MogName in MemberOfGroups )
						{
							var Cag = GetGroupForName( MogName );

							// New address - just add the groups
							Logging.WriteLogLine( "Adding " + Company.CompanyName + " to group " + MogName );

							if( Cag is not null )
							{
								var Cage = new CompanyAddressGroupEntry
										   {
											   CompanyName = Company.CompanyName,
											   GroupNumber = Cag.GroupNumber
										   };
								await Azure.Client.RequestAddCompanyAddressGroupEntry( Cage );
							}
						}

						Title   = (string)Application.Current.TryFindResource( "AddressBookAddTitle" );
						Message = (string)Application.Current.TryFindResource( "AddressBookAdd" );
					}
					else
					{
						Logging.WriteLogLine( "Updating address: " + AddressDescription );

						var Ucc = new UpdateCustomerCompany( "AddressBookModel" )
								  {
									  Company      = Company,
									  CustomerCode = SelectedAccountId
								  };
						await Azure.Client.RequestUpdateCustomerCompany( Ucc );

						// Now update the address's groups
						foreach( var Group in GroupsAndMembers.Keys )
						{
							if( GroupsAndMembers[ Group ].Contains( Company.CompanyName ) )
							{
								if( !MemberOfGroups.Contains( Group ) )
								{
									Logging.WriteLogLine( "Removing " + Company.CompanyName + " from group " + Group );
									var Cag = GetGroupForName( Group );

									if( Cag is not null )
									{
										var Cage = new CompanyAddressGroupEntry
												   {
													   CompanyName = Company.CompanyName,
													   GroupNumber = Cag.GroupNumber
												   };
										await Azure.Client.RequestDeleteCompanyAddressGroupEntry( Cage );
									}
								}
							}
							else if( MemberOfGroups.Contains( Group ) )
							{
								Logging.WriteLogLine( "Adding " + Company.CompanyName + " to group " + Group );
								var Cag = GetGroupForName( Group );

								if( Cag is not null )
								{
									var Cage = new CompanyAddressGroupEntry
											   {
												   CompanyName = Company.CompanyName,
												   GroupNumber = Cag.GroupNumber
											   };
									await Azure.Client.RequestAddCompanyAddressGroupEntry( Cage );
								}
							}
						}

						Title   = (string)Application.Current.TryFindResource( "AddressBookUpdateTitle" );
						Message = (string)Application.Current.TryFindResource( "AddressBookUpdate" );
					}

					// Add to CurrentDetailList or update
					AddOrUpdateLocalAddress( Company );
					await LoadGroups();

					await Dispatcher.Invoke( async () =>
											 {
												 Message = Message.Replace( "@1", Company.CompanyName );
												 MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );

												 Execute_ClearAddress();

												 await LoadAddresses( false ); // Don't load from the server

												 UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses();
											 } );
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown when adding a company: " + E );
				}
			}
		}

		return Errors;
	}

	private void AddOrUpdateLocalAddress( Company company )
	{
		Logging.WriteLogLine( "Adding/Updating company locally " + company.CompanyName );

		var Current = ( from C in CurrentDetailList
						where C.Company.CompanyName == company.CompanyName
						select C ).FirstOrDefault();

		if( Current is not null )
			CurrentDetailList?.Remove( Current );

		CompanyDetail Cd = new()
						   {
							   Company = company,
							   Address = BuildCompanyAddress( company )
						   };
		CurrentDetailList?.Add( Cd );
	}

	private static CompanyAddress BuildCompanyAddress( Company company )
	{
		CompanyAddress Ca = new()
							{
								AddressLine1    = company.AddressLine1,
								AddressLine2    = company.AddressLine2,
								Barcode         = company.Barcode,
								City            = company.City,
								CompanyName     = company.CompanyName,
								CompanyNumber   = company.CompanyNumber,
								ContactName     = company.ContactName,
								Country         = company.Country,
								CountryCode     = company.CountryCode,
								EmailAddress    = company.EmailAddress,
								EmailAddress1   = company.EmailAddress1,
								EmailAddress2   = company.EmailAddress2,
								Enabled         = company.Enabled,
								Fax             = company.Fax,
								Latitude        = company.Latitude,
								LocationBarcode = company.LocationBarcode,
								Longitude       = company.Longitude,
								Mobile          = company.Mobile,
								Mobile1         = company.Mobile1,
								Notes           = company.Notes,
								Phone           = company.Phone,
								Phone1          = company.Phone1,
								PostalBarcode   = company.PostalBarcode,
								PostalCode      = company.PostalCode,
								Region          = company.Region,
								SecondaryId     = company.SecondaryId,
								Suite           = company.Suite,
								Vicinity        = company.Vicinity,
								ZoneName        = company.ZoneName
							};

		return Ca;
	}

	public void Execute_DeleteAddress( List<ExtendedCompanyAddress> ecas )
	{
		//if( SelectedAddress != null )
		if( ecas.Count > 0 )
		{
			Logging.WriteLogLine( "Deleting Address: " + SelectedAddress?.Ca.CompanyName );

			Task.Run( async () =>
					  {
						  try
						  {
							  if( !IsInDesignMode )
							  {
								  var Names = string.Empty;

								  foreach( var Eca in ecas )
								  {
									  //ResellerCustomerCompanyLookup tmp = await Azure.Client.RequestGetCustomerCompanyAddress( SelectedAccountId, SelectedAddress.Ca.CompanyName );
									  //							          var Tmp = await Azure.Client.RequestGetCustomerCompanyAddress( SelectedAccountId, Eca.Ca.CompanyName );

									  //Company company = BuildCompany( SelectedAddress );
									  var Company = BuildCompany( Eca );

									  var Current = ( from C in CurrentDetailList
													  where C.Company.CompanyName == Company.CompanyName
													  select C ).FirstOrDefault();

									  if( Current is not null )
									  {
										  Logging.WriteLogLine( "Deleting local copy" );
										  CurrentDetailList?.Remove( Current );
									  }

									  var Dcc = new DeleteCustomerCompany( "AddressBookModel" )
												{
													Company      = Company,
													CustomerCode = SelectedAccountId
												};

									  await Azure.Client.RequestDeleteCustomerCompany( Dcc );

									  //await Azure.Client.RequestDeleteResellerCustomer(SelectedAccountId, "AddressBookModel");

									  foreach( var Group in GroupsAndMembers.Keys )
									  {
										  if( GroupsAndMembers[ Group ].Contains( Company.CompanyName ) )
										  {
											  Logging.WriteLogLine( "Removing " + Company.CompanyName + " from group " + Group );
											  var Cag = GetGroupForName( Group );

											  if( Cag is not null )
											  {
												  var Cage = new CompanyAddressGroupEntry
															 {
																 CompanyName = Company.CompanyName,
																 GroupNumber = Cag.GroupNumber
															 };
												  await Azure.Client.RequestDeleteCompanyAddressGroupEntry( Cage );
											  }
										  }
									  }

									  Names += Eca.Ca.CompanyName + ", ";
								  }

								  await Dispatcher.Invoke( async () =>
														   {
															   var Title   = (string)Application.Current.TryFindResource( "AddressBookDeleteTitle" );
															   var Message = (string)Application.Current.TryFindResource( "AddressBookDelete" );

															   Logging.WriteLogLine( "Deleted " + Names );
															   Names = "\n" + Names.Substring( 0, Names.Length - 2 ).Trim() + "\n";

															   //message = message.Replace( "@1", company.CompanyName );
															   Message = Message.Replace( "@1", Names );

															   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Information );

															   Execute_ClearAddress();
															   await LoadAddresses( false ); // Don't load from server
															   await LoadGroups();

															   UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses();
														   } );
							  }
						  }
						  catch( Exception E )
						  {
							  Logging.WriteLogLine( "Exception thrown when deleting a company: " + E );
						  }
					  } );
		}
	}

	private Company BuildCompany( ExtendedCompanyAddress? eca = null )
	{
		Company Company;

		decimal Latitude  = 0;
		decimal Longitude = 0;

		if( AddressLatitude.IsNotNullOrWhiteSpace() )
			Latitude = decimal.Parse( AddressLatitude.Trim() );

		if( AddressLongitude.IsNotNullOrWhiteSpace() )
			Longitude = decimal.Parse( AddressLongitude.Trim() );

		if( eca is null )
		{
			Company = new Company
					  {
						  LocationBarcode = AddressLocationBarcode,
						  Suite           = AddressSuite,
						  AddressLine1    = AddressStreet,
						  City            = AddressCity,
						  Country         = AddressCountry,
						  CompanyName     = AddressDescription,
						  PostalCode      = AddressZipPostal,
						  Region          = AddressProvState,
						  UserName        = AddressContactName,
						  ContactName     = AddressContactName,
						  Phone           = AddressContactPhone,
						  EmailAddress    = AddressContactEmail,
						  Notes           = AddressNotes,

						  Latitude  = Latitude,
						  Longitude = Longitude
					  };
		}
		else
		{
			// Used by Execute_Delete
			Company = new Company
					  {
						  LocationBarcode = eca.Ca.LocationBarcode,
						  Suite           = eca.Ca.Suite,
						  AddressLine1    = eca.Ca.AddressLine1,
						  City            = eca.Ca.City,
						  Country         = eca.Ca.Country,
						  CompanyName     = eca.Ca.CompanyName,
						  PostalCode      = eca.Ca.PostalCode,
						  Region          = eca.Ca.Region,

						  //UserName = eca.,
						  Phone        = eca.Ca.Phone,
						  EmailAddress = eca.Ca.EmailAddress,
						  Notes        = eca.Ca.Notes,

						  Latitude  = eca.Ca.Latitude,
						  Longitude = eca.Ca.Longitude
					  };
		}

		//if (rccl == null)
		//{
		//    company = new Company
		//    {
		//        Suite = AddressSuite,
		//        AddressLine1 = AddressStreet,
		//        City = AddressCity,
		//        Country = AddressCountry,
		//        EmailAddress = AddressContactEmail,
		//        Phone = AddressContactPhone,
		//        CompanyName = AddressDescription,
		//        PostalCode = AddressZipPostal,
		//        Region = AddressProvState,
		//        Notes = AddressNotes
		//    };
		//}
		//else
		//{
		//    company = new Company
		//    {
		//        Suite = AddressSuite,
		//        AddressLine1 = rccl.AddressLine1,
		//        City = rccl.City,
		//        Country = rccl.Country,
		//        EmailAddress = rccl.EmailAddress,
		//        Phone = rccl.Phone,
		//        CompanyName = rccl.CompanyName,
		//        PostalCode = rccl.PostalCode,
		//        Region = rccl.Region,
		//        Notes = AddressNotes
		//    };
		//}

		return Company;
	}

	private List<string> ValidateAddress( Dictionary<string, Control> controls )
	{
		var Errors = new List<string>();

		if( controls.Count > 0 )
		{
			foreach( var FieldName in controls.Keys )
			{
				var Error = string.Empty;

				switch( FieldName )
				{
				case "AddressDescription":
					Error = SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "AddressBookValidationErrorsDescription" ) );

					if( !IsControlEmpty( controls[ FieldName ] ) && ( SelectedAddress == null ) )
					{
						// Check to see if the description is unique
						var Description = ( (TextBox)controls[ FieldName ] ).Text.Trim();

						foreach( var Eca in CurrentCompanyAddresses )
						{
							if( string.Equals( Eca.Ca.CompanyName, Description, StringComparison.CurrentCultureIgnoreCase ) )
							{
								Error                            = (string)Application.Current.TryFindResource( "AddressBookValidationErrorsDescriptionDuplicate" );
								Error                            = Error.Replace( "@1", Description );
								Error                            = Error.Replace( "@2", Eca.Ca.CompanyName );
								controls[ FieldName ].Background = Brushes.Yellow;
							}
						}
					}

					break;

				case "AddressStreet":
					Error = SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "AddressBookValidationErrorsStreet" ) );

					break;

				case "AddressCity":
					Error = SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "AddressBookValidationErrorsCity" ) );

					break;

				case "AddressLocationBarcode":
					if( !IsControlEmpty( controls[ FieldName ] ) && ( SelectedAddress == null ) )
					{
						// Check to see if the barcode is unique
						var Barcode = ( (TextBox)controls[ FieldName ] ).Text.Trim();

						//foreach (ExtendedCompanyAddress eca in ExtendedAddresses)
						foreach( var Eca in CurrentCompanyAddresses ) // This is the unfiltered list
						{
							// TODO the eca.Ca.LocationBarcode is always empty
							if( string.Equals( Eca.Ca.LocationBarcode, Barcode, StringComparison.CurrentCultureIgnoreCase ) )
							{
								Error                            = (string)Application.Current.TryFindResource( "AddressBookValidationErrorsLocationBarcode" );
								Error                            = Error.Replace( "@1", Barcode );
								Error                            = Error.Replace( "@2", Eca.Ca.CompanyName );
								controls[ FieldName ].Background = Brushes.Yellow;
							}
						}
					}

					break;
				}

				if( !string.IsNullOrEmpty( Error ) )
					Errors.Add( Error );
			}
		}

		return Errors;
	}

	private static string SetErrorState( Control ctrl, string message )
	{
		var Error = string.Empty;

		if( IsControlEmpty( ctrl ) )
		{
			ctrl.Background = Brushes.Yellow;
			Error           = message;
		}
		else
			ctrl.Background = Brushes.White;

		return Error;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="ctrl">
	/// </param>
	/// <returns></returns>
	private static bool IsControlEmpty( Control ctrl )
	{
		return ctrl switch
			   {
				   TextBox Tb         => IsTextBoxEmpty( Tb ),
				   ComboBox Cb        => IsComboBoxSelected( Cb ),
				   DateTimePicker Dtp => IsDateTimePickerEmpty( Dtp ),
				   TimePicker Tp      => IsTimePickerEmpty( Tp ),
				   _                  => true
			   };
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="tb">
	/// </param>
	/// <returns></returns>
	private static bool IsTextBoxEmpty( TextBox tb ) => string.IsNullOrEmpty( tb.Text );

	/// <summary>
	/// </summary>
	/// <param
	///     name="cb">
	/// </param>
	/// <returns></returns>
	private static bool IsComboBoxSelected( ComboBox cb ) => !( cb.SelectedIndex > -1 );

	/// <summary>
	/// </summary>
	/// <param
	///     name="dtp">
	/// </param>
	/// <returns></returns>
	private static bool IsDateTimePickerEmpty( DateTimePicker dtp ) => string.IsNullOrEmpty( dtp.Text );

	private static bool IsTimePickerEmpty( TimePicker tp ) => string.IsNullOrEmpty( tp.Text );

	public void Execute_ClearAddress()
	{
		Logging.WriteLogLine( "Clear Address clicked" );

		Dispatcher.Invoke( () =>
						   {
							   IsDescriptionReadOnly = false;

							   SelectedAddress = null;

							   AddressDescription     = string.Empty;
							   AddressLocationBarcode = string.Empty;
							   AddressSuite           = string.Empty;
							   AddressStreet          = string.Empty;
							   AddressCity            = string.Empty;
							   AddressZipPostal       = string.Empty;

							   AddressProvState = string.Empty;
							   AddressLatitude  = string.Empty;
							   AddressLongitude = string.Empty;

							   AddressContactName  = string.Empty;
							   AddressContactPhone = string.Empty;
							   AddressContactEmail = string.Empty;
							   AddressNotes        = string.Empty;

							   LoadAddressesGroupNames();

							   //AvailableGroups = AvailableGroups; // LoadAddressesGroupNames();

							   Execute_ClearImport();
						   } );
	}

	public void Execute_ClearImport()
	{
		SelectedImportFile = "";

		if( GroupNames.Count > 0 )
			SelectedGroupName = GroupNames[ 0 ];
	}

	public void Execute_LoadAddress( ExtendedCompanyAddress? eca = null )
	{
		if( eca != null )
			SelectedAddress = eca;

		if( SelectedAddress is not null )
		{
			Logging.WriteLogLine( "Load Address clicked - SelectedAddress: " + SelectedAddress.Ca.CompanyName );

			Task.Run( async () =>
					  {
						  try
						  {
							  IsDescriptionReadOnly = true;

							  var Ca = SelectedAddress.Ca;

							  //await Azure.Client.requestget

							  //var tmp = await Azure.Client.RequestGetCustomerCompanyAddress("", ca.CompanyName);
							  var Ro = new CompanyByAccountAndCompanyNameList();

							  var Company = new CompanyByAccountAndCompanyName
											{
												CompanyName  = Ca.CompanyName,
												CustomerCode = SelectedAccountId // ca.CompanyNumber
											};
							  Ro.Add( Company );

							  var List = await Azure.Client.RequestGetCustomerCompanyAddressList( Ro );

							  Dispatcher.Invoke( () =>
												 {
													 // Do this first, so that the Regions are loaded
													 //AddressCountry = ca.Country; // TODO dirty data - case is wrong 'United states'
													 AddressCountry = FindCountryMatch( Ca.Country );

													 AddressDescription     = Ca.CompanyName;
													 AddressLocationBarcode = Ca.LocationBarcode;
													 AddressSuite           = Ca.Suite;
													 AddressStreet          = Ca.AddressLine1;
													 AddressCity            = Ca.City;
													 AddressZipPostal       = Ca.PostalCode;
													 AddressNotes           = Ca.Notes;

													 //AddressProvState = ca.Region; // TODO dirty data - case is wrong 'MICHEGAN'
													 bool Found;
													 ( AddressProvState, Found ) = CountriesRegions.FindRegionForCountry( Ca.Region, AddressCountry );

													 if( !Found )
													 {
														 // Try treating it as an abbreviation
														 AddressProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( Ca.Region, AddressCountry );
													 }

													 if( List is {Count: > 0} )
													 {
														 foreach( var Co in List )
														 {
															 if( Co.Companies.Count > 0 )
															 {
																 if( Co.Companies[ 0 ].CompanyName == Ca.CompanyName )
																 {
																	 AddressContactName     = Co.Companies[ 0 ].UserName;
																	 AddressLocationBarcode = Co.Companies[ 0 ].LocationBarcode;
																 }
															 }
														 }
													 }

													 AddressLatitude  = Ca.Latitude.ToString( CultureInfo.InvariantCulture );
													 AddressLongitude = Ca.Longitude.ToString( CultureInfo.InvariantCulture );

													 if( eca is not null && eca.UserName.IsNotNullOrWhiteSpace() )
														 AddressContactName = eca.UserName;
													 AddressContactPhone = Ca.Phone;
													 AddressContactEmail = Ca.EmailAddress;

													 // Update Member Of Groups and Available Groups lists
													 LoadAddressesGroupNames();
												 } );
						  }
						  catch( Exception E )
						  {
							  Logging.WriteLogLine( "Exception thrown fetching the address for " + SelectedAddress.Ca.CompanyName + ": " + E );
						  }
					  } );
		}
	}

	public void Execute_ShowHelp()
	{
		Logging.WriteLogLine( "Opening " + HelpUri );
		var Uri = new Uri( HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	private static string FindCountryMatch( string orig )
	{
		var Country = orig.ToLower();

		foreach( var Tmp in CountriesRegions.Countries )
		{
			if( Tmp.ToLower() == Country )
			{
				Country = Tmp;
				break;
			}
		}

		return Country;
	}

	/// <summary>
	///     Note: This only looks at the description field.
	///     From ca_mod.jsp:
	///     boolean matches = p.matcher(ca.description).matches();
	/// </summary>
	public void Execute_ExportAddresses()
	{
		Logging.WriteLogLine( "Export Addresses clicked" );

		//var cas = this.lvAddressBook.Items;

		//var filter = this.tbAddresssesExportFilter.Text.Trim();
		//bool reverse = (bool)this.cbAddressesExportExclude.IsChecked;

		var Filter  = ExportDescription;
		var Reverse = ExportIsExclude;

		if( ExtendedAddresses.Count > 0 )
		{
			Regex? R = null;

			if( Filter.Length > 0 )
				R = new Regex( Filter );

			//var csvList = "#accountId,description,suite,street,city,province,country,contactName,contactPhone,contactEmail,routeId,gpsLong,gpsLat,zip/postal\r\n";
			var CsvList = "#accountId,description,suite,street,city,province,country,contactPhone,contactEmail,gpsLong,gpsLat,zip/postal\r\n";

			foreach( var Eca in ExtendedAddresses )
			{
				var Line = '"' + SanitizeForCsv( Eca.Ca.CompanyNumber ) + '"' + "," + '"' + SanitizeForCsv( Eca.Ca.CompanyName ) + '"' + ","
						   + '"' + SanitizeForCsv( Eca.Ca.Suite ) + '"' + "," + '"' + SanitizeForCsv( Eca.Ca.AddressLine1 ) + '"' + ","
						   + '"' + SanitizeForCsv( Eca.Ca.City ) + '"' + "," + '"' + SanitizeForCsv( Eca.Ca.Region ) + '"' + ","
						   + '"' + SanitizeForCsv( Eca.Ca.Country ) + '"' + "," // TODO Missing field  + '"' + this.SanitizeForCsv(eca.Ca.contactName) + '"' + ","
						   + '"' + SanitizeForCsv( Eca.Ca.Phone ) + '"' + "," + '"' + SanitizeForCsv( Eca.Ca.EmailAddress ) + '"' + ","
						   + '"' + Eca.Ca.Longitude + '"' + "," + '"' + Eca.Ca.Latitude + '"' + "," + '"'
						   + SanitizeForCsv( Eca.Ca.PostalCode ) + '"' + "\r\n";

				if( R is not null )
				{
					var IsMatch = R.IsMatch( Eca.Ca.CompanyName );

					switch( IsMatch )
					{
					case true when !Reverse:
					case false when Reverse:
						CsvList += Line;

						break;
					}
				}
				else
					CsvList += Line;
			}

			NotepadHelper.ShowMessage( CsvList, "Exported Address Book" );
		}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="item">
	/// </param>
	/// <returns></returns>
	private static string SanitizeForCsv( string item )
	{
		//if (item != null && item.Length > 0 && item.Contains('"'))
		if( !string.IsNullOrEmpty( item ) && item.Contains( "\"" ) )
			item = item.Replace( '"', '\'' );

		return item;
	}

	private void UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses()
	{
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( Tis.Count > 0 )
		{
			foreach( var Ti in Tis )
			{
				switch( Ti.Content )
				{
				case TripEntry:
					Logging.WriteLogLine( "Reloading addresses in extant " + Globals.TRIP_ENTRY + " tab" );
					( (TripEntryModel)Ti.Content.DataContext ).WhenAccountChanges();

					break;

				case SearchTrips:
					Logging.WriteLogLine( "Reloading addresses in extant " + Globals.SEARCH_TRIPS + " tab" );

					//( (SearchTrips)ti.Content ).ComboBox_SelectionChanged_2Async( null, null );
					( (SearchTripsModel)Ti.Content.DataContext ).UpdateNewCompanyAddresses( SelectedAccountName );

					break;

				case Shipments:
					Logging.WriteLogLine( "Reloading addresses in extant " + Globals.UPLIFT_SHIPMENTS + " tab" );
					( (ShipmentsModel)Ti.Content.DataContext ).LoadAddresses( null, CurrentDetailList );

					break;
				}
			}
		}
	}

	// Offsets
	private const int CSV_NAME             = 0;
	private const int CSV_LOCATION_BARCODE = 1;
	private const int CSV_STREET           = 2;
	private const int CSV_UNITSUITE        = 3;
	private const int CSV_CITY             = 4;
	private const int CSV_REGION           = 5;
	private const int CSV_COUNTRY          = 6;
	private const int CSV_POSTALCODE       = 7;
	private const int CSV_EMAIL            = 8;
	private const int CSV_CONTACT_NAME     = 9;
	private const int CSV_CONTACT_PHONE    = 10;

	/// <summary>
	///     The CSV file to be imported will have the following format:
	///     1. Address Name – this is the Company Name aka Pick Up Name for the address. (required)
	///     2. Location Number – this is the Location Barcode for the address. (required)
	///     3. Street Address – this is the Street address for the address. (required).
	///     4. Unit/Suite – this is the Unit or Suite number for the address. (not required).
	///     5. City – this is the City for the address(required).
	///     6. State/Prov – this is the State for the address(required).
	///     7. Country – this is the Country for the address(required).
	///     8. Postal Code/Zip – this is the Postal Code for the address(required).
	///     9. Email Address – this is the Email Address for the address(not required).
	///     10. Contact Name – this is the Contact Name for the address(not required).
	///     11. Contact Phone – this is the Contact Phone for the address(not required).
	///     TODO Ensure that the country is one of the (currently) 3.
	///     TODO Ensure that the regions are translated to the correct values (abbreviation to full name).
	/// </summary>
	/// <returns></returns>
	public async void Execute_ImportAddresses()
	{
		if( IsSelectGroupFromListSelected )
			Logging.WriteLogLine( "Importing Addresses from file: " + SelectedImportFile + " into the group: " + SelectedGroupName );
		else
			Logging.WriteLogLine( "Importing Addresses from file: " + SelectedImportFile + " into the new group: " + NewImportGroupName );

		// (bool exists, List<string> lines) = ReadLocalFile(SelectedImportFile);
		if( File.Exists( SelectedImportFile ) )
		{
			var Errors        = false;
			var ErrorMessages = new List<string>();
			var LineAsString  = string.Empty;

			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			//var lines = new List<CsvAddressLine>();
			//var lines = new List<Utils.Csv.Row>();
			//var names = new List<string>();
			//	var Names = new SortedDictionary<string, string>();

			var NumberOfLines = 1;

			List<string> Names = new();

			// Skip the header
			for( var I = 1; I < CsvObj.RowCount; I++ )
			{
				++NumberOfLines;

				var Line = CsvObj[ I ];

				var Name = Line[ CSV_NAME ].AsString.Trim();
				Logging.WriteLogLine( "Name " + Name );

				if( Name.IsNotNullOrWhiteSpace() )
				{
					// Used for group assignment
					Names.Add( Name );
				}
				var LocationBarcode = Line[ CSV_LOCATION_BARCODE ].AsString.Trim();
				Logging.WriteLogLine( "LocationBarcode " + LocationBarcode );
				var Street = Line[ CSV_STREET ].AsString.Trim();
				Logging.WriteLogLine( "Street " + Street );
				var UnitSuite = Line[ CSV_UNITSUITE ].AsString.Trim();
				Logging.WriteLogLine( "UnitSuite " + UnitSuite );
				var City = Line[ CSV_CITY ].AsString.Trim();
				Logging.WriteLogLine( "City " + City );
				var Region = Line[ CSV_REGION ].AsString.Trim();
				Logging.WriteLogLine( "Region " + Region );
				var Country = Line[ CSV_COUNTRY ].AsString.Trim();
				Logging.WriteLogLine( "Country " + Country );
				var PostalCode = Line[ CSV_POSTALCODE ].AsString.Trim();
				Logging.WriteLogLine( "PostalCode " + PostalCode );
				var Email = Line[ CSV_EMAIL ].AsString.Trim();
				Logging.WriteLogLine( "Email " + Email );
				var ContactName = Line[ CSV_CONTACT_NAME ].AsString.Trim();
				Logging.WriteLogLine( "Contact Name: " + ContactName );
				var ContactPhone = Line[ CSV_CONTACT_PHONE ].AsString.Trim();
				Logging.WriteLogLine( "Contact Phone: " + ContactPhone );

				( Errors, ErrorMessages ) = ValidateLine( Line );

				if( Errors )
				{
					LineAsString = NumberOfLines + ": " + Name + "," + LocationBarcode + "," + Street + "," + UnitSuite + "," +
								   City + "," + Region + "," + Country + "," + PostalCode + "," + Email + "," +
								   ContactName + "," + ContactPhone;
					Logging.WriteLogLine( "Errors in line: " + NumberOfLines + ", line: " + LineAsString );

					break;
				}

				// -----------------------------
				// Not used in execution path
				// -----------------------------
				// Ensure that the region is complete and not an abbreviation
				var (AbbreviationAndCountry, Found) = CountriesRegions.FindRegionForCountry( Region, Country );

				if( !Found )
					AbbreviationAndCountry = CountriesRegions.FindRegionForAbbreviationAndCountry( Region, Country );

				if( AbbreviationAndCountry.IsNotNullOrWhiteSpace() )
					Region = AbbreviationAndCountry;
			}

			if( NumberOfLines == 0 )
			{
				Errors = true;
				ErrorMessages.Add( "File is empty" );
			}

			if( !Errors )
			{
				// Import the addresses
				//var (success, problems) = AddUpdateAddressBook(lines);
				var Success = true;

				try
				{
					// TODO Re-enable
					Success = await AddUpdateAddressBook( CsvObj );
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "ERROR Exception: " + E );
				}

				if( Success )
				{
					string Group;

					if( IsNewGroupSelected )
					{
						// Make the group
						//success = CreateGroup(NewImportGroupName);
						Group = NewImportGroupName;
					}
					else
					{
						Group = SelectedGroupName;

						// test1 \t(Members
						if( Group.IndexOf( '\t' ) > -1 )
							Group = Group.Substring( 0, Group.IndexOf( '\t' ) ).Trim();
					}

					// Names after duplicates adjusted
					Names.Clear();

					for( var I = 1; I < CsvObj.RowCount; I++ )
						Names.Add( CsvObj[ I ][ CSV_NAME ].AsString.Trim() );

					// Group now exists - time to add the addresses to the group
					await SaveGroup( Group, Names, false );

					await Dispatcher.Invoke( async () =>
											 {
												 var Title   = (string)Application.Current.TryFindResource( "AddressBookImportedTitle" );
												 var Message = (string)Application.Current.TryFindResource( "AddressBookImportedWithGroupMessage" );
												 Message = Message.Replace( "@1", ( NumberOfLines - 1 ) + "" );
												 Message = Message.Replace( "@2", Group );
												 MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Exclamation );

												 Execute_ClearImport();
												 await LoadAddresses();
												 await LoadGroups();
											 } );

					IsImportButtonEnabled = false;
				}
				else

				{
					Dispatcher.Invoke( () =>
									   {
										   var Title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
										   var Message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrors" ) + "\n";

										   foreach( var Error in ErrorMessages )
											   Message += "\t" + Error + "\n";

										   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
									   } );
				}
			}
			else
			{
				Dispatcher.Invoke( () =>
								   {
									   var Title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
									   var Message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFile" ) + "\n";
									   Message = Message.Replace( "@1", SelectedImportFile );

									   if( LineAsString.IsNotNullOrWhiteSpace() )
										   Message += (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsBadLine" ) + LineAsString + "\n";

									   foreach( var Error in ErrorMessages )
										   Message += "\t" + Error + "\n";

									   Message += (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileEnd" ) + "\n";
									   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
								   } );
			}
		}
	}

	//public void Execute_ImportAddresses_V1()
	//{
	//	if( IsSelectGroupFromListSelected )
	//		Logging.WriteLogLine( "Importing Addresses from file: " + SelectedImportFile + " into the group: " + SelectedGroupName );
	//	else
	//		Logging.WriteLogLine( "Importing Addresses from file: " + SelectedImportFile + " into the new group: " + NewImportGroupName );

	//	// (bool exists, List<string> lines) = ReadLocalFile(SelectedImportFile);
	//	if( File.Exists( SelectedImportFile ) )
	//	{
	//		var errors        = false;
	//		var errorMessages = new List<string>();

	//		var lines = new List<CsvAddressLine>();

	//		using( var csv = new CsvReader( new StreamReader( SelectedImportFile ), CultureInfo.CurrentCulture, true ) )
	//		{
	//			var numberOfLines = 0;

	//			csv.Read();
	//			csv.ReadHeader();

	//			while( csv.Read() )
	//			{
	//				++numberOfLines;

	//				var line = new CsvAddressLine
	//				           {
	//					           Name            = csv.GetField<string>( "Address Name" ).Trim(),
	//					           LocationBarcode = csv.GetField<string>( "Location Number" ).Trim(),
	//					           Street          = csv.GetField<string>( "Street Address" ).Trim(),
	//					           UnitSuite       = csv.GetField<string>( "Unit/Suite" ).Trim(),
	//					           City            = csv.GetField<string>( "City" ).Trim(),
	//					           Region          = csv.GetField<string>( "State/Prov" ).Trim(),
	//					           Country         = csv.GetField<string>( "Country" ).Trim(),
	//					           PostalCode      = csv.GetField<string>( "Postal Code/Zip" ).Trim(),
	//					           Email           = csv.GetField<string>( "Email Address" ).Trim()
	//				};

	//				( errors, errorMessages ) = ValidateLine( line );

	//				if( errors )
	//				{
	//					Logging.WriteLogLine( "Errors in line: " + numberOfLines + ", line: " + line );

	//					break;
	//				}

	//				// Ensure that the region is complete and not an abbreviation
	//				(string region, bool found) = CountriesRegions.FindRegionForCountry(line.Region, line.Country);
	//				if (!found)
	//                      {
	//					region = CountriesRegions.FindRegionForAbbreviationAndCountry(line.Region, line.Country);
	//                      }
	//				if (region.IsNotNullOrWhiteSpace())
	//                      {
	//					line.Region = region;
	//                      }

	//				lines.Add( line );
	//			}

	//			if( numberOfLines == 0 )
	//			{
	//				errors = true;
	//				errorMessages.Add( "File is empty" );
	//			}
	//		}

	//		if( !errors )
	//		{
	//			// Import the addresses
	//			var (success, problems) = AddUpdateAddressBook_V1( lines );

	//			if( success )
	//			{
	//				string group;

	//				if( IsNewGroupSelected )
	//				{
	//					// Make the group
	//					//success = CreateGroup(NewImportGroupName);
	//					group = NewImportGroupName;
	//				}
	//				else
	//				{
	//					group = SelectedGroupName;

	//					// test1 \t(Members
	//					group = group.Substring( 0, group.IndexOf( '\t' ) ).Trim();
	//				}

	//				// Group now exists - time to add the addresses to the group

	//				var names = new List<string>();

	//				foreach( var cal in lines )
	//					names.Add( cal.Name );

	//				success = SaveGroup( group, names );

	//				IsImportButtonEnabled = false;
	//			}
	//			else
	//			{
	//				Dispatcher.Invoke( () =>
	//				                   {
	//					                   var title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
	//					                   var message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrors" ) + "\n";

	//					                   foreach( var error in errorMessages )
	//						                   message += "\t" + error + "\n";

	//					                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
	//				                   } );
	//			}
	//		}
	//		else
	//		{
	//			Dispatcher.Invoke( () =>
	//			                   {
	//				                   var title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
	//				                   var message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFile" ) + "\n";

	//				                   foreach( var error in errorMessages )
	//					                   message += "\t" + error + "\n";

	//				                   message += (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileEnd" ) + "\n";
	//				                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
	//			                   } );
	//		}
	//	}
	//}


	public bool IsImporting
	{
		get { return Get( () => IsImporting, false ); }
		set { Set( () => IsImporting, value ); }
	}


	public string ImportingCompanyName
	{
		get { return Get( () => ImportingCompanyName, "" ); }
		set { Set( () => ImportingCompanyName, value ); }
	}

	private async Task<bool> AddUpdateAddressBook( Csv csv )
	{
		const bool SUCCESS = true;

		if( IsGlobalSelected )
		{
			Logging.WriteLogLine( "Adding/Updating " + ( csv.RowCount - 1 ) + " addresses for global address book " + GlobalAddressBookId );
			SelectedAccountId = GlobalAddressBookId; // Use for Global Address Book
		}
		else
			Logging.WriteLogLine( "Adding/Updating " + ( csv.RowCount - 1 ) + " addresses for account " + SelectedAccountId );

		if( !IsInDesignMode )
		{
			IsImporting          = true;
			ImportingCompanyName = "";

			// Skip the header
			try
			{
				var CoNames = new List<string>();

				for( var I = 1; I < csv.RowCount; I++ )
					CoNames.Add( csv[ I ][ CSV_NAME ].AsString.Trim() );

				var Duplicates = ( from C in CoNames
								   group C by C
								   into G
								   where G.Count() > 1
								   select G.Key ).ToHashSet();

				for( var I = 1; I < csv.RowCount; I++ )
				{
					var Line = csv[ I ];
					Logging.WriteLogLine( "Line: " + DumpRow( Line ) );

					var LocationBarcode = Line[ CSV_LOCATION_BARCODE ].AsString.Trim();

					var Address = ( from Eca in ExtendedAddresses
									where Eca.Ca.LocationBarcode == LocationBarcode
									select Eca ).FirstOrDefault();

					var CompanyName = Address is not null ? Address.Ca.CompanyName : Line[ CSV_NAME ].AsString.Trim();

					var PostalCode = Line[ CSV_POSTALCODE ].AsString.Trim();

					if( Duplicates.Contains( CompanyName ) )
					{
						CompanyName = $"{CompanyName} P{PostalCode}";
						Address     = null;
					}

					if( IsGlobalSelected )
						CompanyName = $"{CompanyName}'";

					Line[ CSV_NAME ] = CompanyName;

					var Company = new Company
								  {
									  LocationBarcode = LocationBarcode,
									  Suite           = Line[ CSV_UNITSUITE ].AsString.Trim(),
									  AddressLine1    = Line[ CSV_STREET ].AsString.Trim(),
									  City            = Line[ CSV_CITY ].AsString.Trim(),
									  Country         = Line[ CSV_COUNTRY ].AsString.Trim(),
									  CompanyName     = CompanyName,
									  PostalCode      = PostalCode,
									  Region          = Line[ CSV_REGION ].AsString.Trim(),

									  UserName = Line[ CSV_CONTACT_NAME ].AsString.Trim(),

									  //SecondaryId = AddressContactName,
									  Password     = Line[ CSV_NAME ].AsString.Trim(),
									  Phone        = Line[ CSV_CONTACT_PHONE ].AsString.Trim(),
									  EmailAddress = Line[ CSV_EMAIL ].AsString.Trim(),
									  Notes        = AddressNotes
								  };

					// Ensure that the region is complete and not an abbreviation
					//var Region = Line[ CSV_REGION ].AsString.Trim();
					//( string AbbreviationAndCountry, var Found ) = CountriesRegions.FindRegionForCountry( Region, Line[ CSV_COUNTRY ].AsString );

					//if( !Found )
					// AbbreviationAndCountry = CountriesRegions.FindRegionForAbbreviationAndCountry( Region, Line[ CSV_COUNTRY ].AsString );

					//if( AbbreviationAndCountry.IsNotNullOrWhiteSpace() )
					// Region = AbbreviationAndCountry;

					//if( Region != Line[ CSV_REGION ].AsString.Trim() )
					// Company.Region = Region;

					ImportingCompanyName = CompanyName;

					if( Address is null )
					{
						Logging.WriteLogLine( "Adding new address: " + Line[ CSV_NAME ].AsString + " to " + SelectedAccountId );

						var Acc = new AddCustomerCompany( "AddressBookModel" )
								  {
									  Company      = Company,
									  CustomerCode = SelectedAccountId
								  };
						await Azure.Client.RequestAddCustomerCompany( Acc );
					}
					else
					{
						Logging.WriteLogLine( "Updating address: " + Line[ CSV_NAME ].AsString + " to " + SelectedAccountId );

						var Ucc = new UpdateCustomerCompany( "AddressBookModel" )
								  {
									  Company      = Company,
									  CustomerCode = SelectedAccountId
								  };
						await Azure.Client.RequestUpdateCustomerCompany( Ucc );
					}
				}

				await Dispatcher.Invoke( async () =>
										 {
											 //await LoadAddresses();
											 SelectedAccountName = GlobalAddressBookId;
											 await WhenSelectedAccountNameChanges();
											 await LoadGroups();
										 } );
			}
			finally
			{
				IsImporting = false;
			}
		}

		return SUCCESS;
	}

	private static string DumpRow( Row? row )
	{
		var Line = string.Empty;

		if( row != null )
		{
			foreach( var Cell in row )
			{
				if( Cell != null )
					Line += Cell.AsString + ", ";
			}
		}
		//Logging.WriteLogLine( "DEBUG line: " + Line );
		return Line;
	}

	private static (bool errors, List<string> messages) ValidateLine( Row line )
	{
		var Errors   = false;
		var Messages = new List<string>();

		//if( line.Name.IsNullOrWhiteSpace() )
		if( line[ CSV_NAME ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "Address Name is empty" );
			Messages.Add( "Address Name is empty" );
		}

		//if( line.LocationBarcode.IsNullOrWhiteSpace() )
		if( line[ CSV_LOCATION_BARCODE ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "Location Number is empty" );
			Messages.Add( "Location Number is empty" );
		}

		//if( line.Street.IsNullOrWhiteSpace() )
		if( line[ CSV_STREET ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "Street Address is empty" );
			Messages.Add( "Street Address is empty" );
		}

		//if( line.City.IsNullOrWhiteSpace() )
		if( line[ CSV_CITY ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "City is empty" );
			Messages.Add( "City is empty" );
		}

		//if( line.Region.IsNullOrWhiteSpace() )
		if( line[ CSV_REGION ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "State/Prov is empty" );
			Messages.Add( "State/Prov is empty" );
		}

		//if( line.Country.IsNullOrWhiteSpace() )
		if( line[ CSV_COUNTRY ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "Country is empty" );
			Messages.Add( "Country is empty" );
		}

		//if( line.PostalCode.IsNullOrWhiteSpace() )
		if( line[ CSV_POSTALCODE ].AsString.IsNullOrWhiteSpace() )
		{
			Errors = true;
			Logging.WriteLogLine( "Postal Code/Zip is empty" );
			Messages.Add( "Postal Code/Zip is empty" );
		}

		var Exists = false;

		foreach( var C in CountriesRegions.Countries )
		{
			//if (line.Country.TrimToLower() == c.TrimToLower())
			if( line[ CSV_COUNTRY ].AsString.TrimToLower() == C.TrimToLower() )
			{
				Exists = true;

				break;
			}
		}

		if( !Exists )
		{
			Errors = true;

			//string message = "Country: " + line.Country + " MUST be one of '";
			var Message = "Country: " + line[ CSV_COUNTRY ].AsString + " MUST be one of '";

			foreach( var C in CountriesRegions.Countries )
				Message += C + ", ";
			Message = Message.Trim() + "'";
			Logging.WriteLogLine( Message );
			Messages.Add( Message );
		}

		return ( Errors, Messages );
	}

	/// <summary>
	///     The CSV file to be imported will have the following format:
	///     1. Address Name – this is the Company Name aka Pick Up Name for the address. (required)
	///     2. Location Number – this is the Location Barcode for the address. (required)
	///     3. Street Address – this is the Street address for the address. (required).
	///     4. Unit/Suite – this is the Unit or Suite number for the address. (not required).
	///     5. City – this is the City for the address(required).
	///     6. State/Prov – this is the State for the address(required).
	///     7. Country – this is the Country for the address(required).
	///     8. Postal Code/Zip – this is the Postal Code for the address(required).
	///     9. Email Address – this is the Email Address for the address(not required).
	/// </summary>
	/// <returns></returns>
	public (bool correct, List<string> errors) CheckHeaders()
	{
		var IsCorrect = true;
		var Errors    = new List<string>();

		var CsvObj = Csv.ReadFromFile( SelectedImportFile );

		//for (int i = 0; i < csvObj.RowCount; i++) 
		//         {
		//	var row = csvObj[i];

		//         }

		// NOTE the first line will be the header
		try
		{
			// Just the first line
			var Header = CsvObj[ 0 ];

			var Name = Header[ CSV_NAME ].AsString.Trim();
			Logging.WriteLogLine( "Name " + Name );
			var LocationBarcode = Header[ CSV_LOCATION_BARCODE ].AsString.Trim();
			Logging.WriteLogLine( "LocationBarcode " + LocationBarcode );
			var Street = Header[ CSV_STREET ].AsString.Trim();
			Logging.WriteLogLine( "Street " + Street );
			var UnitSuite = Header[ CSV_UNITSUITE ].AsString.Trim();
			Logging.WriteLogLine( "UnitSuite " + UnitSuite );
			var City = Header[ CSV_CITY ].AsString.Trim();
			Logging.WriteLogLine( "City " + City );
			var Region = Header[ CSV_REGION ].AsString.Trim();
			Logging.WriteLogLine( "Region " + Region );
			var Country = Header[ CSV_COUNTRY ].AsString.Trim();
			Logging.WriteLogLine( "Country " + Country );
			var PostalCode = Header[ CSV_POSTALCODE ].AsString.Trim();
			Logging.WriteLogLine( "PostalCode " + PostalCode );
			var Email = Header[ CSV_EMAIL ].AsString.Trim();
			Logging.WriteLogLine( "Email " + Email );

			//csv.ReadHeader();
			//csv.Read();

			//// Should be header
			//var line = new CsvAddressLine
			//{
			//	Name = csv.GetField<string>(0),
			//	LocationBarcode = csv.GetField<string>(1),
			//	Street = csv.GetField<string>(2),
			//	UnitSuite = csv.GetField<string>(3),
			//	City = csv.GetField<string>(4),
			//	Region = csv.GetField<string>(5),
			//	Country = csv.GetField<string>(6),
			//	PostalCode = csv.GetField<string>(7),
			//	Email = csv.GetField<string>(8)
			//};

			if( Name != "Address Name" )
			{
				Logging.WriteLogLine( "Address Name is wrong: " + Name );
				Errors.Add( "Found " + Name + " - should be 'Address Name'" );
				IsCorrect = false;
			}

			if( LocationBarcode != "Location Number" )
			{
				Logging.WriteLogLine( "LocationBarcode is wrong: " + LocationBarcode );
				Errors.Add( "Found " + LocationBarcode + " - should be 'Location Number'" );
				IsCorrect = false;
			}

			if( Street != "Street Address" )
			{
				Logging.WriteLogLine( "Street is wrong: " + Street );
				Errors.Add( "Found " + Street + " - should be 'Street Address'" );
				IsCorrect = false;
			}

			if( UnitSuite != "Unit/Suite" )
			{
				Logging.WriteLogLine( "UnitSuite is wrong: " + UnitSuite );
				Errors.Add( "Found " + UnitSuite + " - should be 'Unit/Suite'" );
				IsCorrect = false;
			}

			if( City != "City" )
			{
				Logging.WriteLogLine( "City is wrong: " + City );
				Errors.Add( "Found " + City + " - should be 'City'" );
				IsCorrect = false;
			}

			if( Region != "State/Prov" )
			{
				Logging.WriteLogLine( "Region is wrong: " + Region );
				Errors.Add( "Found " + Region + " - should be 'State/Prov'" );
				IsCorrect = false;
			}

			if( Country != "Country" )
			{
				Logging.WriteLogLine( "Country is wrong: " + Country );
				Errors.Add( "Found " + Country + " - should be 'Country'" );
				IsCorrect = false;
			}

			if( PostalCode != "Postal Code/Zip" )
			{
				Logging.WriteLogLine( "PostalCode is wrong: " + PostalCode );
				Errors.Add( "Found " + PostalCode + " - should be 'Postal Code/Zip'" );
				IsCorrect = false;
			}

			if( Email != "Email Address" )
			{
				Logging.WriteLogLine( "Email is wrong: " + Email );
				Errors.Add( "Found " + Email + " - should be 'Email Address'" );
				IsCorrect = false;
			}

			if( !IsCorrect )
			{
				var Message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeader" );
				Message = Message.Replace( "@1", SelectedImportFile );
				Errors.Insert( 0, Message );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );
			IsCorrect = false;
		}

		return ( IsCorrect, Errors );
	}


	public (bool correct, List<string> errors) CheckFileContents()
	{
		var IsCorrect = true;
		var Errors    = new List<string>();

		// NOTE the first line will be the header
		try
		{
			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			var NumberOfLines = 1; // Include header

			// Skip the header
			for( var I = 1; I < CsvObj.RowCount; I++ )
			{
				++NumberOfLines;

				var Line = CsvObj[ I ];

				var Name = Line[ CSV_NAME ];
				Logging.WriteLogLine( "Name " + Name.AsString );
				var LocationBarcode = Line[ CSV_LOCATION_BARCODE ];
				Logging.WriteLogLine( "LocationBarcode " + LocationBarcode.AsString );
				var Street = Line[ CSV_STREET ];
				Logging.WriteLogLine( "Street " + Street.AsString );
				var UnitSuite = Line[ CSV_UNITSUITE ];
				Logging.WriteLogLine( "UnitSuite " + UnitSuite.AsString );
				var City = Line[ CSV_CITY ];
				Logging.WriteLogLine( "City " + City.AsString );
				var Region = Line[ CSV_REGION ];
				Logging.WriteLogLine( "Region " + Region.AsString );
				var Country = Line[ CSV_COUNTRY ];
				Logging.WriteLogLine( "Country " + Country.AsString );
				var PostalCode = Line[ CSV_POSTALCODE ];
				Logging.WriteLogLine( "PostalCode " + PostalCode.AsString );
				var Email = Line[ CSV_EMAIL ];
				Logging.WriteLogLine( "Email " + Email.AsString );

				var Exists = false;

				foreach( var C in CountriesRegions.Countries )
				{
					if( Country.AsString.TrimToLower() == C.TrimToLower() )
					{
						Exists = true;

						break;
					}
				}

				if( !Exists )
				{
					var Message = "Line " + NumberOfLines + ": Country: " + Country + " must be one of '";

					foreach( var C in CountriesRegions.Countries )
					{
						if( C.IsNotNullOrWhiteSpace() )
							Message += C + ", ";
					}
					Message = Message.Trim();
					Message = Message.Substring( 0, Message.Length - 1 ) + "'";
					Logging.WriteLogLine( Message );
					Errors.Add( Message );
					IsCorrect = false;
				}
				else
				{
					// Valid country - check the region
					var (_, Found) = CountriesRegions.FindRegionForCountry( Region.AsString, Country.AsString );

					if( !Found )
					{
						var region = CountriesRegions.FindRegionForAbbreviationAndCountry( Region.AsString, Country.AsString );

						if( region != null )
							Found = true;
					}

					if( !Found )
					{
						var Message = "Line " + NumberOfLines + ": Region: '" + Region.AsString + "' must be one of '";

						foreach( var R in CountriesRegions.GetRegionsForCountry( Country.AsString ) )
						{
							if( R.IsNotNullOrWhiteSpace() )
								Message += R + ", ";
						}
						Message = Message.Trim();
						Message = Message.Substring( 0, Message.Length - 1 ) + "'";
						Logging.WriteLogLine( Message );
						Errors.Add( Message );
						IsCorrect = false;
					}
				}
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );
			IsCorrect = false;
		}

		return ( IsCorrect, Errors );
	}
#endregion
}

/// <summary>
///     Wrapper for CompanyAddress to provide an address string for list.
/// </summary>
public class ExtendedCompanyAddress
{
	public CompanyAddress Ca { get; set; }

	// This is stored in the company, not the address
	public string UserName { get; set; }

	public string AddressString
	{
		get
		{
			var Value = string.Empty;

			if( Ca.Suite.Trim().Length > 0 )
				Value += Ca.Suite + "-";

			var Street = Ca.AddressLine1;

			if( !string.IsNullOrWhiteSpace( Ca.AddressLine2 ) )
				Street += ", " + Ca.AddressLine2;

			Value += Street + ", " + Ca.City + ", " + Ca.Region;

			return Value;
		}
	}

	public ExtendedCompanyAddress( CompanyAddress ca, string userName )
	{
		Ca       = ca;
		UserName = userName;
	}
}

public class CsvAddressLine
{
	public string City            = "";
	public string Country         = "";
	public string Email           = "";
	public string LocationBarcode = "";
	public string Name            = "";
	public string PostalCode      = "";
	public string Region          = "";
	public string Street          = "";
	public string UnitSuite       = "";

	public override string ToString() => "Name: " + Name + ", LocationBarcode: " + LocationBarcode + ", Street: " + Street + ", UnitSuite: " + UnitSuite
										 + ", City: " + City + ", Region: " + Region + ", Country: " + Country + ", PostalCode: " + PostalCode + ", Email: " + Email;
}