﻿using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Win32;
using ViewModels.Trips.TripEntry;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using SearchTrips = ViewModels.Trips.SearchTrips.SearchTrips;

namespace ViewModels.Customers.AddressBook;

/// <summary>
///     Interaction logic for AddressBook.xaml
/// </summary>
public partial class AddressBook : Page
{
	private readonly Dictionary<string, Control> DictControls = new();

	public AddressBook()
	{
		InitializeComponent();

		foreach( TabItem Ti in tcGroups.Items )
			Ti.Visibility = Visibility.Collapsed;

		// TODO This should probably be only on for PML as they have 1 account
		//cbSelectCompany.SelectedIndex = -1; // Don't load an account
	}

	private async void BtnAddUpdate_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			if( DictControls.Count == 0 )
				LoadMappingDictionary();

			// Kludge - this is sometimes empty
			Model.AddressLocationBarcode = tbLocationBarcode.Text;

			var Errors = await Model.AddUpdateAddress( DictControls );

			if( Errors is {Count: > 0} )
			{
				var Message = (string)Application.Current.TryFindResource( "AddressBookValidationErrors" )
				              + Environment.NewLine;

				foreach( var Line in Errors )

					//message += "\t" + line + Environment.NewLine;
					Message += "    " + Line + Environment.NewLine;

				Message += (string)Application.Current.TryFindResource( "AddressBookValidationErrors1" );
				var Caption = (string)Application.Current.TryFindResource( "AddressBookValidationErrorsTitle" );

				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );

				UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses();
			}
		}
	}

	private static void UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses()
	{
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		foreach( var Ti in Tis )
		{
			switch( Ti.Content )
			{
			case TripEntry:
				Logging.WriteLogLine( "Reloading addresses in extant " + Globals.UPLIFT_SHIPMENTS + " tab" );

				( (TripEntryModel)Ti.Content.DataContext ).WhenAccountChanges();
				break;

			case SearchTrips Trips:
				Logging.WriteLogLine( "Reloading addresses in extant " + Globals.SEARCH_TRIPS + " tab" );

				//((Trips.SearchTrips.SearchTripsModel)ti.Content.DataContext).ComboBox_SelectionChanged_2Async(null, null);
				Trips.ComboBox_SelectionChanged_2Async( null, null );
				break;
			}
		}
	}

	//private void BtnExport_Click(object sender, System.Windows.RoutedEventArgs e)
	//{
	//    if (DataContext is AddressBookModel Model)
	//    {

	//    }
	//}

	private void LvAddresses_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		Logging.WriteLogLine( "Double-clicked address" );
		var                    Obj = GetListViewItemObject( lvAddresses, e.OriginalSource );
		ExtendedCompanyAddress Eca = null;

		if( Obj is ExtendedCompanyAddress Address )
			Eca = Address;

		if( DataContext is AddressBookModel Model )
		{
			if( Eca is not null )
			{
				Logging.WriteLogLine( "Double-clicked " + Eca.Ca.CompanyName );
				Model.Execute_LoadAddress( Eca );
			}
		}
	}

	// From https://stackoverflow.com/questions/929179/get-the-item-doubleclick-event-of-listview
	private static object GetListViewItemObject( ListView lv, object originalSource )
	{
		var Dep = (DependencyObject)originalSource;

		while( Dep is not null && !( Dep.GetType() == typeof( ListViewItem ) ) )
			Dep = VisualTreeHelper.GetParent( Dep );

		if( Dep == null )
			return null;

		var Obj = lv.ItemContainerGenerator.ItemFromContainer( Dep );
		return Obj;
	}

	//private void BtnEdit_Click(object sender, System.Windows.RoutedEventArgs e)
	//{
	//    if (DataContext is AddressBookModel Model)
	//    {
	//        if (this.lvAddresses.SelectedIndex > -1 && this.lvAddresses.SelectedItem != null)
	//        {
	//            Model.Execute_LoadAddress();
	//        }
	//    }
	//}

	private void BtnDelete_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			//DisplayNotImplementedMessage();
			if( ( lvAddresses.SelectedIndex > -1 ) && ( lvAddresses.SelectedItems.Count > 0 ) )
			{
				var Addresses = lvAddresses.SelectedItems.Cast<ExtendedCompanyAddress>().ToList();

				var Caption = (string)Application.Current.TryFindResource( "AddressBookDeleteQuestionTitle" );
				var Message = (string)Application.Current.TryFindResource( "AddressBookDeleteQuestion" ) + "\n";
				Message += (string)Application.Current.TryFindResource( "AddressBookDeleteQuestion2" );
				var Mbr = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

				if( Mbr == MessageBoxResult.Yes )
					Model.Execute_DeleteAddress( Addresses );
			}
		}
	}

	/// <summary>
	///     Add widgets and their associated fields.
	///     Uses the GetChildren extension method defined in Widgets.cs.
	/// </summary>
	/// <returns></returns>
	private void LoadMappingDictionary()
	{
		var Children = this.GetChildren();

		foreach( var Ctrl in Children )
		{
			var Binding = Ctrl switch
			              {
				              TextBox Tb         => BindingOperations.GetBinding( Tb, TextBox.TextProperty ),
				              ComboBox Cb        => BindingOperations.GetBinding( Cb, ItemsControl.ItemsSourceProperty ),
				              DateTimePicker Dtp => BindingOperations.GetBinding( Dtp, DateTimePicker.ValueProperty ),
				              TimePicker Tp      => BindingOperations.GetBinding( Tp, DateTimePicker.ValueProperty ),
				              _                  => null
			              };

			if( Binding is not null )
			{
				var Path = Binding.Path.Path;

				if( !DictControls.ContainsKey( Path ) )
					DictControls.Add( Path, (Control)Ctrl );
				else
					Logging.WriteLogLine( "ERROR " + Path + " already exists" );
			}
		}
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		Toolbar_MouseDoubleClick( null, null );
	}

	/// <summary>
	///     Kludge - the field isn't being updated through the binding.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void TbNotes_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressNotes = tbNotes.Text.Trim();
	}

	/// <summary>
	///     Kludge - the field isn't being updated through the binding.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void TbContactName_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressContactName = tbContactName.Text.Trim();
	}

	/// <summary>
	///     Kludge - the field isn't being updated through the binding.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void TbContactEmail_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressContactEmail = tbContactEmail.Text.Trim();
	}

	private void TbFilterCompanyName_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Logging.WriteLogLine( "FilterCompanyName: " + tbFilterCompanyName.Text + ", Model.FilterCompanyName: " + Model.FilterCompanyName );

			Model.WhenFilterCompanyNameChanges( tbFilterCompanyName.Text.Trim() );
		}
	}

	private void TbFilterLocationBarcode_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Logging.WriteLogLine( "FilterLocationBarcode: " + tbFilterLocationBarcode.Text + ", Model.FilterLocationBarcode: " + Model.FilterLocationBarcode );
			Model.WhenFilterLocationBarcodeChanges( tbFilterLocationBarcode.Text.Trim() );
		}
	}

	private void tbAddressBookNewGroupName_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Model.NewGroupDescription = tbAddressBookNewGroupName.Text.Trim();
			Logging.WriteLogLine( "New Group Name: " + Model.NewGroupDescription );
		}
	}

	private void lbAddressBookGroups_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.Execute_ModifyGroup();
	}


	private void btnAddressBookDeleteGroup_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			if( Model.SelectedGroup.IsNotNullOrWhiteSpace() )
			{
				var Message = (string)Application.Current.TryFindResource( "AddressBookGroupsDeleteGroup" ) + Environment.NewLine;
				Message += (string)Application.Current.TryFindResource( "AddressBookGroupsDeleteGroup1" ) + Environment.NewLine;
				Message =  Message.Replace( "@1", Model.SelectedGroup );
				Message =  Message.Replace( "\t", " " );
				var Caption = (string)Application.Current.TryFindResource( "AddressBookGroupsDeleteGroupTitle" );
				var Mbr     = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

				if( Mbr == MessageBoxResult.Yes )
					Model.Execute_DeleteGroup();
			}
		}
	}

	private void btnModifyGroupAdd_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			// Model.SelectedAddresses = (List<ExtendedCompanyAddress>)lvAddresses.SelectedItems;
			// Model.SelectedAddresses.Clear();
			var List = lvAddresses.SelectedItems.Cast<ExtendedCompanyAddress>().ToList();

			Model.SelectedAddresses = List;

			Model.Execute_AddSelectedToGroup();
		}
	}

	private void btnModifyGroupRemove_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			if( lbGroupUpdate.SelectedItems.Count > 0 )
			{
				var List = lbGroupUpdate.SelectedItems.Cast<string>().ToList();

				Model.SelectedGroupMembers = List;
				Model.Execute_RemoveSelectedFromGroup();
			}
		}
	}

	private void BtnSelectAll_Click( object sender, RoutedEventArgs e )
	{
		foreach( var Tmp in lvAddresses.Items )
			lvAddresses.SelectedItems.Add( Tmp );
	}

	private void BtnSelectNone_Click( object sender, RoutedEventArgs e )
	{
		lvAddresses.SelectedItems.Clear();
	}

	private void LbAddressBookAddCurrentAddressToGroup_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.Execute_AddToMemberOfGroups();
	}

	private void LbAddressBookMemberOfGroups_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.Execute_RemoveFromMemberOfGroups();
	}

	private void BtnToolbarClear_Click( object sender, RoutedEventArgs e )
	{
		tbDescription.Background     = Brushes.White;
		tbLocationBarcode.Background = Brushes.White;
		tbStreet.Background          = Brushes.White;
		tbCity.Background            = Brushes.White;

		tbLatitude.Text  = string.Empty;
		tbLongitude.Text = string.Empty;
	}

	private void MenuItem_Click_2( object sender, RoutedEventArgs e )
	{
		BtnToolbarClear_Click( null, null );
	}

	private void TbContactPhone_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressContactPhone = tbContactPhone.Text.Trim();
	}

	private void TbDescription_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressDescription = tbDescription.Text.Trim();
	}

	private void TbLocationBarcode_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressLocationBarcode = tbLocationBarcode.Text.Trim();
	}

	private void TbSuite_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressSuite = tbSuite.Text.Trim();
	}

	private void TbStreet_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressStreet = tbStreet.Text.Trim();
	}

	private void TbCity_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressCity = tbCity.Text.Trim();
	}

	private void TbZipPostal_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressZipPostal = tbZipPostal.Text.Trim();
	}

	private void BtnBrowse_Click( object sender, RoutedEventArgs e )
	{
		const string FILTER = "CSV Files (*.csv)|*.csv|Text Files (*.txt)|*txt";

		var Ofd = new OpenFileDialog
		          {
			          Filter      = FILTER,
			          Multiselect = false
		          };
		var Result = Ofd.ShowDialog();

		if( Result == true )
		{
			Logging.WriteLogLine( "Selected " + Ofd.FileName );
			var File = Ofd.FileName;

			if( DataContext is AddressBookModel Model )
			{
				try
				{
					Model.SelectedImportFile = File;
					var (IsCorrect, Errors)  = Model.CheckHeaders();

					if( !IsCorrect )
					{
						Model.SelectedImportFile    = string.Empty;
						Model.IsImportButtonEnabled = false;
						var Caption = (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeaderTitle" );
						var Message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeader" );
						Message = Message.Replace( "@1", File );

						foreach( var Error in Errors )
							Message += "\n\t" + Error;
						Message += "\n" + (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeaderEnd" );

						NotepadHelper.ShowMessage( Message, "Errors" );

						MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
					}
					else
					{
						( IsCorrect, Errors ) = Model.CheckFileContents();

						if( !IsCorrect )
						{
							Model.SelectedImportFile    = string.Empty;
							Model.IsImportButtonEnabled = false;
							var Caption = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
							var Message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFile" );
							Message = Message.Replace( "@1", File );

							foreach( var Error in Errors )
								Message += "\n" + Error;
							Message += "\n" + (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileEnd" );

							NotepadHelper.ShowMessage( Message, "Errors" );

							MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
						}
						else
							Model.IsImportButtonEnabled = true;
					}
				}
				catch (Exception ex)
				{
					Logging.WriteLogLine("Exception caught: " + ex.Message + "\n" + ex.StackTrace);
                    Model.SelectedImportFile = string.Empty;
                    Model.IsImportButtonEnabled = false;
                    var Caption = (string)Application.Current.TryFindResource("AddressBookImportAddressesErrorsInFileTitle");
                    var Message = (string)Application.Current.TryFindResource("AddressBookImportAddressesErrorsInFile");
                    Message = Message.Replace("@1", File);

					//foreach (var Error in Errors)
					//    Message += "\n" + Error;
					Message += "\n" + (string)Application.Current.TryFindResource("AddressBookImportAddressesErrorsInFileLocked") + "\n";

                    Message += "\n" + (string)Application.Current.TryFindResource("AddressBookImportAddressesErrorsInFileEnd");

                    NotepadHelper.ShowMessage(Message, Caption);

                    MessageBox.Show(Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
			}
		}
	}


	/// <summary>
	///     From https://stackoverflow.com/questions/1293883/putting-listbox-in-scrollviewer-mouse-wheel-does-not-work
	/// </summary>
	private void svAddresses_PreviewMouseWheel( object sender, MouseWheelEventArgs e )
	{
		if( e.Delta < 0 )
			svAddresses.LineDown();
		else
			svAddresses.LineUp();
	}


	private void IsFloatOrInt( object sender, TextCompositionEventArgs e )
	{
		var Key = e.Text[ 0 ];

		if( Key is >= '0' and <= '9' or '.' or '-' or '+' )
			return;

		e.Handled = true;
	}

	private void tbLongitude_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
			Model.AddressLongitude = tbLongitude.Text;
	}

	private void RbSelectAccount_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Model.IsAccountAddressBook        = true;
			Model.IsGlobalAddressBookSelected = false;
		}
	}

	private void RbSelectAccount_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Model.IsAccountAddressBook        = false;
			Model.IsGlobalAddressBookSelected = true;
		}
	}

	private void RbGlobal_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Model.IsAccountAddressBook        = false;
			Model.IsGlobalAddressBookSelected = true;
		}
	}

	private void RbGlobal_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is AddressBookModel Model )
		{
			Model.IsAccountAddressBook        = false;
			Model.IsGlobalAddressBookSelected = false;
		}
	}
}