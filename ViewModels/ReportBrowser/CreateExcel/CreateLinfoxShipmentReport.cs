﻿using System.CodeDom;
using System.IO;
using ClosedXML.Excel;
using Microsoft.Win32;
using Protocol.Data._Customers.Pml;

namespace ViewModels.ReportBrowser.CreateExcel;

internal class CreateLinfoxShipmentReport
{
	//private readonly Color PrimaryHeaderBg   = ColorTranslator.FromHtml( "#305496" );
	//private readonly Color SecondaryHeaderBg = ColorTranslator.FromHtml( "#00b0f0" );
	private const string PRIMARY_HEADER_BG   = "#305496";
	private const string SECONDARY_HEADER_BG = "#00b0f0";
	public static bool   IsInDesignMode { get; private set; }

	private ReportSettings ReportSettings;

	private TripList Trips;

	//private SortedDictionary<string, Trip> sdTrips = new SortedDictionary<string, Trip>();
	private readonly Dictionary<string, Dictionary<string, List<Trip>>> DictTripsByOperationAndRegion = new();
	private          List<Trip>                                         TripsInOrder                  = new();

	//private          ExcelPackage   package           = null;
	//private          ExcelWorksheet worksheet         = null;

	private XLWorkbook Package;

	private IXLWorksheet Worksheet;

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public async Task<CreateLinfoxShipmentReport> Load()
	{
		await LoadGroups();
		await LoadGroupNames();
		await LoadPmlRoutes();
		return this;
	}

	public (bool success, List<string> errors, string fileName) Run( ReportSettings rs )
	{
		bool Success;
		var  Errors   = new List<string>();
		var  FileName = string.Empty;

		ReportSettings = rs;

		Trips = SearchTrips();
		//Dictionary<string, decimal> pcs = GetHistoryPieceCounts(trips);

		Logging.WriteLogLine( "Found " + Trips?.Count + " trips" );

		if( Trips is {Count: > 0} )
		{
			//trips.Sort((x, y) => DateTimeOffset.Compare(x.CallTime, y.CallTime));
			TripsInOrder = Trips.OrderBy( x => x.CallTime ).ToList();

			LoadTripsByPickupZone();

			( Success, Errors, FileName ) = BuildReport();

			if( Success )
			{
				var Giveup = false;

				while( !Giveup )
				{
					try
					{
						// TODO Ask the user for directory to save the file
						var Dlg = new SaveFileDialog
								  {
									  DefaultExt = ".xlsx",
									  Filter     = "Microsoft Excel 2007-2013 XML (*.xlsx)|*.xlsx",
									  FileName   = FileName
								  };
						var Result = Dlg.ShowDialog();

						if( Result == true )
						{
							FileName = Dlg.FileName;

							var Fi = new FileInfo( FileName );

							//package.SaveAs( fi );
							Package.SaveAs( Fi.FullName );
							Giveup = true; // Don't try again
						}
						else
						{
							Giveup = true;

							// Cancelled
							Success = false;
							Errors.Add( (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsCancelled" ) );
						}
					}
					catch
					{
						//Dispatcher.Invoke(() =>
						//{
						//    string message = "";
						//});
						var Title   = (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsCantSaveReportTitle" );
						var Message = (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsCantSaveReport" );
						Message += "\n\t" + FileName + "\n";
						Message += (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsCantSaveReport2" );

						var Mbr = MessageBox.Show( Message, Title, MessageBoxButton.YesNo, MessageBoxImage.Question );

						if( Mbr == MessageBoxResult.No )
						{
							Logging.WriteLogLine( "Can't save the file - giving up" );
							Giveup  = true;
							Success = false;
							Errors.Add( (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsCancelled" ) );
						}
					}
				}
			}
		}
		else
		{
			Success = false;
			Errors.Add( (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsNoShipmentsFound" ) );
		}

		return ( Success, Errors, FileName );
	}

	public CreateLinfoxShipmentReport()
	{
		var Prop = DesignerProperties.IsInDesignModeProperty;
		IsInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty( Prop, typeof( FrameworkElement ) ).Metadata.DefaultValue;
	}

#region Data
	//public List<CompanyAddressGroup> Groups
	//{
	//    get { return Get(() => Groups, LoadGroups); }
	//    set { Set(() => Groups, value); }
	//}

	//public List<string> GroupNames
	//{
	//    get { return Get(() => GroupNames, new List<string>()); }
	//    set { Set(() => GroupNames, value); }
	//}

	private List<CompanyAddressGroup> Groups;

	/// <summary>
	///     Holds a map of group names and their companies.
	/// </summary>
	private readonly SortedDictionary<string, List<string>> GroupsAndMembers = new();

	//public Dictionary<string, PmlRoute> PmlRoutes
	//      {
	//          get => Get(() => PmlRoutes, LoadPMLRoutes);
	//          set => Set(() => PmlRoutes, value);
	//      }

	public Dictionary<string, PmlRoute> PmlRoutes = new();
#endregion

#region Cell Assignments
	// Summary section
	private readonly string CellSummaryNotYetCompleted           = "B3";
	private readonly string CellSummaryNotYetCompletedPercentage = "C3";
	private readonly string CellSummaryFutileNoStock             = "B4";
	private readonly string CellSummaryFutileNoStockPercentage   = "C4";
	private readonly string CellSummaryFutileNotReady            = "B5";
	private readonly string CellSummaryFutileNotReadyPercentage  = "C5";
	private readonly string CellSummaryComplete                  = "B6";
	private readonly string CellSummaryCompletePercentage        = "C6";
	private readonly string CellSummaryVerified                  = "B7";
	private readonly string CellSummaryTotalPu                   = "B8";
	private readonly string CellSummaryTotalPercentage           = "C8";

	private readonly string CellSummaryVerifiedPercentage = "C7";

	//private readonly string CellSummaryTotal = "B8";
	//private readonly string CellSummaryTotalPercentage = "C8";

	// Small section to the right
	private readonly string CellSummary2Attended = "F4";
	private readonly string CellSummary2Verified = "F5";

	// PickedUp section
	//private readonly string CellPickedUpNotYetCompleted = "B13";
	//private readonly string CellPickedUpFutileNotReady = "B14";
	//private readonly string CellPickedUpFutileNoStock = "B15";
	//private readonly string CellPickedUpComplete = "B16";
	//private readonly string CellPickedUpVerified = "B17";
	//private readonly string CellPickedUpNotYetAssigned = "B18";
	//private readonly string CellPickedUpTotal = "B19";

	// Total section
	//private readonly string CellTotalNotYetCompleted = "B21";
	//private readonly string CellTotalStoresAttended = "B22";
	//private readonly string CellTotalTotal = "B23";
	//private readonly string CellTotalPercentageToBeCompleted = "B24";
	//private readonly string CellTotalPercentageCompleted = "B25";
#endregion

#region Utilities
	/// <summary>
	/// </summary>
	/// <returns></returns>
	private TripList SearchTrips()
	{
		var          TripList               = new TripList();
		List<string> TripsWithoutPickupZone = new();

		var St = new SearchTrips
				 {
					 FromDate    = ReportSettings.SelectedFrom,
					 ToDate      = ReportSettings.SelectedTo,
					 ByTripId    = false,
					 StartStatus = GetStatusForString( ReportSettings.SelectedFromStatus ),
					 EndStatus   = GetStatusForString( ReportSettings.SelectedToStatus )
				 };

		if( ReportSettings.SelectedPackageType != ReportSettings.All )
			St.PackageType = ReportSettings.SelectedPackageType;
		Logging.WriteLogLine( "DEBUG About to fetch trips for report - fromDate: " + St.FromDate + " toDate: " + St.ToDate );
		TripList Raw = null;
		// PML has exceptions when trying to load trips from 1 June to the end of October.
		// Trying to break the search down into months
		//try
		//{
		//	Raw = await Azure.Client.RequestSearchTrips( St );
		//	Logging.WriteLogLine("DEBUG Finished fetching " + Raw?.Count + " trips for report");
		//	//throw new Exception("test");
		//}
		//catch (Exception e)
		//{
		//	Raw = null;
		//	Logging.WriteLogLine("Exception thrown fetching trips: " + e.Message + "\n" + e.StackTrace);
		//	string title = FindStringResource("ReportDialogErrorTitle");
		//	string message = FindStringResource("ReportDialogErrorFetchingTripsMessage");
		//	MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
		//}

		// Testing - make sure the results are the same
		//var Raw2 = await Azure.Client.RequestSearchTrips(St);
		//Logging.WriteLogLine("DEBUG Finished fetching " + Raw2?.Count + " trips for report in one range");

		Raw = GetTripsInDateChunks();

		//if (Raw != null && Raw2 != null && Raw.Count != Raw2.Count)
		//{
		//	Logging.WriteLogLine("____________________\nDEBUG: Raw.Count: " + Raw.Count + ", Raw2.Count: " + Raw2.Count + "\n____________________");
		//	CompareTwoTripLists(Raw2, Raw);
		//}

		if( Raw != null )
		{
			foreach( var Trip in Raw )
			{
				if( Trip.PickupZone.IsNullOrWhiteSpace() && Trip.PickupAddressPostalCode.IsNotNullOrWhiteSpace() )
				{
					Logging.WriteLogLine( "PickupZone is empty - looking up " + Trip.PickupAddressPostalCode );

					if( PmlRoutes.ContainsKey( Trip.PickupAddressPostalCode ) )
						Trip.PickupZone = PmlRoutes[ Trip.PickupAddressPostalCode ].Operation + "+" + PmlRoutes[ Trip.PickupAddressPostalCode ].Region;
					Logging.WriteLogLine( "Setting PickupZone " + Trip.PickupZone );
				}

				if( Trip.PickupZone.IsNotNullOrWhiteSpace() )
				{
					//Logging.WriteLogLine("\tDEBUG");
					if( IsTripInSelectedGroups( Trip ) )
					{
						var Operations = ReportSettings.SelectedOperations;

						//Logging.WriteLogLine("\tDEBUG");
						if( ( Operations.Count == 0 ) || ( Operations[ 0 ] == ReportSettings.All ) )
						{
							//Logging.WriteLogLine("\tDEBUG");
							TripList.Add( Trip );
						}
						else
						{
							//Logging.WriteLogLine("\tDEBUG");
							var (Op, _) = ExtractOperationRegion( Trip.PickupZone );

							var T = ( from Operation in ReportSettings.SelectedOperations
									  where Operation == Op
									  select Operation ).FirstOrDefault();

							if( T != null )
							{
								//Logging.WriteLogLine("\tDEBUG");
								TripList.Add( Trip );
							}
						}
					}
				}
				else
				{
					var Line = "TripId: " + Trip.TripId + " PickupAddressPostalCode: " + Trip.PickupAddressPostalCode;

					Logging.WriteLogLine( "Skipped trip with no pickupzone: " + Line );

					TripsWithoutPickupZone.Add( Line );
				}
			}

			if( TripsWithoutPickupZone.Count > 0 )
			{
				StringBuilder Sb = new();
				Sb.Append( FindStringResource( "ReportBrowserReportsMissingPickupZones" ) ).Append( "\n\n" );

				foreach( var Line in TripsWithoutPickupZone )
					Sb.Append( Line ).Append( "\n" );
				var Title = FindStringResource( "ReportBrowserReportsMissingPickupZonesTitle" );
				NotepadHelper.ShowMessage( Sb.ToString(), Title );
			}
		}

		return TripList;
	}

	private TripList GetTripsInDateChunks()
	{
		TripList TripList = new();

		var FromDate = ReportSettings.SelectedFrom;
		var ToDate   = ReportSettings.SelectedTo;

		var St = new SearchTrips
				 {
					 FromDate    = FromDate,
					 ToDate      = ToDate,
					 ByTripId    = false,
					 StartStatus = GetStatusForString( ReportSettings.SelectedFromStatus ),
					 EndStatus   = GetStatusForString( ReportSettings.SelectedToStatus )
				 };

		if( ReportSettings.SelectedPackageType != ReportSettings.All )
			St.PackageType = ReportSettings.SelectedPackageType;

		if( FromDate >= ToDate.AddMonths( -1 ) )
		{
			// Within the last month
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   Logging.WriteLogLine( "Fetching trips for range: fromDate: " + FromDate + " to " + ToDate );
									   Logging.WriteLogLine( "DEBUG About to fetch trips for report..." );
									   TripList Raw = null;

									   try
									   {
										   Raw = await Azure.Client.RequestSearchTrips( St );
										   Logging.WriteLogLine( "DEBUG Finished fetching " + Raw?.Count + " trips for report" );

										   TripList = Raw;
									   }
									   catch( Exception E )
									   {
										   Raw = null;
										   Logging.WriteLogLine( "Exception thrown fetching trips: " + E.Message + "\n" + E.StackTrace );
										   var Title   = FindStringResource( "ReportDialogErrorTitle" );
										   var Message = FindStringResource( "ReportDialogErrorFetchingTripsMessage" );
										   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
									   }
								   } )
						);
		}
		else
		{
			// Greater than a month - break down into pieces
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   var      Start = FromDate;
									   var      End   = FromDate.AddMonths( 1 );
									   TripList Raw   = null;

									   while( Start < ToDate && End <= ToDate )
									   {
										   Logging.WriteLogLine( "Fetching trips for range: fromDate: " + Start + " to " + End );
										   St.FromDate = Start;
										   St.ToDate   = End;

										   Logging.WriteLogLine( "DEBUG About to fetch trips for report..." );
										   Raw = null;

										   try
										   {
											   Raw = await Azure.Client.RequestSearchTrips( St );
											   Logging.WriteLogLine( "DEBUG Finished fetching " + Raw?.Count + " trips for report" );

											   TripList.AddRange( Raw );

											   Start = End;
											   End   = End.AddMonths( 1 );
										   }
										   catch( Exception E )
										   {
											   Raw = null;
											   Logging.WriteLogLine( "Exception thrown fetching trips: " + E.Message + "\n" + E.StackTrace );
											   var Title   = FindStringResource( "ReportDialogErrorTitle" );
											   var Message = FindStringResource( "ReportDialogErrorFetchingTripsMessage" );
											   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
										   }
									   }

									   // Deal with the remaining days
									   Start = End.AddMonths( -1 );
									   End   = ToDate;
									   Logging.WriteLogLine( "Fetching trips for range: fromDate: " + Start + " to " + End );
									   St.FromDate = Start;
									   St.ToDate   = End;

									   Logging.WriteLogLine( "DEBUG About to fetch trips for report..." );
									   Raw = null;

									   try
									   {
										   Raw = await Azure.Client.RequestSearchTrips( St );
										   Logging.WriteLogLine( "DEBUG Finished fetching " + Raw?.Count + " trips for report" );

										   TripList.AddRange( Raw );
									   }
									   catch( Exception E )
									   {
										   Raw = null;
										   Logging.WriteLogLine( "Exception thrown fetching trips: " + E.Message + "\n" + E.StackTrace );
										   var Title   = FindStringResource( "ReportDialogErrorTitle" );
										   var Message = FindStringResource( "ReportDialogErrorFetchingTripsMessage" );
										   MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
									   }

									   TripList = RemoveDuplicates( TripList );
								   } )
						);
		}

		return TripList;
	}

	private TripList RemoveDuplicates( TripList trips )
	{
		TripList Tl = new();

		if( trips != null )
		{
			foreach( var Trip in trips )
			{
				var Found = ( from T in Tl
							  where Trip.TripId == T.TripId
							  select T ).FirstOrDefault();

				if( Found == null )
				{
					Tl.Add( Trip );
				}
				else
				{
					Logging.WriteLogLine( "Found duplicate " + Trip.TripId + " CallTime: " + Trip.CallTime );
				}
			}
		}

		return Tl;
	}

	//private Dictionary<string, decimal> GetHistoryPieceCounts(TripList trips)
	//{
	//	Dictionary<string, decimal> pcs = new();

	//	foreach (Trip trip in trips)
	//	{
	//		List<AuditLine> auditLines = GetAuditForTrip(trip.TripId);
	//		if (auditLines.Count > 0)
	//		{
	//			Logging.WriteLogLine("DEBUG TripId: " + trip.TripId);
	//			foreach (AuditLine al in auditLines)
	//                  {
	//				if (al.Description.ToLower().Contains("piece"))
	//                      {
	//					string[] pieces = al.Description.Split('\r');
	//					foreach (string piece in pieces)
	//                          {
	//						if (piece.ToLower().Contains("piece"))
	//                              {
	//							 Logging.WriteLogLine("\tDEBUG " + piece.Trim().Replace('\r', ' '));
	//                              }
	//					}
	//                      }
	//				else if (al.Description.ToLower().Contains("program"))
	//				{
	//					string[] pieces = al.Description.Split('\r');
	//					foreach (string piece in pieces)
	//					{
	//						if (piece.ToLower().Contains("program"))
	//						{
	//							Logging.WriteLogLine("\tDEBUG " + piece.Trim().Replace('\r', ' '));
	//						}
	//					}
	//				}
	//				else if (al.Description.ToLower().Contains("stamp"))
	//				{
	//					string[] pieces = al.Description.Split('\r');
	//					foreach (string piece in pieces)
	//					{
	//						if (piece.ToLower().Contains("stamp"))
	//						{
	//							Logging.WriteLogLine("\tDEBUG " + piece.Trim().Replace('\r', ' '));
	//						}
	//					}
	//				}
	//			}
	//		}
	//		else
	//		{
	//			pcs.Add(trip.TripId, trip.Pieces);
	//		}
	//	}

	//	return pcs;
	//}

	public Func<LogLookup, LogLookup> RequestOverride = lookup => lookup;

	public Func<string, string> FormatData = data => data;

	/*
	private List<AuditLine> GetAuditForTrip( string tripId )
	{
		List<AuditLine> Items = new();

		if( !IsInDesignMode )
		{
			// From LogViewerModel
			var Lookup = new LogLookup
			             {
				             Log      = LOG.TRIP,
				             FromDate = DateTime.Now,
				             ToDate   = DateTime.Now,

				             //Key      = CurrentTrip.TripId
				             Key = tripId
			             };

			try
			{
				Task.WaitAll(
				             Task.Run( async () =>
				                       {
					                       var Lg = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );
					                       //Logging.WriteLogLine( "Found " + Lg.Count + " log lines" );

					                       //var LogItems = new ObservableCollection<String>();

					                       foreach( var Entry in Lg )
					                       {
						                       Entry.Data = FormatData( Entry.Data );

						                       //LogItems.Add(new DisplayEntry(L, Entry));
						                       var PartitionKey = Entry.PartitionKey;
						                       var Operation    = Entry.Operation;
						                       var Data         = Entry.Data;

						                       var Line = new AuditLine( Entry.Timestamp, PartitionKey, Operation, Data );

						                       //Logging.WriteLogLine("Found " + line.Description + ", " + line.ToString());
						                       // LogItems.Add(L.ToString());
						                       Items.Add( Line );
					                       }

					                       //Dispatcher.Invoke( () => { AuditLines = NewItems; } );

					                       //Dispatcher.Invoke(() =>
					                       //               {
					                       //                   //NothingFound = LogItems.Count == 0;

					                       //                   //Items = LogItems;
					                       //                   //SearchButtonEnabled = true;
					                       //                   //NewItems = LogItems;
					                       //               });
				                       } )
				            );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}
		}

		return Items;
	}
	*/

	/// <summary>
	/// </summary>
	/// <param
	///     name="trip">
	/// </param>
	/// <returns></returns>
	private bool IsTripInSelectedGroups( Trip trip )
	{
		var Yes = false;

		if( ReportSettings.SelectedGroups[ 0 ] == ReportSettings.All )
			Yes = true;
		else
		{
			var CompanyName = trip.PickupCompanyName;
			Logging.WriteLogLine( "Looking for PickupCompanyName: " + CompanyName );

			foreach( var GroupName in ReportSettings.SelectedGroups )
			{
				var Gn = GroupName.Substring( 0, GroupName.IndexOf( '\t' ) ).Trim();

				if( GroupsAndMembers.ContainsKey( Gn ) )
				{
					var Gns = GroupsAndMembers[ Gn ];

					foreach( var Cn in Gns )
					{
						if( Cn.Trim() == CompanyName )
						{
							Logging.WriteLogLine( "Found it in " + Gn );
							Yes = true;
							break;
						}
					}

					if( Yes )
						break;
				}
			}
		}

		return Yes;
	}

	private async Task LoadGroups()
	{
		Logging.WriteLogLine( "Loading groups" );
		var Ags = new List<CompanyAddressGroup>();

		CompanyAddressGroups Cags = null;

		try
		{
			Cags = await Azure.Client.RequestGetCompanyAddressGroups();
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown: " + E );
		}

		if( Cags is not null )
		{
			foreach( var Cag in Cags )
				Ags.Add( Cag );

			//Logging.WriteLogLine( "Found " + ags.Count + " groups" );
			Groups = Ags;
			await LoadGroupNames();
			return;
		}
		Groups = Ags;
	}


	private async Task LoadGroupNames()
	{
		var Sd = new SortedDictionary<string, string>();
		// ReSharper disable once CollectionNeverQueried.Local
		var Counts = new SortedDictionary<string, int>();
		GroupsAndMembers.Clear();

		foreach( var Cag in Groups )
		{
			if( !Sd.ContainsKey( Cag.Description.ToLower() ) )
			{
				Sd.Add( Cag.Description.ToLower(), Cag.Description );

				// Now get the number of companies in this group
				try
				{
					var Companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup( Cag.GroupNumber );

					if( Companies.Count > 0 )
					{
						var Count = Companies[ 0 ].Companies.Count;
						Counts.Add( Cag.Description.ToLower(), Count );

						var Sd1 = new SortedDictionary<string, string>();

						foreach( var Company in Companies[ 0 ].Companies )
						{
							//Logging.WriteLogLine( "Adding: " + company.ToLower() );

							if( !Sd1.ContainsKey( Company.ToLower() ) )
								Sd1.Add( Company.ToLower().Trim(), Company );
							//else
							//{
							// Logging.WriteLogLine( "Duplicate found: " + company );
							//}
						}

						var Sorted = new List<string>();

						foreach( var Key in Sd1.Keys )
							Sorted.Add( Sd1[ Key ] );

						GroupsAndMembers.Add( Cag.Description, Sorted );
					}
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown: " + E );
				}
			}
		}

//		var Members        = (string)Application.Current.TryFindResource( "AddressBookGroupsMembers" );
//		var LabelSingle    = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountSingle" );
//		var LabelNotSingle = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountNotSingle" );

		//if (SelectedAddress == null)
		//{
		//    LoadAddressesGroupNames();
		//    Loaded = true;
		//}
		//else
		//{
		//    Loaded = true;
		//}
	}


	private async Task LoadPmlRoutes()
	{
		Dictionary<string, PmlRoute> Prs = new();

		if( !IsInDesignMode )
		{
			var Routes = await Azure.Client.RequestPML_GetRoutes();

			if( Routes is not null )
			{
				Logging.WriteLogLine( "Found routes: " + Routes.Count );

				foreach( var Pr in Routes )
				{
					if( !Prs.ContainsKey( Pr.PostalCode ) )
						Prs.Add( Pr.PostalCode, Pr );
				}
			}
		}

		PmlRoutes = Prs;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="status">
	/// </param>
	/// <returns></returns>
	private STATUS GetStatusForString( string status )
	{
		var Result = STATUS.UNSET;

		switch( status )
		{
		case "UNSET":
			Result = STATUS.UNSET;
			break;

		case "NEW":
			Result = STATUS.NEW;
			break;

		case "ACTIVE":
			Result = STATUS.ACTIVE;
			break;

		case "DISPATCHED":
			Result = STATUS.DISPATCHED;
			break;

		case "PICKED_UP":
			Result = STATUS.PICKED_UP;
			break;

		case "DELIVERED":
			Result = STATUS.DELIVERED;
			break;

		case "VERIFIED":
			Result = STATUS.VERIFIED;
			break;

		case "POSTED":
			Result = STATUS.POSTED;
			break;

		case "INVOICED":
			Result = STATUS.INVOICED;
			break;

		case "PAID":
			Result = STATUS.FINALISED;
			break;

		case "DELETED":
			Result = STATUS.DELETED;
			break;
		}

		return Result;
	}


	/// <summary>
	/// </summary>
	/// <param
	///     name="operation">
	/// </param>
	/// <returns></returns>
	private List<string> FindRegionsForOperation( string operation )
	{
		var Regions = new List<string>();

		foreach( var Zone in ReportSettings.Zones )
		{
			var (Op, Region) = ExtractOperationRegion( Zone.Name );

			if( Op == operation )
				Regions.Add( Region );
		}

		return Regions;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="zone">
	/// </param>
	/// <returns></returns>
	private (string operation, string region) ExtractOperationRegion( string zone )
	{
		var Operation = string.Empty;
		var Region    = string.Empty;

		if( zone.Contains( "-" ) )
		{
			var Pieces = zone.Split( '-' );
			Operation = Pieces[ 0 ];
			Region    = Pieces[ 1 ];
		}
		else if( zone.Contains( "+" ) )
		{
			var Pieces = zone.Split( '+' );
			Operation = Pieces[ 0 ];
			Region    = Pieces[ 1 ];
		}

		return ( Operation, Region );
	}


	/// <summary>
	///     Creates a filename from the report name and the date.
	/// </summary>
	/// <returns></returns>
	private string BuildFileName()
	{
		var ReportName = (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsLinfox" );
		var Pieces     = ReportName.Split( ' ' );
		var BaseName   = string.Join( "_", Pieces );

		BaseName = BaseName + "_" + DateTime.Now.ToString( "yyyy-MM-dd" ) + ".xlsx";

		//Logging.WriteLogLine( "baseName: " + baseName );

		return BaseName;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="op">
	/// </param>
	/// <param
	///     name="region">
	/// </param>
	/// <returns></returns>
	private List<Trip> FindTripsForOperationAndRegion( string op, string region )
	{
		List<Trip> Found = null;

		if( ( DictTripsByOperationAndRegion[ op ] != null ) && ( DictTripsByOperationAndRegion[ op ][ region ] != null ) )
			Found = DictTripsByOperationAndRegion[ op ][ region ];

		return Found;
	}

	private void LoadTripsByPickupZone()
	{
		// Build the dictionaries
		foreach( var Zone in ReportSettings.Zones )
		{
			var (O, R) = ExtractOperationRegion( Zone.Name );

			if( O.IsNotNullOrWhiteSpace() && R.IsNotNullOrWhiteSpace() )
			{
				// Dictionary<operation, Dictionary<region, List<Trip>>>
				if( !DictTripsByOperationAndRegion.ContainsKey( O ) )
					DictTripsByOperationAndRegion.Add( O, new Dictionary<string, List<Trip>>() );

				if( !DictTripsByOperationAndRegion[ O ].ContainsKey( R ) )
					DictTripsByOperationAndRegion[ O ].Add( R, new List<Trip>() );
			}
		}

		foreach( var Trip in Trips )
		{
			var (O, R) = ExtractOperationRegion( Trip.PickupZone );

			if( O.IsNotNullOrWhiteSpace() && R.IsNotNullOrWhiteSpace() )
			{
				if( ( DictTripsByOperationAndRegion[ O ] != null ) && ( DictTripsByOperationAndRegion[ O ][ R ] != null ) )
				{
					//Logging.WriteLogLine( "Adding trip" + trip.TripId + " to operation: " + o + ", region: " + r );
					DictTripsByOperationAndRegion[ O ][ R ].Add( Trip );
				}
			}
			else
				Logging.WriteLogLine( "Skipping trip " + Trip.TripId + " with a bad PickupZone: " + Trip.PickupZone );
		}
	}
#endregion

#region Create Data and Populate
	/// <summary>
	///     Generates the summary data and populates the appropriate fields.
	///     Note: the percentage fields are formatted as percentages so no need to convert.
	/// </summary>
	private void CreateSummaryData()
	{
		var TotalTrips = Trips.Count;

		decimal NotYetCompleted        = 0;
		decimal FutileStoreNotReady    = 0;
		decimal FutileNoStockToCollect = 0;
		decimal CompletePickedUp       = 0;
		decimal Verified               = 0;

		foreach( var Trip in Trips )
		{
			switch( Trip.Status1 )
			{
			case STATUS.DISPATCHED:
				++NotYetCompleted;
				break;

			case STATUS.DELETED:
				++FutileNoStockToCollect;
				break;

			case STATUS.ACTIVE:
				++FutileStoreNotReady;
				break;

			case STATUS.PICKED_UP:
				++CompletePickedUp;
				break;

			case STATUS.VERIFIED:
				++Verified;
				break;

			case STATUS.NEW:
				break;
			}
		}

		Worksheet.Cell( CellSummaryNotYetCompleted ).Value = NotYetCompleted;
		Worksheet.Cell( CellSummaryFutileNoStock ).Value   = FutileNoStockToCollect;
		Worksheet.Cell( CellSummaryFutileNotReady ).Value  = FutileStoreNotReady;
		Worksheet.Cell( CellSummaryComplete ).Value        = CompletePickedUp;
		Worksheet.Cell( CellSummaryVerified ).Value        = Verified;

		// Percentages
		if( NotYetCompleted > 0 )
		{
			var CellNotYetCompleted = NotYetCompleted / TotalTrips;
			Worksheet.Cell( CellSummaryNotYetCompletedPercentage ).Value = CellNotYetCompleted;
		}
		else
			Worksheet.Cell( CellSummaryNotYetCompletedPercentage ).Value = "0%";

		if( FutileNoStockToCollect > 0 )
		{
			var CellFutileNoStock = FutileNoStockToCollect / TotalTrips;
			Worksheet.Cell( CellSummaryFutileNoStockPercentage ).Value = CellFutileNoStock;
		}
		else
			Worksheet.Cell( CellSummaryFutileNoStockPercentage ).Value = "0%";

		if( FutileStoreNotReady > 0 )
		{
			var CellFutileStoreNotReady = FutileStoreNotReady / TotalTrips;
			Worksheet.Cell( CellSummaryFutileNotReadyPercentage ).Value = CellFutileStoreNotReady;
		}
		else
			Worksheet.Cell( CellSummaryFutileNotReadyPercentage ).Value = "0%";

		if( CompletePickedUp > 0 )
		{
			var CellCompletePickedUp = CompletePickedUp / TotalTrips;
			Worksheet.Cell( CellSummaryCompletePercentage ).Value = CellCompletePickedUp;
		}
		else
			Worksheet.Cell( CellSummaryCompletePercentage ).Value = "0%";

		if( Verified > 0 )
		{
			var CellVerified = Verified / TotalTrips;
			Worksheet.Cell( CellSummaryVerifiedPercentage ).Value = CellVerified;
		}
		else
			Worksheet.Cell( CellSummaryVerifiedPercentage ).Value = "0%";

		// Now work out the percentages for small grid to the right

		var CellSummaryAttended = ( FutileNoStockToCollect + FutileStoreNotReady + CompletePickedUp ) / TotalTrips;

		if( CellSummaryAttended > 0 )
			Worksheet.Cell( CellSummary2Attended ).Value = CellSummaryAttended;
		else
			Worksheet.Cell( CellSummary2Attended ).Value = "0%";
		var SummaryVerified = Verified / TotalTrips;

		if( SummaryVerified > 0 )
			Worksheet.Cell( CellSummary2Verified ).Value = SummaryVerified;
		else
			Worksheet.Cell( CellSummary2Verified ).Value = "0%";
	}
	//private void CreateSummaryData_V1()
	//{
	//	int totalTrips = trips.Count;

	//	decimal notYetCompleted        = 0;
	//	decimal futileStoreNotReady    = 0;
	//	decimal futileNoStockToCollect = 0;
	//	decimal completePickedUp       = 0;
	//	decimal verified               = 0;
	//	decimal notYetAssigned         = 0;

	//	foreach( Trip trip in trips )
	//	{
	//		switch( trip.Status1 )
	//		{
	//		case STATUS.DISPATCHED:
	//			++notYetCompleted;
	//			break;

	//		case STATUS.DELETED:
	//			++futileNoStockToCollect;
	//			break;

	//		case STATUS.ACTIVE:
	//			++futileStoreNotReady;
	//			break;

	//		case STATUS.PICKED_UP:
	//			++completePickedUp;
	//			break;

	//		case STATUS.VERIFIED:
	//			++verified;
	//			break;

	//		case STATUS.NEW:
	//			++notYetAssigned;
	//			break;
	//		}
	//	}

	//	worksheet.Cells[ CellSummaryNotYetCompleted ].Value = notYetCompleted;
	//	worksheet.Cells[ CellSummaryFutileNoStock ].Value   = futileNoStockToCollect;
	//	worksheet.Cells[ CellSummaryFutileNotReady ].Value  = futileStoreNotReady;
	//	worksheet.Cells[ CellSummaryComplete ].Value        = completePickedUp;
	//	worksheet.Cells[ CellSummaryVerified ].Value        = verified;

	//	// Percentages
	//	if( notYetCompleted > 0 )
	//	{
	//		decimal cellNotYetCompleted = notYetCompleted / totalTrips;
	//		worksheet.Cells[ CellSummaryNotYetCompletedPercentage ].Value = cellNotYetCompleted;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummaryNotYetCompletedPercentage ].Value = "0%";
	//	}

	//	if( futileNoStockToCollect > 0 )
	//	{
	//		decimal cellFutileNoStock = futileNoStockToCollect / totalTrips;
	//		worksheet.Cells[ CellSummaryFutileNoStockPercentage ].Value = cellFutileNoStock;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummaryFutileNoStockPercentage ].Value = "0%";
	//	}

	//	if( futileStoreNotReady > 0 )
	//	{
	//		decimal cellFutileStoreNotReady = futileStoreNotReady / totalTrips;
	//		worksheet.Cells[ CellSummaryFutileNotReadyPercentage ].Value = cellFutileStoreNotReady;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummaryFutileNotReadyPercentage ].Value = "0%";
	//	}

	//	if( completePickedUp > 0 )
	//	{
	//		decimal cellCompletePickedUp = completePickedUp / totalTrips;
	//		worksheet.Cells[ CellSummaryCompletePercentage ].Value = cellCompletePickedUp;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummaryCompletePercentage ].Value = "0%";
	//	}

	//	if( verified > 0 )
	//	{
	//		decimal cellVerified = verified / totalTrips;
	//		worksheet.Cells[ CellSummaryVerifiedPercentage ].Value = cellVerified;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummaryVerifiedPercentage ].Value = "0%";
	//	}

	//	// Now work out the percentages for small grid to the right

	//	decimal cellSummaryAttended = ( futileNoStockToCollect + futileStoreNotReady + completePickedUp ) / totalTrips;

	//	if( cellSummaryAttended > 0 )
	//	{
	//		worksheet.Cells[ CellSummary2Attended ].Value = cellSummaryAttended;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummary2Attended ].Value = "0%";
	//	}
	//	decimal cellSummaryVerified = verified / totalTrips;

	//	if( cellSummaryVerified > 0 )
	//	{
	//		worksheet.Cells[ CellSummary2Verified ].Value = cellSummaryVerified;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ CellSummary2Verified ].Value = "0%";
	//	}
	//}

	private void CreatePickedUpData()
	{
		var Col         = 1;
		var TotalCols   = 0;
		var StartingRow = 13;
		int Row;

		decimal TotalNotYetCompleted = 0;
		decimal TotalAttended        = 0;

		decimal TotalCellNotYetCompleted  = 0;
		decimal TotalCellFutileNotReady   = 0;
		decimal TotalCellFutileNoStock    = 0;
		decimal TotalCellCompletePickedUp = 0;
		decimal TotalCellVerified         = 0;
		decimal TotalCellNotYetAssigned   = 0;

		// First, get the operations and regions
		foreach( var Op in ReportSettings.SelectedOperations )
		{
			//Logging.WriteLogLine("Operation: " + op);
			var Regions = FindRegionsForOperation( Op );

			if( Regions.Count > 0 )
			{
				foreach( var Region in Regions )
				{
					//Logging.WriteLogLine("Region: " + region);
					Row = StartingRow; // Start at top again
					++Col;             // Next column

					decimal CellNotYetCompleted  = 0;
					decimal CellFutileNotReady   = 0;
					decimal CellFutileNoStock    = 0;
					decimal CellCompletePickedUp = 0;
					decimal CellVerified         = 0;
					decimal CellNotYetAssigned   = 0;

					// decimal cellNotYetCompleted = 0;
					decimal CellToBeCompletedPercentage = 0;
					decimal CellCompletedPercentage     = 0;
					var     TripsForOperationAndRegion  = FindTripsForOperationAndRegion( Op, Region );
					Logging.WriteLogLine( "Found " + TripsForOperationAndRegion.Count + " trips for " + Op + "-" + Region );

					if( TripsForOperationAndRegion.Count > 0 )
					{
						foreach( var Trip in TripsForOperationAndRegion )
						{
							switch( Trip.Status1 )
							{
							case STATUS.DISPATCHED:
								++CellNotYetCompleted;
								break;

							case STATUS.DELETED:
								++CellFutileNoStock;
								break;

							case STATUS.ACTIVE:
								++CellFutileNotReady;
								break;

							case STATUS.PICKED_UP:
								++CellCompletePickedUp;
								break;

							case STATUS.VERIFIED:
								++CellVerified;
								break;

							case STATUS.NEW:
								++CellNotYetAssigned;
								break;
							}
						}
					}

					// Populate the cells
					Worksheet.Cell( Row++, Col ).Value = CellNotYetCompleted;
					Worksheet.Cell( Row++, Col ).Value = CellFutileNotReady;
					Worksheet.Cell( Row++, Col ).Value = CellFutileNoStock;
					Worksheet.Cell( Row++, Col ).Value = CellCompletePickedUp;
					Worksheet.Cell( Row++, Col ).Value = CellVerified;
					Worksheet.Cell( Row++, Col ).Value = CellNotYetAssigned;

					// Total row
					Worksheet.Cell( Row++, Col ).Value = CellNotYetCompleted + CellFutileNotReady + CellFutileNoStock + CellCompletePickedUp + CellVerified;

					//row += 2; // Skip over total row and blank row
					Row += 1; // Skip over blank row

					Worksheet.Cell( Row++, Col ).Value = CellNotYetCompleted;

					//cellAttended = cellNotYetCompleted + cellFutileNotReady + cellFutileNoStock + cellCompletePickedUp + cellVerified;
					var CellAttended = CellFutileNotReady + CellFutileNoStock + CellCompletePickedUp + CellVerified;
					Worksheet.Cell( Row++, Col ).Value = CellAttended;

					Worksheet.Cell( Row++, Col ).Value = CellNotYetCompleted + CellAttended;
					//++row; // Skip over total row
					/*
					nyc	    5				            4       0
					c	    0				            1       5

					%nyc	100% nyc / total * 100		80%     0%
					%c	    0%   c   / total * 100		20%     100%
					 */

					if( ( CellNotYetCompleted != 0 ) && ( TripsForOperationAndRegion.Count != 0 ) )
						CellToBeCompletedPercentage = CellNotYetCompleted / TripsForOperationAndRegion.Count;

					if( CellToBeCompletedPercentage > 0 )
						Worksheet.Cell( Row++, Col ).Value = CellToBeCompletedPercentage;
					else
						Worksheet.Cell( Row++, Col ).Value = "0%";

					if( ( CellNotYetCompleted != 0 ) && ( TripsForOperationAndRegion.Count != 0 ) )
					{
						//cellCompletedPercentage = (trips.Count - cellNotYetCompleted) / trips.Count;
						CellCompletedPercentage = CellAttended / TripsForOperationAndRegion.Count;
					}

					if( CellCompletedPercentage > 0 )
						Worksheet.Cell( Row, Col ).Value = CellCompletedPercentage;
					else
						Worksheet.Cell( Row, Col ).Value = "0%";

					TotalNotYetCompleted += CellNotYetCompleted;
					TotalAttended        += CellAttended;

					TotalCols = Col + 1;

					TotalCellNotYetCompleted  += CellNotYetCompleted;
					TotalCellFutileNotReady   += CellFutileNotReady;
					TotalCellFutileNoStock    += CellFutileNoStock;
					TotalCellCompletePickedUp += CellCompletePickedUp;
					TotalCellVerified         += CellVerified;
					TotalCellNotYetAssigned   += TotalCellNotYetAssigned;
				}

				// Populate the totals column
				Row                                      = StartingRow;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalCellNotYetCompleted;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalCellFutileNotReady;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalCellFutileNoStock;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalCellCompletePickedUp;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalCellVerified;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalCellNotYetAssigned;

				// Total of totals
				Worksheet.Cell( Row, TotalCols ).Value = TotalCellNotYetCompleted + TotalCellFutileNotReady + TotalCellFutileNoStock + TotalCellCompletePickedUp + TotalCellVerified + TotalCellNotYetAssigned;

				// Skip over blank line
				Row                                      = 21;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalNotYetCompleted;
				Worksheet.Cell( Row++, TotalCols ).Value = TotalAttended;
				Worksheet.Cell( Row, TotalCols ).Value   = TotalNotYetCompleted + TotalAttended;
			}
		}

		// Percentage totals for TOTAL column
		Row = 24;
		Col = TotalCols;

		if( TotalNotYetCompleted > 0 )
			Worksheet.Cell( Row++, Col ).Value = TotalNotYetCompleted / Trips.Count;
		else
			Worksheet.Cell( Row++, Col ).Value = "0%";

		if( TotalAttended > 0 )
			Worksheet.Cell( Row, Col ).Value = TotalAttended / Trips.Count;
		else
			Worksheet.Cell( Row, Col ).Value = "0%";
	}

	//private void CreatePickedUpData_V1()
	//{
	//	int col         = 1;
	//	int totalCols   = 0;
	//	int startingRow = 13;
	//	int row         = startingRow;

	//	decimal totalNotYetCompleted = 0;
	//	decimal totalAttended        = 0;

	//	// First, get the operations and regions
	//	foreach( string op in rs.SelectedOperations )
	//	{
	//		Logging.WriteLogLine( "operation: " + op );
	//		List<string> regions = FindRegionsForOperation( op );

	//		if( regions.Count > 0 )
	//		{
	//			foreach( string region in regions )
	//			{
	//				Logging.WriteLogLine( "region: " + region );
	//				row = startingRow; // Start at top again
	//				++col;             // Next column

	//				decimal cellNotYetCompleted  = 0;
	//				decimal cellFutileNotReady   = 0;
	//				decimal cellFutileNoStock    = 0;
	//				decimal cellCompletePickedUp = 0;
	//				decimal cellVerified         = 0;
	//				decimal cellNotYetAssigned   = 0;

	//				// decimal cellNotYetCompleted = 0;
	//				decimal    cellAttended                = 0;
	//				decimal    cellToBeCompletedPercentage = 0;
	//				decimal    cellCompletedPercentage     = 0;
	//				List<Trip> tripsForOperationAndRegion  = FindTripsForOperationAndRegion( op, region );
	//				Logging.WriteLogLine( "Found " + tripsForOperationAndRegion.Count + " trips for " + op + "-" + region );

	//				if( tripsForOperationAndRegion.Count > 0 )
	//				{
	//					foreach( Trip trip in tripsForOperationAndRegion )
	//					{
	//						switch( trip.Status1 )
	//						{
	//						case STATUS.DISPATCHED:
	//							++cellNotYetCompleted;
	//							break;

	//						case STATUS.DELETED:
	//							++cellFutileNoStock;
	//							break;

	//						case STATUS.ACTIVE:
	//							++cellFutileNotReady;
	//							break;

	//						case STATUS.PICKED_UP:
	//							++cellCompletePickedUp;
	//							break;

	//						case STATUS.VERIFIED:
	//							++cellVerified;
	//							break;

	//						case STATUS.NEW:
	//							++cellNotYetAssigned;
	//							break;
	//						}
	//					}
	//				}

	//				// Populate the cells
	//				worksheet.Cells[ row++, col ].Value = cellNotYetCompleted;
	//				worksheet.Cells[ row++, col ].Value = cellFutileNotReady;
	//				worksheet.Cells[ row++, col ].Value = cellFutileNoStock;
	//				worksheet.Cells[ row++, col ].Value = cellCompletePickedUp;
	//				worksheet.Cells[ row++, col ].Value = cellVerified;
	//				worksheet.Cells[ row++, col ].Value = cellNotYetAssigned;

	//				row += 2; // Skip over total row and blank row

	//				worksheet.Cells[ row++, col ].Value = cellNotYetCompleted;

	//				//cellAttended = cellNotYetCompleted + cellFutileNotReady + cellFutileNoStock + cellCompletePickedUp + cellVerified;
	//				cellAttended                        = cellFutileNotReady + cellFutileNoStock + cellCompletePickedUp + cellVerified;
	//				worksheet.Cells[ row++, col ].Value = cellAttended;

	//				++row; // Skip over total row
	//				/*
	//				nyc	    5				            4       0
	//				c	    0				            1       5

	//				%nyc	100% nyc / total * 100		80%     0%
	//				%c	    0%   c   / total * 100		20%     100%
	//				 */

	//				if( cellNotYetCompleted != 0 && tripsForOperationAndRegion.Count != 0 )
	//				{
	//					cellToBeCompletedPercentage = cellNotYetCompleted / tripsForOperationAndRegion.Count;
	//				}

	//				if( cellToBeCompletedPercentage > 0 )
	//				{
	//					worksheet.Cells[ row++, col ].Value = cellToBeCompletedPercentage;
	//				}
	//				else
	//				{
	//					worksheet.Cells[ row++, col ].Value = "0%";
	//				}

	//				if( cellNotYetCompleted != 0 && tripsForOperationAndRegion.Count != 0 )
	//				{
	//					//cellCompletedPercentage = (trips.Count - cellNotYetCompleted) / trips.Count;
	//					cellCompletedPercentage = cellAttended / tripsForOperationAndRegion.Count;
	//				}

	//				if( cellCompletedPercentage > 0 )
	//				{
	//					worksheet.Cells[ row++, col ].Value = cellCompletedPercentage;
	//				}
	//				else
	//				{
	//					worksheet.Cells[ row++, col ].Value = "0%";
	//				}

	//				totalNotYetCompleted += cellNotYetCompleted;
	//				totalAttended        += cellAttended;

	//				totalCols = col + 1;
	//			}
	//		}
	//	}

	//	// Percentage totals for TOTAL column
	//	row = 24;
	//	col = totalCols;

	//	if( totalNotYetCompleted > 0 )
	//	{
	//		worksheet.Cells[ row++, col ].Value = totalNotYetCompleted / trips.Count;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ row++, col ].Value = "0%";
	//	}

	//	if( totalAttended > 0 )
	//	{
	//		worksheet.Cells[ row, col ].Value = totalAttended / trips.Count;
	//	}
	//	else
	//	{
	//		worksheet.Cells[ row, col ].Value = "0%";
	//	}
	//}
#endregion

#region Build Sections
	/// <summary>
	///     Notes and changes from scope (Gord):
	///     - Satchel trips are _NOT_excluded from the list of trips.
	///     - 6. “Not Yet Assigned” will be shipments with an IDS status of New - NOT Dispatched.
	/// </summary>
	/// <returns></returns>
	private (bool success, List<string> errors, string fileName) BuildReport()
	{
		var Success  = true;
		var Errors   = new List<string>();
		var FileName = BuildFileName();

		//package = new ExcelPackage();
		Package = new XLWorkbook();

		// Add a new worksheet to the empty workbook
		var ReportName = (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsLinfox" );
		//worksheet = package.Workbook.Worksheets.Add( reportName );
		Worksheet = Package.Worksheets.Add( ReportName );

		// Turns on readonly status for cells marked 'Locked'
		//worksheet.Protection.IsProtected = true;

		// Sections
		try
		{
			BuildSummarySection();
			BuildPickedUpSection();
			BuildTripsSection();
		}
		catch( Exception E )
		{
			Success = false;
			Errors.Add( E.Message );
		}

		if( Success )
		{
			CreateSummaryData();
			CreatePickedUpData();

			// IMPORTANT Do this last, after the sheet has been fully populated
			//worksheet.Cells.AutoFitColumns( 0 ); //Autofit columns for all cells
			Worksheet.Columns().AdjustToContents();

			// And the workbook calculation mode:
			Package.CalculateMode = XLCalculateMode.Auto;
		}

		return ( Success, Errors, FileName );
	}


	/// <summary>
	///     First header.
	///     Starts at A1.
	/// </summary>
	private void BuildSummarySection()
	{
		// Section Header
		Worksheet.Cell( "A1" ).Value           = "Summary";
		Worksheet.Cell( "A1" ).Style.Font.Bold = true;

		//Add the headers
		Worksheet.Cell( "A2" ).Value = "Status";
		Worksheet.Cell( "B2" ).Value = "P-Up";
		Worksheet.Cell( "C2" ).Value = "%";

		var Range = Worksheet.Range( "A2:C2" );
		Range.Style.Font.Bold = true;
		//range.Style.Fill.PatternType = ExcelFillStyle.Solid;

		Range.Style.Fill.BackgroundColor = XLColor.FromHtml( PRIMARY_HEADER_BG );
		Range.Style.Font.FontColor       = XLColor.White;

		Worksheet.Cell( "A3" ).Value = "Not Yet Completed";
		Worksheet.Cell( "A4" ).Value = "Futile - No Stock to Collect";
		Worksheet.Cell( "A5" ).Value = "Futile - Store not Ready";
		Worksheet.Cell( "A6" ).Value = "Complete (Picked Up)";
		Worksheet.Cell( "A7" ).Value = "Verified";

		Worksheet.Cell( "A8" ).Value = "Total";
		Range                        = Worksheet.Range( "A8:C8" );
		Range.Style.Border.TopBorder = XLBorderStyleValues.Thin;
		Range.Style.Font.Bold        = true;

		Range = Worksheet.Range( "A2:C8" );
		//range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

		Range = Worksheet.Range( "B2:C8" );
		//range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
		Range.Style.Alignment.SetHorizontal( XLAlignmentHorizontalValues.Center );

		Worksheet.Cell( CellSummaryTotalPu ).FormulaA1         = "{B3+B4+B5+B6+B7}";
		Worksheet.Cell( CellSummaryTotalPercentage ).FormulaA1 = "{C3+C4+C5+C6+C7}";
		//worksheet.Cell(8, 2).Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(3, 2, 7, 2).Address);
		//worksheet.Cells[8, 3].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(3, 3, 7, 3).Address);

		Range                           = Worksheet.Range( "C3:H8" );
		Range.Style.NumberFormat.Format = "#.##%";

		//worksheet.Cells[8, 2].Style.Locked = true;
		//worksheet.Cells[8, 3].Style.Locked = true;

		//            
		// Small section to right
		//
		Worksheet.Cell( "E4" ).Value = "Attended";
		Worksheet.Cell( "E5" ).Value = "Verified";
		//worksheet.Cell("E4:E5").Style.Font.Bold = true;
		//worksheet.Cells["E4:F5"].Style.Border.BorderAround(ExcelBorderStyle.Thin);
		Range                            = Worksheet.Range( "E4:F5" );
		Range.Style.Font.Bold            = true;
		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
		//range.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

		//using (var range = worksheet.Cells["E4:F5"])
		//{
		//	range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
		//}
		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
		//using (var range = worksheet.Cells["F4:F5"])
		//{
		//	range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
		//	range.Style.Numberformat.Format = "#.##%";
		//}
		Range.Style.Alignment.SetHorizontal( XLAlignmentHorizontalValues.Center );
		Range.Style.NumberFormat.Format = "#.##%";
	}

	//private void BuildSummarySection_V1()
	//{
	//	// Section Header
	//	worksheet.Cells[ "A1" ].Value              = "Summary";
	//	worksheet.Cells[ "A1:A1" ].Style.Font.Bold = true;

	//	//Add the headers
	//	worksheet.Cells[ "A2" ].Value = "Status";
	//	worksheet.Cells[ "B2" ].Value = "P-Up";
	//	worksheet.Cells[ "C2" ].Value = "%";

	//	using( var range = worksheet.Cells[ 2, 1, 2, 3 ] ) // A2..C2
	//	{
	//		range.Style.Font.Bold        = true;
	//		range.Style.Fill.PatternType = ExcelFillStyle.Solid;
	//		range.Style.Fill.BackgroundColor.SetColor( PrimaryHeaderBg );
	//		range.Style.Font.Color.SetColor( Color.White );
	//	}

	//	worksheet.Cells[ "A3" ].Value = "Not Yet Completed";
	//	worksheet.Cells[ "A4" ].Value = "Futile - No Stock to Collect";
	//	worksheet.Cells[ "A5" ].Value = "Futile - Store not Ready";
	//	worksheet.Cells[ "A6" ].Value = "Complete (Picked Up)";
	//	worksheet.Cells[ "A7" ].Value = "Verified";

	//	worksheet.Cells[ "A8" ].Value                     = "Total";
	//	worksheet.Cells[ "A8:C8" ].Style.Border.Top.Style = ExcelBorderStyle.Thin;
	//	worksheet.Cells[ "A8:C8" ].Style.Font.Bold        = true;

	//	using( var range = worksheet.Cells[ "A2:C8" ] )
	//	{
	//		range.Style.Border.BorderAround( ExcelBorderStyle.Thick );
	//	}

	//	using( var range = worksheet.Cells[ "B2:C8" ] )
	//	{
	//		range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
	//	}

	//	worksheet.Cells[ 8, 2 ].Formula                         = string.Format( "SUBTOTAL(9,{0})", new ExcelAddress( 3, 2, 7, 2 ).Address );
	//	worksheet.Cells[ 8, 3 ].Formula                         = string.Format( "SUBTOTAL(9,{0})", new ExcelAddress( 3, 3, 7, 3 ).Address );
	//	worksheet.Cells[ 3, 3, 8, 3 ].Style.Numberformat.Format = "#.##%";

	//	//worksheet.Cells[8, 2].Style.Locked = true;
	//	//worksheet.Cells[8, 3].Style.Locked = true;

	//	//            
	//	// Small section to right
	//	//
	//	worksheet.Cells[ "E4" ].Value              = "Attended";
	//	worksheet.Cells[ "E5" ].Value              = "Verified";
	//	worksheet.Cells[ "E4:E5" ].Style.Font.Bold = true;
	//	worksheet.Cells[ "E4:F5" ].Style.Border.BorderAround( ExcelBorderStyle.Thin );

	//	using( var range = worksheet.Cells[ "E4:F5" ] )
	//	{
	//		range.Style.Border.BorderAround( ExcelBorderStyle.Thick );
	//	}

	//	using( var range = worksheet.Cells[ "F4:F5" ] )
	//	{
	//		range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
	//		range.Style.Numberformat.Format = "#.##%";
	//	}
	//}

	/// <summary>
	///     Starts at [A11].
	/// </summary>
	private void BuildPickedUpSection()
	{
		Worksheet.Cell( 11, 1 ).Value           = "PickedUp";
		Worksheet.Cell( 11, 1 ).Style.Font.Bold = true;

		var StartingCol = 2;
		var Row         = 11;
		var Col         = StartingCol;

		IXLRange Range;

		foreach( var Op in ReportSettings.SelectedOperations )
		{
			var RegionCount = FindRegionsForOperation( Op ).Count;

			if( RegionCount == 1 )
			{
				Worksheet.Cell( Row, Col ).Value = Op;
				//worksheet.Cell(row, col).Style.Border.BorderAround(ExcelBorderStyle.Thin);
				Worksheet.Cell( Row, Col ).Style.Border.OutsideBorder =  XLBorderStyleValues.Thin;
				Col                                                   += 1;
			}
			else
			{
				Range = Worksheet.Range( Row, Col, Row, ( Col + RegionCount ) - 1 );
				Range.Merge();
				//worksheet.Cells[row, col, row, col + regionCount - 1].Merge = true;
				// TODO should this be for the range?
				Worksheet.Cell( Row, Col ).Value                      =  Op;
				Worksheet.Cell( Row, Col ).Style.Border.OutsideBorder =  XLBorderStyleValues.Thin;
				Col                                                   += RegionCount; // Skip to next set
			}
		}

		Range                            = Worksheet.Range( Row, StartingCol, Row, Col - 1 );
		Range.Style.Font.Bold            = true;
		Range.Style.Fill.PatternType     = XLFillPatternValues.Solid;
		Range.Style.Fill.BackgroundColor = XLColor.FromHtml( SECONDARY_HEADER_BG );
		Range.Style.Font.FontColor       = XLColor.White;

		Range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

		Range                            = Worksheet.Range( Row, StartingCol + 1, Row, Col - 1 );
		Range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

		Row                            = 12;
		Worksheet.Cell( Row, 1 ).Value = "Status";

		Col = StartingCol;

		foreach( var Op in ReportSettings.SelectedOperations )
		{
			var Regions = FindRegionsForOperation( Op );

			foreach( var Region in Regions )
			{
				Worksheet.Cell( Row, Col ).Value                      = Region;
				Worksheet.Cell( Row, Col ).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
				++Col;
			}
		}
		var TotalCols = Col;

		Worksheet.Cell( Row, Col ).Value = "TOTAL";

		Range                            = Worksheet.Range( Row, 1, Row, Col );
		Range.Style.Font.Bold            = true;
		Range.Style.Fill.PatternType     = XLFillPatternValues.Solid;
		Range.Style.Fill.BackgroundColor = XLColor.FromHtml( PRIMARY_HEADER_BG );
		Range.Style.Font.FontColor       = XLColor.White;

		var StartingRow = Row;
		++Row;
		StartingCol                                = 1;
		Worksheet.Cell( Row++, StartingCol ).Value = "Not Yet Completed";
		Worksheet.Cell( Row++, StartingCol ).Value = "Futile - Store not Ready";
		Worksheet.Cell( Row++, StartingCol ).Value = "Futile - No stock to Collect";
		Worksheet.Cell( Row++, StartingCol ).Value = "Complete (Picked Up)";
		Worksheet.Cell( Row++, StartingCol ).Value = "Verified";
		Worksheet.Cell( Row++, StartingCol ).Value = "Not Yet Assigned";
		Worksheet.Cell( Row, StartingCol ).Value   = "Total";

		Range                 = Worksheet.Range( Row, StartingCol, Row, TotalCols );
		Range.Style.Font.Bold = true;

		Range                            = Worksheet.Range( StartingRow, StartingCol, Row, TotalCols );
		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

		// Right-hand totals
/*		
 *		TODO Need to fix formulas
			//worksheet.Cells[13, totalCols].Formula = "SUM(B13:F13)";
			string startAddress = ConvertColRowToAddress(2, 13);
			string endAddress = ConvertColRowToAddress(totalCols - 1, 13);
			string formula = "SUM(" + startAddress + ":" + endAddress + ")";
			worksheet.Cell(13, totalCols).Formula = formula;

			//worksheet.Cells[14, totalCols].Formula = "SUM(B14:F14)";
			startAddress = ConvertColRowToAddress(2, 14);
			endAddress = ConvertColRowToAddress(totalCols - 1, 14);
			formula = "SUM(" + startAddress + ":" + endAddress + ")";
			worksheet.Cells[14, totalCols].Formula = formula;

			//worksheet.Cells[15, totalCols].Formula = "SUM(B15:F15)";
			startAddress = ConvertColRowToAddress(2, 15);
			endAddress = ConvertColRowToAddress(totalCols - 1, 15);
			formula = "SUM(" + startAddress + ":" + endAddress + ")";
			worksheet.Cells[15, totalCols].Formula = formula;

			//worksheet.Cells[16, totalCols].Formula = "SUM(B16:F16)";
			startAddress = ConvertColRowToAddress(2, 16);
			endAddress = ConvertColRowToAddress(totalCols - 1, 16);
			formula = "SUM(" + startAddress + ":" + endAddress + ")";
			worksheet.Cells[16, totalCols].Formula = formula;

			//worksheet.Cells[17, totalCols].Formula = "SUM(B17:F17)";
			startAddress = ConvertColRowToAddress(2, 17);
			endAddress = ConvertColRowToAddress(totalCols - 1, 17);
			formula = "SUM(" + startAddress + ":" + endAddress + ")";
			worksheet.Cells[17, totalCols].Formula = formula;

			//worksheet.Cells[18, totalCols].Formula = "SUM(B18:F18)";
			startAddress = ConvertColRowToAddress(2, 18);
			endAddress = ConvertColRowToAddress(totalCols - 1, 18);

			formula = "SUM(" + startAddress + ":" + endAddress + ")";
			worksheet.Cells[18, totalCols].Formula = formula;
			// Bottom totals
			//worksheet.Cells[19, 2].Formula = "SUM(B13:B18)";
			//worksheet.Cells[19, 3].Formula = "SUM(C13:C18)";
			//worksheet.Cells[19, 4].Formula = "SUM(D13:D18)";
			//worksheet.Cells[19, 5].Formula = "SUM(E13:E18)";
			//worksheet.Cells[19, 6].Formula = "SUM(F13:F18)";
			//worksheet.Cells[19, 7].Formula = "SUM(G13:G18)";
	
			for (int i = 2; i <= totalCols; i++)
			{
				startAddress = ConvertColRowToAddress(i, 13);
				endAddress = ConvertColRowToAddress(i, 18);
				formula = "SUM(" + startAddress + ":" + endAddress + ")";
				worksheet.Cells[19, i].Formula = formula;
			}
*/

		//
		// Totals section
		//

		Row                                        += 2;
		StartingRow                                =  Row;
		Worksheet.Cell( Row++, StartingCol ).Value =  "Not Yet Completed";
		Worksheet.Cell( Row++, StartingCol ).Value =  "Stores Attended";
		Worksheet.Cell( Row++, StartingCol ).Value =  "Total";

		Range                 = Worksheet.Range( Row - 1, StartingCol, Row - 1, TotalCols + 1 );
		Range.Style.Font.Bold = true;

		Worksheet.Cell( Row++, StartingCol ).Value = "% To be Completed";
		Worksheet.Cell( Row++, StartingCol ).Value = "% Completed";

		Range                            = Worksheet.Range( StartingRow, StartingCol, Row - 1, TotalCols );
		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

		Range                            = Worksheet.Range( 13, 2, Row - 1, TotalCols );
		Range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

		// Right-hand totals
		/*
					 * TODO Need to fix formulas
					//worksheet.Cells[21, totalCols].Formula = "SUM(B21:F21)";
					startAddress = ConvertColRowToAddress(2, 21);
					endAddress = ConvertColRowToAddress(totalCols - 1, 21);
					formula = "SUM(" + startAddress + ":" + endAddress + ")";
					worksheet.Cells[21, totalCols].Formula = formula;

					//worksheet.Cells[22, totalCols].Formula = "SUM(B22:F22)";
					startAddress = ConvertColRowToAddress(2, 22);
					endAddress = ConvertColRowToAddress(totalCols - 1, 22);
					formula = "SUM(" + startAddress + ":" + endAddress + ")";
					worksheet.Cells[22, totalCols].Formula = formula;

					// Totals row
					//worksheet.Cells[23, 2].Formula = "SUM(B21:B22)";            
					//worksheet.Cells[23, 3].Formula = "SUM(C21:C22)";
					//worksheet.Cells[23, 4].Formula = "SUM(D21:D22)";
					//worksheet.Cells[23, 5].Formula = "SUM(E21:E22)";
					//worksheet.Cells[23, 6].Formula = "SUM(F21:F22)";
					//worksheet.Cells[23, 7].Formula = "SUM(G21:G22)";
					for (int i = 2; i < totalCols; i++)
					{
						startAddress = ConvertColRowToAddress(i, 21);
						endAddress = ConvertColRowToAddress(i, 22);
						formula = "SUM(" + startAddress + ":" + endAddress + ")";
						worksheet.Cells[23, i].Formula = formula;
					}
					startAddress = ConvertColRowToAddress(totalCols, 21);
					endAddress = ConvertColRowToAddress(totalCols, 22);
					formula = "SUM(" + startAddress + ":" + endAddress + ")";
					worksheet.Cells[23, totalCols].Formula = formula;
		*/
		// Percentage totals
		//worksheet.Cells[24, 7].Formula = "SUM(B24:G24)";
		//worksheet.Cells[25, 7].Formula = "SUM(B25:G25)";

		Range                           = Worksheet.Range( 24, 2, 25, TotalCols );
		Range.Style.NumberFormat.Format = "#.##%";
	}

	//private void BuildPickedUpSection()
	//{
	//	worksheet.Cells[ 11, 1 ].Value           = "PickedUp";
	//	worksheet.Cells[ 11, 1 ].Style.Font.Bold = true;

	//	int startingCol = 2;
	//	int row         = 11;
	//	int col         = startingCol;

	//	foreach( string op in rs.SelectedOperations )
	//	{
	//		int regionCount = FindRegionsForOperation( op ).Count;

	//		if( regionCount == 1 )
	//		{
	//			worksheet.Cells[ row, col ].Value = op;
	//			worksheet.Cells[ row, col ].Style.Border.BorderAround( ExcelBorderStyle.Thin );
	//			col += 1;
	//		}
	//		else
	//		{
	//			worksheet.Cells[ row, col, row, col + regionCount - 1 ].Merge = true;
	//			worksheet.Cells[ row, col ].Value                             = op;
	//			worksheet.Cells[ row, col ].Style.Border.BorderAround( ExcelBorderStyle.Thin );
	//			col += regionCount; // Skip to next set
	//		}
	//	}

	//	using( var range = worksheet.Cells[ row, startingCol, row, col - 1 ] )
	//	{
	//		range.Style.Font.Bold        = true;
	//		range.Style.Fill.PatternType = ExcelFillStyle.Solid;
	//		range.Style.Fill.BackgroundColor.SetColor( SecondaryHeaderBg );
	//		range.Style.Font.Color.SetColor( Color.White );

	//		range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

	//		range.Style.Border.BorderAround( ExcelBorderStyle.Thick );
	//	}

	//	using( var range = worksheet.Cells[ row, startingCol + 1, row, col - 1 ] )
	//	{
	//		range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
	//	}

	//	row                             = 12;
	//	worksheet.Cells[ row, 1 ].Value = "Status";

	//	col = startingCol;

	//	foreach( string op in rs.SelectedOperations )
	//	{
	//		List<string> regions = FindRegionsForOperation( op );

	//		foreach( string region in regions )
	//		{
	//			worksheet.Cells[ row, col ].Value = region;
	//			worksheet.Cells.Style.Border.BorderAround( ExcelBorderStyle.Thin );
	//			++col;
	//		}
	//	}
	//	int totalCols = col;

	//	worksheet.Cells[ row, col ].Value = "TOTAL";

	//	using( var range = worksheet.Cells[ row, 1, row, col ] )
	//	{
	//		range.Style.Font.Bold        = true;
	//		range.Style.Fill.PatternType = ExcelFillStyle.Solid;
	//		range.Style.Fill.BackgroundColor.SetColor( PrimaryHeaderBg );
	//		range.Style.Font.Color.SetColor( Color.White );
	//	}

	//	int startingRow = row;
	//	++row;
	//	startingCol                                 = 1;
	//	worksheet.Cells[ row++, startingCol ].Value = "Not Yet Completed";
	//	worksheet.Cells[ row++, startingCol ].Value = "Futile - Store not Ready";
	//	worksheet.Cells[ row++, startingCol ].Value = "Futile - No stock to Collect";
	//	worksheet.Cells[ row++, startingCol ].Value = "Complete (Picked Up)";
	//	worksheet.Cells[ row++, startingCol ].Value = "Verified";
	//	worksheet.Cells[ row++, startingCol ].Value = "Not Yet Assigned";
	//	worksheet.Cells[ row, startingCol ].Value   = "Total";

	//	using( var range = worksheet.Cells[ row, startingCol, row, totalCols ] )
	//	{
	//		range.Style.Font.Bold = true;
	//	}

	//	using( var range = worksheet.Cells[ startingRow, startingCol, row, totalCols ] )
	//	{
	//		range.Style.Border.BorderAround( ExcelBorderStyle.Thick );
	//	}

	//	// Right-hand totals
	//	// TODO need to fix formula
	//	//worksheet.Cells[13, totalCols].Formula = "SUM(B13:F13)";
	//	string startAddress = ConvertColRowToAddress( 2, 13 );
	//	string endAddress   = ConvertColRowToAddress( totalCols - 1, 13 );
	//	string formula      = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 13, totalCols ].Formula = formula;

	//	//worksheet.Cells[14, totalCols].Formula = "SUM(B14:F14)";
	//	startAddress                             = ConvertColRowToAddress( 2, 14 );
	//	endAddress                               = ConvertColRowToAddress( totalCols - 1, 14 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 14, totalCols ].Formula = formula;

	//	//worksheet.Cells[15, totalCols].Formula = "SUM(B15:F15)";
	//	startAddress                             = ConvertColRowToAddress( 2, 15 );
	//	endAddress                               = ConvertColRowToAddress( totalCols - 1, 15 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 15, totalCols ].Formula = formula;

	//	//worksheet.Cells[16, totalCols].Formula = "SUM(B16:F16)";
	//	startAddress                             = ConvertColRowToAddress( 2, 16 );
	//	endAddress                               = ConvertColRowToAddress( totalCols - 1, 16 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 16, totalCols ].Formula = formula;

	//	//worksheet.Cells[17, totalCols].Formula = "SUM(B17:F17)";
	//	startAddress                             = ConvertColRowToAddress( 2, 17 );
	//	endAddress                               = ConvertColRowToAddress( totalCols - 1, 17 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 17, totalCols ].Formula = formula;

	//	//worksheet.Cells[18, totalCols].Formula = "SUM(B18:F18)";
	//	startAddress = ConvertColRowToAddress( 2, 18 );
	//	endAddress   = ConvertColRowToAddress( totalCols - 1, 18 );

	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 18, totalCols ].Formula = formula;

	//	// Bottom totals
	//	//worksheet.Cells[19, 2].Formula = "SUM(B13:B18)";
	//	//worksheet.Cells[19, 3].Formula = "SUM(C13:C18)";
	//	//worksheet.Cells[19, 4].Formula = "SUM(D13:D18)";
	//	//worksheet.Cells[19, 5].Formula = "SUM(E13:E18)";
	//	//worksheet.Cells[19, 6].Formula = "SUM(F13:F18)";
	//	//worksheet.Cells[19, 7].Formula = "SUM(G13:G18)";
	//	for( int i = 2; i <= totalCols; i++ )
	//	{
	//		startAddress                     = ConvertColRowToAddress( i, 13 );
	//		endAddress                       = ConvertColRowToAddress( i, 18 );
	//		formula                          = "SUM(" + startAddress + ":" + endAddress + ")";
	//		worksheet.Cells[ 19, i ].Formula = formula;
	//	}

	//	//
	//	// Totals section
	//	//

	//	row                                         += 2;
	//	startingRow                                 =  row;
	//	worksheet.Cells[ row++, startingCol ].Value =  "Not Yet Completed";
	//	worksheet.Cells[ row++, startingCol ].Value =  "Stores Attended";
	//	worksheet.Cells[ row++, startingCol ].Value =  "Total";

	//	using( var range = worksheet.Cells[ row - 1, startingCol, row - 1, totalCols + 1 ] )
	//	{
	//		range.Style.Font.Bold = true;
	//	}
	//	worksheet.Cells[ row++, startingCol ].Value = "% To be Completed";
	//	worksheet.Cells[ row++, startingCol ].Value = "% Completed";

	//	using( var range = worksheet.Cells[ startingRow, startingCol, row - 1, totalCols ] )
	//	{
	//		range.Style.Border.BorderAround( ExcelBorderStyle.Thick );
	//	}

	//	using( var range = worksheet.Cells[ 13, 2, row - 1, totalCols ] )
	//	{
	//		range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
	//	}

	//	// Right-hand totals
	//	//worksheet.Cells[21, totalCols].Formula = "SUM(B21:F21)";
	//	startAddress                             = ConvertColRowToAddress( 2, 21 );
	//	endAddress                               = ConvertColRowToAddress( totalCols - 1, 21 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 21, totalCols ].Formula = formula;

	//	//worksheet.Cells[22, totalCols].Formula = "SUM(B22:F22)";
	//	startAddress                             = ConvertColRowToAddress( 2, 22 );
	//	endAddress                               = ConvertColRowToAddress( totalCols - 1, 22 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 22, totalCols ].Formula = formula;

	//	// Totals row
	//	//worksheet.Cells[23, 2].Formula = "SUM(B21:B22)";            
	//	//worksheet.Cells[23, 3].Formula = "SUM(C21:C22)";
	//	//worksheet.Cells[23, 4].Formula = "SUM(D21:D22)";
	//	//worksheet.Cells[23, 5].Formula = "SUM(E21:E22)";
	//	//worksheet.Cells[23, 6].Formula = "SUM(F21:F22)";
	//	//worksheet.Cells[23, 7].Formula = "SUM(G21:G22)";
	//	for( int i = 2; i < totalCols; i++ )
	//	{
	//		startAddress                     = ConvertColRowToAddress( i, 21 );
	//		endAddress                       = ConvertColRowToAddress( i, 22 );
	//		formula                          = "SUM(" + startAddress + ":" + endAddress + ")";
	//		worksheet.Cells[ 23, i ].Formula = formula;
	//	}
	//	startAddress                             = ConvertColRowToAddress( totalCols, 21 );
	//	endAddress                               = ConvertColRowToAddress( totalCols, 22 );
	//	formula                                  = "SUM(" + startAddress + ":" + endAddress + ")";
	//	worksheet.Cells[ 23, totalCols ].Formula = formula;

	//	// Percentage totals
	//	//worksheet.Cells[24, 7].Formula = "SUM(B24:G24)";
	//	//worksheet.Cells[25, 7].Formula = "SUM(B25:G25)";

	//	using( var range = worksheet.Cells[ 24, 2, 25, totalCols ] )
	//	{
	//		range.Style.Numberformat.Format = "#.##%";
	//	}
	//}

	/// <summary>
	///     Starts at A28.
	/// </summary>
	private void BuildTripsSection()
	{
		// Build header
		var StartingCol = 1;
		var Row         = 28;
		var Col         = StartingCol;
		var TotalCols   = 17;
		Worksheet.Cell( Row, Col++ ).Value = "Date Entered";
		Worksheet.Cell( Row, Col++ ).Value = "Shipment ID";
		Worksheet.Cell( Row, Col++ ).Value = "Billing Notes";
		Worksheet.Cell( Row, Col++ ).Value = "Driver";
		Worksheet.Cell( Row, Col++ ).Value = "Operation";
		Worksheet.Cell( Row, Col++ ).Value = "Region";
		Worksheet.Cell( Row, Col++ ).Value = "P/U Co";
		Worksheet.Cell( Row, Col++ ).Value = "P/U";
		Worksheet.Cell( Row, Col++ ).Value = "Pcity";
		Worksheet.Cell( Row, Col++ ).Value = "Status";
		Worksheet.Cell( Row, Col++ ).Value = "Days Old";
		Worksheet.Cell( Row, Col++ ).Value = "Days Since Status";
		Worksheet.Cell( Row, Col++ ).Value = "Date of PU";
		Worksheet.Cell( Row, Col++ ).Value = "PU Notes";
		Worksheet.Cell( Row, Col++ ).Value = "Planned PCS";
		Worksheet.Cell( Row, Col++ ).Value = "Actual PCS";
		Worksheet.Cell( Row, Col ).Value   = "Variance";

		var Range = Worksheet.Range( Row, StartingCol, Row, TotalCols );
		Range.Style.Font.Bold            = true;
		Range.Style.Fill.PatternType     = XLFillPatternValues.Solid;
		Range.Style.Fill.BackgroundColor = XLColor.FromHtml( PRIMARY_HEADER_BG );
		Range.Style.Font.FontColor       = XLColor.White;

		Range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

		Range.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

		// Now for the trips
		var StartingRow = 29;
		Row = StartingRow;

		foreach( var Trip in TripsInOrder )
		{
			//Logging.WriteLogLine( "Adding trip: " + trip.TripId + " - CallTime: " + trip.CallTime );
			Worksheet.Cell( Row, 1 ).Value                      = ( Trip.CallTime ?? DateTimeOffset.MinValue ).ToString( "dd/MM/yyyy" );
			Worksheet.Cell( Row, 1 ).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

			Worksheet.Cell( Row, 2 ).Value = Trip.TripId.Trim();

			Worksheet.Cell( Row, 3 ).Value = Trip.BillingNotes.Trim();
			Worksheet.Cell( Row, 4 ).Value = Trip.Driver.Trim();

			var (Op, Region)               = ExtractOperationRegion( Trip.PickupZone );
			Worksheet.Cell( Row, 5 ).Value = Op;
			Worksheet.Cell( Row, 6 ).Value = Region;

			Worksheet.Cell( Row, 7 ).Value  = Trip.PickupCompanyName.Trim();
			Worksheet.Cell( Row, 8 ).Value  = Trip.PickupAddressAddressLine1.Trim();
			Worksheet.Cell( Row, 9 ).Value  = Trip.PickupAddressCity.Trim();
			Worksheet.Cell( Row, 10 ).Value = (int)Trip.Status1;

			var Now = DateTimeOffset.Now;

			var DaysOld = ( Now - ( Trip.CallTime ?? DateTimeOffset.MinValue ) ).Days;
			//int            daysOld = ( now - (trip.CallTime != null ? trip.CallTime : DateTimeOffset.MinValue) ).Days;

			Worksheet.Cell( Row, 11 ).Value = DaysOld;

			if( DaysOld > ReportSettings.SelectedExceedDays )
			{
				Worksheet.Cell( Row, 11 ).Style.Fill.PatternType     = XLFillPatternValues.Solid;
				Worksheet.Cell( Row, 11 ).Style.Fill.BackgroundColor = XLColor.Red;
			}

			var DaysOldStatus = 0;

			switch( Trip.Status1 )
			{
			case STATUS.DISPATCHED:
				// TODO
				break;

			case STATUS.PICKED_UP:
				if( Trip.PickupTime < Now )
				{
					DaysOldStatus = ( Now - ( Trip.PickupTime ?? DateTimeOffset.MinValue ) ).Days;
					//daysOldStatus = ( now - (trip.PickupTime != null ? trip.PickupTime : DateTimeOffset.MinValue) ).Days;
				}
				break;

			case STATUS.VERIFIED:
				if( Trip.VerifiedTime < Now )
					DaysOldStatus = ( Now - Trip.VerifiedTime ).Days;
				break;

			case STATUS.DELETED:
				// TODO
				break;
			}
			Worksheet.Cell( Row, 12 ).Value = DaysOldStatus;

			if( DaysOldStatus > ReportSettings.SelectedExceedDays )
			{
				Worksheet.Cell( Row, 12 ).Style.Fill.PatternType     = XLFillPatternValues.Solid;
				Worksheet.Cell( Row, 12 ).Style.Fill.BackgroundColor = XLColor.Red;
			}

			Worksheet.Cell( Row, 13 ).Value = ( Trip.PickupTime ?? DateTimeOffset.MinValue ).ToString( "dd/MM/yyyy" );
//				worksheet.Cell( row, 12 ).Value = (trip.PickupTime != null ? trip.PickupTime : DateTimeOffset.MinValue).ToString( "dd/MM/yyyy" );
			Worksheet.Cell( Row, 14 ).Value = Trip.PickupNotes;

			//worksheet.Cell( row, 15 ).Value = trip.OriginalPieceCount;
			decimal Original = 0;

			if( Trip.Packages.Count > 0 )
			{
				foreach( var Tp in Trip.Packages )
				{
					if( Tp.Items.Count > 0 )
					{
						foreach( var Item in Tp.Items )
							Original += Item.Original;
					}
				}
			}
			Worksheet.Cell( Row, 15 ).Value = Original;

			Worksheet.Cell( Row, 16 ).Value = Trip.Pieces;
			//worksheet.Cell( row, 17 ).Value = trip.Pieces - trip.OriginalPieceCount;
			Worksheet.Cell( Row, 17 ).Value = Trip.Pieces - Original;

			++Row;
		}
	}
#endregion
}