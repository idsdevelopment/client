﻿using System.Windows.Controls;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.ReportBrowser;

/// <summary>
///     Interaction logic for ReportBrowser.xaml
/// </summary>
public partial class ReportBrowser : Page
{
	public ReportBrowser()
	{
		InitializeComponent();

		//dpFrom.SelectedDate = dpTo.SelectedDate = DateTime.Now;
		dpTo.SelectedDate = DateTime.Now;
	}

	private void DpFrom_SelectedDateChanged( object sender, SelectionChangedEventArgs e )
	{
		if( DataContext is ReportBrowserModel Model && sender is DatePicker dp )
		{
			if( dp.SelectedDate != null )
				Model.SelectedFrom = (DateTimeOffset)dp.SelectedDate;
		}
	}

	private void DpTo_SelectedDateChanged( object sender, SelectionChangedEventArgs e )
	{
		if( DataContext is ReportBrowserModel Model && sender is DatePicker dp )
		{
			if( dp.SelectedDate != null )
				Model.SelectedTo = (DateTimeOffset)dp.SelectedDate;
		}
	}

	private void iudExceedDays_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) )
			e.Handled = true;
		else if( sender is IntegerUpDown iud )
		{
			if( iud.Value < 1 )
			{
				iud.Value = 1;
				e.Handled = true;
			}
		}
	}

	private void BtnSelectNone_Click( object sender, RoutedEventArgs e )
	{
		lbGroups.UnselectAll();
	}

	private void LbReports_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		var report = (ReportSettings)lbReports.SelectedItem;

		if( ( report != null ) && DataContext is ReportBrowserModel Model )
			Model.SelectedReportSettings = report;
	}

	private void LbGroups_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( DataContext is ReportBrowserModel Model )
		{
			Model.SelectedGroupNames.Clear();

			foreach( string item in lbGroups.SelectedItems )
			{
				Logging.WriteLogLine( "Selected group: " + item );
				Model.SelectedGroupNames.Add( item );
			}
		}
	}

	private void BtnRun_Click( object sender, RoutedEventArgs e )
	{
		var errors = new List<string>();

		if( dpFrom.SelectedDate == null && dpFrom.IsVisible )
			errors.Add( (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsFromDate" ) );

		if( dpTo.SelectedDate == null )
			errors.Add( (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsToDate" ) );

		if( ( rbSelect.IsChecked == true ) && ( lbGroups.SelectedItems.Count == 0 ) )
			errors.Add( (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsGroups" ) );

		if( errors.Count > 0 )
		{
			var message = (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsFound" );

			foreach( var error in errors )
				message += "\n\t" + error;

			var title = (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsTitle" );
			MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
		}
		else
		{
			if( DataContext is ReportBrowserModel Model )
				Model.Execute_Run();
		}
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	/// <summary>
	///     Toggles the visibility of the toolbar and menu.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

    private void lbReports_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {

    }
}