﻿using System.Diagnostics;
using ViewModels.ReportBrowser.CreateExcel;
using ViewModels.Reports.WebReports;

namespace ViewModels.ReportBrowser;

internal class ReportBrowserModel : ViewModelBase
{
	public static readonly string PROGRAM = "ReportBrowser (" + Globals.CurrentVersion.APP_VERSION + ")";

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		Dispatcher.Invoke( async () =>
						   {
							   LoadReports();
							   await LoadGroups();
							   await LoadZones();
							   await LoadDrivers();

							   Loaded = true;
						   } );

		//Task.WaitAll(Task.Run(() =>
		//{
		//    Dispatcher.Invoke(() =>
		//    {
		//        LoadReports();
		//        LoadGroups();
		//        LoadZones();

		//        Loaded = true;
		//    });
		//}));
	}


#region Data
	public List<ReportSettings> Reports
	{
		//get { return Get(() => Reports, new SortedSet<ReportSettings>()); }
		get { return Get( () => Reports, new List<ReportSettings>() ); }
		set { Set( () => Reports, value ); }
	}


	public List<string> ReportNames
	{
		get { return Get( () => ReportNames, new List<string>() ); }
		set { Set( () => ReportNames, value ); }
	}


	public ReportSettings SelectedReportSettings
	{
		//get { return Get(() => SelectedReportSettings, new ReportSettings()); }
		get { return Get( () => SelectedReportSettings, new ReportSettings() ); }
		set { Set( () => SelectedReportSettings, value ); }
	}


	[DependsUpon( nameof( SelectedReportSettings ) )]
	public void WhenSelectedReportSettingsChanges()
	{
		if( SelectedReportSettings != null )
		{
			Logging.WriteLogLine( "Setting SelectedReportSettings to " + SelectedReportSettings.Description );

			Dispatcher.Invoke( () =>
							   {
								   IsRunButtonEnabled = true;

								   // Turn on/off the fields
								   ShowSelectFrom        = SelectedReportSettings.ShowSelectFrom;
								   ShowSelectTo          = SelectedReportSettings.ShowSelectTo;
								   ShowSelectPackageType = SelectedReportSettings.ShowSelectPackageType;
								   ShowSelectedDriver    = SelectedReportSettings.ShowSelectDriver;
								   ShowSelectOperation   = SelectedReportSettings.ShowSelectOperation;
								   ShowSelectGroup       = SelectedReportSettings.ShowSelectGroup;
								   ShowFromStatus        = SelectedReportSettings.ShowSelectFromStatus;
								   ShowToStatus          = SelectedReportSettings.ShowSelectToStatus;
								   ShowSelectExceedDays  = SelectedReportSettings.ShowSelectExceedDays;
								   ShowString1	= SelectedReportSettings.ShowString1;

								   SelectedFrom = SelectedTo = DateTimeOffset.Now;
							   } );
		}
		else
			IsRunButtonEnabled = false;
	}

	[DependsUpon( nameof( SelectedReportSettings ) )]
	public bool IsRunButtonEnabled
	{
		get { return Get( () => IsRunButtonEnabled, false ); }
		set { Set( () => IsRunButtonEnabled, value ); }
	}


	public DateTimeOffset SelectedFrom
	{
		get { return Get( () => SelectedFrom, DateTimeOffset.Now ); }
		set { Set( () => SelectedFrom, value ); }
	}


	public bool ShowSelectFrom
	{
		get { return Get( () => ShowSelectFrom, true ); }
		set { Set( () => ShowSelectFrom, value ); }
	}


	public DateTimeOffset SelectedTo
	{
		get { return Get( () => SelectedTo, DateTimeOffset.Now ); }
		set { Set( () => SelectedTo, value ); }
	}


	public bool ShowSelectTo
	{
		get { return Get( () => ShowSelectTo, true ); }
		set { Set( () => ShowSelectTo, value ); }
	}


	public List<string> PackageTypes
	{
		get { return Get( () => PackageTypes, GetPackageTypes ); }
		set { Set( () => PackageTypes, value ); }
	}

	private static List<string> GetPackageTypes()
	{
		var Pts = new List<string>
				  {
					  "Return",
					  "Satchel",
					  "All"
				  };

		return Pts;
	}

	public string SelectedPackageType
	{
		get { return Get( () => SelectedPackageType, ReportSettings.All ); }
		set { Set( () => SelectedPackageType, value ); }
	}


	public bool ShowSelectPackageType
	{
		get { return Get( () => ShowSelectPackageType, true ); }
		set { Set( () => ShowSelectPackageType, value ); }
	}

	public List<string> Drivers
	{
		get { return Get( () => Drivers, new List<string>() ); }
		set { Set( () => Drivers, value ); }
	}

	public string SelectedDriver
	{
		get { return Get( () => SelectedDriver, ReportSettings.All ); }
		set { Set( () => SelectedDriver, value ); }
	}

	public bool ShowSelectedDriver
	{
		get { return Get( () => ShowSelectedDriver, true ); }
		set { Set( () => ShowSelectedDriver, value ); }
	}

	public List<string> Operations
	{
		get { return Get( () => Operations, new List<string>() ); }
		set { Set( () => Operations, value ); }
	}


	public string SelectedOperation
	{
		get { return Get( () => SelectedOperation, "" ); }
		set { Set( () => SelectedOperation, value ); }
	}


	public bool ShowSelectOperation
	{
		get { return Get( () => ShowSelectOperation, true ); }
		set { Set( () => ShowSelectOperation, value ); }
	}


	public List<CompanyAddressGroup> Groups
	{
		get { return Get( () => Groups, new List<CompanyAddressGroup>() ); }
		set { Set( () => Groups, value ); }
	}


	public List<string> GroupNames
	{
		get { return Get( () => GroupNames, new List<string>() ); }
		set { Set( () => GroupNames, value ); }
	}


	/// <summary>
	///     Holds a map of group names and their companies.
	/// </summary>
	private readonly SortedDictionary<string, List<string>> GroupsAndMembers = new();


	public string SelectedGroupName
	{
		get { return Get( () => SelectedGroupName, "" ); }
		set { Set( () => SelectedGroupName, value ); }
	}


	public bool ShowSelectGroup
	{
		get { return Get( () => ShowSelectGroup, true ); }
		set { Set( () => ShowSelectGroup, value ); }
	}


	public bool EnableGroupList
	{
		get { return Get( () => EnableGroupList, false ); }
		set { Set( () => EnableGroupList, value ); }
	}


	public bool IsAllGroupsSelected
	{
		get { return Get( () => IsAllGroupsSelected, true ); }
		set { Set( () => IsAllGroupsSelected, value ); }
	}

	[DependsUpon( nameof( IsAllGroupsSelected ) )]
	public void WhenAllGroupsSelected()
	{
		if( IsAllGroupsSelected )
		{
			SelectedGroupName = ReportSettings.All;
			EnableGroupList   = false;
		}
		else
			EnableGroupList = true;
	}

	public List<string> SelectedGroupNames = new();

	public List<string> FromStatus
	{
		get { return Get( () => FromStatus, GetPublicStatusStrings() ); }
		set { Set( () => FromStatus, value ); }
	}

	private static List<string> GetPublicStatusStrings()
	{
		var Strings = new List<string>
					  {
						  //STATUS.UNSET.AsString(),
						  STATUS.NEW.AsString(),
						  STATUS.ACTIVE.AsString(),
						  STATUS.DISPATCHED.AsString(),

						  //STATUS.PICKED_UP.AsString(),
						  //STATUS.DELIVERED.AsString(),
						  STATUS.VERIFIED.AsString(),

						  //STATUS.POSTED.AsString(),
						  STATUS.INVOICED.AsString(),

						  //STATUS.SCHEDULED.AsString(),
						  //STATUS.PAID.AsString(),
						  STATUS.DELETED.AsString()
					  };

		return Strings;
	}


	public string SelectedFromStatus
	{
		get { return Get( () => SelectedFromStatus, FromStatus[ 0 ] ); }
		set { Set( () => SelectedFromStatus, value ); }
	}


	public bool ShowFromStatus
	{
		get { return Get( () => ShowFromStatus, true ); }
		set { Set( () => ShowFromStatus, value ); }
	}


	public List<string> ToStatus
	{
		get { return Get( () => ToStatus, GetPublicStatusStrings() ); }
		set { Set( () => ToStatus, value ); }
	}


	public string SelectedToStatus
	{
		get { return Get( () => SelectedToStatus, ToStatus[ 3 ] ); }
		set { Set( () => SelectedToStatus, value ); }
	}


	public bool ShowToStatus
	{
		get { return Get( () => ShowToStatus, true ); }
		set { Set( () => ShowToStatus, value ); }
	}


	public int SelectedExceedDays
	{
		get { return Get( () => SelectedExceedDays, 7 ); }
		set { Set( () => SelectedExceedDays, value ); }
	}


	public bool ShowSelectExceedDays
	{
		get { return Get( () => ShowSelectExceedDays, true ); }
		set { Set( () => ShowSelectExceedDays, value ); }
	}


	public Zones Zones
	{
		get { return Get( () => Zones, new Zones() ); }
		set { Set( () => Zones, value ); }
	}


	public List<string> ZoneNames
	{
		get { return Get( () => ZoneNames, new List<string>() ); }
		set { Set( () => ZoneNames, value ); }
	}


	public Zone SelectedZone
	{
		get { return Get( () => SelectedZone, new Zone() ); }
		set { Set( () => SelectedZone, value ); }
	}


	public bool ShowSelectZone
	{
		get { return Get( () => ShowSelectZone, true ); }
		set { Set( () => ShowSelectZone, value ); }
	}

	public string SelectedString1
	{
		get { return Get(() => SelectedString1, ""); }
		set { Set(() => SelectedString1, value); }
	}

	public bool ShowString1
	{
		get { return Get(() => ShowString1, true); }
		set { Set(() => ShowString1, value); }
	}

	#endregion

	#region DataMethods
	/// <summary>
	///     Starts the process of loading all group lists.
	/// </summary>
	/// <returns></returns>
	private async Task LoadGroups()
	{
		Logging.WriteLogLine( "Loading groups" );
		var Ags = new List<CompanyAddressGroup>();

		CompanyAddressGroups Cags = null;

		try
		{
			Cags = await Azure.Client.RequestGetCompanyAddressGroups();
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown: " + E );
		}

		if( Cags is not null )
		{
			foreach( var Cag in Cags )
				Ags.Add( Cag );

			//Logging.WriteLogLine( "Found " + ags.Count + " groups" );

			Groups = Ags;
			await LoadGroupNames();
			return;
		}

		Groups = Ags;
	}


	private async Task LoadGroupNames()
	{
		var Sd     = new SortedDictionary<string, string>();
		var Counts = new SortedDictionary<string, int>();
		GroupsAndMembers.Clear();

		foreach( var Cag in Groups )
		{
			if( !Sd.ContainsKey( Cag.Description.ToLower() ) )
			{
				Sd.Add( Cag.Description.ToLower(), Cag.Description );

				// Now get the number of companies in this group
				try
				{
					var Companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup( Cag.GroupNumber );

					if( Companies.Count > 0 )
					{
						var CompaniesCount = Companies[ 0 ].Companies.Count;
						Counts.Add( Cag.Description.ToLower(), CompaniesCount );

						var Sd1 = new SortedDictionary<string, string>();

						foreach( var Company in Companies[ 0 ].Companies )
						{
							//Logging.WriteLogLine( "Adding: " + company.ToLower() );

							if( !Sd1.ContainsKey( Company.ToLower() ) )
								Sd1.Add( Company.ToLower(), Company );
							else
								Logging.WriteLogLine( "Duplicate found: " + Company );
						}

						var Sorted = new List<string>();

						foreach( var Key in Sd1.Keys )
							Sorted.Add( Sd1[ Key ] );

						if( !GroupsAndMembers.ContainsKey( Cag.Description ) )
							GroupsAndMembers.Add( Cag.Description, Sorted );
					}
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown: " + E );
				}
			}
		}

		var LabelSingle    = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountSingle" );
		var LabelNotSingle = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountNotSingle" );
		var List           = new List<string>();

		foreach( var Key in Sd.Keys )
		{
			var Line = Sd[ Key ] + " \t(" + Counts[ Key ] + " ";

			if( Counts[ Key ] != 1 )
			{
				//line = sd[key] + " \t(" + counts[key] + " members)"; 
				Line += LabelNotSingle + ")";
			}
			else
				Line += LabelSingle + ")";

			List.Add( Line );
		}

		GroupNames = List;

		//if (SelectedAddress == null)
		//{
		//    LoadAddressesGroupNames();
		//    Loaded = true;
		//}
		//else
		//{
		//    Loaded = true;
		//}
	}


	/// <summary>
	///     Needed for Operations.
	/// </summary>
	private async Task LoadZones()
	{
		if( !IsInDesignMode )
		{
			var NewZones     = new List<Zone>();
			var NewZoneNames = new List<string>();

			//ZoneNames.Clear();
			//newZoneNames.Add("All");
			Zones = await Azure.Client.RequestZones();

			if( Zones is {Count: > 0} )
			{
				foreach( var Zone in Zones )

					//newZones.Add(zone);
					NewZoneNames.Add( Zone.Name );
				ZoneNames = NewZoneNames;

				//Zones.AddRange(newZones);
			}
			else
			{
				// Testing
				var Zone = new Zone
						   {
							   Name      = "VWDerrimut-Metro",
							   SortIndex = 0
						   };
				NewZones.Add( Zone );
				NewZoneNames.Add( Zone.Name );

				Zone = new Zone
					   {
						   Name      = "VWDerrimut-Regional",
						   SortIndex = 1
					   };
				NewZones.Add( Zone );
				NewZoneNames.Add( Zone.Name );

				Zone = new Zone
					   {
						   Name      = "SA-Metro",
						   SortIndex = 2
					   };
				NewZones.Add( Zone );
				NewZoneNames.Add( Zone.Name );

				Zone = new Zone
					   {
						   Name      = "SA-Regional",
						   SortIndex = 3
					   };
				NewZones.Add( Zone );
				NewZoneNames.Add( Zone.Name );

				Zone = new Zone
					   {
						   Name      = "TAS-TAS",
						   SortIndex = 4
					   };
				NewZones.Add( Zone );
				NewZoneNames.Add( Zone.Name );

				ZoneNames = NewZoneNames;

				Zones?.AddRange( NewZones );
			}

			var NewOperations = new List<string>();

			foreach( var Zone in ZoneNames )
			{
				var Operation = ExtractOperation( Zone );

				if( Operation.IsNotNullOrWhiteSpace() && !NewOperations.Contains( Operation ) )
					NewOperations.Add( Operation );
			}

			NewOperations.Insert( 0, "All" );
			Operations = NewOperations;
		}
	}

	private static string ExtractOperation( string zone )
	{
		var Operation = string.Empty;

		if( zone.Contains( "-" ) )
		{
			var Pieces = zone.Split( '-' );
			Operation = Pieces[ 0 ];
		}
		else if( zone.Contains( "+" ) )
		{
			var Pieces = zone.Split( '+' );
			Operation = Pieces[ 0 ];
		}

		return Operation;
	}

	/// <summary>
	/// Load Drivers for drop down menu
	/// Used by Driver Manifest Report
	/// </summary>
	/// <returns></returns>
	//[DependsUpon(nameof(SelectedReportSettings))]
	private async Task LoadDrivers()
	{
		Logging.WriteLogLine( "Loading drivers" );

		var trucks = new List<string>() {"All"};

		try
		{
			var Names = await Azure.Client.RequestGetDrivers();

			if( Names is not null )
			{
				var CoNames = ( from N in Names
							    let S = N.StaffId
							    orderby S
							    select S ).ToList();

				trucks.AddRange( CoNames );
			}
			Drivers = trucks;
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown: " + E );
		}

		SelectedDriver = Drivers[ 0 ].ToString();
	}

	/// <summary>
	///     TODO Move into db.
	/// </summary>
	private void LoadReports()
	{
		var ListReports = new List<ReportSettings>();

		// If account is PML...
		if( Globals.Company.CarrierId.Contains( "PML", StringComparison.OrdinalIgnoreCase ) )
		{
			var Report = CreateLinfoxReport();

			ListReports.Add( Report );
		}

		//ReportNames.Add(SelectedReportSettings.Description);

		// Phoenix Reports
		//if( Globals.Company.CarrierId.Equals( "phoenix", StringComparison.OrdinalIgnoreCase ) )
		if( Globals.Company.CarrierId.StartsWith( "phoenix", StringComparison.OrdinalIgnoreCase ) || Globals.Company.CarrierId.ToLower() == "christopher" || Globals.Company.CarrierId.ToLower() == "rowan")
		{
			var DM = CreatePhoenixDriverManifest();
			ListReports.Add( DM );

			var DATR = CreatePhoenixDriverArriveTimeReport();
			ListReports.Add( DATR );

			var CT = CreatePhoenixCallTakerReport();
			ListReports.Add( CT );

			var WB = CreatePhoenixWaybillReport();
			ListReports.Add(WB);
		}

		Reports = ListReports;
	}

	private ReportSettings CreateLinfoxReport()
	{
		Logging.WriteLogLine( "Creating Shipment Report" );

		var Report = new ReportSettings
					 {
						 Description = (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsLinfox" ),
						 Zones       = Zones
					 };

		return Report;
	}

	private ReportSettings CreatePhoenixDriverManifest()
	{
		Logging.WriteLogLine( "Creating Shipment Report: Driver Manifest" );

		var Report = new ReportSettings
					 {
						 Description           = (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsDriverManifest" ),
						 ShowSelectExceedDays  = false,
						 ShowSelectFromStatus  = false,
						 ShowSelectToStatus    = false,
						 ShowSelectGroup       = false,
						 ShowSelectOperation   = false,
						 ShowSelectPackageType = false,
						 ShowSelectDriver      = true,
						ShowString1 = false
		};

		return Report;
	}

	private ReportSettings CreatePhoenixDriverArriveTimeReport()
	{
		Logging.WriteLogLine( "Creating Shipment Report: DriverArriveTimeReport" );

		var Report = new ReportSettings
					 {
						 Description           = (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsDriverArriveTimeReport" ),
						 ShowSelectExceedDays  = false,
						 ShowSelectFromStatus  = false,
						 ShowSelectToStatus    = false,
						 ShowSelectGroup       = false,
						 ShowSelectOperation   = false,
						 ShowSelectPackageType = false,
						 ShowSelectDriver      = true,
						 ShowString1 = false
		};

		return Report;
	}

	private ReportSettings CreatePhoenixCallTakerReport()
	{
		Logging.WriteLogLine("Creating Shipment Report: CallTakerReport");

		var Report = new ReportSettings
		{
			Description = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsCallTakerReport"),
			ShowSelectExceedDays = false,
			ShowSelectFromStatus = false,
			ShowSelectToStatus = false,
			ShowSelectGroup = false,
			ShowSelectOperation = false,
			ShowSelectPackageType = false,
			ShowSelectDriver = false,
			ShowString1 = false
		};

		return Report;
	}

	private ReportSettings CreatePhoenixWaybillReport()
	{
		Logging.WriteLogLine("Creating Shipment Report: WaybillReport");

		var Report = new ReportSettings
		{
			Description = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsWaybillReport"),
			ShowSelectFrom = false,
			ShowSelectTo = false,
			ShowSelectExceedDays = false,
			ShowSelectFromStatus = false,
			ShowSelectToStatus = false,
			ShowSelectGroup = false,
			ShowSelectOperation = false,
			ShowSelectPackageType = false,
			ShowSelectDriver = false
		};

		return Report;
	}
	#endregion

	#region Actions
	public void Execute_Clear()
	{
		Logging.WriteLogLine( "Clearing the tab" );

		SelectedReportSettings = null;

		SelectedExceedDays  = 7;
		SelectedFrom        = DateTimeOffset.Now;
		SelectedFromStatus  = FromStatus[ 0 ];
		SelectedGroupName   = string.Empty;
		SelectedOperation   = Operations[ 0 ];
		SelectedPackageType = PackageTypes[ 0 ];
		SelectedDriver      = Drivers[ 0 ];
		SelectedTo          = DateTimeOffset.Now;
		SelectedToStatus    = ToStatus[ 0 ];
		SelectedString1 = string.Empty;
	}

	public async void Execute_Run()
	{
		Logging.WriteLogLine( "Running the report" );

		if( !IsAllGroupsSelected && ( SelectedGroupNames.Count == 0 ) )
			Logging.WriteLogLine( "No group selected" );
		else
		{
			if( SelectedReportSettings is not null )
			{
				SelectedReportSettings.Zones = new List<Zone>();
				SelectedReportSettings.Zones.AddRange( Zones );

				SelectedReportSettings.SelectedFrom        = SelectedFrom;
				SelectedReportSettings.SelectedTo          = SelectedTo.AddDays( 1 ); // Set it to the beginning of the next day (12:00 am) // This can/is also done in reports themselves by using EndOfDay()
				SelectedReportSettings.SelectedPackageType = SelectedPackageType;
				SelectedReportSettings.SelectedDriver      = SelectedDriver;
				SelectedReportSettings.String1 = SelectedString1;

				SelectedReportSettings.SelectedOperations.Clear();

				if( SelectedOperation == ReportSettings.All )
				{
					SelectedReportSettings.SelectedOperations.AddRange( Operations );
					SelectedReportSettings.SelectedOperations.Remove( ReportSettings.All );
				}
				else
					SelectedReportSettings.SelectedOperations.Add( SelectedOperation );

				SelectedReportSettings.SelectedGroups.Clear();

				if( IsAllGroupsSelected )
					SelectedReportSettings.SelectedGroups.Add( ReportSettings.All );
				else
					SelectedReportSettings.SelectedGroups.AddRange( SelectedGroupNames );

				SelectedReportSettings.SelectedFromStatus = SelectedFromStatus;
				SelectedReportSettings.SelectedToStatus   = SelectedToStatus;

				Logging.WriteLogLine( "Report: " + SelectedReportSettings );

				//Dispatcher.Invoke(() =>
				//{
				//    ReportDialog rd = new ReportDialog(SelectedReportSettings);
				//    rd.Show();
				//});

				if( SelectedReportSettings.Description == (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsLinfox" ) )
					await RunLinfoxShipmentReport( SelectedReportSettings );

				if( SelectedReportSettings.Description == (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsDriverManifest" ) )
				{
					// This report already uses end of chosen To day. Undo correction done above. 
					// Is that correction used for Linfox? It would be better not to go adding days until generating the report. 
					SelectedReportSettings.SelectedTo = SelectedTo;
					RunDriverManifestShipmentReport( SelectedReportSettings );
				}

				if( SelectedReportSettings.Description == (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsDriverArriveTimeReport" ) )
				{
					// This report already uses end of chosen To day. Undo correction done above. 
					// Is that correction used for Linfox? It would be better not to go adding days until generating the report. 
					SelectedReportSettings.SelectedTo = SelectedTo;
					RunDriverArriveTimeReport( SelectedReportSettings );
				}

				if (SelectedReportSettings.Description == (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsCallTakerReport"))
				{ 
					RunCallTakerReport(SelectedReportSettings);
				}

				if (SelectedReportSettings.Description == (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsWaybillReport"))
				{ 
					RunWaybillReport(SelectedReportSettings);
				}
			}
		}
	}

	/// <summary>
	///     This doesn't load a report in ReportDialog - it creates an Excel
	///     spreadsheet on disk.
	/// </summary>
	/// <param
	///     name="rs">
	/// </param>
	private static async Task RunLinfoxShipmentReport( ReportSettings rs )
	{
		Logging.WriteLogLine( "Running Linfox shipment report" );

		var Rpt = await new CreateLinfoxShipmentReport().Load();

		var (Success, Errors, FileName) = Rpt.Run( rs );
		string Title;
		string Message;
		var    Mbi = MessageBoxImage.Information;

		if( Success )
		{
			Logging.WriteLogLine( "Created report in " + FileName );

			// TODO inform user
			Title   =  (string)Application.Current.TryFindResource( "ReportBrowserReportsReportCreatedTitle" );
			Message =  (string)Application.Current.TryFindResource( "ReportBrowserReportsReportCreated" );
			Message += "\n" + FileName + "\n\n" + (string)Application.Current.TryFindResource( "ReportBrowserReportsReportCreatedOpen" );
		}
		else
		{
			var Tmp = string.Join( ", ", Errors );
			Logging.WriteLogLine( "Errors: " + Tmp );

			// TODO inform user
			Title   = (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsTitle" );
			Message = (string)Application.Current.TryFindResource( "ReportBrowserReportsErrorsFound" );

			foreach( var Line in Errors )
				Message += "\n\t" + Line;
			Mbi = MessageBoxImage.Error;
		}

		Dispatcher.Invoke( () =>
						   {
							   if( !Success )
							   {
								   MessageBox.Show( Message, Title, MessageBoxButton.OK, Mbi );
							   }
							   else
							   {
								   MessageBoxResult mbr = MessageBox.Show( Message, Title, MessageBoxButton.YesNo, MessageBoxImage.Question );

								   if( mbr == MessageBoxResult.Yes )
								   {
									   Logging.WriteLogLine( "Opening report " + FileName );
									   using Process fileOpener = new();

									   fileOpener.StartInfo.FileName  = "explorer";
									   fileOpener.StartInfo.Arguments = "\"" + FileName + "\"";
									   fileOpener.Start();
								   }
							   }
						   } );
	}

	/// <summary>
	/// This generates a web report and opens it in a browser
	/// (Embeded Display TBD)
	/// </summary>
	/// <param name="rs"></param>
	private static void RunDriverManifestShipmentReport( ReportSettings rs )
	{
		Logging.WriteLogLine( "Running Driver Manifest shipment report" );
		var Model = new WebReportsModel();
		Model.ReportBrowserExecuteDriverManifest( rs );
	}

	/// <summary>
	/// Generate a webreport called driver arrive time report
	/// Displays in browser
	/// </summary>
	/// <param name="rs"></param>
	/// <returns></returns>
	private static void RunDriverArriveTimeReport( ReportSettings rs )
	{
		Logging.WriteLogLine( "Running Driver Arrive Time shipment report" );
		var Model = new WebReportsModel();
		Model.ReportBrowserExecuteDriverArriveTimeReport( rs );
	}

	/// <summary>
	/// Generate a webreport called call taker report
	/// Displays in browser
	/// </summary>
	/// <param name="rs"></param>
	/// <returns></returns>
	private static void RunCallTakerReport(ReportSettings rs)
	{
		Logging.WriteLogLine("Running Call Taker shipment report");
		var Model = new WebReportsModel();
		Model.ReportBrowserExecuteCallTakerReport(rs);
	}

	/// <summary>
	/// Generate a webreport called waybill report
	/// Displays in browser
	/// </summary>
	/// <param name="rs"></param>
	/// <returns></returns>
	private static void RunWaybillReport(ReportSettings rs)
	{
		Logging.WriteLogLine("Running Waybill shipment report");
		var Model = new WebReportsModel();
		Model.ReportBrowserExecuteWaybillReport(rs);
	}

	/// <summary>
	///     TODO - need a URI for ReportBrowser.
	/// </summary>
	public void Execute_ShowHelp()
	{
	}
#endregion
}

/// <summary>
///     Holds the settings for the report fields visibility.
///     TODO - put into db.
/// </summary>
public class ReportSettings
{
	public static readonly string All = "All";

	// They will select Return, Up or All.
	public static readonly List<string> PackageType = new() {"Return", "Up", All};

	public string Description { get; set; }

	public string String1 { get; set; }
	public bool ShowString1 { get; set; }

	public string CompanyCode { get; set; }

	// They will select a “From” and a “To” date from a Calendar Dropdown
	public bool           ShowSelectFrom { get; set; }
	public bool           ShowSelectTo   { get; set; }
	public DateTimeOffset SelectedFrom   { get; set; }
	public DateTimeOffset SelectedTo     { get; set; }

	// They will select Return, Upliftcomplete or All
	public bool   ShowSelectPackageType { get; set; }
	public string SelectedPackageType   { get; set; }

	// They will select 1 Operation or All.
	public bool ShowSelectOperation { get; set; }

	//public string SelectedOperation { get; set; }
	public List<string> SelectedOperations { get; set; }

	// They will select a Group from a dropdown.
	public bool         ShowSelectGroup { get; set; }
	public List<string> SelectedGroups  { get; set; }

	// They will select a driver
	public bool   ShowSelectDriver { get; set; }
	public string SelectedDriver   { get; set; }

	// They will select a From Status
	public bool   ShowSelectFromStatus { get; set; }
	public string SelectedFromStatus   { get; set; }

	// They will select a To Status
	public bool   ShowSelectToStatus { get; set; }
	public string SelectedToStatus   { get; set; }

	// They will enter a number into a field labelled, 'Exceeds Days'. This will default to the number, 7.
	public bool ShowSelectExceedDays { get; set; }
	public int  SelectedExceedDays   { get; set; }

	public List<Zone> Zones { get; set; }

	public override string ToString()
	{
		var Sb = new StringBuilder();
		Sb.Append( "Description: " + Description );
		Sb.Append( ", CompanyCode: " + CompanyCode );
		Sb.Append( ", SelectedFrom: " + SelectedFrom );
		Sb.Append( ", SelectedTo: " + SelectedTo );
		Sb.Append( ", SelectedPackageType: " + SelectedPackageType );
		Sb.Append( ", SelectedDriver: " + SelectedDriver );

		//sb.Append(", SelectedOperation: " + SelectedOperation);
		foreach( var Op in SelectedOperations )
			Sb.Append( ", SelectedOperation: " + Op );

		foreach( var Tmp in SelectedGroups )
			Sb.Append( ", SelectedGroup: " + Tmp );
		Sb.Append( ", SelectedFromStatus: " + SelectedFromStatus );
		Sb.Append( ", SelectedToStatus: " + SelectedToStatus );
		Sb.Append( ", SelectedExceedDays: " + SelectedExceedDays );

		return Sb.ToString();
	}

	/// <summary>
	///     Sets some default settings.
	/// </summary>
	public ReportSettings()
	{
		ShowSelectFrom = ShowSelectTo = ShowSelectPackageType = ShowSelectOperation = ShowString1 = ShowSelectGroup = 
			ShowSelectFromStatus = ShowSelectToStatus = ShowSelectExceedDays = true;
		String1 = "";
		SelectedOperations = new List<string>();
		SelectedGroups     = new List<string>();
		SelectedExceedDays = 7;
	}
}