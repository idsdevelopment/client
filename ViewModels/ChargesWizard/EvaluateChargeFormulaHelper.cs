﻿namespace ViewModels.ChargesWizard;

public static class EvaluateChargeFormulaHelper
{
	private static readonly string INIT_TOTAL = "Total = 0";
	private static readonly string RESULT     = "Result = Total";
	private static readonly string CONNECTOR  = "Else";

	public static decimal EvaluateFullFormula( string formula, decimal pieces, decimal weight, decimal field )
	{
		decimal result = 0;

		//Logging.WriteLogLine("DEBUG formula: " + formula);
		if( formula.Contains( "\n" ) )
		{
			var segments = BreakFormulaIntoParts( formula );

			foreach( var segment in segments )
			{
				//Logging.WriteLogLine("DEBUG segment: " + segment);
				result = EvaluateFormulaPiece( segment, pieces, weight, field );

				if( result > 0 )
					break;
			}
		}
		else
		{
			var parts = formula.Split( ' ' );
			result = CalculateResultSimple( parts, pieces, weight, field );
		}

		return result;
	}

	public static decimal EvaluateFormulaPiece( string formula, decimal pieces, decimal weight, decimal field )
	{
		decimal result = 0;
		/*
		 Total = 0
		 If Weight < 3 And Pieces = 1 
		 Total = 5
		 Endif
		 Result = Total
		 */
		var isConditionTrue = false;
		var lines           = formula.Split( '\n' );

		// Skip over the variable declaration
		for( var i = 1; i < lines.Length; i++ )
		{
			var line = lines[ i ].Trim();

			//Logging.WriteLogLine("DEBUG line: " + line);
			if( line.ToLower().StartsWith( "if" ) )
			{
				isConditionTrue = EvaluateConditionLine( line, pieces, weight, field );
				continue;
			}

			if( line.ToLower().StartsWith( "total" ) )
			{
				if( isConditionTrue )
					result = CalculateResult( line, pieces, weight, field );
				else
				{
					Logging.WriteLogLine( "DEBUG Condition was false" );
					break;
				}
			}
		}

		return result;
	}

	public static List<string> BreakFormulaIntoParts( string formula )
	{
		List<string> list = new();

		Logging.WriteLogLine( "DEBUG formula: " + formula );
		// "Total = 0\nIf Pieces <= 5 \nTotal = 10\nEndif\n Else If Pieces <= 10 \nTotal = 20\nEndif\nResult = Total"
		var segments = formula.Split( new[] {CONNECTOR}, StringSplitOptions.None );

		foreach( var segment in segments )
		{
			var seg = segment.Trim();

			if( !seg.StartsWith( INIT_TOTAL ) )
				seg = INIT_TOTAL + "\n" + seg;

			if( !seg.Contains( RESULT ) )
				seg += "\n" + RESULT;

			Logging.WriteLogLine( "DEBUG segment: " + seg );
			list.Add( seg );
		}

		return list;
	}

	private static bool EvaluateConditionLine( string condition, decimal pieces, decimal weight, decimal field )
	{
		var isTrue = false;

		var parts = condition.Split( ' ' );

		if( parts.Length == 4 )
		{
			// If Weight < 3
			isTrue = EvaluateCondition( parts[ 1 ], parts[ 2 ], parts[ 3 ], pieces, weight, field );
		}
		else if( parts.Length == 8 )
		{
			// If Weight < 3 And Pieces = 1 
			var first  = EvaluateCondition( parts[ 1 ], parts[ 2 ], parts[ 3 ], pieces, weight, field );
			var second = EvaluateCondition( parts[ 5 ], parts[ 6 ], parts[ 7 ], pieces, weight, field );
			isTrue = parts[ 4 ].ToLower() == "and" ? first && second : first || second;
		}
		else
			Logging.WriteLogLine( "ERROR can't break the condition line into 4 or 8 pieces: " + condition );

		return isTrue;
	}

	private static bool EvaluateCondition( string pricingObject, string condition, string stringValue, decimal pieces, decimal weight, decimal field )
	{
		var isTrue = false;

		if( decimal.TryParse( stringValue, out var value ) )
		{
			Logging.WriteLogLine( "DEBUG Found value: " + value );

			switch( condition )
			{
			case ">":
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces > value;
					break;

				case "Weight":
					isTrue = weight > value;
					break;

				case "Field":
					isTrue = field > value;
					break;
				}
				break;

			case ">=":
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces >= value;
					break;

				case "Weight":
					isTrue = weight >= value;
					break;

				case "Field":
					isTrue = field >= value;
					break;
				}
				break;

			case "=":
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces == value;
					break;

				case "Weight":
					isTrue = weight == value;
					break;

				case "Field":
					isTrue = field == value;
					break;
				}
				break;

			case "<=":
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces <= value;
					break;

				case "Weight":
					isTrue = weight <= value;
					break;

				case "Field":
					isTrue = field <= value;
					break;
				}
				break;

			case "<":
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces < value;
					break;

				case "Weight":
					isTrue = weight < value;
					break;

				case "Field":
					isTrue = field < value;
					break;
				}
				break;
			}
		}
		else
			Logging.WriteLogLine( "ERROR Can't convert : " + stringValue );

		return isTrue;
	}

	private static decimal CalculateResult( string formula, decimal pieces, decimal weight, decimal field )
	{
		decimal answer = 0;

		var parts = formula.Split( ' ' );

		if( ( parts.Length == 7 ) || ( parts.Length == 5 ) )
			answer = CalculateResultSimple( parts, pieces, weight, field );
		else if( parts.Length > 7 )
			answer = CalculateResultComplex( parts, pieces, weight, field );

		return answer;
	}

	private static decimal CalculateResultSimple( string[] parts, decimal pieces, decimal weight, decimal field )
	{
		decimal answer = 0;

		// Simple formula
		switch( parts.Length )
		{
		case 5:
			// 0      1 2      3 4
			// Result = Weight * 0.50
			if( decimal.TryParse( parts[ 4 ], out var value ) )
				answer = PerformOperation( parts[ 2 ].Trim(), parts[ 3 ].Trim(), pieces, weight, field, value );
			else
				Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 4 ].Trim() );
			break;

		case 7:
			// 0      1 2       3 4  5 6
			// Result = (Pieces - 1) * 0.50
			var pricingObject = parts[ 2 ].Trim().Replace( "(", "" );
			var op            = parts[ 3 ].Trim();
			var part          = parts[ 4 ].Trim().Replace( ")", "" );

			if( decimal.TryParse( part, out value ) )
			{
				// Pieces - 1
				var newCount = PerformOperation( pricingObject, op, pieces, weight, field, value );

				if( decimal.TryParse( parts[ 6 ], out value ) )
					answer = PerformOperation( pricingObject, parts[ 5 ].Trim(), pieces, weight, field, value, newCount );
				else
					Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 6 ].Trim() );
			}
			else
				Logging.WriteLogLine( "ERROR This isn't a number: " + part );
			break;
		}

		return answer;
	}

	private static decimal PerformOperation( string pricingObject, string op, decimal pieces, decimal weight, decimal field, decimal value, decimal newCountForCalc = -1 )
	{
		decimal answer = 0;

		//Logging.WriteLogLine("DEBUG Found value: " + value);
		switch( pricingObject )
		{
		case "Pieces":
			answer = newCountForCalc == -1 ? SimpleCalc( pieces, op, value ) : SimpleCalc( newCountForCalc, op, value );
			break;

		case "Weight":
			answer = newCountForCalc == -1 ? SimpleCalc( weight, op, value ) : SimpleCalc( newCountForCalc, op, value );
			break;

		case "Field":
			answer = newCountForCalc == -1 ? SimpleCalc( field, op, value ) : SimpleCalc( newCountForCalc, op, value );
			break;
		}

		return answer;
	}

	private static decimal CalculateResultComplex( string[] parts, decimal pieces, decimal weight, decimal mileage )
	{
		decimal answer    = 0;
		decimal basePrice = 0;

		if( decimal.TryParse( parts[ 2 ].Trim(), out var value ) )
		{
			//Logging.WriteLogLine("DEBUG basePrice: " + value);
			basePrice = value;
		}
		else
			Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 2 ].Trim() );

		if( parts[ 4 ].IndexOf( "((" ) == -1 )
		{
			switch( parts.Length )
			{
			case 9:
				//    0     1 2  3 4       5 6  7 8
				//  9 Total = 10 + (Pieces - 1) * 0.5
				var pricingObject = parts[ 4 ].Substring( 1 );
				var op            = parts[ 5 ];
				var part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );
					Logging.WriteLogLine( "DEBUG answer: " + answer );
					op   = parts[ 7 ];
					part = parts[ 8 ].Trim();

					if( decimal.TryParse( part, out value ) )
						answer = SimpleCalc( answer, op, value );
				}
				break;

			case 11:
				//    0     1 2  3 4        5 6    7 8        9 0
				// 11 Total = 10 + (Pieces  * 0.5) + (Weight * 0.5)
				pricingObject = parts[ 4 ].Substring( 1 );
				op            = parts[ 5 ];
				part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

					// Second clause
					// + (Weight * 0.5)
					pricingObject = parts[ 8 ].Substring( 1 );
					op            = parts[ 9 ];
					part          = parts[ 10 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						var clause = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

						op     = parts[ 7 ];
						answer = SimpleCalc( answer, op, clause );
					}
				}
				break;

			case 13:
				//    0     1 2  3 4       5 6    7 8        9 0  1 2
				// 13 Total = 10 + (Pieces * 0.5) + ((Weight - 1) * 0.5)
				pricingObject = parts[ 4 ].Substring( 1 );
				op            = parts[ 5 ];
				part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

					// Second clause
					// + ((Weight - 1) * 0.5)
					pricingObject = parts[ 8 ].Substring( 2 );
					op            = parts[ 9 ];
					part          = parts[ 10 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						var clause = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

						op   = parts[ 11 ];
						part = parts[ 12 ].Replace( ")", "" );

						if( decimal.TryParse( part, out value ) )
						{
							clause = SimpleCalc( clause, op, value );

							op     = parts[ 7 ];
							answer = SimpleCalc( answer, op, clause );
						}
					}
				}
				break;
			}
		}
		else
		{
			switch( parts.Length )
			{
			case 13:
				//    0     1 2  3 4        5 6  7 8     9 0       1 2
				// 13 Total = 10 + ((Pieces - 1) * 0.26) + (Weight * 0.5)
				var pricingObject = parts[ 4 ].Substring( 1 );
				var op            = parts[ 5 ];
				var part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

					op   = parts[ 7 ];
					part = parts[ 8 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						answer = SimpleCalc( answer, op, value );

						// Second clause
						// + (Weight * 0.5)
						pricingObject = parts[ 10 ].Substring( 1 );
						op            = parts[ 11 ];
						part          = parts[ 12 ].Replace( ")", "" );

						if( decimal.TryParse( part, out value ) )
						{
							var clause = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

							op     = parts[ 9 ];
							answer = SimpleCalc( answer, op, clause );
						}
					}
				}
				break;

			case 15:
				//    0     1 2  3 4        5 6  7 8     9 0        1 2  3 4
				// 15 Total = 10 + ((Weight - 1) * 0.26) + ((Pieces - 1) * 5)
				pricingObject = parts[ 4 ].Substring( 2 );
				op            = parts[ 5 ];
				part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

					op   = parts[ 7 ];
					part = parts[ 8 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						answer = SimpleCalc( answer, op, value );

						// Second clause
						//  9 0        1 2  3 4
						//  + ((Pieces - 1) * 5)
						pricingObject = parts[ 10 ].Substring( 2 );
						op            = parts[ 11 ];
						part          = parts[ 12 ].Replace( ")", "" );

						if( decimal.TryParse( part, out value ) )
						{
							var clause = CalcPiecesWeightField( pricingObject, pieces, weight, mileage, op, value );

							op   = parts[ 13 ];
							part = parts[ 14 ].Replace( ")", "" );

							if( decimal.TryParse( part, out value ) )
							{
								clause = SimpleCalc( clause, op, value );

								op     = parts[ 9 ];
								answer = SimpleCalc( answer, op, clause );
							}
						}
					}
				}
				break;
			}
		}

		answer += basePrice;

		return answer;
	}

	private static decimal CalcPiecesWeightField( string pricingObject, decimal pieces, decimal weight, decimal field, string op, decimal value )
	{
		decimal answer = 0;

		switch( op )
		{
		case "*":
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces * value;
				break;

			case "Weight":
				answer = weight * value;
				break;

			case "Field":
				answer = field * value;
				break;
			}
			break;

		case "+":
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces + value;
				break;

			case "Weight":
				answer = weight + value;
				break;

			case "Field":
				answer = field + value;
				break;
			}
			break;

		case "/":
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces / value;
				break;

			case "Weight":
				answer = weight / value;
				break;

			case "Field":
				answer = field / value;
				break;
			}
			break;

		case "-":
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces - value;
				break;

			case "Weight":
				answer = weight - value;
				break;

			case "Field":
				answer = field - value;
				break;
			}
			break;
		}

		return answer;
	}

	private static decimal SimpleCalc( decimal previous, string op, decimal value )
	{
		var answer = previous;

		switch( op )
		{
		case "*":
			answer *= value;
			break;

		case "+":
			answer += value;
			break;

		case "/":
			answer /= value;
			break;

		case "-":
			answer -= value;
			break;
		}

		return answer;
	}
}