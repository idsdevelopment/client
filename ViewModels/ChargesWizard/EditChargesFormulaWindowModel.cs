﻿namespace ViewModels.ChargesWizard;

internal class EditChargesFormulaWindowModel : ViewModelBase
{
	public ObservableCollection<string> PricingObjects { get; } = new()
	                                                              {
		                                                              FindStringResource( "ChargesWizardEditFormulaComboObjectPieces" ),
		                                                              FindStringResource( "ChargesWizardEditFormulaComboObjectWeight" ),
		                                                              FindStringResource( "ChargesWizardEditFormulaComboObjectField" )
		                                                              //FindStringResource("ChargesWizardEditFormulaComboObjectMileage"),
	                                                              };

	public ObservableCollection<string> Conditions { get; } = new()
	                                                          {
		                                                          ">",
		                                                          ">=",
		                                                          "=",
		                                                          "<=",
		                                                          "<"
	                                                          };

	public ObservableCollection<string> LogicalOps { get; } = new()
	                                                          {
		                                                          " ",
		                                                          //"And",
		                                                          "Or"
	                                                          };

	public ObservableCollection<string> MathOps { get; } = new()
	                                                       {
		                                                       "*",
		                                                       "+",
		                                                       "/",
		                                                       "-"
	                                                       };

	// Note - Took * & / out to simplify formulas
	public ObservableCollection<string> MathOpsWithSpace { get; } = new()
	                                                                {
		                                                                " ",
		                                                                "+",
		                                                                "-"
		                                                                //"*",
		                                                                //"/"
	                                                                };

	public ObservableCollection<string> Parentheses { get; } = new()
	                                                           {
		                                                           " ",
		                                                           "(",
		                                                           ")"
	                                                           };

	public ObservableCollection<string> FormulaPricingObjects { get; } = new()
	                                                                     {
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectPieces" ),
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectPieces" ) + " - ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectPieces" ) + " + ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectPieces" ) + " * ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectPieces" ) + " / ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectWeight" ),
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectWeight" ) + " - ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectWeight" ) + " + ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectWeight" ) + " * ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectWeight" ) + " / ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectField" ),
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectField" ) + " - ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectField" ) + " + ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectField" ) + " * ",
		                                                                     FindStringResource( "ChargesWizardEditFormulaComboObjectField" ) + " / "
		                                                                     //FindStringResource("ChargesWizardComboObjectMileage"),
		                                                                     //FindStringResource("ChargesWizardComboObjectMileage") + " - ",
		                                                                     //FindStringResource("ChargesWizardComboObjectMileage") + " + ",
	                                                                     };


	public bool IsSimpleChecked
	{
		get => Get( () => IsSimpleChecked, true );
		set => Set( () => IsSimpleChecked, value );
	}


	public bool IsComplexChecked
	{
		get => Get( () => IsComplexChecked, false );
		set => Set( () => IsComplexChecked, value );
	}

	public string SelectedSimplePricingObject
	{
		get => Get( () => SelectedSimplePricingObject, "" );
		set => Set( () => SelectedSimplePricingObject, value );
	}


	public string FirstValueVisibility
	{
		get { return Get( () => FirstValueVisibility, "Collapsed" ); }
		set { Set( () => FirstValueVisibility, value ); }
	}


	public string SelectedSimpleFirstValue
	{
		get => Get( () => SelectedSimpleFirstValue, "0" );
		set => Set( () => SelectedSimpleFirstValue, value );
	}


	public string SelectedSimpleMathObject
	{
		get => Get( () => SelectedSimpleMathObject, "" );
		set => Set( () => SelectedSimpleMathObject, value );
	}


	public string SelectedSimpleSecondValue
	{
		get => Get( () => SelectedSimpleSecondValue, "0" );
		set => Set( () => SelectedSimpleSecondValue, value );
	}


	//
	// First Condition
	//

	public string SelectedComplexPricingObject
	{
		get { return Get( () => SelectedComplexPricingObject, FormulaPricingObjects[ 0 ] ); }
		set { Set( () => SelectedComplexPricingObject, value ); }
	}


	public string SelectedComplexCondition
	{
		get { return Get( () => SelectedComplexCondition, Conditions[ 0 ] ); }
		set { Set( () => SelectedComplexCondition, value ); }
	}


	public string SelectedComplexConditionLimit
	{
		get { return Get( () => SelectedComplexConditionLimit, "" ); }
		set { Set( () => SelectedComplexConditionLimit, value ); }
	}

	//
	// First Result
	//

	public string SelectedComplexFirstResultPricingObject
	{
		get { return Get( () => SelectedComplexFirstResultPricingObject, FormulaPricingObjects[ 0 ] ); }
		set { Set( () => SelectedComplexFirstResultPricingObject, value ); }
	}

	public string SelectedComplexFirstResultValue
	{
		get { return Get( () => SelectedComplexFirstResultValue, "" ); }
		set { Set( () => SelectedComplexFirstResultValue, value ); }
	}

	public string ComplexFirstResultValueVisibility
	{
		get { return Get( () => ComplexFirstResultValueVisibility, "Collapsed" ); }
		set { Set( () => ComplexFirstResultValueVisibility, value ); }
	}


	public string SelectedComplexFirstResultMathObject
	{
		get { return Get( () => SelectedComplexFirstResultMathObject, MathOps[ 0 ] ); }
		set { Set( () => SelectedComplexFirstResultMathObject, value ); }
	}


	public string SelectedComplexFirstResultSecondValue
	{
		get { return Get( () => SelectedComplexFirstResultSecondValue, "" ); }
		set { Set( () => SelectedComplexFirstResultSecondValue, value ); }
	}


	//
	// Second Condition
	//

	public string SelectedComplexSecondConditionLogicOp
	{
		get { return Get( () => SelectedComplexSecondConditionLogicOp, "" ); }
		set { Set( () => SelectedComplexSecondConditionLogicOp, value ); }
	}


	public string ComplexSecondConditionVisiblity
	{
		get { return Get( () => ComplexSecondConditionVisiblity, "Collapsed" ); }
		set { Set( () => ComplexSecondConditionVisiblity, value ); }
	}


	public string SelectedComplexSecondPricingObject
	{
		get { return Get( () => SelectedComplexSecondPricingObject, FormulaPricingObjects[ 0 ] ); }
		set { Set( () => SelectedComplexSecondPricingObject, value ); }
	}


	public string SelectedComplexSecondCondition
	{
		get { return Get( () => SelectedComplexSecondCondition, Conditions[ 0 ] ); }
		set { Set( () => SelectedComplexSecondCondition, value ); }
	}


	public string SelectedComplexSecondConditionLimit
	{
		get { return Get( () => SelectedComplexSecondConditionLimit, "" ); }
		set { Set( () => SelectedComplexSecondConditionLimit, value ); }
	}

	//
	// Second Result
	//

	public string SelectedComplexSecondResultPricingObject
	{
		get { return Get( () => SelectedComplexSecondResultPricingObject, FormulaPricingObjects[ 0 ] ); }
		set { Set( () => SelectedComplexSecondResultPricingObject, value ); }
	}

	public string SelectedComplexSecondResultValue
	{
		get { return Get( () => SelectedComplexSecondResultValue, "" ); }
		set { Set( () => SelectedComplexSecondResultValue, value ); }
	}

	public string ComplexSecondResultValueVisibility
	{
		get { return Get( () => ComplexSecondResultValueVisibility, "Collapsed" ); }
		set { Set( () => ComplexSecondResultValueVisibility, value ); }
	}


	public string SelectedComplexSecondResultMathObject
	{
		get { return Get( () => SelectedComplexSecondResultMathObject, MathOps[ 0 ] ); }
		set { Set( () => SelectedComplexSecondResultMathObject, value ); }
	}


	public string SelectedComplexSecondResultSecondValue
	{
		get { return Get( () => SelectedComplexSecondResultSecondValue, "" ); }
		set { Set( () => SelectedComplexSecondResultSecondValue, value ); }
	}

	//
	// Third Condition
	//

	public string SelectedComplexThirdConditionLogicOp
	{
		get { return Get( () => SelectedComplexThirdConditionLogicOp, "" ); }
		set { Set( () => SelectedComplexThirdConditionLogicOp, value ); }
	}

	public string ComplexThirdConditionVisibility
	{
		get { return Get( () => ComplexThirdConditionVisibility, "Collapsed" ); }
		set { Set( () => ComplexThirdConditionVisibility, value ); }
	}

	public string SelectedComplexThirdPricingObject
	{
		get { return Get( () => SelectedComplexThirdPricingObject, FormulaPricingObjects[ 0 ] ); }
		set { Set( () => SelectedComplexThirdPricingObject, value ); }
	}

	public string SelectedComplexThirdCondition
	{
		get { return Get( () => SelectedComplexThirdCondition, Conditions[ 0 ] ); }
		set { Set( () => SelectedComplexThirdCondition, value ); }
	}

	public string SelectedComplexThirdConditionLimit
	{
		get { return Get( () => SelectedComplexThirdConditionLimit, "" ); }
		set { Set( () => SelectedComplexThirdConditionLimit, value ); }
	}

	//
	// Third Result
	//


	public string SelectedComplexThirdResultPricingObject
	{
		get { return Get( () => SelectedComplexThirdResultPricingObject, FormulaPricingObjects[ 0 ] ); }
		set { Set( () => SelectedComplexThirdResultPricingObject, value ); }
	}

	public string SelectedComplexThirdResultValue
	{
		get { return Get( () => SelectedComplexThirdResultValue, "" ); }
		set { Set( () => SelectedComplexThirdResultValue, value ); }
	}

	public string ComplexThirdResultValueVisibility
	{
		get { return Get( () => ComplexThirdResultValueVisibility, "Collapsed" ); }
		set { Set( () => ComplexThirdResultValueVisibility, value ); }
	}


	public string SelectedComplexThirdResultMathObject
	{
		get { return Get( () => SelectedComplexThirdResultMathObject, MathOps[ 0 ] ); }
		set { Set( () => SelectedComplexThirdResultMathObject, value ); }
	}


	public string SelectedComplexThirdResultSecondValue
	{
		get { return Get( () => SelectedComplexThirdResultSecondValue, "" ); }
		set { Set( () => SelectedComplexThirdResultSecondValue, value ); }
	}


	public string CurrentFormula
	{
		get => Get( () => CurrentFormula, "" );
		set => Set( () => CurrentFormula, value );
	}


	public string NewFormula
	{
		get => Get( () => NewFormula, "" );
		set => Set( () => NewFormula, value );
	}


	public string TestPieces
	{
		get { return Get( () => TestPieces, "" ); }
		set { Set( () => TestPieces, value ); }
	}


	public string TestWeight
	{
		get { return Get( () => TestWeight, "" ); }
		set { Set( () => TestWeight, value ); }
	}


	public string TestField
	{
		get { return Get( () => TestField, "" ); }
		set { Set( () => TestField, value ); }
	}


	public decimal TestResult
	{
		get { return Get( () => TestResult, 0 ); }
		set { Set( () => TestResult, value ); }
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	[DependsUpon( nameof( IsComplexChecked ) )]
	public void WhenIsComplexCheckedChanges()
	{
	}

	[DependsUpon( nameof( SelectedSimplePricingObject ) )]
	public void WhenSelectedSimplePricingObjectChanges()
	{
		FirstValueVisibility = SelectedSimplePricingObject.IndexOf( " " ) > -1 ? "Visible" : "Collapsed";
	}


	[DependsUpon( nameof( SelectedComplexFirstResultPricingObject ) )]
	public void WhenSelectedComplexFirstResultPricingObjectChanges()
	{
		ComplexFirstResultValueVisibility = SelectedComplexFirstResultPricingObject.IndexOf( " " ) > -1 ? "Visible" : "Collapsed";
	}

	[DependsUpon( nameof( SelectedComplexSecondConditionLogicOp ) )]
	public void WhenSelectedComplexSecondConditionLogicOpChanges()
	{
		ComplexSecondConditionVisiblity = SelectedComplexSecondConditionLogicOp.IsNotNullOrWhiteSpace() ? "Visible" : "Collapsed";

		//ComplexThirdConditionVisibility = SelectedComplexSecondConditionLogicOp.IsNotNullOrWhiteSpace() ? "Visible" : "Collapsed";
		if( SelectedComplexSecondConditionLogicOp.IsNullOrWhiteSpace() )
			SelectedComplexThirdConditionLogicOp = " ";
	}


	[DependsUpon( nameof( SelectedComplexSecondResultPricingObject ) )]
	public void WhenSelectedComplexSecondResultPricingObjectChanges()
	{
		ComplexSecondResultValueVisibility = SelectedComplexSecondResultPricingObject.IndexOf( " " ) > -1 ? "Visible" : "Collapsed";
	}

	[DependsUpon( nameof( SelectedComplexThirdConditionLogicOp ) )]
	public void WhenSelectedComplexThirdConditionLogicOpChanges()
	{
		ComplexThirdConditionVisibility = SelectedComplexThirdConditionLogicOp.IsNotNullOrWhiteSpace() ? "Visible" : "Collapsed";
	}


	[DependsUpon( nameof( SelectedComplexThirdResultPricingObject ) )]
	public void WhenSelectedComplexThirdResultPricingObjectChanges()
	{
		ComplexThirdResultValueVisibility = SelectedComplexThirdResultPricingObject.IndexOf( " " ) > -1 ? "Visible" : "Collapsed";
	}


#region Actions
	public void LoadFormula( string formula )
	{
		Logging.WriteLogLine( "DEBUG formula: " + formula );

		if( formula.IsNotNullOrEmpty() )
		{
			if( formula.StartsWith( "Result" ) )
			{
				IsSimpleChecked  = true;
				IsComplexChecked = false;
				LoadSimpleFormula( formula );
			}
			else
			{
				IsSimpleChecked  = false;
				IsComplexChecked = true;
				LoadComplexFormula( formula );
			}
		}
	}

	private void LoadSimpleFormula( string formula )
	{
		var pieces = formula.Split( ' ' );

		switch( pieces.Length )
		{
		case 5:
			// 0      1 2      3 4
			// Result = Pieces * 0.50
			SelectedSimplePricingObject = pieces[ 2 ];
			SelectedSimpleFirstValue    = "";
			SelectedSimpleMathObject    = pieces[ 3 ];
			SelectedSimpleSecondValue   = pieces[ 4 ];
			break;

		case 7:
			// 0      1 2       3 4  5 6
			// Result = (Pieces - 1) * 0.50
			var po = pieces[ 2 ] + " " + pieces[ 3 ] + " ";
			po                          = po.Replace( "(", "" );
			SelectedSimplePricingObject = po;
			var fv = pieces[ 4 ];
			fv                        = fv.Replace( ")", "" );
			SelectedSimpleFirstValue  = fv;
			SelectedSimpleMathObject  = pieces[ 5 ];
			SelectedSimpleSecondValue = pieces[ 6 ];
			break;
		}
	}

	private void LoadComplexFormula( string formula )
	{
		Logging.WriteLogLine( "DEBUG Loading\n" + formula );
		var lines           = formula.Split( '\n' );
		var conditionNumber = 0;

		foreach( var line in lines )
		{
			// Skip first line: Total = 0
			if( line == "Total = 0" )
				continue;

			var pieces = line.Split( ' ' );

			// First condition
			//0  1      2  3 
			//If Pieces <= 5
			if( line.StartsWith( "If" ) )
			{
				if( pieces.Length == 4 )
				{
					conditionNumber               = 1;
					SelectedComplexPricingObject  = pieces[ 1 ].Trim();
					SelectedComplexCondition      = pieces[ 2 ].Trim();
					SelectedComplexConditionLimit = pieces[ 3 ].Trim();
				}
				else
					Logging.WriteLogLine( "Error - Wrong number of pieces: " + pieces.Length + " in line: " + line + ", should be 4" );
			}
			else if( line.StartsWith( "Else If" ) )
			{
				// 0    1  2      3  4
				// Else If Pieces <= 10
				if( pieces.Length == 5 )
				{
					if( conditionNumber == 1 )
					{
						// Build second 
						conditionNumber                       = 2;
						SelectedComplexSecondConditionLogicOp = LogicalOps[ 1 ];
						SelectedComplexSecondPricingObject    = pieces[ 2 ].Trim();
						SelectedComplexSecondCondition        = pieces[ 3 ].Trim();
						SelectedComplexSecondConditionLimit   = pieces[ 4 ].Trim();
					}
					else if( conditionNumber == 2 )
					{
						// Build third
						conditionNumber                      = 3;
						SelectedComplexThirdConditionLogicOp = LogicalOps[ 1 ];
						SelectedComplexThirdPricingObject    = pieces[ 2 ].Trim();
						SelectedComplexThirdCondition        = pieces[ 3 ].Trim();
						SelectedComplexThirdConditionLimit   = pieces[ 4 ].Trim();
					}
				}
				else
					Logging.WriteLogLine( "Error - Wrong number of pieces: " + pieces.Length + " in line: " + line + ", should be 5" );
			}
			else if( line.StartsWith( "Total" ) )
			{
				// 0     1 2      3 4
				// Total = Pieces * 0.4
				// 0     1 2       3 4  5 6
				// Total = (Pieces - 1) * 0.4
				switch( conditionNumber )
				{
				case 1:
					if( pieces.Length == 5 )
					{
						// 0     1 2      3 4
						// Total = Pieces * 0.4
						SelectedComplexFirstResultPricingObject = pieces[ 2 ].Trim();
						SelectedComplexFirstResultMathObject    = pieces[ 3 ].Trim();
						SelectedComplexFirstResultSecondValue   = pieces[ 4 ].Trim();
					}
					else if( pieces.Length == 7 )
					{
						// 0     1 2       3 4  5 6
						// Total = (Pieces - 1) * 0.4
						var po = pieces[ 2 ].Replace( "(", "" ).Trim();
						var mo = pieces[ 3 ].Trim();
						SelectedComplexFirstResultPricingObject = po + " " + mo;
						var sv = pieces[ 4 ].Replace( ")", "" ).Trim();
						SelectedComplexFirstResultValue       = sv;
						SelectedComplexFirstResultMathObject  = pieces[ 5 ].Trim();
						SelectedComplexFirstResultSecondValue = pieces[ 6 ].Trim();
					}
					else
						Logging.WriteLogLine( "Error - Wrong number of pieces: " + pieces.Length + " in line: " + line + ", should be 5 or 7" );
					break;

				case 2:
					if( pieces.Length == 5 )
					{
						// 0     1 2      3 4
						// Total = Pieces * 0.4
						SelectedComplexSecondResultPricingObject = pieces[ 2 ].Trim();
						SelectedComplexSecondResultMathObject    = pieces[ 3 ].Trim();
						SelectedComplexSecondResultSecondValue   = pieces[ 4 ].Trim();
					}
					else if( pieces.Length == 7 )
					{
						// 0     1 2       3 4  5 6
						// Total = (Pieces - 1) * 0.4
						var po = pieces[ 2 ].Replace( "(", "" ).Trim();
						var mo = pieces[ 3 ].Trim();
						SelectedComplexSecondResultPricingObject = po + " " + mo;
						var sv = pieces[ 4 ].Replace( ")", "" ).Trim();
						SelectedComplexSecondResultValue       = sv;
						SelectedComplexSecondResultMathObject  = pieces[ 5 ].Trim();
						SelectedComplexSecondResultSecondValue = pieces[ 6 ].Trim();
					}
					else
						Logging.WriteLogLine( "Error - Wrong number of pieces: " + pieces.Length + " in line: " + line + ", should be 5 or 7" );
					break;

				case 3:
					if( pieces.Length == 5 )
					{
						// 0     1 2      3 4
						// Total = Pieces * 0.4
						SelectedComplexThirdResultPricingObject = pieces[ 2 ].Trim();
						SelectedComplexThirdResultMathObject    = pieces[ 3 ].Trim();
						SelectedComplexThirdResultSecondValue   = pieces[ 4 ].Trim();
					}
					else if( pieces.Length == 7 )
					{
						// 0     1 2       3 4  5 6
						// Total = (Pieces - 1) * 0.4
						var po = pieces[ 2 ].Replace( "(", "" ).Trim();
						var mo = pieces[ 3 ].Trim();
						SelectedComplexThirdResultPricingObject = po + " " + mo;
						var sv = pieces[ 4 ].Replace( ")", "" ).Trim();
						SelectedComplexThirdResultValue       = sv;
						SelectedComplexThirdResultMathObject  = pieces[ 5 ].Trim();
						SelectedComplexThirdResultSecondValue = pieces[ 6 ].Trim();
					}
					else
						Logging.WriteLogLine( "Error - Wrong number of pieces: " + pieces.Length + " in line: " + line + ", should be 5 or 7" );
					break;
				}
			}
			else if( line.StartsWith( "Result" ) )
			{
				// Done
				break;
			}
		}
	}

	public string BuildFormula()
	{
		var formula = IsSimpleChecked ? BuildSimpleFormula() : BuildComplexFormula();
		return formula;
	}

	private string BuildSimpleFormula()
	{
		// 0      1 2      3 4
		// Result = Pieces * 0.50
		// 0      1 2       3 4  5 6
		// Result = (Pieces - 1) * 0.50
		var formula = "Result = ";

		if( SelectedSimplePricingObject.IndexOf( " " ) == -1 )
			formula += SelectedSimplePricingObject + " ";
		else
		{
			formula += "(" + SelectedSimplePricingObject;

			if( SelectedSimpleFirstValue.IsNotNullOrWhiteSpace() )
				formula += SelectedSimpleFirstValue + ") ";
			else
				formula += "0) ";
		}

		if( SelectedSimpleSecondValue.IsNotNullOrWhiteSpace() )
			formula += SelectedSimpleMathObject + " " + SelectedSimpleSecondValue;
		else
			formula += SelectedSimpleMathObject + " 0";
		Logging.WriteLogLine( "Formula: " + formula );
		return formula;
	}

	private string BuildComplexFormula()
	{
		var formula = "Total = 0\n";
		// First Condition
		// If Pieces <= 5
		formula += "If " + SelectedComplexPricingObject + " " + SelectedComplexCondition + " " + SelectedComplexConditionLimit + "\n";

		// First Result
		formula += "Total = ";

		if( SelectedComplexFirstResultPricingObject.IndexOf( " " ) == -1 )
			formula += SelectedComplexFirstResultPricingObject + " ";
		else
		{
			formula += "(" + SelectedComplexFirstResultPricingObject;

			if( SelectedComplexFirstResultValue.IsNotNullOrWhiteSpace() )
				formula += SelectedComplexFirstResultValue + ") ";
			else
				formula += "0) ";
		}
		formula += SelectedComplexFirstResultMathObject + " ";

		if( SelectedComplexFirstResultSecondValue.IsNotNullOrWhiteSpace() )
			formula += SelectedComplexFirstResultSecondValue + "\n";
		else
			formula += "0\n";

		// Is there a second condition?
		if( SelectedComplexSecondConditionLogicOp.IsNotNullOrWhiteSpace() )
		{
			formula += "Else If " + SelectedComplexSecondPricingObject + " " + SelectedComplexSecondCondition + " " + SelectedComplexSecondConditionLimit + "\n";

			// Second Result
			formula += "Total = ";

			if( SelectedComplexSecondResultPricingObject.IndexOf( " " ) == -1 )
				formula += SelectedComplexSecondResultPricingObject + " ";
			else
			{
				formula += "(" + SelectedComplexSecondResultPricingObject;

				if( SelectedComplexSecondResultValue.IsNotNullOrWhiteSpace() )
					formula += SelectedComplexSecondResultValue + ") ";
				else
					formula += "0) ";
			}
			formula += SelectedComplexSecondResultMathObject + " ";

			if( SelectedComplexSecondResultSecondValue.IsNotNullOrWhiteSpace() )
				formula += SelectedComplexSecondResultSecondValue + "\n";
			else
				formula += "0\n";

			// Is there a third condition?
			if( SelectedComplexThirdConditionLogicOp.IsNotNullOrWhiteSpace() )
			{
				formula += "Else If " + SelectedComplexThirdPricingObject + " " + SelectedComplexThirdCondition + " " + SelectedComplexThirdConditionLimit + "\n";

				// Third Result
				formula += "Total = ";

				if( SelectedComplexThirdResultPricingObject.IndexOf( " " ) == -1 )
					formula += SelectedComplexThirdResultPricingObject + " ";
				else
				{
					formula += "(" + SelectedComplexThirdResultPricingObject;

					if( SelectedComplexThirdResultValue.IsNotNullOrWhiteSpace() )
						formula += SelectedComplexThirdResultValue + ") ";
					else
						formula += "0) ";
				}
				formula += SelectedComplexThirdResultMathObject + " ";

				if( SelectedComplexThirdResultSecondValue.IsNotNullOrWhiteSpace() )
					formula += SelectedComplexThirdResultSecondValue + "\n";
				else
					formula += "0\n";
			}
		}

		// Finish the conditions and publish the result
		formula += "Endif\nResult = Total";
		Logging.WriteLogLine( "Formula: " + formula );
		return formula;
	}

	public void TestFormula()
	{
		decimal result = 0;
		decimal pieces = -1;
		decimal weight = -1;
		decimal field  = -1;

		if( TestPieces.IsNotNullOrWhiteSpace() && !decimal.TryParse( TestPieces, out pieces ) )
			Logging.WriteLogLine( "Error parsing TestPieces: " + TestPieces );

		if( TestWeight.IsNotNullOrWhiteSpace() && !decimal.TryParse( TestWeight, out weight ) )
			Logging.WriteLogLine( "Error parsing TestWeight: " + TestWeight );

		if( TestField.IsNotNullOrWhiteSpace() && !decimal.TryParse( TestField, out field ) )
			Logging.WriteLogLine( "Error parsing TestField: " + TestField );

		Logging.WriteLogLine( "Testing formula with values: pieces: " + TestPieces + " weight: " + TestWeight + " field: " + TestField );
		var formula = BuildFormula();

		result = EvaluateChargeFormulaHelper.EvaluateFullFormula( formula, pieces, weight, field );

		//if (IsSimpleChecked)
		//{
		//    // 0      1 2      3 4
		//    // Result = Weight * 0.50
		//    // 0      1 2       3 4  5 6 
		//    // Result = (Pieces - 1) * 0.50
		//    // Result = (Field * 0.02) + 10

		//    string[] parts = formula.Split(' ');
		//    switch (parts.Length)
		//    {
		//        case 5:
		//            if (SelectedSimplePricingObject == FindStringResource("ChargesWizardEditFormulaComboObjectPieces"))
		//            {

		//            }
		//            else if (SelectedSimplePricingObject == FindStringResource("ChargesWizardEditFormulaComboObjectWeight"))
		//            {

		//            }
		//            else if (SelectedSimplePricingObject == FindStringResource("ChargesWizardEditFormulaComboObjectField"))
		//            {

		//            }
		//            break;
		//        case 7:

		//            break;
		//        default:
		//            break;
		//    }
		//}
		//else
		//{

		//}

		TestResult = result;
	}
#endregion
}