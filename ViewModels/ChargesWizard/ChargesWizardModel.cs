﻿using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.ChargesWizard;

internal class ChargesWizardModel : ViewModelBase
{
	public string ArePendingChanges
	{
		get
		{
			var pending = ArePendingCharges || ArePendingOverrides || ArePendingGroups;
			Logging.WriteLogLine( "ArePendingChanges: " + pending );

			// return pending;
			var visibility = "Collapsed";

			if( pending )
				visibility = "Visible";

			return visibility;
		}
		set => Set( () => ArePendingChanges, value );
	}

	public bool ArePendingCharges
	{
		get { return Get( () => ArePendingCharges, false ); }
		set { Set( () => ArePendingCharges, value ); }
	}

	public bool ArePendingOverrides
	{
		get { return Get( () => ArePendingOverrides, false ); }
		set { Set( () => ArePendingOverrides, value ); }
	}


	public bool ArePendingGroups
	{
		get { return Get( () => ArePendingGroups, false ); }
		set { Set( () => ArePendingGroups, value ); }
	}

	public ChargesWizard view;

	public void SetView( ChargesWizard cw )
	{
		view = cw;
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public readonly string ACCOUNT_CHARGE_PREFIX = Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX;
    public readonly string SURCHARGE_PREFIX = Globals.DataContext.MainDataContext.SURCHARGE_PREFIX;

    [DependsUpon( nameof( ArePendingCharges ) )]
	[DependsUpon( nameof( ArePendingOverrides ) )]
	[DependsUpon( nameof( ArePendingGroups ) )]
	public void WhenArePendingChanges()
	{
		var pc = ArePendingCharges;
		var po = ArePendingOverrides;
		var pg = ArePendingGroups;

		if( pc || po || pg )
			ArePendingChanges = "Visible";
		else
			ArePendingChanges = "Collapsed";

		Logging.WriteLogLine( "ArePendingChanges: " + ArePendingChanges );

		RaisePropertyChanged( nameof( ArePendingChanges ) );
	}

	public void SaveAll()
	{
		SaveCharges(); // Handles both Charges and Overrides
		ArePendingCharges = ArePendingOverrides = ArePendingGroups = false;
		WhenArePendingChanges();
	}

#region Charges
	public List<Charge> Charges
	{
		get { return Get( () => Charges, new List<Charge>() ); }
		set { Set( () => Charges, value ); }
	}

    public List<Charge> DefaultCharges
    {
        get { return Get(() => DefaultCharges, new List<Charge>()); }
        set { Set(() => DefaultCharges, value); }
    }

    public List<Charge> SurchargeCharges
    {
        get { return Get(() => SurchargeCharges, new List<Charge>()); }
        set { Set(() => SurchargeCharges, value); }
    }


    //public List<Charge> OverridesCharges
    //{
    //    get { return Get(() => OverridesCharges, new List<Charge>()); }
    //    set { Set(() => OverridesCharges, value); }
    //}


    public ObservableCollection<string> ChargesList { get; } = new();

	[DependsUpon( nameof( Charges ) )]
	public void WhenChargesChanges()
	{
		Dispatcher.Invoke( () =>
		                   {
			                   //Logging.WriteLogLine("DEBUG ");
			                   ChargesList.Clear();

			                   //OverridesCharges.Clear();
			                   if( Charges.Count > 0 )
			                   {
				                   short i = 0;

				                   foreach( var c in Charges )
				                   {
					                   c.SortIndex = i++;

					                   // Don't include overrides
					                   if( !c.IsOverride )
						                   ChargesList.Add( FormatCharge( c ) );
					                   //ChargesList.Add(FormatCharge(c));
				                   }
			                   }

			                   GetChargesNames();      // Make sure this is updated
			                   WhenOverridesChanges(); // And this
		                   } );
	}


	public ObservableCollection<string> PredefinedCharges { get; } = new()
	                                                                 {
		                                                                 "",
		                                                                 FindStringResource( "ChargesWizardChargesTabPredefinedChargesSpacer" ),
		                                                                 FindStringResource( "ChargesWizardChargesTabPredefinedChargesPst" ),
		                                                                 FindStringResource( "ChargesWizardChargesTabPredefinedChargesGst" ),
		                                                                 FindStringResource( "ChargesWizardChargesTabPredefinedChargesHst" )
	                                                                 };

	public ObservableCollection<string> ChargeTypes { get; } = new()
	                                                           {
		                                                           "",
		                                                           FindStringResource( "ChargesWizardChargesTabChargeTypeCumulativeAddition" ),
		                                                           FindStringResource( "ChargesWizardChargesTabChargeTypePercentage" ),
		                                                           FindStringResource( "ChargesWizardChargesTabChargeTypeReference" ),
		                                                           FindStringResource( "ChargesWizardChargesTabChargeTypeFormula" )
		                                                           //FindStringResource("ChargesWizardChargesTabChargeTypeOverridePackage"),
		                                                           //FindStringResource("ChargesWizardChargesTabChargeTypeOverrideServiceLevel"),
	                                                           };

	public readonly int CHARGE_TYPE_CUMULATIVE_ADDITION = 0;
	public readonly int CHARGE_TYPE_PERCENTAGE          = 1;
	public readonly int CHARGE_TYPE_REFERENCE           = 2;
	public readonly int CHARGE_TYPE_FORMULA             = 3;

	public ObservableCollection<string> DisplayOptions { get; } = new()
	                                                              {
		                                                              "",
		                                                              FindStringResource( "ChargesWizardChargesTabDisplayOptionsNothing" ),
		                                                              FindStringResource( "ChargesWizardChargesTabDisplayOptionsCheckboxOnly" ),
		                                                              FindStringResource( "ChargesWizardChargesTabDisplayOptionsFieldOnly" ),
		                                                              FindStringResource( "ChargesWizardChargesTabDisplayOptionsCheckboxAndField" ),
		                                                              FindStringResource( "ChargesWizardChargesTabDisplayOptionsTotal" )
	                                                              };

	public Charge Spacer { get; } = new()
	                                {
		                                ChargeId    = "",
		                                Label       = "",
		                                ChargeType  = 0,
		                                DisplayType = 0,
		                                // TODO Default Load from system
		                                Value         = 0,
		                                IsTax         = false,
		                                AlwaysApplied = false
	                                };

	public Charge Pst { get; } = new()
	                             {
		                             ChargeId    = "PST",
		                             Label       = "PST",
		                             ChargeType  = 1,
		                             DisplayType = 4,
		                             // TODO Default Load from system
		                             Value         = 0.05M,
		                             IsTax         = true,
		                             AlwaysApplied = true
	                             };

	public Charge Gst { get; } = new()
	                             {
		                             ChargeId    = "GST",
		                             Label       = "GST",
		                             ChargeType  = 1,
		                             DisplayType = 4,
		                             // TODO Default Load from system
		                             Value         = 0.05M,
		                             IsTax         = true,
		                             AlwaysApplied = true
	                             };

	public Charge Hst { get; } = new()
	                             {
		                             ChargeId    = "HST",
		                             Label       = "HST",
		                             ChargeType  = 1,
		                             DisplayType = 4,
		                             // TODO Default Load from system
		                             Value         = 0.10M,
		                             IsTax         = true,
		                             AlwaysApplied = true
	                             };

	public Dictionary<string, Charge> DictPredefinedCharges { get; } = new();

	public string SelectedPredefinedCharge
	{
		get => Get( () => SelectedPredefinedCharge, "" );
		set => Set( () => SelectedPredefinedCharge, value );
	}

	[DependsUpon( nameof( SelectedPredefinedCharge ) )]
	public void WhenSelectedPredefinedChargeChanges()
	{
		if( DictPredefinedCharges.Count == 0 )
		{
			DictPredefinedCharges.Add( "SPACER", Spacer );
			DictPredefinedCharges.Add( "PST", Pst );
			DictPredefinedCharges.Add( "GST", Gst );
			DictPredefinedCharges.Add( "HST", Hst );
		}

		if( SelectedPredefinedCharge != string.Empty )
		{
			//SelectedCharge = DictPredefinedCharges[SelectedPredefinedCharge];
			var charge = DictPredefinedCharges[ SelectedPredefinedCharge ];
			ClearCharge( true );
			LoadCharge( charge );
		}
	}

	public string SelectedChargeId
	{
		get => Get( () => SelectedChargeId, "" );
		set => Set( () => SelectedChargeId, value );
	}

	[DependsUpon( nameof( SelectedChargeId ) )]
	public void WhenSelectedChargeIdChanges()
	{
		if( SelectedChargeId.Trim().Length > 0 )
		{
			if( IsNewCharge )
			{
				var id = ( from C in Charges
				           where C.ChargeId == SelectedChargeId.Trim()
				           select C.ChargeId ).FirstOrDefault();

				if( ( id != null ) && ( id.Length > 0 ) )
				{
					view.TbChargeId.Foreground = Brushes.Red;
					IsUpdateButtonEnabled      = false;
				}
				else
				{
					view.TbChargeId.Foreground = Brushes.Black;
					IsUpdateButtonEnabled      = true;
				}
			}
		}
	}


	public bool IsUpdateButtonEnabled
	{
		get { return Get( () => IsUpdateButtonEnabled, false ); }
		set { Set( () => IsUpdateButtonEnabled, value ); }
	}


	public string SelectedChargeDisplayLabel
	{
		get => Get( () => SelectedChargeDisplayLabel, "" );
		set => Set( () => SelectedChargeDisplayLabel, value );
	}


	public string SelectedChargeType
	{
		get => Get( () => SelectedChargeType, "" );
		set => Set( () => SelectedChargeType, value );
	}


	public string SelectedDisplayOption
	{
		get => Get( () => SelectedDisplayOption, "" );
		set => Set( () => SelectedDisplayOption, value );
	}


	public string SelectedDefault
	{
		get => Get( () => SelectedDefault, "" );
		set => Set( () => SelectedDefault, value );
	}


	public bool IsDefaultSelected
	{
		get { return Get( () => IsDefaultSelected, true ); }
		set { Set( () => IsDefaultSelected, value ); }
	}


	public string SelectedFormula
	{
		get { return Get( () => SelectedFormula, "" ); }
		set { Set( () => SelectedFormula, value ); }
	}


	public bool IsFormulaSelected
	{
		get { return Get( () => IsFormulaSelected, false ); }
		set { Set( () => IsFormulaSelected, value ); }
	}


	public string SelectedMinimum
	{
		get => Get( () => SelectedMinimum, "0" );
		set => Set( () => SelectedMinimum, value );
	}


	public string SelectedMaximum
	{
		get => Get( () => SelectedMaximum, "0" );
		set => Set( () => SelectedMaximum, value );
	}


	public bool IsTax
	{
		get => Get( () => IsTax, false );
		set => Set( () => IsTax, value );
	}


	public bool AlwaysApply
	{
		get => Get( () => AlwaysApply, false );
		set => Set( () => AlwaysApply, value );
	}


	public bool IsMultiplier
	{
		get => Get( () => IsMultiplier, false );
		set => Set( () => IsMultiplier, value );
	}


	public bool ShowOnWebEntry
	{
		get => Get( () => ShowOnWebEntry, false );
		set => Set( () => ShowOnWebEntry, value );
	}


	public bool ShowOnWaybill
	{
		get => Get( () => ShowOnWaybill, false );
		set => Set( () => ShowOnWaybill, value );
	}


	public bool ExcludeFromPayroll
	{
		get => Get( () => ExcludeFromPayroll, false );
		set => Set( () => ExcludeFromPayroll, value );
	}


	public bool ExcludeFromTaxes
	{
		get => Get( () => ExcludeFromTaxes, false );
		set => Set( () => ExcludeFromTaxes, value );
	}


	public bool ShowToDrivers
	{
		get => Get( () => ShowToDrivers, false );
		set => Set( () => ShowToDrivers, value );
	}


	public bool IsFuelSurcharge
	{
		get => Get( () => IsFuelSurcharge, false );
		set => Set( () => IsFuelSurcharge, value );
	}


	public bool ExcludeFromFuelSurcharge
	{
		get => Get( () => ExcludeFromFuelSurcharge, false );
		set => Set( () => ExcludeFromFuelSurcharge, value );
	}


#region Charges Actions
	public List<Charge> GetCharges()
	{
		List<Charge> charges = new();

		if( !IsInDesignMode )
		{
			Task.Run( () =>
			          {
				          try
				          {
					          Logging.WriteLogLine( "Getting charges..." );
					          var result = Azure.Client.RequestGetAllCharges().Result;
					          Logging.WriteLogLine( "Found " + result.Count + " charges" );

					          // Ensure that the default charges are ignored
					          charges = ( from C in result
										  where !C.ChargeId.Contains(ACCOUNT_CHARGE_PREFIX) && !C.ChargeId.StartsWith(SURCHARGE_PREFIX)
					                      orderby C.SortIndex
					                      select C ).ToList();

							  //Charges = charges;
							  bool checkForBadSortIndex = (from C in charges where C.SortIndex < 0 select C).Any();

							  DefaultCharges = (from C in result
															 where C.ChargeId.Contains(ACCOUNT_CHARGE_PREFIX)
															 orderby C.SortIndex
															 select C).ToList();

                              SurchargeCharges = (from C in result
                                                  where C.ChargeId.Contains(SURCHARGE_PREFIX)
												  orderby C.SortIndex
                                                  select C).ToList();

                              if ( !checkForBadSortIndex )
							  {
								  Charges = charges;
							  }
							  else
							  {
								  Charges = SetSortIndexes( charges );
								  // TODO - Need to save any Default Charges that exist
								  SaveCharges();
							  }
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "Exception thrown: " + e );
							  Dispatcher.Invoke(() =>
							  {
								  MessageBox.Show("Error fetching Charges\n" + e, "Error", MessageBoxButton.OK);
							  });
				          }
			          } );
		}

		return charges;
	}

	private List<Charge> SetSortIndexes( List<Charge> charges )
	{
		List<Charge> sorted = new();
		for (short i = 0; i < charges.Count; i++ )
		{
			Charge c = charges[i];
			c.SortIndex = i;
			sorted.Add( c );
		}

		return sorted;
	}

	private string FormatCharge( Charge charge )
	{
		StringBuilder line = new();
		Logging.WriteLogLine( "Formatting charge " + charge.ChargeId );

		if( charge.IsOverride )
			line.Append( FindStringResource( "ChargesWizardChargesTabLabelOverride" ) ).Append( " - " );

		var snippit = string.Empty;

		if( charge.IsFormula )
		{
			var lines = charge.Formula.Split( '\n' );

			if( lines.Length == 1 ) // Simple
				snippit = lines[ 0 ];
			else
				snippit = lines[ 1 ] + "...";
		}
		var value = charge.IsFormula ? snippit : charge.Value + "";
		_ = line.Append( "[" ).Append( charge.ChargeId ).Append( "] " ).Append( charge.Label ).Append( " (" ).Append( value ).Append( ")" );

		//if (charge.Value > -1)
		//{
		//    _ = line.Append("[").Append(charge.ChargeId).Append("] ").Append(charge.Label).Append(" (").Append(charge.Value).Append(")");
		//}
		//else
		//{
		//    _ = line.Append("[").Append(charge.ChargeId).Append("] ").Append(charge.Label).Append(" (").Append(charge.Formula).Append(")");
		//}

		return line.ToString();
	}

	public void EditCharge()
	{
		if( SelectedCharge != null )
		{
			if( !SelectedCharge.IsOverride )
			{
				ClearCharge();
				LoadCharge( SelectedCharge );
				IsEditingCharge       = true;
				IsUpdateButtonEnabled = true;
			}
			else
			{
				// Switch to Overrides tab and load
				view.tabControl.SelectedIndex = 1;
				SelectedOverrideChargeObject  = SelectedCharge;
				OverridesEdit();
			}
		}
	}

	private void LoadCharge( Charge charge )
	{
		SelectedChargeId           = charge.ChargeId;
		SelectedChargeDisplayLabel = charge.Label;
		SelectedChargeType         = ChargeTypes[ charge.ChargeType + 1 ];
		SelectedDisplayOption      = DisplayOptions[ charge.DisplayType + 1 ];

		if( !charge.IsFormula )
		{
			SelectedDefault                = charge.Value + "";
			view.RbChargeDefault.IsChecked = true;
			view.TbDefault.IsEnabled       = true;
			view.TbFormula.ToolTip         = string.Empty;
			view.TbFormula.IsEnabled       = false;
			IsDefaultSelected              = true;
			IsFormulaSelected              = false;
		}
		else
		{
			SelectedFormula                = charge.Formula;
			view.TbFormula.ToolTip         = charge.Formula;
			view.RbChargeFormula.IsChecked = true;
			view.TbFormula.IsEnabled       = true;
			view.TbDefault.IsEnabled       = false;
			IsDefaultSelected              = false;
			IsFormulaSelected              = true;
		}
		SelectedMinimum = charge.Min + "";
		SelectedMaximum = charge.Max + "";

		IsTax          = charge.IsTax;
		AlwaysApply    = charge.AlwaysApplied;
		IsMultiplier   = charge.IsMultiplier;
		ShowOnWebEntry = charge.DisplayOnEntry;
		ShowOnWaybill  = charge.DisplayOnWaybill;
		//ExcludeFromPayroll = charge.ExcludeFromPayroll; // TODO Not in object
		ExcludeFromTaxes         = charge.ExcludeFromTaxes;
		ExcludeFromFuelSurcharge = charge.ExcludeFromFuelSurcharge;
	}

	public void NewCharge()
	{
		ClearCharge();
		IsEditingCharge       = true;
		IsNewCharge           = true;
		IsUpdateButtonEnabled = true;
	}

	public void ClearCharge( bool dontClearPredefined = false )
	{
		if( !dontClearPredefined )
			SelectedPredefinedCharge = string.Empty;
		SelectedDisplayOption      = string.Empty;
		SelectedChargeId           = string.Empty;
		SelectedChargeDisplayLabel = string.Empty;
		SelectedChargeType         = string.Empty;
		SelectedDisplayOption      = string.Empty;

		SelectedDefault = string.Empty;
		SelectedFormula = string.Empty;
		SelectedMinimum = string.Empty;
		SelectedMaximum = string.Empty;

		IsTax          = false;
		AlwaysApply    = false;
		IsMultiplier   = false;
		ShowOnWebEntry = false;
		ShowOnWaybill  = false;
		//ExcludeFromPayroll = charge.ExcludeFromPayroll; // TODO Not in object
		ExcludeFromTaxes         = false;
		ExcludeFromFuelSurcharge = false;
	}

	public void CancelEditCharge()
	{
		ClearCharge();
		IsEnabled             = false;
		IsEditingCharge       = false;
		IsUpdateButtonEnabled = false;
		AreButtonsEnabled     = true;
	}

	public void UpdateCharge()
	{
		var charge = IsNewCharge ? new Charge() : SelectedCharge;
		charge = BuildCharge( charge );

		if( IsNewCharge )
			Charges.Add( charge );
		else
		{
			var index = Charges.IndexOf( SelectedCharge );
			Charges.Remove( SelectedCharge );
			Charges.Insert( index, charge );
		}

		// Rebuild the list
		WhenChargesChanges();

		ClearCharge();
		IsNewCharge       = IsEditingCharge   = IsUpdateButtonEnabled = false;
		ArePendingCharges = AreButtonsEnabled = true;
		WhenArePendingChanges();
	}

	private Charge BuildCharge( Charge charge )
	{
		charge.ChargeId = SelectedChargeId.Trim();
		//charge.Label = SelectedChargeDisplayLabel.Trim();
		charge.Label = SelectedChargeDisplayLabel.Trim();
		// Need to exclude the blank at the top of the combo
		charge.ChargeType  = (short)( ChargeTypes.IndexOf( SelectedChargeType ) - 1 );
		charge.DisplayType = (short)( DisplayOptions.IndexOf( SelectedDisplayOption ) - 1 );

		if( IsDefaultSelected )
		{
			if( SelectedDefault.Trim().Length > 0 )
			{
				decimal.TryParse( SelectedDefault.Trim(), out var value );
				charge.Value = value;
			}
			charge.Formula   = string.Empty;
			charge.IsFormula = false;
		}
		else
		{
			charge.Formula   = SelectedFormula.Trim();
			charge.Value     = -1;
			charge.IsFormula = true;
		}

		charge.IsText = SelectedChargeType == FindStringResource( "ChargesWizardChargesTabChargeTypeReference" ) ? true : false;

		charge.Min = SelectedMinimum.Trim().Length > 0 ? decimal.Parse( SelectedMinimum.Trim() ) : 0;
		charge.Max = SelectedMaximum.Trim().Length > 0 ? decimal.Parse( SelectedMaximum.Trim() ) : 0;

		charge.IsTax            = IsTax;
		charge.AlwaysApplied    = AlwaysApply;
		charge.IsMultiplier     = IsMultiplier;
		charge.DisplayOnEntry   = ShowOnWebEntry;
		charge.DisplayOnWaybill = ShowOnWaybill;
		//ExcludeFromPayroll = charge.ExcludeFromPayroll; // TODO Not in object
		charge.ExcludeFromTaxes         = ExcludeFromTaxes;
		charge.IsFuelSurcharge          = IsFuelSurcharge;
		charge.ExcludeFromFuelSurcharge = ExcludeFromFuelSurcharge;

		return charge;
	}

	public void MoveUp()
	{
		if( SelectedCharge != null )
		{
			var index = Charges.IndexOf( SelectedCharge );

			if( index > 0 )
			{
				Charges.Remove( SelectedCharge );
				Charges.Insert( index - 1, SelectedCharge );

				// Rebuild the list
				WhenChargesChanges();
				ArePendingCharges = AreButtonsEnabled = true;
				WhenArePendingChanges();
			}
		}
	}

	public void MoveDown()
	{
		if( SelectedCharge != null )
		{
			var index = Charges.IndexOf( SelectedCharge );

			if( index < ( Charges.Count - 1 ) )
			{
				Charges.Remove( SelectedCharge );
				Charges.Insert( index + 1, SelectedCharge );

				// Rebuild the list
				WhenChargesChanges();
				ArePendingCharges = AreButtonsEnabled = true;
				WhenArePendingChanges();
			}
		}
	}

	public void DeleteCharge()
	{
		if( SelectedCharge != null )
		{
			Logging.WriteLogLine( "Deleting charge: " + SelectedCharge.ChargeId );
			Charges.Remove( SelectedCharge );
			WhenChargesChanges();
			ArePendingCharges = AreButtonsEnabled = true;
			WhenArePendingChanges();
		}
	}

	public async void SaveCharges()
	{
		Logging.WriteLogLine( $"Saving {Charges.Count} Charges and {DefaultCharges} Default Charges" );
		Charges charges = new();
		charges.AddRange( Charges );
		charges.AddRange( DefaultCharges );
		charges.AddRange( SurchargeCharges );

		await Azure.Client.RequestAddUpdateCharges( charges );

		ArePendingCharges = false;
	}
#endregion

#region Charges List
	public Charge SelectedCharge { get; set; }

	[DependsUpon( nameof( SelectedCharge ) )]
	public void WhenSelectedChargeChanges()
	{
		Logging.WriteLogLine( "DEBUG SelectedCharge: " + SelectedCharge.ChargeId );
		IsEditEnabled = SelectedCharge != null ? true : false;
	}


	public bool IsChargeTextBoxReadOnly
	{
		get { return Get( () => IsChargeTextBoxReadOnly, true ); }
		set { Set( () => IsChargeTextBoxReadOnly, value ); }
	}


	public bool IsEditingCharge
	{
		get { return Get( () => IsEditingCharge, false ); }
		set { Set( () => IsEditingCharge, value ); }
	}

	[DependsUpon( nameof( IsNewCharge ) )]
	[DependsUpon( nameof( IsEditingCharge ) )]
	public void WhenIsNewOrIsEditingChargeChanges()
	{
		AreButtonsEnabled = !IsNewCharge && !IsEditingCharge;
	}

	public bool AreButtonsEnabled
	{
		get { return Get( () => AreButtonsEnabled, true ); }
		set { Set( () => AreButtonsEnabled, value ); }
	}


	public bool IsNewCharge
	{
		get { return Get( () => IsNewCharge, false ); }
		set { Set( () => IsNewCharge, value ); }
	}

	[DependsUpon( nameof( IsEditingCharge ) )]
	[DependsUpon( nameof( IsNewCharge ) )]
	public void WhenEditingOrNewChanges()
	{
		if( IsEditingCharge || IsNewCharge )
			IsEnabled = true;
		else
			IsEnabled = false;
	}


	public bool IsEnabled
	{
		get { return Get( () => IsEnabled, false ); }
		set { Set( () => IsEnabled, value ); }
	}

	[DependsUpon( nameof( IsEnabled ) )]
	public void WhenIsEnabledChanges()
	{
		IsListEnabled = !IsEnabled;
	}

	public bool IsListEnabled
	{
		get { return Get( () => IsListEnabled, !IsEnabled ); }
		set { Set( () => IsListEnabled, value ); }
	}


	public bool IsEditEnabled
	{
		get { return Get( () => IsEditEnabled, false ); }
		set { Set( () => IsEditEnabled, value ); }
	}
#endregion
#endregion

#region Overrides
	public ObservableCollection<string> ChargesNames
	{
		get { return Get( () => ChargesNames, new ObservableCollection<string>() ); }
		set { Set( () => ChargesNames, value ); }
	}


	public ObservableCollection<string> OverridesChargesList { get; } = new();

	private ObservableCollection<string> GetChargesNames()
	{
		ObservableCollection<string> chargesNames = new();
		//chargesNames.Add( "" );

		if( Charges.Count > 0 )
		{
			SortedDictionary<string, string> sd = new();

			foreach( var c in Charges )
			{
				// Don't include overrides
				if( !sd.ContainsKey( c.ChargeId ) && !c.IsOverride )
					sd.Add( c.ChargeId, c.ChargeId );
			}

			foreach( var id in sd.Keys )
				chargesNames.Add( id );
			ChargesNames = chargesNames;
		}

		return chargesNames;
	}

	private string FormatOverrideCharge( Charge charge )
	{
		StringBuilder line = new();

		//string value = charge.IsFormula ? charge.Formula : charge.Value + "";
		var snippit = string.Empty;

		if( charge.IsFormula )
		{
			var lines = charge.Formula.Split( '\n' );

			if( lines.Length == 1 ) // Simple
				snippit = lines[ 0 ];
			else
				snippit = lines[ 1 ] + "...";
		}
		var value = charge.IsFormula ? snippit : charge.Value + "";
		//line.Append("(").Append(charge.ChargeId).Append(") ").Append(charge.Label).Append(" ").Append(charge.OverridenPackage).Append(" (").Append(value).Append(")");
		line.Append( "(" ).Append( charge.ChargeId ).Append( ") " ).Append( charge.Label ).Append( " " ).Append( " (" ).Append( value ).Append( ")" );

		return line.ToString();
	}

	public List<PackageType> PackageTypeObjects = new();

	public ObservableCollection<string> PackageTypeNames
	{
		get => Get( () => PackageTypeNames, new ObservableCollection<string>() );
		set => Set( () => PackageTypeNames, value );
	}

	public ObservableCollection<string> GetPackageTypes()
	{
		ObservableCollection<string> packageTypes = new();

		Task.Run( () =>
		          {
			          try
			          {
				          Logging.WriteLogLine( "Getting package types..." );

				          PackageTypeList pts = null;

				          // From Terry: Need to do this to avoid bad requests in the server log
				          if( !IsInDesignMode )
				          {
					          pts = Azure.Client.RequestGetPackageTypes().Result;
					          SortedDictionary<string, string> sd = new();

					          foreach( var pt in pts )
					          {
						          PackageTypeObjects.Add( pt );

						          if( !sd.ContainsKey( pt.Description ) )
							          sd.Add( pt.Description, pt.Description );
					          }

					          Dispatcher.Invoke( () =>
					                             {
						                             PackageTypeNames.Clear();
						                             PackageTypeNames.Add( "" );

						                             foreach( var name in sd.Keys )
							                             PackageTypeNames.Add( name );
					                             } );
				          }
			          }
			          catch( Exception e )
			          {
				          Logging.WriteLogLine( "Exception thrown: " + e );
				          MessageBox.Show( "Error fetching Package Types\n" + e, "Error", MessageBoxButton.OK );
			          }
		          } );

		return packageTypes;
	}

	public List<ServiceLevel> ServiceLevelObjects = new();

	public ObservableCollection<string> ServiceLevelNames
	{
		get => Get( () => ServiceLevelNames, new ObservableCollection<string>() );
		set => Set( () => ServiceLevelNames, value );
	}

	public ObservableCollection<string> GetServiceLevels()
	{
		ObservableCollection<string> serviceLevels = new();

		Task.Run( () =>
		          {
			          try
			          {
				          Logging.WriteLogLine( "Getting service levels..." );

				          ServiceLevelListDetailed sls = null;

				          // From Terry: Need to do this to avoid bad requests in the server log
				          if( !IsInDesignMode )
				          {
					          sls = Azure.Client.RequestGetServiceLevelsDetailed().Result;
					          SortedDictionary<string, string> sd = new();

					          foreach( var sl in sls )
					          {
						          ServiceLevelObjects.Add( sl );

						          if( !sd.ContainsKey( sl.OldName ) )
							          sd.Add( sl.OldName, sl.OldName );
					          }

					          Dispatcher.Invoke( () =>
					                             {
						                             ServiceLevelNames.Clear();
						                             ServiceLevelNames.Add( "" );

						                             var names = ( from S in sls
						                                           orderby S.SortOrder
						                                           select S.OldName ).ToList();

						                             //foreach (string name in sd.Keys)
						                             foreach( var name in names )
							                             ServiceLevelNames.Add( name );
					                             } );
				          }
			          }
			          catch( Exception e )
			          {
				          Logging.WriteLogLine( "Exception thrown: " + e );
				          MessageBox.Show( "Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK );
			          }
		          } );

		return serviceLevels;
	}


	public string OverrideGroup
	{
		get { return Get( () => OverrideGroup, "" ); }
		set { Set( () => OverrideGroup, value ); }
	}

	public string SelectedOverrideChargeId
	{
		get { return Get( () => SelectedOverrideChargeId, "" ); }
		set { Set( () => SelectedOverrideChargeId, value ); }
	}


	public string SelectedOverrideLabel
	{
		get { return Get( () => SelectedOverrideLabel, "" ); }
		set { Set( () => SelectedOverrideLabel, value ); }
	}


	public string SelectedOverridePackage
	{
		get { return Get( () => SelectedOverridePackage, "" ); }
		set { Set( () => SelectedOverridePackage, value ); }
	}


	public string SelectedOverrideService
	{
		get { return Get( () => SelectedOverrideService, "" ); }
		set { Set( () => SelectedOverrideService, value ); }
	}


	public string OverrideDefault
	{
		get { return Get( () => OverrideDefault, "" ); }
		set { Set( () => OverrideDefault, value ); }
	}


	public string OverrideFormula
	{
		get { return Get( () => OverrideFormula, "" ); }
		set { Set( () => OverrideFormula, value ); }
	}


	public bool AreOverridesButtonsEnabled
	{
		get { return Get( () => AreOverridesButtonsEnabled, true ); }
		set { Set( () => AreOverridesButtonsEnabled, value ); }
	}


	public bool IsOverrideEnabled
	{
		get { return Get( () => IsOverrideEnabled, false ); }
		set { Set( () => IsOverrideEnabled, value ); }
	}


	[DependsUpon( nameof( IsOverrideEnabled ) )]
	public void WhenIsOverrideEnabledChanges()
	{
		IsOverrideListEnabled = !IsOverrideEnabled;
	}

	public bool IsOverrideListEnabled
	{
		get { return Get( () => IsOverrideListEnabled, !IsOverrideEnabled ); }
		set { Set( () => IsOverrideListEnabled, value ); }
	}


	public bool IsPackageComboEnabled
	{
		get { return Get( () => IsPackageComboEnabled, false ); }
		set { Set( () => IsPackageComboEnabled, value ); }
	}


	public bool IsSelectPackageChecked
	{
		get { return Get( () => IsSelectPackageChecked, false ); }
		set { Set( () => IsSelectPackageChecked, value ); }
	}


	public bool IsServiceComboEnabled
	{
		get { return Get( () => IsServiceComboEnabled, false ); }
		set { Set( () => IsServiceComboEnabled, value ); }
	}


	public bool IsSelectServiceSelected
	{
		get { return Get( () => IsSelectServiceSelected, false ); }
		set { Set( () => IsSelectServiceSelected, value ); }
	}


	public bool IsOverrideDefaultSelected
	{
		get { return Get( () => IsOverrideDefaultSelected, true ); }
		set { Set( () => IsOverrideDefaultSelected, value ); }
	}


	public bool IsOverrideFormulaSelected
	{
		get { return Get( () => IsOverrideFormulaSelected, false ); }
		set { Set( () => IsOverrideFormulaSelected, value ); }
	}


	public bool IsOverridesUpdateButtonEnabled
	{
		get { return Get( () => IsOverridesUpdateButtonEnabled, true ); }
		set { Set( () => IsOverridesUpdateButtonEnabled, value ); }
	}


#region Overrides Actions
	public void OverridesEdit()
	{
		LoadOverride();
		IsOverrideEnabled          = true;
		IsEditingOverride          = true;
		AreOverridesButtonsEnabled = false;
	}

	private void LoadOverride()
	{
		if( SelectedOverrideChargeObject != null )
		{
			ClearOverride();

			SelectedOverrideLabel = SelectedOverrideChargeObject.Label;
			// test2_override
			SelectedOverrideChargeId = GetBaseChargeFromOverrideChargeId( SelectedOverrideChargeObject.ChargeId );

			// Need to differentiate between Packages and Service Levels
			/*
			 * string label = IsSelectPackageChecked
			    ? sourceCharge.ChargeId + " " + FindStringResource("ChargesWizardOverridesTabLabelPackageMarker") + " " + SelectedOverridePackage
			    : sourceCharge.ChargeId + " " + FindStringResource("ChargesWizardOverridesTabLabelServiceMarker") + " " + SelectedOverrideService;
			 */
			//SelectedOverridePackage = SelectedOverrideChargeObject.OverridenPackage;
			//IsSelectPackageChecked = SelectedOverridePackage.Length > 0 ? true : false;
			if( SelectedOverrideChargeObject.Label.Contains( FindStringResource( "ChargesWizardOverridesTabLabelPackageMarker" ) ) )
			{
				SelectedOverridePackage = SelectedOverrideChargeObject.OverridenPackage;
				IsSelectPackageChecked  = true;
			}
			else if( SelectedOverrideChargeObject.Label.Contains( FindStringResource( "ChargesWizardOverridesTabLabelServiceMarker" ) ) )
			{
				SelectedOverrideService = SelectedOverrideChargeObject.OverridenPackage;
				IsSelectServiceSelected = true;
			}

			//SelectedOverrideService = SelectedOverrideChargeObject.OverrideService; // TODO Not in Charge
			if( !SelectedOverrideChargeObject.IsFormula )
			{
				OverrideDefault           = SelectedOverrideChargeObject.Value + "";
				IsOverrideDefaultSelected = true;
				IsOverrideFormulaSelected = false;
			}
			else
			{
				OverrideFormula           = SelectedOverrideChargeObject.Formula;
				IsOverrideDefaultSelected = false;
				IsOverrideFormulaSelected = true;
			}
		}
	}

	private string GetBaseChargeFromOverrideChargeId( string overrideId )
	{
		var chargeId = overrideId.Substring( 0, overrideId.IndexOf( '_' ) );
		return chargeId;
	}

	public void OverridesNewOverride()
	{
		SelectedOverrideChargeObject = new Charge();
		ClearOverride();
		IsOverrideEnabled          = true;
		IsEditingOverride          = true;
		IsNewOverride              = true;
		AreOverridesButtonsEnabled = false;
	}

	private void ClearOverride()
	{
		OverrideGroup            = string.Empty;
		SelectedOverrideLabel    = string.Empty;
		SelectedOverrideChargeId = string.Empty;
		SelectedOverridePackage  = string.Empty;
		SelectedOverrideService  = string.Empty;
		OverrideDefault          = string.Empty;
		OverrideFormula          = string.Empty;

		view.UncheckPackageService();
	}

	public void CancelEditOverride()
	{
		ClearOverride();
		IsNewOverride              = IsEditingOverride = IsOverrideEnabled = IsPackageComboEnabled = IsServiceComboEnabled = false;
		AreOverridesButtonsEnabled = true;
	}

	public void UpdateOverrideChargeObject()
	{
		// SelectedOverrideChargeObject is either an empty Charge (new) or one that has already been built
		// Look up the charge to be overridden
		var sourceCharge = ( from C in Charges
		                     where C.ChargeId == SelectedOverrideChargeId
		                     select C ).FirstOrDefault();

		if( sourceCharge != null )
		{
			var previousChargeId = sourceCharge.ChargeId;
			var newCharge        = BuildOverrideCharge( sourceCharge, SelectedOverrideChargeObject );

			var current = ( from C in Charges
			                where C.ChargeId == SelectedOverrideChargeObject.ChargeId
			                select C ).FirstOrDefault();

			if( current != null ) // Editing
			{
				var index = Charges.IndexOf( current );
				Charges.Remove( SelectedOverrideChargeObject );
				Charges.Insert( index, SelectedOverrideChargeObject );
			}
			else // New
			{
				SelectedOverrideChargeObject.SortIndex = (short)Charges.Count;
				Charges.Add( SelectedOverrideChargeObject );
			}
		}

		// Rebuild the list
		WhenOverridesChanges();

		ClearOverride();
		IsNewOverride       = IsEditingOverride          = IsOverrideEnabled = IsPackageComboEnabled = IsServiceComboEnabled = false;
		ArePendingOverrides = AreOverridesButtonsEnabled = true;
		WhenArePendingChanges();
	}

	public void DeleteOverride()
	{
		if( SelectedOverrideChargeObject != null )
		{
			Logging.WriteLogLine( "Removing override " + SelectedOverrideChargeObject.ChargeId );

			var charge = ( from C in Charges
			               where C.ChargeId == SelectedOverrideChargeObject.ChargeId
			               select C ).FirstOrDefault();

			if( charge != null )
			{
				Charges.Remove( charge );

				// Rebuild the list
				WhenOverridesChanges();

				ArePendingOverrides = AreOverridesButtonsEnabled = true;
				WhenArePendingChanges();
			}
		}
	}

	private Charge BuildOverrideCharge( Charge sourceCharge, Charge toModify )
	{
		if( ( toModify.ChargeId == null ) || ( toModify.ChargeId.Length == 0 ) )
		{
			toModify.ChargeId = MakeOverrideChargeId( SelectedOverrideChargeId );
			//toModify.Label = SelectedOverrideLabel;
			toModify.Label = MakeOverrideChargeLabel( sourceCharge );
		}
		else if( GetBaseChargeFromOverrideChargeId( toModify.ChargeId ) != SelectedOverrideChargeId )
		{
			// Selected charge to override has changed
			Logging.WriteLogLine( "Changing Charge to override: was " + GetBaseChargeFromOverrideChargeId( toModify.ChargeId ) + ", now " + SelectedOverrideChargeId );
			toModify.ChargeId = MakeOverrideChargeId( SelectedOverrideChargeId );
			toModify.Label    = MakeOverrideChargeLabel( sourceCharge );
		}

		if( IsOverrideDefaultSelected )
		{
			if( decimal.TryParse( OverrideDefault.Trim(), out var value ) )
				toModify.Value = value;
			else
			{
				Logging.WriteLogLine( "Error - can't parse " + OverrideDefault.Trim() );
				toModify.Value = -1;
			}
			toModify.IsFormula = false;
		}
		else
		{
			toModify.Value     = -1;
			toModify.Formula   = OverrideFormula.Trim();
			toModify.IsFormula = true;
		}

		toModify.ChargeType       = sourceCharge.ChargeType;
		toModify.DisplayType      = sourceCharge.DisplayType;
		toModify.IsTax            = sourceCharge.IsTax;
		toModify.AlwaysApplied    = sourceCharge.AlwaysApplied;
		toModify.IsMultiplier     = sourceCharge.IsMultiplier;
		toModify.DisplayOnEntry   = sourceCharge.DisplayOnEntry;
		toModify.DisplayOnWaybill = sourceCharge.DisplayOnWaybill;
		//ExcludeFromPayroll = charge.ExcludeFromPayroll; // TODO Not in object
		toModify.ExcludeFromTaxes         = sourceCharge.ExcludeFromTaxes;
		toModify.ExcludeFromFuelSurcharge = sourceCharge.ExcludeFromFuelSurcharge;

		toModify.IsOverride = true;

		if( IsSelectPackageChecked )
			toModify.OverridenPackage = SelectedOverridePackage;
		else
		{
			//toModify.OverridenPackage = string.Empty;
			toModify.OverridenPackage = SelectedOverrideService; // TODO Not in object - using label to differentiate
		}

        toModify.Label = MakeOverrideChargeLabel( sourceCharge );

		return toModify;
	}

	private Charge BuildOverrideCharge_V1( Charge sourceCharge, Charge toModify )
	{
		if( ( toModify.ChargeId == null ) || ( toModify.ChargeId.Length == 0 ) )
		{
			toModify.ChargeId = MakeOverrideChargeId( SelectedOverrideChargeId );
			//toModify.Label = SelectedOverrideLabel;
			toModify.Label = sourceCharge.ChargeId + " OVERRIDE for " + SelectedOverridePackage;
		}
		else if( GetBaseChargeFromOverrideChargeId( toModify.ChargeId ) != SelectedOverrideChargeId )
		{
			// Selected charge to override has changed
			Logging.WriteLogLine( "Changing Charge to override: was " + GetBaseChargeFromOverrideChargeId( toModify.ChargeId ) + ", now " + SelectedOverrideChargeId );
			toModify.ChargeId = toModify.Label = MakeOverrideChargeId( SelectedOverrideChargeId );
		}

		if( IsOverrideDefaultSelected )
		{
			if( decimal.TryParse( OverrideDefault.Trim(), out var value ) )
				toModify.Value = value;
			else
			{
				Logging.WriteLogLine( "Error - can't parse " + OverrideDefault.Trim() );
				toModify.Value = -1;
			}
			toModify.IsFormula = false;
		}
		else
		{
			toModify.Value     = -1;
			toModify.Formula   = OverrideFormula.Trim();
			toModify.IsFormula = true;
		}

		toModify.ChargeType       = sourceCharge.ChargeType;
		toModify.DisplayType      = sourceCharge.DisplayType;
		toModify.IsTax            = sourceCharge.IsTax;
		toModify.AlwaysApplied    = sourceCharge.AlwaysApplied;
		toModify.IsMultiplier     = sourceCharge.IsMultiplier;
		toModify.DisplayOnEntry   = sourceCharge.DisplayOnEntry;
		toModify.DisplayOnWaybill = sourceCharge.DisplayOnWaybill;
		//ExcludeFromPayroll = charge.ExcludeFromPayroll; // TODO Not in object
		toModify.ExcludeFromTaxes         = sourceCharge.ExcludeFromTaxes;
		toModify.ExcludeFromFuelSurcharge = sourceCharge.ExcludeFromFuelSurcharge;

		toModify.IsOverride = true;

		if( IsSelectPackageChecked )
			toModify.OverridenPackage = SelectedOverridePackage;
		else
			toModify.OverridenPackage = string.Empty;
		//charge.OverridenService = SelectedOverrideService; // TODO Not in object

		return toModify;
	}

	private string MakeOverrideChargeId( string chargeId )
	{
		var newId = chargeId + "_override";

		var test = ( from C in Charges
		             where C.ChargeId == newId
		             select C ).FirstOrDefault();

		if( test != null )
		{
			for( var i = 1; i < 1000; i++ )
			{
				test = ( from C in Charges
				         where C.ChargeId == ( newId + i )
				         select C ).FirstOrDefault();

				if( test == null )
				{
					newId += i;
					break;
				}
			}
		}

		Logging.WriteLogLine( "DEBUG newId: " + newId );

		return newId;
	}

	private string MakeOverrideChargeLabel( Charge sourceCharge )
	{
		//? sourceCharge.ChargeId + " OVERRIDE for package " + SelectedOverridePackage
		//: sourceCharge.ChargeId + " OVERRIDE for service " + SelectedOverrideService;
		var label = IsSelectPackageChecked
			            ? sourceCharge.ChargeId + " " + FindStringResource( "ChargesWizardOverridesTabLabelPackageMarker" ) + " " + SelectedOverridePackage
			            : sourceCharge.ChargeId + " " + FindStringResource( "ChargesWizardOverridesTabLabelServiceMarker" ) + " " + SelectedOverrideService;

		return label;
	}

	private void WhenOverridesChanges()
	{
		OverridesList.Clear();

		var charges = ( from C in Charges
		                where C.IsOverride
		                orderby C.SortIndex
		                select C ).ToList();

		if( charges != null )
		{
			foreach( var c in charges )
			{
				var line = FormatOverrideCharge( c );
				OverridesList.Add( line );
			}
		}
	}

	public async void SaveOverrides()
	{
		Logging.WriteLogLine( "Saving Overrides" );
		Charges charges = new();
		charges.AddRange( Charges );
		charges.AddRange( DefaultCharges );

		await Azure.Client.RequestAddUpdateCharges( charges );

		ArePendingOverrides = false;
	}
#endregion

#region Overrides List
	public ObservableCollection<string> OverridesList { get; } = new();


	public Charge SelectedOverrideChargeObject { get; set; }


	public bool IsEditingOverride
	{
		get { return Get( () => IsEditingOverride, false ); }
		set { Set( () => IsEditingOverride, value ); }
	}


	public bool IsNewOverride
	{
		get { return Get( () => IsNewOverride, false ); }
		set { Set( () => IsNewOverride, value ); }
	}
#endregion
#endregion
}