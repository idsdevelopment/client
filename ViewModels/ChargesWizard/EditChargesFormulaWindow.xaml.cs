﻿using System.Windows.Input;

namespace ViewModels.ChargesWizard;

/// <summary>
///     Interaction logic for EditChargesFormulaWindow.xaml
/// </summary>
public partial class EditChargesFormulaWindow : Window
{
	private bool Cancelled;

	public string ShowEditChargesFormulaWindow( string currentFormula )
	{
		if( DataContext is EditChargesFormulaWindowModel Model )
		{
			if( ( currentFormula != null ) && ( currentFormula.Length > 0 ) )
			{
				Model.CurrentFormula = currentFormula;
				Model.LoadFormula( currentFormula );
			}

			ShowDialog();

			if( !Cancelled )
				currentFormula = Model.BuildFormula();
			else
				currentFormula = string.Empty;
		}

		return currentFormula;
	}

	public void EnsureDecimal( object sender, TextCompositionEventArgs e )
	{
		var          ToCheck      = e.Text.Substring( e.Text.Length - 1 );
		const string DECIMAL_CHAR = ".";

		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) && !ToCheck.Equals( DECIMAL_CHAR ) )
			e.Handled = true;
	}

	public EditChargesFormulaWindow( Window owner )
	{
		InitializeComponent();

		Owner = owner;
	}

	private void BtnSave_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}

	private void BtnCancel_Click( object sender, RoutedEventArgs e )
	{
		Cancelled = true;

		if( DataContext is EditChargesFormulaWindowModel Model )
			Model.NewFormula = Model.CurrentFormula;

		Close();
	}

	private void BtnTest_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditChargesFormulaWindowModel Model )
			Model.TestFormula();
	}
}