﻿#nullable enable

using System.Windows.Controls;
using System.Windows.Input;

namespace ViewModels.ChargesWizard;

/// <summary>
///     Interaction logic for ChargesWizard.xaml
/// </summary>
public partial class ChargesWizard : Page
{
	public ChargesWizard()
	{
		InitializeComponent();

		Model = (ChargesWizardModel)DataContext;

		Model.SetView( this );

		Model.GetCharges();
		Model.GetServiceLevels();
		Model.GetPackageTypes();
	}

	private readonly ChargesWizardModel Model;

	private void SaveAll( object sender, RoutedEventArgs e )
	{
		Model.SaveAll();

		// TODO Update any open Shipments Entry screens
	}

	private bool ValidateStringAsNumber( string text ) => int.TryParse( text, out _ );

	public string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

#region Charges
	private void BtnChargeEdit_Click( object? sender, RoutedEventArgs? e )
	{
		var SelectedIndex = LbCharges.SelectedIndex;

		if( SelectedIndex > -1 )
		{
			SetChargeEnabledState( true );

			List<Charge> List = ( from C in Model.Charges
								  where !C.IsOverride
								  select C ).ToList();
			Model.SelectedCharge = List[ SelectedIndex ];
			//Model.SelectedCharge = Model.Charges[ selectedIndex];
			//Model.SelectedCharge = GetChargeFromLine(LbCharges.SelectedItem.ToString());

			Model.EditCharge();
		}
	}

	private void BtnChargeNew_Click( object sender, RoutedEventArgs e )
	{
		SetChargeEnabledState( true );
		Model.NewCharge();
	}

	private void BtnChargeDelete_Click( object sender, RoutedEventArgs e )
	{
		var SelectedIndex = LbCharges.SelectedIndex;

		if( SelectedIndex > -1 )
		{
			//Model.SelectedCharge = Model.Charges[ selectedIndex ];
			//Model.SelectedCharge = GetChargeFromLine(LbCharges.SelectedItem.ToString());
			List<Charge> List = ( from C in Model.Charges
								  where !C.IsOverride
								  select C ).ToList();
			Model.SelectedCharge = List[ SelectedIndex ];
			var Caption = FindStringResource( "ChargesWizardChargesTabDeleteChargeTitle" );
			var Message = FindStringResource( "ChargesWizardChargesTabDeleteChargeMessage" );
			Message += "\n" + FindStringResource( "ChargesWizardChargesTabDeleteChargeMessage2" );
			Message =  Message.Replace( "@1", Model.SelectedCharge.ChargeId );
			var Mbr = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

			if( Mbr == MessageBoxResult.Yes )
				Model.DeleteCharge();
		}
	}

	private void BtnChargeUp_Click( object sender, RoutedEventArgs e )
	{
		var SelectedIndex = LbCharges.SelectedIndex;

		if( SelectedIndex > -1 )
		{
			//Model.SelectedCharge = Model.Charges[ selectedIndex ];
			//Model.SelectedCharge = GetChargeFromLine(LbCharges.SelectedItem.ToString());
			List<Charge> List = ( from C in Model.Charges
								  where !C.IsOverride
								  select C ).ToList();
			Model.SelectedCharge = List[ SelectedIndex ];
			Model.MoveUp();
		}
	}

	private void BtnChargeDown_Click( object sender, RoutedEventArgs e )
	{
		var SelectedIndex = LbCharges.SelectedIndex;

		if( SelectedIndex > -1 )
		{
			//Model.SelectedCharge = Model.Charges[ selectedIndex ];
			//Model.SelectedCharge = GetChargeFromLine(LbCharges.SelectedItem.ToString());
			List<Charge> List = ( from C in Model.Charges
								  where !C.IsOverride
								  select C ).ToList();
			Model.SelectedCharge = List[ SelectedIndex ];
			Model.MoveDown();
		}
	}

	private void LbCharges_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		BtnChargeEdit_Click( null, null );
	}

	private void BtnChargeUpdate_Click( object sender, RoutedEventArgs e )
	{
		var (Ok, Errors) = ValidateCharge();

		if( Ok )
		{
			Model.UpdateCharge();
			SetChargeEnabledState( false );
		}
		else
		{
			// TODO show errors
			var Title   = FindStringResource( "ChargesWizardChargesTabErrorsTitle" );
			var Message = FindStringResource( "ChargesWizardChargesTabErrorsMessage" );

			foreach( var Error in Errors )
				Message += "\n\t" + Error;

			MessageBox.Show( Message, Title, MessageBoxButton.OK, MessageBoxImage.Error );
		}
	}

	private (bool ok, List<string> errors) ValidateCharge()
	{
		var          Ok     = true;
		List<string> Errors = new();

		if( DataContext is ChargesWizardModel Model )
		{
			if( Model.SelectedChargeId.IsNullOrWhiteSpace() )
				Errors.Add( FindStringResource( "ChargesWizardChargesTabErrorMissingChargeId" ) );

			if( Model.SelectedChargeDisplayLabel.IsNullOrWhiteSpace() )
				Errors.Add( FindStringResource( "ChargesWizardChargesTabErrorMissingChargeLabel" ) );
		}

		return ( Ok, Errors );
	}

	private void BtnChargeCancel_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			Model.CancelEditCharge();
			SetChargeEnabledState( false );
		}
	}

	private void BtnChargeSave_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
			Model.SaveCharges();
	}

	public void SetChargeEnabledState( bool enabled )
	{
		RbChargeDefault.IsEnabled     = enabled;
		RbChargeFormula.IsEnabled     = enabled;
		CbPredefinedCharges.IsEnabled = enabled;
		TbChargeId.IsEnabled          = enabled;
		TbDisplayLabel.IsEnabled      = enabled;
		CbChargeType.IsEnabled        = enabled;
		CbDisplayOptions.IsEnabled    = enabled;
		TbDefault.IsEnabled           = enabled;
		// This is handled by radio button handlers
		//TbFormula.IsEnabled = enabled;
		TbFormula.IsEnabled = false;
		TbMinimum.IsEnabled = enabled;
		TbMaximum.IsEnabled = enabled;
	}

	private void TbChargeId_PreviewKeyDown( object sender, KeyEventArgs e )
	{
		if( e.Key == Key.Space )
			e.Handled = true;
	}

	private void TbDefault_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		if( e.Text == "." )
			e.Handled = false;
		else if( !ValidateStringAsNumber( e.Text ) )
			e.Handled = true;
	}


	private void LbCharges_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		//if (DataContext is ChargesWizardModel Model)
		//{
		//    try
		//    {
		//        Model.SelectedCharge = Model.Charges[LbCharges.SelectedIndex];
		//    }
		//    catch (Exception ex)
		//    {
		//        Logging.WriteLogLine("DEBUG " + ex.ToString());
		//    }
		//}
	}


	public void RbChargeDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			if( Model.IsEnabled )
			{
				if( TbDefault != null )
					TbDefault.IsEnabled = true;

				if( RbChargeFormula != null )
					RbChargeFormula.IsChecked = false;

				if( TbFormula != null )
					TbFormula.IsEnabled = false;

				Model.IsDefaultSelected = true;
				Model.IsFormulaSelected = false;
			}
		}
	}

	public void RbChargeFormula_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			if( Model.IsEnabled )
			{
				if( TbFormula != null )
					TbFormula.IsEnabled = true;

				if( RbChargeDefault != null )
					RbChargeDefault.IsChecked = false;

				if( TbDefault != null )
					TbDefault.IsEnabled = false;

				Model.IsDefaultSelected = false;
				Model.IsFormulaSelected = true;
			}
		}
	}


	private void BtnEditChargeFormula_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			// Testing
			// Model.SelectedFormula = "Result = Pieces * 0.50";
			var Formula = new EditChargesFormulaWindow( Application.Current.MainWindow ).ShowEditChargesFormulaWindow( Model.SelectedFormula );

			if( Formula != string.Empty )
			{
				Logging.WriteLogLine( "New formula: " + Formula );
				Model.SelectedFormula = Formula;
				TbFormula.ToolTip     = Formula;
			}
		}
	}
#endregion

#region Overrides
	private void SetOverrideEnabledState( bool enabled )
	{
		CbOverrideCharge.IsEnabled        = enabled;
		TbOverrideDefault.IsEnabled       = enabled;
		RbSelectOverridePackage.IsEnabled = enabled;
		RbSelectOverrideService.IsEnabled = enabled;
	}

	private void BtnOverridesEdit_Click( object? sender, RoutedEventArgs? e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			var Overrides = ( from C in Model.Charges
							  where C.IsOverride
							  select C ).ToList();

			var SelectedIndex = LbOverrides.SelectedIndex;

			if( SelectedIndex > -1 )
			{
				SetOverrideEnabledState( true );
				//Model.SelectedOverrideChargeObject = Model.Charges[selectedIndex];
				Model.SelectedOverrideChargeObject = Overrides[ SelectedIndex ];
				Model.OverridesEdit();
			}
		}
	}

	private void BtnOverridesNew_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			SetOverrideEnabledState( true );
			Model.OverridesNewOverride();
		}
	}

	private void BtnOverridesDelete_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ChargesWizardModel Model )
		{
			var SelectedIndex = LbOverrides.SelectedIndex;

			if( SelectedIndex > -1 )
			{
				var Overrides = ( from C in Model.Charges
								  where C.IsOverride
								  select C ).ToList();

				//Model.SelectedOverrideChargeObject = Model.Charges[selectedIndex];
				Model.SelectedOverrideChargeObject = Overrides[ SelectedIndex ];
				var Title   = FindStringResource( "ChargesWizardOverridesTabDeleteOverrideTitle" );
				var Message = FindStringResource( "ChargesWizardOverridesTabDeleteOverrideMessage" );
				Message += "\n" + FindStringResource( "ChargesWizardOverridesTabDeleteOverrideMessage2" );
				Message =  Message.Replace( "@1", Model.SelectedOverrideChargeObject.ChargeId );
				var Mbr = MessageBox.Show( Message, Title, MessageBoxButton.YesNo, MessageBoxImage.Question );

				if( Mbr == MessageBoxResult.Yes )
					Model.DeleteOverride();
			}
		}
	}

	private void BtnOverridesUp_Click( object sender, RoutedEventArgs e )
	{
	}

	private void BtnOverridesDown_Click( object sender, RoutedEventArgs e )
	{
	}

	private void LbOverrides_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		BtnOverridesEdit_Click( null, null );
	}

	private void BtnOverridesSave_Click( object sender, RoutedEventArgs e )
	{
			Model.SaveOverrides();
	}

	private void BtnOverridesUpdate_Click( object sender, RoutedEventArgs e )
	{
		// Check that a base charge has been chosen as well as a package or service
		if( ( Model.SelectedOverrideChargeId != string.Empty ) && ( ( Model.SelectedOverridePackage != string.Empty ) || ( Model.SelectedOverrideService != string.Empty ) ) )
		{
			Model.UpdateOverrideChargeObject();
			SetOverrideEnabledState( false );
		}
		else
		{
			// Show error "ChargesWizardOverridesTabErrorsTitle"
			var Caption   = FindStringResource( "ChargesWizardOverridesTabErrorsTitle" );
			var Message = FindStringResource( "ChargesWizardOverridesTabErrorsMessage" );
			MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
		}
	}

	private void BtnOverridesCancel_Click( object sender, RoutedEventArgs e )
	{
		Model.CancelEditOverride();
		SetOverrideEnabledState( false );
	}

	private void TbOverrideDefault_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		if( e.Text == "." )
			e.Handled = false;
		else if( !ValidateStringAsNumber( e.Text ) )
			e.Handled = true;
	}

	//private void CbSelectOverridePackage_Checked(object sender, RoutedEventArgs e)
	private void RbSelectOverridePackage_Checked( object sender, RoutedEventArgs e )
	{
		//CbOverridePackage.IsEnabled = true;
		Model.IsPackageComboEnabled = true;
	}

	//private void CbSelectOverridePackage_Unchecked(object sender, RoutedEventArgs e)
	private void RbSelectOverridePackage_Unchecked( object? sender, RoutedEventArgs? e )
	{
		//CbOverridePackage.IsEnabled = false;
		//CbSelectOverridePackage.IsChecked = false;
		RbSelectOverridePackage.IsChecked = false;
		Model.IsPackageComboEnabled       = false;
	}

	//private void CbSelectOverrideService_Checked(object sender, RoutedEventArgs e)
	private void RbSelectOverrideService_Checked( object sender, RoutedEventArgs e )
	{
		//CbOverrideService.IsEnabled = true;
		Model.IsServiceComboEnabled = true;
	}

	//private void CbSelectOverrideService_Unchecked(object sender, RoutedEventArgs e)
	private void RbSelectOverrideService_Unchecked( object? sender, RoutedEventArgs? e )
	{
		//CbOverrideService.IsEnabled = false;
		//CbSelectOverrideService.IsChecked = false;
		RbSelectOverrideService.IsChecked = false;
		Model.IsServiceComboEnabled       = false;
	}

	public void UncheckPackageService()
	{
		//CbSelectOverridePackage_Unchecked(null, null);
		RbSelectOverridePackage_Unchecked( null, null );
		//CbSelectOverrideService_Unchecked(null, null);
		RbSelectOverrideService_Unchecked( null, null );
	}


	private void RbOverrideDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( Model != null && Model.IsOverrideEnabled )
		{
			if( TbOverrideDefault != null )
				TbOverrideDefault.IsEnabled = true;

			if( RbOverrideFormula != null )
				RbOverrideFormula.IsChecked = false;

			if( TbOverrideFormula != null )
				TbOverrideFormula.IsEnabled = false;
			Model.IsOverrideDefaultSelected = true;
			Model.IsOverrideFormulaSelected = false;
		}
	}

	private void RbOverrideFormula_Checked( object sender, RoutedEventArgs e )
	{
		if( Model != null && Model.IsOverrideEnabled )
		{
			if( TbOverrideFormula != null )
				TbOverrideFormula.IsEnabled = true;

			if( TbOverrideDefault != null )
				TbOverrideDefault.IsEnabled = false;

			if( RbOverrideDefault != null )
				RbOverrideDefault.IsChecked = false;
			Model.IsOverrideDefaultSelected = false;
			Model.IsOverrideFormulaSelected = true;
		}
	}

	private void BtnEditOverrideFormula_Click( object sender, RoutedEventArgs e )
	{
		// Testing
		// Model.OverrideFormula = "Result = Pieces * 0.50";
		var Formula = new EditChargesFormulaWindow( Application.Current.MainWindow ).ShowEditChargesFormulaWindow( Model.OverrideFormula );

		if( Formula != string.Empty )
		{
			Logging.WriteLogLine( "New formula: " + Formula );
			Model.OverrideFormula     = Formula;
			TbOverrideFormula.ToolTip = Formula;
		}
	}
}
#endregion