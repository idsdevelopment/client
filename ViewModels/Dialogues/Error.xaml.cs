﻿namespace ViewModels.Dialogues;

/// <summary>
///     Interaction logic for Error.xaml
/// </summary>
public partial class Error : Window
{
	public Error()
	{
		InitializeComponent();
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}
}