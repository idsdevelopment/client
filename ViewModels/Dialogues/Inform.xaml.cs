﻿namespace ViewModels.Dialogues;

/// <summary>
///     Interaction logic for Inform.xaml
/// </summary>
public partial class Inform : Window
{
	public new void ShowDialog()
	{
		base.ShowDialog();
	}

	public Inform()
	{
		InitializeComponent();
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		DialogResult = true;
	}
}