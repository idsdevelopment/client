﻿using Microsoft.Reporting.WinForms;
using System.Reflection;
using ViewModels.ReportBrowser;

namespace ViewModels.Dialogues;

//#if USE_DIALOGUE // Used as block comment out
/// <summary>
///     Interaction logic for ReportDialog.xaml
/// </summary>
public partial class ReportDialog : Window
{
	private readonly ReportSettings ReportSettings;

	// TODO Add to here
	private readonly string LinfoxReportName = (string)Application.Current.TryFindResource( "ReportBrowserReportsDescriptionsLinfox" );
	private readonly string PhoenixInvoiceName = (string)Application.Current.TryFindResource( "ReportDialogPhoenixInvoiceTitle" );

	public ReportDialog( Window owner, ReportSettings reportSettings )
	{
		//LinfoxReportName = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox");
		Owner = owner;
		WindowStartupLocation = WindowStartupLocation.CenterOwner;
		InitializeComponent();
		ReportSettings = reportSettings;
		Logging.WriteLogLine( "Setting report to " + reportSettings.Description );
		Title = (string)Application.Current.TryFindResource( "ReportDialogTitle" );
		Title = Title.Replace( "@1", reportSettings.Description );
		ReportViewer.Reset();

		//ReportViewer.ProcessingMode = ProcessingMode.Local;
		//ReportViewer.PrinterSettings.PrinterName = printer;

		// string Report = "Weighbills.ShipNow.rdlc";
		//var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.ShipNow.rdlc";
		//var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Zebra_Intermountain.rdlc";
		//var Rep = MainWindow.REPORT_BASE_NAME + ".Reports.Zebra_Intermountain.rdlc";
		//var Rep = "Ids2Wpf.ViewModels.Reports.Zebra_Intermountain.rdlc";

		//var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Zebra_Intermountain.rdlc";

		Task.Run(async() =>
		{
			var (Rep, ReportDataSource) = await PickReport( reportSettings.Description );

			if( Rep.IsNotNullOrWhiteSpace() && ( ReportDataSource != null ) )
			{
				var Local = ReportViewer.LocalReport;
				Local.EnableExternalImages = true; // Needed for loading the barcode
				Local.ReportEmbeddedResource = Rep;

				//var ReportDataSource = new ReportDataSource
				//{
				//    Name = "DataSet1",
				//    // Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
				//    // Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
				//    //Value = new DataSets.WeighbillDetailsList(trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone)
				//    Value = new DataSets.LinfoxShipmentReportDetailsList(reportSettings)
				//};
				Local.DataSources.Clear();
				Local.DataSources.Add( ReportDataSource );

				//System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
				//setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
				//setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
				//setup.Landscape = false;
				//this.ReportViewer.SetPageSettings(setup);

				// From https://stackoverflow.com/questions/44390663/c-using-reportviewer-to-print-on-a-custom-size-paper
				//System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
				//setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
				//setup.Landscape = false;
				//setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
				//this.ReportViewer.SetPageSettings(setup);

				// Trying DataTable
				//Local.DataSources.Clear();
				//Local.DataSources.Add(mainDataSource);

				try
				{
					/*
					tmp = (Reporting.DataSets.WeighbillDetailsList)this.ReportViewer.LocalReport.DataSources.First().Value;
					tmp2 = tmp.First();
					Console.WriteLine("ReportDataSource.Value from ReportViewer.LocalReport: " + tmp2.ToString());
					*/
					// this.ReportViewer.LocalReport.Refresh();
					ReportViewer.RefreshReport();
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception caught: " + E );
				}
			}
		});
	}

	/// <summary>
	///     Picks the correct settings based upon the report's description.
	/// </summary>
	/// <param
	///     name="description">
	/// </param>
	/// <returns></returns>
	private async Task<(string report, ReportDataSource rds)> PickReport( string description )
	{
		var              Report = string.Empty;
		ReportDataSource Rds = null;

		if( description == LinfoxReportName )
		{
			Report = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.LinfoxShipmentReport.rdlc";

			Rds = new ReportDataSource
			      {
				      Name = "DataSet1",

				      // Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
				      // Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
				      //Value = new DataSets.WeighbillDetailsList(trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone)
				      Value = await new Reporting.DataSets.LinfoxShipmentReportDetailsList( ReportSettings ).Load()
			      };
		}
		else if (description == PhoenixInvoiceName)
		{
            Report = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Phoenix.PhoenixInvoiceWaybill.rdlc";

			Rds = new ReportDataSource
			{
				Name = "DataSet1",

				// Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
				// Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
				//Value = new DataSets.WeighbillDetailsList(trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone)
				//Value = await new Reporting.DataSets.LinfoxShipmentReportDetailsList(ReportSettings).Load()
				Value = new Reporting.DataSets.PhoenixInvoiceWaybillDetailsList((Invoicing.InvoicingModel.InvoiceReportSettings)ReportSettings)
            };
        }
		
		return ( Report, Rds );
	}

    private void BtnPrint_Click(object sender, RoutedEventArgs e)
    {

    }

    private void BtnClose_Click(object sender, RoutedEventArgs e)
    {
		// TODO - Check that it has been printed
		Close();
    }
}
//#endif