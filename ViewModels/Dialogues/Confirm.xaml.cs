﻿namespace ViewModels.Dialogues;

/// <summary>
///     Interaction logic for Error.xaml
/// </summary>
public partial class Confirm : Window
{
	private readonly string Text;

	public new bool ShowDialog() => base.ShowDialog() ?? false;

	public Confirm( Window owner, string text )
	{
		Text  = text;
		Owner = owner;
		InitializeComponent();
	}

	public Confirm( string text ) : this( Application.Current.MainWindow, text )
	{
	}


	private void Window_Loaded( object sender, RoutedEventArgs e )
	{
		if( DataContext is ConfirmModel Model )
			Model.Text = Text;
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		DialogResult = true;
	}

	private void Button_Click_1( object sender, RoutedEventArgs e )
	{
		DialogResult = false;
	}
}