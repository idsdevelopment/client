﻿using System.Drawing.Printing;
using System.Reflection;
using Microsoft.Reporting.WinForms;
using ViewModels.Reporting;
using ViewModels.ReportViewer;
using ViewModels.Reports.WebReports;
using Microsoft.Web.WebView2.Core;
using System.Windows.Controls;

//using IdsRemoteService.IdsRemoteServiceReference;
//using IdsControlLibraryV2.ReportViewer;

//using ViewModels.Trips.Boards.Common;

namespace ViewModels.Dialogues;

/// <summary>
///     Interaction logic for WeighbillPage.xaml
/// </summary>
public partial class WeighbillDialog : Window
{
	private          Trip   trip;
	private readonly bool   isCOD;
	private readonly string codAmount;
	private readonly bool   isShowTripIdPlusPieceCount;

	private readonly string PrinterName;

	// Use to prompt if weighbill hasn't been printed when btnClose is clicked
	private bool Printed;

	public void setTrip( Trip _trip )
	{
		trip = _trip;
	}

	protected override void OnClosing( CancelEventArgs e )
	{
		if( !Printed )
		{
			// TODO fix this
			var Choice = MessageBox.Show( "The waybill hasn't been printed.\nAre you sure?", "Waybill Not Printed", MessageBoxButton.YesNo, MessageBoxImage.Question );

			if( Choice == MessageBoxResult.No )
			{
				base.OnClosing( e );
				e.Cancel = true;
			}

			//if (Choice == MessageBoxResult.Yes)
			//{
			//    this.Close();
			//}
			//else
			//{
			//    base.OnClosing(e);
			//    e.Cancel = true;
			//}
		}

		//else
		//{
		//    this.Close();
		//}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="printer">
	/// </param>
	/// <param
	///     name="_trip">
	/// </param>
	/// <param
	///     name="_isCOD">
	/// </param>
	/// <param
	///     name="_codAmount">
	/// </param>
	/// <param
	///     name="_showTripIdPlusPieceCount">
	/// </param>
	/// <param
	///     name="resellerName">
	/// </param>
	/// <param
	///     name="resellerPhone">
	/// </param>
	//	public WeighbillDialog( string printer, Trip _trip, bool _isCOD, string _codAmount, bool _showTripIdPlusPieceCount,
	//	                        string resellerName, string resellerPhone )
	//	{
	//		WindowStartupLocation = WindowStartupLocation.CenterOwner;

	//		PrinterName = printer;
	//		InitializeComponent();

	//		trip                       = _trip;
	//		isCOD                      = _isCOD;
	//		codAmount                  = _codAmount;
	//		isShowTripIdPlusPieceCount = _showTripIdPlusPieceCount;

	//		// this.Title = Util.FindStringResource("DialoguesWeighBillDialogTitle") + " : " + _trip.pieces + (_trip.pieces > 1 ? " Pages" : " Page");
	//		if( trip.Pieces == 1 )
	//			Title = (string)Application.Current.TryFindResource( "DialoguesWeighBillDialogTitle1Page" );
	//		else
	//		{
	//			var title = (string)Application.Current.TryFindResource( "DialoguesWeighBillDialogTitleMorePages" );
	//			Title = title.Replace( "@1", trip.Pieces + "" );
	//		}

	//		// var Opt = ReportBase.ReportOptions;
	//		// Opt.InitialiseDataset();
	//		//this.ReportViewer.Reset();

	//		// DataSet ReportDataSet = new DataSet();
	//		// ReportDataSource customReportData = new ReportDataSource();

	//		// customReportData.Name = "Weighbills/Test";
	//		// string Report = "Weighbills.Test.rdlc";
	//		//string Report = "Weighbills.ShipNow.rdlc";

	//		// this.ReportViewer.LocalReport.ReportPath = "Test.rdlc";
	//		// this.ReportViewer.LocalReport.ReportPath = "Reports/Weighbills/Test.rdlc";
	//		// this.ReportViewer.LocalReport.ReportPath = Assembly.GetExecutingAssembly().GetName().Name + "/Reports/Weighbills/Test.rdlc";
	//		// this.ReportViewer.LocalReport.ReportPath = ".\\" + Assembly.GetExecutingAssembly().GetName().Name + "\\Reports\\Weighbills\\Test.rdlc";
	//		// this.ReportViewer.LocalReport.ReportPath = "c:\\Users\\user\\work\\shipnow2\\" + Assembly.GetExecutingAssembly().GetName().Name + "\\Reports\\Weighbills\\Test.rdlc";

	//		//this.ReportViewer.ProcessingMode = ProcessingMode.Local;
	//		//var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports." + Report;
	//		//var Local = this.ReportViewer.LocalReport;
	//		//Local.ReportEmbeddedResource = Rep;

	//		/*          var Local = this.ReportViewer.LocalReport;
	//					Local.ReportEmbeddedResource = Rep;

	//					Local.DataSources.Clear(); // Suggested by Terry
	//					var wbd = new Reporting.DataSets.WeighbillDetails();
	//					List<Reporting.DataSets.WeighbillDetails> list = new List<Reporting.DataSets.WeighbillDetails>();
	//					list.Add(wbd);
	//					Local.DataSources.Add(new ReportDataSource("DataSet1", new Reporting.DataSets.WeighbillDetailsList(list)));
	//		*/

	//		//var ReportDataSource = new ReportDataSource
	//		//{
	//		//    Name = "DataSet1",
	//		//    Value = new Reporting.DataSets.WeighbillDetailsList()
	//		//};

	//		//Local.DataSources.Clear();
	//		//Local.DataSources.Add(ReportDataSource);

	//		/*
	//		 this.ReportViewer.LocalReport.DataSources.Clear(); // Suggested by Terry
	//		var wbd = new Reporting.DataSets.WeighbillDetails();
	//		List<Reporting.DataSets.WeighbillDetails> list = new List<Reporting.DataSets.WeighbillDetails>();
	//		list.Add(wbd);
	//		this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", new Reporting.DataSets.WeighbillDetailsList(list)));
	//		*/

	//		/*
	//					// This leads to a blank report - Terry says that this is caused by the data not being found
	//					DataTable mainDataTable = new DataTable();
	//					mainDataTable.Columns.Add("CARRIER_NAME");
	//					mainDataTable.Columns.Add("PU_ADDRESS");
	//					mainDataTable.Columns.Add("SERVICE");
	//					mainDataTable.Columns.Add("PU_CONTACT");
	//					mainDataTable.Columns.Add("DEL_CONTACT");
	//					mainDataTable.Columns.Add("DEL_PHONE");
	//					mainDataTable.Columns.Add("DEL_ADDRESS");
	//					mainDataTable.Columns.Add("CALLTIME");
	//					mainDataTable.Columns.Add("WEIGHT");
	//					mainDataTable.Columns.Add("BARCODE");
	//					mainDataTable.Columns.Add("TRIPID");
	//					mainDataTable.Columns.Add("DEL_ZONE");
	//					mainDataTable.Columns.Add("DEL_NOTES");
	//					mainDataTable.Columns.Add("PIECE_COUNT");

	//					// https://stackoverflow.com/questions/1042618/how-to-create-a-datatable-in-c-sharp-and-how-to-add-rows
	//					DataRow row = mainDataTable.NewRow();
	//					row["CARRIER_NAME"] = "IDS";
	//					row["PU_ADDRESS"] = "Pickup Address";
	//					row["SERVICE"] = "Service Type";
	//					row["PU_CONTACT"] = "Pickup Contact";
	//					row["DEL_CONTACT"] = "Delivery Contact";
	//					row["DEL_PHONE"] = "Delivery Phone";
	//					row["DEL_ADDRESS"] = "Delivey Address";
	//					row["CALLTIME"] = "Calltime";
	//					row["WEIGHT"] = "Weight";
	//					row["BARCODE"] = "Barcode";
	//					row["TRIPID"] = "Trip ID";
	//					row["DEL_ZONE"] = "Delivery Zone";
	//					row["DEL_NOTES"] = "Delivery Notes";
	//					row["PIECE_COUNT"] = "Piece Count";

	//					mainDataTable.Rows.Add(row);

	//					ReportDataSource mainDataSource = new ReportDataSource("DataSet1", mainDataTable);
	//		*/

	//		// this.ReportViewer.LocalReport.DataSources.Clear();
	//		// this.ReportViewer.LocalReport.DataSources.Add(mainDataSource);

	//		// var ReportDataSource = new ReportDataSource("rds", values);
	//		/* {
	//			//Name = Opt.DataSourceName,
	//			//Value = Opt.DataSource
	//			Name = "CARRIER_NAME",
	//			Value = "IDS"
	//		}; */
	//		/*
	//		Menus.Parameters parameters = new Menus.Parameters();
	//		parameters.Add("CARRIER_NAME", "IDS");
	//		parameters.Add("PU_ADDRESS", "Pickup Address");

	//		var tmp = new List<ReportParameter>();

	//		if (parameters.Count > 0)
	//		{
	//			foreach (System.Collections.Generic.KeyValuePair<string, string> param in parameters)
	//			{
	//				tmp.Add(new ReportParameter(param.Key, param.Value));
	//			}
	//		}

	//		//Local.SetParameters(parameters.Select(parameter => new ReportParameter(parameter.Key, parameter.Value)).ToArray());
	//		Local.SetParameters(tmp);
	//		*/
	//		// Local.DataSources.Clear();
	//		// Local.DataSources.Add(ReportDataSource);
	//		/* ReportData rd = new ReportData();
	//		foreach (System.Collections.Generic.KeyValuePair<string, string> tmp in rd)
	//		{
	//			Console.WriteLine("key: " + tmp.Key + ", value: " + tmp.Value);
	//		}

	//		var ReportDataSource = new ReportDataSource("rds", rd);
	//		Local.DataSources.Add(ReportDataSource);
	//		*/

	//		//var wbd = new WeighbillData();
	//		//var reportDataSource = new ReportDataSource("rds", wbd);
	//		//Local.DataSources.Add(reportDataSource);

	////			Utilities.LoadNativeAssemblies( AppDomain.CurrentDomain.BaseDirectory );

	//		ReportViewer.Reset();
	//		ReportViewer.ProcessingMode              = ProcessingMode.Local;
	//		ReportViewer.PrinterSettings.PrinterName = printer;

	//		// string Report = "Weighbills.ShipNow.rdlc";
	//		//var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.ShipNow.rdlc";
	//		//var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Zebra_Intermountain.rdlc";
	//		//var Rep = MainWindow.REPORT_BASE_NAME + ".Reports.Zebra_Intermountain.rdlc";
	//		//var Rep = "Ids2Wpf.ViewModels.Reports.Zebra_Intermountain.rdlc";
	//		var Rep   = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Zebra_Intermountain.rdlc";
	//		var Local = ReportViewer.LocalReport;
	//		Local.EnableExternalImages   = true; // Needed for loading the barcode
	//		Local.ReportEmbeddedResource = Rep;

	//		var ReportDataSource = new ReportDataSource
	//		                       {
	//			                       Name = "DataSet1",
	//			                       // Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
	//			                       // Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
	//			                       Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone )
	//		                       };
	//		Local.DataSources.Clear();
	//		Local.DataSources.Add( ReportDataSource );

	//		//System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
	//		//setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
	//		//setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
	//		//setup.Landscape = false;
	//		//this.ReportViewer.SetPageSettings(setup);

	//		// From https://stackoverflow.com/questions/44390663/c-using-reportviewer-to-print-on-a-custom-size-paper
	//		//System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
	//		//setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
	//		//setup.Landscape = false;
	//		//setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
	//		//this.ReportViewer.SetPageSettings(setup);

	//		// Trying DataTable
	//		//Local.DataSources.Clear();
	//		//Local.DataSources.Add(mainDataSource);

	//		try
	//		{
	//			/*
	//								tmp = (Reporting.DataSets.WeighbillDetailsList)this.ReportViewer.LocalReport.DataSources.First().Value;
	//								tmp2 = tmp.First();
	//								Console.WriteLine("ReportDataSource.Value from ReportViewer.LocalReport: " + tmp2.ToString());
	//			*/
	//			//    this.ReportViewer.LocalReport.Refresh();

	//			ReportViewer.RefreshReport();
	//		}
	//		catch( Exception E )
	//		{
	//			Logging.WriteLogLine( "Exception caught: " + E );
	//		}
	//	}

	private readonly WebReportsModel ModelWebReports;
	public WeighbillDialog(string printer, Trip _trip, bool _isCOD, string _codAmount, bool _showTripIdPlusPieceCount,
							string resellerName, string resellerPhone)
	{
		WindowStartupLocation = WindowStartupLocation.CenterOwner;

		PrinterName = printer;
		InitializeComponent();

		// From file > diagnostics > reports 
		// dispalys html reports in the browser 
		ModelWebReports = new WebReportsModel();

		WebView2.EnsureCoreWebView2Async();

		ModelWebReports.OnPageAvailable = htmlFileName =>
		{
			try
			{
				WebView2.Source = new Uri($"file://{htmlFileName}");
			}
			catch (Exception Exception)
			{
				Console.WriteLine(Exception);
			}
		};
		//

		trip = _trip;
		var shipmentId = trip.TripId;
		isCOD = _isCOD;
		codAmount = _codAmount;
		isShowTripIdPlusPieceCount = _showTripIdPlusPieceCount;

		var rs = new ReportBrowser.ReportSettings();
		

		Logging.WriteLogLine("Running Driver Manifest shipment report");
		ModelWebReports.ShipmentEntry_Waybill(shipmentId);
	}


	private void BtnClose_Click( object sender, RoutedEventArgs e )
	{
		if( !Printed )
		{
			// TODO fix this
			var Choice = MessageBox.Show( "The waybill hasn't been printed.\nAre you sure?", "Waybill Not Printed", MessageBoxButton.YesNo, MessageBoxImage.Question );

			if( Choice == MessageBoxResult.Yes )
			{
				Printed = true;
				Close();
			}
		}
		else
			Close();
	}

    //private void BtnPrint_Click( object sender, RoutedEventArgs e )
    //{
    //	var Report   = ReportViewer.LocalReport;
    //	var Settings = Report.GetDefaultPageSettings();
    //	Logging.WriteLogLine( "WeighbillDialog: BtnPrint_Click: settings paper size: " + Settings.PaperSize );
    //	Logging.WriteLogLine( "WeighbillDialog: BtnPrint_Click: settings margins: " + Settings.Margins );

    //	//// From https://stackoverflow.com/questions/44390663/c-using-reportviewer-to-print-on-a-custom-size-paper
    //	//System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
    //	//setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
    //	//setup.Landscape = false;
    //	//setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
    //	//this.ReportViewer.SetPageSettings(setup);
    //	//ReportViewer.RefreshReport();

    //	// ReportViewer.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);
    //	// End of from

    //	// PrintDialog printDialog = new PrintDialog();
    //	var setup = new PageSettings();
    //	setup.PaperSize = new PaperSize( "CustomType", 400, 590 );
    //	setup.Margins   = new Margins( 0, 0, 0, 0 );
    //	setup.Landscape = false;
    //	ReportViewer.SetPageSettings( setup );
    //	ReportViewer.RefreshReport();

    //	ReportViewer.PageCountMode = PageCountMode.Actual;
    //	Logging.WriteLogLine( "WeighbillDialog: BtnPrint_Click: this.ReportViewer.GetTotalPages(): " + ReportViewer.GetTotalPages() );
    //	Printed = ReportViewer.LocalReport.PrintToPrinter( PrinterName );

    //	//try
    //	//{
    //	//    ReportViewer.PrintDialog();
    //	//}
    //	//catch 
    //	//{

    //	//}
    //	// Console.WriteLine( Printed ? "WeighbillDialog: BtnPrint_Click: print" : "WeighbillDialog: BtnPrint_Click: Cancelled" );
    //}

    #region WebView2 print options - Test

    CoreWebView2PrintSettings GetSelectedPrinterPrintSettings(string printerName)
    {
        CoreWebView2PrintSettings printSettings = null;
        printSettings = WebView2.CoreWebView2.Environment.CreatePrintSettings();
        printSettings.ShouldPrintBackgrounds = true;
        printSettings.ShouldPrintHeaderAndFooter = true;

        return printSettings;

        // or
        //
        // Get PrintQueue for the selected printer and use GetPrintCapabilities() of PrintQueue from System.Printing
        // to get the capabilities of the selected printer.
        // Display the printer capabilities to the user along with the page settings.
        // Return the user selected settings.
    }

    private async void BtnPrint_Click(object sender, RoutedEventArgs e)
    {

        //await CoreWebView2Environment.CreateAsync();
        //CoreWebView2PrintSettings printSettings = null;
        //printSettings = WebView2.CoreWebView2.Environment.CreatePrintSettings();
        //printSettings.PageWidth = 400;
        //printSettings.PageHeight = 590;
        //printSettings.MarginLeft = 0;
        //printSettings.MarginRight = 0;
        //printSettings.HeaderTitle = "Header Title";
        //printSettings.ShouldPrintSelectionOnly = true;

        //printSettings.Orientation = CoreWebView2PrintOrientation.Portrait;

        //await WebView2.CoreWebView2.PrintToPdfAsync("C:/Users/Rowan/Desktop/IDS", printSettings);


        string printerName = PrinterName;
        CoreWebView2PrintSettings printSettings = GetSelectedPrinterPrintSettings(printerName);
        string title = WebView2.CoreWebView2.DocumentTitle;
        try
        {
            bool printStatus = await WebView2.CoreWebView2.PrintToPdfAsync("C:/Users/Rowan/Desktop/IDS", printSettings);

            if (printStatus == true)
            {
                MessageBox.Show(this, "Printing " + title + " document to printer is succeeded", "Print to printer");
            }
            else if (printStatus == false)
            {
                MessageBox.Show(this, "Selected printer is not found, not available, offline or error state", "Print to printer");
            }
            else
            {
                MessageBox.Show(this, "Printing " + title + " document to printer is failed",
                    "Print");
            }
        }
        catch (ArgumentException)
        {
            MessageBox.Show(this, "Invalid settings provided for the specified printer",
                "Print");
        }
        catch (Exception)
        {
            MessageBox.Show(this, "Printing " + title + " document already in progress",
                    "Print");

        }
    }

    #endregion

}