﻿namespace ViewModels.Dialogues;

public class ErrorModel : ViewModelBase
{
	public string Title
	{
		get { return Get( () => Title, "Default Text" ); }
		set { Set( () => Title, value ); }
	}

	public string Text
	{
		get { return Get( () => Text, "Default Text" ); }
		set { Set( () => Text, value ); }
	}

	public ErrorModel()
	{
		Title = Globals.Dictionary.AsString( "Error" );
	}
}