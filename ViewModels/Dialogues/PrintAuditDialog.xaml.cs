﻿using System.Reflection;
using Microsoft.Reporting.WinForms;
using static ViewModels.Reporting.DataSets;

namespace ViewModels.Dialogues;

/// <summary>
///     Interaction logic for PrintAuditDialog.xaml
/// </summary>
public partial class PrintAuditDialog : Window
{
	public PrintAuditDialog( Trip trip, List<AuditLine> auditLines, string title = "Audit for Item" )
	{
		InitializeComponent();

		Title = title;

		//var title = (string)Application.Current.TryFindResource("DialoguesTrackingDialogTitle");
		//Title = title.Replace("@1", trip.TripId);

		AuditReportViewer.Reset();
		AuditReportViewer.ProcessingMode = ProcessingMode.Local;

		// TODO 
		var Rep   = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.TripAudit.rdlc";
		var Local = AuditReportViewer.LocalReport;
		Local.EnableExternalImages   = true; // Needed for loading the barcode
		Local.ReportEmbeddedResource = Rep;

		var ReportDataSource = new ReportDataSource
		                       {
			                       Name  = "DataSet1",
			                       Value = new AuditDetailsList( trip, auditLines )
		                       };

		Local.DataSources.Clear();
		Local.DataSources.Add( ReportDataSource );

		try
		{
			AuditReportViewer.RefreshReport();
			//this.reportLoaded = true;
			btnPrint.IsEnabled = true;
		}
		catch( Exception exception )
		{
			Logging.WriteLogLine( "ERROR: Trying to generate report: " + exception );
		}
	}

	private void BtnPrint_Click( object sender, RoutedEventArgs e )
	{
		var Report   = AuditReportViewer.LocalReport;
		var Settings = Report.GetDefaultPageSettings();

		Logging.WriteLogLine( "settings paper size: " + Settings.PaperSize );
		Logging.WriteLogLine( "settings margins: " + Settings.Margins );
		AuditReportViewer.PageCountMode = PageCountMode.Actual;
		Logging.WriteLogLine( "this.ManifestViewer.GetTotalPages(): " + AuditReportViewer.GetTotalPages() );

		try
		{
			AuditReportViewer.PrintDialog();
		}
		catch( Exception exception )
		{
			Logging.WriteLogLine( "Exception: " + "Message: " + exception.Message + ", " + exception.StackTrace );
		}
	}

	private void BtnClose_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}
}