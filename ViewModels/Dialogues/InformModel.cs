﻿#nullable enable

namespace ViewModels.Dialogues;

public class InformModel : ViewModelBase
{
	public string Text
	{
		get { return Get( () => Text, "Default Text" ); }
		set { Set( () => Text, value ); }
	}
}