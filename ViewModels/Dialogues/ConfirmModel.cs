﻿namespace ViewModels.Dialogues;

public class ConfirmModel : ViewModelBase
{
	public string Text
	{
		get { return Get( () => Text, "Default Text" ); }
		set { Set( () => Text, value ); }
	}
}