﻿#nullable enable

using System.Windows.Input;

// ReSharper disable InconsistentNaming

namespace ViewModels.Roles;

public class CheckableRole : INotifyPropertyChanged
{
	public bool IsRename => OriginalName.IsNotNullOrWhiteSpace() && ( Name != OriginalName );

	public string Name
	{
		get => _Name;
		set
		{
			_Name = value;
			OnPropertyChanged();
		}
	}

	public bool Checked
	{
		get => _Checked;
		set
		{
			_Checked = value;
			OnPropertyChanged();
		}
	}

	public bool IsAdmin
	{
		get => _IsAdmin;
		set
		{
			_IsAdmin = value;
			OnPropertyChanged();
		}
	}

	public bool IsDriver
	{
		get => _IsDriver;
		set
		{
			_IsDriver = value;
			OnPropertyChanged();
		}
	}

	private bool _Checked;

	private bool _IsAdmin;

	private bool _IsDriver;

	private string _Name = "";

	public MaintainRolesModel.OnAddRoleEvent? OnAddRole;
	public OnChangedEvent?                    OnChanged;

	public string OriginalName;
	public Role   Role;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		OnChanged?.Invoke( propertyName, this );
	}

	public CheckableRole( string name, string json )
	{
		OriginalName = name;
		Name         = name;
		Role         = json.IsNotNullOrWhiteSpace() ? Role.FromJson( json ) : new Role();
	}

	public CheckableRole( string name ) : this( name, "" )
	{
	}

	public CheckableRole( Protocol.Data.Role role ) : this( role.Name, role.JsonObject )
	{
		IsAdmin  = role.IsAdministrator;
		IsDriver = role.IsDriver;
	}

	public delegate Task OnChangedEvent( string? propertyName, CheckableRole checkableRole );

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class MaintainRolesModel : ViewModelBase
{
	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638124048/How+to+Maintain+Roles" ); }
	}

	public ObservableCollection<CheckableRole> Roles
	{
		get { return Get( () => Roles, new ObservableCollection<CheckableRole>() ); }
		set { Set( () => Roles, value ); }
	}


	public CheckableRole SelectedRole
	{
		get { return Get( () => SelectedRole, new CheckableRole( "", "" ) ); }
		set { Set( () => SelectedRole, value ); }
	}

	public bool EnableReports
	{
		get { return Get( () => EnableReports, false ); }
		set { Set( () => EnableReports, value ); }
	}

	public bool EnableEdit
	{
		get { return Get( () => EnableEdit, false ); }
		set { Set( () => EnableEdit, value ); }
	}


	public bool EnableRateWizard
	{
		get { return Get( () => EnableRateWizard, false ); }
		set { Set( () => EnableRateWizard, value ); }
	}


	public bool EnableBoards
	{
		get { return Get( () => EnableBoards, false ); }
		set { Set( () => EnableBoards, value ); }
	}


	public bool EnableDriversBoard
	{
		get { return Get( () => EnableDriversBoard, false ); }
		set { Set( () => EnableDriversBoard, value ); }
	}


	public bool EnableDispatchBoard
	{
		get { return Get( () => EnableDispatchBoard, false ); }
		set { Set( () => EnableDispatchBoard, value ); }
	}


	public bool EnablePreferences
	{
		get { return Get( () => EnablePreferences, false ); }
		set { Set( () => EnablePreferences, value ); }
	}


	public bool EnableRoles
	{
		get { return Get( () => EnableRoles, false ); }
		set { Set( () => EnableRoles, value ); }
	}

	public bool EnableSearch
	{
		get { return Get( () => EnableSearch, false ); }
		set { Set( () => EnableSearch, value ); }
	}


	public bool EnableAudit
	{
		get { return Get( () => EnableAudit, false ); }
		set { Set( () => EnableAudit, value ); }
	}


	public bool EnableAuditStaff
	{
		get { return Get( () => EnableAuditStaff, false ); }
		set { Set( () => EnableAuditStaff, value ); }
	}


	public bool EnableAuditTrip
	{
		get { return Get( () => EnableAuditTrip, false ); }
		set { Set( () => EnableAuditTrip, value ); }
	}


	public bool EnableRoutes
	{
		get { return Get( () => EnableRoutes, false ); }
		set { Set( () => EnableRoutes, value ); }
	}


	public bool EnableDeleteButton
	{
		get { return Get( () => EnableDeleteButton, false ); }
		set { Set( () => EnableDeleteButton, value ); }
	}


	public bool EnableLogs
	{
		get { return Get( () => EnableLogs, false ); }
		set { Set( () => EnableLogs, value ); }
	}


	public bool EnableViewCurrentLog
	{
		get { return Get( () => EnableViewCurrentLog, false ); }
		set { Set( () => EnableViewCurrentLog, value ); }
	}


	public bool EnableExploreLog
	{
		get { return Get( () => EnableExploreLog, false ); }
		set { Set( () => EnableExploreLog, value ); }
	}


	public bool EnableCustomers
	{
		get { return Get( () => EnableCustomers, false ); }
		set { Set( () => EnableCustomers, value ); }
	}


	public bool EnableCustomerAccounts
	{
		get { return Get( () => EnableCustomerAccounts, false ); }
		set { Set( () => EnableCustomerAccounts, value ); }
	}


	public bool EnableCustomerAddresses
	{
		get { return Get( () => EnableCustomerAddresses, false ); }
		set { Set( () => EnableCustomerAddresses, value ); }
	}


	public ICommand DeleteSelectedRoles => Commands[ nameof( DeleteSelectedRoles ) ];


	public List<CheckableRole> ChangedRoles
	{
		get { return Get( () => ChangedRoles, new List<CheckableRole>() ); }
		set { Set( () => ChangedRoles, value ); }
	}


	public bool EnableSaveButton
	{
		get { return Get( () => EnableSaveButton, false ); }
		set { Set( () => EnableSaveButton, value ); }
	}

	private bool InLoadMenu;

	public OnAddRoleEvent? OnAddRole;

	[DependsUpon( nameof( SelectedRole ) )]
	public void WhenSelectedRoleChanges()
	{
		InLoadMenu = true;

		var Sr = SelectedRole;

		var HasInventory = HasInventoryModule;

		if( Sr.IsNotNull() && Sr.Role.IsNotNull() )
		{
			EnableEdit = true;
			var R = Sr.Role;

			EnableInventory = R.Inventory && HasInventory;

			EnablePreferences = R.Preferences;
			EnableRoles       = R.Roles;

			EnableShipments        = R.Shipments;
			EnableShipment         = R.Shipment;
			EnableAdvancedShipment = R.AdvancesShipment;

			EnableSearch = R.Search;

			EnableReports = R.Reports;

			EnableAudit      = R.Audit;
			EnableAuditStaff = R.AuditStaff;
			EnableAuditTrip  = R.AuditTrip;

		#region Staff
			EnableStaff         = R.Staff;
			EnableMaintainStaff = R.MaintainStaff;
			EnableShiftRates    = R.ShiftRates;
		#endregion

			EnableRoutes = R.Routes;

			EnableBoards        = R.Boards;
			EnableDispatchBoard = R.DispatchBoard;
			EnableDriversBoard  = R.DriversBoard;

			EnableRateWizard = R.RateWizard;

			EnableLogs           = R.Logs;
			EnableViewCurrentLog = R.ViewLog;
			EnableExploreLog     = R.ExploreLog;

			EnableCustomers         = R.Customers;
			EnableCustomerAccounts  = R.CustomerAccounts;
			EnableCustomerAddresses = R.CustomerAddresses;

			EnablePML           = R.Pml;
			EnablePML_Products  = R.PmlProducts;
			EnablePML_Shipments = R.PmlShipments;
		}
		else
		{
			EnableEdit = false;

			EnableInventory = false;

			EnablePreferences = false;
			EnableRoles       = false;

			EnableShipments = false;
			EnableSearch    = false;

			EnableReports = false;

			EnableAudit      = false;
			EnableAuditStaff = false;
			EnableAuditTrip  = false;

		#region Staff
			EnableStaff         = false;
			EnableMaintainStaff = false;
			EnableShiftRates    = false;
		#endregion

			EnableRoutes = false;

			EnableBoards        = false;
			EnableDispatchBoard = false;
			EnableDriversBoard  = false;

			EnableRateWizard = false;

			EnableLogs           = false;
			EnableViewCurrentLog = false;
			EnableExploreLog     = false;

			EnableCustomers         = false;
			EnableCustomerAccounts  = false;
			EnableCustomerAddresses = false;

			EnablePML           = false;
			EnablePML_Products  = false;
			EnablePML_Shipments = false;
		}

		InLoadMenu = false;
	}

	[DependsUpon( nameof( EnableInventory ) )]
	[DependsUpon( nameof( EnablePreferences ) )]
	[DependsUpon( nameof( EnableRoles ) )]
	[DependsUpon( nameof( EnableShipments ) )]
	[DependsUpon( nameof( EnableShipment ) )]
	[DependsUpon( nameof( EnableAdvancedShipment ) )]
	[DependsUpon( nameof( EnableSearch ) )]
	[DependsUpon( nameof( EnableReports ) )]
	[DependsUpon( nameof( EnableAudit ) )]
	[DependsUpon( nameof( EnableAuditStaff ) )]
	[DependsUpon( nameof( EnableAuditTrip ) )]
	[DependsUpon( nameof( EnableRoutes ) )]
	[DependsUpon( nameof( EnableBoards ) )]
	[DependsUpon( nameof( EnableDriversBoard ) )]
	[DependsUpon( nameof( EnableDispatchBoard ) )]
	[DependsUpon( nameof( EnableRateWizard ) )]
	[DependsUpon( nameof( EnableLogs ) )]
	[DependsUpon( nameof( EnableViewCurrentLog ) )]
	[DependsUpon( nameof( EnableExploreLog ) )]
	[DependsUpon( nameof( EnableCustomers ) )]
	[DependsUpon( nameof( EnableCustomerAccounts ) )]
	[DependsUpon( nameof( EnableCustomerAddresses ) )]
	[DependsUpon( nameof( EnablePML ) )]
	[DependsUpon( nameof( EnablePML_Products ) )]
	[DependsUpon( nameof( EnablePML_Shipments ) )]
	[DependsUpon( nameof( EnableStaff ) )]
	[DependsUpon( nameof( EnableMaintainStaff ) )]
	[DependsUpon( nameof( EnableShiftRates ) )]
	public async Task WhenMenuChanges()
	{
		if( !InLoadMenu )
		{
			var Sr = SelectedRole;
			var R  = SelectedRole.Role;

			var HasInventory = HasInventoryModule;

			R.Inventory = EnableInventory && HasInventory;

			R.Preferences = EnablePreferences;
			R.Roles       = EnableRoles;

			R.Shipments        = EnableShipments;
			R.Shipment         = EnableShipment;
			R.AdvancesShipment = EnableAdvancedShipment;

			R.Search = EnableSearch;

			R.Reports = EnableReports;

			R.Audit      = EnableAudit;
			R.AuditStaff = EnableAuditStaff;
			R.AuditTrip  = EnableAuditTrip;

		#region Staff
			R.Staff         = EnableStaff;
			R.MaintainStaff = EnableMaintainStaff;
			R.ShiftRates    = EnableShiftRates;
		#endregion

			R.Routes        = EnableRoutes;
			R.Boards        = EnableBoards;
			R.DriversBoard  = EnableDriversBoard;
			R.DispatchBoard = EnableDispatchBoard;
			R.RateWizard    = EnableRateWizard;

			R.Logs       = EnableLogs;
			R.ViewLog    = EnableViewCurrentLog;
			R.ExploreLog = EnableExploreLog;

			R.Customers         = EnableCustomers;
			R.CustomerAccounts  = EnableCustomerAccounts;
			R.CustomerAddresses = EnableCustomerAddresses;

			R.Pml          = EnablePML;
			R.PmlProducts  = EnablePML_Products;
			R.PmlShipments = EnablePML_Shipments;

			await OnChanged( "", Sr );
		}
	}

	public async Task Execute_DeleteSelectedRoles()
	{
		var DRoles = new DeleteRoles();
		var R      = Roles;

		for( var I = R.Count; --I >= 0; )
		{
			var Rl = R[ I ];

			if( Rl.Checked )
			{
				DRoles.Add( Rl.Name );
				R.RemoveAt( I );
			}

			await Azure.Client.RequestDeleteRoles( DRoles );
		}
	}


	public async Task Execute_SaveRoles()
	{
		Logging.WriteLogLine( "DEBUG Saving " + ChangedRoles.Count + " roles" );

		if( !IsInDesignMode )
		{
			if( ChangedRoles.Count > 0 )
			{
				foreach( var c in ChangedRoles )
				{
					if( c.IsRename )
					{
						await Azure.Client.RequestRenameRole( new RoleRename
						                                      {
							                                      NewName = c.Name,
							                                      Name    = c.OriginalName,

							                                      IsAdministrator = c.IsAdmin,
							                                      IsDriver        = c.IsDriver,
							                                      JsonObject      = GetRole().ToJson
						                                      } );
					}
					else
					{
						await Azure.Client.RequestAddUpdateRole( new Protocol.Data.Role
						                                         {
							                                         Name            = c.Name,
							                                         IsAdministrator = c.IsAdmin,
							                                         IsDriver        = c.IsDriver,
							                                         JsonObject      = GetRole().ToJson
						                                         } );
					}
				}
			}

			Logging.WriteLogLine( "DEBUG Clearing " + ChangedRoles.Count + " roles" );
			ChangedRoles.Clear();
			EnableSaveButton = false;
		}
	}

	public MaintainRolesModel()
	{
		if( !IsInDesignMode )
		{
			HasInventoryModule = Globals.Modules.HasInventoryModule;

			_ = Task.Run( async () =>
			              {
				              var RequestRoles = await Azure.Client.RequestRoles();
				              var Prefs        = await Azure.Client.RequestMenuPreferences();
				              IsPml = Prefs.IsPml;

				              var Rls = new ObservableCollection<CheckableRole>();

				              foreach( var Role in RequestRoles )
				              {
					              if( !Role.Mandatory )
					              {
						              var R = new CheckableRole( Role ) {OnChanged = OnChanged};
						              Rls.Add( R );
					              }
				              }

				              Dispatcher.Invoke( () =>
				                                 {
					                                 Roles = Rls;
				                                 } );
			              } );
		}
	}

	public delegate void OnAddRoleEvent( CheckableRole newRole );

	private Role GetRole() =>
		new()
		{
			CanView   = true,
			CanCreate = true,
			CanEdit   = true,
			CanDelete = true,

			Inventory = EnableInventory,

			Routes = EnableRoutes,
			Roles  = EnableRoles,

			Shipments        = EnableShipments,
			Shipment         = EnableShipment,
			AdvancesShipment = EnableAdvancedShipment,

			Preferences = EnablePreferences,
			Search      = EnableSearch,

		#region Staff
			Staff         = EnableStaff,
			MaintainStaff = EnableMaintainStaff,
			ShiftRates    = EnableShiftRates,
		#endregion

			Reports = EnableReports,

			Audit      = EnableAudit,
			AuditStaff = EnableAuditStaff,
			AuditTrip  = EnableAuditTrip,

			Boards        = EnableBoards,
			DriversBoard  = EnableDriversBoard,
			DispatchBoard = EnableDispatchBoard,
			RateWizard    = EnableRateWizard,

			Logs       = EnableLogs,
			ViewLog    = EnableViewCurrentLog,
			ExploreLog = EnableExploreLog,

			Customers         = EnableCustomers,
			CustomerAccounts  = EnableCustomerAccounts,
			CustomerAddresses = EnableCustomerAddresses,

			Pml          = EnablePML,
			PmlProducts  = EnablePML_Products,
			PmlShipments = EnablePML_Shipments
		};

	private void SetupAdmin( bool isAdmin )
	{
		InLoadMenu = true;

		EnableEdit = !isAdmin;

		var EnableInv = isAdmin && HasInventoryModule;

		EnableInventory = EnableInv;

		EnablePreferences = isAdmin;
		EnableRoles       = isAdmin;

		EnableShipments        = isAdmin;
		EnableShipment         = isAdmin;
		EnableAdvancedShipment = isAdmin;

		EnableSearch = isAdmin;

		EnableReports = isAdmin;

		EnableAudit      = isAdmin;
		EnableAuditStaff = isAdmin;
		EnableAuditTrip  = isAdmin;

	#region Staff
		EnableStaff         = isAdmin;
		EnableMaintainStaff = isAdmin;
		EnableShiftRates    = isAdmin;
	#endregion

		EnableRoutes = isAdmin;

		EnableBoards        = isAdmin;
		EnableDriversBoard  = isAdmin;
		EnableDispatchBoard = isAdmin;
		EnableRateWizard    = isAdmin;

		EnableLogs           = isAdmin;
		EnableViewCurrentLog = isAdmin;
		EnableExploreLog     = isAdmin;

		EnableCustomers         = isAdmin;
		EnableCustomerAccounts  = isAdmin;
		EnableCustomerAddresses = isAdmin;

		EnablePML           = isAdmin;
		EnablePML_Products  = isAdmin;
		EnablePML_Shipments = isAdmin;

		InLoadMenu = false;
	}

	private async Task OnChanged( string? propertyName, CheckableRole c )
	{
		if( !IsInDesignMode && c.Name.IsNotNullOrWhiteSpace() )
		{
			EnableSaveButton = true;

			switch( propertyName )
			{
			case nameof( CheckableRole.IsAdmin ):
				SetupAdmin( c.IsAdmin );
				await WhenMenuChanges();

				break;

			case nameof( CheckableRole.Checked ):
				if( Roles.Any( Role => Role.Checked ) )
				{
					EnableDeleteButton = true;

					return;
				}

				EnableDeleteButton = false;

				break;

			default:
				if( ChangedRoles.Contains( c ) )
					ChangedRoles.Remove( c );
				ChangedRoles.Add( c );

				//if ( c.IsRename )
				//{
				//                       await Azure.Client.RequestRenameRole(new RoleRename
				//                       {
				//                           NewName = c.Name,
				//                           Name = c.OriginalName,

				//                           IsAdministrator = c.IsAdmin,
				//                           IsDriver = c.IsDriver,
				//                           JsonObject = GetRole().ToJson
				//                       });
				//                   }
				//else
				//{
				//	await Azure.Client.RequestAddUpdateRole( new Protocol.Data.Role
				//	                                         {
				//		                                         Name            = c.Name,
				//		                                         IsAdministrator = c.IsAdmin,
				//		                                         IsDriver        = c.IsDriver,
				//		                                         JsonObject      = GetRole().ToJson
				//	                                         } );
				//}

				break;
			}
		}
	}

#region Add Role
	public ICommand AddRole => Commands[ nameof( AddRole ) ];

	public void Execute_AddRole()
	{
		var R = new CheckableRole( "" ) {OnChanged = OnChanged};
		Roles.Add( R );
		SelectedRole = R;
		OnAddRole?.Invoke( R );
	}
#endregion

#region Shipments
	public bool EnableShipments
	{
		get { return Get( () => EnableShipments, false ); }
		set { Set( () => EnableShipments, value ); }
	}

	public bool EnableShipment
	{
		get { return Get( () => EnableShipment, false ); }
		set { Set( () => EnableShipment, value ); }
	}


	public bool EnableAdvancedShipment
	{
		get { return Get( () => EnableAdvancedShipment, false ); }
		set { Set( () => EnableAdvancedShipment, value ); }
	}
#endregion

#region Staff
	public bool EnableStaff
	{
		get { return Get( () => EnableStaff, false ); }
		set { Set( () => EnableStaff, value ); }
	}


	public bool EnableMaintainStaff
	{
		get { return Get( () => EnableMaintainStaff, false ); }
		set { Set( () => EnableMaintainStaff, value ); }
	}


	public bool EnableShiftRates
	{
		get { return Get( () => EnableShiftRates, false ); }
		set { Set( () => EnableShiftRates, value ); }
	}
#endregion

#region Inventory
	public bool HasInventoryModule
	{
		get { return Get( () => HasInventoryModule, false ); }
		set { Set( () => HasInventoryModule, value ); }
	}

	public bool EnableInventory
	{
		get { return Get( () => EnableInventory, false ); }
		set { Set( () => EnableInventory, value ); }
	}
#endregion

#region Pml
	public bool IsPml
	{
		get { return Get( () => IsPml, false ); }
		set { Set( () => IsPml, value ); }
	}

	public bool EnablePML
	{
		get { return Get( () => EnablePML, false ); }
		set { Set( () => EnablePML, value ); }
	}


	public bool EnablePML_Products
	{
		get { return Get( () => EnablePML_Products, false ); }
		set { Set( () => EnablePML_Products, value ); }
	}


	public bool EnablePML_Shipments
	{
		get { return Get( () => EnablePML_Shipments, false ); }
		set { Set( () => EnablePML_Shipments, value ); }
	}
#endregion
}