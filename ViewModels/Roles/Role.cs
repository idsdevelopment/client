﻿#nullable enable

namespace ViewModels.Roles;

public class Role
{
	public bool CanView   { get; set; }
	public bool CanCreate { get; set; }
	public bool CanEdit   { get; set; }
	public bool CanDelete { get; set; }

	public bool Preferences { get; set; }
	public bool Roles       { get; set; }

	public bool Search { get; set; }

	public bool Boards        { get; set; }
	public bool DriversBoard  { get; set; }
	public bool DispatchBoard { get; set; }

	public bool Reports { get; set; }

	public bool Audit      { get; set; }
	public bool AuditStaff { get; set; }
	public bool AuditTrip  { get; set; }

	public bool Routes { get; set; }

	public bool RateWizard { get; set; }

	public bool Logs       { get; set; }
	public bool ViewLog    { get; set; }
	public bool ExploreLog { get; set; }


	public bool Customers         { get; set; }
	public bool CustomerAccounts  { get; set; }
	public bool CustomerAddresses { get; set; }

	[JsonIgnore]
	public string ToJson => JsonConvert.SerializeObject( this );

#region Inventory
	public bool Inventory { get; set; }
#endregion


	public static Role FromJson( string json ) => JsonConvert.DeserializeObject<Role>( json ) ?? new Role();

#region Staff
	public bool Staff         { get; set; }
	public bool MaintainStaff { get; set; }
	public bool ShiftRates    { get; set; }
#endregion

#region Shipments
	public bool Shipments        { get; set; }
	public bool Shipment         { get; set; }
	public bool AdvancesShipment { get; set; }
#endregion


#region Pml
	public bool Pml          { get; set; }
	public bool PmlProducts  { get; set; }
	public bool PmlShipments { get; set; }
#endregion
}