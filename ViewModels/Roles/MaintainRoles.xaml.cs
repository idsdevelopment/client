﻿using System.Diagnostics;
using System.Windows.Controls;

namespace ViewModels.Roles;

/// <summary>
///     Interaction logic for MaintainRoles.xaml
/// </summary>
public partial class MaintainRoles : Page
{
	public MaintainRoles()
	{
		Loaded += ( _, _ ) =>
		          {
			          if( DataContext is MaintainRolesModel Model )
			          {
				          Model.OnAddRole = newRole =>
				                            {
					                            Dg.ScrollIntoView( newRole );
				                            };
			          }
		          };

		InitializeComponent();
	}

	private void Dg_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
	}

	private void HelpButton_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is MaintainRolesModel Model )
		{
			Logging.WriteLogLine( "Opening " + Model.HelpUri );
			var uri = new Uri( Model.HelpUri );
			Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
		}
	}
}