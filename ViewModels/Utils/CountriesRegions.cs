﻿namespace Utils;

public static class CountriesRegions
{
	public static readonly List<string> Countries = new()
	                                                {
		                                                "Canada",
		                                                "United States",
		                                                "Australia"
	                                                };


	public static readonly List<string> CanadaRegions = new()
	                                                    {
		                                                    "Alberta",
		                                                    "British Columbia",
		                                                    "Manitoba",
		                                                    "New Brunswick",
		                                                    "Newfoundland and Labrador",
		                                                    "Northwest Territories",
		                                                    "Nova Scotia",
		                                                    "Nunavut",
		                                                    "Ontario",
		                                                    "Prince Edward Island",
		                                                    "Quebec",
		                                                    "Saskatchewan",
		                                                    "Yukon Territory"
	                                                    };

	public static readonly List<string> CanadaRegionAbbreviations = new()
	                                                                {
		                                                                "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"
	                                                                };

	public static readonly Dictionary<string, string> CanadaRegionAbbreviationList = new()
	                                                                                 {
		                                                                                 {"Alberta", "AB"},
		                                                                                 {"British Columbia", "BC"},
		                                                                                 {"Manitoba", "MB"},
		                                                                                 {"New Brunswick", "NB"},
		                                                                                 {"Newfoundland and Labrador", "NL"},
		                                                                                 {"Northwest Territories", "NT"},
		                                                                                 {"Nova Scotia", "NS"},
		                                                                                 {"Nunavut", "NU"},
		                                                                                 {"Ontario", "ON"},
		                                                                                 {"Prince Edward Island", "PE"},
		                                                                                 {"Quebec", "QC"},
		                                                                                 {"Saskatchewan", "SK"},
		                                                                                 {"Yukon Territory", "YT"}
	                                                                                 };

	public static readonly List<string> USRegions = new()
	                                                {
		                                                "Alaska",
		                                                "Alabama",
		                                                "Arkansas",
		                                                "Arizona",
		                                                "California",
		                                                "Colorado",
		                                                "Conneticut",
		                                                "Delaware",
		                                                "District of Columbia",
		                                                "Florida",
		                                                "Georgia",
		                                                "Hawaii",
		                                                "Iowa",
		                                                "Idaho",
		                                                "Illinois",
		                                                "Indiana",
		                                                "Kansas",
		                                                "Kentucky",
		                                                "Louisiana",
		                                                "Massachusetts",
		                                                "Maryland",
		                                                "Maine",
		                                                "Michigan",
		                                                "Minnesota",
		                                                "Missouri",
		                                                "Mississippi",
		                                                "Montana",
		                                                "North Carolina",
		                                                "North Dakota",
		                                                "Nebraska",
		                                                "Nevada",
		                                                "New Hampshire",
		                                                "New Jersey",
		                                                "New Mexico",
		                                                "New York",
		                                                "Ohio",
		                                                "Oklahoma",
		                                                "Oregon",
		                                                "Pennsylvania",
		                                                "Rhode Island",
		                                                "South Carolina",
		                                                "South Dakota",
		                                                "Tennessee",
		                                                "Texas",
		                                                "Utah",
		                                                "Vermont",
		                                                "Virginia",
		                                                "Washington",
		                                                "Wisconsin",
		                                                "West Virginia",
		                                                "Wyoming"
	                                                };

	public static readonly List<string> USRegionAbbreviations = new()
	                                                            {
		                                                            "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV",
		                                                            "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"
	                                                            };

	public static readonly Dictionary<string, string> USRegionAbbreviationList = new()
	                                                                             {
		                                                                             {"Alaska", "AL"},
		                                                                             {"Alabama", "AL"},
		                                                                             {"Arizona", "AZ"},
		                                                                             {"Arkansas", "AR"},
		                                                                             {"California", "CA"},
		                                                                             {"Colorado", "CO"},
		                                                                             {"Conneticut", "CT"},
		                                                                             {"Delaware", "DE"},
		                                                                             {"District of Columbia", "DC"},
		                                                                             {"Florida", "FL"},
		                                                                             {"Georgia", "GA"},
		                                                                             {"Hawaii", "HI"},
		                                                                             {"Idaho", "ID"},
		                                                                             {"Illinois", "IL"},
		                                                                             {"Indiana", "IN"},
		                                                                             {"Iowa", "IO"},
		                                                                             {"Kansas", "KS"},
		                                                                             {"Kentucky", "KY"},
		                                                                             {"Louisiana", "LA"},
		                                                                             {"Maine", "ME"},
		                                                                             {"Maryland", "MD"},
		                                                                             {"Massachusetts", "MA"},
		                                                                             {"Michigan", "MI"},
		                                                                             {"Minnesota", "MN"},
		                                                                             {"Mississippi", "MS"},
		                                                                             {"Missouri", "MO"},
		                                                                             {"Montana", "MT"},
		                                                                             {"Nebraska", "NE"},
		                                                                             {"Nevada", "NV"},
		                                                                             {"New Hampshire", "NH"},
		                                                                             {"New Jersey", "NJ"},
		                                                                             {"New Mexico", "NM"},
		                                                                             {"New York", "NY"},
		                                                                             {"North Carolina", "NC"},
		                                                                             {"North Dakota", "ND"},
		                                                                             {"Ohio", "OH"},
		                                                                             {"Oklahoma", "OK"},
		                                                                             {"Oregon", "OR"},
		                                                                             {"Pennsylvania", "PA"},
		                                                                             {"Rhode Island", "RI"},
		                                                                             {"South Carolina", "SC"},
		                                                                             {"South Dakota", "SD"},
		                                                                             {"Tennessee", "TN"},
		                                                                             {"Texas", "TX"},
		                                                                             {"Utah", "UT"},
		                                                                             {"Vermont", "VT"},
		                                                                             {"Virginia", "VA"},
		                                                                             {"Washington", "WA"},
		                                                                             {"West Virginia", "WV"},
		                                                                             {"Wisconsin", "WI"},
		                                                                             {"Wyoming", "WY"}
	                                                                             };

	public static readonly List<string> AustraliaRegions = new()
	                                                       {
		                                                       "Australian Capital Territory",
		                                                       "New South Wales",
		                                                       "Northern Territory",
		                                                       "Queensland",
		                                                       "South Australia",
		                                                       "Tasmania",
		                                                       "Victoria",
		                                                       "Western Australia"
	                                                       };

	public static readonly List<string> AustraliaRegionAbbreviations = new()
	                                                                   {
		                                                                   "ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA"
	                                                                   };


	public static readonly Dictionary<string, string> AustraliaRegionAbbreviationList = new()
	                                                                                    {
		                                                                                    {"Australian Capital Territory", "ACT"},
		                                                                                    {"New South Wales", "NSW"},
		                                                                                    {"Northern Territory", "NT"},
		                                                                                    {"Queensland", "QLD"},
		                                                                                    {"South Australia", "SA"},
		                                                                                    {"Tasmania", "TAS"},
		                                                                                    {"Victoria", "VIC"},
		                                                                                    {"Western Australia", "WA"}
	                                                                                    };

	/// <summary>
	///     Note - it is a case-insensitive match and also handles
	///     common abbreviations for the countries:
	///     australia, au, aus
	///     canada, ca, can
	///     united states, us, usa
	/// </summary>
	/// <param
	///     name="country">
	/// </param>
	/// <returns></returns>
	public static List<string> GetRegionsForCountry( string country )
	{
		if( country is not null )
		{
			switch( country.ToLower() )
			{
			case "australia":
			case "au":
			case "aus":
				return AustraliaRegions;

			case "canada":
			case "ca":
			case "can":
				return CanadaRegions;

			case "united states":
			case "us":
			case "u.s.":
			case "usa":
			case "u.s.a.":
				return USRegions;
			}
		}
		return null;
	}

	public static List<string> GetAbbreviationsForCountry( string country )
	{
		if( country is not null )
		{
			switch( country.ToLower() )
			{
			case "australia":
			case "au":
			case "aus":
				return AustraliaRegionAbbreviations;

			case "canada":
			case "ca":
			case "can":
				return CanadaRegionAbbreviations;

			case "united states":
			case "us":
			case "u.s.":
			case "usa":
			case "u.s.a.":
				return USRegionAbbreviations;
			}
		}
		return null;
	}

	public static Dictionary<string, string> GetRegionAndAbbreviationsForCountry( string country )
	{
		if( country is not null )
		{
			switch( country.ToLower() )
			{
			case "australia":
			case "au":
			case "aus":
				return AustraliaRegionAbbreviationList;

			case "canada":
			case "ca":
			case "can":
				return CanadaRegionAbbreviationList;

			case "united states":
			case "us":
			case "u.s.":
			case "usa":
			case "u.s.a.":
				return USRegionAbbreviationList;
			}
		}
		return null;
	}

	public static (string name, bool found) FindRegionForCountry( string region, string country )
	{
		Logging.WriteLogLine( "Looking for '" + region + "' in " + country );
		var name = string.Empty;
		region = ( region ?? "" ).ToLower();
		var found   = false;
		var regions = GetRegionsForCountry( country );

		if( regions != null )
		{
			foreach( var tmp in regions )
			{
				if( tmp.ToLower() == region )
				{
					name  = tmp;
					found = true;
					Logging.WriteLogLine( "Found region '" + name + "' in " + country );

					break;
				}
			}
		}

		return ( name, found );
	}

	public static string FindRegionForAbbreviationAndCountry( string abbrev, string country )
	{
		Logging.WriteLogLine( "Looking for '" + abbrev + "' in " + country );

		var region = abbrev ?? "";
		abbrev = region.ToUpper();

		if( country.IsNotNullOrWhiteSpace() )
		{
			var dict = GetRegionAndAbbreviationsForCountry( country );

			if( dict != null )
			{
				foreach( var key in dict.Keys )
				{
					if( dict[ key ].ToUpper() == abbrev )
					{
						region = key;
						Logging.WriteLogLine( "Found region: " + region + " for abbreviation: " + abbrev + " in country: " + country );

						break;
					}
				}
			}
		}

		return region;
	}
}