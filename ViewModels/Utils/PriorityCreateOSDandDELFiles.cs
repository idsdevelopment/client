﻿namespace ViewModels.Utils;

/// <summary>
///     Testing framework for export.
/// </summary>
internal class BuildPriorityOsdAndDelOutput
{
	private readonly List<Trip>     Trips;
	private readonly List<Trip>     ValidTrips = new();
	private readonly DateTimeOffset From;
	private readonly DateTimeOffset To;
	private readonly bool           ExportPickupsXmlOnly;

	// Line containers
	private readonly StringBuilder Buff               = new();
	private readonly StringBuilder FailedBuff         = new();
	private readonly StringBuilder HeartlandBuff      = new();
	private readonly StringBuilder XmlBuff            = new();
	private readonly StringBuilder XmlOSDBuff         = new();
	private readonly StringBuilder XmlBuffSeparate    = new();
	private readonly StringBuilder XmlOSDBuffSeparate = new();

	private readonly string PREFIX             = "CX-S";
	private readonly string UNDELIVERABLE_FLAG = "flagged UNDELIVERABLE";


	/// <summary>
	///     Note: if exportPickupsXmlOnly is true, then trips should only
	///     be trips whose status has changed between from and to.
	///     Otherwise, trips should be trips between from and to and whose
	///     TripIds begin with CX-S.
	/// </summary>
	/// <param
	///     name="trips">
	/// </param>
	/// <param
	///     name="from">
	/// </param>
	/// <param
	///     name="to">
	/// </param>
	/// <param
	///     name="exportPickupsXmlOnly">
	/// </param>
	public BuildPriorityOsdAndDelOutput( List<Trip> trips, DateTimeOffset from, DateTimeOffset to, bool exportPickupsXmlOnly )
	{
		Trips                = trips;
		From                 = from;
		To                   = to;
		ExportPickupsXmlOnly = exportPickupsXmlOnly;

		if( ExportPickupsXmlOnly )
			Trips = BuildTestTripsOSD();
		else
			Trips = BuildTestTripsDEL();

		Run();
	}

	private void Run()
	{
		ProcessTrips();

		if( !ExportPickupsXmlOnly && ( ValidTrips.Count > 0 ) )
		{
			// TODO Get real invoice number
			var invoiceId = GetInvoiceNumber();
			// TODO Create invoice

			// TODO Update trips - POSTED, invoiced=true, invoiceId=invoiceId
		}

		if( ExportPickupsXmlOnly )
		{
			var outputFileName = GetOutputFilename();

			if( XmlOSDBuff.Length > 0 )
			{
				var contents = XmlOSDHeader( outputFileName, "PRIORITYDP" ) + XmlOSDBuff + XmlOSDFooter();
				outputFileName += ".OSD";
				Logging.WriteLogLine( "DEBUG XmlOSDBuff - Contents of " + outputFileName + ":\n" + contents );
			}
		}
		else
		{
			// Regular .DEL and Heartland trips
			if( Buff.Length > 0 )
			{
				var outputFileName = GetOutputFilename() + ".txt";
				var contents       = Buff.ToString();
				Logging.WriteLogLine( "DEBUG Buff - Contents of " + outputFileName + ":\n" + contents );
			}

			if( XmlBuff.Length > 0 )
			{
				var outputFileName = GetOutputFilename() + ".DEL";
				var contents       = XmlHeader( outputFileName, "PRIORITYDP" ) + XmlBuff + XmlFooter();
				Logging.WriteLogLine( "DEBUG DEL XmlBuff - Contents of " + outputFileName + ":\n" + contents );
			}

			if( FailedBuff.Length > 0 )
			{
				var outputFileName = GetOutputFilename() + "_FAILED.txt";
				var contents       = FailedBuff.ToString();
				Logging.WriteLogLine( "DEBUG FailedBuff - Contents of " + outputFileName + ":\n" + contents );
			}

			if( HeartlandBuff.Length > 0 )
			{
				var outputFileName = GetOutputFilename() + ".txt";
				var contents       = HeartlandBuff.ToString();
				Logging.WriteLogLine( "DEBUG HEARTLAND HeartlandBuff - Contents of " + outputFileName + ":\n" + contents );
			}
		}
	}

	/// <summary>
	///     Trips from 20210407_083000_000000.OSD.
	/// </summary>
	/// <returns></returns>
	private List<Trip> BuildTestTripsOSD()
	{
		List<Trip> trips = new();

		var line         = "CX-S717+D899-2N+I16450432 | 2021-04-07 05:39:13 | 2021-04-07 06:23:03 | 93345  | Multistat   | 717          | angelique | DTH HARTGROVE BEHVRL HLTH (899-2N) |       | 5730 W ROOSEVELT RD | CHICAGO | ILLINOIS | 60644 | United states";
		var pieces       = line.Split( '|' );
		var tripId       = pieces[ 0 ].Trim();
		var pickupTime   = pieces[ 1 ].Trim();
		var deliveryTime = pieces[ 2 ].Trim();
		var driver       = pieces[ 3 ].Trim();
		var packageType  = pieces[ 4 ].Trim();
		var billingNotes = pieces[ 5 ].Trim();
		var podName      = pieces[ 6 ].Trim();
		var description  = pieces[ 7 ].Trim();
		var suite        = pieces[ 8 ].Trim();
		var street       = pieces[ 9 ].Trim();
		var city         = pieces[ 10 ].Trim();
		var province     = pieces[ 11 ].Trim();
		var pc           = pieces[ 12 ].Trim();
		var country      = pieces[ 13 ].Trim();

		Trip trip = new()
		            {
			            TripId                      = tripId,
			            PickupTime                  = DateTimeOffset.Parse( pickupTime ),
			            DeliveryTime                = DateTimeOffset.Parse( deliveryTime ),
			            Driver                      = driver,
			            PackageType                 = packageType,
			            BillingNotes                = billingNotes,
			            POD                         = podName,
			            DeliveryCompanyName         = description,
			            DeliveryAddressSuite        = suite,
			            DeliveryAddressAddressLine1 = street,
			            DeliveryAddressCity         = city,
			            DeliveryAddressRegion       = province,
			            DeliveryAddressPostalCode   = pc,
			            DeliveryAddressCountry      = country
		            };
		trips.Add( trip );

		return trips;
	}

	/// <summary>
	///     Trips from 20210407_090000_083780.DEL.
	/// </summary>
	/// <returns></returns>
	private List<Trip> BuildTestTripsDEL()
	{
		List<string> raw   = new();
		List<Trip>   trips = new();

		raw.Add( "CX-S705+D3126-PCCE2+I16886486     | 2021-04-07 01:48:18 | 2021-04-07 06:19:13 | 13380  | Route: NIGHT ROUTE SO2     | 705          | Kelsey CNA           | WILLOWDALE VILLAGE (3126-PCCE2)        |       | 404 W. WILLOW ROAD        | DALE             | INDIANA  | 47523      | United states" );
		raw.Add( "CX-S705+D3126-PCCW+I16886642      | 2021-04-07 01:48:18 | 2021-04-07 06:18:47 | 13380  | Route: NIGHT ROUTE SO2     | 705          | Kelsey CNA           | WILLOWDALE VILLAGE (3126-PCCW)         |       | 404 W. WILLOW ROAD        | DALE             | INDIANA  | 47523      | United states" );
		raw.Add( "CX-S705+D3137-300+I16886275       | 2021-04-07 01:48:18 | 2021-04-07 07:04:13 | 13380  | Route: NIGHT ROUTE SO2     | 705          | Jocie RN             | TIMBERS OF JASPER (3137-300)           |       | 2909 HOWARD DRIVE         | JASPER           | INDIANA  | 47546      | United states" );
		raw.Add( "CX-S705+D3137-300+I16886727       | 2021-04-07 01:35:39 | 2021-04-07 07:03:30 | 13380  | Route: NIGHT ROUTE SO2     | 705          | Jocie RN             | TIMBERS OF JASPER (3137-300)           |       | 2909 HOWARD DRIVE         | JASPER           | INDIANA  | 47546      | United states" );
		raw.Add( "CX-S705+D496-CEN4+I16886895       | 2021-04-07 06:35:16 | 2021-04-07 07:08:28 | 17377  | Route: DAY RT 0730 MON-FRI | 705          | Jennifer work        | LUTHERWOOD TREATMENT CENT (496-CEN4)   |       | 1525 N RITTER AVE         | INDIANAPOLIS     | INDIANA  | 46219-3026 | United states" );
		raw.Add( "CX-S705+D571-ACOT3+I16884911      | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-ACOT3)           |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-ACOT3+I16886496      | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-ACOT3)           |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-ACOT3+I16886656      | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-ACOT3)           |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-C+I16886791          | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-C)               |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-C+I16886855          | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-C)               |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-E+I16886793          | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-E)               |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-E+I16886851          | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-E)               |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16885665         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16885769         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16885774         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16885809         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16885811         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16886689         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D571-MF+I16886818         | 2021-04-07 01:38:32 | 2021-04-07 06:34:17 | 23104  | Route: NIGHT ROUTE 8       | 705          | arlene and carrie    | AMERICAN VILLAGE (571-MF)              |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D602-AUG+I16886674        | 2021-04-07 01:33:46 | 2021-04-07 06:43:44 | 25222  | Route: NIGHT ROUTE 10      | 705          | Jody                 | STONEBROOKE (602-AUG)                  |       | 990 N 16TH ST             | NEW CASTLE       | INDIANA  | 47362-4317 | United states" );
		raw.Add( "CX-S705+D602-TRJLA+I16886509      | 2021-04-07 01:33:46 | 2021-04-07 06:43:44 | 25222  | Route: NIGHT ROUTE 10      | 705          | Jody                 | STONEBROOKE (602-TRJLA)                |       | 990 N 16TH ST             | NEW CASTLE       | INDIANA  | 47362-4317 | United states" );
		raw.Add( "CX-S705+D606-AWEST+I16886997      | 2021-04-07 06:00:11 | 2021-04-07 06:30:38 | 10570  | Stat                       | 705          | Sheila Jackson       | ZIONSVILLE MEADOWS (606-AWEST)         |       | 675 S FORD RD             | ZIONSVILLE       | INDIANA  | 46077-1825 | United states" );
		raw.Add( "CX-S705+D624-AL2+I16886673        | 2021-04-07 01:38:32 | 2021-04-07 06:35:08 | 23104  | Route: NIGHT ROUTE 8       | 705          | arelene and carrie   | AMERICAN VILLAGE AL (624-AL2)          |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D624-COTT+I16886692       | 2021-04-07 01:38:32 | 2021-04-07 06:35:08 | 23104  | Route: NIGHT ROUTE 8       | 705          | arelene and carrie   | AMERICAN VILLAGE AL (624-COTT)         |       | 2026 EAST 54TH STREET     | INDIANAPOLIS     | INDIANA  | 46220      | United states" );
		raw.Add( "CX-S705+D661-2ND+I16886651        | 2021-04-07 02:03:24 | 2021-04-07 06:10:03 | 20432  | Route: NIGHT ROUTE 11      | 705          | Octavia cottrell     | ENCLAVE SR LIV @ SAXONY (661-2ND)      |       | 12950 TABLICK STREET      | FISHERS          | INDIANA  | 46037      | United states" );
		raw.Add( "CX-S705+D661-MC+I16886600         | 2021-04-07 02:01:15 | 2021-04-07 06:10:03 | 20432  | Route: NIGHT ROUTE 11      | 705          | Octavia cottrell     | ENCLAVE SR LIV @ SAXONY (661-MC)       |       | 12950 TABLICK STREET      | FISHERS          | INDIANA  | 46037      | United states" );
		raw.Add( "CX-S705+D668-MAN2+I16886873       | 2021-04-07 01:38:32 | 2021-04-07 06:43:36 | 23104  | Route: NIGHT ROUTE 8       | 705          | bree                 | WYNDMOOR OF CASTLETON (668-MAN2)       |       | 8480 CRAIG ST.            | INDIANAPOLIS     | INDIANA  | 46250      | United states" );
		raw.Add( "CX-S705+D668-TERRA+I16886846      | 2021-04-07 01:38:32 | 2021-04-07 06:43:36 | 23104  | Route: NIGHT ROUTE 8       | 705          | bree                 | WYNDMOOR OF CASTLETON (668-TERRA)      |       | 8480 CRAIG ST.            | INDIANAPOLIS     | INDIANA  | 46250      | United states" );
		raw.Add( "CX-S705+DHERITAGE-DUNCA+I16886868 | 2021-04-07 02:49:28 | 2021-04-07 07:08:41 | 25807  | Route: NIGHT ROUTE 9       | 705          | sabrina              | HERITAGE HEALTHCARE (HERITAGE-DUNCA)   |       | 3401 SOLDIERS HOME RD.    | WEST LAFAYETTE   | INDIANA  | 47906      | United states" );
		raw.Add( "CX-S705+DHERITAGE-EARH+I16885962  | 2021-04-07 02:49:28 | 2021-04-07 07:08:41 | 25807  | Route: NIGHT ROUTE 9       | 705          | sabrina              | HERITAGE HEALTHCARE (HERITAGE-EARH)    |       | 3401 SOLDIERS HOME RD.    | WEST LAFAYETTE   | INDIANA  | 47906      | United states" );
		raw.Add( "CX-S705+DHERITAGE-ROSS1+I16886694 | 2021-04-07 02:49:28 | 2021-04-07 07:08:41 | 25807  | Route: NIGHT ROUTE 9       | 705          | sabrina              | HERITAGE HEALTHCARE (HERITAGE-ROSS1)   |       | 3401 SOLDIERS HOME RD.    | WEST LAFAYETTE   | INDIANA  | 47906      | United states" );
		raw.Add( "CX-S717+D021-MAIN+I16450414       | 2021-04-07 00:20:46 | 2021-04-07 06:43:13 | 90399  | Route: 0300C               | 717          | ritchel Alcazar      | BELMONT CROSSINGS (021-MAIN)           |       | 1936 W. BELMONT           | CHICAGO          | ILLINOIS | 60657      | United states" );
		raw.Add( "CX-S717+D40267-1E+I16450155       | 2021-04-07 00:20:46 | 2021-04-07 06:10:44 | 90399  | Route: 0300C               | 717          | christina Green      | LITTLE SIS. OF POOR (P) (40267-1E)     |       | 2325 N. LAKEWOOD AVE.     | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-2N+I16449969         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-2N)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-2N+I16450281         | 2021-04-07 00:21:04 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-2N)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-2S+I16449806         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-2S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-3S+I16450064         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-3S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-4N+I16449893         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-4N)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-4S+I16450066         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-4S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-4S+I16450285         | 2021-04-07 00:21:04 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-4S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-5N+I16449950         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-5N)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-5N+I16450284         | 2021-04-07 00:21:04 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-5N)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-5S+I16450157         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-5S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-5S+I16450328         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-5S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-6N+I16450224         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-6N)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D418-6S+I16450068         | 2021-04-07 00:20:46 | 2021-04-07 06:24:11 | 90399  | Route: 0300C               | 717          | Francis Garcia       | SYMPHONY OF LINCOLN PARK (418-6S)      |       | 1366 W FULLERTON AVENUE   | CHICAGO          | ILLINOIS | 60614      | United states" );
		raw.Add( "CX-S717+D899-2N+I16450432         | 2021-04-07 05:39:13 | 2021-04-07 06:23:03 | 93345  | Multistat                  | 717          | angelique            | DTH HARTGROVE BEHVRL HLTH (899-2N)     |       | 5730 W ROOSEVELT RD       | CHICAGO          | ILLINOIS | 60644      | United states" );
		raw.Add( "CX-S717+D985-3RD+I16450253        | 2021-04-07 00:14:58 | 2021-04-07 06:43:34 | 90399  | Route: 0300H               | 717          | latonya Winfield     | MERCY CIRCLE SNF (985-3RD)             |       | 3659 W 99TH STREET        | CHICAGO          | ILLINOIS | 60655      | United states" );
		raw.Add( "CX-S742+D6872-A+I30903169         | 2021-04-07 06:08:39 | 2021-04-07 06:54:13 | 37178  | Route: 901                 | 742          | k mewman             | CIRCLEVILLE POST ACUTE(E (6872-A)      |       | 1155 ATWATER AVENUE       | CIRCLEVILLE      | OHIO     | 43113      | United states" );
		raw.Add( "CX-S742+D6872-A+I30903241         | 2021-04-07 06:08:39 | 2021-04-07 06:54:13 | 37178  | Route: 901                 | 742          | k mewman             | CIRCLEVILLE POST ACUTE(E (6872-A)      |       | 1155 ATWATER AVENUE       | CIRCLEVILLE      | OHIO     | 43113      | United states" );
		raw.Add( "CX-S743+D2209-02+I30902449        | 2021-04-07 05:24:47 | 2021-04-07 06:22:15 | 50063  | Route: Sunrise North 8am   | 793          | brown                | SUNRISE N FARMINGTON HILL (2209-02)    |       | 29681 MIDDLEBELT RD       | FARMINGTON HILLS | MICHIGAN | 48334      | United states" );
		raw.Add( "CX-S743+D2270-01+I30902485        | 2021-04-07 05:24:47 | 2021-04-07 07:00:25 | 50063  | Route: Sunrise North 8am   | 793          | myers                | SUNRISE BLOOMFLD NTH SL (2270-01)      |       | 2080 S TELEGRAPH RD       | BLOOMFIELD       | MICHIGAN | 48302-0247 | United states" );
		raw.Add( "CX-S743+D2805-04+I30902390        | 2021-04-07 05:24:47 | 2021-04-07 06:25:36 | 50063  | Route: Sunrise North 8am   | 793          | cobb                 | SUNRISE BLOOMFLD STH SL (2805-04)      |       | 6790 TELEGRAPH RD         | BLOOMFIELD HILLS | MICHIGAN | 48301      | United states" );
		raw.Add( "CX-S764+D8210-OFFIC+I16885949     | 2021-04-07 04:47:25 | 2021-04-07 06:50:02 | 50780  | Route: 08:00 M-F           | 764          | Sam                  | CTI DEVELOPMENTAL & REHAB (8210-OFFIC) |       | 2012 IRONWOOD CIRCLE, SUI | SOUTH BEND       | INDIANA  | 46635-1889 | United states" );
		raw.Add( "CX-S764+D8210-OFFIC+I16886052     | 2021-04-07 04:47:25 | 2021-04-07 06:50:02 | 50780  | Route: 08:00 M-F           | 764          | Sam                  | CTI DEVELOPMENTAL & REHAB (8210-OFFIC) |       | 2012 IRONWOOD CIRCLE, SUI | SOUTH BEND       | INDIANA  | 466351889  | United states" );
		raw.Add( "CX-S764+D8210-OFFIC+I16886124     | 2021-04-07 04:47:25 | 2021-04-07 06:50:02 | 50780  | Route: 08:00 M-F           | 764          | Sam                  | CTI DEVELOPMENTAL & REHAB (8210-OFFIC) |       | 2012 IRONWOOD CIRCLE, SUI | SOUTH BEND       | INDIANA  | 46635-1889 | United states" );
		raw.Add( "CX-S764+D8210-OFFIC+I16886126     | 2021-04-07 04:47:25 | 2021-04-07 06:50:02 | 50780  | Route: 08:00 M-F           | 764          | Sam                  | CTI DEVELOPMENTAL & REHAB (8210-OFFIC) |       | 2012 IRONWOOD CIRCLE, SUI | SOUTH BEND       | INDIANA  | 46635-1889 | United states" );
		raw.Add( "CX-S788+D3341-01+IE2556605        | 2021-04-07 06:33:07 | 2021-04-07 07:08:58 | 54151  | Multistat                  | 788          | Todd korcal          | GRAND RAPIDS VETERANS (3341-01)        |       | 3000 MONROE AVE NE        | GRAND RAPIDS     | MICHIGAN | 49505      | United states" );
		raw.Add( "CX-S788+D3610-02+I30903410        | 2021-04-07 06:33:07 | 2021-04-07 06:48:09 | 54151  | Multistat                  | 788          | Jody soltys gawineka | SUNRISE OF CASCADE SL (3610-02)        |       | 3041 CHARLEVOIX DR SE     | GRAND RAPIDS     | MICHIGAN | 49546      | United states" );
		raw.Add( "CX-S793+D2209-03+I30902755        | 2021-04-07 05:24:47 | 2021-04-07 06:22:15 | 50063  | Route: Sunrise North 8am   | 793          | brown                | SUNRISE N FARMINGTON HILL (2209-03)    |       | 29681 MIDDLEBELT RD       | FARMINGTON HILLS | MICHIGAN | 48334      | United states" );
		raw.Add( "CX-S793+D2270-02+I30902986        | 2021-04-07 05:24:47 | 2021-04-07 07:00:25 | 50063  | Route: Sunrise North 8am   | 793          | myers                | SUNRISE BLOOMFLD NTH SL (2270-02)      |       | 2080 S TELEGRAPH RD       | BLOOMFIELD       | MICHIGAN | 48302-0247 | United states" );

		foreach( var line in raw )
		{
			var pieces       = line.Split( '|' );
			var tripId       = pieces[ 0 ].Trim();
			var pickupTime   = pieces[ 1 ].Trim();
			var deliveryTime = pieces[ 2 ].Trim();
			var driver       = pieces[ 3 ].Trim();
			var packageType  = pieces[ 4 ].Trim();
			var billingNotes = pieces[ 5 ].Trim();
			var podName      = pieces[ 6 ].Trim();
			var description  = pieces[ 7 ].Trim();
			var suite        = pieces[ 8 ].Trim();
			var street       = pieces[ 9 ].Trim();
			var city         = pieces[ 10 ].Trim();
			var province     = pieces[ 11 ].Trim();
			var pc           = pieces[ 12 ].Trim();
			var country      = pieces[ 13 ].Trim();

			Trip trip = new()
			            {
				            TripId                      = tripId,
				            PickupTime                  = DateTimeOffset.Parse( pickupTime ),
				            DeliveryTime                = DateTimeOffset.Parse( deliveryTime ),
				            Driver                      = driver,
				            PackageType                 = packageType,
				            BillingNotes                = billingNotes,
				            POD                         = podName,
				            DeliveryCompanyName         = description,
				            DeliveryAddressSuite        = suite,
				            DeliveryAddressAddressLine1 = street,
				            DeliveryAddressCity         = city,
				            DeliveryAddressRegion       = province,
				            DeliveryAddressPostalCode   = pc,
				            DeliveryAddressCountry      = country
			            };
			trips.Add( trip );
		}

		return trips;
	}

	private string FormatXml( string str, int maxLen = -1 )
	{
		str = str.Replace( "&", "&amp;" ); // Must do this first
		str = str.Replace( "<", "&lt;" );
		str = str.Replace( "\r", "&#13;" );
		str = str.Replace( ">", "&gt;" );
		str = str.Replace( "\"", "&quot;" );
		str = str.Replace( "'", "&apos;" );

		if( ( maxLen > -1 ) && ( str.Length > maxLen ) )
			str = str.Substring( 0, maxLen );

		return str;
	}

	private string XmlOSDHeader( string exportId, string courierName ) => "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + "\r\n" +
	                                                                      "<ns0:OverageShortageAndDamages xmlns:ns0=\"http://www.omnicare.com/schema/OverageShortageAndDamages.xsd\">" + "\r\n" +
	                                                                      "  <ns0:OSDID>" + exportId + "</ns0:OSDID>" + "\r\n" +
	                                                                      "  <ns0:CourierID>" + courierName + "</ns0:CourierID>" + "\r\n" +
	                                                                      "  <ns0:Pickups>" + "\r\n";

	private string XmlHeader( string exportId, string courierName ) => "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + "\r\n" +
	                                                                   "<ns0:Deliveries xmlns:ns0=\"http://www.omnicare.com/schema/Deliveries.xsd\">" + "\r\n" +
	                                                                   "  <ns0:DLVID>" + exportId + "</ns0:DLVID>" + "\r\n" +
	                                                                   "  <ns0:CourierID>" + courierName + "</ns0:CourierID>" + "\r\n" +
	                                                                   "  <ns0:DeliveryDetail>" + "\r\n";

	private string XmlOSDFooter() => "  </ns0:Pickups>" + "\r\n" +
	                                 "</ns0:OverageShortageAndDamages>" + "\r\n";

	private string XmlFooter() => "  </ns0:DeliveryDetail>" + "\r\n" +
	                              "</ns0:Deliveries>" + "\r\n";

	private string ExportXmlOSDTrip( Trip trip )
	{
		var barcode   = trip.TripId;
		var puId      = GetPULocationId( barcode );
		var fullDelId = GetDelLocationId( barcode );
		var uniqueId  = GetPackingCode( barcode );

		//var delLocationId = fullDelId;
		//var nursingStationId = string.Empty;
		//         if (fullDelId.IndexOf("-") > -1)
		//         {
		//	(delLocationId, nursingStationId) = GetDelAndNursingStationId(fullDelId);
		//         }

		var pickupTime = DateTimeOffset.Now.ToString( "yyyy-MM-dd'T'HH:mmss" );

		if( trip.PickupTime != null )
			pickupTime = trip.PickupTime?.ToString( "yyyy-MM-dd'T'HH:mmss" );
		var drLat  = "0.0";
		var drLong = "0.0";

		if( trip.PickupLatitude > 0 )
			drLat = trip.PickupLatitude + "";

		if( trip.PickupLongitude > 0 )
			drLong = trip.PickupLongitude + "";

		var line =
			"    <ns0:Pickup>" + "\r\n" +
			"      <ns0:DeliveryID>" + FormatXml( uniqueId, 20 ) + "</ns0:DeliveryID>" + "\r\n" +
			"      <ns0:SenderCode>" + FormatXml( puId, 10 ) + "</ns0:SenderCode>" + "\r\n" +
			"      <ns0:PickupDateTime>" + pickupTime.Substring( 0, pickupTime.Length - 2 ) + ":00</ns0:PickupDateTime>" + "\r\n" +
			"      <ns0:ScanCode>" + FormatXml( trip.TripId ) + "</ns0:ScanCode>" + "\r\n" +
			"      <ns0:DriverName>" + FormatXml( trip.Driver ) + "</ns0:DriverName>" + "\r\n" +
			"      <ns0:DriverNumber>" + FormatXml( trip.Driver ) + "</ns0:DriverNumber>" + "\r\n" +
			"      <ns0:EventCode>" + FormatXml( "PICKEDUP" ) + "</ns0:EventCode>" + "\r\n" +
			"      <ns0:RouteCode>" + FormatXml( trip.PackageType ) + "</ns0:RouteCode>" + "\r\n" +
			"      <ns0:GPS><ns0:latitude>" + drLat + "</ns0:latitude><ns0:longitude>" + drLong + "</ns0:longitude></ns0:GPS>" + "\r\n" +
			"    </ns0:Pickup>" + "\r\n";

		return line;
	}

	private string ExportXmlTrip( Trip trip, string sigFileName )
	{
		var barcode   = trip.TripId;
		var puId      = GetPULocationId( barcode );
		var fullDelId = GetDelLocationId( barcode );
		var uniqueId  = GetPackingCode( barcode );

		var delLocationId    = fullDelId;
		var nursingStationId = string.Empty;

		if( fullDelId.IndexOf( "-" ) > -1 )
			( delLocationId, nursingStationId ) = GetDelAndNursingStationId( fullDelId );

		var deliveredTime = DateTimeOffset.Now.ToString( "yyyy-MM-dd'T'HH:mmss" );

		if( trip.DeliveryTime != null )
			deliveredTime = trip.DeliveryTime?.ToString( "yyyy-MM-dd'T'HH:mmss" );
		var drLat  = "0.0";
		var drLong = "0.0";

		if( trip.DeliveryLatitude > 0 )
			drLat = trip.DeliveryLatitude + "";

		if( trip.DeliveryLongitude > 0 )
			drLong = trip.DeliveryLongitude + "";
		var podName = trip.POD;

		if( podName is null || ( podName.Length == 0 ) )
			podName = "NA";
		var deliveryAddress = string.Empty;

		if( trip.DeliveryAddressSuite.IsNotNullOrWhiteSpace() )
			deliveryAddress = trip.DeliveryAddressSuite + " ";

		if( trip.DeliveryAddressAddressLine1.IsNotNullOrWhiteSpace() )
			deliveryAddress += trip.DeliveryAddressAddressLine1;

		var line =
			"    <ns0:Delivery>" + "\r\n" +
			"      <ns0:DeliveryID>" + FormatXml( uniqueId, 20 ) + "</ns0:DeliveryID>" + "\r\n" +
			"      <ns0:SenderCode>" + FormatXml( puId, 10 ) + "</ns0:SenderCode>" + "\r\n" +
			"      <ns0:RecipientCode>" + FormatXml( delLocationId ) + "</ns0:RecipientCode>" + "\r\n" +
			( nursingStationId is null || ( nursingStationId.Length == 0 ) ? "" : "      <ns0:NursingStation>" + FormatXml( nursingStationId ) + "</ns0:NursingStation>" + "\r\n" ) +
			"      <ns0:EventCode>" + "DELIVERED" + "</ns0:EventCode>" + "\r\n" +
			"      <ns0:EventDateTime>" + deliveredTime.Substring( 0, deliveredTime.Length - 2 ) + ":00</ns0:EventDateTime>" + "\r\n" +
			"      <ns0:RecipientName>" + FormatXml( podName, 50 ) + "</ns0:RecipientName>" + "\r\n" +
			"      <ns0:RecipientNameAndAddress>" + "\r\n" +
			"        <ns0:Name>" + FormatXml( podName, 50 ) + "</ns0:Name>" + "\r\n" +
			"        <ns0:Address>" + "\r\n" +
			"          <ns0:Line1>" + FormatXml( deliveryAddress ) + "</ns0:Line1>" + "\r\n" +
			"          <ns0:CityTownOrLocality>" + FormatXml( trip.DeliveryAddressCity ) + "</ns0:CityTownOrLocality>" + "\r\n" +
			"          <ns0:StateOrProvince>" + FormatXml( trip.DeliveryAddressRegion ) + "</ns0:StateOrProvince>" + "\r\n" +
			"          <ns0:PostalCode>" + FormatXml( trip.DeliveryAddressPostalCode ) + "</ns0:PostalCode>" + "\r\n" +
			"          <ns0:Country>" + FormatXml( trip.DeliveryAddressCountry ) + "</ns0:Country>" + "\r\n" +
			"        </ns0:Address>" + "\r\n" +
			"      </ns0:RecipientNameAndAddress>" + "\r\n" +
			"      <ns0:RecipientRelationship>" + "4" + "</ns0:RecipientRelationship>" + "\r\n" +
			"      <ns0:SignatureFile>" + sigFileName + "</ns0:SignatureFile>" + "\r\n" +
			"      <ns0:DriverName>" + FormatXml( trip.Driver ) + "</ns0:DriverName>" + "\r\n" +
			"      <ns0:DriverNumber>" + FormatXml( trip.Driver, 10 ) + "</ns0:DriverNumber>" + "\r\n" +
			"      <ns0:Comment>" + "delivered" + "</ns0:Comment>" + "\r\n" +
			"      <ns0:CourierTrackingID>" + FormatXml( trip.TripId ) + "</ns0:CourierTrackingID>" + "\r\n" +
			"      <ns0:GPS><ns0:latitude>" + drLat + "</ns0:latitude><ns0:longitude>" + drLong + "</ns0:longitude></ns0:GPS>" + "\r\n" +
			"      <ns0:ScanCode>" + FormatXml( trip.TripId ) + "</ns0:ScanCode>" + "\r\n" +
			"    </ns0:Delivery>" + "\r\n";

		return line;
	}

	private void ProcessTrips()
	{
		if( ( Trips != null ) && ( Trips.Count > 0 ) )
		{
			foreach( var trip in Trips )
			{
				if( trip.TripId.StartsWith( PREFIX ) )
				{
					Logging.WriteLogLine( "DEBUG Processing trip: " + trip.TripId );
					var isHeartland       = trip.AccountId.TrimToLower() == "toledo";
					var isOtherUnexported = trip.AccountId.TrimToLower().StartsWith( "remedi" );

					var outputFileName = GetOutputFilename();

					var puId         = GetPULocationId( trip.TripId );
					var delId        = GetDelLocationId( trip.TripId );
					var packingCode  = GetPackingCode( trip.TripId );
					var billingNotes = GetBillingNotes( trip );
					var podName      = GetPODName( trip );
					var (deliveryTimeHours, deliveryTimeDay) = GetDeliveryTimeHours( trip );

					var sigFileName = puId + "_" + packingCode + "_" + deliveryTimeDay + "_" + deliveryTimeHours + ".jpg";

					// TODO 
					//var sigFileExists = (trip.Signatures.Count > 0);
					var sigFileExists = true;
					// TODO Need to create the signature

					var line = packingCode + "\t" + podName + "\t" + deliveryTimeHours + "\t" + deliveryTimeDay + "\t" + puId + "\t" + delId + "\t";

					if( sigFileExists )
						line += sigFileName;
					line += "\r\n";

					if( !sigFileExists ) // No signature
					{
						Logging.WriteLogLine( "DEBUG Failed line: " + line );
						FailedBuff.Append( line );

						// TODO Update trip with serviceLevel = UNEXPORTED
					}
					else
					{
						Logging.WriteLogLine( "DEBUG Valid line: " + line );
						ValidTrips.Add( trip );

						if( isHeartland )
						{
							var hline = string.Empty;

							if( !trip.Reference.IsNotNullOrWhiteSpace() )
								hline += billingNotes;
							hline += "|";

							if( trip.Reference.IsNotNullOrWhiteSpace() )
								hline += trip.TripId;
							else
								hline += trip.Reference;
							hline += "|";
							hline += podName + "|" + deliveryTimeHours + "|" + deliveryTimeDay + "|";

							if( sigFileExists )
								hline += sigFileName + "\r\n";

							HeartlandBuff.Append( hline );
						}
						else if( isOtherUnexported )
						{
							// Do nothing
						}
						else
						{
							Buff.Append( line );
							XmlBuff.Append( ExportXmlTrip( trip, sigFileName ) );
							XmlOSDBuff.Append( ExportXmlOSDTrip( trip ) );
						}
					}
				}
				else
					Logging.WriteLogLine( "DEBUG Skipping non-CX-S trip: " + trip.TripId );
			}
		}
	}

	private string GetPULocationId( string tripId )
	{
		var id = string.Empty;
		// CX-S717+D899-2N+I16450432
		//    ^   ^
		var start  = PREFIX.Length;
		var length = tripId.IndexOf( "+" ) - start;

		if( ( start > -1 ) && ( length > 0 ) )
			id = tripId.Substring( start, length );
		Logging.WriteLogLine( "DEBUG id: " + id );

		return id;
	}

	private string GetDelLocationId( string tripId )
	{
		var id = string.Empty;
		// CX-S717+D899-2N+I16450432
		//          ^     ^
		var start  = tripId.IndexOf( "+D" ) + 2;
		var length = tripId.IndexOf( "+", start + 1 ) - start;

		if( ( start > -1 ) && ( length > 0 ) )
			id = tripId.Substring( start, length );
		Logging.WriteLogLine( "DEBUG id: " + id );

		return id;
	}

	private (string delId, string nsId) GetDelAndNursingStationId( string fullDelId )
	{
		var delId = string.Empty;
		var nsId  = string.Empty;

		// 899-2N
		//    ^
		if( fullDelId.IndexOf( "-" ) > -1 )
		{
			var pieces = fullDelId.Split( '-' );

			if( pieces.Length > 1 )
			{
				delId = pieces[ 0 ];
				nsId  = pieces[ 1 ];
			}
		}

		return ( delId, nsId );
	}

	private string GetPackingCode( string tripId )
	{
		var id = string.Empty;
		// CX-S717+D899-2N+I16450432
		//				    ^     
		var start = tripId.LastIndexOf( "+I" ) + 2;

		if( start > -1 )
			id = tripId.Substring( start );
		Logging.WriteLogLine( "DEBUG id: " + id );

		return id;
	}

	private string GetBillingNotes( Trip trip )
	{
		var billingNotes = trip.BillingNotes;

		if( billingNotes.ToLower().IndexOf( UNDELIVERABLE_FLAG.ToLower() ) > -1 )
			billingNotes = billingNotes.Substring( 0, billingNotes.ToLower().IndexOf( UNDELIVERABLE_FLAG.ToLower() ) );
		Logging.WriteLogLine( "DEBUG billingNotes: " + billingNotes );

		return billingNotes;
	}

	private string GetPODName( Trip trip )
	{
		var podName = trip.POD;

		if( podName.IsNullOrWhiteSpace() )
			podName = "PODNAME";
		Logging.WriteLogLine( "DEBUG podName: " + podName );

		return podName;
	}

	private (string hours, string day) GetDeliveryTimeHours( Trip trip )
	{
		var hours = string.Empty;
		var day   = string.Empty;

		if( trip.DeliveryTime != null )
		{
			hours = trip.DeliveryTime?.ToString( "HHmm" );
			day   = trip.DeliveryTime?.ToString( "MMddyyyy" );
		}

		return ( hours, day );
	}

	private string GetOutputFilename()
	{
		var filename = From.ToString( "yyyyMMdd_HHmmss" ) + "_0";

		if( ExportPickupsXmlOnly )
			filename += "00000";

		return filename;
	}

	/// <summary>
	///     TODO Needs to actually get a real invoice number.
	/// </summary>
	/// <returns></returns>
	private string GetInvoiceNumber()
	{
		var invoice = DateTime.Now.Millisecond + "";
		return invoice;
	}
}