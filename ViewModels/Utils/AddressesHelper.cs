﻿namespace Utils;

/// <summary>
///     Holds some common methods for dealing with addresses and groups.
/// </summary>
public static class AddressesHelper
{
	/// <summary>
	///     Utility method that only returns groups that have addresses
	///     for the current account.
	///     Empty groups are not included.
	/// </summary>
	/// <param
	///     name="groups">
	/// </param>
	/// <param
	///     name="groupsAndMembers">
	/// </param>
	/// <param
	///     name="addrs">
	/// </param>
	/// <param
	///     name="accountId">
	/// </param>
	/// <param
	///     name="includeEmptyGroups">
	/// </param>
	/// <returns></returns>
	public static List<CompanyAddressGroup> GetGroupsForAccount( List<CompanyAddressGroup> groups, Dictionary<string, List<string>> groupsAndMembers,
	                                                             CompanyDetailList addrs, string accountId, bool includeEmptyGroups )
	{
		var cags = new List<CompanyAddressGroup>();

		Logging.WriteLogLine( "Selecting groups for account: " + accountId );

		if( ( groups != null ) && ( groups.Count > 0 ) && ( groupsAndMembers != null ) && ( groupsAndMembers.Count > 0 ) && ( addrs != null )
		    && ( addrs.Count > 0 ) && accountId.IsNotNullOrWhiteSpace() )
		{
			foreach( var group in groups )
			{
				var name = group.Description;
				Logging.WriteLogLine( "Looking at group " + name );

				//if (groupsAndMembers.ContainsKey(name) && groupsAndMembers[name].Count > 0)
				if( groupsAndMembers.ContainsKey( name ) )
				{
					if( groupsAndMembers[ name ].Count > 0 )
					{
						var addrName = groupsAndMembers[ name ][ 0 ];

						//Logging.WriteLogLine("addrName: " + addrName);
						foreach( var addr in addrs )
						{
							//Logging.WriteLogLine("Comparing " + addrName.Trim() + " to " + addr.Address.CompanyName.Trim());
							if( addr.Company.CompanyName.Trim() == addrName.Trim() )
							{
								Logging.WriteLogLine( "Found group " + name );
								cags.Add( group );

								break;
							}
						}
					}
					else if( includeEmptyGroups )
					{
						Logging.WriteLogLine( "Adding empty group " + name );
						cags.Add( group );
					}
				}
				else
					Logging.WriteLogLine( "DEBUG groupsAndMembers doesn't contain: " + name );
			}
		}
		else if( includeEmptyGroups && ( addrs != null ) && ( addrs.Count == 0 ) )
		{
			foreach( var group in groups )
			{
				var name = group.Description;

				if( groupsAndMembers.ContainsKey( name ) && ( groupsAndMembers[ name ].Count == 0 ) )
					cags.Add( group );
			}
		}

		return cags;
	}
}