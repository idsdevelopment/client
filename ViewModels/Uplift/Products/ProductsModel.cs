﻿using System.Diagnostics;
using Protocol.Data._Customers.Pml;
using ViewModels.Uplift.Shipments;

namespace ViewModels.Uplift.Products;

public class ProductsModel : ViewModelBase
{
	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	// TODO this needs to be updated to the correct page
	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/658145281/How+to+Manage+Products" ); }
	}

	public ObservableCollection<PmlInventory> Inventory
	{
		get { return Get( () => Inventory, new ObservableCollection<PmlInventory>() ); }
		set { Set( () => Inventory, value ); }
	}

	public ObservableCollection<string> InventoryNames
	{
		get { return Get( () => InventoryNames, new ObservableCollection<string>() ); }
		set { Set( () => InventoryNames, value ); }
	}

	public PmlInventory SelectedInventory
	{
		get { return Get( () => SelectedInventory, new PmlInventory() ); }
		set { Set( () => SelectedInventory, value ); }
	}


	public bool IsAddUpdateEnabled
	{
		get { return Get( () => IsAddUpdateEnabled, false ); }
		set { Set( () => IsAddUpdateEnabled, value ); }
	}

	//[DependsUpon(nameof(SelectedInventory))]
	//public void WhenSelectedInventoryChanges()
	//{
	//    if (SelectedInventory == null)
	//    {
	//        IsAddUpdateEnabled = false;
	//    }
	//    else
	//    {
	//        IsAddUpdateEnabled = true;
	//    }
	//}


	public List<PmlInventory> SelectedInventoryItems
	{
		get { return Get( () => SelectedInventoryItems, new List<PmlInventory>() ); }
		set { Set( () => SelectedInventoryItems, value ); }
	}


	public string SelectedInventoryName
	{
		get { return Get( () => SelectedInventoryName, "" ); }
		set { Set( () => SelectedInventoryName, value ); }
	}


	public string PackageName
	{
		get { return Get( () => PackageName, "" ); }
		set { Set( () => PackageName, value ); }
	}


	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}


	public int PackageQuantity
	{
		get { return Get( () => PackageQuantity, 1 ); }
		set { Set( () => PackageQuantity, value ); }
	}

	public string AdhocFilter
	{
		get { return Get( () => AdhocFilter, "" ); }
		set { Set( () => AdhocFilter, value ); }
	}

	private readonly List<PmlInventory> OriginalInventory = new();

	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( !IsInDesignMode )
		{
			Loaded = false;

			Task.Run( () =>
			          {
				          Task.WaitAll( Task.Run( () =>
				                                  {
					                                  //GetAccountNames();
					                                  GetInventory();
				                                  } ),
				                        Task.Run( () =>
				                                  {
					                                  Dispatcher.Invoke( () =>
					                                                     {
						                                                     //LoadGroups();
					                                                     } );
				                                  } )
				                      );
			          } );
		}
	}


	[DependsUpon( nameof( PackageName ) )]
	[DependsUpon( nameof( Barcode ) )]
	public void WhenPackageNameOrBarcodeChanges()
	{
		if( PackageName.IsNotNullOrWhiteSpace() && Barcode.IsNotNullOrWhiteSpace() )
			IsAddUpdateEnabled = true;
		else
			IsAddUpdateEnabled = false;
	}

	[DependsUpon( nameof( AdhocFilter ) )]
	public void WhenAdhocFilterChanges()
	{
		Logging.WriteLogLine( "Filter: " + AdhocFilter );

		if( AdhocFilter.IsNotNullOrWhiteSpace() )
		{
			var matches = new List<PmlInventory>();
			var words   = AdhocFilter.Split( ' ' );

			if( words.Length > 0 )
			{
				foreach( var pi in OriginalInventory )
				{
					var lowerName    = pi.Description.ToLower();
					var lowerBarcode = pi.Barcode.ToLower();
					var match        = true;

					foreach( var word in words )
					{
						if( !lowerName.Contains( word ) && !lowerBarcode.Contains( word ) )
						{
							match = false;

							break;
						}
					}

					if( match )
						matches.Add( pi );
				}

				Dispatcher.Invoke( () =>
				                   {
					                   Inventory = new ObservableCollection<PmlInventory>( matches );
				                   } );
			}
		}
		else
		{
			// Restore all 
			Dispatcher.Invoke( () =>
			                   {
				                   Inventory = new ObservableCollection<PmlInventory>( OriginalInventory );
			                   } );
		}
	}

	private void GetInventory()
	{
		if( !IsInDesignMode )
		{
			Task.Run( async () =>
			          {
				          Logging.WriteLogLine( "Loading Inventory" );

				          var Inv = await Azure.Client.RequestPML_GetInventory();

				          if( Inv is not null )
				          {
					          OriginalInventory.Clear();
					          var names = new List<string>();
					          var IList = new ObservableCollection<PmlInventory>();

					          foreach( var I in Inv )
					          {
						          Logging.WriteLogLine( "Adding item: " + I.Description );
						          IList.Add( I );
						          OriginalInventory.Add( I );
						          names.Add( I.Description );
					          }

					          if( names.Count > 0 )
					          {
						          Dispatcher.Invoke( () =>
						                             {
							                             Inventory             = IList;
							                             InventoryNames        = new ObservableCollection<string>( names );
							                             SelectedInventoryName = InventoryNames[ 0 ];
						                             } );
					          }
				          }
			          } );
		}
	}


#region Actions
	public void Execute_LoadProduct()
	{
		if( SelectedInventory != null )
		{
			Logging.WriteLogLine( "Loading " + SelectedInventory.Description );

			Dispatcher.Invoke( () =>
			                   {
				                   PackageName = SelectedInventory.Description;
				                   Barcode     = SelectedInventory.Barcode;
			                   } );
		}
	}

	/// <summary>
	///     Cases to check:
	///     new:
	///     name and barcode must be unique
	///     editing:
	///     barcode must be unique
	///     if name has changed, it must be unique
	/// </summary>
	public void Execute_AddUpdate()
	{
		Logging.WriteLogLine( "Saving " + PackageName );
		var name    = PackageName.Trim();
		var barcode = Barcode.Trim();

		var isNew = false;

		if( SelectedInventory == null )
			isNew = true;
		else if( ( name != SelectedInventory.Description ) && ( barcode != SelectedInventory.Barcode ) )
		{
			// Both name and barcode are different - treat as new
			isNew = true;
		}

		if( isNew )
		{
			var (NameExists, _, BarcodeExists, HasBarcode) = CheckNameAndBarcodeInInventory( name, barcode );

			if( !NameExists && !BarcodeExists )
			{
				var pi = new PmlInventory
				         {
					         Barcode       = barcode,
					         Description   = name,
					         InventoryCode = barcode
				         };

				PerformAddUpdate( pi );
			}
			else
			{
				// Tell the user
				var message = string.Empty;

				if( NameExists )
					message = (string)Application.Current.TryFindResource( "ProductsErrorNameExists" );

				if( BarcodeExists )
				{
					var tmp = (string)Application.Current.TryFindResource( "ProductsErrorBarcodeExists" );
					tmp     =  tmp.Replace( "@1", HasBarcode.Description );
					message += tmp;
				}

				var title = (string)Application.Current.TryFindResource( "ProductsErrorTitle" );

				Dispatcher.Invoke( () =>
				                   {
					                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
				                   } );
			}
		}
		else
		{
			// Update
			var          nameExists    = false;
			var          barcodeExists = false;
			PmlInventory hasBarcode    = null;

			// if name has changed, make sure the new one is unique
			if( name != SelectedInventory.Description )
			{
				var checkName = CheckNameInInventory( name );
				nameExists = checkName.nameExists;
			}

			if( barcode != SelectedInventory.Barcode )
			{
				var checkBarcode = CheckBarcodeInInventory( barcode );
				barcodeExists = checkBarcode.barcodeExists;
				hasBarcode    = checkBarcode.hasBarcode;
			}

			if( !nameExists && !barcodeExists )
			{
				SelectedInventory.Description = name;
				SelectedInventory.Barcode     = barcode;

				PerformAddUpdate( SelectedInventory );
			}
			else
			{
				// Tell the user
				var message = string.Empty;

				if( nameExists )
					message = (string)Application.Current.TryFindResource( "ProductsErrorNameExists" );

				if( barcodeExists )
				{
					var tmp = (string)Application.Current.TryFindResource( "ProductsErrorBarcodeExists" );
					tmp     =  tmp.Replace( "@1", hasBarcode.Description );
					message += tmp;
				}

				var title = (string)Application.Current.TryFindResource( "ProductsErrorTitle" );

				Dispatcher.Invoke( () =>
				                   {
					                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
				                   } );
			}
		}
	}

	private void PerformAddUpdate( PmlInventory pi )
	{
		Logging.WriteLogLine( "Saving " + pi.Description );

		var list = new PmlInventoryList
		           {
			           pi
		           };

		Task.Run( async () =>
		          {
			          await Azure.Client.RequestPML_AddUpdateInventory( list );

			          Dispatcher.Invoke( () =>
			                             {
				                             var title = (string)Application.Current.TryFindResource( "ProductsSavedTitle" );

				                             var message = (string)Application.Current.TryFindResource( "ProductsSaved" );
				                             message = message.Replace( "@1", pi.Description );
				                             MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );

				                             AdhocFilter       = string.Empty;
				                             PackageName       = string.Empty;
				                             Barcode           = string.Empty;
				                             SelectedInventory = null;
				                             GetInventory();

				                             UpdateUpliftShipmentTabs();
			                             } );
		          } );
	}

	private (bool nameExists, PmlInventory hasName, bool barcodeExists, PmlInventory hasBarcode) CheckNameAndBarcodeInInventory( string name, string barcode )
	{
		//bool nameExists = false;
		//PmlInventory hasName = null;
		//bool barcodeExists = false;
		//PmlInventory hasBarcode = null;

		if( name.IsNotNullOrWhiteSpace() )
			name = name.ToLower();

		if( barcode.IsNotNullOrWhiteSpace() )
			barcode = barcode.ToLower();

		var (NameExists, HasName)       = CheckNameInInventory( name );
		var (BarcodeExists, HasBarcode) = CheckBarcodeInInventory( barcode );

		//foreach (PmlInventory pi in OriginalInventory)
		//{
		//    if (name.IsNotNullOrWhiteSpace() && pi.Description.ToLower() == name)
		//    {
		//        Logging.WriteLogLine("Name exists in " + pi.Description);
		//        nameExists = true;
		//        hasName = pi;
		//    }
		//    if (barcode.IsNotNullOrWhiteSpace() && pi.Barcode.ToLower() == barcode)
		//    {
		//        Logging.WriteLogLine("Barcode exists in " + pi.Description);
		//        barcodeExists = true;
		//        hasBarcode = pi;
		//    }
		//    if (nameExists && barcodeExists)
		//    {
		//        break;
		//    }
		//}

		return ( NameExists, HasName, BarcodeExists, HasBarcode );
	}

	private (bool nameExists, PmlInventory hasName) CheckNameInInventory( string name )
	{
		var          nameExists = false;
		PmlInventory hasName    = null;

		if( name.IsNotNullOrWhiteSpace() )
			name = name.ToLower();

		foreach( var pi in OriginalInventory )
		{
			if( name.IsNotNullOrWhiteSpace() && ( pi.Description.ToLower() == name ) )
			{
				Logging.WriteLogLine( "Name exists in " + pi.Description );
				nameExists = true;
				hasName    = pi;

				break;
			}
		}

		return ( nameExists, hasName );
	}

	private (bool barcodeExists, PmlInventory hasBarcode) CheckBarcodeInInventory( string barcode )
	{
		var          barcodeExists = false;
		PmlInventory hasBarcode    = null;

		if( barcode.IsNotNullOrWhiteSpace() )
			barcode = barcode.ToLower();

		foreach( var pi in OriginalInventory )
		{
			if( barcode.IsNotNullOrWhiteSpace() && ( pi.Barcode.ToLower() == barcode ) )
			{
				Logging.WriteLogLine( "Name exists in " + pi.Description );
				barcodeExists = true;
				hasBarcode    = pi;

				break;
			}
		}

		return ( barcodeExists, hasBarcode );
	}

	public void Execute_Clear()
	{
		Logging.WriteLogLine( "Clearing " + PackageName );

		PackageName       = string.Empty;
		Barcode           = string.Empty;
		SelectedInventory = null;
		SelectedInventoryItems.Clear();
	}

	public void Execute_Delete()
	{
		Logging.WriteLogLine( "Deleting " + SelectedInventoryItems.Count + " item(s)" );

		if( SelectedInventoryItems.Count > 0 )
		{
			var list    = new PmlInventoryList();
			var message = string.Empty;

			if( SelectedInventoryItems.Count == 1 )
			{
				message = (string)Application.Current.TryFindResource( "ProductsDeleteSingle" );
				message = message.Replace( "@1", SelectedInventoryItems[ 0 ].Description );
				message = message.Replace( "@2", SelectedInventoryItems[ 0 ].Barcode );

				list.Add( SelectedInventoryItems[ 0 ] );
			}
			else
			{
				message = (string)Application.Current.TryFindResource( "ProductsDeleteMultipleStart" );
				message = message.Replace( "@1", SelectedInventoryItems.Count + "" );

				foreach( var pi in SelectedInventoryItems )
				{
					var line = "\n\t" + (string)Application.Current.TryFindResource( "ProductsDeleteMultipleNext" );
					line    =  line.Replace( "@1", pi.Description );
					line    =  line.Replace( "@2", pi.Barcode );
					message += line;

					list.Add( pi );
				}
			}

			Logging.WriteLogLine( message );

			var title = (string)Application.Current.TryFindResource( "ProductsDeleteTitle" );

			Dispatcher.Invoke( () =>
			                   {
				                   var mbr = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );

				                   if( mbr == MessageBoxResult.Yes )
				                   {
					                   Logging.WriteLogLine( "Deletion proceeding" );

					                   Task.Run( async () =>
					                             {
						                             await Azure.Client.RequestPML_DeleteInventory( list );

						                             SelectedInventoryItems.Clear();

						                             Inventory = new ObservableCollection<PmlInventory>();

						                             GetInventory();
					                             } );

					                   UpdateUpliftShipmentTabs();
				                   }
				                   else
					                   Logging.WriteLogLine( "Deletion cancelled" );
			                   } );
		}
	}

	private void UpdateUpliftShipmentTabs()
	{
		var tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( ( tis != null ) && ( tis.Count > 0 ) )
		{
			//PageTabItem pti = null;
			for( var i = 0; i < tis.Count; i++ )
			{
				var ti = tis[ i ];

				if( ti.Content is Shipments.Shipments )
				{
					// Reload the Inventory
					Logging.WriteLogLine( "Reloading inventory in extant " + Globals.UPLIFT_SHIPMENTS + " tab" );

					( (ShipmentsModel)ti.Content.DataContext ).GetInventory();
				}
			}
		}
	}

	public void Execute_ShowHelp()
	{
		Logging.WriteLogLine( "Opening " + HelpUri );
		var uri = new Uri( HelpUri );
		Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
	}
#endregion
}