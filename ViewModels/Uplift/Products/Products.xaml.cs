﻿using System.Windows.Controls;
using System.Windows.Input;
using Protocol.Data._Customers.Pml;

namespace ViewModels.Uplift.Products;

/// <summary>
///     Interaction logic for Products.xaml
/// </summary>
public partial class Products : Page
{
	public Products()
	{
		InitializeComponent();
	}

	private void BtnAddUpdate_Click( object sender, RoutedEventArgs e )
	{
		// Validate

		if( DataContext is ProductsModel Model )
			Model.Execute_AddUpdate();
	}

	private void BtnDelete_Click( object sender, RoutedEventArgs e )
	{
		if( ( lvProducts.SelectedItems != null ) && ( lvProducts.SelectedItems.Count > 0 ) )
		{
			if( DataContext is ProductsModel Model )
			{
				//Model.SelectedInventoryItems.Clear();
				var list = new List<PmlInventory>();

				foreach( PmlInventory pi in lvProducts.SelectedItems )
					list.Add( pi );
				//Model.SelectedInventoryItems.Add(pi);

				Model.SelectedInventoryItems = list;
				Model.Execute_Delete();
			}
		}
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		Toolbar_MouseDoubleClick( null, null );
	}


	private void LvProducts_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		if( DataContext is ProductsModel Model )
		{
			//Model.SelectedInventory = new List<PmlInventory> { (PmlInventory)lvProducts.SelectedItem };
			Model.SelectedInventory = (PmlInventory)lvProducts.SelectedItem;
			Model.Execute_LoadProduct();
		}
	}


	private void TbFilterProducts_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is ProductsModel Model )
			Model.AdhocFilter = tbFilterProducts.Text.Trim().ToLower();
	}

	private void TbProductsPackageName_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is ProductsModel Model )
			Model.PackageName = tbProductsPackageName.Text.Trim();
	}

	private void TbProductsBarcode_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is ProductsModel Model )
			Model.Barcode = tbProductsBarcode.Text.Trim();
	}

	private void IntegerTextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) )
			e.Handled = true;
	}
}