﻿using System.Collections;
using System.Diagnostics;
using System.Security.Policy;
using System.Windows.Threading;
using Protocol.Data._Customers.Pml;

namespace ViewModels.Uplift.Shipments;

internal class ShipmentsModel : ViewModelBase
{
	public const string SERVICE_LEVEL_UPLIFT    = "uplift";
	public const string SERVICE_LEVEL_UPPACK    = "uppack";
	public const string SERVICE_LEVEL_UPPC      = "uppc";
	public const string SERVICE_LEVEL_UPTOTAL   = "uptotal";
	public const string SERVICE_LEVEL_STARTRACK = "startrack";

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}


	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1106247681/How+to+Use+the+Advanced+Shipments+Tab" ); }
	}

	private DispatcherTimer updateAddressesTimer;

    private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		Task.Run( () =>
		          {
			          Task.WaitAll( Task.Run( () =>
			                                  {
				                                  GetAccountNames();

				                                  //GetPackageTypes();
				                                  GetInventory();
				                                  GetServiceLevels();

				                                  LoadAddresses( SelectedAccountId );
			                                  } ),
			                        Task.Run( () =>
			                                  {
				                                  Dispatcher.Invoke( () =>
				                                                     {
					                                                     LoadGroups();
				                                                     } );
			                                  } )
			                      );
		          } );
	}

#region Addresses and Groups
	private readonly Dictionary<string, string> dictCustomerNamesIds    = new();
	private readonly List<CompanyAddress>       currentCompanyAddresses = new();

	public string SelectedAccountName
	{
		get { return Get( () => SelectedAccountName, "" ); }
		set { Set( () => SelectedAccountName, value ); }
	}

	[DependsUpon( nameof( SelectedAccountName ) )]
	public void WhenSelectedAccountNameChanges()
	{
		if( !string.IsNullOrEmpty( SelectedAccountName ) )
		{
			if( dictCustomerNamesIds.ContainsKey( SelectedAccountName ) )
			{
				var tmp = SelectedAccountId = dictCustomerNamesIds[ SelectedAccountName ];

				LoadAddresses( tmp );

				LoadGroups();

				//GetCities();
			}
		}
	}

	public string SelectedAccountId
	{
		//get { return Get( () => SelectedAccountId, "PMLOz" ); }
		get { return Get( () => SelectedAccountId, "" ); }
		set { Set( () => SelectedAccountId, value ); }
	}

	[DependsUpon( nameof( SelectedAccountId ) )]
	public void WhenSelectedAccountIdChanges()
	{
		if( SelectedAccountId.IsNotNullOrWhiteSpace() )
			LoadAddresses( SelectedAccountId );
	}

	public ObservableCollection<CompanyAddress> Addresses
	{
		get { return Get( () => Addresses, new ObservableCollection<CompanyAddress>() ); }
		set { Set( () => Addresses, value ); }
	}

	public CompanyAddress SelectedAddress
	{
		get { return Get( () => SelectedAddress, new CompanyAddress() ); }
		set { Set( () => SelectedAddress, value ); }
	}


	public CompanyAddress DeliveryAddress
	{
		get { return Get( () => DeliveryAddress, GetDeliveryAddress ); }
		set { Set( () => DeliveryAddress, value ); }
	}

	private CompanyAddress GetDeliveryAddress()
	{
		var ca = new CompanyAddress
		         {
			         AddressLine1    = "1 Innovation Court",
			         City            = "Derrimut",
			         CompanyName     = "Melbourne Warehouse (MelWare)",
			         CompanyNumber   = "",
			         Country         = "Australia",
			         LocationBarcode = "MelWare",
			         PostalCode      = "3030",
			         Region          = "Victoria"
		         };

		return ca;
	}

	public IList Regions
	{
		get { return Get( () => Regions, new List<object>() ); }
		set { Set( () => Regions, value ); }
	}

	// Address Details section


	public string AddressDescription
	{
		get { return Get( () => AddressDescription, "" ); }
		set { Set( () => AddressDescription, value ); }
	}


	public string AddressLocationBarcode
	{
		get { return Get( () => AddressLocationBarcode, "" ); }
		set { Set( () => AddressLocationBarcode, value ); }
	}


	public string AddressSuite
	{
		get { return Get( () => AddressSuite, "" ); }
		set { Set( () => AddressSuite, value ); }
	}


	public string AddressStreet
	{
		get { return Get( () => AddressStreet, "" ); }
		set { Set( () => AddressStreet, value ); }
	}


	public string AddressCity
	{
		get { return Get( () => AddressCity, "" ); }
		set { Set( () => AddressCity, value ); }
	}


	public string AddressZipPostal
	{
		get { return Get( () => AddressZipPostal, "" ); }
		set { Set( () => AddressZipPostal, value ); }
	}


	public string AddressProvState
	{
		get { return Get( () => AddressProvState, "" ); }
		set { Set( () => AddressProvState, value ); }
	}


	public string AddressCountry
	{
		get { return Get( () => AddressCountry, "" ); }
		set { Set( () => AddressCountry, value ); }
	}

	[DependsUpon( nameof( AddressCountry ) )]
	public void WhenAddressCountryChanges()
	{
		List<string> regions = null;

		switch( AddressCountry )
		{
		case "Australia":
			regions = CountriesRegions.AustraliaRegions;

			break;

		case "Canada":
			regions = CountriesRegions.CanadaRegions;

			break;

		case "United States":
			regions = CountriesRegions.USRegions;

			break;
		}

		Regions = regions;
	}

	//public string AddressContactName
	//{
	//    get { return Get(() => AddressContactName, ""); }
	//    set { Set(() => AddressContactName, value); }
	//}


	//public string AddressContactPhone
	//{
	//    get { return Get(() => AddressContactPhone, ""); }
	//    set { Set(() => AddressContactPhone, value); }
	//}


	//public string AddressContactEmail
	//{
	//    get { return Get(() => AddressContactEmail, ""); }
	//    set { Set(() => AddressContactEmail, value); }
	//}


	//public string AddressNotes
	//{
	//    get { return Get(() => AddressNotes, ""); }
	//    set { Set(() => AddressNotes, value); }
	//}

	public IList AccountNames
	{
		get { return Get( () => AccountNames, GetAccountNames ); }
		set { Set( () => AccountNames, value ); }
	}


	public List<string> GetAccountNames()
	{
		var names = new List<string>();
		Loaded = false;

		Task.Run( async () =>
		          {
			          try
			          {
				          if( ( Addresses == null ) || ( Addresses.Count == 0 ) )
				          {
					          if( !IsInDesignMode )
					          {
						          if( AccountNames != null )
							          AccountNames.Clear();
						          else
							          AccountNames = new List<string>();

						          var ccl = await Azure.Client.RequestGetCustomerCodeList();
						          Logging.WriteLogLine( "Found " + ccl.Count + " Customers" );

						          if( ( ccl != null ) && ( ccl.Count > 0 ) )
						          {
							          var Sd = new SortedDictionary<string, string>();

							          //SortedDictionary<string, CompanyAddress> sdCas = new SortedDictionary<string, CompanyAddress>();
							          var sdCas = new SortedDictionary<string, CompanyAddress>();

							          //List<CompanyAddress> cas = new List<CompanyAddress>();
							          var cas = new List<CompanyAddress>();

							          foreach( var id in ccl )
							          {
								          var ca = await Azure.Client.RequestGetResellerCustomerCompany( id );

								          if( !Sd.ContainsKey( ca.CompanyName.ToLower() ) )
									          Sd.Add( ca.CompanyName.ToLower(), ca.CompanyName );

								          if( !sdCas.ContainsKey( ca.CompanyName.ToLower() ) )
								          {
									          //sdCas.Add(ca.CompanyName.ToLower(), ca);
									          sdCas.Add( ca.CompanyName.ToLower(), ca );
								          }

								          if( !dictCustomerNamesIds.ContainsKey( ca.CompanyName ) )
								          {
									          Logging.WriteLogLine( "Adding to dictCustomerNamesIds: " + ca.CompanyName + ", id: " + id );
									          dictCustomerNamesIds.Add( ca.CompanyName, id );
								          }
							          }

							          foreach( var Key in Sd.Keys )
							          {
								          if( !names.Contains( Sd[ Key ] ) )
									          names.Add( Sd[ Key ] );

								          //cas.Add(sdCas[Key]);
								          if( !sdCas.ContainsKey( Sd[ Key ] ) )
									          cas.Add( sdCas[ Key ] );
							          }

							          Dispatcher.Invoke( () =>
							                             {
								                             AccountNames = names;

								                             // Hardcode for pml
								                             //foreach( var ca in cas )
								                             //{
								                             // if( ca.CompanyNumber.ToLower() == "pmloz" )
								                             // {
								                             //  SelectedAccountName = ca.CompanyName;
								                             //  SelectedAccountId   = ca.CompanyNumber;

								                             //  break;
								                             // }
								                             //}

								                             //if( SelectedAccountName.IsNullOrWhiteSpace() )
								                             //{
								                             // SelectedAccountName = cas[ 0 ].CompanyName;
								                             // SelectedAccountId   = cas[ 0 ].CompanyNumber;
								                             //}

								                             //Logging.WriteLogLine( "Loading addresses for " + SelectedAccountName + " (id: " + SelectedAccountId + ")" );
							                             } );
						          }
					          }
				          }
			          }
			          catch( Exception e )
			          {
				          Logging.WriteLogLine( "Exception thrown fetching customers:\n" + e );
			          }
		          } );

		return names;
	}

	public void LoadAddresses( string id = null, CompanyDetailList cdl = null )
	{
		if( Addresses == null )
			Addresses = new ObservableCollection<CompanyAddress>();

		var sdCas = new SortedDictionary<string, CompanyAddress>();
		var cas   = new List<CompanyAddress>();

		Task.Run( async () =>
		          {
			          try
			          {
				          Loaded = false;

				          if( !IsInDesignMode )
				          {
					          if( id.IsNotNullOrWhiteSpace() )
						          SelectedAccountId = id;

					          Logging.WriteLogLine( "Loading addresses for " + SelectedAccountName + " (" + SelectedAccountId + ")..." );

					          CompanyDetailList Details = null;

					          if( cdl == null )
						          Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( SelectedAccountId );
					          else
						          Details = cdl;

					          var sd = new SortedDictionary<string, CompanyAddress>();

					          foreach( var Detail in Details )
					          {
						          var Company = Detail.Company;

						          if( Company.CompanyName.IsNotNullOrWhiteSpace() )
						          {
							          var Key = Company.CompanyName.ToLower();

							          if( !sd.ContainsKey( Key ) )
							          {
								          // Change state abbreviations to full name
								          var region = Detail.Address.Region;

								          foreach( var key in CountriesRegions.AustraliaRegionAbbreviationList.Keys )
								          {
									          if( region == CountriesRegions.AustraliaRegionAbbreviationList[ key ] )
									          {
										          Detail.Address.Region = key;

										          break;
									          }
								          }

								          sd.Add( Key, Detail.Address );
							          }
							          else
								          Logging.WriteLogLine( "Duplicate: " + Company.CompanyName.ToLower() );
						          }
					          }

					          //CompanyByAccountAndCompanyNameList list = new CompanyByAccountAndCompanyNameList();

					          //list.Add(new CompanyByAccountAndCompanyName
					          //{
					          //    CompanyName = SelectedAccountName,
					          //    CustomerCode = SelectedAccountId,
					          //    LocationBarcode = ""
					          //});
					          //var tmp = await Azure.Client.RequestGetCustomerCompanyAddressList(list);

					          //CompanyByAccountAndCompanyNameList list = new CompanyByAccountAndCompanyNameList();
					          //list.Add(new CompanyByAccountAndCompanyName
					          //{
					          //    CompanyName = SelectedAccountName,
					          //    CustomerCode = SelectedAccountId
					          //});
					          //var ccl = await Azure.Client.RequestGetCustomerCompanyAddressList(list);
					          /*
      
      
											          var csl = await Azure.Client.RequestGetCustomerCompaniesSummary(SelectedAccountId);
											          SortedDictionary<string, CompanyAddress> sd = new SortedDictionary<string, CompanyAddress>();
											          foreach (CompanyByAccountSummary cbas in csl)
											          {
												          //Logging.WriteLogLine("cbas: " + cbas.CompanyName);
      
												          ResellerCustomerCompanyLookup tmp = await Azure.Client.RequestGetCustomerCompanyAddress(cbas.CustomerCode, cbas.CompanyName);
												          if (tmp != null)
												          {
													          //Logging.WriteLogLine("Found: " + tmp.AddressLine1 + " for " + tmp.CompanyName);
													          if (tmp.LocationBarcode.IsNullOrEmpty() && cbas.LocationBarcode.IsNotNullOrEmpty())
													          {
														          tmp.LocationBarcode = cbas.LocationBarcode;
													          }
      
													          CompanyAddress ca = ResellerCustomerCompanyLookupToCompanyAddress(tmp);
													          string key = ca.CompanyName.ToLower() + ca.CompanyNumber;
													          if (!sd.ContainsKey(key))
													          {
														          sd.Add(key, ca);
													          }
													          else
													          {
														          Logging.WriteLogLine("Duplicate: " + ca.CompanyName.ToLower() + " in " + ca.CompanyNumber);
													          }
      
												          }
											          }
					          */

					          Dispatcher.Invoke( () =>
					                             {
						                             Addresses.Clear();
						                             currentCompanyAddresses.Clear();

						                             foreach( var key1 in sd.Keys )
						                             {
							                             currentCompanyAddresses.Add( sd[ key1 ] );
							                             Addresses.Add( sd[ key1 ] );
						                             }

						                             if( Addresses.Count > 0 )
						                             {
							                             foreach( var ca in Addresses )
							                             {
								                             var country = ca.Country;

								                             if( country.IsNotNullOrWhiteSpace() )
								                             {
									                             FilterRegions = CountriesRegions.GetRegionsForCountry( country );

									                             if( FilterRegions.Count > 0 )
									                             {
										                             if( FilterRegions[ 0 ].IsNotNullOrWhiteSpace() )
											                             FilterRegions.Insert( 0, "" );
									                             }

									                             break;
								                             }
							                             }

							                             GetCities();
						                             }

						                             Loaded = true;
					                             } );
				          }
			          }
			          catch( Exception e )
			          {
				          Logging.WriteLogLine( "Exception thrown fetching addresses:\n" + e );
			          }
		          } );
	}


	public Dictionary<string, PmlRoute> PmlRoutes
	{
		get { return Get( () => PmlRoutes, LoadPMLRoutes ); }
		set { Set( () => PmlRoutes, value ); }
	}

	private Dictionary<string, PmlRoute> LoadPMLRoutes()
	{
		Dictionary<string, PmlRoute> prs = new();

		if( !IsInDesignMode )
		{
			Task.WaitAll(
			             Task.Run( async () =>
			                       {
				                       var routes = await Azure.Client.RequestPML_GetRoutes();
				                       Logging.WriteLogLine( "Found routes: " + routes?.Count );

				                       foreach( var pr in routes )
				                       {
					                       if( !prs.ContainsKey( pr.PostalCode ) )
						                       prs.Add( pr.PostalCode, pr );
				                       }
			                       } )
			            );
		}

		return prs;
	}

	private CompanyAddress ResellerCustomerCompanyLookupToCompanyAddress( ResellersCustomerCompanyLookup src )
	{
		var ca = new CompanyAddress
		         {
			         AddressLine1    = src.AddressLine1,
			         AddressLine2    = src.AddressLine2,
			         Barcode         = src.Barcode,
			         City            = src.City,
			         CompanyName     = src.CompanyName,
			         CompanyNumber   = src.CustomerCode,
			         Country         = src.Country,
			         CountryCode     = src.CountryCode,
			         EmailAddress    = src.EmailAddress,
			         EmailAddress1   = src.EmailAddress1,
			         EmailAddress2   = src.EmailAddress2,
			         Fax             = src.Fax,
			         Latitude        = src.Latitude,
			         LocationBarcode = src.LocationBarcode,
			         Longitude       = src.Longitude,
			         Mobile          = src.Mobile,
			         Mobile1         = src.Mobile1,
			         Notes           = src.Notes,
			         Phone           = src.Phone,
			         Phone1          = src.Phone1,
			         PostalBarcode   = src.PostalBarcode,
			         PostalCode      = src.PostalCode,
			         Region          = src.Region,
			         SecondaryId     = src.SecondaryId,
			         Suite           = src.Suite,
			         Vicinity        = src.Vicinity
		         };

		return ca;
	}


	public List<CompanyAddressGroup> Groups
	{
		get { return Get( () => Groups, LoadGroups ); }
		set { Set( () => Groups, value ); }
	}


	public List<string> GroupNames
	{
		get { return Get( () => GroupNames, new List<string>() ); }
		set { Set( () => GroupNames, value ); }
	}


	public string SelectedGroup
	{
		get { return Get( () => SelectedGroup, "" ); }
		set { Set( () => SelectedGroup, value ); }
	}


	public List<CompanyAddress> SelectedGroupAddresses
	{
		get { return Get( () => SelectedGroupAddresses, new List<CompanyAddress>() ); }
		set { Set( () => SelectedGroupAddresses, value ); }
	}


	public List<string> SelectedGroupAddressNames
	{
		get { return Get( () => SelectedGroupAddressNames, new List<string>() ); }
		set { Set( () => SelectedGroupAddressNames, value ); }
	}


	/// <summary>
	///     Holds a map of group names and their companies.
	/// </summary>
	private readonly Dictionary<string, List<string>> GroupsAndMembers = new();

	/// <summary>
	///     Starts the process of loading all group lists.
	/// </summary>
	/// <returns></returns>
	private List<CompanyAddressGroup> LoadGroups()
	{
		Logging.WriteLogLine( "Loading groups" );
		var Ags = new List<CompanyAddressGroup>();

		Task.Run( async () =>
		          {
			          CompanyAddressGroups Cags = null;

			          try
			          {
				          Cags = await Azure.Client.RequestGetCompanyAddressGroups();
			          }
			          catch( Exception E )
			          {
				          Logging.WriteLogLine( "Exception thrown: " + E );
			          }

			          if( Cags != null )
			          {
				          PopulateGroupsAndMembers( Cags );
				          var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( SelectedAccountId );
				          Ags.AddRange( AddressesHelper.GetGroupsForAccount( Cags, GroupsAndMembers, Details, SelectedAccountId, false ) );

				          //foreach( var cag in cags )
				          // ags.Add( cag );

				          //
				          // TODO Deletions are going to leave this out of sync
				          //
				          //NextGroupNumber = cags.Count;
				          //Logging.WriteLogLine("Found " + ags.Count + " groups ; next GroupNumber: " + NextGroupNumber);

				          Groups = Ags;
				          LoadGroupNames();
			          }
		          } );

		return Ags;
	}

	private void PopulateGroupsAndMembers( List<CompanyAddressGroup> cags )
	{
		if( cags.IsNotNull() )
		{
			Task.WaitAll(
			             //Task.Run(async () =>
			             Task.Run( () =>
			                       {
				                       try
				                       {
					                       foreach( var cag in cags )
					                       {
						                       //var companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup(cag.GroupNumber);
						                       var companies = Azure.Client.RequestGetCompaniesWithinAddressGroup( cag.GroupNumber ).Result;
						                       var sd1       = new SortedDictionary<string, string>();

						                       foreach( var company in companies[ 0 ].Companies )
						                       {
							                       //sd1.Add( company.ToLower(), company );
							                       if( !sd1.ContainsKey( company ) )
								                       sd1.Add( company, company );
						                       }

						                       var sorted = new List<string>();

						                       foreach( var key in sd1.Keys )
							                       sorted.Add( sd1[ key ] );

						                       if( !GroupsAndMembers.ContainsKey( cag.Description ) )
							                       GroupsAndMembers.Add( cag.Description, sorted );
					                       }
				                       }
				                       catch( Exception e )
				                       {
					                       Logging.WriteLogLine( "Exception thrown: " + e );
				                       }
			                       } )
			            );
		}
	}

	private void LoadGroupNames()
	{
		Task.Run( async () =>
		          {
			          var sd     = new SortedDictionary<string, string>();
			          var counts = new SortedDictionary<string, int>();

			          //GroupsAndMembers.Clear();

			          foreach( var cag in Groups )
			          {
				          if( !sd.ContainsKey( cag.Description.ToLower() ) )
				          {
					          sd.Add( cag.Description.ToLower(), cag.Description );

					          // Now get the number of companies in this group
					          try
					          {
						          var companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup( cag.GroupNumber );
						          var count     = companies[ 0 ].Companies.Count;
						          counts.Add( cag.Description.ToLower(), count );

						          //var sd1 = new SortedDictionary<string, string>();

						          //foreach( var company in companies[ 0 ].Companies )
						          //{
						          // //sd1.Add( company.ToLower(), company );
						          // if( !sd1.ContainsKey( company ) )
						          //  sd1.Add( company, company );
						          //}

						          //var sorted = new List<string>();

						          //foreach( var key in sd1.Keys )
						          // sorted.Add( sd1[ key ] );

						          //GroupsAndMembers.Add( cag.Description, sorted );
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "Exception thrown: " + e );
					          }
				          }
			          }

			          var members        = (string)Application.Current.TryFindResource( "AddressBookGroupsMembers" );
			          var labelSingle    = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountSingle" );
			          var labelNotSingle = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountNotSingle" );
			          var list           = new List<string>();

			          foreach( var key in sd.Keys )
			          {
				          var line = sd[ key ] + " \t(" + counts[ key ] + " ";

				          if( counts[ key ] != 1 )
				          {
					          //line = sd[key] + " \t(" + counts[key] + " members)"; 
					          line += labelNotSingle + ")";
				          }
				          else
					          line += labelSingle + ")";

				          list.Add( line );
			          }

			          GroupNames = list;

			          //if (SelectedAddress == null)
			          //{
			          //    LoadAddressesGroupNames();
			          //}
		          } );
	}

	private void LoadAddress()
	{
		if( SelectedAddress != null )
		{
			Logging.WriteLogLine( "Selected Address: " + SelectedAddress.CompanyName );
			SelectedAddressGroupTab = 0;
			SelectedGroup           = string.Empty;

			AddressDescription     = SelectedAddress.CompanyName;
			AddressLocationBarcode = SelectedAddress.LocationBarcode;
			AddressSuite           = SelectedAddress.Suite;

			if( SelectedAddress.AddressLine2.IsNullOrWhiteSpace() )
				AddressStreet = SelectedAddress.AddressLine1;
			else
				AddressStreet = SelectedAddress.AddressLine1 + " " + SelectedAddress.AddressLine2;

			AddressCity      = SelectedAddress.City;
			AddressProvState = SelectedAddress.Region;
			AddressCountry   = SelectedAddress.Country;
			AddressZipPostal = SelectedAddress.PostalCode;
		}
	}


	public int SelectedAddressGroupTab
	{
		get { return Get( () => SelectedAddressGroupTab, 0 ); }
		set { Set( () => SelectedAddressGroupTab, value ); }
	}


	public void Execute_LoadGroup()
	{
		if( SelectedGroup != null )
		{
			Logging.WriteLogLine( "Selected Group: " + SelectedGroup );
			SelectedAddress = null;
			SelectedGroupAddresses.Clear();
			SelectedGroupAddressNames.Clear();
			SelectedAddressGroupTab = 1;

			var group = FindGroupForName( SelectedGroup );

			if( group != null )
			{
				Task.Run( async () =>
				          {
					          var addresses = await Azure.Client.RequestGetCompaniesWithinAddressGroup( group.GroupNumber );

					          if( ( addresses != null ) && ( addresses.Count > 0 ) && ( addresses[ 0 ].Companies.Count > 0 ) )
					          {
						          var cas = new List<CompanyAddress>();
						          var hs  = new HashSet<string>();

						          var sd = new SortedDictionary<string, string>();

						          foreach( var tmp in addresses[ 0 ].Companies )
						          {
							          var name = tmp.Trim();

							          if( !sd.ContainsKey( name ) )
								          sd.Add( name, "" );
						          }

						          foreach( var tmp in sd.Keys )
						          {
							          // Need to build the full line : name (location barcode) -- suite street, city, region
							          var line = tmp;

							          var ca = ( from a in Addresses
							                     where a.CompanyName.TrimToLower() == line.TrimToLower()
							                     select a ).FirstOrDefault();

							          if( ca != null )
							          {
								          cas.Add( ca );
								          line += " (" + ca.LocationBarcode + ") -- ";

								          if( ca.Suite.IsNotNullOrWhiteSpace() )
									          line += ca.Suite + " ";

								          line += ca.AddressLine1;

								          if( ca.AddressLine2.IsNotNullOrWhiteSpace() )
									          line += " " + ca.AddressLine2;

								          line += ", " + ca.City + ", " + ca.Region;

								          //Logging.WriteLogLine("DEBUG line: " + line);
							          }
							          else
								          Logging.WriteLogLine( "DEBUG Couldn't find address for " + tmp );

							          hs.Add( line );
						          }

						          SelectedGroupAddressNames = hs.ToList();
						          SelectedGroupAddresses    = cas;
					          }
				          } );
			}
		}
	}

	private CompanyAddressGroup FindGroupForName( string name )
	{
		CompanyAddressGroup cag = null;

		if( name.IndexOf( "\t" ) > -1 )
			name = name.Substring( 0, name.IndexOf( "\t" ) ).Trim();

		foreach( var tmp in Groups )
		{
			if( tmp.Description == name )
			{
				cag = tmp;

				break;
			}
		}

		return cag;
	}
#endregion


#region Addresses Filter
	public string FilterCompanyName
	{
		get { return Get( () => FilterCompanyName, "" ); }
		set { Set( () => FilterCompanyName, value ); }
	}

	[DependsUpon( nameof( FilterCompanyName ) )]
	public void WhenFilterCompanyNameChanges()
	{
		Logging.WriteLogLine( "FilterCompanyName: " + FilterCompanyName );

		////FilterCompanyName = string.Empty;
		//FilterLocationBarcode = string.Empty;
		//FilterSelectedCity = string.Empty;
		//FilterSelectedRegion = string.Empty;

		//string tmp = FilterCompanyName;
		//Execute_ClearFilter(FilterCompanyName);
		//FilterCompanyName = tmp;

		ClearAddressGroup();

		if( FilterCompanyName.IsNotNullOrWhiteSpace() )
		{
			var name = FilterCompanyName.ToLower();
			var hs   = new HashSet<CompanyAddress>();

			foreach( var ca in currentCompanyAddresses )
			{
				if( ca.CompanyName.ToLower().Contains( name ) )
					hs.Add( ca );
			}

			Addresses = new ObservableCollection<CompanyAddress>( hs.ToList() );
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
			                   {
				                   Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );
			                   } );
		}
	}


	public string FilterLocationBarcode
	{
		get { return Get( () => FilterLocationBarcode, "" ); }
		set { Set( () => FilterLocationBarcode, value ); }
	}

	[DependsUpon( nameof( FilterLocationBarcode ) )]
	public void WhenFilterLocationBarcodeChanges()
	{
		Logging.WriteLogLine( "FilterLocationBarcode: " + FilterLocationBarcode );

		//FilterCompanyName = string.Empty;
		////FilterLocationBarcode = string.Empty;
		//FilterSelectedCity = string.Empty;
		//FilterSelectedRegion = string.Empty;

		//string tmp = FilterLocationBarcode;
		//Execute_ClearFilter(FilterLocationBarcode);
		//FilterLocationBarcode = tmp;

		ClearAddressGroup();

		if( FilterLocationBarcode.IsNotNullOrWhiteSpace() )
		{
			var name = FilterLocationBarcode.ToLower();
			var hs   = new HashSet<CompanyAddress>();

			foreach( var ca in currentCompanyAddresses )
			{
				if( ca.LocationBarcode.ToLower().Contains( name ) )
					hs.Add( ca );
			}

			Addresses = new ObservableCollection<CompanyAddress>( hs.ToList() );
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
			                   {
				                   Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );
			                   } );
		}
	}

	public List<string> FilterCities
	{
		get { return Get( () => FilterCities, GetCities ); }
		set { Set( () => FilterCities, value ); }
	}


	/// <summary>
	///     Walks the currently used cities and builds a sorted list.
	/// </summary>
	/// <returns></returns>
	[DependsUpon( nameof( FilterCities ) )]
	private List<string> GetCities()
	{
		var cities = new List<string>();

		Logging.WriteLogLine( "Loading currently used cities in the current account's addresses" );

		if( ( Addresses != null ) && ( Addresses.Count > 0 ) )
		{
			var sd = new SortedDictionary<string, string>();

			foreach( var ca in Addresses )
			{
				if( !sd.ContainsKey( ca.City.ToLower() ) )
					sd.Add( ca.City.ToLower(), ca.City );
			}

			foreach( var key in sd.Keys )
				cities.Add( sd[ key ] );

			if( cities[ 0 ].IsNotNullOrWhiteSpace() )
				cities.Insert( 0, "" ); // Make sure that there is an empty line

			Dispatcher.Invoke( () =>
			                   {
				                   FilterCities = cities;
			                   } );
		}

		return cities;
	}


	public string FilterSelectedCity
	{
		get { return Get( () => FilterSelectedCity, "" ); }
		set { Set( () => FilterSelectedCity, value ); }
	}

	[DependsUpon( nameof( FilterSelectedCity ) )]
	public void WhenFilterSelectedCityChanges()
	{
		Logging.WriteLogLine( "FilterSelectedCity: " + FilterSelectedCity );

		//FilterCompanyName = string.Empty;
		//FilterLocationBarcode = string.Empty;
		////FilterSelectedCity = string.Empty;
		//FilterSelectedRegion = string.Empty;

		//string tmp = FilterSelectedCity;
		//Execute_ClearFilter(FilterSelectedCity);
		//FilterSelectedCity = tmp;

		ClearAddressGroup();

		if( FilterSelectedCity.IsNotNullOrWhiteSpace() )
		{
			// Reload unfiltered list
			Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );

			if( ( Addresses != null ) && ( Addresses.Count > 0 ) )
			{
				var sd = new SortedDictionary<string, CompanyAddress>();

				foreach( var ca in Addresses )
				{
					if( ca.City == FilterSelectedCity )
					{
						if( !sd.ContainsKey( ca.CompanyName.ToLower() ) )
							sd.Add( ca.CompanyName.ToLower(), ca );
					}
				}

				var cas = new List<CompanyAddress>();

				foreach( var key in sd.Keys )
					cas.Add( sd[ key ] );

				Dispatcher.Invoke( () =>
				                   {
					                   Addresses = new ObservableCollection<CompanyAddress>( cas );
				                   } );
			}
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
			                   {
				                   Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );
			                   } );
		}
	}


	public List<string> FilterRegions
	{
		get { return Get( () => FilterRegions, new List<string>() ); }
		set { Set( () => FilterRegions, value ); }
	}


	public string FilterSelectedRegion
	{
		get { return Get( () => FilterSelectedRegion, "" ); }
		set { Set( () => FilterSelectedRegion, value ); }
	}

	[DependsUpon( nameof( FilterSelectedRegion ) )]
	public void WhenFilterSelectedRegionChanges()
	{
		Logging.WriteLogLine( "FilterSelectedRegion: " + FilterSelectedRegion );

		//FilterCompanyName = string.Empty;
		//FilterLocationBarcode = string.Empty;
		//FilterSelectedCity = string.Empty;
		////FilterSelectedRegion = string.Empty;
		//string tmp = FilterSelectedRegion;
		//Execute_ClearFilter(FilterSelectedRegion);
		//FilterSelectedRegion = tmp;

		ClearAddressGroup();

		if( FilterSelectedRegion.IsNotNullOrWhiteSpace() )
		{
			// Reload unfiltered list
			Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );

			if( ( Addresses != null ) && ( Addresses.Count > 0 ) )
			{
				var sd = new SortedDictionary<string, CompanyAddress>();

				foreach( var eca in Addresses )
				{
					if( eca.Region == FilterSelectedRegion )
					{
						if( !sd.ContainsKey( eca.CompanyName.ToLower() ) )
							sd.Add( eca.CompanyName.ToLower(), eca );
					}
				}

				var cas = new List<CompanyAddress>();

				foreach( var key in sd.Keys )
					cas.Add( sd[ key ] );

				Dispatcher.Invoke( () =>
				                   {
					                   Addresses = new ObservableCollection<CompanyAddress>( cas );
				                   } );
			}
		}
		else
		{
			// Reload unfiltered list
			Dispatcher.Invoke( () =>
			                   {
				                   Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );
			                   } );
		}
	}

	public void Execute_ClearFilter( string matchToExclude = "" )
	{
		Logging.WriteLogLine( "Clearing Filter (matchToExclude: " + matchToExclude + ")" );

		if( matchToExclude.IsNullOrWhiteSpace() )
		{
			FilterCompanyName     = string.Empty;
			FilterLocationBarcode = string.Empty;
			FilterSelectedCity    = string.Empty;
			FilterSelectedRegion  = string.Empty;
		}
		else
		{
			if( FilterCompanyName != matchToExclude )
				FilterCompanyName = string.Empty;

			if( FilterLocationBarcode != matchToExclude )
				FilterLocationBarcode = string.Empty;

			if( FilterSelectedCity != matchToExclude )
				FilterSelectedCity = string.Empty;

			if( FilterSelectedRegion != matchToExclude )
				FilterSelectedRegion = string.Empty;
		}

		ClearAddressGroup();

		// Reload unfiltered list
		Dispatcher.Invoke( () =>
		                   {
			                   Addresses = new ObservableCollection<CompanyAddress>( currentCompanyAddresses );
		                   } );
	}

	public void Execute_SelectAddress()
	{
		if( SelectedAddress != null )
			LoadAddress();
	}

	private void ClearAddressGroup()
	{
		Dispatcher.Invoke( () =>
		                   {
			                   SelectedAddress    = null;
			                   AddressCity        = string.Empty;
			                   AddressCountry     = string.Empty;
			                   AddressDescription = string.Empty;
			                   AddressProvState   = string.Empty;
			                   AddressStreet      = string.Empty;
			                   AddressSuite       = string.Empty;
			                   AddressZipPostal   = string.Empty;

			                   SelectedGroup = string.Empty;
			                   SelectedGroupAddresses.Clear();
			                   SelectedGroupAddressNames.Clear();

			                   SelectedAddressGroupTab = 0;
		                   } );
	}
#endregion

#region PackageTypes, Inventory, and ServiceLevels
	/*
	 *  this.cbShipmentDetailsPackageType.IsEnabled = false;
		this.tbShipmentDetailsQuantity.IsEnabled = false;
		this.tbShipmentDetailsQuantity.Text = string.Empty;
		this.btnShipmentDetailsAdd.IsEnabled = false;
				
		this.lvPackageList.Items.Clear();
		this.lvPackageList.IsEnabled = false;
		this.btnShipmentDetailsRemove.IsEnabled = false;
	 */


	public bool IsShipmentDetailsPackageTypeEnabled
	{
		get { return Get( () => IsShipmentDetailsPackageTypeEnabled, false ); }
		set { Set( () => IsShipmentDetailsPackageTypeEnabled, value ); }
	}


	public bool IsShipmentDetailsQuantityEnabled
	{
		get { return Get( () => IsShipmentDetailsQuantityEnabled, false ); }
		set { Set( () => IsShipmentDetailsQuantityEnabled, value ); }
	}


	public bool IsShipmentDetailsAddEnabled
	{
		get { return Get( () => IsShipmentDetailsAddEnabled, false ); }
		set { Set( () => IsShipmentDetailsAddEnabled, value ); }
	}


	public bool IsPackageListEnabled
	{
		get { return Get( () => IsPackageListEnabled, false ); }
		set { Set( () => IsPackageListEnabled, value ); }
	}

	[DependsUpon( nameof( SelectedInventoryInPackageList ) )]
	public bool IsShipmentDetailsRemoveEnabled
	{
		get
		{
			var isEnabled = false;

			if( ( PackageList.Count > 0 ) && ( SelectedInventoryInPackageList != null ) && SelectedInventoryInPackageList.Description.IsNotNullOrWhiteSpace() )
				isEnabled = true;

			return isEnabled;
		}
		set { Set( () => IsShipmentDetailsRemoveEnabled, value ); }
	}


	public List<string> PackageTypes
	{
		get { return Get( () => PackageTypes, new List<string>() ); }
		set { Set( () => PackageTypes, value ); }
	}


	private void GetPackageTypes()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var pts = Azure.Client.RequestGetPackageTypes().Result;

				if( pts != null )
				{
					var PTypes = ( from Pt in pts
					               orderby Pt.SortOrder, Pt.Description
					               select Pt.Description ).ToList();

					Dispatcher.Invoke( () =>
					                   {
						                   PackageTypes        = PTypes;
						                   SelectedPackageType = PackageTypes[ 0 ];
					                   } );
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown: " + e, "GetCompanyNames", "SearchTripsModel" );
				MessageBox.Show( "Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK );
			}
		}
	}


	public string SelectedPackageType
	{
		get { return Get( () => SelectedPackageType, "" ); }
		set { Set( () => SelectedPackageType, value ); }
	}

	public string SelectedServiceLevel
	{
		get { return Get( () => SelectedServiceLevel, "" ); }
		set { Set( () => SelectedServiceLevel, value ); }
	}


	public string ShipmentDetailsQuantity
	{
		get { return Get( () => ShipmentDetailsQuantity, "1" ); }
		set { Set( () => ShipmentDetailsQuantity, value ); }
	}

	[DependsUpon( nameof( ShipmentDetailsQuantity ) )]
	public void WhenShipmentDetailsQuantityChanges()
	{
		if( ShipmentDetailsQuantity.IsNotNullOrWhiteSpace() && ( ShipmentDetailsQuantity != "0" ) && ( SelectedServiceLevel.ToLower() == SERVICE_LEVEL_UPPC ) )
			IsShipmentDetailsAddEnabled = true;
		else
			IsShipmentDetailsAddEnabled = false;
	}

	public List<PmlInventory> PackageList
	{
		get { return Get( () => PackageList, new List<PmlInventory>() ); }
		set { Set( () => PackageList, value ); }
	}

	//[DependsUpon(nameof(PackageList))]
	//public string GetPackageListCount()
	//{
 //       string message = string.Empty;
	//	if (PackageList != null)
	//	{
	//		message = FindStringResource("ShipmentTabShipmentDetailsAddedCount");
	//		message = message.Replace("@1", PackageList.Count.ToString());

	//	}

	//	return message;
	//}

	//public string PackageListCount
	//{
	//	get { return Get(() => GetPackageListCount(), string.Empty); }
	//	set { Set(() => PackageListCount, value); }
	//}


	//get { return Get( () => SelectedInventoryInPackageList, new PmlInventory() ); }
	public PmlInventory SelectedInventoryInPackageList
	{
		get { return Get( () => SelectedInventoryInPackageList, new PmlInventory() ); }
		set { Set( () => SelectedInventoryInPackageList, value ); }
	}

	public List<PmlInventory> SelectedInventoryItemsInPackageList
	{
		get { return Get(() => SelectedInventoryItemsInPackageList, new List<PmlInventory>()); }
		set { Set(() => SelectedInventoryItemsInPackageList, value); }
	}

	//     [DependsUpon(nameof(SelectedInventoryInPackageList))]
	//     public void WnenSelectedInventoryInPackageListChanges()
	//     {
	//         Logging.WriteLogLine("SelectedInventoryInPackageList: " + SelectedInventoryInPackageList.Description);
	//IsShipmentDetailsRemoveEnabled = true;
	//     }

	public List<string> ServiceLevels
	{
		get => Get( () => ServiceLevels, new List<string>() );
		set { Set( () => ServiceLevels, value ); }
	}

	/// <summary>
	/// </summary>
	/// <returns></returns>
	private List<string> GetServiceLevels()
	{
		var serviceLevels = new List<string>();

		if( !IsInDesignMode )
		{
			try
			{
				var sls = Azure.Client.RequestGetServiceLevelsDetailed().Result;

				if( sls != null )
				{
					serviceLevels = ( from S in sls
					                  orderby S.SortOrder, S.OldName
					                  select S.OldName ).ToList();
					var keep = new List<string>();

					foreach( var sl in serviceLevels )
					{
						switch( sl.ToLower() )
						{
						case SERVICE_LEVEL_UPLIFT:
						case SERVICE_LEVEL_UPPACK:
						case SERVICE_LEVEL_UPPC:
						case SERVICE_LEVEL_UPTOTAL:
						case SERVICE_LEVEL_STARTRACK:
							keep.Add( sl );

							break;

						default:
							Logging.WriteLogLine( "Skipping " + sl );

							break;
						}
					}

					if( keep.Count > 0 )
					{
						Dispatcher.Invoke( () =>
						                   {
							                   //ServiceLevels = serviceLevels;
							                   ServiceLevels = keep;

							                   SelectedServiceLevel = ServiceLevels[ 0 ];
						                   } );
					}
				}
			}
			catch( Exception e )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + e );

				//MessageBox.Show("Error fetching Service Levels\n" + e, "Error", MessageBoxButton.OK);
			}
		}
		//// TODO Not in db yet
		//List<string> sls = new List<string>()
		//{
		//    "Regular",
		//    "Rush",
		//    "Truck"
		//};

		//return sls;
		return serviceLevels;
	}

	[DependsUpon( nameof( SelectedServiceLevel ) )]
	public void WhenSelectedServiceLevelChanges()
	{
		Logging.WriteLogLine( "Selected Service Level: " + SelectedServiceLevel );

		Dispatcher.Invoke( () =>
		                   {
			                   ShipmentDetailsQuantity = string.Empty;

			                   LoadShipmentDescription();

			                   var sl = SelectedServiceLevel.ToLower();

			                   switch( sl.ToLower() )
			                   {
			                   case SERVICE_LEVEL_UPLIFT:
			                   case SERVICE_LEVEL_STARTRACK:
				                   /*
					                *  this.cbShipmentDetailsPackageType.IsEnabled = false;
					                   this.tbShipmentDetailsQuantity.IsEnabled = false;
					                   this.tbShipmentDetailsQuantity.Text = string.Empty;
					                   this.btnShipmentDetailsAdd.IsEnabled = false;
           
					                   this.lvPackageList.Items.Clear();
					                   this.lvPackageList.IsEnabled = false;
					                   this.btnShipmentDetailsRemove.IsEnabled = false;
					                */
				                   IsShipmentDetailsPackageTypeEnabled = false;
				                   IsShipmentDetailsQuantityEnabled    = false;
				                   ShipmentDetailsQuantity             = string.Empty;
				                   IsShipmentDetailsAddEnabled         = false;
				                   IsPackageListEnabled                = false;
				                   PackageList.Clear();
				                   IsShipmentDetailsRemoveEnabled = false;

				                   break;

			                   case SERVICE_LEVEL_UPPACK:
				                   /*
					                *  this.cbShipmentDetailsPackageType.IsEnabled = true;
					                   this.tbShipmentDetailsQuantity.IsEnabled = false;
					                   this.tbShipmentDetailsQuantity.Text = string.Empty;
					                   this.btnShipmentDetailsAdd.IsEnabled = true;
           
					                   this.lvPackageList.Items.Clear();
					                   this.lvPackageList.IsEnabled = true;
					                   this.btnShipmentDetailsRemove.IsEnabled = true;
					                */
				                   IsShipmentDetailsPackageTypeEnabled = true;
				                   IsShipmentDetailsQuantityEnabled    = false;
				                   ShipmentDetailsQuantity             = string.Empty;
				                   IsShipmentDetailsAddEnabled         = true;
				                   IsPackageListEnabled                = true;
				                   PackageList.Clear();
				                   IsShipmentDetailsRemoveEnabled = true;

				                   break;

			                   case SERVICE_LEVEL_UPPC:
				                   /*
					                *  this.cbShipmentDetailsPackageType.IsEnabled = true;
					                   this.tbShipmentDetailsQuantity.IsEnabled = true;
					                   this.tbShipmentDetailsQuantity.Text = string.Empty;
					                   this.btnShipmentDetailsAdd.IsEnabled = true;
           
					                   this.lvPackageList.Items.Clear();
					                   this.lvPackageList.IsEnabled = true;
					                   this.btnShipmentDetailsRemove.IsEnabled = true;
					                */
				                   IsShipmentDetailsPackageTypeEnabled = true;
				                   IsShipmentDetailsQuantityEnabled    = true;
				                   ShipmentDetailsQuantity             = string.Empty;

				                   //IsShipmentDetailsAddEnabled = true;
				                   IsShipmentDetailsAddEnabled = false; // Turned on by presence of value in ShipmentDetailsQuantity
				                   IsPackageListEnabled        = true;
				                   PackageList.Clear();
				                   IsShipmentDetailsRemoveEnabled = true;

				                   break;

			                   case SERVICE_LEVEL_UPTOTAL:
				                   /*
					                *  this.cbShipmentDetailsPackageType.IsEnabled = false;
					                   this.tbShipmentDetailsQuantity.IsEnabled = true;
					                   this.tbShipmentDetailsQuantity.Text = string.Empty;
					                   this.btnShipmentDetailsAdd.IsEnabled = false;
           
					                   this.lvPackageList.Items.Clear();
					                   this.lvPackageList.IsEnabled = false;
					                   this.btnShipmentDetailsRemove.IsEnabled = false;
					                */
				                   IsShipmentDetailsPackageTypeEnabled = false;
				                   IsShipmentDetailsQuantityEnabled    = true;
				                   ShipmentDetailsQuantity             = string.Empty;
				                   IsShipmentDetailsAddEnabled         = false;
				                   IsPackageListEnabled                = false;
				                   PackageList.Clear();
				                   IsShipmentDetailsRemoveEnabled = false;

				                   break;

			                   default:
				                   /*
					                *  this.cbShipmentDetailsPackageType.IsEnabled = false;
					                   this.tbShipmentDetailsQuantity.IsEnabled = false;
					                   this.tbShipmentDetailsQuantity.Text = string.Empty;
					                   this.btnShipmentDetailsAdd.IsEnabled = false;
           
					                   this.lvPackageList.Items.Clear();
					                   this.lvPackageList.IsEnabled = false;
					                   this.btnShipmentDetailsRemove.IsEnabled = false;
					                */
				                   IsShipmentDetailsPackageTypeEnabled = false;
				                   IsShipmentDetailsQuantityEnabled    = false;
				                   ShipmentDetailsQuantity             = string.Empty;
				                   IsShipmentDetailsAddEnabled         = false;
				                   IsPackageListEnabled                = false;
				                   PackageList.Clear();
				                   IsShipmentDetailsRemoveEnabled = false;

				                   break;
			                   }
		                   } );
	}

	/// <summary>
	///     2 error conditions:
	///     if servicelevel == uppc, quantity cannot be empty
	///     no duplicate inventory items
	///     TODO It would be nice if the added item was removed from the drop down list
	///     and added back if removed from the PackageList.
	/// </summary>
	public void Execute_AddItem()
	{
		Logging.WriteLogLine( "Adding Item: " + SelectedInventoryName );

		var original = new List<PmlInventory>();

		foreach( var pi in PackageList )
			original.Add( pi );

		switch( SelectedServiceLevel.ToLower() )
		{
		case SERVICE_LEVEL_UPLIFT:
		case SERVICE_LEVEL_STARTRACK:
			// NOTHING
			break;

		case SERVICE_LEVEL_UPPACK:
			// Inventory - no quantity
			if( !IsInventoryItemAlreadyAdded(SelectedInventoryName) )
			{
				var pi = GetPmlInventoryForName( SelectedInventoryName );

				if( pi != null )
				{
					pi.PackageQuantity = 9999;
					original.Add( pi );
				}
			}

			break;

		case SERVICE_LEVEL_UPPC:
			// Inventory and quantity
			if( !IsInventoryItemAlreadyAdded(SelectedInventoryName) )
			{
				var pi = GetPmlInventoryForName( SelectedInventoryName );

				if( pi != null )
				{
					pi.PackageQuantity = decimal.Parse( ShipmentDetailsQuantity );
					original.Add( pi );
				}
			}

			break;

		case SERVICE_LEVEL_UPTOTAL:
			// NOTHING
			break;
		}

		Dispatcher.Invoke( () =>
		                   {
			                   PackageList = original;
		                   } );
	}

	public void Execute_AddAll()
	{
		List<PmlInventory> original = new();
		original.AddRange( PackageList );

        switch (SelectedServiceLevel.ToLower())
        {
            case SERVICE_LEVEL_UPLIFT:
            case SERVICE_LEVEL_STARTRACK:
                // NOTHING
                break;

            case SERVICE_LEVEL_UPPACK:
                // Inventory - no quantity
				foreach (string name in InventoryNames)
                {
	                if (!IsInventoryItemAlreadyAdded(name))
					{
						var pi = GetPmlInventoryForName(name);

						if (pi != null)
						{
							pi.PackageQuantity = 9999;
							original.Add(pi);
						}
					}
                }

                break;

            case SERVICE_LEVEL_UPPC:
                // Inventory and quantity
				decimal quantity = decimal.Parse( ShipmentDetailsQuantity );
				foreach (string name in InventoryNames)
				{
					if (!IsInventoryItemAlreadyAdded(name))
					{

						var pi = GetPmlInventoryForName(name);

						if (pi != null)
						{
							pi.PackageQuantity = decimal.Parse(ShipmentDetailsQuantity);
							original.Add(pi);
						}
					}
				}

                break;

            case SERVICE_LEVEL_UPTOTAL:
                // NOTHING
                break;
        }

        Dispatcher.Invoke(() =>
                           {
                               PackageList = original;
                           });
    }
	public void Execute_RemoveItem()
	{
		if( SelectedInventoryInPackageList != null )
		{
			SelectedInventoryName = SelectedInventoryInPackageList.Description;
			Logging.WriteLogLine( "Removing Item: " + SelectedInventoryName );
			var original = new List<PmlInventory>();

			foreach( var pi in PackageList )
			{
				if( pi.Description != SelectedInventoryName )
					original.Add( pi );
			}

			//PackageList = original;
			Dispatcher.Invoke( () =>
			                   {
				                   PackageList = original;
			                   } );
		}
	}

	public void Execute_RemoveAllItems()
	{
        Dispatcher.Invoke(() =>
                               {
                                   PackageList = new();
                               });
    }

	private PmlInventory GetPmlInventoryForName( string name )
	{
		PmlInventory pi = null;

		foreach( var tmp in Inventory )
		{
			if( tmp.Description == name )
			{
				pi = tmp;

				break;
			}
		}

		return pi;
	}

	private bool IsInventoryItemAlreadyAdded(string name)
	{
		var yes = false;

		//foreach( var pi in PackageList )
		//{
		//	if( pi.Description == SelectedInventoryName )
		//	{
		//		yes = true;

		//		break;
		//	}
		//}
		PmlInventory pi = (from P in PackageList where P.Description == name select P).FirstOrDefault(); 
		if (pi != null)
		{
			yes = true;
		}

		return yes;
	}

	public ObservableCollection<PmlInventory> Inventory
	{
		get { return Get( () => Inventory, new ObservableCollection<PmlInventory>() ); }
		set { Set( () => Inventory, value ); }
	}


	public ObservableCollection<string> InventoryNames
	{
		get { return Get( () => InventoryNames, new ObservableCollection<string>() ); }
		set { Set( () => InventoryNames, value ); }
	}

	private List<string> FullListOfInventoryNames = new();


	public ICollection<PmlInventory> SelectedInventory
	{
		get { return Get( () => SelectedInventory, new Collection<PmlInventory>() ); }
		set { Set( () => SelectedInventory, value ); }
	}


	public string SelectedInventoryName
	{
		get { return Get( () => SelectedInventoryName, "" ); }
		set { Set( () => SelectedInventoryName, value ); }
	}


	public void GetInventory()
	{
		if( !IsInDesignMode )
		{
			Task.Run( async () =>
			          {
				          Logging.WriteLogLine( "Loading Inventory" );

				          var Inv = await Azure.Client.RequestPML_GetInventory();

				          if( Inv is not null )
				          {
					          var names = new List<string>();
					          var IList = new ObservableCollection<PmlInventory>();

					          foreach( var I in Inv )
					          {
						          Logging.WriteLogLine( "Adding item: " + I.Description );
						          IList.Add( I );
						          names.Add( I.Description );
					          }

					          if( names.Count > 0 )
					          {
						          Dispatcher.Invoke( () =>
						                             {
							                             Inventory                = IList;
							                             InventoryNames           = new ObservableCollection<string>( names );
							                             FullListOfInventoryNames = names;
							                             SelectedInventoryName    = InventoryNames[ 0 ];
						                             } );
					          }
				          }
			          } );
		}
	}


	//private void AddInventoryForTest()
	//{
	//    List<string> names = new List<string>()
	//    {
	//        " Bond Street Blue KS 20 AU 70631 (PK)                        ",
	//        " Alpine Filter Menthol 25's + Firm Filter (PK)               ",
	//        " Alpine Filter Menthol 25's + Firm Filter (CT)               ",
	//        " Alpine Fine Menthol 25's + Firm Filter (PK)                 ",
	//        " Alpine Fine Menthol 25's + Firm Filter (CT)                 ",
	//        " Alpine Original Menthol 25's + Firm Filter (PK)             ",
	//        " Alpine Original Menthol 25's + Firm Filter (CT)             ",
	//        " Alpine Rich Menthol 25's + Firm Filter (PK)                 ",
	//        " Alpine Rich Menthol 25's + Firm Filter (CT)                 ",
	//        " Alpine Supreme Menthol 25's + Firm Filter (PK)              ",
	//        " Alpine Supreme Menthol 25's + Firm Filter (CT)              ",
	//        " Bond Street 20's Blue (PK)                                  ",
	//        " Bond Street 20's Blue (CT)                                  ",
	//        " Bond Street 20's Gold (PK)                                  ",
	//        " Bond Street 20's Gold (CT)                                  ",
	//        " Bond Street 20's Red (PK)                                   ",
	//        " Bond Street 20's Red (CT)                                   ",
	//        " Bond Street 25's Blue (PK)                                  ",
	//        " Bond Street 25's Blue (CT)                                  ",
	//        " Bond Street 25's Blue Twin Pack (PK)                        ",
	//        " Bond Street 25's Blue Twin Pack (CT)                        ",
	//        " Bond Street 25's Gold (PK)                                  ",
	//        " Bond Street 25's Gold (CT)                                  ",
	//        " Bond Street 25's Gold Twin Pack (PK)                        ",
	//        " Bond Street 25's Gold Twin Pack (CT)                        ",
	//        " Bond Street 25's Red (PK)                                   ",
	//        " Bond Street 25's Red (CT)                                   ",
	//        " Bond Street 30's Blue (PK)                                  ",
	//        " Bond Street 30's Blue (CT)                                  ",
	//        " Bond Street 30's Gold (PK)                                  ",
	//        " Bond Street 30's Gold (CT)                                  ",
	//        " Bond Street 30's Red (PK)                                   ",
	//        " Bond Street 30's Red (CT)                                   ",
	//        " Bond Street 40's Blue (PK)                                  ",
	//        " Bond Street 40's Blue (CT)                                  ",
	//        " Bond Street 40's Gold (PK)                                  ",
	//        " Bond Street 40's Gold (CT)                                  ",
	//        " Bond Street 40's Menthol (PK)                               ",
	//        " Bond Street 40's Menthol (CT)                               ",
	//        " Bond Street 40's Red (PK)                                   ",
	//        " Bond Street 40's Red (CT)                                   ",
	//        " choice 40's Full Red (PK)                                   ",
	//        " choice 40's Full Red (CT)                                   ",
	//        " choice 40's Original Blue (PK)                              ",
	//        " choice 40's Original Blue (CT)                              ",
	//        " choice 40's Rich Gold (PK)                                  ",
	//        " choice 40's Rich Gold (CT)                                  ",
	//        " choice Premium Tubing Tobacco 100 70g (PK)                  ",
	//        " choice Premium Tubing Tobacco 100 70g (CT)                  ",
	//        " choice RYO 25g Full Red (PK)                                ",
	//        " choice RYO 25g Full Red (CT)                                ",
	//        " choice RYO 25g Original Blue (PK)                           ",
	//        " choice RYO 25g Original Blue (CT)                           ",
	//        " choice RYO 25g Rich Gold (PK)                               ",
	//        " choice RYO 25g Rich Gold (CT)                               ",
	//        " choice RYO 25g Twin Pack (PK)                               ",
	//        " choice RYO 25g Twin Pack (CT)                               ",
	//        " choice Signature Blonde Blend 20's (PK)                     ",
	//        " choice Signature Blonde Blend 20's (CT)                     ",
	//        " choice Signature Blonde Blend 25's (PK)                     ",
	//        " choice Signature Blonde Blend 25's (CT)                     ",
	//        " choice Signature Bold Blend 20's (PK)                       ",
	//        " choice Signature Bold Blend 20's (CT)                       ",
	//        " choice Signature Bold Blend 25's (PK)                       ",
	//        " choice Signature Bold Blend 25's (CT)                       ",
	//        " choice Signature Classic Blend 20's (PK)                    ",
	//        " choice Signature Classic Blend 20's (CT)                    ",
	//        " choice Signature Classic Blend 25's (PK)                    ",
	//        " choice Signature Classic Blend 25's (CT)                    ",
	//        " choice Volume Tubing Tobacco 100 55g (PK)                   ",
	//        " choice Volume Tubing Tobacco 100 55g (CT)                   ",
	//        " Craftsman RYO Brunswick Blend 15g - Zip Lock (PK)           ",
	//        " Craftsman RYO Brunswick Blend 15g - Zip Lock (CT)           ",
	//        " Craftsman RYO Brunswick Blend 20g (PK)                      ",
	//        " Craftsman RYO Brunswick Blend 20g (CT)                      ",
	//        " Craftsman RYO Brunswick Blend 25g - Zip Lock (PK)           ",
	//        " Craftsman RYO Brunswick Blend 25g - Zip Lock (CT)           ",
	//        " Craftsman RYO Expresso 15g - Zip Lock (PK)                  ",
	//        " Craftsman RYO Expresso 15g - Zip Lock (CT)                  ",
	//        " Craftsman RYO Expresso 25g - Zip Lock (PK)                  ",
	//        " Craftsman RYO Expresso 25g - Zip Lock (CT)                  ",
	//        " Craftsman RYO Sailors Rum 15g - Zip Lock (PK)               ",
	//        " Craftsman RYO Sailors Rum 15g - Zip Lock (CT)               ",
	//        " Craftsman RYO Sailors Rum 20g (PK)                          ",
	//        " Craftsman RYO Sailors Rum 20g (CT)                          ",
	//        " Craftsman RYO Sailors Rum 25g (PK)                          ",
	//        " Craftsman RYO Sailors Rum 25g (CT)                          ",
	//        " Longbeach 30's Original Yellow Twin Pack (PK)               ",
	//        " Longbeach 30's Original Yellow Twin Pack (CT)               ",
	//        " Longbeach 30's Rich Blue Twin Pack (PK)                     ",
	//        " Longbeach 30's Rich Blue Twin Pack (CT)                     ",
	//        " Longbeach 40's Original Yellow Twin Pack + Firm Filter (PK) ",
	//        " Longbeach 40's Original Yellow Twin Pack + Firm Filter (CT) ",
	//        " Longbeach 40's Rich Blue Twin Pack + Firm Filter (PK)       ",
	//        " Longbeach 40's Rich Blue Twin Pack + Firm Filter (CT)       ",
	//        " Longbeach Filter Red 40's + Firm Filter (PK)                ",
	//        " Longbeach Filter Red 40's + Firm Filter (CT)                ",
	//        " Longbeach Fine Silver 20's + Firm Filter (PK)               ",
	//        " Longbeach Fine Silver 20's + Firm Filter (CT)               ",
	//        " Longbeach Fine Silver 30's + Firm Filter (PK)               ",
	//        " Longbeach Fine Silver 30's + Firm Filter (CT)               ",
	//        " Longbeach Fine Silver 40's + Firm Filter (PK)               ",
	//        " Longbeach Fine Silver 40's + Firm Filter (CT)               ",
	//        " Longbeach Menthol Fresh 40's + Firm Filter (PK)             ",
	//        " Longbeach Menthol Fresh 40's + Firm Filter (CT)             ",
	//        " Longbeach Menthol Green 20's + Firm Filter (PK)             ",
	//        " Longbeach Menthol Green 20's + Firm Filter (CT)             ",
	//        " Longbeach Menthol Green 30's + Firm Filter (PK)             ",
	//        " Longbeach Menthol Green 30's + Firm Filter (CT)             ",
	//        " Longbeach Menthol Green 40's + Firm Filter (PK)             ",
	//        " Longbeach Menthol Green 40's + Firm Filter (CT)             ",
	//        " Longbeach Moments East Coast 30's (PK)                      ",
	//        " Longbeach Moments East Coast 30's (CT)                      ",
	//        " Longbeach Moments Fresh Coast 30's (PK)                     ",
	//        " Longbeach Moments Fresh Coast 30's (CT)                     ",
	//        " Longbeach Moments North Coast 30's (PK)                     ",
	//        " Longbeach Moments North Coast 30's (CT)                     ",
	//        " Longbeach Moments West Coast 30's (PK)                      ",
	//        " Longbeach Moments West Coast 30's (CT)                      ",
	//        " Longbeach Original Yellow 20's + Firm Filter (PK)           ",
	//        " Longbeach Original Yellow 20's + Firm Filter (CT)           ",
	//        " Longbeach Original Yellow 30's + Firm Filter (PK)           ",
	//        " Longbeach Original Yellow 30's + Firm Filter (CT)           ",
	//        " Longbeach Original Yellow 40's + Firm Filter (PK)           ",
	//        " Longbeach Original Yellow 40's + Firm Filter (CT)           ",
	//        " Longbeach Rich Blue 20's + Firm Filter (PK)                 ",
	//        " Longbeach Rich Blue 20's + Firm Filter (CT)                 ",
	//        " Longbeach Rich Blue 30's + Firm Filter (PK)                 ",
	//        " Longbeach Rich Blue 30's + Firm Filter (CT)                 ",
	//        " Longbeach Rich Blue 40's + Firm Filter (PK)                 ",
	//        " Longbeach Rich Blue 40's + Firm Filter (CT)                 ",
	//        " Longbeach RYO 27g Original Yellow (PK)                      ",
	//        " Longbeach RYO 27g Original Yellow (CT)                      ",
	//        " Longbeach RYO 27g Rich Blue (PK)                            ",
	//        " Longbeach RYO 27g Rich Blue (CT)                            ",
	//        " Longbeach RYO 55g Original Yellow (PK)                      ",
	//        " Longbeach RYO 55g Original Yellow (CT)                      ",
	//        " Longbeach RYO 55g Rich Blue (PK)                            ",
	//        " Longbeach RYO 55g Rich Blue (CT)                            ",
	//        " Longbeach RYO Original Yellow 25g - Zip Lock (PK)           ",
	//        " Longbeach RYO Original Yellow 25g - Zip Lock (CT)           ",
	//        " Longbeach RYO Rich Blue 25g - Zip Lock (PK)                 ",
	//        " Longbeach RYO Rich Blue 25g - Zip Lock (CT)                 ",
	//        " Longbeach Select White 40's + Firm Filter (PK)              ",
	//        " Longbeach Select White 40's + Firm Filter (CT)              ",
	//        " Marlboro + Firm Filter 20's Gold Original (PK)              ",
	//        " Marlboro + Firm Filter 20's Gold Original (CT)              ",
	//        " Marlboro + Firm Filter 20's Red (PK)                        ",
	//        " Marlboro + Firm Filter 20's Red (CT)                        ",
	//        " Marlboro + Firm Filter 25's Gold Original (PK)              ",
	//        " Marlboro + Firm Filter 25's Gold Original (CT)              ",
	//        " Marlboro + Firm Filter 25's Menthol (PK)                    ",
	//        " Marlboro + Firm Filter 25's Menthol (CT)                    ",
	//        " Marlboro + Firm Filter 25's Red (PK)                        ",
	//        " Marlboro + Firm Filter 25's Red (CT)                        ",
	//        " Marlboro 20's Double Blast (PK)                             ",
	//        " Marlboro 20's Double Blast (CT)                             ",
	//        " Marlboro 20's Ice Blast (PK)                                ",
	//        " Marlboro 20's Ice Blast (CT)                                ",
	//        " Marlboro 25's Ice Blast (PK)                                ",
	//        " Marlboro 25's Ice Blast (CT)                                ",
	//        " Marlboro 25's Ice Blast Twin Pack (PK)                      ",
	//        " Marlboro 25's Ice Blast Twin Pack (CT)                      ",
	//        " Marlboro Black Menthol 20's (PK)                            ",
	//        " Marlboro Black Menthol 20's (CT)                            ",
	//        " Marlboro Blue + Firm Filter 25's (PK)                       ",
	//        " Marlboro Blue + Firm Filter 25's (CT)                       ",
	//        " Peter Jackson  RYO Blue 15g - Zip Lock (PK)                 ",
	//        " Peter Jackson  RYO Blue 15g - Zip Lock (CT)                 ",
	//        " Peter Jackson 30's Classic Blue (PK)                        ",
	//        " Peter Jackson 30's Classic Blue (CT)                        ",
	//        " Peter Jackson 30's Classic Gold (PK)                        ",
	//        " Peter Jackson 30's Classic Gold (CT)                        ",
	//        " Peter Jackson 30's Original Blue Twin Pack + Firm  (PK)     ",
	//        " Peter Jackson 30's Original Blue Twin Pack + Firm  (CT)     ",
	//        " Peter Jackson 30's Rich Gold Twin Pack + Firm Filter (PK)   ",
	//        " Peter Jackson 30's Rich Gold Twin Pack + Firm Filter (CT)   ",
	//        " Peter Jackson 40's Original Blue (PK)                       ",
	//        " Peter Jackson 40's Original Blue (CT)                       ",
	//        " Peter Jackson 40's Original Twin Pack (CT)                  ",
	//        " Peter Jackson 40's Original Twin Pack (PK)                  ",
	//        " Peter Jackson 40's Rich Gold (PK)                           ",
	//        " Peter Jackson 40's Rich Gold (CT)                           ",
	//        " Peter Jackson 40's Rich Gold Twin Pack (CT)                 ",
	//        " Peter Jackson 40's Rich Gold Twin Pack (PK)                 ",
	//        " Peter Jackson Fine Silver 20's + Firm Filter (PK)           ",
	//        " Peter Jackson Fine Silver 20's + Firm Filter (CT)           ",
	//        " Peter Jackson Fine Silver 30's + Firm Filter (PK)           ",
	//        " Peter Jackson Fine Silver 30's + Firm Filter (CT)           ",
	//        " Peter Jackson Hybrid 25's Silver Regular to Fresh (PK)      ",
	//        " Peter Jackson Hybrid 25's Silver Regular to Fresh (CT)      ",
	//        " Peter Jackson Menthol Green 30's + Firm Filter (PK)         ",
	//        " Peter Jackson Menthol Green 30's + Firm Filter (CT)         ",
	//        " Peter Jackson Original Blue 20's + Firm Filter (PK)         ",
	//        " Peter Jackson Original Blue 20's + Firm Filter (CT)         ",
	//        " Peter Jackson Original Blue 30's + Firm Filter (PK)         ",
	//        " Peter Jackson Original Blue 30's + Firm Filter (CT)         ",
	//        " Peter Jackson Rich Gold 20's + Firm Filter (PK)             ",
	//        " Peter Jackson Rich Gold 20's + Firm Filter (CT)             ",
	//        " Peter Jackson Rich Gold 30's + Firm Filter (PK)             ",
	//        " Peter Jackson Rich Gold 30's + Firm Filter (CT)             ",
	//        " Peter Jackson Supreme White 30's + Firm Filter (PK)         ",
	//        " Peter Jackson Supreme White 30's + Firm Filter (CT)         ",
	//        " Peter Jackson Virginia Red 30's + Firm Filter (PK)          ",
	//        " Peter Jackson Virginia Red 30's + Firm Filter (CT)          ",
	//        " Peter Jackson Viriginia Red 20's + Firm Filter (PK)         ",
	//        " Peter Jackson Viriginia Red 20's + Firm Filter (CT)         ",
	//        " PJ Hybrid 25's Blue to Fresh (PK)                           ",
	//        " PJ Hybrid 25's Blue to Fresh (CT)                           ",
	//        " PJ Hybrid 25's Gold to Fresh (PK)                           ",
	//        " PJ Hybrid 25's Gold to Fresh (CT)                           ",
	//        " PJ Hybrid 2Xtreme Fresh 25's (PK)                           ",
	//        " PJ Hybrid 2Xtreme Fresh 25's (CT)                           ",
	//        " Wee Willem Blue 10's (TIN)                                  ",
	//        " Wee Willem Gold 10's  (TIN)                                 ",
	//        " Wee Willem Regular 10's  (TIN)"
	//    };

	//    Task.Run(async () =>
	//    {
	//        PmlInventoryList pil = new PmlInventoryList();

	//        int code = 1;
	//        foreach (string name in names)
	//        {
	//            Logging.WriteLogLine("Adding Inventory Item: " + name.Trim());

	//            PmlInventory pi = new PmlInventory();
	//            pi.Barcode = code.ToString();
	//            pi.Description = name;
	//            pi.PackageQuantity = 1;
	//            pi.InventoryCode = code.ToString();

	//            pil.Add(pi);
	//        }

	//        await Azure.Client.RequestPML_AddUpdateInventory(pil);

	//    });
	//}

	private void ClearShipmentDetails()
	{
		Dispatcher.Invoke( () =>
		                   {
			                   SelectedServiceLevel  = ServiceLevels[ 0 ];
			                   SelectedInventory     = null;
			                   SelectedInventoryName = InventoryNames[ 0 ];

			                   //PackageList.Clear();
			                   var list = new List<PmlInventory>();
			                   PackageList = list;
		                   } );
	}


	public string ShipmentDescription
	{
		get { return Get( () => ShipmentDescription, LoadShipmentDescription ); }
		set { Set( () => ShipmentDescription, value ); }
	}

	private string LoadShipmentDescription()
	{
		var description = string.Empty;

		Logging.WriteLogLine( "Loading description for " + SelectedServiceLevel );

		switch( SelectedServiceLevel.ToLower() )
		{
		case SERVICE_LEVEL_UPLIFT:
			description = (string)Application.Current.TryFindResource( "ShipmentTabTripsUpLiftDescription" );

			break;

		case SERVICE_LEVEL_UPPACK:
			description = (string)Application.Current.TryFindResource( "ShipmentTabTripsUpPackDescription" );

			break;

		case SERVICE_LEVEL_UPPC:
			description = (string)Application.Current.TryFindResource( "ShipmentTabTripsUpPCDescription" );

			break;

		case SERVICE_LEVEL_UPTOTAL:
			description = (string)Application.Current.TryFindResource( "ShipmentTabTripsUpTotalDescription" );

			break;

		case SERVICE_LEVEL_STARTRACK:
			description = (string)Application.Current.TryFindResource( "ShipmentTabTripsStartrackDescription" );

			break;
		}

		Dispatcher.Invoke( () =>
		                   {
			                   ShipmentDescription = description;
		                   } );

		return description;
	}


	public string FilterPackageTypes
	{
		get { return Get( () => FilterPackageTypes, "" ); }
		set { Set( () => FilterPackageTypes, value ); }
	}

	/// <summary>
	///     Searches for ALL words (separated by spaces) in the list.
	///     This allows the user to find matches with both benson and carton in the name.
	/// </summary>
	[DependsUpon( nameof( FilterPackageTypes ) )]
	public void WhenFilterPackageTypesChanges()
	{
		if( FilterPackageTypes.IsNotNullOrWhiteSpace() )
		{
			var matches = new List<string>();
			var words   = FilterPackageTypes.Split( ' ' );

			if( words.Length > 0 )
			{
				foreach( var name in FullListOfInventoryNames )
				{
					var lower = name.ToLower();
					var match = true;

					foreach( var word in words )
					{
						if( !lower.Contains( word ) )
						{
							match = false;

							break;
						}
					}

					if( match )
						matches.Add( name );
				}
			}

			Dispatcher.Invoke( () =>
			                   {
				                   InventoryNames = new ObservableCollection<string>( matches );
			                   } );
		}
		else
		{
			Dispatcher.Invoke( () =>
			                   {
				                   InventoryNames = new ObservableCollection<string>( FullListOfInventoryNames );
			                   } );
		}
	}


	public List<PmlRoute> Routes
	{
		get { return Get( () => Routes, new List<PmlRoute>() ); }
		set { Set( () => Routes, value ); }
	}
#endregion


#region Main Actions
	public void Execute_Clear()
	{
		Execute_ClearFilter();

		ClearAddressGroup();

		ClearShipmentDetails();
	}

	public void Execute_ShowHelp()
	{
		Logging.WriteLogLine( "Opening " + HelpUri );
		var uri = new Uri( HelpUri );
		Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
	}
#endregion

#region Create Shipments
	public void Execute_Save()
	{
		var errors = Validate();

		if( errors.IsNullOrEmpty() )
		{
			// For auto dispatch
			GetRoutes();

			List<Trip> trips = null;

			switch( SelectedServiceLevel.ToLower() )
			{
			case SERVICE_LEVEL_UPLIFT:
				trips = CreateUpLiftShipment();

				break;

			case SERVICE_LEVEL_UPPACK:
				trips = CreateUpPackShipment();

				break;

			case SERVICE_LEVEL_UPPC:
				trips = CreateUpPCShipment();

				break;

			case SERVICE_LEVEL_UPTOTAL:
				trips = CreateUpTotalShipment();

				break;

			case SERVICE_LEVEL_STARTRACK:
				trips = CreateStartrackShipment();

				break;
			}

			if( trips.Count() > 0 )
			{
				var tul = new TripUpdateList
				          {
					          Program = "ShipmentsModel"
				          };

				if( !IsInDesignMode )
				{
					Task.WaitAll(
					             Task.Run( async () =>
					                       {
						                       foreach( var trip in trips )
						                       {
							                       Logging.WriteLogLine( "Creating trip for serviceLevel: " + SelectedServiceLevel
							                                                                                + ", companyName: " + trip.PickupCompanyName
							                                                                                + " with " + trip.Packages.Count + 1 + " Products" );

							                       //var newTripId = await Azure.Client.RequestGetNextTripId();
							                       //Logging.WriteLogLine("Got new tripid: " + newTripId);

							                       //trip.TripId = newTripId;
						                       }

						                       tul.Trips.AddRange( trips );

						                       await Azure.Client.RequestAddUpdateTrips( tul );
					                       } )
					            );
				}

				var title   = (string)Application.Current.TryFindResource( "ShipmentTabTripsCreatedTitle" );
				var message = string.Empty;

				if( SelectedAddress != null )
				{
					if( trips.Count == 1 )
						message = (string)Application.Current.TryFindResource( "ShipmentTabTripsCreatedSingle" );
					else
					{
						message = (string)Application.Current.TryFindResource( "ShipmentTabTripsCreatedMultiple" );
						message = message.Replace( "@1", trips.Count().ToString() );
					}

					//message = message.Replace( "@2", SelectedAddress.CompanyName );
					message += "\n" + SelectedAddress.CompanyName;
				}
				else
				{
					if( trips.Count == 1 )
						message = (string)Application.Current.TryFindResource( "ShipmentTabTripsCreatedSingleGroup" );
					else
					{
						message = (string)Application.Current.TryFindResource( "ShipmentTabTripsCreatedMultipleGroup" );
						message = message.Replace( "@1", trips.Count().ToString() );
					}

					//message = message.Replace( "@2", SelectedGroup );
					message += "\n" + SelectedGroup;
				}

				Dispatcher.Invoke( () =>
				                   {
					                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );
				                   } );

				// From Gord - leave address or group selected
				//Execute_Clear();
				//ClearShipmentDetails();  // For Eddy
			}
		}
		else
		{
			var title = (string)Application.Current.TryFindResource( "ShipmentTabValidationErrorsTitle" );

			Dispatcher.Invoke( () =>
			                   {
				                   MessageBox.Show( errors, title, MessageBoxButton.OK, MessageBoxImage.Information );
			                   } );
		}
	}

	private List<PmlRoute> GetRoutes()
	{
		var prs = new List<PmlRoute>();

		if( !IsInDesignMode )
		{
			Task.WaitAll(
			             Task.Run( async () =>
			                       {
				                       var pr = await Azure.Client.RequestPML_GetRoutes();
				                       Logging.WriteLogLine( "Found routes: " + pr?.Count );

				                       if( pr != null )
				                       {
					                       //foreach (var route in pr)
					                       //{
					                       //	//Logging.WriteLogLine("IdsRoute: " + route.IdsRoute + " Operation: " + route.Operation + " Region: " + route.Region + " PostalCode: " + route.PostalCode);
					                       //	prs.Add(route);
					                       //}
					                       prs = ( from R in pr
					                               select R ).ToList();
				                       }
			                       }
			                     ) );
		}

		Routes = prs;

		return prs;
	}

	private string Validate()
	{
		var errors = string.Empty;

		if( ( ( SelectedAddress == null ) || SelectedAddress.CompanyName.IsNullOrWhiteSpace() ) && SelectedGroup.IsNullOrEmpty() )
			errors += (string)Application.Current.TryFindResource( "ShipmentTabValidationAddressGroupNotSelected" ) + "\n";

		switch( SelectedServiceLevel.ToLower() )
		{
		// NO error conditions for UpLift or Startrack
		//case SERVICE_LEVEL_UPLIFT:                    
		//case SERVICE_LEVEL_STARTRACK:                    
		//    break;
		case SERVICE_LEVEL_UPPACK:
		case SERVICE_LEVEL_UPPC:
			if( PackageList.Count == 0 )
				errors += (string)Application.Current.TryFindResource( "ShipmentTabValidationPackageNotSelected" ) + "\n";

			break;

		case SERVICE_LEVEL_UPTOTAL:
			if( ShipmentDetailsQuantity.IsNullOrWhiteSpace() || ( ShipmentDetailsQuantity == "0" ) )
				errors += (string)Application.Current.TryFindResource( "ShipmentTabValidationPackageNoQuantity" ) + "\n";

			break;
		}

		return errors;
	}

	private List<Trip> CreateUpLiftShipment()
	{
		Logging.WriteLogLine( "Creating Uplift Shipment" );

		var trips = new List<Trip>();

		if( SelectedAddressGroupTab == 0 )
		{
			var trip = CreateUpLiftShipmentForAddress( SelectedAddress );
			trips.Add( trip );
		}
		else
		{
			// Group - 1 for each address
			foreach( var ca in SelectedGroupAddresses )
			{
				var trip = CreateUpLiftShipmentForAddress( ca );
				trips.Add( trip );
			}
		}

		return trips;
	}

	private List<Trip> CreateUpPackShipment()
	{
		Logging.WriteLogLine( "Creating UpPack Shipment" );

		var trips = new List<Trip>();

		if( SelectedAddressGroupTab == 0 )
		{
			var trip = CreateUpPackShipmentForAddress( SelectedAddress );
			trips.Add( trip );
		}
		else
		{
			// Group - 1 for each address
			foreach( var ca in SelectedGroupAddresses )
			{
				var trip = CreateUpPackShipmentForAddress( ca );
				trips.Add( trip );
			}
		}

		return trips;
	}

	private List<Trip> CreateUpPCShipment()
	{
		Logging.WriteLogLine( "Creating Uppc Shipment" );

		var trips = new List<Trip>();

		if( SelectedAddressGroupTab == 0 )
		{
			var trip = CreateUpPCShipmentForAddress( SelectedAddress );
			trips.Add( trip );
		}
		else
		{
			// Group - 1 for each address
			foreach( var ca in SelectedGroupAddresses )
			{
				var trip = CreateUpPCShipmentForAddress( ca );
				trips.Add( trip );
			}
		}

		return trips;
	}

	private List<Trip> CreateUpTotalShipment()
	{
		Logging.WriteLogLine( "Creating Uptotal Shipment" );

		var trips = new List<Trip>();

		if( SelectedAddressGroupTab == 0 )
		{
			var trip = CreateUpTotalShipmentForAddress( SelectedAddress );
			trips.Add( trip );
		}
		else
		{
			// Group - 1 for each address
			foreach( var ca in SelectedGroupAddresses )
			{
				var trip = CreateUpTotalShipmentForAddress( ca );
				trips.Add( trip );
			}
		}

		return trips;
	}

	private List<Trip> CreateStartrackShipment()
	{
		Logging.WriteLogLine( "Creating Startrack Shipment" );

		var trips = new List<Trip>();

		if( SelectedAddressGroupTab == 0 )
		{
			var trip = CreateStartrackShipmentForAddress( SelectedAddress );
			trips.Add( trip );
		}
		else
		{
			// Group - 1 for each address
			foreach( var ca in SelectedGroupAddresses )
			{
				var trip = CreateStartrackShipmentForAddress( ca );
				trips.Add( trip );
			}
		}

		return trips;
	}

	private PmlRoute FindRouteForPcode( Trip trip )
	{
		var puPcode = trip.PickupAddressPostalCode;

		var pr = ( from R in Routes
		           where R.PostalCode == puPcode
		           select R ).FirstOrDefault();

		return pr;
	}

	private Trip AutoDispatchTrip( Trip trip )
	{
		var puPcode = trip.PickupAddressPostalCode;

		var pr = ( from R in Routes
		           where R.PostalCode == puPcode
		           select R ).FirstOrDefault();

		if( pr != null )
		{
			trip.Driver                 = pr.IdsRoute;
			trip.Status1                = STATUS.DISPATCHED;
			trip.BroadcastToDriverBoard = true;
		}

		return trip;
	}

	private Trip CreateUpLiftShipmentForAddress( CompanyAddress puAddr )
	{
		Trip trip = null;

		if( puAddr != null )
		{
			trip = new Trip
			       {
				       ServiceLevel = SelectedServiceLevel,
				       Pieces       = 1
			       };

			// NOTE - no attached Inventory items
			//trip.Packages.Add( new TripPackage
			//                   {
			//	                   PackageType = "Return",
			//	                   Pieces      = 1
			//                   } );
			var tps = new List<TripPackage> {new() {PackageType = "Return", Pieces = 1}};
			trip.Packages = tps;

			trip = PopulateTripWithCommonData( trip );

			trip = PopulateTripWithBillingPickupAndDeliveryAddresses( trip, puAddr, DeliveryAddress );

			// Now for auto-dispatch
			trip = AutoDispatchTrip( trip );
		}

		return trip;
	}

	private Trip CreateUpPackShipmentForAddress( CompanyAddress puAddr )
	{
		Trip trip = null;

		if( puAddr != null )
		{
			trip = new Trip
			       {
				       Pieces       = 9999,
				       ServiceLevel = SelectedServiceLevel
			       };

			trip = PopulateTripWithCommonData( trip );

			trip = PopulateTripWithBillingPickupAndDeliveryAddresses( trip, puAddr, DeliveryAddress );

			//trip.Packages.Add( new TripPackage
			//                   {
			//	                   PackageType = "Return"
			//                   } );

			//trip.Packages = tps;

			// Items
			var items       = new List<TripItem>();
			var totalPieces = 0;

			foreach( var pi in PackageList )
			{
				var ti = new TripItem
				         {
					         Barcode     = pi.Barcode,
					         Description = pi.Description,
					         Pieces      = pi.PackageQuantity,
					         ItemCode    = SelectedServiceLevel // pi.InventoryCode // uppc, uppack, ..
				         };

				// The quantities are negative
				totalPieces += Math.Abs( (int)pi.PackageQuantity );
				items.Add( ti );
			}

			//trip.Packages[ 0 ].Items  = items;
			//trip.Packages[ 0 ].Pieces = totalPieces;

			var tps = new List<TripPackage> {new() {PackageType = "Return", Items = items, Pieces = totalPieces}};

			//tps[0].Items = items;
			//tps[0].Pieces = totalPieces;

			trip.Packages = tps;

			//bool isFirst = true;
			//foreach (PmlInventory pi in PackageList)
			//{
			//    if (isFirst)
			//    {
			//        trip.PackageType = pi.Description;
			//        isFirst = false;
			//    }
			//    else
			//    {
			//        TripPackage ti = new TripPackage
			//        {
			//            // TODO
			//            //Barcode = pi.Barcode,
			//            //Description = pi.Description,
			//            //ItemCode = pi.InventoryCode,                            
			//            Pieces = 9999,
			//            Weight = 1
			//        };
			//        //ti.items

			//        trip.Packages.Add(ti);
			//    }
			//}

			// Now for auto-dispatch
			trip = AutoDispatchTrip( trip );
		}

		return trip;
	}

	private Trip CreateUpPCShipmentForAddress( CompanyAddress puAddr )
	{
		Trip trip = null;

		if( puAddr != null )
		{
			trip = new Trip
			       {
				       //PackageType = SelectedServiceLevel,
				       ServiceLevel = SelectedServiceLevel,
				       Pieces       = 1
			       };

			trip = PopulateTripWithCommonData( trip );

			trip = PopulateTripWithBillingPickupAndDeliveryAddresses( trip, puAddr, DeliveryAddress );

			//trip.Packages.Add( new TripPackage
			//                   {
			//	                   PackageType = "Return"
			//                   } );

			// Items
			var items       = new List<TripItem>();
			var totalPieces = 0;

			foreach( var pi in PackageList )
			{
				var ti = new TripItem
				         {
					         Barcode     = pi.Barcode,
					         Description = pi.Description,
					         Pieces      = pi.PackageQuantity,
					         ItemCode    = SelectedServiceLevel // pi.InventoryCode // uppc, uppack, ..
				         };

				totalPieces += (int)pi.PackageQuantity;
				items.Add( ti );
			}

			//trip.Packages[ 0 ].Items  = items;
			//trip.Packages[ 0 ].Pieces = totalPieces;
			var tps = new List<TripPackage> {new() {PackageType = "Return", Items = items, Pieces = totalPieces}};

			trip.Packages = tps;

			// Now for auto-dispatch
			trip = AutoDispatchTrip( trip );
		}

		return trip;
	}

	private Trip CreateUpTotalShipmentForAddress( CompanyAddress puAddr )
	{
		Trip trip = null;

		if( puAddr != null )
		{
			trip = new Trip
			       {
				       ServiceLevel = SelectedServiceLevel,
				       Pieces       = decimal.Parse( ShipmentDetailsQuantity )
			       };

			// NOTE - no attached Inventory items
			//trip.Packages.Add( new TripPackage
			//                   {
			//	                   PackageType = "Return",
			//	                   Pieces      = trip.Pieces
			//                   } );
			var tps = new List<TripPackage> {new() {PackageType = "Return", Pieces = trip.Pieces}};
			trip.Packages = tps;

			trip = PopulateTripWithCommonData( trip );

			trip = PopulateTripWithBillingPickupAndDeliveryAddresses( trip, puAddr, DeliveryAddress );

			// Now for auto-dispatch
			trip = AutoDispatchTrip( trip );
		}

		return trip;
	}

	private Trip CreateStartrackShipmentForAddress( CompanyAddress puAddr )
	{
		Trip trip = null;

		if( puAddr != null )
		{
			trip = new Trip
			       {
				       ServiceLevel = SelectedServiceLevel,
				       Pieces       = 1
			       };

			// NOTE - no attached Inventory items
			//trip.Packages.Add( new TripPackage
			//                   {
			//	                   PackageType = "Return",
			//	                   Pieces      = 1
			//                   } );
			var tps = new List<TripPackage> {new() {PackageType = "Return", Pieces = 1}};
			trip.Packages = tps;

			trip = PopulateTripWithCommonData( trip );

			trip = PopulateTripWithBillingPickupAndDeliveryAddresses( trip, puAddr, DeliveryAddress );

			// Now for auto-dispatch
			trip = AutoDispatchTrip( trip );
		}

		return trip;
	}

	private Trip PopulateTripWithCommonData( Trip trip )
	{
		trip.AccountId                = SelectedAccountId;
		trip.BroadcastToDispatchBoard = true;
		trip.BroadcastToDriverBoard   = true;

		var now = DateTimeOffset.Now;
		trip.CallTime  = now;
		trip.ReadyTime = now;
		trip.DueTime   = now;

		trip.ServiceLevel = SelectedServiceLevel;
		trip.PackageType  = "Return";
		trip.Status1      = STATUS.ACTIVE;
		trip.TripCharges  = new List<TripCharge>();

		return trip;
	}

	private Trip PopulateTripWithBillingPickupAndDeliveryAddresses( Trip trip, CompanyAddress pu, CompanyAddress del )
	{
		if( ( trip != null ) && ( pu != null ) && ( del != null ) )
		{
			trip.PickupAddressAddressLine1  = pu.AddressLine1;
			trip.PickupAddressAddressLine2  = pu.AddressLine2;
			trip.PickupAddressBarcode       = pu.LocationBarcode;
			trip.PickupAddressCity          = pu.City;
			trip.PickupAddressCountry       = pu.Country;
			trip.PickupAddressCountryCode   = pu.CountryCode;
			trip.PickupAddressEmailAddress  = pu.EmailAddress;
			trip.PickupAddressEmailAddress1 = pu.EmailAddress1;
			trip.PickupAddressEmailAddress2 = pu.EmailAddress2;
			trip.PickupAddressFax           = pu.Fax;
			trip.PickupAddressLatitude      = pu.Latitude;
			trip.PickupAddressLongitude     = pu.Longitude;
			trip.PickupAddressMobile        = pu.Mobile;
			trip.PickupAddressMobile1       = pu.Mobile1;
			trip.PickupAddressNotes         = pu.Notes;
			trip.PickupAddressPhone         = pu.Phone;
			trip.PickupAddressPhone1        = pu.Phone1;
			trip.PickupAddressPostalBarcode = pu.PostalBarcode;
			trip.PickupAddressPostalCode    = pu.PostalCode;
			trip.PickupAddressRegion        = pu.Region;
			trip.PickupAddressSuite         = pu.Suite;
			trip.PickupAddressVicinity      = pu.Vicinity;
			trip.PickupCompanyName          = pu.CompanyName;
			trip.PickupZone                 = pu.ZoneName;

			trip.DeliveryAddressAddressLine1  = del.AddressLine1;
			trip.DeliveryAddressAddressLine2  = del.AddressLine2;
			trip.DeliveryAddressBarcode       = del.LocationBarcode;
			trip.DeliveryAddressCity          = del.City;
			trip.DeliveryAddressCountry       = del.Country;
			trip.DeliveryAddressCountryCode   = del.CountryCode;
			trip.DeliveryAddressEmailAddress  = del.EmailAddress;
			trip.DeliveryAddressEmailAddress1 = del.EmailAddress1;
			trip.DeliveryAddressEmailAddress2 = del.EmailAddress2;
			trip.DeliveryAddressFax           = del.Fax;
			trip.DeliveryAddressLatitude      = del.Latitude;
			trip.DeliveryAddressLongitude     = del.Longitude;
			trip.DeliveryAddressMobile        = del.Mobile;
			trip.DeliveryAddressMobile1       = del.Mobile1;
			trip.DeliveryAddressNotes         = del.Notes;
			trip.DeliveryAddressPhone         = del.Phone;
			trip.DeliveryAddressPhone1        = del.Phone1;
			trip.DeliveryAddressPostalBarcode = del.PostalBarcode;
			trip.DeliveryAddressPostalCode    = del.PostalCode;
			trip.DeliveryAddressRegion        = del.Region;
			trip.DeliveryAddressSuite         = del.Suite;
			trip.DeliveryAddressVicinity      = del.Vicinity;
			trip.DeliveryCompanyName          = del.CompanyName;
			trip.DeliveryZone                 = del.ZoneName;

			trip.BillingAddressAddressLine1  = del.AddressLine1;
			trip.BillingAddressAddressLine2  = del.AddressLine2;
			trip.BillingAddressBarcode       = del.Barcode;
			trip.BillingAddressCity          = del.City;
			trip.BillingAddressCountry       = del.Country;
			trip.BillingAddressCountryCode   = del.CountryCode;
			trip.BillingAddressEmailAddress  = del.EmailAddress;
			trip.BillingAddressEmailAddress1 = del.EmailAddress1;
			trip.BillingAddressEmailAddress2 = del.EmailAddress2;
			trip.BillingAddressFax           = del.Fax;
			trip.BillingAddressLatitude      = del.Latitude;
			trip.BillingAddressLongitude     = del.Longitude;
			trip.BillingAddressMobile        = del.Mobile;
			trip.BillingAddressMobile1       = del.Mobile1;
			trip.BillingAddressNotes         = del.Notes;
			trip.BillingAddressPhone         = del.Phone;
			trip.BillingAddressPhone1        = del.Phone1;
			trip.BillingAddressPostalBarcode = del.PostalBarcode;
			trip.BillingAddressPostalCode    = del.PostalCode;
			trip.BillingAddressRegion        = del.Region;
			trip.BillingAddressSuite         = del.Suite;
			trip.BillingAddressVicinity      = del.Vicinity;
			trip.BillingCompanyName          = del.CompanyName;
		}

		if( trip.PickupZone.IsNullOrWhiteSpace() && trip.PickupAddressPostalCode.IsNotNullOrWhiteSpace() )
		{
			Logging.WriteLogLine( "PickupZone is empty - looking up " + trip.PickupAddressPostalCode );

			if( PmlRoutes.ContainsKey( trip.PickupAddressPostalCode ) )
				trip.PickupZone = PmlRoutes[ trip.PickupAddressPostalCode ].Operation + "+" + PmlRoutes[ trip.PickupAddressPostalCode ].Region;
			Logging.WriteLogLine( "Setting PickupZone " + trip.PickupZone );
		}

		if( trip.DeliveryZone.IsNullOrWhiteSpace() && trip.DeliveryAddressPostalCode.IsNotNullOrWhiteSpace() )
		{
			Logging.WriteLogLine( "DeliveryZone is empty - looking up " + trip.DeliveryAddressPostalCode );

			if( PmlRoutes.ContainsKey( trip.DeliveryAddressPostalCode ) )
				trip.DeliveryZone = PmlRoutes[ trip.DeliveryAddressPostalCode ].Operation + "+" + PmlRoutes[ trip.DeliveryAddressPostalCode ].Region;
			Logging.WriteLogLine( "Setting DeliveryZone " + trip.DeliveryZone );
		}

		return trip;
	}
#endregion

#region Address Timer
	public bool IsUpdateAddressesRunning()
	{
		var yes = false;

		if( ( updateAddressesTimer != null ) && updateAddressesTimer.IsEnabled )
			yes = true;

		return yes;
	}

	/// <summary>
	///     Stops the update addresses timer.
	/// </summary>
	public void StopUpdateAddresses()
	{
		if( updateAddressesTimer != null )
		{
			Logging.WriteLogLine( "Stopping timer" );
			updateAddressesTimer.Stop();
		}
	}

	/// <summary>
	///     Starts the update trips timer.
	/// </summary>
	public void StartUpdateAddresses()
	{
		Logging.WriteLogLine( "Starting timer" );

		// var updateTripsDelegate = new TimerCallback(UpdateTripsChecker);
		// var updateTripsChecker = new UpdateTripsChecker();

		// Run every 15 seconds
		// timerUpdateTrips = new Timer(updateTripsChecker.UpdateTrips, null, 0, 15 * 1000);

		updateAddressesTimer          =  new DispatcherTimer();
		updateAddressesTimer.Tick     += UpdateAddresses_Tick;
		updateAddressesTimer.Interval =  new TimeSpan( 0, 0, 15 );
		updateAddressesTimer.Start();
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void UpdateAddresses_Tick( object sender, EventArgs e )
	{
		//Logging.WriteLogLine("UpdateAddresses fired");
		LoadAddresses();
	}
#endregion
}