﻿using System.Windows.Controls;
using System.Windows.Input;

namespace ViewModels.RateWizard;

/// <summary>
///     Interaction logic for EditRateFormulaWindow.xaml
/// </summary>
public partial class EditRateFormulaWindow : Window
{
	private bool cancelled;

	public string ShowEditRateFormulaWindow( string currentFormula )
	{
		var formula = string.Empty;

		if( DataContext is EditRateFormulaWindowModel Model )
		{
			if( ( currentFormula != null ) && ( currentFormula.Length > 0 ) )
			{
				TcResult.SelectedIndex = IsSimpleValue( currentFormula ) ? 0 : 1;
				Model.CurrentFormula   = currentFormula;
				Model.LoadFormula();
				//LoadFormula(Model);
			}

			ShowDialog();

			if( !cancelled )
			{
				Model.BuildFormula();
				formula = Model.NewFormula;
			}
		}

		return formula;
	}

	public EditRateFormulaWindow( Window owner )
	{
		Owner = owner;
		InitializeComponent();

		CbWhatPricingObject.Focus();
	}

	private bool IsSimpleValue( string formula )
	{
		var yes   = true;
		var lines = formula.Split( '\n' );

		for( var i = 1; i < lines.Length; i++ )
		{
			if( lines[ i ].StartsWith( "Total" ) )
			{
				var pieces = lines[ i ].Split( ' ' );

				if( pieces.Length > 3 )
					yes = false;
			}
		}
		return yes;
	}

	private void BtnSave_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}

	private void BtnCancel_Click( object sender, RoutedEventArgs e )
	{
		cancelled = true;

		if( DataContext is EditRateFormulaWindowModel Model )
			Model.NewFormula = Model.CurrentFormula;

		Close();
	}

	private void RbResultFormula_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.IsResultFormulaChecked = true;
			Model.IsResultValueChecked   = false;
		}
	}

	private void RbResultFormula_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.IsResultFormulaChecked = false;
			Model.IsResultValueChecked   = true;
		}
	}

	private void RbResultValue_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.IsResultFormulaChecked = false;
			Model.IsResultValueChecked   = true;
		}
	}

	private void RbResultValue_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.IsResultFormulaChecked = true;
			Model.IsResultValueChecked   = false;
		}
	}

	private void RbResultValue_Click( object sender, RoutedEventArgs e )
	{
		TcResult.SelectedIndex = 0;

		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.TcResultTabSelected    = 0;
			Model.IsResultFormulaChecked = false;
			Model.IsResultValueChecked   = true;
		}
	}

	private void RbResultFormula_Click( object sender, RoutedEventArgs e )
	{
		TcResult.SelectedIndex = 1;

		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.TcResultTabSelected    = 1;
			Model.IsResultFormulaChecked = true;
			Model.IsResultValueChecked   = false;
		}
	}

	private void EnsureDecimal( object sender, TextCompositionEventArgs e )
	{
		var          ToCheck      = e.Text.Substring( e.Text.Length - 1 );
		const string DECIMAL_CHAR = ".";

		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) && !ToCheck.Equals( DECIMAL_CHAR ) )
			e.Handled = true;
	}

	private void BtnTest_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditRateFormulaWindowModel Model )
		{
			Model.BuildFormula();

			if( Model.NewFormula != string.Empty )
			{
				var result = EvaluateRateFormulaHelper.EvaluateFormulaPiece( Model.NewFormula, Model.TestPieces, Model.TestWeight, Model.TestMileage, Model.IsResultValueChecked );
				//decimal result = Model.EvaluateFormula(Model.NewFormula, Model.TestPieces, Model.TestWeight, Model.IsResultValueChecked);
				//decimal pieces = (decimal)DudTestPieces.Value;
				//decimal weight = (decimal)DudTestWeight.Value;
				//decimal result = Model.EvaluateFormula(Model.NewFormula, pieces, weight);
				DudTestResult.Text = result.ToString( "F2" );
			}
		}
	}

	private void CbResultFormulaPricingObject_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( ( DudResultFormulaPricingObjectValue != null ) && DataContext is EditRateFormulaWindowModel Model )
		{
			if( Model.SelectedResultFormulaPricingObject != null )
			{
				if( ( Model.SelectedResultFormulaPricingObject.IndexOf( "-" ) > -1 ) || ( Model.SelectedResultFormulaPricingObject.IndexOf( "+" ) > 0 ) )
					DudResultFormulaPricingObjectValue.Visibility = Visibility.Visible;
				else
				{
					DudResultFormulaPricingObjectValue.Visibility = Visibility.Collapsed;
					Model.SelectedResultFormulaPricingObjectValue = 0;
				}
			}
		}
	}

	private void CbResultFormulaAnotherClauseMathOp_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( DataContext is EditRateFormulaWindowModel Model )
		{
			if( Model.SelectedResultFormulaAnotherClauseMathOp != null )
			{
				if( Model.SelectedResultFormulaAnotherClauseMathOp.Trim() != string.Empty )
				{
					CbResultFormulaSecondPricingObject.Visibility = Visibility.Visible;
					CbResultFormulaSecondMathOp.Visibility        = Visibility.Visible;
					DudResultFormulaSecondValue.Visibility        = Visibility.Visible;
				}
				else
				{
					if( CbResultFormulaSecondPricingObject != null )
					{
						CbResultFormulaSecondPricingObject.Visibility       = Visibility.Collapsed;
						DudResultFormulaSecondPricingObjectValue.Visibility = Visibility.Collapsed;
						CbResultFormulaSecondMathOp.Visibility              = Visibility.Collapsed;
						DudResultFormulaSecondValue.Visibility              = Visibility.Collapsed;
					}
				}
			}
		}
	}

	private void CbResultFormulaSecondPricingObject_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( ( DudResultFormulaSecondPricingObjectValue != null ) && DataContext is EditRateFormulaWindowModel Model )
		{
			if( Model.SelectedResultFormulaSecondPricingObject != null )
			{
				if( ( Model.SelectedResultFormulaSecondPricingObject.IndexOf( "-" ) > -1 ) || ( Model.SelectedResultFormulaSecondPricingObject.IndexOf( "+" ) > 0 ) )
					DudResultFormulaSecondPricingObjectValue.Visibility = Visibility.Visible;
				else
				{
					DudResultFormulaSecondPricingObjectValue.Visibility = Visibility.Collapsed;
					Model.SelectedResultFormulaSecondPricingObjectValue = 0;
				}
			}
		}
	}
}