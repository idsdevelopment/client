﻿#nullable enable

using System.Diagnostics;
using System.Threading;
using System.Windows.Controls;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.RateWizard;

public class RateWizardModel : ViewModelBase
{
	private const string RATE_WIZARD = "Rate Wizard";

	public bool Loaded
	{
		get { return Get(() => Loaded, false); }
		set { Set(() => Loaded, value); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;
		Logging.WriteLogLine("DEBUG Loaded: " + Loaded);

		//Task.WaitAll(
		//			 Task.Run(() =>
		//					   {
		//						   _ = GetZones();
		//					   })
		//			);
		Task.WaitAll(
					 Task.Run(() =>
							   {
								   _ = GetPackageTypes();
								   _ = GetServiceLevels();
								   //_ = GetZones();
							   })
					);

		Task.WaitAll(
					 Task.Run(() =>
							   {
								   _ = GetRateMatrixItems();
								   GetCharges();
								   //BuildSdRates();
							   })
					);

		//view.BuildPriceMatrix();
	}

	public Dictionary<string, List<PackageType>> DictServiceLevelPackageTypes = new();

	public List<DisplayServiceLevel> DisplayServiceLevelObjects = new();

	public List<PackageType> PackageTypeObjects = new();

	//public List<PackageType> PackageTypeObjects
	//{
	//    get { return Get(() => PackageTypeObjects, new List<PackageType>()); }
	//    set { Set(() => PackageTypeObjects, value); }
	//}


	public List<PackageType> PackageTypeObjectsToDelete = new();
	public List<ServiceLevel> ServiceLevelObjects = new();

	public List<ServiceLevel> ServiceLevelObjectsToDelete = new();
	public RateWizard? View;

	public void SetView(RateWizard rw)
	{
		View = rw;
	}

	//[DependsUpon(nameof(Loaded))]
	public void WhenLoadedChanges()
	{
		Logging.WriteLogLine("DEBUG Loaded: " + Loaded);
	}


	//public List<ServiceLevel> ServiceLevelObjects
	//{
	//    get { return Get(() => ServiceLevelObjects); }
	//    set { Set(() => ServiceLevelObjects, value); }
	//}

	public static string FindStringResource(string name) => (string)Application.Current.TryFindResource(name);

	#region Common
	public bool ArePendingServiceLevels
	{
		get => Get(() => ArePendingServiceLevels, false);
		set => Set(() => ArePendingServiceLevels, value);
	}

	public bool ArePendingPackageTypes
	{
		get => Get(() => ArePendingPackageTypes, false);
		set => Set(() => ArePendingPackageTypes, value);
	}

	public bool ArePendingZones
	{
		get => Get(() => ArePendingZones, false);
		set => Set(() => ArePendingZones, value);
	}

	public bool ArePendingRateMatricies
	{
		get => Get(() => ArePendingRateMatricies, false);
		set => Set(() => ArePendingRateMatricies, value);

		//set => ArePendingRateMatricies = !IsPriceMatrixLocked;
	}

	[DependsUpon(nameof(ArePendingRateMatricies))]
	public void WhenArePendingRateMatriciesChanges()
	{
		Logging.WriteLogLine("ArePendingRateMatricies: " + ArePendingRateMatricies);
	}

	public void SetArePendingRateMatricies(bool yes)
	{
		if (!IsPriceMatrixLocked)
		{
			ArePendingRateMatricies = yes;
			SetArePendingChanges(true);
		}
	}


	public string Test
	{
		get { return Get(() => Test, ""); }
		set { Set(() => Test, value); }
	}


	[DependsUpon(nameof(ArePendingServiceLevels))]
	[DependsUpon(nameof(ArePendingPackageTypes))]
	[DependsUpon(nameof(ArePendingZones))]
	[DependsUpon(nameof(ArePendingRateMatricies))]
	[DependsUpon(nameof(ArePendingSurcharges))]
	public string ArePendingChanges
	{
		get
		{
			var Pending = ArePendingServiceLevels || ArePendingPackageTypes || ArePendingZones || (ArePendingRateMatricies && !IsPriceMatrixLocked) || ArePendingSurcharges;
			Logging.WriteLogLine("ArePendingChanges: " + Pending);

			// return pending;
			var Visibility = "Collapsed";

			if (Pending)
				Visibility = "Visible";

			return Visibility;
		}
		set => Set(() => ArePendingChanges, value);
	}

	public void SaveAll()
	{
		Logging.WriteLogLine("Saving tabs");

		if (ArePendingServiceLevels)
			SaveServiceLevels();

		if (ArePendingPackageTypes)
			SavePackageTypes();

		if (ArePendingZones)
			Execute_SaveZones();

		if (ArePendingRateMatricies)
			Execute_SaveRateMatricies();

		if (ArePendingSurcharges)
			Execute_SaveSurcharges();

		//SetArePendingChanges(false);
		SetArePendingChanges(ArePendingChangesBool);
	}

	//     public string ArePendingChanges
	//     {
	//         //get { return Get(() => ArePendingChanges, ""); }
	//get
	//         {
	//	var pending = ArePendingServiceLevels || ArePendingPackageTypes || ArePendingZones;
	//             // return pending;
	//             var visibility = "Collapsed";
	//             //var visibility = "Visible";

	//             if (pending)
	//                 visibility = "Visible";

	//             return visibility;

	//         }
	//set { Set(() => ArePendingChanges, value); }
	//     }


	public bool ArePendingChangesBool
	{
		get
		{
			var Pending = ArePendingServiceLevels || ArePendingPackageTypes || ArePendingZones || ArePendingRateMatricies || ArePendingSurcharges;
			return Pending;
		}
	}


	public void SetArePendingChanges(bool arePendingChanges)
	{
		Logging.WriteLogLine("Setting ArePendingChanges: " + arePendingChanges);

		ArePendingChanges = arePendingChanges ? "Visible" : "Collapsed";
	}
	#endregion

	#region ServiceLevels Data
	public ObservableCollection<string> ServiceLevels
	{
		get => Get(() => ServiceLevels, GetServiceLevels);
		set => Set(() => ServiceLevels, value);
	}

	//public readonly ObservableCollection<string> ServiceLevelNames = new();

	public ObservableCollection<string> ServiceLevelNames
	{
		get => Get(() => ServiceLevelNames, new ObservableCollection<string>());
		set => Set(() => ServiceLevelNames, value);
	}


	//public ObservableCollection<DisplayServiceLevel> DisplayServiceLevels
	//{
	//    get { return Get(() => DisplayServiceLevels, GetServiceLevels); }
	//    set { Set(() => DisplayServiceLevels, value); }
	//}

	/// <summary>
	/// </summary>
	/// <returns></returns>
	private ObservableCollection<string> GetServiceLevels()
	{
		var ServiceLevels = new ObservableCollection<string>();

		//Task.Run( async () =>
		Task.Run(() =>
				  {
					  try
					  {
						  Logging.WriteLogLine("Getting service levels...");

						  // From Terry: Need to do this to avoid bad requests in the server log
						  //ServiceLevelList sls = null;
						  ServiceLevelListDetailed? Sls = null;

						  if (!IsInDesignMode)

						  {
							  //sls = await Azure.Client.RequestGetServiceLevels();
							  //sls = Azure.Client.RequestGetServiceLevels().Result;
							  Sls = Azure.Client.RequestGetServiceLevelsDetailed().Result;

							  Dispatcher.Invoke(() =>
												 {
													 ServiceLevelNames.Clear();

													 //foreach( var sl in sls )
													 //{
													 // //ServiceLevelNames.Add( sl );
													 // ServiceLevelNames.Add( sl.OldName );
													 //}
													 var Names = (from S in Sls
																  orderby S.SortOrder
																  select S.OldName).ToList();

													 foreach (var Name in Names)
														 ServiceLevelNames.Add(Name);

													 PreviousServiceLevelForMatrix = SelectedServiceLevelForMatrix = ServiceLevelNames.Count > 0 ? ServiceLevelNames[0] : "";
												 });

							  //RaisePropertyChanged(nameof(ServiceLevelNames));
						  }

						  if (Sls != null)
						  {
							  Logging.WriteLogLine("Got " + Sls.Count + " service levels ");
							  ServiceLevelObjects.Clear();

							  //List<string> strings = new List<string>();

							  SortedDictionary<int, ServiceLevel> Sd = new();
							  var ToAdd = new List<ServiceLevel>();

							  //ushort sortOrder = 0;
							  foreach (var Sl in Sls)
							  {
								  //var result = await Azure.Client.RequestGetServiceLevel( name );
								  /*var result = Azure.Client.RequestGetServiceLevel( name ).Result;

								  if( result != null )
								  {
									  var sl = result.ServiceLevel;
									  Logging.WriteLogLine( "Loading ServiceLevel: " + name + ", sortOrder: " + sl.SortOrder );

									  //sl.SortOrder = sortOrder++;
									  // Duplicate - put at end
									  if( sd.ContainsKey( sl.SortOrder ) )
									  {
										  //Logging.WriteLogLine("SortOrder already exists - changing it to " + (sd.Count + 1));
										  //sl.SortOrder = (ushort)(sd.Count + 1);
										  //Logging.WriteLogLine("SortOrder already exists - changing it to " + (sls.Count - 1));
										  //sl.SortOrder = (ushort)(sls.Count - 1);

										  //_ = Azure.Client.RequestAddUpdateServiceLevel( sl );

										  toAdd.Add( sl );
									  }
									  else
									  {
										  sd.Add( sl.SortOrder, sl );
									  }

									  //sd.Add( sl.SortOrder, sl );

									  //this.ServiceLevelObjects.Add(sl);
								  }*/

								  Logging.WriteLogLine("Loading ServiceLevel: " + Sl.OldName + ", sortOrder: " + Sl.SortOrder);

								  //sl.SortOrder = sortOrder++;
								  // Duplicate - put at end
								  if (Sd.ContainsKey(Sl.SortOrder))
								  {
									  //Logging.WriteLogLine("SortOrder already exists - changing it to " + (sd.Count + 1));
									  //sl.SortOrder = (ushort)(sd.Count + 1);
									  //Logging.WriteLogLine("SortOrder already exists - changing it to " + (sls.Count - 1));
									  //sl.SortOrder = (ushort)(sls.Count - 1);

									  //_ = Azure.Client.RequestAddUpdateServiceLevel( sl );

									  ToAdd.Add(Sl);
								  }
								  else
									  Sd.Add(Sl.SortOrder, Sl);
							  }

							  if (ToAdd.Count > 0)
							  {
								  foreach (var Sl in ToAdd)
								  {
									  Logging.WriteLogLine("SortOrder already exists - changing it to " + Sd.Count);
									  Sl.SortOrder = (ushort)Sd.Count;
									  _ = Azure.Client.RequestAddUpdateServiceLevel(Sl);

									  if (!Sd.ContainsKey(Sl.SortOrder))
										  Sd.Add(Sl.SortOrder, Sl);
								  }
							  }

							  // Check for holes
							  var Missing = new List<int>();

							  for (var I = 0; I < Sd.Count; I++)
							  {
								  if (!Sd.ContainsKey(I))
								  {
									  Logging.WriteLogLine("ERROR there isn't a service level with a sortorder of " + I);
									  Missing.Add(I);
								  }
							  }

							  if (Missing.Count > 0)
							  {
								  var Sds = new List<ServiceLevel>();

								  foreach (var Tmp in Sd.Keys)
									  Sds.Add(Sd[Tmp]);

								  for (var I = 0; I < Sds.Count; I++)
								  {
									  var Sl = Sds[I];
									  Sl.SortOrder = (ushort)I;
									  _ = Azure.Client.RequestAddUpdateServiceLevel(Sl);
								  }
							  }

							  var SlNames = new List<string>();

							  foreach (var Key in Sd.Keys)
							  {
								  // slNames.Add(sd[key].N);
								  var Line = FormatServiceLevelDetailed(Sd[Key]);
								  Logging.WriteLogLine("Adding " + Line);
								  SlNames.Add(Line);

								  // Make sure that they are in matching order
								  ServiceLevelObjects.Add(Sd[Key]);

								  var Dsl = new DisplayServiceLevel(Sd[Key])
								  {
									  Name = Line
								  };
								  DisplayServiceLevelObjects.Add(Dsl);
							  }

							  Dispatcher.Invoke(() =>
												 {
													 //serviceLevels.AddRange(slNames);
													 foreach (var Name in SlNames)
														 ServiceLevels.Add(Name);
												 });

							  // serviceLevels.AddRange(slNames);
							  Logging.WriteLogLine("Finished loading service levels");
						  }
					  }
					  catch (Exception E)
					  {
						  //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
						  Logging.WriteLogLine("Exception thrown: " + E);
						  MessageBox.Show("Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK);
					  }
				  });

		return ServiceLevels;
	}

	public string SelectedServiceLevel
	{
		get => Get(() => SelectedServiceLevel, "");
		set => Set(() => SelectedServiceLevel, value);
	}


	public string SelectedServiceLevelName
	{
		get => Get(() => SelectedServiceLevelName, "");
		set => Set(() => SelectedServiceLevelName, value);
	}


	public DateTime SelectedServiceLevelStartTime
	{
		get => Get(() => SelectedServiceLevelStartTime, new DateTime());
		set => Set(() => SelectedServiceLevelStartTime, value);
	}


	public TimeSpan SelectedServiceLevelStartTimeTimeSpan
	{
		get => Get(() => SelectedServiceLevelStartTimeTimeSpan, new TimeSpan());
		set => Set(() => SelectedServiceLevelStartTimeTimeSpan, value);
	}


	public DateTime SelectedServiceLevelEndTime
	{
		get => Get(() => SelectedServiceLevelEndTime, new DateTime());
		set => Set(() => SelectedServiceLevelEndTime, value);
	}

	public TimeSpan SelectedServiceLevelEndTimeTimeSpan
	{
		get => Get(() => SelectedServiceLevelEndTimeTimeSpan, new TimeSpan());
		set => Set(() => SelectedServiceLevelEndTimeTimeSpan, value);
	}

	[DependsUpon(nameof(SelectedServiceLevelStartTime))]
	[DependsUpon(nameof(SelectedServiceLevelEndTime))]
	public void WhenStartAndEndTimeChange()
	{
		Logging.WriteLogLine("StartTime: " + SelectedServiceLevelStartTime + ", EndTime: " + SelectedServiceLevelEndTime);
		SelectedServiceLevelStartTimeTimeSpan = new TimeSpan(SelectedServiceLevelStartTime.Hour, SelectedServiceLevelStartTime.Minute, SelectedServiceLevelStartTime.Second);
		SelectedServiceLevelEndTimeTimeSpan = new TimeSpan(SelectedServiceLevelEndTime.Hour, SelectedServiceLevelEndTime.Minute, SelectedServiceLevelEndTime.Second);
	}

	public bool SelectedServiceLevelSun
	{
		get => Get(() => SelectedServiceLevelSun, false);
		set => Set(() => SelectedServiceLevelSun, value);
	}

	public bool SelectedServiceLevelMon
	{
		get => Get(() => SelectedServiceLevelMon, false);
		set => Set(() => SelectedServiceLevelMon, value);
	}

	public bool SelectedServiceLevelTue
	{
		get => Get(() => SelectedServiceLevelTue, false);
		set => Set(() => SelectedServiceLevelTue, value);
	}

	public bool SelectedServiceLevelWed
	{
		get => Get(() => SelectedServiceLevelWed, false);
		set => Set(() => SelectedServiceLevelWed, value);
	}

	public bool SelectedServiceLevelThu
	{
		get => Get(() => SelectedServiceLevelThu, false);
		set => Set(() => SelectedServiceLevelThu, value);
	}

	public bool SelectedServiceLevelFri
	{
		get => Get(() => SelectedServiceLevelFri, false);
		set => Set(() => SelectedServiceLevelFri, value);
	}

	public bool SelectedServiceLevelSat
	{
		get => Get(() => SelectedServiceLevelSat, false);
		set => Set(() => SelectedServiceLevelSat, value);
	}


	public string SelectedServiceLevelDeliveryHours
	{
		get => Get(() => SelectedServiceLevelDeliveryHours, "");
		set => Set(() => SelectedServiceLevelDeliveryHours, value);
	}


	public TimeSpan SelectedServiceLevelDeliveryHoursTimeSpan
	{
		get => Get(() => SelectedServiceLevelDeliveryHoursTimeSpan, new TimeSpan());
		set => Set(() => SelectedServiceLevelDeliveryHoursTimeSpan, value);
	}


	public string SelectedServiceLevelWarn
	{
		get => Get(() => SelectedServiceLevelWarn, "");
		set => Set(() => SelectedServiceLevelWarn, value);
	}


	public string SelectedServiceLevelCritical
	{
		get => Get(() => SelectedServiceLevelCritical, "");
		set => Set(() => SelectedServiceLevelCritical, value);
	}


	public string SelectedServiceLevelBeyond
	{
		get => Get(() => SelectedServiceLevelBeyond, "");
		set => Set(() => SelectedServiceLevelBeyond, value);
	}


	public bool IsEditingServiceLevel
	{
		get => Get(() => IsEditingServiceLevel, false);
		set => Set(() => IsEditingServiceLevel, value);
	}

	[DependsUpon(nameof(IsEditingServiceLevel))]
	public bool IsServiceLevelTextBoxReadOnly
	{
		get => Get(() => IsServiceLevelTextBoxReadOnly, !IsEditingServiceLevel);
		set => Set(() => IsServiceLevelTextBoxReadOnly, value);
	}

	[DependsUpon(nameof(IsEditingServiceLevel))]
	[DependsUpon(nameof(IsServiceLevelTextBoxReadOnly))]
	public bool HasIsEditingServiceChanged
	{
		get
		{
			Logging.WriteLogLine("IsEditingServiceLevel: " + IsEditingServiceLevel);
			Logging.WriteLogLine("IsServiceLevelTextBoxReadOnly: " + IsServiceLevelTextBoxReadOnly);

			return true;
		}
	}


	public ServiceLevel CurrentlyEditedServiceLevel
	{
		get => Get(() => CurrentlyEditedServiceLevel, new ServiceLevel());
		set => Set(() => CurrentlyEditedServiceLevel, value);
	}


	public bool DoesServiceLevelNameExist(string name)
	{
		var Exists = false;

		if (ServiceLevelObjects.Count > 0)
		{
			foreach (var Sl in ServiceLevelObjects)
			{
				if (Sl.N.Trim() == name)
				{
					Exists = true;

					break;
				}
			}
		}

		return Exists;
	}

	public void ReloadServiceLevels()
	{
		ServiceLevels = GetServiceLevels();
	}


	public string HelpUriServiceLevel
	{
		get { return Get(() => HelpUriServiceLevel, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/644186113/How+to+use+Rate+Wizard"); }
	}


	public ObservableCollection<string> ServiceLevelPackageTypes
	{
		get => Get(() => ServiceLevelPackageTypes, new ObservableCollection<string>());
		set => Set(() => ServiceLevelPackageTypes, value);
	}

	public ObservableCollection<string> SelectedServiceLevelPackageTypes
	{
		get => Get(() => SelectedServiceLevelPackageTypes, new ObservableCollection<string>());
		set => Set(() => SelectedServiceLevelPackageTypes, value);
	}

	/// <summary>
	///     This is the one selected in the combo box
	/// </summary>
	public string SelectedServiceLevelPackageType
	{
		get => Get(() => SelectedServiceLevelPackageType, "");
		set => Set(() => SelectedServiceLevelPackageType, value);
	}
	#endregion

	#region ServiceLevel Commands
	//public void EditServiceLevel()
	//{
	//    string line = this.SelectedServiceLevel;
	//    // sl is the detailed string - need to extract the service level
	//    string slName = this.GetServiceLevelFromDetailed(line);
	//    //int selectedIndex = lbServiceLevels.SelectedIndex;

	//    List<ServiceLevel> sls = this.ServiceLevelObjects;

	//    //if (selectedIndex > -1)
	//    //{
	//    //    ServiceLevel sl = sls[selectedIndex];
	//    //    if (sl != null)
	//    //    {
	//    //        this.SelectedServiceLevelName = sl.N;
	//    //        this.SelectedServiceLevelStartTime = sl.StartTime.ToString();
	//    //        this.SelectedServiceLevelEndTime = sl.EndTime.ToString();
	//    //    }
	//    //}
	//}

	//private string GetServiceLevelFromDetailed(string line)
	//{
	//    string sl = string.Empty;

	//    // Service[Next Day,DEL hrs: 00:00:00,END time: 00:00:00]
	//    //int start = "Service[".Length;
	//    int start = "[".Length;
	//    string endTag = ",DEL";
	//    int end = line.IndexOf(endTag) - endTag.Length;
	//    sl = line.Substring(start, end);

	//    return sl;
	//}

	/// <summary>
	///     Note: sortOrder hasn't been modified yet.
	/// </summary>
	/// <param
	///     name="sl">
	/// </param>
	/// <param
	///     name="newSortOrder">
	/// </param>
	public void MoveServiceLevelLocally(ServiceLevel sl, int newSortOrder)
	{
		Logging.WriteLogLine("Moving ServiceLevel from " + sl.SortOrder + " to " + newSortOrder);

		ServiceLevelObjects[newSortOrder].SortOrder = sl.SortOrder;
		ServiceLevelObjects[sl.SortOrder].SortOrder = (ushort)newSortOrder;

		var Sd = new SortedDictionary<int, ServiceLevel>();

		foreach (var Tmp in ServiceLevelObjects)
		{
			if (!Sd.ContainsKey(Tmp.SortOrder))
				Sd.Add(Tmp.SortOrder, Tmp);
			else
				Logging.WriteLogLine("ERROR sd already contains sortOrder: " + Tmp.SortOrder);
		}

		var NewList = new List<ServiceLevel>();

		foreach (var Key in Sd.Keys)
			NewList.Add(Sd[Key]);

		ServiceLevelObjects.Clear();
		ServiceLevelObjects.AddRange(NewList);

		var SlNames = new List<string>();

		foreach (var Tmp in ServiceLevelObjects)
		{
			var Line = FormatServiceLevelDetailed(Tmp);
			SlNames.Add(Line);
		}

		var ServiceLevels = new ObservableCollection<string>();

		Dispatcher.Invoke(() =>
						   {
							   //serviceLevels.AddRange(slNames);
							   foreach (var Name in SlNames)
								   ServiceLevels.Add(Name);
						   });

		this.ServiceLevels = ServiceLevels;

		ArePendingServiceLevels = true;
		SetArePendingChanges(true);
	}

	public void DeleteServiceLevelLocally(ServiceLevel sl)
	{
		if (sl != null)
		{
			var SlName = sl.NewName;
			Logging.WriteLogLine("Deleting Service Level: " + SlName);

			var NewList = new List<ServiceLevel>();

			foreach (var Tmp in ServiceLevelObjects)
			{
				if (Tmp.N == SlName)
				{
					// OldName == "Value", NewName == "" Is a Delete
					sl.OldName = SlName;
					sl.NewName = string.Empty;
					ServiceLevelObjectsToDelete.Add(sl);
				}
				else
					NewList.Add(Tmp);
			}

			// At this point, there is a hole in the sortorder sequence
			var Sd = new SortedDictionary<int, ServiceLevel>();

			for (var I = 0; I < NewList.Count; I++)
			{
				var Tmp = NewList[I];

				if (Tmp.SortOrder != I)
				{
					Tmp.SortOrder = (ushort)I;
					_ = Azure.Client.RequestAddUpdateServiceLevel(Tmp);
				}
				Sd.Add(I, Tmp);
			}

			NewList.Clear();

			foreach (var Key in Sd.Keys)
				NewList.Add(Sd[Key]);

			ServiceLevelObjects.Clear();
			ServiceLevelObjects.AddRange(NewList);

			var SlNames = new List<string>();

			foreach (var Tmp in ServiceLevelObjects)
			{
				var Line = FormatServiceLevelDetailed(Tmp);
				SlNames.Add(Line);
			}

			var ServiceLevels = new ObservableCollection<string>();

			Dispatcher.Invoke(() =>
							   {
								   //serviceLevels.AddRange(slNames);
								   foreach (var Name in SlNames)
									   ServiceLevels.Add(Name);
							   });

			this.ServiceLevels = ServiceLevels;

			ArePendingServiceLevels = true;
			SetArePendingChanges(true);
		}
	}

	public void SaveServiceLevels()
	{
		Logging.WriteLogLine("Saving Service Levels");

		Task.Run(async () =>
				  {
					  try
					  {
						  foreach (var Sl in ServiceLevelObjects)
						  {
							  Logging.WriteLogLine("Saving NewName: " + Sl.NewName + ", OldName: " + Sl.OldName);
							  await Azure.Client.RequestAddUpdateServiceLevel(Sl);
						  }

						  foreach (var Sl in ServiceLevelObjectsToDelete)
						  {
							  Logging.WriteLogLine("Deleting NewName: " + Sl.NewName + ", OldName: " + Sl.OldName);
							  await Azure.Client.RequestAddUpdateServiceLevel(Sl);
						  }

						  ServiceLevelObjectsToDelete = new List<ServiceLevel>();

						  // Need pause to give server time to finish processing
						  Thread.Sleep(100);
						  var Sls = GetServiceLevels();

						  Dispatcher.Invoke(() =>
											 {
												 ServiceLevels = Sls;
											 });

						  ArePendingServiceLevels = false;

						  //if (ArePendingChangesBool)
						  //                     {
						  // SetArePendingChanges(false);
						  //                     }
						  SetArePendingChanges(ArePendingChangesBool);
					  }
					  catch (Exception E)
					  {
						  Logging.WriteLogLine("Exception thrown: " + E);
						  MessageBox.Show("Error Saving Service Levels:\n" + E, "Error", MessageBoxButton.OK);
					  }
				  });
	}


	//public IList ServiceLevelDetailedStrings
	//{
	//    get { return Get(() => ServiceLevelDetailedStrings, GetServiceLevelDetailedStrings); }
	//    set { Set(() => ServiceLevelDetailedStrings, value); }
	//}

	//private IList GetServiceLevelDetailedStrings()
	//{
	//    List<string> strings = new List<string>();

	//    GetServiceLevels();

	//    if (this.serviceLevelObjects.Count > 0)
	//    {
	//        foreach (ServiceLevel sl in this.serviceLevelObjects)
	//        {
	//            string line = this.FormatServiceLevelDetailed(sl);
	//            strings.Add(line);
	//        }
	//    }

	//    return strings;
	//}

	private string FormatServiceLevelDetailed(ServiceLevel sl)
	{
		var Service = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionService");

		var DelHrs = sl.DeliveryHours.ToString();
		var StartTime = sl.StartTime.ToString();
		var EndTime = sl.EndTime.ToString();

		if (DelHrs.Contains("23:59:59.9990000"))
			DelHrs = "00:00:00";

		if (StartTime.Contains("23:59:59.9990000"))
			StartTime = "00:00:00";

		if (EndTime.Contains("23:59:59.9990000"))
			EndTime = "00:00:00";
		DelHrs = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionDelHrs") + " " + DelHrs;
		StartTime = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionStartTime") + " " + StartTime;
		EndTime = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionEndTime") + " " + EndTime;

		//var days = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionDays") + " ";
		var Days = string.Empty;

		if (sl.Sunday && sl.Monday && sl.Tuesday && sl.Wednesday && sl.Thursday && sl.Friday && sl.Saturday)
			Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionEveryDay");
		else
		{
			if (sl.Sunday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelSun") + ",";

			if (sl.Monday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelMon") + ",";

			if (sl.Tuesday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelTue") + ",";

			if (sl.Wednesday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelWed") + ",";

			if (sl.Thursday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelThu") + ",";

			if (sl.Friday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelFri") + ",";

			if (sl.Saturday)
				Days += (string)Application.Current.TryFindResource("RateWizardTabServiceLevelSat") + ",";

			if (Days.Length > 0) // Empty if no days are checked
				Days = Days.Substring(0, Days.Length - 1);
		}

		var Line = Service + " [" + sl.N.Trim() + ", " + DelHrs + ", " + StartTime + ", " + EndTime + ", " + Days + "]" + " (SortOrder " + sl.SortOrder + ")";

		return Line;
	}

	private void UpdateLocalServiceLevels(ServiceLevel sl)
	{
		var SlName = sl.NewName;

		Logging.WriteLogLine("Updating " + SlName);

		var Exists = false;

		for (var I = 0; I < ServiceLevelObjects.Count; I++)
		{
			var Level = ServiceLevelObjects[I];

			if (Level.N == SlName)
			{
				Exists = true;
				ServiceLevelObjects[I] = sl;

				break;
			}
		}

		if (!Exists)
		{
			sl.OldName = string.Empty;
			sl.NewName = sl.N;
			ServiceLevelObjects.Add(sl);
		}

		var SlNames = new List<string>();

		foreach (var Tmp in ServiceLevelObjects)
		{
			var Line = FormatServiceLevelDetailed(Tmp);
			SlNames.Add(Line);
		}

		var ServiceLevels = new ObservableCollection<string>();

		Dispatcher.Invoke(() =>
						   {
							   //serviceLevels.AddRange(slNames);
							   foreach (var Name in SlNames)
								   ServiceLevels.Add(Name);
						   });

		this.ServiceLevels = ServiceLevels;

		ArePendingServiceLevels = true;
		SetArePendingChanges(true);
	}

	/// <summary>
	///     Handles the update process.
	/// </summary>
	/// <param
	///     name="sl">
	/// </param>
	/// <returns></returns>
	public bool AddUpdateServiceLevel(ServiceLevel sl)
	{
		var Success = true;

		Logging.WriteLogLine("Adding/Updating Service Level: " + sl.N);
		UpdateLocalServiceLevels(sl);

		ArePendingServiceLevels = true;
		SetArePendingChanges(true);

		return Success;
	}


	public void Execute_ShowHelpServiceLevel()
	{
		Logging.WriteLogLine("Opening " + HelpUriServiceLevel);
		var Uri = new Uri(HelpUriServiceLevel);
		Process.Start(new ProcessStartInfo(Uri.AbsoluteUri));
	}
	#endregion

	#region PackageTypes Data
	public ObservableCollection<string> PackageTypes
	{
		get => Get(() => PackageTypes, GetPackageTypes());
		set => Set(() => PackageTypes, value);
	}

	//public readonly ObservableCollection<string> PackageTypeNames = new();

	public ObservableCollection<string> PackageTypeNames
	{
		get => Get(() => PackageTypeNames, new ObservableCollection<string>());
		set => Set(() => PackageTypeNames, value);
	}


	private ObservableCollection<string> GetPackageTypes()
	{
		var PackageTypes = new ObservableCollection<string>();

		Task.Run(() =>
				  {
					  try
					  {
						  Logging.WriteLogLine("Getting package types...");

						  PackageTypeList? Pts = null;

						  if (!IsInDesignMode)
						  {
							  //pts = await Azure.Client.RequestGetPackageTypes();
							  Pts = Azure.Client.RequestGetPackageTypes().Result;

							  Dispatcher.Invoke(() =>
												 {
													 if (View is not null)
													 {
														 PackageTypeNames.Clear();

														 foreach (var Pt in Pts)
															 PackageTypeNames.Add(Pt.Description);

														 //RaisePropertyChanged(nameof(PackageTypeNames));
														 View.CbPackageTypes.SelectedIndex = 0;

														 // Make sure these are set in the RateMatrix
														 PreviousPackageTypeForMatrix = SelectedPackageTypeForMatrix = PackageTypeNames.Count > 0 ? PackageTypeNames[0] : "";

														 View.PopulateSurchargePackageTypeList();
													 }
												 });
						  }

						  if (Pts != null)
						  {
							  var Sd = new SortedDictionary<int, PackageType>();
							  var ToAdd = new List<PackageType>();

							  // KLUDGE this task seems to run multiple time
							  if (PackageTypeObjects.Count == 0)
							  {
								  PackageTypeObjects = new List<PackageType>();

								  //PackageTypeObjects.Clear();

								  foreach (var Pt in Pts)
								  {
									  Logging.WriteLogLine("Loading PackageType: " + Pt.Description + ", sortOrder: " + Pt.SortOrder);

									  // If there is a duplicate sortOrder, put at bottom
									  if (Sd.ContainsKey(Pt.SortOrder))
									  {
										  //pt.SortOrder = (ushort)sd.Count;
										  //_ = Azure.Client.RequestAddUpdatePackageType(pt);
										  ToAdd.Add(Pt);
									  }
									  else
									  {
										  Sd.Add(Pt.SortOrder, Pt);
										  PackageTypeObjects.Add(Pt);
									  }
								  }

								  if (ToAdd.Count > 0)
								  {
									  foreach (var Pt in ToAdd)
									  {
										  Logging.WriteLogLine("SortOrder already exists - changing it to " + Sd.Count);
										  Pt.SortOrder = (ushort)Sd.Count;
										  _ = Azure.Client.RequestAddUpdatePackageType(Pt);

										  if (!Sd.ContainsKey(Pt.SortOrder))
										  {
											  Sd.Add(Pt.SortOrder, Pt);
											  PackageTypeObjects.Add(Pt);
										  }
									  }
								  }
							  }
							  else
							  {
								  foreach (var Pt in PackageTypeObjects)
								  {
									  if ((Pt != null) && !Sd.ContainsKey(Pt.SortOrder))
										  Sd.Add(Pt.SortOrder, Pt);
								  }
							  }

							  // Check for holes
							  var Missing = new List<int>();

							  for (var I = 0; I < Sd.Count; I++)
							  {
								  if (!Sd.ContainsKey(I))
								  {
									  Logging.WriteLogLine("ERROR there isn't a package type with a sortorder of " + I);
									  Missing.Add(I);
								  }
							  }

							  if (Missing.Count > 0)
							  {
								  var Packs = new List<PackageType>();

								  foreach (var Tmp in Sd.Keys)
									  Packs.Add(Sd[Tmp]);

								  for (var I = 0; I < Packs.Count; I++)
								  {
									  var Pt = Packs[I];
									  Pt.SortOrder = (ushort)I;
									  _ = Azure.Client.RequestAddUpdatePackageType(Pt);
								  }
							  }

							  Dispatcher.Invoke(() =>
												 {
													 ServiceLevelPackageTypes.Clear();

													 foreach (var Key in Sd.Keys)
													 {
														 //packageTypes.Add(sd[Key].Description);
														 var Line = FormatPackageTypeDetailed(Sd[Key]);

														 //Logging.WriteLogLine("Adding " + line);
														 PackageTypes.Add(Line);

														 ServiceLevelPackageTypes.Add(Sd[Key].Description);
													 }
												 });
						  }
					  }
					  catch (Exception E)
					  {
						  //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
						  Logging.WriteLogLine("Exception thrown: " + E);
						  MessageBox.Show("Error fetching Package Types\n" + E, "Error", MessageBoxButton.OK);
					  }
				  });

		return PackageTypes;
	}

	public string SelectedPackageTypeName
	{
		get => Get(() => SelectedPackageTypeName, "");
		set => Set(() => SelectedPackageTypeName, value);
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="pt">
	/// </param>
	/// <returns></returns>
	private string FormatPackageTypeDetailed(PackageType pt)
	{
		var ByWeight = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDescriptionByWeight") + ": " + pt.WeightXRate;
		var Hourly = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDescriptionHourly") + ": " + pt.HoursXRate;
		var Formula = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDescriptionFormula") + ": " + pt.Formula;

		var Line = pt.Description.Trim() + " [" + ByWeight + "," + Hourly + "," + Formula + "]" + " (SortOrder " + pt.SortOrder + ")";

		return Line;
	}


	public PackageType CurrentlyEditedPackageType
	{
		get => Get(() => CurrentlyEditedPackageType, new PackageType());
		set => Set(() => CurrentlyEditedPackageType, value);
	}

	public bool IsEditingPackageType
	{
		get => Get(() => IsEditingPackageType, false);
		set => Set(() => IsEditingPackageType, value);
	}

	[DependsUpon(nameof(IsEditingPackageType))]
	public bool IsPackageTypeTextBoxReadOnly
	{
		get => Get(() => IsPackageTypeTextBoxReadOnly, !IsEditingPackageType);
		set => Set(() => IsPackageTypeTextBoxReadOnly, value);
	}

	public bool DoesPackageTypeNameExist(string name)
	{
		var Exists = false;

		if (PackageTypeObjects.Count > 0)
		{
			foreach (var Pt in PackageTypeObjects)
			{
				if (Pt.Description.Trim() == name)
				{
					Exists = true;

					break;
				}
			}
		}

		return Exists;
	}

	public string HelpUriPackageType
	{
		get { return Get(() => HelpUriPackageType, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/644186113/How+to+use+Rate+Wizard"); }
	}
	#endregion

	#region PackageTypes Commands
	/// <summary>
	/// </summary>
	/// <param
	///     name="pt">
	/// </param>
	/// <param
	///     name="newSortOrder">
	/// </param>
	public void MovePackageTypeLocally(PackageType pt, int newSortOrder)
	{
		Logging.WriteLogLine("Moving PackageType from " + pt.SortOrder + " to " + newSortOrder);

		if ((newSortOrder < PackageTypeObjects.Count) && (pt.SortOrder < PackageTypeObjects.Count))
		{
			PackageTypeObjects[newSortOrder].SortOrder = pt.SortOrder;
			PackageTypeObjects[pt.SortOrder].SortOrder = (ushort)newSortOrder;
		}
		else
			Logging.WriteLogLine("ERROR PackageTypeObject.count: " + PackageTypeObjects.Count + ", newSortOrder: " + newSortOrder + ", pt.SortOrder: " + pt.SortOrder);

		var Sd = new SortedDictionary<int, PackageType>();

		foreach (var Tmp in PackageTypeObjects)
		{
			//if( !sd.ContainsKey( tmp.SortOrder ) )
			if ((Tmp != null) && !Sd.ContainsKey(Tmp.SortOrder))
				Sd.Add(Tmp.SortOrder, Tmp);
			else
			{
				if (Tmp != null)
					Logging.WriteLogLine("ERROR sd already contains sortOrder: " + Tmp.SortOrder);
			}
		}

		var NewList = new List<PackageType>();

		foreach (var Key in Sd.Keys)
			NewList.Add(Sd[Key]);

		PackageTypeObjects = new List<PackageType>();

		//PackageTypeObjects.Clear();
		PackageTypeObjects.AddRange(NewList);

		var PtNames = new List<string>();

		foreach (var Tmp in PackageTypeObjects)
		{
			var Line = FormatPackageTypeDetailed(Tmp);
			PtNames.Add(Line);
		}

		var PackageTypes = new ObservableCollection<string>();

		Dispatcher.Invoke(() =>
						   {
							   //serviceLevels.AddRange(slNames);
							   foreach (var Name in PtNames)
								   PackageTypes.Add(Name);
						   });

		this.PackageTypes = PackageTypes;

		ArePendingPackageTypes = true;
		SetArePendingChanges(true);
	}

	public void DeletePackageTypeLocally(PackageType pt)
	{
		if (pt != null)
		{
			var PtName = pt.Description;
			Logging.WriteLogLine("Deleting PackageType: " + PtName);

			var NewList = new List<PackageType>();

			foreach (var Tmp in PackageTypeObjects)
			{
				if (Tmp.Description == PtName)
				{
					pt.Deleted = true;
					PackageTypeObjectsToDelete.Add(pt);
				}
				else
					NewList.Add(Tmp);
			}

			// At this point, there is a hole in the sortorder sequence
			var Sd = new SortedDictionary<int, PackageType>();

			for (var I = 0; I < NewList.Count; I++)
			{
				var Tmp = NewList[I];

				if (Tmp.SortOrder != I)
				{
					Tmp.SortOrder = (ushort)I;
					_ = Azure.Client.RequestAddUpdatePackageType(Tmp);
				}
				Sd.Add(I, Tmp);
			}

			PackageTypeObjects = new List<PackageType>();

			//PackageTypeObjects.Clear();
			PackageTypeObjects.AddRange(NewList);

			var PtNames = new List<string>();

			foreach (var Tmp in PackageTypeObjects)
			{
				var Line = FormatPackageTypeDetailed(Tmp);
				PtNames.Add(Line);
			}

			var PackageTypes = new ObservableCollection<string>();

			Dispatcher.Invoke(() =>
							   {
								   foreach (var Name in PtNames)
									   PackageTypes.Add(Name);
							   });

			this.PackageTypes = PackageTypes;

			ArePendingPackageTypes = true;
			SetArePendingChanges(true);
		}
	}

	public void SavePackageTypes()
	{
		Logging.WriteLogLine("Saving PackageTypes");

		Task.Run(async () =>
				  {
					  try
					  {
						  foreach (var Pt in PackageTypeObjects)
						  {
							  Logging.WriteLogLine("Saving PackageType: " + Pt.Description);
							  await Azure.Client.RequestAddUpdatePackageType(Pt);
						  }

						  foreach (var Pt in PackageTypeObjectsToDelete)
						  {
							  Logging.WriteLogLine("Deleting PackageType: " + Pt.Description);
							  await Azure.Client.RequestAddUpdatePackageType(Pt);
						  }

						  PackageTypeObjectsToDelete = new List<PackageType>();

						  var Pts = GetPackageTypes();

						  Dispatcher.Invoke(() =>
											 {
												 PackageTypes = Pts;
											 });

						  //UpdateShipmentEntryTabsWithNewPackageTypes();
						  ArePendingPackageTypes = false;

						  //if (ArePendingChanges == "Collapsed")
						  //{
						  //    SetArePendingChanges(false);
						  //} 
						  //if (ArePendingChangesBool)
						  //{
						  //    SetArePendingChanges(false);
						  //}
						  SetArePendingChanges(ArePendingChangesBool);
					  }
					  catch (Exception E)
					  {
						  Logging.WriteLogLine("Exception thrown: " + E);
						  MessageBox.Show("Error Saving Package Types:\n" + E, "Error", MessageBoxButton.OK);
					  }
				  });
	}

	private void UpdateLocalPackageType(PackageType pt)
	{
		var PtName = pt.Description;

		Logging.WriteLogLine("Updating " + PtName);

		var Exists = false;

		for (var I = 0; I < PackageTypeObjects.Count; I++)
		{
			var Level = PackageTypeObjects[I];

			if (Level.Description == PtName)
			{
				Exists = true;
				PackageTypeObjects[I] = pt;

				break;
			}
		}

		if (!Exists)
			PackageTypeObjects.Add(pt);

		var PtNames = new List<string>();

		foreach (var Tmp in PackageTypeObjects)
		{
			var Line = FormatPackageTypeDetailed(Tmp);
			PtNames.Add(Line);
		}

		var PackageTypes = new ObservableCollection<string>();

		Dispatcher.Invoke(() =>
						   {
							   //serviceLevels.AddRange(slNames);
							   foreach (var Name in PtNames)
								   PackageTypes.Add(Name);
						   });

		this.PackageTypes = PackageTypes;

		ArePendingPackageTypes = true;
		SetArePendingChanges(true);
	}

	public bool AddUpdatePackageType(PackageType pt)
	{
		var Success = true;
		Logging.WriteLogLine("Adding/Updating Package Type: " + pt.Description);

		UpdateLocalPackageType(pt);

		return Success;
	}

	public void Execute_ShowHelpPackageType()
	{
		Logging.WriteLogLine("Opening " + HelpUriPackageType);
		var Uri = new Uri(HelpUriPackageType);
		Process.Start(new ProcessStartInfo(Uri.AbsoluteUri));
	}
	#endregion

	#region Zones Data
	public class EditZone : Zone
	{
		public enum EDIT_ACTION
		{
			NONE,
			NEW,
			MODIFY,
			DELETE
		}

		public EDIT_ACTION EditAction { get; set; } = EDIT_ACTION.NONE;

		public EditZone()
		{
		}

		public EditZone(Zone zone)
		{
			Name = zone.Name;
			Abbreviation = zone.Abbreviation;
			SortIndex = zone.SortIndex;
			ZoneId = zone.ZoneId;
		}
	}

	public List<EditZone> Zones
	{
		get => Get(() => Zones, GetZones);
		set => Set(() => Zones, value);
	}

	public List<EditZone> OriginalZones
	{
		get => Get(() => OriginalZones, new List<EditZone>());
		set => Set(() => OriginalZones, value);
	}


	public bool IsEditingZone
	{
		get => Get(() => IsEditingZone, false);
		set => Set(() => IsEditingZone, value);
	}


	public bool IsNewZone
	{
		get => Get(() => IsNewZone, false);
		set => Set(() => IsNewZone, value);
	}


	public bool IsZoneTextBoxReadOnly
	{
		get => Get(() => IsZoneTextBoxReadOnly, !IsEditingZone);
		set => Set(() => IsZoneTextBoxReadOnly, value);
	}


	public EditZone CurrentlyEditedZone
	{
		get => Get(() => CurrentlyEditedZone, new EditZone());
		set => Set(() => CurrentlyEditedZone, value);
	}


	public string SelectedZoneName
	{
		get => Get(() => SelectedZoneName, "");
		set => Set(() => SelectedZoneName, value);
	}


	public string SelectedZoneAbbreviation
	{
		get => Get(() => SelectedZoneAbbreviation, "");
		set => Set(() => SelectedZoneAbbreviation, value);
	}


	public int SelectedZoneSortIndex
	{
		get => Get(() => SelectedZoneSortIndex, 0);
		set => Set(() => SelectedZoneSortIndex, value);
	}


	public string SelectedSortOrder
	{
		get => Get(() => SelectedSortOrder, "");
		set => Set(() => SelectedSortOrder, value);
	}

	public string HelpUriZones
	{
		get { return Get(() => HelpUriZones, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/644186113/How+to+use+Rate+Wizard"); }
	}

	public bool LoadPriceMatrix { get; set; } = true;
	#endregion

	#region Zones Commands
	public List<EditZone> GetZones()
	{
		Logging.WriteLogLine("Loading Zones");
		var EditZones = new List<EditZone>();

		if (!IsInDesignMode)
		{
			//Task.WaitAll(
			Task.Run(async () =>
					  {
						  DeletedZones.Clear();

						  Zones = (from Z in await Azure.Client.RequestZones()
								   select new EditZone(Z)).ToList();

						  Logging.WriteLogLine("Found " + Zones.Count + " Zones");

						  if ((Zones.Count == 0) && (OriginalZones.Count > 0))
						  {
							  // Restore zones from before save.  Sometimes the server seems to forget them.
							  Logging.WriteLogLine("Zones from server is empty - restoring " + OriginalZones.Count + " zones from before save");

							  var UpdateZones = new Zones
							  {
								  ProgramName = RATE_WIZARD
							  };

							  foreach (var OriginalZone in OriginalZones)
							  {
								  UpdateZones.Add(new Zone
								  {
									  Name = OriginalZone.Name,
									  Abbreviation = OriginalZone.Abbreviation,
									  SortIndex = OriginalZone.SortIndex,
									  ZoneId = OriginalZone.ZoneId
								  });
							  }

							  await Azure.Client.RequestAddUpdateZones(UpdateZones);
							  Zones.AddRange(OriginalZones);
						  }

						  if (Zones.IsNotNull() && (Zones.Count > 0))
						  {
							  // Blank zones sometimes appear
							  Zones = (from Z in Zones
									   where Z.Name.IsNotNullOrWhiteSpace()
									   orderby Z.SortIndex
									   select Z).ToList();
							  SortedDictionary<string, Zone> Sd = new();
							  Zones Tmp = new();

							  foreach (var Zone in Zones)
							  {
								  if (!Sd.ContainsKey(Zone.Name))
								  {
									  Sd.Add(Zone.Name, Zone);
									  EditZones.Add(Zone);
									  Tmp.Add(Zone);
								  }
							  }

							  // Save to get rid of duplicates
							  if (Zones.Count > EditZones.Count)
							  {
								  Logging.WriteLogLine("DEBUG Zones.Count " + Zones.Count + " > zones.Count " + EditZones.Count);
								  await Azure.Client.RequestAddUpdateZones(Tmp);
							  }

							  Zones = EditZones;
						  }

						  if (LoadPriceMatrix)
						  {
							  Dispatcher.Invoke(() =>
												 {
													 if (View is not null)
													 {
														 View.BuildPriceMatrix(View.GridPriceMatrix, true);
														 View.BuildPriceMatrix(View.GridLanePricingMatrix, false);

														 //view.BtnAddLane_Click(null, null);
														 View.BuildGridLanePricing();
														 View.BuildPriceMatrix(View.GridLaneTestMatrix, true, false);

														 View.PopulateSurchargeZonesList();
													 }
													 Loaded = true;
												 });
						  }
					  });
		}

		return EditZones;
	}

	public void Execute_SaveZones()
	{
		Logging.WriteLogLine("Saving Zones");

		Task.Run(async () =>
				  {
					  SetZoneSortOrders();
					  OriginalZones.Clear();

					  ZoneNameList ToDelete = new()
					  {
						  ProgramName = RATE_WIZARD
					  };

					  Zones ToUpdate = new()
					  {
						  ProgramName = RATE_WIZARD
					  };

					  HashSet<string> HashSet = new();

					  foreach (var DeletedZone in DeletedZones)
						  ToDelete.Add(DeletedZone.Name);

					  foreach (var Zone in Zones)
					  {
						  if (Zone.Name.IsNotNullOrWhiteSpace() && !HashSet.Contains(Zone.Name))
						  {
							  HashSet.Add(Zone.Name);
							  ToUpdate.Add(Zone);

							  OriginalZones.Add(Zone);
						  }
					  }

					  if (ToDelete.Count > 0)
						  await Azure.Client.RequestDeleteZones(ToDelete);

					  if (ToUpdate.Count > 0)
						  await Azure.Client.RequestAddUpdateZones(ToUpdate);

					  ArePendingZones = false;

					  //if (ArePendingChangesBool)
					  //{
					  // SetArePendingChanges(false);
					  //}
					  SetArePendingChanges(ArePendingChangesBool);

					  LoadPriceMatrix = false;
					  GetZones(); // Don't rebuild the price matrix

					  //Logging.WriteLogLine("Found " + Zones.Count + " Zones");

					  Dispatcher.Invoke(() =>
										 {
											 var Title = FindStringResource("RateWizardTabZoneSavedTitle");
											 var Message = FindStringResource("RateWizardTabZoneSaved");
											 MessageBox.Show(Message, Title, MessageBoxButton.OK, MessageBoxImage.Information);
										 });
				  });
	}

	private void SetZoneSortOrders()
	{
		var Ndx = 0;

		foreach (var Zone in Zones)
		{
			if (Zone.EditAction != EditZone.EDIT_ACTION.DELETE)
				Zone.SortIndex = Ndx++;
		}
	}

	public void Execute_ShowHelpZones()
	{
		Logging.WriteLogLine("Opening " + HelpUriServiceLevel);
		var Uri = new Uri(HelpUriServiceLevel);
		Process.Start(new ProcessStartInfo(Uri.AbsoluteUri));
	}

	public void MoveZoneLocally(Zone zone, int newSortOrder)
	{
		Zones[newSortOrder].SortIndex = zone.SortIndex;
		Zones[zone.SortIndex].SortIndex = (ushort)newSortOrder;

		Dispatcher.Invoke(() =>
						   {
							   var EditZones = Zones.OrderBy(x => x.SortIndex).ToList();
							   Zones = EditZones;
							   var Tmp = string.Empty;

							   foreach (var Z in Zones)
								   Tmp += Z.Name + " (SortIndex: " + Z.SortIndex + "), ";
							   Logging.WriteLogLine("Sorted zones: " + Tmp);

							   ArePendingZones = true;
							   SetArePendingChanges(true);
						   });
	}

	/// <summary>
	///     Make a list of the zones.
	///     If !new zone, remove the old one from the list.
	///     Then check that the new name doesn't exist.
	/// </summary>
	/// <returns></returns>
	public string AddUpdateZoneLocally()
	{
		var Error = string.Empty;

		var CheckAgainst = new List<EditZone>();
		CheckAgainst.AddRange(Zones);

		if (!IsNewZone)
		{
			// Editing - remove the old zone
			CheckAgainst.Remove(CurrentlyEditedZone);
		}

		foreach (var Z in CheckAgainst)
		{
			if (Z.Name == SelectedZoneName.Trim())
			{
				Error = (string)Application.Current.TryFindResource("RateWizardTabZoneErrorDuplicateName");
				break;
			}

			if (Z.Abbreviation == SelectedZoneAbbreviation)
			{
				Error = (string)Application.Current.TryFindResource("RateWizardTabZoneErrorDuplicateAbbreviation");
				break;
			}
		}

		if (Error.IsNullOrWhiteSpace())
		{
			// If new, create a zone, otherwise update the Current
			var Zone = new EditZone
			{
				EditAction = EditZone.EDIT_ACTION.NEW,
				Name = SelectedZoneName.Trim(),
				Abbreviation = SelectedZoneAbbreviation.Trim(),
				SortIndex = SelectedZoneSortIndex
				//ZoneId       = CurrentlyEditedZone.ZoneId
			};

			if (CurrentlyEditedZone != null)
				Zone.ZoneId = CurrentlyEditedZone.ZoneId;
			else
				Zone.SortIndex = Zones.Count; // Put at bottom

			CheckAgainst.Add(Zone);
			CheckAgainst = CheckAgainst.OrderBy(x => x.SortIndex).ToList();

			Dispatcher.Invoke(() =>
							   {
								   Zones = CheckAgainst;
							   });
			ArePendingZones = true;
			SetArePendingChanges(true);
		}

		return Error;
	}

	private readonly List<EditZone> DeletedZones = new();


	/// <summary>
	///     Note - this reorders the SortIndex, removing the hole.
	/// </summary>
	/// <param
	///     name="zone">
	/// </param>
	public void DeleteZoneLocally(EditZone zone)
	{
		Logging.WriteLogLine("Deleting from local list: " + zone.Name + " (" + zone.Abbreviation + "), SortIndex: " + zone.SortIndex);

		var Copy = new List<EditZone>();
		var SortIndex = 0;

		zone.EditAction = EditZone.EDIT_ACTION.DELETE;
		DeletedZones.Add(zone);

		foreach (var Z in Zones)
		{
			if (Z.Name != zone.Name)
			{
				Z.SortIndex = SortIndex++;
				Copy.Add(Z);
			}
		}

		Dispatcher.Invoke(() =>
						   {
							   Zones = Copy;
						   });

		ArePendingZones = true;
		SetArePendingChanges(true);
	}
	#endregion

	#region Price Matrix Data
	public bool IsPriceMatrixLocked
	{
		get => Get(() => IsPriceMatrixLocked, true);
		set => Set(() => IsPriceMatrixLocked, value);
	}

	[DependsUpon(nameof(IsPriceMatrixLocked))]
	public void WhenIsPriceMatrixLockedChanges()
	{
		Logging.WriteLogLine("DEBUG IsPriceMatrixLocked: " + IsPriceMatrixLocked);

		PriceMatrixLockUnlockLabelContent = IsPriceMatrixLocked ? FindStringResource("RateWizardTabPriceMatrixLabelLocked")
												: FindStringResource("RateWizardTabPriceMatrixLabelUnlocked");

		PriceMatrixLockButtonContent = IsPriceMatrixLocked ? FindStringResource("RateWizardTabPriceMatrixButtonUnlock")
										   : FindStringResource("RateWizardTabPriceMatrixButtonLock");

		PriceMatrixLockButtonBackground = IsPriceMatrixLocked ? Brushes.Red : Brushes.Green;

		if (IsPriceMatrixLocked)
		{
			ModifiedRateNames.Clear();
			ArePendingRateMatricies = false;
		}
	}

	public string PriceMatrixLockUnlockLabelContent
	{
		get => Get(() => PriceMatrixLockUnlockLabelContent, FindStringResource("RateWizardTabPriceMatrixLabelLocked"));
		set => Set(() => PriceMatrixLockUnlockLabelContent, value);
	}

	public string PriceMatrixLockButtonContent
	{
		get => Get(() => PriceMatrixLockButtonContent, FindStringResource("RateWizardTabPriceMatrixButtonUnlock"));
		set => Set(() => PriceMatrixLockButtonContent, value);
	}

	public Brush PriceMatrixLockButtonBackground
	{
		get => Get(() => PriceMatrixLockButtonBackground, Brushes.Red);
		set => Set(() => PriceMatrixLockButtonBackground, value);
	}

	public ObservableCollection<string> ComboActionLines
	{
		get => Get(() => ComboActionLines, GetActionLines);
		set => Set(() => ComboActionLines, value);
	}

	public ObservableCollection<string> GetActionLines()
	{
		ObservableCollection<string> Oc = new()
										  {
											  FindStringResource( "RateWizardTabPriceMatrixComboActionInitializeValues" ),
											  FindStringResource( "RateWizardTabPriceMatrixComboActionAddToValues" ),
											  FindStringResource( "RateWizardTabPriceMatrixComboActionMultiplyValuesBy" ),
											  " ",
											  FindStringResource( "RateWizardTabPriceMatrixComboActionInitializeTo" )
										  };

		return Oc;
	}

	public ObservableCollection<string> PackageTypeNamesForMatrix = new();

	public string PreviousPackageTypeForMatrix { get; set; } = string.Empty;

	public string SelectedPackageTypeForMatrix
	{
		get => Get(() => SelectedPackageTypeForMatrix, "");
		set => Set(() => SelectedPackageTypeForMatrix, value);
	}

	public ObservableCollection<string> ServiceLevelNamesForMatrix = new();

	public string PreviousServiceLevelForMatrix { get; set; } = string.Empty;

	public string SelectedServiceLevelForMatrix
	{
		get => Get(() => SelectedServiceLevelForMatrix, "");
		set => Set(() => SelectedServiceLevelForMatrix, value);
	}

	[DependsUpon(nameof(SelectedPackageTypeForMatrix))]
	[DependsUpon(nameof(SelectedServiceLevelForMatrix))]
	public void WhenSelectedPackageOrServiceLevelForMatrixChanges()
	{
		//if (SdRates.Count == 0)
		//         {
		//	BuildSdRates();
		//         }
		// Check to see if any values have been modified
		if ((SdRates.Count > 0) && SelectedServiceLevelForMatrix.IsNotNullOrWhiteSpace() && SelectedPackageTypeForMatrix.IsNotNullOrWhiteSpace())
		{
			//if (SelectedServiceLevelForMatrix.IsNullOrWhiteSpace())
			//            {
			//	SelectedServiceLevelForMatrix = ServiceLevelNames[0];
			//            }
			//if (SelectedPackageTypeForMatrix.IsNullOrWhiteSpace())
			//            {
			//	SelectedPackageTypeForMatrix = PackageTypeNames[0];
			//            }				

			Dispatcher.Invoke(() =>
							   {
								   if (!IsPriceMatrixLocked)
									   CheckMatrixForModification();
								   LoadSelectedRates();
								   LoadActivePackageServicePairs();
							   });
		}
	}


	public string SelectedActionForMatrix
	{
		get => Get(() => SelectedActionForMatrix, ComboActionLines[0]);
		set => Set(() => SelectedActionForMatrix, value);
	}


	public decimal ActionValue
	{
		get => Get(() => ActionValue, 0);
		set => Set(() => ActionValue, value);
	}


	public List<RateMatrixItem> RateMatrixItems
	{
		//get { return Get(() => RateMatrixItems, GetRateMatrixItems()); }
		get { return Get(() => RateMatrixItems, new List<RateMatrixItem>()); }
		set { Set(() => RateMatrixItems, value); }
	}


	// Key:		Next Day -> Weight Pieces
	// Value:	Dictionary: Key: <row label>-><col label>, Value: <value in cell> eg. "march 9 2021+north vancouver->TAS+TAS", 0.00
	//private readonly SortedDictionary<string, List<Dictionary<string, decimal>>> SdRates = new();
	// NOTE For lane pricing, the lane# goes into the value and the assembled formula into the formula
	public readonly SortedDictionary<string, List<LocalRateMatrixItem?>> SdRates = new();

	// Holds the rates that are currently loaded - used to detect changes
	public readonly List<LocalRateMatrixItem> CurrentlyLoadedRates = new();

	public readonly List<string> ModifiedRateNames = new();

	//public readonly SortedDictionary<string, List<LocalRateMatrixItem>> SdModifiedRates = new();

	public ObservableCollection<string> ActivePackageServicePairs { get; } = new();


	/// <summary>
	///     Note - has to exclude current pair.
	/// </summary>
	public void LoadActivePackageServicePairs()
	{
		Dispatcher.Invoke(() =>
						   {
							   var CurrentPair = FormatRateName();
							   ActivePackageServicePairs.Clear();

							   //foreach (string key in SdModifiedRates.Keys)
							   foreach (var Key in SdRates.Keys)
							   {
								   if (Key != CurrentPair)
									   ActivePackageServicePairs.Add(Key);
							   }
						   });
	}

	public string HelpUriRateMatrix => Get(() => HelpUriRateMatrix, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1945665537/Rate+Wizard");


	public bool IsFixedPriceSelected
	{
		get => Get(() => IsFixedPriceSelected, true);
		set => Set(() => IsFixedPriceSelected, value);
	}

	[DependsUpon(nameof(IsFixedPriceSelected))]
	public void WhenIsFixedPriceSelectedChanges()
	{
		Logging.WriteLogLine("DEBUG IsFixedPriceSelected: " + IsFixedPriceSelected);

		//Visibility visibility = Visibility.Visible;
		if (!IsFixedPriceSelected)
		{
			// visibility = Visibility.Hidden;
			ActionAndUpdateVisiblility = Visibility.Hidden;
			InitializeLaneVisibility = Visibility.Visible;
		}
		else
		{
			ActionAndUpdateVisiblility = Visibility.Visible;
			InitializeLaneVisibility = Visibility.Hidden;
		}
	}


	public Visibility ActionAndUpdateVisiblility
	{
		get => Get(() => ActionAndUpdateVisiblility, Visibility.Visible);
		set => Set(() => ActionAndUpdateVisiblility, value);
	}


	public Visibility InitializeLaneVisibility
	{
		get { return Get(() => InitializeLaneVisibility, Visibility.Collapsed); }
		set { Set(() => InitializeLaneVisibility, value); }
	}


	public bool IsLanePricingSelected
	{
		get => Get(() => IsLanePricingSelected, false);
		set => Set(() => IsLanePricingSelected, value);
	}

	public bool IsThisLanePricingPairCumulativeChecked
	{
		get => Get(() => IsThisLanePricingPairCumulativeChecked, false);
		set => Set(() => IsThisLanePricingPairCumulativeChecked, value);
	}

	[DependsUpon(nameof(IsThisLanePricingPairCumulativeChecked))]
	public void WhenIsThisLanePricingPairCumulativeCheckedChanges()
	{
		Logging.WriteLogLine("DEBUG IsThisLanePricingPairCumulativeChecked: " + IsThisLanePricingPairCumulativeChecked);

		BuildFullFormulae();

		foreach (var Tmp in DictLanes.Keys)
		{
			var Lrmis = DictLanes[Tmp];

			foreach (var Item in Lrmis)
			{
				Logging.WriteLogLine("Found formula:\n" + Item.Formula);

				if (Item.Formula.IsNotNullOrWhiteSpace())
				{
					var Formula = Item.Formula;
					//if (!formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_BASE))
					//{
					//	if (IsThisLanePricingPairCumulativeChecked)
					//	{

					//	}
					//}

					if (Formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE) && IsThisLanePricingPairCumulativeChecked)
					{
						SetArePendingRateMatricies(true);
						Formula.Replace(EvaluateRateFormulaHelper.CUMULATIVE_FALSE, EvaluateRateFormulaHelper.CUMULATIVE_TRUE);
						BuildFullFormulae();
					}
					else if (Formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE) && !IsThisLanePricingPairCumulativeChecked)
					{
						SetArePendingRateMatricies(true);
						Formula.Replace(EvaluateRateFormulaHelper.CUMULATIVE_TRUE, EvaluateRateFormulaHelper.CUMULATIVE_FALSE);
						BuildFullFormulae();
					}
				}
			}
		}

		//if (DictFullFormulas.Count > 0)
		//{
		//	foreach (string key in DictFullFormulas.Keys)
		//	{
		//		if (DictFullFormulas[key] != null)
		//		{
		//			if (DictFullFormulas[key].Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE) && IsThisLanePricingPairCumulativeChecked)
		//			{
		//				SetArePendingRateMatricies(true);

		//				foreach (var tmp in DictLanes.Keys)
		//				{
		//					var lrmis = DictLanes[tmp];

		//					foreach (var item in lrmis)
		//					{
		//						Logging.WriteLogLine("Found formula:\n" + item.Formula);
		//						if (item.Formula.IsNotNullOrWhiteSpace())
		//						{
		//							string formula = item.Formula;
		//							if (formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE))
		//							{
		//								formula.Replace(EvaluateRateFormulaHelper.CUMULATIVE_FALSE, EvaluateRateFormulaHelper.CUMULATIVE_TRUE);
		//								BuildFullFormulae();
		//							}
		//						}
		//					}
		//				}
		//			}
		//			else if (DictFullFormulas[key].Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE) && !IsThisLanePricingPairCumulativeChecked)
		//			{
		//				SetArePendingRateMatricies(true);

		//				foreach (var tmp in DictLanes.Keys)
		//				{
		//					var lrmis = DictLanes[tmp];

		//					foreach (var item in lrmis)
		//					{
		//						Logging.WriteLogLine("Found formula:\n" + item.Formula);
		//						if (item.Formula.IsNotNullOrWhiteSpace())
		//						{
		//							string formula = item.Formula;
		//							if (formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE))
		//							{
		//								formula.Replace(EvaluateRateFormulaHelper.CUMULATIVE_TRUE, EvaluateRateFormulaHelper.CUMULATIVE_FALSE);
		//								BuildFullFormulae();
		//							}
		//						}
		//					}
		//				}
		//			}
		//		}
		//	}
		//}
	}

	public void WhenIsThisLanePricingPairCumulativeCheckedChanges_V1()
	{
		Logging.WriteLogLine("DEBUG IsThisLanePricingPairCumulativeChecked: " + IsThisLanePricingPairCumulativeChecked);

		BuildFullFormulae();

		if (DictFullFormulas.Count > 0)
		{
			foreach (var Key in DictFullFormulas.Keys)
			{
				if (DictFullFormulas[Key] != null)
				{
					if (DictFullFormulas[Key].Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE) && IsThisLanePricingPairCumulativeChecked)
					{
						SetArePendingRateMatricies(true);

						foreach (var Tmp in DictLanes.Keys)
						{
							var Lrmis = DictLanes[Tmp];

							foreach (var Item in Lrmis)
							{
								Logging.WriteLogLine("Found formula:\n" + Item.Formula);

								if (Item.Formula.IsNotNullOrWhiteSpace())
								{
									var Formula = Item.Formula;

									if (Formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE))
									{
										Formula.Replace(EvaluateRateFormulaHelper.CUMULATIVE_FALSE, EvaluateRateFormulaHelper.CUMULATIVE_TRUE);
										BuildFullFormulae();
									}
								}
							}
						}
					}
					else if (DictFullFormulas[Key].Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE) && !IsThisLanePricingPairCumulativeChecked)
					{
						SetArePendingRateMatricies(true);

						foreach (var Tmp in DictLanes.Keys)
						{
							var Lrmis = DictLanes[Tmp];

							foreach (var Item in Lrmis)
							{
								Logging.WriteLogLine("Found formula:\n" + Item.Formula);

								if (Item.Formula.IsNotNullOrWhiteSpace())
								{
									var Formula = Item.Formula;

									if (Formula.Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE))
									{
										Formula.Replace(EvaluateRateFormulaHelper.CUMULATIVE_TRUE, EvaluateRateFormulaHelper.CUMULATIVE_FALSE);
										BuildFullFormulae();
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public ObservableCollection<string> PricingObjects { get; } = new()
																  {
																	  FindStringResource( "RateWizardTabPriceMatrixComboObjectPieces" ),
																	  FindStringResource( "RateWizardTabPriceMatrixComboObjectWeight" ),
																	  FindStringResource( "RateWizardTabPriceMatrixComboObjectMileage" )
																  };

	public ObservableCollection<string> Conditions { get; } = new()
															  {
																  ">",
																  ">=",
																  "=",
																  "<=",
																  "<"
															  };

	public ObservableCollection<string> LogicalOps { get; } = new()
															  {
																  "     ",
																  " And ",
																  " Or "
															  };

	public ObservableCollection<string> Lanes { get; } = new()
														 {
															 " "
														 };

	/// <summary>
	///     Ordered by lane number, then the LocalRateMatrixItems ordered from left to right
	/// </summary>
	public Dictionary<string, List<LocalRateMatrixItem>> DictLanes { get; } = new();

	public Dictionary<string, string> DictFullFormulas { get; } = new();


	public decimal TestPieces
	{
		get => Get(() => TestPieces, 0);
		set => Set(() => TestPieces, value);
	}


	public decimal TestWeight
	{
		get => Get(() => TestWeight, 0);
		set => Set(() => TestWeight, value);
	}

	public decimal TestMileage
	{
		get => Get(() => TestMileage, 0);
		set => Set(() => TestMileage, value);
	}

	public List<Brush> BackgroundBrushes = new()
										   {
											   Brushes.LightGray,
											   Brushes.LightGreen,
											   Brushes.LightBlue,
											   Brushes.LightPink,
											   Brushes.LightYellow,
											   Brushes.LightSalmon
										   };
	#endregion

	#region Price Matrix Commands
	public List<RateMatrixItem> GetRateMatrixItems()
	{
		List<RateMatrixItem> Rmis = new();

		if (!IsInDesignMode)
		{
			SdRates.Clear();

			Task.Run(async () =>
					  {
						  var Rmis = await Azure.Client.RequestGetAllRateMatrixItems();

						  if (Rmis != null)
						  {
							  Logging.WriteLogLine("Got " + Rmis.Count + " RateMatrixItems");

							  //if (PackageTypeObjects.Count == 0)
							  //                  {
							  //	Task.WaitAll(Task.Run(() =>
							  //	{
							  //		GetPackageTypes();

							  //	}));
							  //	Task.WaitAll(Task.Run(() =>
							  //	{
							  //		GetServiceLevels();

							  //	}));
							  //}

							  RateMatrixItems.Clear();
							  RateMatrixItems.AddRange(Rmis);

							  BuildSdRates();

							  //Loaded = true;
						  }
					  });
		}

		return Rmis;
	}

	private async void BuildSdRates()
	{
		//Logging.WriteLogLine("DEBUG PackageTypeObjects.Count: " + PackageTypeObjects.Count);
		if (PackageTypeObjects.Count == 0)
		{
			Logging.WriteLogLine("PackageTypeObjects is empty - fetching");
			var Ptl = Azure.Client.RequestGetPackageTypes().Result;

			if (Ptl != null)
			{
				foreach (var Tmp in Ptl)
					PackageTypeObjects.Add(Tmp);
			}
		}

		//Logging.WriteLogLine("DEBUG ServiceLevelObjects.Count: " + ServiceLevelObjects.Count);
		if (ServiceLevelObjects.Count == 0)
		{
			Logging.WriteLogLine("ServiceLevelObjects is empty - fetching");
			var Slld = Azure.Client.RequestGetServiceLevelsDetailed().Result;

			if (Slld != null)
			{
				foreach (var Tmp in Slld)
					ServiceLevelObjects.Add(Tmp);
			}
		}
		List<string> ZoneNames = new();
		List<string> Reversed = new();

		if (Zones.Count == 0)
		{
			Logging.WriteLogLine("Zones is empty - fetching");

			var EditZones = (from Z in await Azure.Client.RequestZones()
							 select new EditZone(Z)).ToList();

			Logging.WriteLogLine("Found " + EditZones.Count + " Zones");

			if (EditZones.IsNotNull() && (EditZones.Count > 0))
			{
				lock (Zones)
				{
					EditZones = EditZones.OrderBy(x => x.SortIndex).ToList();
					Zones = EditZones;

					ZoneNames = (from Z in EditZones
								 select Z.Name).ToList();

					Reversed = (from Z in EditZones
								select Z.Name).ToList();
					Reversed.Reverse();
				}
			}
		}
		else
		{
			ZoneNames = (from Z in Zones
						 select Z.Name).ToList();

			Reversed = (from Z in Zones
						select Z.Name).ToList();
			Reversed.Reverse();
		}

		Logging.WriteLogLine("DEBUG zoneNames.Count: " + ZoneNames.Count);

		if (ZoneNames?.Count > 0)
		{
			foreach (var Raw in RateMatrixItems)
			{
				var Lrmi = ConvertRateMatrixItemToLocal(Raw, ZoneNames, Reversed);

				if (Lrmi is not null)
				{
					var RateName = Lrmi.PackageType + " -> " + Lrmi.ServiceLevel;

					if (!SdRates.ContainsKey(RateName))
					{
						//Logging.WriteLogLine("DEBUG Building " + rateName);
						SdRates.Add(RateName, new List<LocalRateMatrixItem?>());
					}

					//var formula = string.Empty;
					//if( lrmi.Formula.IsNotNullOrWhiteSpace() )
					//	lrmi.Formula.Substring( 0, 10 );
					//Logging.WriteLogLine("DEBUG name: " + lrmi.Name + " value: " + lrmi.Value + " formula: " + formula + " FromZone " + lrmi.FromZone + " ToZone " + lrmi.ToZone);
					SdRates[RateName].Add(Lrmi);
				}
			}
		}
		else
			Logging.WriteLogLine("ERROR zoneNames is empty");

		//WhenSelectedPackageOrServiceLevelForMatrixChanges();
		LoadSelectedRates();
		LoadActivePackageServicePairs();

		//Dispatcher.Invoke(() =>
		//{
		//             //WhenSelectedPackageOrServiceLevelForMatrixChanges();
		//             LoadSelectedRates();
		//             LoadActivePackageServicePairs();
		//});

		//Loaded = true;
		//Logging.WriteLogLine("DEBUG Loaded: " + Loaded);
	}

	public string MakeRateMatrixCellName(int x, int y) => "xy_" + x + "_" + y;

	private LocalRateMatrixItem? ConvertRateMatrixItemToLocal(RateMatrixItem rmi, List<string> zoneNames, List<string> reversed)
	{
		LocalRateMatrixItem? Lrmi = null;
		////Logging.WriteLogLine("DEBUG PackageTypeObjects.Count: " + PackageTypeObjects.Count);
		//if (PackageTypeObjects.Count == 0)
		//{
		//    Logging.WriteLogLine("PackageTypeObjects is empty - fetching");
		//    PackageTypeList ptl = Azure.Client.RequestGetPackageTypes().Result;
		//    if (ptl != null)
		//    {
		//        foreach (PackageType tmp in ptl)
		//        {
		//            PackageTypeObjects.Add(tmp);
		//        }
		//    }
		//}
		////Logging.WriteLogLine("DEBUG ServiceLevelObjects.Count: " + ServiceLevelObjects.Count);
		//if (ServiceLevelObjects.Count == 0)
		//{
		//    Logging.WriteLogLine("ServiceLevelObjects is empty - fetching");
		//    ServiceLevelListDetailed slld = Azure.Client.RequestGetServiceLevelsDetailed().Result;
		//    if (slld != null)
		//    {
		//        foreach (ServiceLevel tmp in slld)
		//        {
		//            ServiceLevelObjects.Add(tmp);
		//        }
		//    }
		//}

		var Pt = (from P in PackageTypeObjects
				  where P.PackageTypeId == rmi.PackageTypeId
				  select P).FirstOrDefault();

		if (Pt != null)
		{
			var Sl = (from S in ServiceLevelObjects
					  where S.ServiceLevelId == rmi.ServiceLevelId
					  select S).FirstOrDefault();

			if (Sl != null)
			{
				var Fz = (from Z in Zones
						  where Z.ZoneId == rmi.FromZoneId
						  select Z).FirstOrDefault();

				if (Fz != null)
				{
					var Tz = (from Z in Zones
							  where Z.ZoneId == rmi.ToZoneId
							  select Z).FirstOrDefault();

					if (Tz != null)
					{
						Lrmi = new LocalRateMatrixItem
						{
							//Name = pt.Description + " -> " + sl.OldName,
							//Name = "xy_" + (zoneNames.IndexOf(fz.Name) + 1) + "_" + (reversed.IndexOf(tz.Name) + 1),
							//Name = "xy_" + (zoneNames.IndexOf(tz.Name) + 1) + "_" + (reversed.IndexOf(fz.Name) + 1),
							// Name = "xy_" + ( reversed.IndexOf( tz.Name ) + 1 ) + "_" + ( zoneNames.IndexOf( fz.Name ) + 1 ),  // This was the previous version
							Name = MakeRateMatrixCellName(reversed.IndexOf(Tz.Name) + 1, zoneNames.IndexOf(Fz.Name) + 1),
							//Name = MakeRateMatrixCellName(reversed.IndexOf( fz.Name ) + 1, zoneNames.IndexOf( tz.Name ) + 1),
							//Name = "xy_" + (reversed.IndexOf(fz.Name) + 1) + "_" + (zoneNames.IndexOf(tz.Name) + 1),
							Formula = rmi.Formula,
							Value = rmi.Value,
							PackageType = Pt.Description,
							ServiceLevel = Sl.OldName,
							FromZone = Fz.Name,
							ToZone = Tz.Name
						};
						//if (rmi.Value == 2)
						//{
						//	Logging.WriteLogLine("DEBUG Found Value==2 - Name: " + lrmi.Name);
						//}
						//Logging.WriteLogLine("DEBUG lrmi.Name: " + lrmi.Name + " rmi.ToZoneId: " + rmi.ToZoneId + " Zone.Name: " + tz.Name + " rmi.FromZoneId: " + rmi.FromZoneId + " Zone.Name: " + fz.Name);
						//if (lrmi.Name.StartsWith("xy_3_") || lrmi.Name.StartsWith("xy_5_"))
						//{
						//    Logging.WriteLogLine("\tDEBUG found xy_3_ or xy_5_ name: " + lrmi.Name + " value: " + lrmi.Value);
						//}
					}
					else
						Logging.WriteLogLine("DEBUG tz is null for " + rmi.ToZoneId);
				}
				else
				{
					Logging.WriteLogLine("DEBUG fz is null for " + rmi.FromZoneId + " Formula object: " + rmi.Formula);

					Lrmi = new LocalRateMatrixItem
					{
						//Name = pt.Description + " -> " + sl.OldName,
						//Name = "xy_" + (zoneNames.IndexOf(fz.Name) + 1) + "_" + (reversed.IndexOf(tz.Name) + 1),
						//Name = "xy_" + (zoneNames.IndexOf(tz.Name) + 1) + "_" + (reversed.IndexOf(fz.Name) + 1),
						// Name = "xy_" + ( reversed.IndexOf( tz.Name ) + 1 ) + "_" + ( zoneNames.IndexOf( fz.Name ) + 1 ),  // This was the previous version
						Name = string.Empty,
						//Name = MakeRateMatrixCellName(reversed.IndexOf(tz.Name) + 1, zoneNames.IndexOf(fz.Name) + 1),
						//Name = MakeRateMatrixCellName(reversed.IndexOf( fz.Name ) + 1, zoneNames.IndexOf( tz.Name ) + 1),
						//Name = "xy_" + (reversed.IndexOf(fz.Name) + 1) + "_" + (zoneNames.IndexOf(tz.Name) + 1),
						Formula = rmi.Formula,
						Value = rmi.Value,
						PackageType = Pt.Description,
						ServiceLevel = Sl.OldName,
						FromZone = string.Empty,
						ToZone = string.Empty
					};
				}
			}
			else
				Logging.WriteLogLine("DEBUG sl is null for " + rmi.ServiceLevelId);
		}
		else
			Logging.WriteLogLine("DEBUG pt is null for " + rmi.PackageTypeId);

		return Lrmi;
	}

	private RateMatrixItem? ConvertFromLocalRateMatrixItem(LocalRateMatrixItem lrmi)
	{
		RateMatrixItem? Rmi = null;

		var Pt = (from P in PackageTypeObjects
				  where P.Description == lrmi.PackageType
				  select P).FirstOrDefault();

		if (Pt != null)
		{
			var Sl = (from S in ServiceLevelObjects
					  where S.OldName == lrmi.ServiceLevel
					  select S).FirstOrDefault();

			if (Sl != null)
			{
				// Now for the zones
				//         y zone                      -> x zone
				// Name = "march 9 2021+north vancouver->TAS+TAS"
				var Fz = (from Z in Zones
						  where Z.Name == lrmi.FromZone
						  select Z).FirstOrDefault();

				if (Fz != null)
				{
					var Tz = (from Z in Zones
							  where Z.Name == lrmi.ToZone
							  select Z).FirstOrDefault();

					if (Tz != null)
					{
						Rmi = new RateMatrixItem
						{
							Formula = lrmi.Formula,
							Value = lrmi.Value,
							PackageTypeId = Pt.PackageTypeId,
							ServiceLevelId = Sl.ServiceLevelId,
							FromZoneId = Fz.ZoneId,
							ToZoneId = Tz.ZoneId,
							VehicleTypeId = 0 // TODO lrmi.VehicleType,
						};
					}
				}
			}
		}

		return Rmi;
	}

	public void LoadSelectedRates()
	{
		Dispatcher.Invoke(() =>
						   {
							   var RateName = FormatRateName();
							   Logging.WriteLogLine("DEBUG Loading rates for rateName: " + RateName);
							   // TODO After loading, SdModifiedRates has return -> return but with empty values and formulas
							   //if( SdModifiedRates.ContainsKey( rateName ) )
							   //{
							   // view.PopulateMatrix( SdModifiedRates[ rateName ], Zones );
							   //}
							   //else if( SdRates.ContainsKey( rateName ) )
							   // Make sure this is off unless checked
							   IsThisLanePricingPairCumulativeChecked = false;

							   if (RateName.IsNotNullOrWhiteSpace())
							   {
								   if (SdRates.ContainsKey(RateName))
								   {
									   CurrentlyLoadedRates.Clear();

									   // TODO This seems to be loading the previous rates, althought the rateName is correct
									   //CurrentlyLoadedRates.AddRange( SdRates[ rateName ] );

									   //view.PopulateMatrix( SdRates[ rateName ], Zones );
									   // Seem to be getting random collection modified exceptions on startup when looping through SdRates
									   for (var I = 0; I < 5; I++)
									   {
										   lock (SdRates)
										   {
											   //foreach ( var rate in SdRates[ rateName ] )
											   try
											   {
												   for (var J = 0; J < SdRates[RateName].Count; J++)
												   {
													   var Rate = SdRates[RateName][J];

													   //Logging.WriteLogLine("DEBUG rate: " + rate.PackageType + "->" + rate.ServiceLevel);
													   if (Rate is not null)
														   CurrentlyLoadedRates.Add(Rate);
												   }
												   break;
											   }
											   catch (Exception E)
											   {
												   Logging.WriteLogLine(E);
												   Thread.Sleep(50);
											   }
										   }
									   }
									   View?.PopulateMatrix(CurrentlyLoadedRates, Zones);
								   }
								   else
								   {
									   // Load empty values - fixed price
									   View?.PopulateMatrix(null, Zones);
								   }
							   }
						   });
	}

	private void CheckMatrixForModification()
	{
		var RateName = FormatRateName(true);
		//List<LocalRateMatrixItem> items = new();

		//if( SdModifiedRates.ContainsKey( rateName ) )
		//{
		//	items = SdModifiedRates[ rateName ];
		//}
		//else if( SdRates.ContainsKey( rateName ) )
		//if( SdRates.ContainsKey( rateName ) )
		//{
		//	items = SdRates[ rateName ];
		//}

		//if( view.CheckMatrixForModifications( items ) )
		if (View is not null && RateName.IsNotNullOrWhiteSpace() && View.CheckMatrixForModifications(CurrentlyLoadedRates))
		{
			if (!ModifiedRateNames.Contains(RateName))
				ModifiedRateNames.Add(RateName);
			UpdateRatesDictionary(RateName);

			if (!IsPriceMatrixLocked)
			{
				ArePendingRateMatricies = true;
				SetArePendingChanges(true);
			}
		}
	}

	public void Execute_ModifyPriceMatrix()
	{
		var RateName = FormatRateName();
		//Logging.WriteLogLine( "DEBUG Setting matrix values for: " + rateName + ", " + SelectedActionForMatrix + ", " + ActionValue );

		Dispatcher.Invoke(() =>
						   {
							   if (View is not null)
							   {
								   if (SelectedActionForMatrix == FindStringResource("RateWizardTabPriceMatrixComboActionInitializeValues"))
									   View.SetMatrixToNewValue(ActionValue);
								   else if (SelectedActionForMatrix == FindStringResource("RateWizardTabPriceMatrixComboActionAddToValues"))
									   View.SetMatrixAddValue(ActionValue);
								   else if (SelectedActionForMatrix == FindStringResource("RateWizardTabPriceMatrixComboActionMultiplyValuesBy"))
									   View.SetMatrixMultiplyValue(ActionValue);
							   }
						   });
		UpdateRatesDictionary(RateName);

		if (!IsPriceMatrixLocked)
		{
			ArePendingRateMatricies = true;
			SetArePendingChanges(true);
		}
	}

	//private void UpdateRatesDictionary( string rateName )
	//{
	//	if( !SdModifiedRates.ContainsKey( rateName ) )
	//	{
	//		//SdModifiedRates.Add(rateName, new List<Dictionary<string, decimal>>());
	//		SdModifiedRates.Add( rateName, new List<LocalRateMatrixItem>() );
	//	}

	//	Dispatcher.Invoke( () =>
	//	                   {
	//		                   List<LocalRateMatrixItem> list = IsFixedPriceSelected ? view.GatherValuesForFixedPrice( Zones )
	//			                                                    : view.GatherValuesForLanePricing( Zones );
	//		                   SdModifiedRates[ rateName ] = list;

	//						   ArePendingRateMatricies = true;
	//		                   SetArePendingChanges( true );
	//	                   } );
	//}
	private void UpdateRatesDictionary(string rateName)
	{
		if (!SdRates.ContainsKey(rateName))
		{
			//SdModifiedRates.Add(rateName, new List<Dictionary<string, decimal>>());
			SdRates.Add(rateName, new List<LocalRateMatrixItem?>());
		}

		Dispatcher.Invoke(() =>
						   {
							   if (View is not null)
							   {
								   var List = IsFixedPriceSelected ? View.GatherValuesForFixedPrice(Zones)
												  : View.GatherValuesForLanePricing(Zones);
								   SdRates[rateName] = List;

								   ArePendingRateMatricies = true;
								   SetArePendingChanges(true);
							   }
						   });
	}

	public string FormatRateName(bool usePreviousValues = false)
	{
		var RateName = !usePreviousValues
						   ? SelectedPackageTypeForMatrix + " -> " + SelectedServiceLevelForMatrix
						   : PreviousPackageTypeForMatrix + " -> " + PreviousServiceLevelForMatrix;

		if (RateName.Trim().StartsWith("->") || RateName.Trim().EndsWith("->"))
			RateName = string.Empty;
		return RateName;
	}

	public void Execute_SaveRateMatricies()
	{
		RateMatrixItems Rmis = new();

		CheckMatrixForModification();

		if (ModifiedRateNames.Count > 0)
		{
			List<RateMatrixItem?> Modified = new();

			foreach (var Key in ModifiedRateNames)
			{
				foreach (var Lrmi in SdRates[Key])
				{
					if (Lrmi is not null)
					{
						var Rmi = ConvertFromLocalRateMatrixItem(Lrmi);
						Modified.Add(Rmi);
					}
				}
			}

			/*
			 * Need to merge the modified rates with the others in RateMatrixItems
			 *	before updating the server.
			 * Add if new, 
			 * Replace if not.
			 */
			foreach (var Rmi in Modified)
			{
				if (Rmi is not null)
				{
					var Found = IsRateMatrixItemInRateMatrixItems(Rmi);

					RateMatrixItems.Remove(Found);
					Rmis.Add(Rmi);
				}
			}

			//GetRateMatrixItems();
			if (Rmis.Count > 0)
			{
				//List<RateMatrixItem> formulae = BuildLaneFormulaRateMatrixItems(rmis[0]);
				//rmis.AddRange(formulae);
				var Formulae = BuildLaneFormulaRateMatrixItem(Rmis[0]);
				Rmis.Add(Formulae);
			}
		}

		Task.Run(async () =>
				  {
					  Logging.WriteLogLine("Saving " + Rmis?.Count + " RateMatrix Items");
					  await Azure.Client.RequestAddUpdateRateMatrixItems(Rmis);
				  });

		Dispatcher.Invoke(() =>
						   {
							   ArePendingRateMatricies = false;
							   ModifiedRateNames.Clear();
							   SetArePendingChanges(ArePendingChangesBool);
						   });
	}

	private RateMatrixItem BuildLaneFormulaRateMatrixItem(RateMatrixItem baseOn)
	{
		RateMatrixItem Rmi = new()
		{
			ServiceLevelId = baseOn.ServiceLevelId,
			PackageTypeId = baseOn.PackageTypeId,
			FromZoneId = 0,
			ToZoneId = 0,
			Value = -1
		};
		var Formula = string.Empty;

		if (DictFullFormulas.Count > 0)
		{
			var I = 1;

			foreach (var Key in DictFullFormulas.Keys)
			{
				//formula += "// Lane " + i + "\n" + DictFullFormulas[key] + "\n";
				Formula += DictFullFormulas[Key] + "\n";
				//Logging.WriteLogLine("DEBUG Adding formula: " + formula + " for lane " + i);
				++I;
			}
			Rmi.Formula = Formula.Substring(0, Formula.Length - 1);
		}

		return Rmi;
	}

	//public void Execute_SaveRateMatricies_V1()
	//{
	//	RateMatrixItems rmis = new();

	//	CheckMatrixForModification();

	//	if (SdModifiedRates.Count > 0)
	//          {
	//		List<RateMatrixItem> modified = new();
	//		foreach (string key in SdModifiedRates.Keys)
	//              {
	//			foreach (LocalRateMatrixItem lrmi in SdModifiedRates[key])
	//                  {
	//				RateMatrixItem rmi = ConvertFromLocalRateMatrixItem(lrmi);
	//				modified.Add(rmi);
	//                  }
	//              }

	//		/*
	//		 * Need to merge the modified rates with the others in RateMatrixItems
	//		 *	before updating the server.
	//		 * Add if new, 
	//		 * Replace if not.
	//		 */
	//		foreach (RateMatrixItem rmi in modified)
	//              {
	//			RateMatrixItem found = IsRateMatrixItemInRateMatrixItems(rmi);
	//			if (found != null)
	//                  {
	//				RateMatrixItems.Remove(found);
	//                  }
	//			rmis.Add(rmi);
	//              }

	//		GetRateMatrixItems();
	//          }

	//	Task.Run( async() =>
	//	{
	//		await Azure.Client.RequestAddUpdateRateMatrixItems(rmis);
	//	});

	//	Dispatcher.Invoke( () =>
	//	                   {
	//		                   ArePendingRateMatricies = false;

	//		                   SetArePendingChanges( ArePendingChangesBool );
	//	                   } );
	//}

	private RateMatrixItem IsRateMatrixItemInRateMatrixItems(RateMatrixItem rmi)
	{
		var Result = (from R in RateMatrixItems
					  where (R.FromZoneId == rmi.FromZoneId)
							&& (R.PackageTypeId == rmi.PackageTypeId)
							&& (R.ServiceLevelId == rmi.ServiceLevelId)
							&& (R.ToZoneId == rmi.ToZoneId)
					  select R).FirstOrDefault();

		return Result;
	}

	public void Execute_ShowHelpRateMatrix()
	{
		Logging.WriteLogLine("Opening " + HelpUriRateMatrix);
		Uri Uri = new(HelpUriRateMatrix);
		Process.Start(new ProcessStartInfo(Uri.AbsoluteUri));
	}

	public void BuildFullFormulae()
	{
		DictFullFormulas.Clear();

		foreach (var Lane in DictLanes.Keys)
		{
			if (DictLanes.ContainsKey(Lane))
			{
				var Lrmis = DictLanes[Lane];
				List<string> Parts = new();

				foreach (var Item in Lrmis)
				{
					Logging.WriteLogLine("Found formula:\n" + Item.Formula + " ");

					if (Item.Formula.IsNotNullOrWhiteSpace())
						Parts.Add(Item.Formula);
				}

				if (Parts.Count > 0)
				{
					var Full = EvaluateRateFormulaHelper.BuildFullFormula(Parts, IsThisLanePricingPairCumulativeChecked);

					if (Full.IsNotNullOrWhiteSpace() && !DictFullFormulas.ContainsKey(Lane))
					{
						var Test = EvaluateRateFormulaHelper.LANE + Lane;

						if (!Full.Contains(Test))
							Full = Test + "\n" + Full;
						DictFullFormulas.Add(Lane, Full);
					}
				}
			}
			else
			{
				StringBuilder sb = new();
				foreach (string key in DictLanes.Keys)
				{
					sb.Append(key).Append(",");
				}
				Logging.WriteLogLine($"ERROR DictLanes doesn't contain an entry for {Lane}.  Current entries: {sb.ToString()}");
			}
		}
	}

	public string GetAllFormulae()
	{
		var Formula = string.Empty;

		foreach (var Lane in DictFullFormulas.Keys)
			Formula += DictFullFormulas[Lane] + "\n";

		return Formula;
	}

	public void InitializeCurrentPairFromOther(string otherName)
	{
		var CurrentName = FormatRateName();
		Logging.WriteLogLine("Initializing " + CurrentName + " to " + otherName);

		// Pull out the selected bunch from the SdModifiedRates collection
		//if (SdModifiedRates.ContainsKey(otherName))
		if (SdRates.ContainsKey(otherName))
		{
			// pts = (from P in PackageTypeObjects orderby P.SortOrder select P.Description).ToList();
			//List<LocalRateMatrixItem> values = (from I in SdModifiedRates[otherName] select I).ToList();
			var Values = (from I in SdRates[otherName]
						  select I).ToList();

			//SdModifiedRates.Add(currentName, values);
			SdRates.Add(CurrentName, Values);

			if (!ModifiedRateNames.Contains(CurrentName))
				ModifiedRateNames.Add(CurrentName);

			LoadSelectedRates();
			LoadActivePackageServicePairs();

			if (View is not null)
				View.TcPricing.SelectedIndex = IsFixedPrice(Values) ? 0 : 1;

			if (!IsPriceMatrixLocked)
			{
				ArePendingRateMatricies = true;
				SetArePendingChanges(true);
			}
		}
	}

	private bool IsFixedPrice(List<LocalRateMatrixItem> items)
	{
		var Yes = true;

		var Formula = (from I in items
					   where I.Formula.IsNotNullOrWhiteSpace()
					   select I.Formula).FirstOrDefault();

		if (Formula.IsNotNullOrWhiteSpace())
			Yes = false;

		return Yes;
	}
	#endregion

	#region Surcharges Data

	public readonly string ACCOUNT_CHARGE_PREFIX = Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX;
	public readonly string SURCHARGE_PREFIX = Globals.DataContext.MainDataContext.SURCHARGE_PREFIX;

	public List<Charge> AllNonSurchargeCharges
	{
		get { return Get(() => AllNonSurchargeCharges, new List<Charge>()); }
		set { Set(() => AllNonSurchargeCharges, value); }
	}

	public List<Charge> Surcharges
	{
		get { return Get(() => Surcharges, new List<Charge>()); }
		set { Set(() => Surcharges, value); }
	}

	public string SelectedSurcharge
	{
		get => Get(() => SelectedSurcharge, "");
		set => Set(() => SelectedSurcharge, value);
	}

	public Charge? SelectedSurchargeObject { get; set; } = null;

	[DependsUpon(nameof(SelectedSurchargeObject))]
	public void WhenSelectedSurchargeObjectChanges()
	{
		Logging.WriteLogLine($"DEBUG SelectedSurchargeObject: {SelectedSurchargeObject?.ChargeId} {SelectedSurchargeObject?.Formula}");
	}

	public bool IsEditingSurcharge
	{
		get => Get(() => IsEditingSurcharge, false);
		set => Set(() => IsEditingSurcharge, value);
	}

	//[DependsUpon(nameof(IsEditingSurcharge))]
	public bool IsSurchargeReadOnly
	{
		get => Get(() => IsSurchargeReadOnly, !IsEditingSurcharge);
		set => Set(() => IsSurchargeReadOnly, value);
	}

	[DependsUpon(nameof(IsEditingSurcharge))]
	public void WhenIsEditingSurchargeChanges()
	{
		IsSurchargeReadOnly = !IsEditingSurcharge;
		//Logging.WriteLogLine($"IsEditingSurcharge: {IsEditingSurcharge}  IsSurchargeReadOnly: {IsSurchargeReadOnly}");
	}

	public bool IsNewSurcharge
	{
		get => Get(() => IsNewSurcharge, false);
		set => Set(() => IsNewSurcharge, value);
	}

	public string SurchargeFormula
	{
		get => Get(() => SurchargeFormula, "");
		set => Set(() => SurchargeFormula, value);
	}

	public string SurchargeName
	{
		get => Get(() => SurchargeName, "");
		set => Set(() => SurchargeName, value);
	}

	public List<string> SelectedSurchargeZones
	{
		get => Get(() => SelectedSurchargeZones, new List<string>());
		set => Set(() => SelectedSurchargeZones, value);
	}

	public List<string> SelectedSurchargePackageTypes
	{
		get => Get(() => SelectedSurchargePackageTypes, new List<string>());
		set => Set(() => SelectedSurchargePackageTypes, value);
	}

	public decimal? SelectedSurchargeValue
	{
		get => Get(() => SelectedSurchargeValue, 0);
		set => Set(() => SelectedSurchargeValue, value);
	}

	public bool ArePendingSurcharges
	{
		get => Get(() => ArePendingSurcharges, false);
		set => Set(() => ArePendingSurcharges, value);
	}

	#endregion
	#region Surcharges Actions

	public List<Charge> GetCharges()
	{
		List<Charge> charges = new();

		if (!IsInDesignMode)
		{
			Task.Run(() =>
			{
				try
				{
					Logging.WriteLogLine("Getting charges...");
					var result = Azure.Client.RequestGetAllCharges().Result;
					Logging.WriteLogLine("Found " + result.Count + " charges");

					// Ensure that the default charges are ignored
					AllNonSurchargeCharges = (from C in result
											  where !C.ChargeId.StartsWith(SURCHARGE_PREFIX)
											  orderby C.SortIndex
											  select C).ToList();

					Surcharges = (from C in result
								  where C.ChargeId.StartsWith(SURCHARGE_PREFIX)
								  select C).ToList();

				}
				catch (Exception e)
				{
					Logging.WriteLogLine("Exception thrown: " + e);
					Dispatcher.Invoke(() =>
					{
						MessageBox.Show("Error fetching Charges\n" + e, "Error", MessageBoxButton.OK);
					});
				}
			});
		}

		return charges;
	}

	public void BuildSurchargeFormula()
	{
		StringBuilder formula = new("If PackageType in (");
		SelectedSurchargePackageTypes = (from S in SelectedSurchargePackageTypes orderby S select S).ToList();
		foreach (string pt in SelectedSurchargePackageTypes)
		{
			formula.Append(pt).Append(",");
		}
		if (SelectedSurchargePackageTypes.Count > 0)
		{
			formula.Remove(formula.Length - 1, 1); // Remove final ,
		}
		formula.Append(") and Zone in (");
		SelectedSurchargeZones = (from S in SelectedSurchargeZones orderby S select S).ToList();
		foreach (string zone in SelectedSurchargeZones)
		{
			formula.Append(zone).Append(",");
		}
		if (SelectedSurchargeZones.Count > 0)
		{
			formula.Remove(formula.Length - 1, 1); // Remove final ,
		}
		formula.Append(") add ").Append(SelectedSurchargeValue?.ToString("C")).Append(" to Rate");

		Logging.WriteLogLine($"Formula: '{formula}'");
		SurchargeFormula = formula.ToString();
		if (IsNewSurcharge || (SelectedSurchargeObject != null && SelectedSurchargeObject.Label.IsNullOrWhiteSpace()))
		{
			BuildSurchargeName();
		}
	}

	public void BuildSurchargeName()
	{
		StringBuilder name = new("Surcharge-PackageTypes-");

        SelectedSurchargePackageTypes = (from S in SelectedSurchargePackageTypes orderby S select S).ToList();
        foreach (string pt in SelectedSurchargePackageTypes)
        {
            name.Append(pt).Append(",");
        }
        if (SelectedSurchargePackageTypes.Count > 0)
        {
            name.Remove(name.Length - 1, 1); // Remove final ,
        }
		name.Append("-Zones-");
        foreach (string zone in SelectedSurchargeZones)
        {
            name.Append(zone).Append(",");
        }
        if (SelectedSurchargeZones.Count > 0)
        {
            name.Remove(name.Length - 1, 1); // Remove final ,
        }
        name.Append("-Value-").Append(SelectedSurchargeValue);

        SurchargeName = name.ToString();
	}

	public void LoadSelectedSurcharge()
	{
		List<string> packageTypes = new();
		List<string> zones = new();
        if (SelectedSurchargeObject != null)
		{
			Logging.WriteLogLine($"Editing Surcharge: {SelectedSurchargeObject.ChargeId} {SelectedSurchargeObject.Formula}");
			IsEditingSurcharge = true;
			IsNewSurcharge = false;
			SurchargeName = SelectedSurchargeObject.Label;

			// Get package types, zones, and value
			// If PackageType in (CORPORTE,Corp25) and Zone in (2B,4B) add $10.00 to Rate
			//string[] pieces = SelectedSurcharge.Split('(');
			string[] pieces = SelectedSurchargeObject.Formula.Split('(');
			if (pieces.Length == 3)
			{
				// 0 If PackageType in
				// 1 CORPORTE,Corp25) and Zone in
				// 2 2B,4B) add $10.00 to Rate
				string[] pts = pieces[1].Split(')');
				// 0 CORPORTE,Corp25
				// 1 and Zone in
				if (pts.Length == 2)
				{
					pts = pts[0].Split(',');
					// This will result in 1 or more 
					if (pts.Length == 1 && pts[0].Trim().Length > 0)
					{
						packageTypes.Add(pts[0].Trim());
					}
					else if (pts.Length > 1)
					{
						foreach (string pt in pts)
						{
							packageTypes.Add(pt.Trim());
						}
					}

					pts = pieces[2].Split(')');
					// 0 2B,4B
					// 1  add $10.00 to Rate
					string valuePart = pts[1].Trim();
					pts = pts[0].Split(',');
					// This will result in 1 or more 
					if (pts.Length == 1 && pts[0].Trim().Length > 0)
					{
						zones.Add(pts[0].Trim());
					}
					else if (pts.Length > 1)
					{
						foreach (string pt in pts)
						{
							zones.Add(pt.Trim());
						}
					}

					//  add $10.00 to Rate
					pts = valuePart.Split(' ');
                    string valueString = pts[1].Substring(1); // Lop off the leading $
					bool success = decimal.TryParse(valueString, out decimal value);
					SelectedSurchargeValue = success ? value : 0;

					if (View != null)
					{
						Dispatcher.Invoke(() =>
						{
							if (packageTypes.Count > 0)
							{
								foreach (string pt in packageTypes)
								{
									foreach (var child in View.GridSurchargePackageTypes.Children)
									{
										if (child != null && child is CheckBox cb && (string)cb.Content == pt)
										{
											cb.IsChecked = true;
											break;
										}
									}
								}
							}
                            if (zones.Count > 0)
                            {
                                foreach (string zone in zones)
                                {
                                    foreach (var child in View.GridSurchargeZones.Children)
                                    {
                                        if (child != null && child is CheckBox cb && (string)cb.Content == zone)
                                        {
                                            cb.IsChecked = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        });
					}
				}
				else
				{
                    Logging.WriteLogLine($"ERROR Can't split {pieces[1]} successfully.");
                }
			}
			else
			{
				Logging.WriteLogLine($"ERROR Can't split {SelectedSurchargeObject.Formula} successfully.");
			}
			
        }
    }

	public void AddUpdateSurchargeLocally()
	{
		BuildSurchargeFormula();
		Dispatcher.Invoke(() =>
		{
			if (View != null)
			{
				string id = SelectedSurchargeObject?.ChargeId ?? string.Empty;
				View.LbSurcharges.ItemsSource = null;
				if (IsNewSurcharge)
				{
					// Create a new Surcharge and set the formula
					Charge charge = new()
					{
						ChargeId = SURCHARGE_PREFIX + DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
						Label = SurchargeName,
						Formula = SurchargeFormula,
						Value = (decimal)(SelectedSurchargeValue != null ? SelectedSurchargeValue : 0),
						SortIndex = -1,
					};
					Surcharges.Add(charge);
				}
				else
				{
					if (SelectedSurcharge != SurchargeFormula && id.IsNotNullOrWhiteSpace())
					{
						Charge charge = (from S in Surcharges where S.ChargeId == id select S).FirstOrDefault();
						if (charge != null)
						{
							charge.Formula = SurchargeFormula;
							charge.Value = (decimal)(SelectedSurchargeValue != null ? SelectedSurchargeValue : 0);
							if (charge.Label.IsNullOrWhiteSpace())
							{
								charge.Label = SurchargeName;
							}
                        }
					}
				}
				View.LbSurcharges.ItemsSource = Surcharges;

                ArePendingSurcharges = true;
                SetArePendingChanges(true);
            }
		});
    }

	public void Execute_SaveSurcharges()
	{
		Logging.WriteLogLine($"Saving {Surcharges.Count} surcharges");
		Task.Run(async () =>
		{
			Charges charges = new();
			charges.AddRange(AllNonSurchargeCharges);
			charges.AddRange(Surcharges);

			await Azure.Client.RequestAddUpdateCharges(charges);

			ArePendingSurcharges = false;
			SetArePendingChanges(false);
		});
	}

	public void DeleteSurchargeLocally()
	{
		if (SelectedSurchargeObject != null && View != null)
		{
			Logging.WriteLogLine($"Deleting {SelectedSurchargeObject.ChargeId} {SelectedSurchargeObject.Formula} locally");
			Dispatcher.Invoke(() =>
			{
                string id = SelectedSurchargeObject.ChargeId ?? string.Empty;
                View.LbSurcharges.ItemsSource = null;
				for (int i = 0; i < Surcharges.Count; i++)
				{
					if (Surcharges[i].ChargeId == id)
					{
						Surcharges.RemoveAt(i);
						break;
					}
				}
                //Surcharges.Remove(SelectedSurchargeObject);
                View.LbSurcharges.ItemsSource = Surcharges;
            });
			ArePendingSurcharges = true;
			SetArePendingChanges(true);
		}
	}

    #endregion
}

/// <summary>
///     Utility class to allow the listbox in the ServiceLevels to be coloured.
/// </summary>
public class DisplayServiceLevel
{
	public DisplayServiceLevel()
	{
		Foreground = new SolidColorBrush( Colors.Black );
		Background = new SolidColorBrush( Colors.White );
	}

	public DisplayServiceLevel( ServiceLevel s )
	{
		Foreground = s.ForegroundARGB.ArgbToSolidColorBrush();
		Background = s.BackgroundARGB.ArgbToSolidColorBrush();
	}

	public SolidColorBrush Foreground,
						   Background;

	public string Name = "";
}

// Copied from server TODO Replace
public class LocalRateMatrixItem
{
	public string  Name         { get; set; } = "";
	public string  FromZone     { get; set; } = "";
	public string  ToZone       { get; set; } = "";
	public string  ServiceLevel { get; set; } = "";
	public string  PackageType  { get; set; } = "";
	public string  VehicleType  { get; set; } = "";
	public decimal Value        { get; set; }
	public string  Formula      { get; set; } = "";
}