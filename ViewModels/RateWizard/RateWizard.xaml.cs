﻿// #nullable enable
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModels.Trips.TripEntry;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace ViewModels.RateWizard;

/// <summary>
///     Interaction logic for RateWizard.xaml
/// </summary>
public partial class RateWizard : Page
{
	public RateWizard()
	{
		InitializeComponent();
		Model = (RateWizardModel)DataContext;
		Model.SetView(this);

		//ToggleIsEditing();

		//this.lbServiceLevels.Items.Refresh();

		//BuildPriceMatrix();
	}

	private readonly RateWizardModel Model;

	private void SaveAll(object sender, RoutedEventArgs e)
	{
		Model.SaveAll();
	}

	private void cpForeground_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
	{
		Logging.WriteLogLine("Setting foreground to " + cpForeground.SelectedColor);
	}

	private void cpBackground_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
	{
		Logging.WriteLogLine("Setting background to " + cpBackground.SelectedColor);
	}

	public static string FindStringResource(string name) => (string)Application.Current.TryFindResource(name);

	//private string GetServiceLevelFromDetailed(string line)
	//{
	//    string sl = string.Empty;

	//    // Service[Next Day,DEL hrs: 00:00:00,END time: 00:00:00]
	//    //int start = "Service[".Length;
	//    int start = "[".Length;
	//    string endTag = ",DEL";
	//    int end = line.IndexOf(endTag) - endTag.Length;
	//    sl = line.Substring(start, end);

	//    return sl;
	//}

	/// <summary>
	///     Handles the updating of the UI
	/// </summary>

	//private void ToggleIsEditing()
	//{
	//    if (DataContext is RateWizardModel Model)
	//    {
	//        //this.btnServiceLevelEdit.IsEnabled = !Model.IsEditingServiceLevel;
	//        //this.btnServiceLevelAdd.IsEnabled = !Model.IsEditingServiceLevel;
	//        //this.btnServiceLevelDel.IsEnabled = !Model.IsEditingServiceLevel;
	//        //this.btnServiceLevelUp.IsEnabled = !Model.IsEditingServiceLevel;
	//        //this.btnServiceLevelDown.IsEnabled = !Model.IsEditingServiceLevel;

	//        //this.btnUpdate.IsEnabled = Model.IsEditingServiceLevel;
	//        //this.btnCancel.IsEnabled = Model.IsEditingServiceLevel;
	//        //this.btnClear.IsEnabled = Model.IsEditingServiceLevel;

	//        Model.IsTextBoxReadOnly = !Model.IsEditingServiceLevel;
	//    }
	//}
	public void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	public void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}

	#region Service Level Handlers
	private (ServiceLevel sl, string oldName) PopulateServiceLevel(ServiceLevel sl)
	{
		ServiceLevel newSl = null;
		var oldName = string.Empty;

		if ((sl != null) && sl.NewName.IsNotNullOrWhiteSpace())
		{
			newSl = sl;
			oldName = sl.N;
		}
		else
		{
			newSl = new ServiceLevel
			{
				// Put new ones at the bottom of the list
				SortOrder = (ushort)lbServiceLevels.Items.Count
			};
		}

		newSl.N = Model.SelectedServiceLevelName;
		newSl.NewName = Model.SelectedServiceLevelName;

		static uint ToUint(ColorPicker p, Color defaultColor)
		{
			var C = p?.SelectedColor ?? defaultColor;

			return ((uint)0xff << 24) | ((uint)C.R << 16) | ((uint)C.G << 8) | C.B; // Make sure we have the alpha channel
		}

		// ServiceLevel colours
		newSl.BackgroundARGB = ToUint(cpBackground, Colors.Transparent);
		newSl.ForegroundARGB = ToUint(cpForeground, Colors.Black);

		newSl.DeliveryHours = Model.SelectedServiceLevelDeliveryHoursTimeSpan;

		int minutes;

		if (Model.SelectedServiceLevelWarn.IsNotNullOrWhiteSpace())
		{
			try
			{
				minutes = int.Parse(Model.SelectedServiceLevelWarn.Trim());
			}
			catch
			{
				Logging.WriteLogLine("ERROR - can't parse warning " + Model.SelectedServiceLevelWarn.Trim());
				minutes = 0;
			}
			newSl.WarningHours = new TimeSpan(0, minutes, 0);
		}

		if (Model.SelectedServiceLevelCritical.IsNotNullOrWhiteSpace())
		{
			try
			{
				minutes = int.Parse(Model.SelectedServiceLevelCritical.Trim());
			}
			catch
			{
				Logging.WriteLogLine("ERROR - can't parse critical " + Model.SelectedServiceLevelCritical.Trim());
				minutes = 0;
			}
			newSl.CriticalHours = new TimeSpan(0, minutes, 0);
		}

		if (Model.SelectedServiceLevelBeyond.IsNotNullOrWhiteSpace())
		{
			try
			{
				minutes = int.Parse(Model.SelectedServiceLevelBeyond.Trim());
			}
			catch
			{
				Logging.WriteLogLine("ERROR - can't parse beyond " + Model.SelectedServiceLevelBeyond.Trim());
				minutes = 0;
			}
			newSl.BeyondHours = new TimeSpan(0, minutes, 0);
		}

		newSl.StartTime = new TimeSpan(Model.SelectedServiceLevelStartTime.Hour, Model.SelectedServiceLevelStartTime.Minute, Model.SelectedServiceLevelStartTime.Second);
		newSl.EndTime = new TimeSpan(Model.SelectedServiceLevelEndTime.Hour, Model.SelectedServiceLevelEndTime.Minute, Model.SelectedServiceLevelEndTime.Second);
		newSl.Sunday = Model.SelectedServiceLevelSun;
		newSl.Monday = Model.SelectedServiceLevelMon;
		newSl.Tuesday = Model.SelectedServiceLevelTue;
		newSl.Wednesday = Model.SelectedServiceLevelWed;
		newSl.Thursday = Model.SelectedServiceLevelThu;
		newSl.Friday = Model.SelectedServiceLevelFri;
		newSl.Saturday = Model.SelectedServiceLevelSat;

		// Assemble new PackageTypes
		newSl.PackageTypes.Clear();
		if (Model.SelectedServiceLevelPackageTypes.Count > 0)
		{
			var list = new List<string>();

			foreach (var pt in Model.SelectedServiceLevelPackageTypes)
			{
				Logging.WriteLogLine("Adding package type " + pt + " to service level: " + newSl.NewName);
				list.Add(pt);
			}
			newSl.PackageTypes = list;
		}

		return (newSl, oldName);
	}

	private void BtnServiceLevelEdit_Click(object sender, RoutedEventArgs e)
	{
		//string line = Model.SelectedServiceLevel;
		// sl is the detailed string - need to extract the service level
		// string slName = this.GetServiceLevelFromDetailed(line);
		var SelectedIndex = lbServiceLevels.SelectedIndex;

		if (SelectedIndex > -1)
		{
			BtnServiceLevelClear_Click(null, null);
			Model.IsEditingServiceLevel = true;
			Model.IsServiceLevelTextBoxReadOnly = false;

			//this.ToggleIsEditing();

			var sls = Model.ServiceLevelObjects;

			var sl = sls[SelectedIndex];

			Logging.WriteLogLine("Editing Service Level: " + sl.N);
			Model.CurrentlyEditedServiceLevel = sl;

			if ((sl.DeliveryHours.Hours < 24) && (sl.DeliveryHours.Minutes <= 59) && (sl.DeliveryHours.Seconds < 59))
				Model.SelectedServiceLevelDeliveryHoursTimeSpan = sl.DeliveryHours;
			else
				Model.SelectedServiceLevelDeliveryHoursTimeSpan = new TimeSpan(0, 0, 0);

			Model.SelectedServiceLevelName = sl.N;
			Model.SelectedServiceLevelWarn = sl.WarningHours.TotalMinutes + "";
			Model.SelectedServiceLevelCritical = sl.CriticalHours.TotalMinutes + "";
			Model.SelectedServiceLevelBeyond = sl.BeyondHours.TotalMinutes + "";

			Model.SelectedServiceLevelStartTime = new DateTime(1, 1, 1) + sl.StartTime;
			Model.SelectedServiceLevelEndTime = new DateTime(1, 1, 1) + sl.EndTime;

			Model.SelectedServiceLevelSun = sl.Sunday;
			Model.SelectedServiceLevelMon = sl.Monday;
			Model.SelectedServiceLevelTue = sl.Tuesday;
			Model.SelectedServiceLevelWed = sl.Wednesday;
			Model.SelectedServiceLevelThu = sl.Thursday;
			Model.SelectedServiceLevelFri = sl.Friday;
			Model.SelectedServiceLevelSat = sl.Saturday;

			// Colours
			var bytes = BitConverter.GetBytes(sl.BackgroundARGB);
			var bg = new SolidColorBrush(Color.FromRgb(bytes[2], bytes[1], bytes[0]));
			cpBackground.SelectedColor = bg.Color;
			bytes = BitConverter.GetBytes(sl.ForegroundARGB);
			var fg = new SolidColorBrush(Color.FromRgb(bytes[2], bytes[1], bytes[0]));
			cpForeground.SelectedColor = fg.Color;

			Model.SelectedServiceLevelPackageTypes.Clear();

			if (sl.PackageTypes.Count > 0)
			{
				foreach (var pt in sl.PackageTypes)
					Model.SelectedServiceLevelPackageTypes.Add(pt);
			}
		}
	}

	private void BtnServiceLevelNew_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Adding Service Level");
		Model.IsEditingServiceLevel = true;
		Model.IsServiceLevelTextBoxReadOnly = false;

		Model.SelectedServiceLevelSun = true;
		Model.SelectedServiceLevelMon = true;
		Model.SelectedServiceLevelTue = true;
		Model.SelectedServiceLevelWed = true;
		Model.SelectedServiceLevelThu = true;
		Model.SelectedServiceLevelFri = true;
		Model.SelectedServiceLevelSat = true;

		//this.ToggleIsEditing();                

		// Set default colours
		cpBackground.SelectedColor = new SolidColorBrush(Colors.White).Color;
		cpForeground.SelectedColor = new SolidColorBrush(Colors.Black).Color;
	}

	private void BtnServiceLevelDel_Click(object sender, RoutedEventArgs e)
	{
		var selectedIndex = lbServiceLevels.SelectedIndex;

		if (selectedIndex > -1)
		{
			var sl = Model.ServiceLevelObjects[selectedIndex];

			var title = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDeleteTitle");

			var message = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDelete");
			message = message.Replace("@1", sl.N);
			var response = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);

			if (response == MessageBoxResult.Yes)
				Model.DeleteServiceLevelLocally(sl);
		}
	}

	private void BtnServiceLevelUp_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbServiceLevels.SelectedIndex;
		Logging.WriteLogLine("Current selectedIndex: " + SelectedIndex + ", moving to " + (SelectedIndex - 1));

		if (SelectedIndex > 0)
		{
			var sl = Model.ServiceLevelObjects[SelectedIndex];

			Model.MoveServiceLevelLocally(sl, SelectedIndex - 1);
		}
	}

	private void BtnServiceLevelDown_Click(object sender, RoutedEventArgs e)
	{
		var selectedIndex = lbServiceLevels.SelectedIndex;
		Logging.WriteLogLine("Current selectedIndex: " + selectedIndex + ", moving to " + (selectedIndex + 1));

		if ((selectedIndex > -1) && (selectedIndex < (lbServiceLevels.Items.Count - 1)))
		{
			var sl = Model.ServiceLevelObjects[selectedIndex];

			Model.MoveServiceLevelLocally(sl, selectedIndex + 1);
		}
	}

	private void BtnServiceLevelUpdate_Click(object sender, RoutedEventArgs e)
	{
		var errors = string.Empty;
		var exists = false;

		if (Model.SelectedServiceLevelName != string.Empty)
		{
			exists = Model.DoesServiceLevelNameExist(Model.SelectedServiceLevelName);

			// Make sure not editing
			if (exists && (Model.CurrentlyEditedServiceLevel == null))
			{
				// Name can't be a duplicate
				errors = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelErrorDuplicateName");
			}
		}
		else
		{
			// Name can't be empty
			errors = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelErrorEmptyName");
		}

		if (errors.Length == 0)
		{
			// ReSharper disable once InvalidXmlDocComment
			/// <summary>
			/// Adds, updates, deletes service levels.
			/// OldName == "", NewName == "Value"		    Is an Add
			/// OldName == "Value", NewName == "NewValue"	Is a Rename
			/// OldName == NewName				            Is an Update
			/// OldName == "Value", NewName == ""		    Is a Delete
			/// </summary>
			var (sl, oldName) = PopulateServiceLevel(Model.CurrentlyEditedServiceLevel);

			if (!exists && ((Model.CurrentlyEditedServiceLevel == null) || oldName.IsNullOrWhiteSpace()))
			{
				// Adding
				sl.OldName = string.Empty;
				sl.SortOrder = (ushort)Model.ServiceLevels.Count;
			}
			else if (!exists && (Model.CurrentlyEditedServiceLevel != null))
			{
				// Rename
				sl.OldName = oldName;
			}
			else if (exists)
			{
				// Update
				sl.OldName = sl.NewName;
			}

			Model.AddUpdateServiceLevel(sl);

			//Model.ReloadServiceLevels();

			Model.CurrentlyEditedServiceLevel = null;
			Model.IsEditingServiceLevel = false;
			Model.IsServiceLevelTextBoxReadOnly = true;
			Model.SetArePendingChanges(true);

			//this.ToggleIsEditing();
			BtnServiceLevelClear_Click(null, null);
		}
		else
		{
			Logging.WriteLogLine("Error found: " + errors);
			var title = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelErrorTitle");

			MessageBox.Show(errors, title, MessageBoxButton.OK, MessageBoxImage.Error);
		}
	}

	private void BtnServiceLevelCancel_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Cancelling Service Level change");

		Model.IsEditingServiceLevel = false;
		Model.IsServiceLevelTextBoxReadOnly = true;
		Model.CurrentlyEditedServiceLevel = null;

		//this.ToggleIsEditing();
		BtnServiceLevelClear_Click(null, null);
	}

	private void BtnServiceLevelClear_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Clearing Service Level");

		Model.SelectedServiceLevelName = string.Empty;
		tpStart.Value = new DateTime();
		tpEnd.Value = new DateTime();
		Model.SelectedServiceLevelSun = false;
		Model.SelectedServiceLevelMon = false;
		Model.SelectedServiceLevelTue = false;
		Model.SelectedServiceLevelWed = false;
		Model.SelectedServiceLevelThu = false;
		Model.SelectedServiceLevelFri = false;
		Model.SelectedServiceLevelSat = false;

		Model.SelectedServiceLevelDeliveryHoursTimeSpan = new TimeSpan();
		Model.SelectedServiceLevelWarn = string.Empty;
		Model.SelectedServiceLevelCritical = string.Empty;
		Model.SelectedServiceLevelBeyond = string.Empty;

		Model.SelectedServiceLevelPackageType = string.Empty;
		Model.SelectedServiceLevelPackageTypes.Clear();

		cpBackground.SelectedColor = null;
		cpForeground.SelectedColor = null;
	}

	private void BtnServiceLevelSave_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Saving Service Levels");
		Model.SaveServiceLevels();
	}

	private void LbServiceLevels_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	{
		BtnServiceLevelEdit_Click(null, null);
	}

	/// <summary>
	///     Ensure that the limit is 1 day.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void tsudDeliveryHours_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		Logging.WriteLogLine("Value changed: " + tsudDeliveryHours.Value);

		if (tsudDeliveryHours.Value.Value.TotalHours > 23)
		{
			var ts = new TimeSpan(23, tsudDeliveryHours.Value.Value.Minutes, 0);
			tsudDeliveryHours.Value = ts;
		}
		else if (tsudDeliveryHours.Value.Value.TotalMinutes < 0)
		{
			var ts = new TimeSpan(0, 0, 0);
			tsudDeliveryHours.Value = ts;
		}
	}

	private void tpStart_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//        if (DataContext is RateWizardModel Model)
		//        {
		//if (tpStart.Value.HasValue)
		//            {
		//	Model.SelectedServiceLevelStartTimeTimeSpan = new TimeSpan(tpStart.Value.Value.Hour, tpStart.Value.Value.Minute, 0);
		//            }
		//        }
	}

	private void tpEnd_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//if (DataContext is RateWizardModel Model)
		//{
		//	if (tpEnd.Value.HasValue)
		//	{
		//		Model.SelectedServiceLevelEndTimeTimeSpan = new TimeSpan(tpEnd.Value.Value.Hour, tpEnd.Value.Value.Minute, 0);
		//	}
		//}
	}


	private void btnAddPackage_Click(object sender, RoutedEventArgs e)
	{
		if (Model.SelectedServiceLevelPackageType.IsNotNullOrWhiteSpace())
		{
			if (!Model.SelectedServiceLevelPackageTypes.Contains(Model.SelectedServiceLevelPackageType))
				Model.SelectedServiceLevelPackageTypes.Add(Model.SelectedServiceLevelPackageType);
		}
	}

	private void btnDeletePackage_Click(object sender, RoutedEventArgs e)
	{
		if (lbSelectedPackageTypes.SelectedIndex > -1)
		{
			Model.SelectedServiceLevelPackageTypes.Remove((string)lbSelectedPackageTypes.SelectedItem);
		}
		else
		{
			string title = FindStringResource("RateWizardTabServiceLevelDeletePTErrorTitle");
			string message = FindStringResource("RateWizardTabServiceLevelDeletePTError");
			MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
		}
	}
	#endregion

	#region Package Type Handlers
	private PackageType PopulatePackageType(PackageType pt)
	{
		PackageType newPt = null;

		if (pt != null)
			newPt = pt;
		else
		{
			newPt = new PackageType
			{
				// Put new ones at the bottom of the list
				SortOrder = (ushort)lbPackageTypes.Items.Count
			};
		}

		newPt.Description = Model.SelectedPackageTypeName;

		return newPt;
	}

	private void BtnPackageTypeUpdate_Click(object sender, RoutedEventArgs e)
	{
		var errors = string.Empty;
		var exists = false;

		if (Model.SelectedPackageTypeName != string.Empty)
		{
			exists = Model.DoesPackageTypeNameExist(Model.SelectedPackageTypeName);

			// Make sure not editing
			if (exists && (Model.CurrentlyEditedPackageType == null))
			{
				// Name can't be a duplicate
				errors = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeErrorDuplicateName");
			}
		}
		else
		{
			// Name can't be empty
			errors = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeErrorEmptyName");
		}

		if (errors.Length == 0)
		{
			//if (Model.SelectedPackageTypeName != Model.CurrentlyEditedPackageType.Description)
			//{
			//    var copy = Model.CurrentlyEditedPackageType;
			//    Model.PackageTypeObjectsToDelete.Add(copy);
			//    //Model.DeletePackageTypeLocally(copy);
			//}
			var pt = PopulatePackageType(Model.CurrentlyEditedPackageType);

			if (!exists)
				pt.SortOrder = (ushort)Model.PackageTypeObjects.Count;

			Model.AddUpdatePackageType(pt);

			//Model.ReloadServiceLevels();

			Model.CurrentlyEditedPackageType = null;
			Model.IsEditingPackageType = false;
			Model.IsPackageTypeTextBoxReadOnly = true;
			Model.SetArePendingChanges(true);

			//this.ToggleIsEditing();
			BtnPackageTypeClear_Click(null, null);
		}
		else
		{
			Logging.WriteLogLine("Error found: " + errors);
			var title = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeErrorTitle");

			MessageBox.Show(errors, title, MessageBoxButton.OK, MessageBoxImage.Error);
		}
	}

	private void BtnPackageTypeCancel_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Cancelling Package Type change");

		Model.IsEditingPackageType = false;
		Model.IsPackageTypeTextBoxReadOnly = true;
		Model.CurrentlyEditedPackageType = null;

		//this.ToggleIsEditing();
		BtnPackageTypeClear_Click(null, null);
	}

	private void BtnPackageTypeClear_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Clearing Package Type");
		Model.SelectedPackageTypeName = string.Empty;
	}

	private void BtnPackageTypeEdit_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbPackageTypes.SelectedIndex;

		if (SelectedIndex > -1)
		{
			BtnPackageTypeClear_Click(null, null);
			Model.IsEditingPackageType = true;
			Model.IsPackageTypeTextBoxReadOnly = false;

			var pts = Model.PackageTypeObjects;

			var pt = pts[SelectedIndex];

			Logging.WriteLogLine("Editing Package Type: " + pt.Description);
			Model.CurrentlyEditedPackageType = pt;

			Model.SelectedPackageTypeName = pt.Description;

			// Model.SelectedServiceLevelStartTime = sl.StartTime.ToString();
			// Model.SelectedServiceLevelEndTime = sl.EndTime.ToString();
		}
	}

	private void BtnPackageTypeNew_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Adding PackageType");
		Model.IsEditingPackageType = true;
		Model.IsPackageTypeTextBoxReadOnly = false;
	}

	private void BtnPackageTypeDel_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbPackageTypes.SelectedIndex;

		if (SelectedIndex > -1)
		{
			var pt = Model.PackageTypeObjects[SelectedIndex];

			var title = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDeleteTitle");

			var message = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDelete");
			message = message.Replace("@1", pt.Description);
			var response = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);

			if (response == MessageBoxResult.Yes)
				Model.DeletePackageTypeLocally(pt);
		}
	}

	private void BtnPackageTypeUp_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbPackageTypes.SelectedIndex;

		if (SelectedIndex > 0)
		{
			var pt = Model.PackageTypeObjects[SelectedIndex];
			Model.MovePackageTypeLocally(pt, SelectedIndex - 1);
		}
	}

	private void BtnPackageTypeDown_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbPackageTypes.SelectedIndex;

		if ((SelectedIndex > -1) && (SelectedIndex < (lbPackageTypes.Items.Count - 1)))
		{
			var pt = Model.PackageTypeObjects[SelectedIndex];
			Model.MovePackageTypeLocally(pt, SelectedIndex + 1);
		}
	}

	private void LbPackageTypes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	{
		BtnPackageTypeEdit_Click(null, null);
	}

	private void BtnPackageTypeSave_Click(object sender, RoutedEventArgs e)
	{
		Logging.WriteLogLine("Saving Package Types");
		Model.SavePackageTypes();
	}
	#endregion

	#region Zone Handlers
	private void BtnZoneEdit_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbZones.SelectedIndex;

		if (SelectedIndex > -1)
		{
			//this.BtnPackageTypeClear_Click(null, null);
			Model.SelectedZoneName = string.Empty;
			Model.SelectedZoneAbbreviation = string.Empty;
			Model.IsEditingZone = true;

			var zones = Model.Zones;

			var zone = zones[SelectedIndex];

			Logging.WriteLogLine("Editing Zone: " + zone.Name);
			Model.CurrentlyEditedZone = zone;

			Model.SelectedZoneName = zone.Name;
			Model.SelectedZoneAbbreviation = zone.Abbreviation;
			Model.SelectedZoneSortIndex = zone.SortIndex;

			Model.IsZoneTextBoxReadOnly = false;

			tbZoneName.Focus();
			ToggleZoneListButtons(false);
		}
	}

	private void BtnZoneNew_Click(object sender, RoutedEventArgs e)
	{
		BtnZoneClear_Click(null, null);
		ToggleZoneListButtons(false);
		tbZoneName.Focus();

		Model.IsNewZone = true;
		Model.IsZoneTextBoxReadOnly = false;
		Model.IsEditingZone = true;
	}

	private void BtnZoneDel_Click(object sender, RoutedEventArgs e)
	{
		ToggleZoneListButtons(true);

		//var selectedIndex = lbZones.SelectedIndex;
		if (lbZones.SelectedItem != null)
		{
			//if (selectedIndex > -1)
			var title = (string)Application.Current.TryFindResource("RateWizardTabZoneDeleteTitle");

			var message = (string)Application.Current.TryFindResource("RateWizardTabZoneDelete");
			message = message.Replace("@1", ((Zone)lbZones.SelectedItem).Name);
			var response = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);

			if (response == MessageBoxResult.Yes)
				Model.DeleteZoneLocally((RateWizardModel.EditZone)lbZones.SelectedItem);
		}
	}

	private void BtnZoneUp_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbZones.SelectedIndex;

		if (SelectedIndex > 0)
		{
			var zone = Model.Zones[SelectedIndex];
			Model.MoveZoneLocally(zone, SelectedIndex - 1);
		}
	}

	private void BtnZoneDown_Click(object sender, RoutedEventArgs e)
	{
		var SelectedIndex = lbZones.SelectedIndex;

		if (SelectedIndex < (lbZones.Items.Count - 1))
		{
			var Zone = Model.Zones[SelectedIndex];
			Model.MoveZoneLocally(Zone, SelectedIndex + 1);
		}
	}

	private void LbZones_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	{
		BtnZoneEdit_Click(null, null);
	}

	private void BtnZoneUpdate_Click(object sender, RoutedEventArgs e)
	{
		if (Model.SelectedZoneName.IsNotNullOrWhiteSpace() && Model.SelectedZoneAbbreviation.IsNotNullOrWhiteSpace())
		{
			var error = Model.AddUpdateZoneLocally();

			if (error.IsNullOrWhiteSpace())
			{
				//ToggleZoneListButtons(true);
				BtnZoneCancel_Click(null, null);
				Model.SetArePendingChanges(true);
				tbZoneName.Background = Brushes.White;
				tbZoneAbbreviation.Background = Brushes.White;
			}
			else
			{
				tbZoneName.Focus();
				var title = (string)Application.Current.TryFindResource("RateWizardTabZoneErrorTitle");
				MessageBox.Show(error, title, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		else
		{
			if (Model.SelectedZoneName.IsNullOrWhiteSpace())
			{
				tbZoneName.Focus();
				tbZoneName.Background = Brushes.Yellow;
			}
			else
				tbZoneName.Background = Brushes.White;

			if (Model.SelectedZoneAbbreviation.IsNullOrWhiteSpace())
			{
				tbZoneAbbreviation.Focus();
				tbZoneAbbreviation.Background = Brushes.Yellow;
			}
			else
				tbZoneAbbreviation.Background = Brushes.White;
		}
	}

	private void BtnZoneCancel_Click(object sender, RoutedEventArgs e)
	{
		Model.CurrentlyEditedZone = null;

		Model.SelectedZoneName = string.Empty;
		Model.SelectedZoneAbbreviation = string.Empty;
		Model.IsEditingZone = false;
		Model.IsNewZone = false;

		Model.IsZoneTextBoxReadOnly = true;
		ToggleZoneListButtons(true);

		tbZoneName.Background = Brushes.White;
		tbZoneAbbreviation.Background = Brushes.White;
	}

	private void BtnZoneClear_Click(object sender, RoutedEventArgs e)
	{
		Model.CurrentlyEditedZone = null;

		Model.SelectedZoneName = string.Empty;
		Model.SelectedZoneAbbreviation = string.Empty;

		//Model.IsEditingZone = false;
	}

	//private void BtnZonesSave_Click(object sender, RoutedEventArgs e)
	//{

	//}

	private void ToggleZoneListButtons(bool isEnabled)
	{
		btnZoneEdit.IsEnabled = isEnabled;
		btnZoneNew.IsEnabled = isEnabled;
		btnZoneDel.IsEnabled = isEnabled;
		btnZoneUp.IsEnabled = isEnabled;
		btnZoneDown.IsEnabled = isEnabled;
	}
	#endregion

	#region Price Matrix Handlers
	private string oldValue = string.Empty;

	private void MakeEditable(object sender, MouseButtonEventArgs e)
	{
		if (sender is TextBox tb)
		{
			tb.IsReadOnly = false;
			oldValue = tb.Text;
			tb.Background = Brushes.LightGoldenrodYellow;
			tb.Focus();
		}
	}

	private new void PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (sender is TextBox tb)
		{
			if (e.Key == Key.Enter)
			{
				tb.IsReadOnly = true;
				tb.Background = Brushes.White;
				ActionDecimalValue.Focus();

				UpdatePriceMatrixCell(tb.Name, tb.Text);
			}
			else if (e.Key == Key.Escape)
			{
				tb.IsReadOnly = true;
				tb.Text = oldValue;
				tb.Background = Brushes.White;
				ActionDecimalValue.Focus();
			}
		}
	}

	private static void TextBox_TextChanged(object sender, TextChangedEventArgs e)
	{
		var s = Regex.Replace(((TextBox)sender).Text, @"[^\d.-]", "");
		((TextBox)sender).Text = s;
	}

	private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if (!Model.IsPriceMatrixLocked)
		{
			if (sender is ComboBox cb)
				Logging.WriteLogLine("DEBUG cb " + cb.Name + " has changed: " + cb.Text);
			Model.ArePendingRateMatricies = true;
			Model.SetArePendingChanges(true);
		}
	}

	private new void LostFocus(object sender, RoutedEventArgs e)
	{
		if (sender is TextBox tb)
		{
			//tb.Text = oldValue;
			tb.Background = Brushes.White;
			ActionDecimalValue.Focus();
		}
	}

	private void UpdatePriceMatrixCell(string cellName, string value)
	{
		foreach (var tb in FindVisualChildren<TextBox>(GridPriceMatrix))
		{
			if (tb.Name == cellName)
			{
				var ok = decimal.TryParse(value.Trim(), out var test);

				if (!ok)
				{
					Logging.WriteLogLine("Invalid number in cellName" + cellName + ": " + value);
					value = "0";
				}

				//Logging.WriteLogLine("DEBUG Setting " + cellName + " to " + value);
				tb.Text = value;

				if (DataContext is RateWizardModel Model && !Model.IsPriceMatrixLocked)
				{
					//Model.ArePendingRateMatricies = true;
					Model.SetArePendingRateMatricies(true);
					Model.SetArePendingChanges(true);
				}
				break;
			}
		}
	}

	public bool CheckMatrixForModifications(List<LocalRateMatrixItem> items)
	{
		var Modified = Model.IsFixedPriceSelected ? CheckFixedPriceMatrixForModifications(items) : CheckLanePricingMatrixForModifications(items);
		return Modified;
	}

	public bool CheckFixedPriceMatrixForModifications(List<LocalRateMatrixItem> items)
	{
		var modified = false;

		foreach (var tb in FindVisualChildren<TextBox>(GridPriceMatrix))
		{
			var item = (from I in items
						where I.Name == tb.Name
						select I).FirstOrDefault();

			if (item != null)
			{
				//if (item.Value != 0)
				//{
				//    if (tb.Text.Trim() != (item.Value + ""))
				//    {
				//        Logging.WriteLogLine("DEBUG tb " + tb.Name + " is modified - Value was " + item.Value + " now " + tb.Text.Trim());
				//        modified = true;
				//        break;
				//    }
				//}
				if (tb.Text.Trim() != (item.Value + ""))
				{
					Logging.WriteLogLine("DEBUG tb " + tb.Name + " is modified - Value was " + item.Value + " now " + tb.Text.Trim());
					modified = true;
					break;
				}

				if (item.Formula != string.Empty)
				{
					if (tb.Text.Trim() != item.Formula)
					{
						Logging.WriteLogLine("DEBUG tb " + tb.Name + " is modified - Formula was " + item.Formula + " now " + tb.Text.Trim());
						modified = true;
						break;
					}
				}
			}
			else
			{
				// Empty rate matrix
				if (tb.Text.Trim() != "0.00")
				{
					Logging.WriteLogLine("DEBUG tb " + tb.Name + " is modified - Value was 0.00 now " + tb.Text.Trim());
					modified = true;
					break;
				}
			}
		}

		return modified;
	}

	public void BuildPriceMatrix_V2(Grid grid, bool isFixedPrice)
	{
		var zones = Model.Zones; // Create a grid zones.Count x zones.Count

		//Logging.WriteLogLine("DEBUG zones.Count: " + zones.Count);
		// Header and left
		grid.RowDefinitions.Add(new RowDefinition());
		grid.ColumnDefinitions.Add(new ColumnDefinition());

		for (var i = 0; i < zones.Count; i++)
		{
			grid.RowDefinitions.Add(new RowDefinition());
			grid.ColumnDefinitions.Add(new ColumnDefinition());
		}

		//Logging.WriteLogLine("DEBUG RowDefinitions.Count: " + grid.RowDefinitions.Count);
		//Logging.WriteLogLine("DEBUG ColumnDefinitions.Count: " + grid.ColumnDefinitions.Count);

		// Now build the labels
		// 1st column
		for (var i = 0; i < zones.Count; i++)
		{
			Label colLabel = new()
			{
				Content = zones[i].Name,
				FontWeight = FontWeights.Bold,
				Name = "leftcol_" + i
			};

			Border border = new()
			{
				BorderBrush = Brushes.Black,
				BorderThickness = new Thickness(0, 0, 1, 1)
			};
			border.Child = colLabel;
			grid.Children.Add(border);
			Grid.SetColumn(border, 0);
			Grid.SetRow(border, i + 1);
		}

		// 1st row
		List<Zone> reversed = new();

		foreach (var zone in zones)
			reversed.Add(zone);
		reversed.Reverse();

		//for (int i = reversed.Count - 1; i >= 0; i--)
		for (var i = 0; i < reversed.Count; i++)
		{
			Label rowLabel = new()
			{
				Content = reversed[i].Name,
				FontWeight = FontWeights.Bold,
				Name = "toprow_" + i
			};

			Border border = new()
			{
				BorderBrush = Brushes.Black,
				BorderThickness = new Thickness(1, 0, 1, 0)
			};
			border.Child = rowLabel;
			grid.Children.Add(border);
			Grid.SetColumn(border, i + 1);
			Grid.SetRow(border, 0);
		}

		// Only 
		//string text = populateCells ? "0.00" : "";
		// Skip the header
		for (var i = 1; i < grid.RowDefinitions.Count; i++)
		{
			// Skip the left column
			for (var j = 1; j < grid.ColumnDefinitions.Count; j++)
			{
				if (j <= (zones.Count - (i - 1)))
				{
					if (isFixedPrice)
					{
						TextBox tb = new()
						{
							Text = "0.00",
							IsReadOnly = true,
							VerticalContentAlignment = VerticalAlignment.Center,
							Name = "xy_" + j + "_" + i // x,y
						};
						tb.MouseDoubleClick += MakeEditable;
						tb.PreviewKeyDown += PreviewKeyDown;
						tb.LostFocus += LostFocus;
						tb.TextChanged += TextBox_TextChanged;

						Border border = new()
						{
							BorderBrush = Brushes.Black,
							BorderThickness = new Thickness(0, 0, 1, 1)
						};
						border.Child = tb;
						grid.Children.Add(border);
						Grid.SetColumn(border, j);
						Grid.SetRow(border, i);
					}
					else
					{
						ComboBox cb = new()
						{
							Name = "xy_" + j + "_" + i, // x,y
							ItemsSource = Model.Lanes,
							Background = Brushes.White,
							Foreground = Brushes.Black,
							BorderBrush = Brushes.White,
							BorderThickness = new Thickness(1)
						};

						Border border = new()
						{
							BorderBrush = Brushes.Black,
							BorderThickness = new Thickness(0, 0, 1, 1)
						};
						border.Child = cb;
						grid.Children.Add(border);
						Grid.SetColumn(border, j);
						Grid.SetRow(border, i);
					}
				}
				else
				{
					Label label = new()
					{
						Background = Brushes.DarkGray
					};
					grid.Children.Add(label);
					Grid.SetColumn(label, j);
					Grid.SetRow(label, i);
				}
			}
		}

		//Model.PopulateLocalRates();

		Model.PreviousPackageTypeForMatrix = Model.SelectedPackageTypeForMatrix;
		Model.PreviousServiceLevelForMatrix = Model.SelectedServiceLevelForMatrix;
	}

	public void BuildPriceMatrix(Grid grid, bool populateCells, bool editable = true)
	{
		var zones = Model.Zones; // Create a grid zones.Count x zones.Count

		//Logging.WriteLogLine("DEBUG zones.Count: " + zones.Count);
		// Header and left
		grid.RowDefinitions.Add(new RowDefinition());
		grid.ColumnDefinitions.Add(new ColumnDefinition());

		for (var i = 0; i < zones.Count; i++)
		{
			grid.RowDefinitions.Add(new RowDefinition());
			grid.ColumnDefinitions.Add(new ColumnDefinition());
		}

		//Logging.WriteLogLine("DEBUG RowDefinitions.Count: " + grid.RowDefinitions.Count);
		//Logging.WriteLogLine("DEBUG ColumnDefinitions.Count: " + grid.ColumnDefinitions.Count);

		// Now build the labels
		// 1st column
		for (var i = 0; i < zones.Count; i++)
		{
			if (zones[i].Name.IsNotNullOrWhiteSpace())
			{
				Label colLabel = new()
				{
					Content = zones[i].Name,
					FontWeight = FontWeights.Bold,
					Name = "leftcol_" + i
				};

				Border border = new()
				{
					BorderBrush = Brushes.Black,
					BorderThickness = new Thickness(0, 0, 1, 1)
				};
				border.Child = colLabel;
				grid.Children.Add(border);
				Grid.SetColumn(border, 0);
				Grid.SetRow(border, i + 1);
			}
			else
				Logging.WriteLogLine("Excluded zone with empty name - Id: " + zones[i].Id);
		}

		// 1st row
		List<Zone> reversed = new();

		foreach (var zone in zones)
			reversed.Add(zone);
		reversed.Reverse();

		//for (int i = reversed.Count - 1; i >= 0; i--)
		for (var i = 0; i < reversed.Count; i++)
		{
			Label rowLabel = new()
			{
				Content = reversed[i].Name,
				FontWeight = FontWeights.Bold,
				Name = "toprow_" + i
			};

			Border border = new()
			{
				BorderBrush = Brushes.Black,
				BorderThickness = new Thickness(1, 0, 1, 0)
			};
			border.Child = rowLabel;
			grid.Children.Add(border);
			Grid.SetColumn(border, i + 1);
			Grid.SetRow(border, 0);
		}

		// Only 
		var text = populateCells ? "0.00" : "";

		// Skip the header
		for (var i = 1; i < grid.RowDefinitions.Count; i++)
		{
			// Skip the left column
			for (var j = 1; j < grid.ColumnDefinitions.Count; j++)
			{
				if (j <= (zones.Count - (i - 1)))
				{
					var name = Model.MakeRateMatrixCellName(j, i);

					if (populateCells)
					{
						TextBox tb = new()
						{
							Text = text,
							IsReadOnly = true,
							VerticalContentAlignment = VerticalAlignment.Center,
							//Name                     = "xy_" + j + "_" + i, // x,y
							//ToolTip                  = "xy_" + j + "_" + i
							Name = name,
							ToolTip = name
						};

						if (editable)
						{
							tb.MouseDoubleClick += MakeEditable;
							tb.PreviewKeyDown += PreviewKeyDown;
							tb.LostFocus += LostFocus;
							tb.TextChanged += TextBox_TextChanged;
						}

						Border border = new()
						{
							BorderBrush = Brushes.Black,
							BorderThickness = new Thickness(0, 0, 1, 1)
						};
						border.Child = tb;
						grid.Children.Add(border);
						Grid.SetColumn(border, j);
						Grid.SetRow(border, i);
					}
					else
					{
						ComboBox cb = new()
						{
							//Name            = "xy_" + j + "_" + i, // x,y
							Name = name,
							ItemsSource = Model.Lanes,
							Background = Brushes.White,
							Foreground = Brushes.Black,
							BorderBrush = Brushes.White,
							BorderThickness = new Thickness(1),
							//ToolTip         = "xy_" + j + "_" + i
							ToolTip = name
						};

						cb.SelectionChanged += ComboBox_SelectionChanged;

						Border border = new()
						{
							BorderBrush = Brushes.Black,
							BorderThickness = new Thickness(0, 0, 1, 1),
							Child = cb
						};
						grid.Children.Add(border);
						Grid.SetColumn(border, j);
						Grid.SetRow(border, i);
					}
				}
				else
				{
					Label label = new()
					{
						Background = Brushes.DarkGray
					};
					grid.Children.Add(label);
					Grid.SetColumn(label, j);
					Grid.SetRow(label, i);
				}
			}
		}

		// Set 0,0 background to gray
		Label lbl = new()
		{
			Background = Brushes.DarkGray,
		};
		grid.Children.Add(lbl);
		Grid.SetColumn(lbl, 0);
		Grid.SetRow(lbl, 0);

		//Model.PopulateLocalRates();

		Model.PreviousPackageTypeForMatrix = Model.SelectedPackageTypeForMatrix;
		Model.PreviousServiceLevelForMatrix = Model.SelectedServiceLevelForMatrix;
	}

	public void BuildPriceMatrix_V1()
	{
		if (DataContext is RateWizardModel Model)
		{
			var zones = Model.Zones; // Create a grid zones.Count x zones.Count

			//Logging.WriteLogLine("DEBUG zones.Count: " + zones.Count);
			// Header and left
			GridPriceMatrix.RowDefinitions.Add(new RowDefinition());
			GridPriceMatrix.ColumnDefinitions.Add(new ColumnDefinition());

			for (var i = 0; i < zones.Count; i++)
			{
				GridPriceMatrix.RowDefinitions.Add(new RowDefinition());
				GridPriceMatrix.ColumnDefinitions.Add(new ColumnDefinition());
			}

			//Logging.WriteLogLine("DEBUG RowDefinitions.Count: " + GridPriceMatrix.RowDefinitions.Count);
			//Logging.WriteLogLine("DEBUG ColumnDefinitions.Count: " + GridPriceMatrix.ColumnDefinitions.Count);

			// Now build the labels
			// 1st column
			for (var i = 0; i < zones.Count; i++)
			{
				Label colLabel = new()
				{
					Content = zones[i].Name,
					FontWeight = FontWeights.Bold,
					Name = "leftcol_" + i
				};

				Border border = new()
				{
					BorderBrush = Brushes.Black,
					BorderThickness = new Thickness(0, 0, 1, 1),
					Child = colLabel
				};
				GridPriceMatrix.Children.Add(border);
				Grid.SetColumn(border, 0);
				Grid.SetRow(border, i + 1);
			}

			// 1st row
			List<Zone> reversed = new();

			foreach (var zone in zones)
				reversed.Add(zone);
			reversed.Reverse();

			//for (int i = reversed.Count - 1; i >= 0; i--)
			for (var i = 0; i < reversed.Count; i++)
			{
				Label rowLabel = new()
				{
					Content = reversed[i].Name,
					FontWeight = FontWeights.Bold,
					Name = "toprow_" + i
				};

				Border border = new()
				{
					BorderBrush = Brushes.Black,
					BorderThickness = new Thickness(1, 0, 1, 0),
					Child = rowLabel
				};
				GridPriceMatrix.Children.Add(border);
				Grid.SetColumn(border, i + 1);
				Grid.SetRow(border, 0);
			}

			// Skip the header
			for (var i = 1; i < GridPriceMatrix.RowDefinitions.Count; i++)
			{
				// Skip the left column
				for (var j = 1; j < GridPriceMatrix.ColumnDefinitions.Count; j++)
				{
					if (j <= (zones.Count - (i - 1)))
					{
						TextBox tb = new()
						{
							Text = "0.00",
							IsReadOnly = true,
							VerticalContentAlignment = VerticalAlignment.Center,
							Name = "xy_" + j + "_" + i // x,y
						};
						tb.MouseDoubleClick += MakeEditable;
						tb.PreviewKeyDown += PreviewKeyDown;
						tb.LostFocus += LostFocus;

						Border border = new()
						{
							BorderBrush = Brushes.Black,
							BorderThickness = new Thickness(0, 0, 1, 1)
						};
						border.Child = tb;
						GridPriceMatrix.Children.Add(border);
						Grid.SetColumn(border, j);
						Grid.SetRow(border, i);
					}
					else
					{
						Label label = new()
						{
							Background = Brushes.DarkGray
						};
						GridPriceMatrix.Children.Add(label);
						Grid.SetColumn(label, j);
						Grid.SetRow(label, i);
					}
				}
			}

			//Model.PopulateLocalRates();

			Model.PreviousPackageTypeForMatrix = Model.SelectedPackageTypeForMatrix;
			Model.PreviousServiceLevelForMatrix = Model.SelectedServiceLevelForMatrix;
		}
	}

	public void SetMatrixToNewValue(decimal value)
	{
		var val = value.ToString("F2");

		foreach (var tb in FindVisualChildren<TextBox>(GridPriceMatrix))
			tb.Text = val;
	}

	public void SetMatrixAddValue(decimal value)
	{
		foreach (var tb in FindVisualChildren<TextBox>(GridPriceMatrix))
		{
			if ((tb.Text != string.Empty) && (tb.Text.Trim().Length > 0))
			{
				var oldValue = decimal.Parse(tb.Text);
				oldValue += value;
				tb.Text = oldValue.ToString("F2");
			}
		}
	}

	public void SetMatrixMultiplyValue(decimal value)
	{
		foreach (var tb in FindVisualChildren<TextBox>(GridPriceMatrix))
		{
			if ((tb.Text != string.Empty) && (tb.Text.Trim().Length > 0))
			{
				var oldValue = decimal.Parse(tb.Text);

				if (oldValue != 0)
				{
					oldValue *= value;
					tb.Text = oldValue.ToString("F2");
				}
			}
		}
	}

	public void PopulateMatrix(List<LocalRateMatrixItem> items, List<RateWizardModel.EditZone> zones)
	{
		// TODO Turned this off for the moment - not sure best way to handle it
		// Had a problem with a leftover RateMatrixItem in the db for a pair
		//int check = 0;
		//for (int i = 1; i <= zones.Count; i++ )
		//{
		//	check += i;
		//}
		//if( ( items != null ) && ( items.Count > 0 ) && items.Count >= check )
		if ((items != null) && (items.Count > 0))
		{
			//var tmp = ( from I in items
			//            where I.Value == 0
			//            select I ).ToList();
			//Logging.WriteLogLine( "DEBUG " + tmp?.Count + " LocalRateMatrixItems have a value of 0" );

			//LocalRateMatrixItem item = items[0];
			var tmp = (from I in items
					   where I.Formula.IsNotNullOrWhiteSpace()
					   select I).ToList();

			//if( items[ 0 ].Formula.IsNullOrWhiteSpace() )
			//if (tmp?.Count > 0 )
			if (tmp.Count == 0)
				PopulateMatrixFixedRates(items, zones);
			else
			{
				TcPricing.SelectedIndex = 1;

				//PopulateMatrixLanePricing(items, zones);
				GridLanePricingMatrix.Visibility = Visibility.Hidden;
				var values = PopulateMatrixLanePricing(items, zones);

				//!!!This works!!! But not in PopulateMatrixLanePricing
				foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
				{
					var name = cb.Name; // xy_12_14

					if ((name != null) && name.StartsWith("xy_"))
					{
						if (values.ContainsKey(name) && (values[name]?.Value > 0))
						{
							var value = values[name].Value + "";
							cb.Text = value;
							cb.SelectedIndex = (int)decimal.Parse(value);
						}
					}
				}

				GridLanePricingMatrix.Visibility = Visibility.Visible;
				GridLanePricingMatrix.InvalidateVisual();
			}
		}
		else
		{
			if (DataContext is RateWizardModel Model)
			{
				Logging.WriteLogLine("Populating empty matrix for " + (Model.IsFixedPriceSelected ? " Fixed Prices" : " Lane Pricing"));
				// Clear both
				PopulateMatrixFixedRates(null, zones);
				PopulateMatrixLanePricing(null, zones);

				Model.IsFixedPriceSelected = true;
				//if( Model.IsFixedPriceSelected )
				//	PopulateMatrixFixedRates( null, zones );
				//else
				//	PopulateMatrixLanePricing( null, zones );
			}
		}
	}

	public void PopulateMatrixFixedRates(List<LocalRateMatrixItem> items, List<RateWizardModel.EditZone> zones)
	{
		GridPriceMatrix.Visibility = Visibility.Hidden;

		var ZoneNames = (from Z in zones
						 select Z.Name).ToList();

		var Reversed = (from Z in zones
						select Z.Name).ToList();
		Reversed.Reverse();

		// cellName, item
		Dictionary<string, LocalRateMatrixItem> Values = new();

		if (items is { Count: > 0 })
		{
			var Sorted = (from L in items
						  orderby L.Name
						  select L).ToList();

			foreach (var Item in Sorted)
			{
				var FromZone = Item.FromZone; // zoneNames[]
				var ToZone = Item.ToZone;   // reversed[]

				// Need to offset for the 1st column and row
				//string name = "xy_" + (zoneNames.IndexOf(fromZone) + 1) + "_" + (reversed.IndexOf(toZone) + 1);
				var Key = "xy_" + (Reversed.IndexOf(ToZone) + 1) + "_" + (ZoneNames.IndexOf(FromZone) + 1);
				Values.Add(Key, Item);

				//Logging.WriteLogLine("DEBUG values.Add: name: " + name + " value: " + item.Value);
			}
		}

		GridPriceMatrix.Visibility = Visibility.Hidden;

		foreach (var Tb in FindVisualChildren<TextBox>(GridPriceMatrix))
		{
			var Key = Tb.Name; // xy_12_14

			if ((Key != null) && Key.StartsWith("xy_") && Values.ContainsKey(Key))
			{
				//Logging.WriteLogLine("DEBUG tb.Name: " + tb.Name + " " + values[name].Value);
				//tb.Text = values.ContainsKey(name)
				//    ? values[name].Formula.IsNotNullOrWhiteSpace() ? values[name].Formula : values[name].Value + ""
				//    : "0.00";
				//tb.Text = values.ContainsKey(name) ? values[name].Value + "" : "0.00";
				//tb.Text = values[ name ].Value + "";
				// tb.Text = string.Format("{0:C}", values[name].Value);
				Tb.Text = Values[Key].Value.ToString("F2");
			}
			else
			{
				//Logging.WriteLogLine( "DEBUG Setting " + name + " to 0.00" );
				Tb.Text = "0.00";
			}

			//if (name != null && name.StartsWith("xy_"))
			//{
			//    //tb.Text = values.ContainsKey(name)
			//    //    ? values[name].Formula.IsNotNullOrWhiteSpace() ? values[name].Formula : values[name].Value + ""
			//    //    : "0.00";
			//    tb.Text = values.ContainsKey(name) ? values[name].Value + "" : "0.00";
			//}
		}

		GridPriceMatrix.Visibility = Visibility.Visible;
		GridPriceMatrix.InvalidateVisual();

		TcPricing.SelectedIndex = 0;
	}

	public List<LocalRateMatrixItem> GatherValuesForFixedPrice(List<RateWizardModel.EditZone> zones)
	{
		List<LocalRateMatrixItem> Values = new();

		if (DataContext is RateWizardModel Model)
		{
			var ZoneNames = (from Z in zones
							 select Z.Name).ToList();

			var Reversed = (from Z in zones
							select Z.Name).ToList();
			Reversed.Reverse();

			foreach (var Tb in FindVisualChildren<TextBox>(GridPriceMatrix))
			{
				var TbName = Tb.Name; // xy_12_14
				var Value = Tb.Text.Trim();

				//Logging.WriteLogLine("\tDEBUG tb: " + name + " -> " + value);
				var Pieces = TbName.Split('_');

				if (Pieces.Length == 3)
				{
					var X = int.Parse(Pieces[1]);
					var Y = int.Parse(Pieces[2]);

					// Note - have to adjust for first column and first row
					// x comes from the reversed zones
					var XName = Reversed[X - 1];

					//string xName = reversed[x + 1];
					// y comes from the zones
					var YName = ZoneNames[Y - 1];

					//string yName = zoneNames[y] + 1;

					// Write name in form of yName->xName
					var CellName = FormatRateName(XName, YName);

					//Logging.WriteLogLine("\tDEBUG tb: " + name + "->" + value + " cellName: " + cellName);
					//Dictionary<string, decimal> dict = new();
					//dict.Add(cellName, decimal.Parse(value));
					//values.Add(dict);
					var Worked = decimal.TryParse(Value, out var Result);

					if (!Worked)
						Result = 0;

					LocalRateMatrixItem Item = new()
					{
						Name = CellName,
						FromZone = YName,
						ToZone = XName,
						PackageType = Model.SelectedPackageTypeForMatrix,
						ServiceLevel = Model.SelectedServiceLevelForMatrix,

						//Value = decimal.Parse(value),
						Value = Result,
						Formula = string.Empty
					};
					Values.Add(Item);
				}
			}
		}

		return Values;
	}

	private static string FormatRateName(string xName, string yName)
	{
		var RateName = yName + "->" + xName;
		return RateName;
	}

	//public IEnumerable<T> FindVisualChildren<T>([NotNull] this DependencyObject parent) where T : DependencyObject
	//private IEnumerable<T> FindVisualChildren<T>(DependencyObject parent) where T : DependencyObject
	//{
	//	if (parent == null)
	//		throw new ArgumentNullException(nameof(parent));

	//	var queue = new Queue<DependencyObject>(new[] { parent });

	//	while (queue.Any())
	//	{
	//		var reference = queue.Dequeue();
	//		var count = VisualTreeHelper.GetChildrenCount(reference);

	//		for (var i = 0; i < count; i++)
	//		{
	//			var child = VisualTreeHelper.GetChild(reference, i);
	//			if (child is T children)
	//				yield return children;

	//			queue.Enqueue(child);
	//		}
	//	}
	//}

	private IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
	{
		if (depObj != null)
		{
			for (var I = 0; I < VisualTreeHelper.GetChildrenCount(depObj); I++)
			{
				var Child = VisualTreeHelper.GetChild(depObj, I);

				if (Child is T Children)
					yield return Children;

				foreach (var ChildOfChild in FindVisualChildren<T>(Child))
					yield return ChildOfChild;
			}
		}
	}

	private void CbPackageTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if (DataContext is RateWizardModel Model)
		{
			Model.PreviousPackageTypeForMatrix = Model.SelectedPackageTypeForMatrix;
			Model.SelectedPackageTypeForMatrix = (string)CbPackageTypes.SelectedItem;
		}
	}

	private void CbServiceLevels_SelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if (DataContext is RateWizardModel Model)
		{
			Model.PreviousServiceLevelForMatrix = Model.SelectedServiceLevelForMatrix;
			Model.SelectedServiceLevelForMatrix = (string)CbServiceLevels.SelectedItem;
		}
	}

	private void BtnLockUnlock_Click(object sender, RoutedEventArgs e)
	{
		if (DataContext is RateWizardModel Model)
		{
			MessageBoxResult Mbr;

			if (Model.IsPriceMatrixLocked)
			{
				var Caption = FindStringResource("RateWizardTabPriceMatrixDialogUnlockTitle");
				var Message = FindStringResource("RateWizardTabPriceMatrixDialogUnlockMessage") + "\n" + FindStringResource("RateWizardTabPriceMatrixDialogUnlockMessage2");
				Mbr = MessageBox.Show(Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question);

				if (Mbr == MessageBoxResult.Yes)
				{
					Model.IsPriceMatrixLocked = !Model.IsPriceMatrixLocked;
					Logging.WriteLogLine("Setting IsPriceMatrixLocked to " + Model.IsPriceMatrixLocked);
				}
			}
			else
			{
				if (Model.ArePendingRateMatricies)
				{
					var Caption = FindStringResource("RateWizardTabPriceMatrixDialogLockTitle");
					var Message = FindStringResource("RateWizardTabPriceMatrixDialogLockMessage") + "\n" + FindStringResource("RateWizardTabPriceMatrixDialogLockMessage2");
					Mbr = MessageBox.Show(Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question);

					if (Mbr == MessageBoxResult.No)
					{
						//Model.IsPriceMatrixLocked = true;
						//Logging.WriteLogLine("Setting IsPriceMatrixLocked to " + Model.IsPriceMatrixLocked);
					}
				}

				Model.ArePendingRateMatricies = false;
				Model.SetArePendingChanges(Model.ArePendingRateMatricies);
				Model.IsPriceMatrixLocked = !Model.IsPriceMatrixLocked;
				Logging.WriteLogLine("Setting IsPriceMatrixLocked to " + Model.IsPriceMatrixLocked);
			}

			//if (mbr == MessageBoxResult.Yes)
			//{
			//    Model.IsPriceMatrixLocked = !Model.IsPriceMatrixLocked;
			//    Logging.WriteLogLine("Setting IsPriceMatrixLocked to " + Model.IsPriceMatrixLocked);
			//}
		}
	}

	private void BtnInitializeFromPair_Click(object sender, RoutedEventArgs e)
	{
		if (DataContext is RateWizardModel Model)
		{
			var Mbr = MessageBoxResult.Yes;

			// Only ask if the current pair has been modified
			var name = Model.SelectedPackageTypeName + "->" + Model.SelectedServiceLevelForMatrix;

			//if (Model.SdModifiedRates.ContainsKey(name)) {
			if (Model.ModifiedRateNames.Contains(name))
			{
				var title = FindStringResource("RateWizardTabPriceMatrixOverwritePairValuesTitle");
				var message = FindStringResource("RateWizardTabPriceMatrixOverwritePairValuesMessage") + "\n" + FindStringResource("RateWizardTabPriceMatrixOverwritePairValuesMessage2");
				Mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
			}

			if (Mbr == MessageBoxResult.Yes)
			{
				var selectedPair = CbActivePairs.Text;
				Model.InitializeCurrentPairFromOther(selectedPair);

				if (!Model.IsPriceMatrixLocked)
				{
					Model.ArePendingRateMatricies = true;
					Model.SetArePendingChanges(true);
				}
			}
		}
	}

	#region Lane Pricing
	private void BtnInitializeToLane_Click(object sender, RoutedEventArgs e)
	{
		foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
			cb.SelectedItem = CbInitializeToLane.SelectedItem;

		if (DataContext is RateWizardModel { IsPriceMatrixLocked: false } Model)
		{
			Model.ArePendingRateMatricies = true;

			Dispatcher.Invoke(() =>
							   {
								   Model.SetArePendingChanges(true);
							   });
		}
	}

	private void EnsureDecimal(object sender, TextCompositionEventArgs e)
	{
		var ToCheck = e.Text.Substring(e.Text.Length - 1);
		const string DECIMAL_CHAR = ".";

		if (!char.IsDigit(e.Text, e.Text.Length - 1) && !ToCheck.Equals(DECIMAL_CHAR))
			e.Handled = true;
	}

	public void BuildGridLanePricing()
	{
		if (DataContext is RateWizardModel Model)
		{
			if (Model.DictLanes.Count > 0)
			{
				lock (Model.DictLanes.Keys)
				{
					var tmp = Model.DictLanes.Keys;
					List<string> keys = new();

					foreach (var key in tmp)
						keys.Add(key);
					Logging.WriteLogLine("DEBUG found " + keys.Count + " keys in Model.DictLanes");

					//foreach( var key in Model.DictLanes.Keys )
					foreach (var key in keys)
					{
						var rowNumber = GridLanePricing.RowDefinitions.Count + 1;
						BtnAddLane_Click(null, null);

						//foreach (LocalRateMatrixItem item in Model.DictLanes[key])
						for (var i = 0; i < Model.DictLanes[key].Count; i++)
							BuildGridLanePricingCondition(rowNumber, i + 1, Model.DictLanes[key][i]);
					}
				}
			}
			else
			{
				var rowNumber = GridLanePricing.RowDefinitions.Count + 1;
				BtnAddLane_Click(null, null);
				BuildGridLanePricingCondition(rowNumber);
			}
		}
	}

	public void RebuildBuildLane(int rowNumber)
	{
		if (DataContext is RateWizardModel Model)
		{
			var key = rowNumber + "";

			if (Model.DictLanes.ContainsKey(key))
			{
				for (var i = 0; i < Model.DictLanes[key].Count; i++)
				{
					var item = Model.DictLanes[key][i];
					BuildGridLanePricingCondition(rowNumber, i + 1, item);

					//BuildGridLanePricingCondition(rowNumber, i + 1, Model.DictLanes[key][i]);
				}
			}
		}
	}

	//public void RebuildBuildLane(int rowNumber)
	//{
	//    if (DataContext is RateWizardModel Model)
	//    {
	//        if (Model.DictLanes.Count > 0)
	//        {
	//            foreach (string key in Model.DictLanes.Keys)
	//            {
	//                //BtnAddLane_Click(null, null);
	//                //foreach (LocalRateMatrixItem item in Model.DictLanes[key])
	//                for (int i = 0; i < Model.DictLanes[key].Count; i++)
	//                {
	//                    var item = Model.DictLanes[key][i];
	//                    BuildGridLanePricingCondition(rowNumber, i + 1, item);
	//                    //BuildGridLanePricingCondition(rowNumber, i + 1, Model.DictLanes[key][i]);
	//                }
	//            }
	//        }
	//    }
	//}

	public void BtnAddLane_Click(object sender, RoutedEventArgs e)
	{
		var rowNumber = GridLanePricing.RowDefinitions.Count + 1;

		if (DataContext is RateWizardModel Model)
		{
			Model.Lanes.Add(rowNumber + "");
			List<LocalRateMatrixItem> list = new();

			//list.Add(new LocalRateMatrixItem()
			//{
			//    Name = "Row_" + rowNumber + "_Col_" + 0
			//});
			Model.DictLanes.Add(rowNumber + "", list);
		}

		GridLanePricing.RowDefinitions.Add(new RowDefinition());

		if (GridLanePricing.ColumnDefinitions.Count == 0)
		{
			GridLanePricing.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
			GridLanePricing.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
		}

		Label lbl = new()
		{
			Name = "Lbl_Row_LaneNumber_" + rowNumber,
			Content = rowNumber + "",
			HorizontalAlignment = HorizontalAlignment.Left,
			VerticalContentAlignment = VerticalAlignment.Center,
			MinWidth = 20,
			FontWeight = FontWeights.Bold
		};

		GridLanePricing.Children.Add(lbl);
		Grid.SetColumn(lbl, 0);
		Grid.SetRow(lbl, rowNumber - 1);

		if (sender != null)
			BuildGridLanePricingCondition(rowNumber);
	}

	private void BtnRemoveLane_Click(object sender, RoutedEventArgs e)
	{
		if (GridLanePricing.RowDefinitions.Count > 1)
		{
			if (CbRemoveLane.SelectedIndex > 1)
				RemoveLane(int.Parse(CbRemoveLane.Text));
			else
			{
				var title = FindStringResource("RateWizardTabPriceMatrixButtonRemoveLaneErrorTitle");
				var message = FindStringResource("RateWizardTabPriceMatrixButtonRemoveLaneErrorMessage");
				MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
	}

	private void RemoveLane(int rowNumber, int startingRow = 1)
	{
		// Can't remove 1st row
		//int rowNumber = int.Parse(CbRemoveLane.Text);
		Logging.WriteLogLine($"Removing lane {rowNumber}");
		RateWizardModel model = null;

		if (DataContext is RateWizardModel m)
			model = m;

		var values = GatherValuesForLanePricing(model.Zones);

		// Remove all lanes and their children - leave the first
		//for (int row = 1; row < GridLanePricing.RowDefinitions.Count; row++)
		for (var row = startingRow; row < GridLanePricing.RowDefinitions.Count; row++)
		{
			List<StackPanel> toRemove = new();
			var rowSection = "Row_" + (row + 1);

			foreach (var sp in FindVisualChildren<StackPanel>(GridLanePricing))
			{
				if (sp.Name.Contains(rowSection))
				{
					var name = sp.Name;
					//Logging.WriteLogLine( "DEBUG Found StackPanel " + name );

					toRemove.Add(sp);
				}
			}

			if (toRemove.Count > 0)
			{
				for (var i = 0; i < toRemove.Count; i++)
				{
					toRemove[i].Children.Clear();
					GridLanePricing.Children.Remove(toRemove[i]);
				}
			}

			Label found = null;

			foreach (var lbl in FindVisualChildren<Label>(GridLanePricing))
			{
				if (lbl.Name.Contains("Lbl_Row_LaneNumber_" + rowNumber))
				{
					found = lbl;
					break;
				}
			}

			if (found != null)
				GridLanePricing.Children.Remove(found);
		}

		// 1st remove the entry that matches the removed row
		model.DictLanes.Remove(rowNumber + "");

		// Now rename the LocalRateMatrixItems to match the new rows
		for (var i = 1; i < model.DictLanes.Count; i++)
		{
			for (var j = 0; i < model.DictLanes[i + ""].Count; j++)
			{
				var name = model.DictLanes[i + ""][j].Name;

				//Logging.WriteLogLine("DEBUG Was: " + name);
				// "Lane_Row_" + rowNumber + "_Col_" + (i + 1);
				var pieces = name.Split('_');
				var newName = pieces[0] + "_" + pieces[1] + "_" + i + "_" + pieces[3] + "_" + pieces[4];
				model.DictLanes[i + ""][j].Name = newName;

				//Logging.WriteLogLine("DEBUG Now: " + newName);
			}
		}

		// Now for lanes
		model.Lanes.Clear();
		model.Lanes.Add(" ");
		model.Lanes.Add("1");

		if (GridLanePricing.RowDefinitions.Count > 2)
		{
			for (var i = 1; i < GridLanePricing.RowDefinitions.Count; i++)
				model.Lanes.Add(i + "");
		}

		if (!model.IsPriceMatrixLocked)
			model.SetArePendingRateMatricies(true);

		// Remove last row
		//GridLanePricing.RowDefinitions[GridLanePricing.RowDefinitions.Count - 1] = null;
		GridLanePricing.RowDefinitions.RemoveAt(GridLanePricing.RowDefinitions.Count - 1);

		// Now, restore the values in the grid
		SetLanePricingMatrixValues(values);
	}

	private void SetLanePricingMatrixValues(List<LocalRateMatrixItem> values)
	{
		if ((values != null) && (values.Count > 0) && DataContext is RateWizardModel model)
		{
			var zoneNames = (from Z in model.Zones
							 select Z.Name).ToList();

			var reversed = (from Z in model.Zones
							select Z.Name).ToList();
			reversed.Reverse();

			foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
			{
				var name = cb.Name; // xy_12_14
				var pieces = name.Split('_');

				if (pieces.Length == 3)
				{
					var x = int.Parse(pieces[1]);
					var y = int.Parse(pieces[2]);

					// Note - have to adjust for first column and first row
					// x comes from the reversed zones
					var xName = reversed[x - 1];

					// y comes from the zones
					var yName = zoneNames[y - 1];

					//Logging.WriteLogLine("\tDEBUG cb: " + name + "->" + value + " xName: " + xName + " yName: " + yName);

					// Write name in form of yName->xName
					var cellName = FormatRateName(xName, yName);

					var lrmi = (from L in values
								where L.Name == cellName
								select L).FirstOrDefault();

					if (lrmi != null)
					{
						var lane = (int)lrmi.Value;

						if (lane < cb.Items.Count)
							cb.SelectedIndex = lane;
					}
				}
			}
		}
	}

	private void BtnCopyLane_Click(object sender, RoutedEventArgs e)
	{
		if (DataContext is RateWizardModel Model)
		{
			//string lane = CbCopyLane.SelectedIndex + "";
			var lane = CbCopyLane.Text.Trim();
			BtnAddLane_Click(null, null);

			//string key = Model.DictLanes.Count - 1 + "";
			var key = Model.DictLanes.Count + "";
			Model.DictLanes[key].Clear();

			for (var i = 0; i < Model.DictLanes[lane].Count; i++)
			{
				var original = Model.DictLanes[lane][i];

				LocalRateMatrixItem condition = new()
				{
					Name = "Lane_Row_" + key + "_Col_" + (i + 1),
					FromZone = original.FromZone,
					PackageType = original.PackageType,
					ServiceLevel = original.ServiceLevel,
					ToZone = original.ToZone,
					Value = original.Value,
					Formula = original.Formula,
					VehicleType = original.VehicleType
				};
				Logging.WriteLogLine("DEBUG Copying " + original.Name + " to " + condition.Name);
				Model.DictLanes[key].Add(condition);
				BuildGridLanePricingCondition(Model.DictLanes.Count, i + 1, condition);
			}

			EnsureOnlyLastAddButtonEnabled(int.Parse(key), Model.DictLanes[lane].Count);

			if (!Model.IsPriceMatrixLocked)
				Model.SetArePendingRateMatricies(true);
		}
	}

	private void EnsureOnlyLastAddButtonEnabled(int row, int lastCol)
	{
		// Name = "Btn_Row_" + rowNumber + "_Col_" + conditionNumber + "_AddCondition",
		var start = "Btn_Row_" + row + "_Col_"; // + conditionNumber + "_AddCondition";
		var tag = "_AddCondition";
		var keep = "Btn_Row_" + row + "_Col_" + lastCol + "_AddCondition";

		foreach (var btn in FindVisualChildren<Button>(GridLanePricing))
		{
			if (btn.Name.StartsWith(start) && (btn.Name.IndexOf(tag) > -1) && (btn.Name != keep))
				btn.IsEnabled = false;
		}
	}

	private void BtnRemoveAllLanes_Click(object sender, RoutedEventArgs e)
	{
		var title = FindStringResource("RateWizardTabPriceMatrixButtonRemoveLanesTitle");

		var message = FindStringResource("RateWizardTabPriceMatrixButtonRemoveLanesMessage") + "\n"
																							   + FindStringResource("RateWizardTabPriceMatrixButtonRemoveLanesMessage2");
		var mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);

		if (mbr == MessageBoxResult.Yes)
			RemoveAllLanes();
	}

	private void RemoveAllLanes(bool AddLane = true)
	{
		if (DataContext is RateWizardModel Model)
		{
			if (!Model.IsPriceMatrixLocked && AddLane)
			{
				Model.ArePendingRateMatricies = true;
				Model.SetArePendingChanges(true);
			}

			////for (int i = 0; i < Model.Lanes.Count; i++)
			//for (int i = 1; i < Model.Lanes.Count; i++)
			//{
			//    RemoveLane(i, 0);
			//    //RemoveLane(i, 1);
			//}
			//string name = "Sp_Row_1_Col_1";
			//foreach (StackPanel sp in FindVisualChildren<StackPanel>(GridLanePricing))
			//{
			//    if (sp.Name == name)
			//    {
			//        foreach (TextBox tb in FindVisualChildren<TextBox>(GridLanePricing))
			//        {
			//            //tb.Text = string.Empty;
			//            GridLanePricing.Children.Remove(tb);
			//        }
			//        break;
			//    }
			//}

			List<StackPanel> sps = new();

			foreach (var sp in FindVisualChildren<StackPanel>(GridLanePricing))
			{
				sp.Children.Clear();
				sps.Add(sp);
			}

			foreach (var sp in sps)
				GridLanePricing.Children.Remove(sp);

			List<Label> toRemove = new();

			foreach (var label in FindVisualChildren<Label>(GridLanePricing))
				toRemove.Add(label);

			for (var i = 0; i < toRemove.Count; i++)
				//toRemove[i] = null;
				GridLanePricing.Children.Remove(toRemove[i]);
			GridLanePricing.RowDefinitions.Clear();

			//GridLanePricing.RowDefinitions.Add(new RowDefinition());

			Model.Lanes.Clear();
			Model.Lanes.Add(" ");

			Model.DictLanes.Clear();

			if (AddLane)
				BuildGridLanePricing();
		}
	}

	public void BtnEditFormula(object sender, RoutedEventArgs e)
	{
		if (sender is Button btn && DataContext is RateWizardModel Model)
		{
			var (rowNumber, colNumber) = GetRowAndColFromName(btn.Name);

			if (Model.DictLanes.ContainsKey(rowNumber + "") && (Model.DictLanes[rowNumber + ""].Count() >= (colNumber - 1)))
			{
				var currentFormula = Model.DictLanes[rowNumber + ""][colNumber - 1].Formula;

				var formula = new EditRateFormulaWindow(Application.Current.MainWindow).ShowEditRateFormulaWindow(currentFormula);

				if ((formula != string.Empty) && (formula != currentFormula))
				{
					//Logging.WriteLogLine("DEBUG formula was\n" + currentFormula + ", now\n" + formula);
					Model.DictLanes[rowNumber + ""][colNumber - 1].Formula = formula;

					// Name of textbox to update
					var name = "Tb_Row_" + rowNumber + "_Col_" + colNumber + "_Formula";

					foreach (var tb in FindVisualChildren<TextBox>(GridLanePricing))
					{
						if (tb.Name == name)
						{
							var pieces = formula.Split('\n');
							var tooltip = string.Empty;

							if (pieces != null)
							{
								foreach (var p in pieces)
								{
									if (p.Contains(EvaluateRateFormulaHelper.CUMULATIVE_BASE))
										continue;
									tooltip += p + "\n";
								}
							}

							//tb.Text    = formula.Substring( formula.IndexOf( "\n" ) + 1, 10 );
							tb.Text = tooltip.Substring(formula.IndexOf("\n") + 1, 10);
							//tb.ToolTip = formula;
							tb.ToolTip = tooltip;
							break;
						}
					}

					Model.BuildFullFormulae();
					//string rateName = Model.FormatRateName();
					//foreach (var rate in Model.SdRates[rateName])
					//{
					//	rate.Formula = Model.DictFullFormulas[rowNumber+""];
					//}

					Model.SetArePendingRateMatricies(true);
				}
			}
			else
			{
				var message = Model.DictLanes.ContainsKey(rowNumber + "") ? "Model.DictLanes.ContainsKey(" + rowNumber + ") is true" : "Model.DictLanes.ContainsKey(" + rowNumber + ") is false\n";

				if (Model.DictLanes.ContainsKey(rowNumber + ""))
					message += Model.DictLanes[rowNumber + ""].Count() >= (colNumber - 1) ? " Model.DictLanes[" + rowNumber + "].Count >= " + (colNumber - 1) : " Model.DictLanes[" + rowNumber + "].Count < " + (colNumber - 1);

				message += " rowNumber: " + rowNumber + ", colNumber: " + colNumber;
				Logging.WriteLogLine(message);
			}
		}
	}

	public void BtnRemoveCondition(object sender, RoutedEventArgs e)
	{
		if (sender is Button btn && DataContext is RateWizardModel Model)
		{
			var (rowNumber, colNumber) = GetRowAndColFromName(btn.Name);

			if (Model.DictLanes.ContainsKey(rowNumber + "") && (Model.DictLanes[rowNumber + ""].Count() >= (colNumber - 1)))
			{
				//Model.DictLanes[rowNumber + ""].Remove(Model.DictLanes[rowNumber + ""][colNumber]);
				Model.DictLanes[rowNumber + ""].RemoveAt(colNumber - 1);

				// Need to rename the items to match the new layout
				for (var i = 0; i < Model.DictLanes[rowNumber + ""].Count; i++)
					//Logging.WriteLogLine("DEBUG Was: " + Model.DictLanes[rowNumber + ""][i].Name);
					Model.DictLanes[rowNumber + ""][i].Name = "Lane_Row_" + rowNumber + "_Col_" + (i + 1);

				//Logging.WriteLogLine("DEBUG Now: " + Model.DictLanes[rowNumber + ""][i].Name);

				// Remove the conditions in the row
				List<StackPanel> toRemove = new();
				var rowSection = "Row_" + rowNumber;

				foreach (var sp in FindVisualChildren<StackPanel>(GridLanePricing))
				{
					if (sp.Name.Contains(rowSection))
					{
						//Logging.WriteLogLine("DEBUG Found StackPanel " + sp.Name);
						toRemove.Add(sp);
					}
				}

				if (toRemove.Count > 0)
				{
					for (var i = 0; i < toRemove.Count; i++)
					{
						toRemove[i].Children.Clear();
						GridLanePricing.Children.Remove(toRemove[i]);
					}
				}

				RebuildBuildLane(rowNumber);
				Model.SetArePendingRateMatricies(true);
			}
			else
			{
				var message = Model.DictLanes.ContainsKey(rowNumber + "") ? "Model.DictLanes.ContainsKey(" + rowNumber + ") is true" : "Model.DictLanes.ContainsKey(" + rowNumber + ") is false\n";

				if (Model.DictLanes.ContainsKey(rowNumber + ""))
					message += Model.DictLanes[rowNumber + ""].Count() >= (colNumber - 1) ? " Model.DictLanes[" + rowNumber + "].Count >= " + (colNumber - 1) : " Model.DictLanes[" + rowNumber + "].Count < " + (colNumber - 1);

				message += " rowNumber: " + rowNumber + ", colNumber: " + colNumber;
				Logging.WriteLogLine(message);
			}
		}
	}

	public void BtnAddCondition(object sender, RoutedEventArgs e)
	{
		//int rowNumber = GridLanePricing.RowDefinitions.Count + 1;
		//int rowNumber = GridLanePricing.RowDefinitions.Count;
		//int colNumber = GridLanePricing.ColumnDefinitions.Count + 1;
		if (sender is Button btn)
		{
			btn.IsEnabled = false; // Only the last button in the lane works
			var (rowNumber, colNumber) = GetRowAndColFromName(btn.Name);
			BuildGridLanePricingCondition(rowNumber, colNumber + 1);

			if (DataContext is RateWizardModel Model)
				Model.SetArePendingRateMatricies(true);
		}
	}

	private (int rowNumber, int colNumber) GetRowAndColFromName(string name)
	{
		var row = 0;
		var col = 0;

		// Btn_Row_0_Col_1_AddCondition
		var pieces = name?.Split('_');

		if ((pieces != null) && (pieces.Length > 5))
		{
			row = int.Parse(pieces[2]);
			col = int.Parse(pieces[4]);
		}

		return (row, col);
	}

	public void BuildGridLanePricingCondition(int rowNumber = 1, int conditionNumber = 1, LocalRateMatrixItem item = null, bool AddItem = true)
	{
		//if (GridLanePricing.ColumnDefinitions.Count > 0)
		if ((GridLanePricing.ColumnDefinitions.Count - 1) <= conditionNumber)
			GridLanePricing.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });

		StackPanel sp = new()
		{
			//Name = "Sp_Row_" + (rowNumber - 1) + "_Col_" + conditionNumber,
			Name = "Sp_Row_" + rowNumber + "_Col_" + conditionNumber,
			Orientation = Orientation.Horizontal,
			HorizontalAlignment = HorizontalAlignment.Left
		};

		Border border = new()
		{
			BorderBrush = Brushes.Black,
			BorderThickness = new Thickness(1),
			Margin = new Thickness(5, 0, 0, 0)
		};

		TextBox tb = new()
		{
			//Name = "Tb_Row_" + (rowNumber - 1) + "_Col_" + conditionNumber + "_Formula",
			Name = "Tb_Row_" + rowNumber + "_Col_" + conditionNumber + "_Formula",
			MinWidth = 50,
			IsReadOnly = true,
			VerticalContentAlignment = VerticalAlignment.Center
		};

		if ((item != null) && item.Formula.IsNotNullOrWhiteSpace())
		{
			//tb.Text = item.Formula;
			//tb.Text    = item.Formula.Substring( item.Formula.IndexOf( "\n" ) + 1, 8 );  // Remove Lane = n\n
			tb.Text = item.Formula.Substring(item.Formula.IndexOf("\n") + 1, 10); // Remove Total = 0\n
			tb.ToolTip = item.Formula;
		}
		border.Child = tb;
		sp.Children.Add(border);

		Button btn = new()
		{
			//Name = "Btn_Row_" + (rowNumber - 1) + "_Col_" + conditionNumber + "_Edit",
			Name = "Btn_Row_" + rowNumber + "_Col_" + conditionNumber + "_Edit",
			Content = FindStringResource("RateWizardTabPriceMatrixButtonEditCondition"),
			Padding = new Thickness(5, 0, 5, 0),
			Margin = new Thickness(5, 0, 0, 0),
			Background = Brushes.White,
			Foreground = Brushes.Black
		};
		btn.Click += BtnEditFormula;
		sp.Children.Add(btn);

		// Can't remove 1st condition
		if (conditionNumber > 1)
		{
			btn = new Button
			{
				//Name = "Btn_Row_" + (rowNumber - 1) + "_Col_" + conditionNumber + "_RemoveCondition",
				Name = "Btn_Row_" + rowNumber + "_Col_" + conditionNumber + "_RemoveCondition",
				Content = "-",
				ToolTip = FindStringResource("RateWizardTabPriceMatrixButtonRemoveConditionTooltip"),
				Padding = new Thickness(5, 0, 5, 0),
				Margin = new Thickness(5, 0, 0, 0)
			};
			btn.Click += BtnRemoveCondition;
			sp.Children.Add(btn);
		}

		btn = new Button
		{
			//Name = "Btn_Row_" + (rowNumber - 1) + "_Col_" + conditionNumber + "_AddCondition",
			Name = "Btn_Row_" + rowNumber + "_Col_" + conditionNumber + "_AddCondition",
			Content = "+",
			ToolTip = FindStringResource("RateWizardTabPriceMatrixButtonAddConditionTooltip"),
			Padding = new Thickness(5, 0, 5, 0),
			Margin = new Thickness(5, 0, 0, 0)
		};
		btn.Click += BtnAddCondition;
		sp.Children.Add(btn);

		GridLanePricing.Children.Add(sp);
		Grid.SetColumn(sp, conditionNumber + 1);
		Grid.SetRow(sp, rowNumber - 1);

		if ((item == null) && AddItem && DataContext is RateWizardModel Model)
		{
			item = new LocalRateMatrixItem
			{
				Name = "Lane_Row_" + rowNumber + "_Col_" + conditionNumber
			};

			//Logging.WriteLogLine("DEBUG Adding condition: " + item.Name);
			//Model.DictLanes[rowNumber - 1 + ""].Add(item);
			Model.DictLanes[rowNumber + ""].Add(item);
		}
	}

	private void BtnTest_Click(object sender, RoutedEventArgs e)
	{
		if (DataContext is RateWizardModel Model)
		{
			Model.BuildFullFormulae();

			if (Model.DictFullFormulas.Count > 0)
			{
				Dictionary<string, decimal> dictResults = new();
				//Logging.WriteLogLine( "DEBUG Calculating test results" );

				foreach (var key in Model.DictFullFormulas.Keys)
				{
					var piece = Model.DictFullFormulas[key];
					var isFixedPrice = !EvaluateRateFormulaHelper.IsFormula(piece);

					//Dictionary<string, string> dictVariables = new();
					//dictVariables.Add("Weight", "51");
					//dictVariables.Add("Total", "0");
					//FormulaResult fr = RunTerrysFormulaEvaluation(dictVariables, piece);

					var result = EvaluateRateFormulaHelper.EvaluateFullFormula(piece, Model.TestPieces, Model.TestWeight, Model.TestMileage, isFixedPrice);

					//Logging.WriteLogLine("DEBUG Result: " + result + "\nFormula: " + piece + "\npieces: " + Model.TestPieces + " weight: " + Model.TestWeight + "\n");
					dictResults.Add(key, result);
				}
				//Logging.WriteLogLine( "DEBUG Results calculated" );

				PopulateTestMatrixWithResults(dictResults);
			}
		}
	}

	private FormulaResult RunTerrysFormulaEvaluation(Dictionary<string, string> variables, string formula)
	{
		FormulaResult result = null;

		var errors = string.Empty;

		if (!ViewModelBase.IsInDesignMode)
		{
			Task.WaitAll(
						 Task.Run(async () =>
								   {
									   var Result = await Azure.Client.RequestEvaluateFormula(new EvaluateFormula
									   {
										   DataType = "TRIP",
										   Variables = variables,
										   Expression = formula
									   });

									   var result = Result.Value.ToString(CultureInfo.InvariantCulture);

									   var Errs = new StringBuilder();

									   foreach (var E in Result.ErrorList)
										   Errs.Append($"{E.Trim()}\r\n");

									   errors = Errs.ToString().Trim();
									   Logging.WriteLogLine(errors);
								   })
						);
		}

		return result;
	}

	private void PopulateTestMatrixWithResults(Dictionary<string, decimal> dictResults)
	{
		if (DataContext is RateWizardModel Model)
		{
			SetWaitCursor();
			GridLanePricingMatrix.Visibility = Visibility.Hidden;
			List<string> names = new();
			List<string> laneNumbers = new();

			foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
			{
				var name = cb.Name;        // xy_12_14
				var value = cb.Text.Trim(); // Lane number

				if (name.StartsWith("xy_") && value.IsNotNullOrWhiteSpace())
				{
					names.Add(name);
					laneNumbers.Add(value);
				}

				//if (name.StartsWith("xy_") && value.IsNotNullOrWhiteSpace())
				//	PopulateTestCell(name, dictResults[value]);
			}

			PopulateTestCells(names, laneNumbers, dictResults);

			GridLanePricingMatrix.Visibility = Visibility.Visible;
			GridLanePricingMatrix.InvalidateVisual();

			ClearWaitCursor();
		}
	}

	private void PopulateTestMatrixWithResults_V1(List<Zone> zones, IReadOnlyDictionary<string, decimal> dictResults)
	{
		if (DataContext is RateWizardModel Model)
		{
			SetWaitCursor();
			GridLanePricingMatrix.Visibility = Visibility.Hidden;

			foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
			{
				var name = cb.Name;        // xy_12_14
				var value = cb.Text.Trim(); // Lane number

				//Logging.WriteLogLine("\tDEBUG tb: " + name + " -> " + value);
				//string[] pieces = name.Split('_');
				//if (pieces.Length == 3 && value.IsNotNullOrWhiteSpace())
				if (name.StartsWith("xy_") && value.IsNotNullOrWhiteSpace())
					PopulateTestCell(name, dictResults[value]);
			}

			GridLanePricingMatrix.Visibility = Visibility.Visible;
			GridLanePricingMatrix.InvalidateVisual();

			ClearWaitCursor();
		}
	}

	private void PopulateTestCells(IReadOnlyList<string> names, IReadOnlyList<string> laneNumbers, IReadOnlyDictionary<string, decimal> dictResults)
	{
		if ((names != null) && (laneNumbers != null) && (names.Count == laneNumbers.Count))
		{
			foreach (var tb in FindVisualChildren<TextBox>(GridLaneTestMatrix))
			{
				var name = tb.Name;

				for (var i = 0; i < names.Count; i++)
				{
					if (names[i] == name)
					{
						var lane = laneNumbers[i];
						tb.Text = dictResults[lane].ToString("F2");
					}
				}
			}
		}
		else
			Logging.WriteLogLine("ERROR names.Count: " + names?.Count + ", laneNumbers.Count: " + laneNumbers?.Count);
	}

	private void PopulateTestCell(string name, decimal value)
	{
		foreach (var tb in FindVisualChildren<TextBox>(GridLaneTestMatrix))
		{
			if (tb.Name == name)
			{
				tb.Text = value.ToString("F2");
				break;
			}
		}
	}

	public List<LocalRateMatrixItem> GatherValuesForLanePricing(List<RateWizardModel.EditZone> zones)
	{
		List<LocalRateMatrixItem> values = new();

		if (DataContext is RateWizardModel Model)
		{
			var zoneNames = (from Z in zones
							 select Z.Name).ToList();

			var reversed = (from Z in zones
							select Z.Name).ToList();
			reversed.Reverse();

			Model.BuildFullFormulae();

			var formula = Model.GetAllFormulae(); // Returns all formulas for lanes

			foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
			{
				var name = cb.Name; // xy_12_14
				var laneNumber = cb.Text.Trim();

				if (laneNumber.IndexOf('.') > -1)
					laneNumber = laneNumber.Substring(0, laneNumber.IndexOf('.'));

				//Logging.WriteLogLine("\tDEBUG cb: " + name + " -> " + laneNumber);
				var pieces = name.Split('_');

				if (pieces.Length == 3)
				{
					var x = int.Parse(pieces[1]);
					var y = int.Parse(pieces[2]);

					// Note - have to adjust for first column and first row
					// x comes from the reversed zones
					var xName = reversed[x - 1];

					// y comes from the zones
					var yName = zoneNames[y - 1];

					//Logging.WriteLogLine("\tDEBUG cb: " + name + "->" + value + " xName: " + xName + " yName: " + yName);

					// Write name in form of yName->xName
					var cellName = FormatRateName(xName, yName);

					LocalRateMatrixItem item = null;
					//var                 formula = string.Empty;

					//Model.BuildFullFormulas();

					var tmp = -1;
					int.TryParse(laneNumber, out tmp);

					if ((laneNumber != string.Empty) && (tmp <= Model.DictFullFormulas.Count))
					{
						//if( !Model.DictFullFormulas.ContainsKey( laneNumber ) )
						//	Model.BuildFullFormulas.ContainsKey( laneNumber ) )
						//Model.BuildFullFormulas();
						//formula = Model.DictFullFormulas[ laneNumber ];

						item = new LocalRateMatrixItem
						{
							Name = cellName,
							FromZone = yName,
							ToZone = xName,
							PackageType = Model.SelectedPackageTypeForMatrix,
							ServiceLevel = Model.SelectedServiceLevelForMatrix,
							Value = (int)decimal.Parse(laneNumber),
							Formula = formula
						};
					}
					else
					{
						item = new LocalRateMatrixItem
						{
							Name = cellName,
							FromZone = yName,
							ToZone = xName,
							PackageType = Model.SelectedPackageTypeForMatrix,
							ServiceLevel = Model.SelectedServiceLevelForMatrix
						};
					}

					values.Add(item);
				}
			}
		}

		return values;
	}

	public Dictionary<string, LocalRateMatrixItem> PopulateMatrixLanePricing(List<LocalRateMatrixItem> items, List<RateWizardModel.EditZone> zones)
	{
		var zoneNames = (from Z in zones
						 select Z.Name).ToList();

		var reversed = (from Z in zones
						select Z.Name).ToList();
		reversed.Reverse();

		// First, build the lanes in GridLanePricing
		if (items != null)
			RemoveAllLanes(false);
		else
			RemoveAllLanes();

		// Accumulate all lanes and formulae
		//var dict = GetLanesAndFormulae(items);
		//Dictionary<string,string> dict = GetLanesAndFormulaeFromLocalRateMatrixItem(items[0]);
		Dictionary<string, string> dict = new();

		if (items is { Count: > 0 })
			dict = GetLanesAndFormulaeFromLocalRateMatrixItem(items[0]);

		if ((dict.Count > 0))
		{
			var FoundCumulative = false;

			foreach (var lane in dict.Keys)
			{
				if (!FoundCumulative && dict[lane].Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE))
				{
					Model.IsThisLanePricingPairCumulativeChecked = false;
					FoundCumulative = true;
				}

				if (!FoundCumulative && dict[lane].Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE))
				{
					Model.IsThisLanePricingPairCumulativeChecked = true;
					FoundCumulative = true;
				}
				var parts = EvaluateRateFormulaHelper.BreakFormulaIntoParts(dict[lane]);

				BtnAddLane_Click(null, null);

				//string key = Model.DictLanes.Count - 1 + "";
				//string key = Model.DictLanes.Count + "";
				if (Model.DictLanes.ContainsKey(lane))
					Model.DictLanes[lane].Clear(); // <--

				//int rowNumber = int.Parse(lane) - 1;
				var rowNumber = (int)decimal.Parse(lane);

				for (var i = 0; i < parts.Count; i++)
				{
					var part = parts[i];
					BuildGridLanePricingCondition(rowNumber, i + 1, null, false);

					LocalRateMatrixItem item = new()
					{
						Name = "Lane_Row_" + rowNumber + "_Col_" + (i + 1),
						Formula = part,
						Value = rowNumber
					};

					//Logging.WriteLogLine("DEBUG Adding condition: " + item.Name);
					//Model.DictLanes[rowNumber - 1 + ""].Add(item);
					Model.DictLanes[rowNumber + ""].Add(item);

					// Set the formula
					var name = "Tb_Row_" + rowNumber + "_Col_" + (i + 1) + "_Formula";

					foreach (var tb in FindVisualChildren<TextBox>(GridLanePricing))
					{
						if (tb.Name == name)
						{
							//tb.Text    = part.Substring( item.Formula.IndexOf( "\n" ) + 1, 10 );
							//tb.ToolTip = part;
							var pieces = item.Formula.Split('\n');
							var tooltip = string.Empty;

							if (pieces != null)
							{
								foreach (var p in pieces)
								{
									if (p.Contains(EvaluateRateFormulaHelper.CUMULATIVE_BASE))
										continue;
									tooltip += p + "\n";
								}
							}

							//tb.Text    = formula.Substring( formula.IndexOf( "\n" ) + 1, 10 );

							var tmp = EvaluateRateFormulaHelper.RemoveLane(tooltip).Trim();

							//if (tooltip.StartsWith(EvaluateRateFormulaHelper.INIT_TOTAL))
							if (tmp.StartsWith(EvaluateRateFormulaHelper.INIT_TOTAL))
							{
								//tb.Text = tooltip.Substring(tooltip.IndexOf("\n") + 1, 10);
								tb.Text = tmp.Substring(tmp.IndexOf("\n") + 1, 10);
							}
							else
								tb.Text = tooltip.Substring(0, 10);
							//tb.ToolTip = formula;
							tb.ToolTip = tooltip;
							break;
						}
					}
				}
			}

			if (!FoundCumulative)
			{
				Logging.WriteLogLine("Didn't find cumulative flag - setting to false");
				Model.IsThisLanePricingPairCumulativeChecked = false;
			}
		}

		// Now populate the matrix
		// cellName, item
		Dictionary<string, LocalRateMatrixItem> values = new();

		if (items is { Count: > 0 })
		{
			Logging.WriteLogLine("items.Count: " + items.Count);
			var activeCount = 0;

			//foreach( var item in items )
			foreach (var Item in items)
			{
				//if (item.Value == 2)
				//{
				//	Logging.WriteLogLine("DEBUG Found Value==2 - Name: " + item.Name);
				//}
				//var name = item.Name; // yName->xName zoneNames[]->reversed[]
				var fromZone = Item.FromZone; // zoneNames[]
				var toZone = Item.ToZone;   // reversed[]

				//var name = "xy_" + ( zoneNames.IndexOf( fromZone ) + 1 ) + "_" + ( reversed.IndexOf( toZone ) + 1 );
				//string name = model.MakeRateMatrixCellName(zoneNames.IndexOf(fromZone) + 1, reversed.IndexOf(toZone) + 1);
				//string name = model.MakeRateMatrixCellName(zoneNames.IndexOf(toZone) + 1, reversed.IndexOf(fromZone) + 1);
				var name = Model.MakeRateMatrixCellName(reversed.IndexOf(toZone) + 1, zoneNames.IndexOf(fromZone) + 1);

				if (!values.ContainsKey(name))
				{
					++activeCount;
					values.Add(name, Item);
				}
				else
					Logging.WriteLogLine("Skipping " + name);
			}
			Logging.WriteLogLine("DEBUG activeCount: " + activeCount);
		}
		GridLanePricingMatrix.Visibility = Visibility.Hidden;
		var count = 0;

		foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
		{
			var name = cb.Name; // xy_12_14

			if ((name != null) && name.StartsWith("xy_"))
			{
				//if (values[name]?.Value > 0)
				if (values.ContainsKey(name) && (values[name]?.Value > 0))
				{
					var tmp = (int)values[name].Value;
					cb.Text = tmp + "";

					// Check that it points to a valid lane
					if (Model.DictLanes.ContainsKey(cb.Text))
					{
						cb.SelectedIndex = tmp;
						cb.SelectedItem = tmp;
					}
					else
					{
						Logging.WriteLogLine("Not setting " + name + " to lane " + tmp + " as it doesn't exist");
						cb.SelectedIndex = 0;
					}

					// Lane 2
					//if (tmp == 2)
					//{
					//	Logging.WriteLogLine("DEBUG Set " + name + " (" + count + ") to " + cb.Text + ", SelectedItem " + cb.SelectedItem + ", SelectedIndes " + cb.SelectedIndex);
					//}
					++count;
				}
				else
					cb.SelectedIndex = 0;
			}
		}

		// Note: This seems to help with ui responsiveness
		GridLanePricingMatrix.Visibility = Visibility.Visible;
		GridLanePricingMatrix.InvalidateVisual();
		TcPricing.SelectedIndex = 1;

		return values;
	}

	public Dictionary<string, LocalRateMatrixItem> PopulateMatrixLanePricing_V1(List<LocalRateMatrixItem> items, List<Zone> zones)
	{
		var zoneNames = (from Z in zones
						 select Z.Name).ToList();

		var reversed = (from Z in zones
						select Z.Name).ToList();
		reversed.Reverse();

		// cellName, item
		Dictionary<string, LocalRateMatrixItem> values = new();

		if ((items != null) && (items.Count > 0) && DataContext is RateWizardModel model)
		{
			Logging.WriteLogLine("items.Count: " + items.Count);
			var activeCount = 0;

			//foreach( var item in items )
			for (var i = 0; i < items.Count; i++)
			{
				var item = items[i];
				//if (item.Value == 2)
				//{
				//	Logging.WriteLogLine("DEBUG Found Value==2 - Name: " + item.Name);
				//}
				//var name = item.Name; // yName->xName zoneNames[]->reversed[]
				var fromZone = item.FromZone; // zoneNames[]
				var toZone = item.ToZone;   // reversed[]

				//var name = "xy_" + ( zoneNames.IndexOf( fromZone ) + 1 ) + "_" + ( reversed.IndexOf( toZone ) + 1 );
				//string name = model.MakeRateMatrixCellName(zoneNames.IndexOf(fromZone) + 1, reversed.IndexOf(toZone) + 1);
				//string name = model.MakeRateMatrixCellName(zoneNames.IndexOf(toZone) + 1, reversed.IndexOf(fromZone) + 1);
				var name = model.MakeRateMatrixCellName(reversed.IndexOf(toZone) + 1, zoneNames.IndexOf(fromZone) + 1);

				if (!values.ContainsKey(name))
				{
					++activeCount;
					values.Add(name, item);
				}
				else
					Logging.WriteLogLine("Skipping " + name);
			}
			Logging.WriteLogLine("DEBUG activeCount: " + activeCount);
		}
		GridLanePricingMatrix.Visibility = Visibility.Hidden;
		var count = 0;

		foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
		{
			var name = cb.Name; // xy_12_14

			if ((name != null) && name.StartsWith("xy_"))
			{
				//if (values[name]?.Value > 0)
				if (values.ContainsKey(name) && (values[name]?.Value > 0))
				{
					var tmp = (int)values[name].Value;
					cb.Text = tmp + "";

					cb.SelectedIndex = tmp;
					cb.SelectedItem = tmp;

					// Lane 2
					//if (tmp == 2)
					//{
					//	Logging.WriteLogLine("DEBUG Set " + name + " (" + count + ") to " + cb.Text + ", SelectedItem " + cb.SelectedItem + ", SelectedIndes " + cb.SelectedIndex);
					//}
					++count;
				}
				else
					cb.SelectedIndex = 0;
			}
		}

		// Note: This seems to help with ui responsiveness
		GridLanePricingMatrix.Visibility = Visibility.Visible;
		GridLanePricingMatrix.InvalidateVisual();
		TcPricing.SelectedIndex = 1;

		// Now for GridLanePricing
		if (items != null)
			RemoveAllLanes(false);
		else
			RemoveAllLanes();

		// Accumulate all lanes and formulae
		var dict = GetLanesAndFormulae(items);

		if ((dict.Count > 0) && DataContext is RateWizardModel Model)
		{
			var foundCumulative = false;

			foreach (var lane in dict.Keys)
			{
				if (!foundCumulative && dict[lane].Contains(EvaluateRateFormulaHelper.CUMULATIVE_FALSE))
				{
					Model.IsThisLanePricingPairCumulativeChecked = false;
					foundCumulative = true;
				}

				if (!foundCumulative && dict[lane].Contains(EvaluateRateFormulaHelper.CUMULATIVE_TRUE))
				{
					Model.IsThisLanePricingPairCumulativeChecked = true;
					foundCumulative = true;
				}
				var parts = EvaluateRateFormulaHelper.BreakFormulaIntoParts(dict[lane]);

				BtnAddLane_Click(null, null);

				//string key = Model.DictLanes.Count - 1 + "";
				//string key = Model.DictLanes.Count + "";
				if (Model.DictLanes.ContainsKey(lane))
					Model.DictLanes[lane].Clear(); // <--

				//int rowNumber = int.Parse(lane) - 1;
				var rowNumber = (int)decimal.Parse(lane);

				for (var i = 0; i < parts.Count; i++)
				{
					var part = parts[i];
					BuildGridLanePricingCondition(rowNumber, i + 1, null, false);

					LocalRateMatrixItem item = new()
					{
						Name = "Lane_Row_" + rowNumber + "_Col_" + (i + 1),
						Formula = part,
						Value = rowNumber
					};

					//Logging.WriteLogLine("DEBUG Adding condition: " + item.Name);
					//Model.DictLanes[rowNumber - 1 + ""].Add(item);
					Model.DictLanes[rowNumber + ""].Add(item);

					// Set the formula
					var name = "Tb_Row_" + rowNumber + "_Col_" + (i + 1) + "_Formula";

					foreach (var tb in FindVisualChildren<TextBox>(GridLanePricing))
					{
						if (tb.Name == name)
						{
							//tb.Text    = part.Substring( item.Formula.IndexOf( "\n" ) + 1, 10 );
							//tb.ToolTip = part;
							var pieces = item.Formula.Split('\n');
							var tooltip = string.Empty;

							if (pieces != null)
							{
								foreach (var p in pieces)
								{
									if (p.Contains(EvaluateRateFormulaHelper.CUMULATIVE_BASE))
										continue;
									tooltip += p + "\n";
								}
							}

							//tb.Text    = formula.Substring( formula.IndexOf( "\n" ) + 1, 10 );

							var tmp = EvaluateRateFormulaHelper.RemoveLane(tooltip).Trim();

							//if (tooltip.StartsWith(EvaluateRateFormulaHelper.INIT_TOTAL))
							if (tmp.StartsWith(EvaluateRateFormulaHelper.INIT_TOTAL))
							{
								//tb.Text = tooltip.Substring(tooltip.IndexOf("\n") + 1, 10);
								tb.Text = tmp.Substring(tmp.IndexOf("\n") + 1, 10);
							}
							else
								tb.Text = tooltip.Substring(0, 10);
							//tb.ToolTip = formula;
							tb.ToolTip = tooltip;
							break;
						}
					}
				}
			}

			if (!foundCumulative)
			{
				Logging.WriteLogLine("Didn't find cumulative flag - setting to false");
				Model.IsThisLanePricingPairCumulativeChecked = false;
			}
		}

		return values;
	}


	private SortedDictionary<string, string> GetLanesAndFormulae(List<LocalRateMatrixItem> items)
	{
		SortedDictionary<string, string> dict = new();

		if (items != null)
		{
			// TODO Really only need to look at 1 item as all have the same formula
			foreach (var item in items)
			{
				//var value = (int)item.Value;
				var lanes = GetLanesAndFormulaeFromLocalRateMatrixItem(item);

				//if (!dict.ContainsKey(item.Value + "") && item.Formula.IsNotNullOrWhiteSpace())
				//if (!dict.ContainsKey(value + "") && item.Formula.IsNotNullOrWhiteSpace())
				foreach (var lane in lanes.Keys)
				{
					//if (!dict.ContainsKey(lane) && item.Formula.IsNotNullOrWhiteSpace())
					if (!dict.ContainsKey(lane) && dict[lane].IsNotNullOrWhiteSpace())
					{
						//dict.Add(item.Value + "", item.Formula);
						//dict.Add(value + "", item.Formula);
						dict.Add(lane, lanes[lane]);
					}
				}
			}
		}

		return dict;
	}

	private Dictionary<string, string> GetLanesAndFormulaeFromLocalRateMatrixItem(LocalRateMatrixItem item)
	{
		Dictionary<string, string> dict = new();

		static string GetLaneFromLine(string line)
		{
			var lane = string.Empty;
			var pieces = line.Split('=');

			if (pieces.Length == 2)
				lane = pieces[1].Trim();

			return lane;
		}

		var formula = item.Formula;

		if ((formula != null) && formula.Contains(EvaluateRateFormulaHelper.LANE))
		{
			var pieces = formula.Split('\n');

			if (pieces.Length > 0)
			{
				var lane = string.Empty;
				//string line = (from L in pieces where L.Contains(EvaluateRateFormulaHelper.LANE) select L).FirstOrDefault();
				var tmp = string.Empty;

				foreach (var line in pieces)
				{
					var prevLane = lane;

					if (line.Contains(EvaluateRateFormulaHelper.LANE))
					{
						lane = GetLaneFromLine(line);

						// If this is the next, save
						if (!dict.ContainsKey(lane))
						{
							if (prevLane.IsNotNullOrEmpty())
							{
								// dict already has slot - put in the formula
								dict[prevLane] = tmp;
							}
							dict.Add(lane, string.Empty);
						}

						tmp = line + "\n"; // Reset						
					}
					else
						tmp += line + "\n";
				}

				dict[lane] = tmp;
			}
		}

		return dict;
	}

	private SortedDictionary<string, string> GetLanesAndFormulae_V1(List<LocalRateMatrixItem> items)
	{
		SortedDictionary<string, string> dict = new();

		if (items != null)
		{
			foreach (var item in items)
			{
				var value = (int)item.Value;

				//if (!dict.ContainsKey(item.Value + "") && item.Formula.IsNotNullOrWhiteSpace())
				if (!dict.ContainsKey(value + "") && item.Formula.IsNotNullOrWhiteSpace())
				{
					//dict.Add(item.Value + "", item.Formula);
					dict.Add(value + "", item.Formula);
				}
			}
		}

		return dict;
	}

	public bool CheckLanePricingMatrixForModifications(List<LocalRateMatrixItem> items)
	{
		var modified = false;

		if (DataContext is RateWizardModel Model)
		{
			foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
			{
				var item = (from I in items
							where I.Name == cb.Name
							select I).FirstOrDefault();

				if (item != null)
				{
					// Test both
					var value = (int)item.Value;

					//if (cb.Text.Trim() != (value + ""))
					if (cb.Text.Trim() != value.ToString("F0"))
					{
						Logging.WriteLogLine("DEBUG cb " + cb.Name + " is modified - Value was " + item.Value + " now " + cb.Text.Trim());
						modified = true;
						//break;
					}

					if (!modified && cb.Name.Contains("_"))
					{
						var pieces = cb.Name.Split('_');

						if (pieces.Length > 1)
						{
							var lane = pieces[1];

							if (Model.DictFullFormulas.ContainsKey(lane))
							{
								var formula = Model.DictFullFormulas[lane];

								if ((formula != null) && (formula != item.Formula))
								{
									Logging.WriteLogLine("DEBUG formula at " + cb.Name + " is modified - Value was " + item.Formula + " now " + formula);
									modified = true;
								}
							}
						}
						//string rateName = item.PackageType + " -> " + item.ServiceLevel;
						//if (Model.SdRates.ContainsKey(rateName)) {
						//	string formula = Model.SdRates[rateName][0].Formula;
						//	if (formula != null && formula != item.Formula)
						//	{
						//		Logging.WriteLogLine("DEBUG formula at " + cb.Name + " is modified - Value was " + item.Formula + " now " + formula);
						//		modified = true;
						//	}
						//}
					}

					if (modified)
						break;
				}
				else
				{
					// Empty rate matrix
					if (cb.Text.Trim() != string.Empty)
					{
						Logging.WriteLogLine("DEBUG cb " + cb.Name + " is modified - Value was empty, now " + cb.Text.Trim());
						modified = true;
						break;
					}
				}
			}
		}

		return modified;
	}

	public bool CheckLanePricingMatrixForModifications_V1(List<LocalRateMatrixItem> items)
	{
		var modified = false;

		foreach (var cb in FindVisualChildren<ComboBox>(GridLanePricingMatrix))
		{
			var item = (from I in items
						where I.Name == cb.Name
						select I).FirstOrDefault();

			if (item != null)
			{
				// Test both
				if (item.Value != 0)
				{
					var value = (int)item.Value;

					//if( cb.Text.Trim() != ( item.Value + "" ) )
					if (cb.Text.Trim() != (value + ""))
					{
						//Logging.WriteLogLine("DEBUG cb " + cb.Name + " is modified - Value was " + item.Value + " now " + cb.Text.Trim());
						modified = true;
						//break;
					}
				}

				//else if( item.Formula != string.Empty )
				if (item.Formula != string.Empty)
				{
					if (cb.Text.Trim() != item.Formula)
					{
						//Logging.WriteLogLine("DEBUG cb " + cb.Name + " is modified - Formula was " + item.Formula + " now " + cb.Text.Trim());
						modified = true;
						//break;
					}
				}

				if (modified)
					break;
			}
			else
			{
				// Empty rate matrix
				if (cb.Text.Trim() != string.Empty)
				{
					//Logging.WriteLogLine("DEBUG cb " + cb.Name + " is modified - Value was empty, now " + cb.Text.Trim());
					modified = true;
					break;
				}
			}
		}

		return modified;
	}

	#endregion

	#endregion

	#region Surcharge Handlers
	public void PopulateSurchargeZonesList()
	{
        // Need to handle multiple calls
        if (Model != null && GridSurchargeZones.RowDefinitions.Count == 0)
		{
			int row = 0;
			foreach (Zone z in  Model.Zones)
			{
				if (z.Name.IsNotNullOrWhiteSpace())
				{
					GridSurchargeZones.RowDefinitions.Add(new RowDefinition());
					CheckBox cb = new()
					{
						Name = "SurchargeZoneCheckBox_" + z.Name,
						Content = z.Name,
					};
					GridSurchargeZones.Children.Add(cb);
					Grid.SetColumn(cb, 0);
					Grid.SetRow(cb, row++);

					cb.Checked += (o, e) => CbSurchargeZone_Checked(o, e);
					cb.Unchecked += (o, e) => CbSurchargeZone_Unchecked(o, e);
				}
            }
		}
	}

    private void CbSurchargeZone_Checked(object sender, RoutedEventArgs e)
    {
		if (Model != null)
		{
			if (sender is CheckBox cb)
			{
				if (!Model.SelectedSurchargeZones.Contains(cb.Content))
				{
					Model.SelectedSurchargeZones.Add((string)cb.Content);
					Model.BuildSurchargeFormula();
				}
			}
		}
    }

    private void CbSurchargeZone_Unchecked(object sender, RoutedEventArgs e)
    {
        if (Model != null)
		{
            if (sender is CheckBox cb)
            {
                if (Model.SelectedSurchargeZones.Contains(cb.Content))
                {
                    Model.SelectedSurchargeZones.Remove((string)cb.Content);
					Model.BuildSurchargeFormula();
                }
            }
        }
    }

    public void PopulateSurchargePackageTypeList()
	{
		// Need to handle multiple calls
        if (Model != null && GridSurchargePackageTypes.RowDefinitions.Count == 0)
        {
            int row = 0;
            foreach (string p in Model.PackageTypeNames)
            {
                GridSurchargePackageTypes.RowDefinitions.Add(new RowDefinition());
                CheckBox cb = new()
                {
                    Name = "SurchargePackageTypeCheckBox_" + row,
                    Content = p,
					FontWeight = FontWeights.Normal,
                };
                GridSurchargePackageTypes.Children.Add(cb);
                Grid.SetColumn(cb, 0);
                Grid.SetRow(cb, row++);

                cb.Checked += (o, e) => CbSurchargePackageTypes_Checked(o, e);
                cb.Unchecked += (o, e) => CbSurchargePackageTypes_Unchecked(o, e);
            }
        }
    }

    private void CbSurchargePackageTypes_Checked(object sender, RoutedEventArgs e)
    {
        if (Model != null)
        {
            if (sender is CheckBox cb)
            {
                if (!Model.SelectedSurchargePackageTypes.Contains(cb.Content))
                {
                    Model.SelectedSurchargePackageTypes.Add((string)cb.Content);
                    Model.BuildSurchargeFormula();
                }
            }
        }
    }

    private void CbSurchargePackageTypes_Unchecked(object sender, RoutedEventArgs e)
    {
        if (Model != null)
        {
            if (sender is CheckBox cb)
            {
                if (Model.SelectedSurchargePackageTypes.Contains(cb.Content))
                {
                    Model.SelectedSurchargePackageTypes.Remove((string)cb.Content);
                    Model.BuildSurchargeFormula();
                }
            }
        }
    }

    private void BtnSurchargeUpdate_Click(object sender, RoutedEventArgs e)
	{
        if (Model != null)
        {
			// Check that it doesn't already exist
			bool go = true;
			if (Model.IsNewSurcharge)
			{
                string[] pieces = Model.SurchargeName.Split(new string[] { "Value" }, StringSplitOptions.RemoveEmptyEntries);

				//Charge test = (from C in Model.Surcharges where C.Formula == Model.SurchargeFormula select C).FirstOrDefault();
				Charge test = (from C in Model.Surcharges where C.Label.StartsWith(pieces[0]) select C).FirstOrDefault();
				if (test != null)
				{
					string title = FindStringResource("RateWizardTabSurchargesDuplicateTitle");
					string message = FindStringResource("RateWizardTabSurchargesDuplicateMessage") + "\n" + test.Formula;
					MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
					go = false;
				}
			}
			if (Model.SelectedSurchargePackageTypes.Count == 0 || Model.SelectedSurchargeZones.Count == 0)
			{
				string title = FindStringResource("RateWizardTabSurchargesNoPackagesZonesTitle");
				string message = FindStringResource("RateWizardTabSurchargesNoPackagesZonesMessage");
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
                go = false;
			}

            if (go)
			{
				Model.IsEditingSurcharge = false;

				// TODO Save
				Model.AddUpdateSurchargeLocally();

				BtnSurchargeClear_Click(sender, e);
				//TbSurchargeFormula.Text = string.Empty;
				//LblSurchargeName.Content = string.Empty;
				Model.SurchargeFormula = string.Empty;
				Model.SurchargeName = string.Empty;
				Model.IsNewSurcharge = false;
				Model.SurchargeFormula = string.Empty;
			}
        }
    }

	private void BtnSurchargeCancel_Click(object sender, RoutedEventArgs e)
	{
        if (Model != null)
        {
            Model.IsEditingSurcharge = false;
			Model.IsNewSurcharge = false;
			Model.SurchargeFormula = string.Empty;
            BtnSurchargeClear_Click(sender, e);
            //TbSurchargeFormula.Text = string.Empty;
            //LblSurchargeName.Content = string.Empty;
            Model.SurchargeFormula = string.Empty;
            Model.SurchargeName = string.Empty;
        }
    }

	private void BtnSurchargeClear_Click(object sender, RoutedEventArgs e)
	{
        foreach (var child in GridSurchargePackageTypes.Children)
        {
            if (child is CheckBox cb)
            {
                cb.IsChecked = false;
            }
        }

        foreach (var child in GridSurchargeZones.Children)
        {
            if (child is CheckBox cb)
            {
                cb.IsChecked = false;
            }
        }

        DudSurchargeValue.Value = 0;
    }

	private void BtnSurchargeEdit_Click(object? sender, RoutedEventArgs? e)
	{
        if (Model != null)
		{
			Model.IsEditingSurcharge = true;
			Model.IsNewSurcharge = false;
            Model.LoadSelectedSurcharge();
        }
    }

	private void BtnSurchargeNew_Click(object sender, RoutedEventArgs e)
	{
        if (Model != null)
        {
            Model.IsEditingSurcharge = true;
			Model.IsNewSurcharge = true;
        }
    }

	private void BtnSurchargeDel_Click(object sender, RoutedEventArgs e)
	{
		if (Model != null)
		{
			string title = FindStringResource("RateWizardTabSurchargesDeleteTitle");
			string message = FindStringResource("RateWizardTabSurchargesDeleteMessage");
			string description = "\n" + Model.SelectedSurchargeObject.Formula + "\n";
			message = message.Replace("@1", description);
			MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (mbr == MessageBoxResult.Yes)
			{
				Model.DeleteSurchargeLocally();
			}
		}
	}

	private void LbSurcharges_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	{
		BtnSurchargeEdit_Click(null, null);
	}
    
	private void DudSurchargeValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
		if (sender is DecimalUpDown dud && Model != null)
		{
			Model.SelectedSurchargeValue = dud.Value;
			Model.BuildSurchargeFormula();
		}
    }

    #endregion

    private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
    {

    }
}
