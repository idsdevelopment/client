﻿namespace ViewModels.RateWizard;

internal class EditRateFormulaWindowModel : ViewModelBase
{
	public ObservableCollection<string> PricingObjects { get; } = new()
	                                                              {
		                                                              FindStringResource( "RateWizardTabPriceMatrixComboObjectPieces" ),
		                                                              FindStringResource( "RateWizardTabPriceMatrixComboObjectWeight" ),
		                                                              FindStringResource( "RateWizardTabPriceMatrixComboObjectMileage" )
	                                                              };

	public ObservableCollection<string> Conditions { get; } = new()
	                                                          {
		                                                          ">",
		                                                          ">=",
		                                                          "=",
		                                                          "<=",
		                                                          "<"
	                                                          };

	public ObservableCollection<string> LogicalOps { get; } = new()
	                                                          {
		                                                          " ",
		                                                          "And",
		                                                          "Or"
	                                                          };

	public ObservableCollection<string> MathOps { get; } = new()
	                                                       {
		                                                       "*",
		                                                       "+",
		                                                       "/",
		                                                       "-"
	                                                       };

	// Note - Took * & / out to simplify formulas
	public ObservableCollection<string> MathOpsWithSpace { get; } = new()
	                                                                {
		                                                                " ",
		                                                                "+",
		                                                                "-"
		                                                                //"*",
		                                                                //"/"
	                                                                };

	public ObservableCollection<string> FormulaPricingObjects { get; } = new()
	                                                                     {
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectPieces" ),
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectPieces" ) + " - ",
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectPieces" ) + " + ",
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectWeight" ),
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectWeight" ) + " - ",
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectWeight" ) + " + ",
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectMileage" ),
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectMileage" ) + " - ",
		                                                                     FindStringResource( "RateWizardTabPriceMatrixComboObjectMileage" ) + " + "
	                                                                     };

	public string TOTAL_INIT { get; } = "Total = 0";

	public string SelectedPricingObject
	{
		get => Get( () => SelectedPricingObject, "" );
		set => Set( () => SelectedPricingObject, value );
	}


	public string SelectedCondition
	{
		get => Get( () => SelectedCondition, "" );
		set => Set( () => SelectedCondition, value );
	}


	public decimal SelectedConditionLimit
	{
		get => Get( () => SelectedConditionLimit, 0 );
		set => Set( () => SelectedConditionLimit, value );
	}


	public string SelectedLogicOp
	{
		get => Get( () => SelectedLogicOp, "" );
		set => Set( () => SelectedLogicOp, value );
	}


	public Visibility SecondConditionVisiblity
	{
		get => Get( () => SecondConditionVisiblity, Visibility.Collapsed );
		set => Set( () => SecondConditionVisiblity, value );
	}


	public string SelectedSecondPricingObject
	{
		get => Get( () => SelectedSecondPricingObject, PricingObjects[ 0 ] );
		set => Set( () => SelectedSecondPricingObject, value );
	}


	public string SelectedSecondCondition
	{
		get => Get( () => SelectedSecondCondition, Conditions[ 0 ] );
		set => Set( () => SelectedSecondCondition, value );
	}


	public decimal SelectedSecondConditionLimit
	{
		get => Get( () => SelectedSecondConditionLimit, 0 );
		set => Set( () => SelectedSecondConditionLimit, value );
	}


	public bool IsResultValueChecked
	{
		get => Get( () => IsResultValueChecked, true );
		set => Set( () => IsResultValueChecked, value );
	}


	public int TcResultTabSelected
	{
		get { return Get( () => TcResultTabSelected, 0 ); }
		set { Set( () => TcResultTabSelected, value ); }
	}


	public bool IsResultFormulaChecked
	{
		get => Get( () => IsResultFormulaChecked, false );
		set => Set( () => IsResultFormulaChecked, value );
	}

	public decimal ResultValue
	{
		get => Get( () => ResultValue, 0 );
		set => Set( () => ResultValue, value );
	}

	public string ResultFormula
	{
		get => Get( () => ResultFormula, "" );
		set => Set( () => ResultFormula, value );
	}


	public decimal SelectedResultFormulaBasePrice
	{
		get => Get( () => SelectedResultFormulaBasePrice, 0 );
		set => Set( () => SelectedResultFormulaBasePrice, value );
	}


	public string SelectedResultFormulaPricingObject
	{
		get => Get( () => SelectedResultFormulaPricingObject, FormulaPricingObjects[ 0 ] );
		set => Set( () => SelectedResultFormulaPricingObject, value );
	}


	public Visibility SelectedResultFormulaPricingObjecValueVisibility
	{
		get { return Get( () => SelectedResultFormulaPricingObjecValueVisibility, WhenSelectedResultFormulaPricingObjectChanges() ); }
		set { Set( () => SelectedResultFormulaPricingObjecValueVisibility, value ); }
	}

	public decimal SelectedResultFormulaPricingObjectValue
	{
		get { return Get( () => SelectedResultFormulaPricingObjectValue, 0 ); }
		set { Set( () => SelectedResultFormulaPricingObjectValue, value ); }
	}


	public string SelectedResultFormulaMathOp
	{
		get => Get( () => SelectedResultFormulaMathOp, MathOps[ 0 ] );
		set => Set( () => SelectedResultFormulaMathOp, value );
	}


	public decimal SelectedResultFormulaMathValue
	{
		get => Get( () => SelectedResultFormulaMathValue, 0 );
		set => Set( () => SelectedResultFormulaMathValue, value );
	}


	public string SelectedResultFormulaAnotherClauseMathOp
	{
		get { return Get( () => SelectedResultFormulaAnotherClauseMathOp, "" ); }
		set { Set( () => SelectedResultFormulaAnotherClauseMathOp, value ); }
	}

	public string SelectedResultFormulaSecondPricingObject
	{
		get => Get( () => SelectedResultFormulaSecondPricingObject, FormulaPricingObjects[ 0 ] );
		set => Set( () => SelectedResultFormulaSecondPricingObject, value );
	}


	public decimal SelectedResultFormulaSecondPricingObjectValue
	{
		get { return Get( () => SelectedResultFormulaSecondPricingObjectValue, 0 ); }
		set { Set( () => SelectedResultFormulaSecondPricingObjectValue, value ); }
	}


	public string SelectedResultFormulaSecondMathOp
	{
		get => Get( () => SelectedResultFormulaSecondMathOp, MathOps[ 0 ] );
		set => Set( () => SelectedResultFormulaSecondMathOp, value );
	}


	public decimal SelectedResultFormulaSecondMathValue
	{
		get => Get( () => SelectedResultFormulaSecondMathValue, 0 );
		set => Set( () => SelectedResultFormulaSecondMathValue, value );
	}


	public string CurrentFormula
	{
		get => Get( () => CurrentFormula, "" );
		set => Set( () => CurrentFormula, value );
	}


	public string NewFormula
	{
		get => Get( () => NewFormula, "" );
		set => Set( () => NewFormula, value );
	}


	public decimal TestPieces
	{
		get => Get( () => TestPieces, 0 );
		set => Set( () => TestPieces, value );
	}


	public decimal TestWeight
	{
		get => Get( () => TestWeight, 0 );
		set => Set( () => TestWeight, value );
	}


	public decimal TestMileage
	{
		get => Get( () => TestMileage, 0 );
		set => Set( () => TestMileage, value );
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	[DependsUpon( nameof( SelectedLogicOp ) )]
	public void WhenSelectedLogicOpChanges()
	{
		SecondConditionVisiblity = SelectedLogicOp.Trim() != string.Empty ? Visibility.Visible : Visibility.Collapsed;
	}

	[DependsUpon( nameof( IsResultValueChecked ) )]
	public void WhenIsResultValueCheckedChanges()
	{
		IsResultFormulaChecked = !IsResultValueChecked;
	}


	[DependsUpon( nameof( SelectedResultFormulaPricingObject ) )]
	public Visibility WhenSelectedResultFormulaPricingObjectChanges()
	{
		var visibility = Visibility.Collapsed;

		if( ( SelectedResultFormulaPricingObject != null ) && SelectedResultFormulaPricingObject.Contains( " " ) )
			visibility = Visibility.Visible;
		return visibility;
	}


#region Actions
	public void LoadFormula_V1()
	{
		/*
		 Total = 0
		 If Weight < 3  And  Pieces = 1 
		 Total = 5
		 Endif
		 Result = Total
		 */

		var finishedFirstCondition = false;
		//bool hasSecondCondition = false;
		var finishedConditions = false;
		var lines              = CurrentFormula.Split( '\n' );

		for( var i = 0; i < lines.Length; i++ )
		{
			var line = lines[ i ].Trim();
			Logging.WriteLogLine( "DEBUG line: " + line );

			if( line.ToLower().StartsWith( "if" ) )
			{
				// Found condition
				var pieces = line.Split( ' ' );

				// Skip over the 'if'
				for( var j = 1; j < pieces.Length; j++ )
				{
					var piece = pieces[ j ].Trim();
					Logging.WriteLogLine( "DEBUG piece: " + piece );

					if( !finishedFirstCondition )
					{
						if( piece.ToLower() is "pieces" or "weight" or "mileage" )
						{
							SelectedPricingObject = piece;
							continue;
						}

						if( Conditions.Contains( piece ) )
						{
							SelectedCondition = piece;
							continue;
						}

						if( decimal.TryParse( piece, out var value ) )
						{
							SelectedConditionLimit = value;
							finishedFirstCondition = true;
						}
					}
					else
					{
						if( piece.ToLower() is "and" or "or" )
						{
							SelectedLogicOp = piece;
							continue;
						}

						if( piece.ToLower() is "pieces" or "weight" or "mileage" )
						{
							SelectedSecondPricingObject = piece;
							continue;
						}

						if( Conditions.Contains( piece ) )
						{
							SelectedSecondCondition = piece;
							continue;
						}

						if( decimal.TryParse( piece, out var value ) )
							SelectedSecondConditionLimit = value;
					}
				}
			}
			else if( line.ToLower().StartsWith( "total" ) )
			{
				// Now for the result - simple first
				// Result = Total
				var pieces = line.Split( ' ' );

				if( pieces.Length == 3 )
				{
					ResultValue          = decimal.Parse( pieces[ 2 ].Trim() );
					IsResultValueChecked = true;
				}
				else
				{
					IsResultValueChecked = false;

					// Total = Weight * 10
					for( var j = 2; j < pieces.Length; j++ )
					{
						var piece = pieces[ j ].Trim().ToLower();

						if( piece is "pieces" or "weight" or "mileage" )
						{
							SelectedResultFormulaPricingObject = piece;
							continue;
						}

						if( MathOps.Contains( piece ) )
						{
							SelectedResultFormulaMathOp = piece;
							continue;
						}

						if( decimal.TryParse( piece, out var value ) )
							SelectedResultFormulaMathValue = value;
					}
				}
			}
			else if( line.ToLower().StartsWith( "endif" ) )
			{
				finishedConditions = true;
				continue;
			}

			if( finishedConditions )
			{
				// Done
				break;
			}
		}
	}

	public void LoadFormula()
	{
		/*
		    Total = 0
		    If Pieces <= 5 And Weight <= 10 
		    Total = 18.9 + ((Weight - 1) * 0.26) + ((Pieces - 1) * 6.3)
		    Endif
		    Result = Total
		 */

		var finishedFirstCondition = false;
		//bool hasSecondCondition = false;
		var finishedConditions = false;
		var lines              = CurrentFormula.Split( '\n' );

		// Skip over Total = 0
		for( var i = 0; i < lines.Length; i++ )
		{
			var line = lines[ i ].Trim();
			Logging.WriteLogLine( "DEBUG line: " + line );
			if (line == TOTAL_INIT)
			{
				continue;
			}

			if( line.ToLower().StartsWith( "if" ) )
			{
				// Found condition
				var pieces = line.Split( ' ' );

				// Skip over the 'if'
				for( var j = 1; j < pieces.Length; j++ )
				{
					var piece = pieces[ j ].Trim();
					Logging.WriteLogLine( "DEBUG piece: " + piece );

					if( !finishedFirstCondition )
					{
						if( piece.ToLower() is "pieces" or "weight" or "mileage" )
						{
							SelectedPricingObject = piece;
							continue;
						}

						if( Conditions.Contains( piece ) )
						{
							SelectedCondition = piece;
							continue;
						}

						if( decimal.TryParse( piece, out var value ) )
						{
							SelectedConditionLimit = value;
							finishedFirstCondition = true;
						}
					}
					else
					{
						if( piece.ToLower() is "and" or "or" )
						{
							SelectedLogicOp = piece;
							continue;
						}

						if( piece.ToLower() is "pieces" or "weight" or "mileage" )
						{
							SelectedSecondPricingObject = piece;
							continue;
						}

						if( Conditions.Contains( piece ) )
						{
							SelectedSecondCondition = piece;
							continue;
						}

						if( decimal.TryParse( piece, out var value ) )
							SelectedSecondConditionLimit = value;
					}
				}
			}
			else if( line.ToLower().StartsWith( "total" ) )
			{
				// Now for the result - simple first
				var pieces = line.Split( ' ' );

				//if (IsResultValueChecked)
				//if (TcResultTabSelected == 0)
				//{
				//}
				if( pieces.Length == 3 )
				{
					// Total = 5
					ResultValue            = decimal.Parse( pieces[ 2 ].Trim() );
					IsResultValueChecked   = true;
					IsResultFormulaChecked = false;
				}
				else
				{
					IsResultValueChecked   = false;
					IsResultFormulaChecked = true;

					// Simple: Total = Weight * 10
					if( pieces.Length == 5 )
					{
						for( var j = 2; j < pieces.Length; j++ )
						{
							var piece = pieces[ j ].Trim();

							if( piece.ToLower() is "pieces" or "weight" or "mileage" )
							{
								SelectedResultFormulaPricingObject = piece;
								continue;
							}

							if( MathOps.Contains( piece ) )
							{
								SelectedResultFormulaMathOp = piece;
								continue;
							}

							if( decimal.TryParse( piece, out var value ) )
								SelectedResultFormulaMathValue = value;
						}
					}
					else
					{
						//    0     1 2  3 4      5 6
						//  7 Total = 10 + Pieces * 0.5
						//    0     1 2  3 4       5 6  7 8
						//  9 Total = 10 + (Pieces - 1) * 0.5
						//    0      1 2  3 4        5 6    7 8        9 0
						// 11 Total = 10 + ((Pieces * 0.5) + ((Weight * 0.5)
						//    0      1 2    3 4        5 6  7 8     9 0        1 2  3 4
						// 15 Total = 18.9 + ((Weight - 1) * 0.26) + ((Pieces - 1) * 5)

						if( decimal.TryParse( pieces[ 2 ].Trim(), out var basePrice ) )
							SelectedResultFormulaBasePrice = basePrice;

						if( pieces[ 4 ].IndexOf( "(" ) == -1 )
						{
							// 7 Total = 10 + Pieces * 0.5
							SelectedResultFormulaPricingObject = pieces[ 4 ].Trim();
							SelectedResultFormulaMathOp        = pieces[ 5 ].Trim();

							if( decimal.TryParse( pieces[ 6 ], out var value ) )
								SelectedResultFormulaMathValue = value;

							break;
						}
						var piece = pieces[ 4 ].Trim();

						if( !piece.StartsWith( "((" ) )
						{
							switch( pieces.Length )
							{
							case 9:
								//   0     1 2  3 4       5 6  7 8 
								// 9 Total = 10 + (Pieces - 1) * 0.5
								// (Pieces
								piece = piece.Substring( 1 );

								if( piece.ToLower() is "pieces" or "weight" or "mileage" )
								{
									// Pieces -
									SelectedResultFormulaPricingObject = piece + " " + pieces[ 5 ] + " ";
									// 1)
									piece = pieces[ 6 ].Trim().Replace( ")", "" );

									if( decimal.TryParse( piece, out var value ) )
									{
										SelectedResultFormulaPricingObjectValue = value;
										// * 0.5
										SelectedResultFormulaMathOp = pieces[ 7 ].Trim();

										if( decimal.TryParse( pieces[ 8 ], out value ) )
											SelectedResultFormulaMathValue = value;
									}
								}
								break;

							case 11:
								//    0     1 2  3 4       5 6    7  8      9 0
								// 11 Total = 10 + (Pieces * 0.5) + (Weight * 0.5)
								piece = piece.Substring( 1 );

								if( piece.ToLower() is "pieces" or "weight" or "mileage" )
								{
									SelectedResultFormulaPricingObject = piece;
									SelectedResultFormulaMathOp        = pieces[ 5 ].Trim();
									piece                              = pieces[ 6 ].Replace( ")", "" );

									if( decimal.TryParse( piece, out var value ) )
									{
										SelectedResultFormulaMathValue = value;

										//  + (Weight * 0.5)
										SelectedResultFormulaAnotherClauseMathOp = pieces[ 7 ].Trim();
										//  (Weight * 0.5)
										piece = pieces[ 8 ].Trim().Substring( 1 );

										if( piece.ToLower() is "pieces" or "weight" or "mileage" )
										{
											SelectedResultFormulaSecondPricingObject = piece;
											SelectedResultFormulaSecondMathOp        = pieces[ 9 ].Trim();
											// 0.5)
											piece = pieces[ 10 ].Trim().Replace( ")", "" );

											if( decimal.TryParse( piece, out value ) )
												SelectedResultFormulaSecondMathValue = value;
										}
									}
								}
								break;

							case 13:
								//    0     1 2  3 4       5 6    7 8        9 0  1 2
								// 13 Total = 10 + (Pieces * 0.5) + ((Weight - 1) * 0.5)
								piece = piece.Substring( 1 );

								if( piece.ToLower() is "pieces" or "weight" or "mileage" )
								{
									SelectedResultFormulaPricingObject = piece;
									SelectedResultFormulaMathOp        = pieces[ 5 ].Trim();
									piece                              = pieces[ 6 ].Replace( ")", "" );

									if( decimal.TryParse( piece, out var value ) )
									{
										SelectedResultFormulaMathValue = value;

										//   + ((Weight - 1) * 0.5)
										SelectedResultFormulaAnotherClauseMathOp = pieces[ 7 ].Trim();
										//  ((Weight - 1) * 0.5)
										piece = pieces[ 8 ].Trim().Substring( 2 );

										if( piece.ToLower() is "pieces" or "weight" or "mileage" )
										{
											SelectedResultFormulaSecondPricingObject = piece + " " + pieces[ 9 ] + " ";
											// 1)
											piece = pieces[ 10 ].Trim().Replace( ")", "" );

											if( decimal.TryParse( piece, out value ) )
											{
												SelectedResultFormulaSecondPricingObjectValue = value;

												SelectedResultFormulaSecondMathOp = pieces[ 11 ].Trim();
												// 0.5)
												piece = pieces[ 12 ].Trim().Replace( ")", "" );

												if( decimal.TryParse( piece, out value ) )
													SelectedResultFormulaSecondMathValue = value;
											}
										}
									}
								}

								break;

							default:
								Logging.WriteLogLine( "ERROR Couldn't process " + line );

								break;
							}
						}
						else
						{
							switch( pieces.Length )
							{
							case 13:
								//    0     1 2  3 4        5 6  7 8     9 0       1 2
								// 13 Total = 10 + ((Pieces - 1) * 0.26) + (Weight * 0.5)
								piece = piece.Substring( 2 );

								if( piece.ToLower() is "pieces" or "weight" or "mileage" )
								{
									// Pieces -
									SelectedResultFormulaPricingObject = piece + " " + pieces[ 5 ] + " ";
									// 1)
									piece = pieces[ 6 ].Trim().Replace( ")", "" );

									if( decimal.TryParse( piece, out var value ) )
									{
										SelectedResultFormulaPricingObjectValue = value;
										// * 0.5
										SelectedResultFormulaMathOp = pieces[ 7 ].Trim();
										piece                       = pieces[ 8 ].Trim().Replace( ")", "" );

										if( decimal.TryParse( piece, out value ) )
										{
											SelectedResultFormulaMathValue = value;

											SelectedResultFormulaAnotherClauseMathOp = pieces[ 9 ].Trim();

											// (Weight * 0.5)
											piece = pieces[ 10 ].Substring( 1 );

											if( piece.ToLower() is "pieces" or "weight" )
											{
												SelectedResultFormulaSecondPricingObject = piece;

												SelectedResultFormulaSecondMathOp = pieces[ 11 ].Trim();
												piece                             = pieces[ 12 ].Trim().Replace( ")", "" );

												if( decimal.TryParse( piece, out value ) )
													SelectedResultFormulaSecondMathValue = value;
											}
										}
									}
								}
								break;

							case 15:
								//    0     1 2    3 4        5 6  7 8     9 0        1 2  3 4
								// 15 Total = 18.9 + ((Weight - 1) * 0.26) + ((Pieces - 1) * 5)
								piece = piece.Substring( 2 );

								if( piece.ToLower() is "pieces" or "weight" or "mileage" )
								{
									// Pieces -
									SelectedResultFormulaPricingObject = piece + " " + pieces[ 5 ] + " ";
									// 1)
									piece = pieces[ 6 ].Trim().Replace( ")", "" );

									if( decimal.TryParse( piece, out var value ) )
									{
										SelectedResultFormulaPricingObjectValue = value;
										// * 0.5
										SelectedResultFormulaMathOp = pieces[ 7 ].Trim();

										piece = pieces[ 8 ].Trim().Replace( ")", "" );

										if( decimal.TryParse( piece, out value ) )
										{
											SelectedResultFormulaMathValue = value;

											SelectedResultFormulaAnotherClauseMathOp = pieces[ 9 ].Trim();

											// ((Pieces - 1) * 5)
											piece = pieces[ 10 ].Substring( 2 );

											if( piece.ToLower() is "pieces" or "weight" or "mileage" )
											{
												SelectedResultFormulaSecondPricingObject = piece + " " + pieces[ 11 ] + " ";

												// 1)
												piece = pieces[ 12 ].Trim().Replace( ")", "" );

												if( decimal.TryParse( piece, out value ) )
												{
													SelectedResultFormulaSecondPricingObjectValue = value;

													SelectedResultFormulaSecondMathOp = pieces[ 13 ].Trim();
													piece                             = pieces[ 14 ].Trim().Replace( ")", "" );

													if( decimal.TryParse( piece, out value ) )
														SelectedResultFormulaSecondMathValue = value;
												}
											}
										}
									}
								}
								break;

							default:
								Logging.WriteLogLine( "ERROR Couldn't process " + line );

								break;
							}
						}
					}
				}
			}
			else if( line.ToLower().StartsWith( "endif" ) )
			{
				finishedConditions = true;
				continue;
			}

			if( finishedConditions )
			{
				// Done
				break;
			}
		}
	}

	public void BuildFormula()
	{
		// If Weight > 0 and Weight < 10 
		var formula = "Total = 0\nIf " + SelectedPricingObject + " " + SelectedCondition + " " + SelectedConditionLimit + " ";

		if( SelectedLogicOp.Trim() != string.Empty )
			formula += SelectedLogicOp.Trim() + " " + SelectedSecondPricingObject.Trim() + " " + SelectedSecondCondition.Trim() + " " + SelectedSecondConditionLimit + " ";

		if( IsResultValueChecked )
			formula += " Then\nTotal = " + ResultValue;
		else
		{
			//formula += "\nTotal = " + SelectedResultFormulaPricingObject.Trim() + " " + SelectedResultFormulaMathOp.Trim() + " " + SelectedResultFormulaMathValue;
			formula += " Then\nTotal = " + SelectedResultFormulaBasePrice + " + ";

			if( SelectedResultFormulaAnotherClauseMathOp.Trim() != string.Empty )
				formula += "(";

			if( ( SelectedResultFormulaPricingObject.IndexOf( "-" ) > -1 ) || ( SelectedResultFormulaPricingObject.IndexOf( "+" ) > -1 ) )
				formula += "(" + SelectedResultFormulaPricingObject.Trim() + " " + SelectedResultFormulaPricingObjectValue + ") ";
			else
			{
				//formula += "(" + SelectedResultFormulaPricingObject.Trim();
				formula += SelectedResultFormulaPricingObject.Trim();
			}
			formula += " " + SelectedResultFormulaMathOp.Trim() + " " + SelectedResultFormulaMathValue;

			if( SelectedResultFormulaAnotherClauseMathOp.Trim() != string.Empty )
			{
				formula += ") " + SelectedResultFormulaAnotherClauseMathOp.Trim() + " (";

				if( ( SelectedResultFormulaSecondPricingObject.IndexOf( "-" ) > -1 ) || ( SelectedResultFormulaSecondPricingObject.IndexOf( "+" ) > -1 ) )
					formula += "(" + SelectedResultFormulaSecondPricingObject.Trim() + " " + SelectedResultFormulaSecondPricingObjectValue + ") ";
				else
				{
					//formula += "(" + SelectedResultFormulaSecondPricingObject.Trim();
					formula += SelectedResultFormulaSecondPricingObject.Trim();
				}
				formula += " " + SelectedResultFormulaSecondMathOp.Trim() + " " + SelectedResultFormulaSecondMathValue + ")";
			}
		}

		formula = formula.Replace( "  ", " " );

		formula += "\nEndif\nResult = Total";
		Logging.WriteLogLine( "DEBUG formula:\n" + formula );

		NewFormula = formula;
	}

	public decimal EvaluateFormula( string formula, decimal pieces, decimal weight, decimal mileage, bool isResultValueChecked )
	{
		decimal result = 0;
		/*
		 Total = 0
		 If Weight < 3 And Pieces = 1 
		 Total = 5
		 Endif
		 Result = Total
		 */

		static bool EvaluateConditionLine( string condition, decimal pieces, decimal weight, decimal mileage )
		{
			var isTrue = false;

			// If Weight < 3 And Pieces = 1 
			var parts = condition.Split( ' ' );

			if( parts.Length == 4 )
				isTrue = EvaluateCondition( parts[ 1 ], parts[ 2 ], parts[ 3 ], pieces, weight, mileage );
			else if( parts.Length == 8 )
			{
				var first  = EvaluateCondition( parts[ 1 ], parts[ 2 ], parts[ 3 ], pieces, weight, mileage );
				var second = EvaluateCondition( parts[ 5 ], parts[ 6 ], parts[ 7 ], pieces, weight, mileage );

				if( parts[ 4 ].ToLower() == "and" )
					isTrue = first && second;
				else
					isTrue = first || second;
			}
			else
				Logging.WriteLogLine( "ERROR can't break the condition line into 4 or 8 pieces: " + condition );

			return isTrue;
		}

		static bool EvaluateCondition( string pricingObject, string condition, string stringValue, decimal pieces, decimal weight, decimal mileage )
		{
			var isTrue = false;

			if( decimal.TryParse( stringValue, out var value ) )
			{
				Logging.WriteLogLine( "DEBUG Found value: " + value );

				switch( condition )
				{
				case ">":
					isTrue = pricingObject == "Pieces" ? pieces > value : (pricingObject == "Weight" ? weight > value : mileage > value);
					break;

				case ">=":
						isTrue = pricingObject == "Pieces" ? pieces >= value : (pricingObject == "Weight" ? weight >= value : mileage >= value);
					break;

				case "=":
					isTrue = pricingObject == "Pieces" ? pieces == value : (pricingObject == "Weight" ? weight == value : mileage == value);
					break;

				case "<=":
					isTrue = pricingObject == "Pieces" ? pieces <= value : (pricingObject == "Weight" ? weight <= value : mileage <= value);
					break;

				case "<":
					isTrue = pricingObject == "Pieces" ? pieces < value : (pricingObject == "Weight" ? weight < value : mileage < value);
					break;
				}
			}
			else
				Logging.WriteLogLine( "ERROR Can't convert : " + stringValue );

			return isTrue;
		}

		static decimal CalculateResult( string formula, decimal pieces, decimal weight, decimal mileage, bool isResultValueChecked )
		{
			decimal answer = 0;

			var parts = formula.Split( ' ' );

			if( isResultValueChecked && ( parts.Length == 3 ) )
			{
				// Simple case - Total = 5
				if( decimal.TryParse( parts[ 2 ].Trim(), out var value ) )
					answer = value;
				else
					Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 2 ].Trim() );
			}
			else if( !isResultValueChecked && ( parts.Length == 5 ) )
			{
				// Simple formula
				// Total = Weight * 10
				if( decimal.TryParse( parts[ 4 ], out var value ) )
				{
					Logging.WriteLogLine( "DEBUG Found value: " + value );
					var pricingObject = parts[ 2 ];
					var op            = parts[ 3 ];

					switch( op )
					{
					case "*":
						answer = pricingObject == "Pieces" ? value * pieces : (pricingObject == "Weight" ? weight * value : mileage * value);
						break;

					case "+":
						answer = pricingObject == "Pieces" ? value + pieces : (pricingObject == "Weight" ? weight + value : mileage + value);
						break;

					case "/":
						answer = pricingObject == "Pieces" ? value / pieces : (pricingObject == "Weight" ? weight / value : mileage / value);
						break;

					case "-":
						answer = pricingObject == "Pieces" ? value - pieces : (pricingObject == "Weight" ? weight - value : mileage - value);
						break;
					}
				}
				else
					Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 4 ].Trim() );
			}

			return answer;
		}

		var isConditionTrue = false;
		var lines           = formula.Split( '\n' );

		// Skip over the variable declaration
		for( var i = 1; i < lines.Length; i++ )
		{
			var line = lines[ i ].Trim();
			Logging.WriteLogLine( "DEBUG line: " + line );

			if( line.ToLower().StartsWith( "if" ) )
			{
				isConditionTrue = EvaluateConditionLine( line, pieces, weight, mileage );
				continue;
			}

			if( line.ToLower().StartsWith( "total" ) )
			{
				if( isConditionTrue )
					result = CalculateResult( line, pieces, weight, mileage, isResultValueChecked );
				else
				{
					Logging.WriteLogLine( "DEBUG Condition was false" );
					break;
				}
			}
		}

		return result;
	}
#endregion
}