﻿namespace ViewModels.RateWizard;

public static class EvaluateRateFormulaHelper
{
	public static readonly string CUMULATIVE_BASE  = "Cumulative = ";
	public static readonly string CUMULATIVE_TRUE  = CUMULATIVE_BASE + "1";
	public static readonly string CUMULATIVE_FALSE = CUMULATIVE_BASE + "0";
	public static readonly string INIT_TOTAL       = "Total = 0";
	public static readonly string RESULT           = "Result = Total";
	public static readonly string CONNECTOR        = "// Else"; // Needed to break formula into parts
	public static readonly string LANE             = "Lane = ";


    public static decimal EvaluateFullFormula(string formula, decimal pieces, decimal weight, decimal mileage, bool isResultValueChecked)
    {
        decimal result = 0;
        //Logging.WriteLogLine("DEBUG formula: " + formula);

        bool isCumulative = false;
        if (formula.Contains(CUMULATIVE_TRUE))
        {
            isCumulative = true;
        }

        var segments = BreakFormulaIntoParts(formula);

        (List<decimal> segPieces, List<decimal> segWeight, List<decimal> segMileage, List<decimal> segPieces1, List<decimal> segWeight1, List<decimal> segMileage1) = GetValuesForSegments(segments, pieces, weight, mileage, isResultValueChecked);
		//for (int j = 0; j < segPieces.Count; j++)
		//{
		//    Logging.WriteLogLine("DEBUG segment# " + j + " segPieces: " + segPieces[j] + " segWeight: " + segWeight[j] + " segMileage: " + segMileage[j]);
		//}

		//decimal prevSegPieces = 0;
		//decimal prevSegWeight = 0;
		//decimal prevSegMileage = 0;
		decimal remainderPieces = 0;
		decimal remainderWeight = 0;
		decimal remainderMileage = 0;
		if (isResultValueChecked)
		{
			// NOTE Never cumulative
			bool matches = false;
            for (int i = 0; i < segments.Count; i++)
			{
				string segment = segments[i];
                foreach (string tmp in segment.Split('\n'))
                {
					if (tmp.ToLower().StartsWith("if "))
					{
						matches = EvaluateConditionLine(tmp, pieces, weight, mileage);
						continue;
					}
                    else if (matches && tmp.ToLower().StartsWith("total") && tmp.ToLower() != "total = 0")
                    {
                        result = CalculateResult(tmp, pieces, weight, mileage, isResultValueChecked);
						//Logging.WriteLogLine($"Segment: {segment.Trim()} matches - result: {result}");
                        break;
                    }
                }
				if (matches)
				{
					break;
				}
            }
		}
		else
		{
			for (int i = 0; i < segments.Count; i++)
			{
				var segment = segments[i];
				if (!isResultValueChecked)
				{
					//Logging.WriteLogLine("DEBUG segmentNumber: " + i + " segment: '" + segment.Replace('\n', ' ') + "' segPieces: "
					//						+ segPieces[i] + " segWeight: " + segWeight[i] + " segMileage: " + segMileage[i]);
				}

				if (!isResultValueChecked)
				{
					if (isCumulative)
					{
						var lines = segment.Split('\n');
						for (int j = 0; j < lines.Length; j++)
						{
							string line = lines[j].Trim();
							if (line.ToLower().StartsWith("if"))
							{
								decimal usePieces = 0;
								decimal useWeight = 0;
								decimal useMileage = 0;

								if (i == 0)
								{
									if (pieces <= segPieces[0])
									{
										usePieces = pieces;
										remainderPieces = 0;
									}
									else
									{
										usePieces = segPieces[i];
										remainderPieces = pieces - segPieces[0];
									}
									if (weight <= segWeight[0])
									{
										useWeight = weight;
										remainderWeight = 0;
									}
									else
									{
										useWeight = segWeight[i];
										remainderWeight = weight - segWeight[0];
									}
									if (mileage <= segMileage[0])
									{
										useMileage = mileage;
										remainderMileage = 0;
									}
									else
									{
										useMileage = segMileage[i];
										remainderMileage = mileage - segMileage[0];
									}
								}
								else
								{
									if (pieces <= segPieces[i])
									{
										usePieces = pieces - segPieces[i - 1];
										remainderPieces = 0;
									}
									else
									{
										usePieces = segPieces[i] - segPieces[i - 1];
										remainderPieces = pieces - segPieces[i];
									}
									if (weight <= segWeight[i])
									{
										useWeight = weight - segWeight[i - 1];
										remainderWeight = 0;
									}
									else
									{
										useWeight = segWeight[i] - segWeight[i - 1];
										remainderWeight = weight - segWeight[i];
									}
									if (mileage <= segMileage[i])
									{
										useMileage = mileage - segMileage[i - 1];
										remainderMileage = 0;
									}
									else
									{
										useMileage = segMileage[i] - segMileage[i - 1];
										remainderMileage = mileage - segMileage[i];
									}
								}

								//Logging.WriteLogLine("DEBUG Use amounts: usePieces: " + usePieces + " useWeight: " + useWeight + " useMileage: " + useMileage + "\nRemainders: remainderPieces: " + remainderPieces + " remainderWeight: " + remainderWeight + " remainderMileage: " + remainderMileage);
								foreach (string tmp in segment.Split('\n'))
								{
									if (tmp.ToLower().StartsWith("total") && tmp.ToLower() != "total = 0")
									{
										if (usePieces > 0 || useWeight > 0 || useMileage > 0)
										{
											result += CalculateResult(tmp, usePieces, useWeight, useMileage, isResultValueChecked);
										}
										else if (remainderPieces > 0 || remainderWeight > 0 || remainderMileage > 0)
										{
											result += CalculateResult(tmp, remainderPieces, remainderWeight, remainderMileage, isResultValueChecked);
										}
										Logging.WriteLogLine("DEBUG result: " + result);
										break;
									}
								}
							}
						}
					}
					else
					{
						foreach (string seg in segments)
						{
							//Logging.WriteLogLine("DEBUG segment: " + seg);
							result = EvaluateFormulaPiece(seg, pieces, weight, mileage, isResultValueChecked);
							if (result > 0)
							{
								break;
							}
						}
					}
				}
				//else
				//{
				//    foreach (string tmp in segment.Split('\n'))
				//    {
				//        if (tmp.ToLower().StartsWith("total") && tmp.ToLower() != "total = 0")
				//        {
				//            result += CalculateResult(tmp, pieces, weight, mileage, isResultValueChecked);
				//            break;
				//        }
				//    }
				//}
			}
		}

        Logging.WriteLogLine("DEBUG result: " + result);

        return result;
    }

	public static (List<decimal> segPieces, List<decimal> segWeight, List<decimal> segMileage, List<decimal> segPieces1, List<decimal> segWeight1, List<decimal> segMileage1) GetValuesForSegments(List<string> segments, decimal pieces, decimal weight, decimal mileage, bool isResultValueChecked)
	{
		List<decimal> segPieces = new();
		List<decimal> segWeight = new();
		List<decimal> segMileage = new();
		List<decimal> segPieces1 = new();
		List<decimal> segWeight1 = new();
		List<decimal> segMileage1 = new();

		/*

		Test Weight = 75

		Total = 0
		If Weight <= 25 
		Total = 0 + Weight * 10.5
		Endif
		Result = Total
		-- Weight s/b 25 
		-- Remainder s/b 50

		Total = 0
		If Weight <= 49 
		Total = 0 + Weight * 8.5
		Endif
		Result = Total
		-- Weight s/b 24
		-- Remainder s/b 26

		Total = 0
		If Weight <= 100 
		Total = 0 + Weight * 7.5
		Endif
		Result = Total
		-- Weight s/b 26
		-- Remainder s/b 0

		Total = 0
		If Weight <= 150 
		Total = 0 + Weight * 6.5
		Endif
		Result = Total
		-- Weight s/b 0
		-- Remainder s/b 0



		 */
		if (!isResultValueChecked)
		{
			for (int i = 0; i < segments.Count; i++)
			{
				string segment = segments[i];

				var lines = segment.Split( '\n' );
				foreach (string line in lines )
				{
					if (line.ToLower().StartsWith("if"))
					{
						// Do the first part
						// If Weight <= 25 Then
						var parts = line.Trim().Split(' ');
						//if (parts.Length == 4)
						if (parts.Length == 4 || parts.Length == 5)
						{
							var thing = parts[1];
							var stringValue = parts[3];
							if( decimal.TryParse( stringValue, out var value ) )
							{
								switch (thing)
								{
									case "Pieces":
										segPieces.Add(value);
										segPieces1.Add(0);
										segWeight.Add(0);
										segWeight1.Add(0);
										segMileage.Add(0);
										segMileage1.Add(0);

										break;
									case "Weight":
										segPieces.Add(0);
										segPieces1.Add(0);
										segWeight.Add(value);
										segWeight1.Add(0);
										segMileage.Add(0);
										segMileage1.Add(0);

										break;
									case "Mileage":
										segPieces.Add(0);
										segPieces1.Add(0);
										segWeight.Add(0);
										segWeight1.Add(0);
										segMileage.Add(value);
										segMileage1.Add(0);

										break;
									default:
										break;
								}
							}
							break;
						}
						// 0  1      2  3  4  5      6 7 8
						// If Weight <= 25 Or Pieces < 5 Then
						if (parts.Length >= 9)
						{
                            var thing = parts[1];
                            var stringValue = parts[3];
							int size = 0;
                            if (decimal.TryParse(stringValue, out var value))
                            {
                                switch (thing)
                                {
                                    case "Pieces":
                                        segPieces.Add(value);
										size = segPieces.Count;
                                        //segPieces1.Add(0);
                                        //segWeight.Add(0);
                                        //segWeight1.Add(0);
                                        //segMileage.Add(0);
                                        //segMileage1.Add(0);

                                        break;
                                    case "Weight":
                                        //segPieces.Add(0);
                                        //segPieces1.Add(0);
                                        segWeight.Add(value);
                                        size = segWeight.Count;
                                        //segWeight1.Add(0);
                                        //segMileage.Add(0);
                                        //segMileage1.Add(0);

                                        break;
                                    case "Mileage":
                                        //segPieces.Add(0);
                                        //segPieces1.Add(0);
                                        //segWeight.Add(0);
                                        //segWeight1.Add(0);
                                        segMileage.Add(value);
                                        size = segMileage.Count;
                                        //segMileage1.Add(0);

                                        break;
                                    default:
                                        break;
                                }
                            }

                            thing = parts[5];
							stringValue = parts[7];
							if (decimal.TryParse(stringValue, out value))
							{
								switch (thing)
								{
									case "Pieces":
										//segPieces.Add(0);
										segPieces1.Add(value);
                                        size = segPieces1.Count;
										//segWeight.Add(0);
										//segWeight1.Add(0);
										//segMileage.Add(0);
										//segMileage1.Add(0);

										break;
									case "Weight":
										//segPieces.Add(0);
										//segPieces1.Add(0);
										//segWeight.Add(0);
										segWeight1.Add(value);
                                        size = segWeight1.Count;
										//segMileage.Add(0);
										//segMileage1.Add(0);

										break;
									case "Mileage":
										//segPieces.Add(0);
										//segPieces1.Add(0);
										//segWeight.Add(0);
										//segWeight1.Add(0);
										//segMileage.Add(0);
										segMileage1.Add(value);
                                        size = segMileage1.Count;

										break;
									default:
										break;
								}
							}

							// Now, make sure all arrays have the same number of elements
							if (segPieces.Count != size)
							{
								segPieces.Add(0);
							}
                            if (segPieces1.Count != size)
                            {
                                segPieces1.Add(0);
                            }
                            if (segWeight.Count != size)
                            {
                                segWeight.Add(0);
                            }
                            if (segWeight1.Count != size)
                            {
                                segWeight1.Add(0);
                            }
                            if (segMileage.Count != size)
                            {
                                segMileage.Add(0);
                            }
                            if (segMileage1.Count != size)
                            {
                                segMileage1.Add(0);
                            }

                            break;
						}
					}
				}
			}
		}


		return (segPieces, segWeight, segMileage, segPieces1, segWeight1, segMileage1);
	}

	public static string GetFormulaForLane(string formula, int lane)
	{
		string section = string.Empty;

		if (formula.IsNotNullOrWhiteSpace() && lane > 0)
		{
			string[] pieces = formula.Split('\n');
			if (pieces.Length > 0)
			{
				bool inside = false;
				string boundary = "Lane =";
				string toMatch = boundary + " " + lane;
				foreach (string piece in pieces)
				{
					if (piece == toMatch)
					{
						section += piece + "\n";
						inside = true;
					}
					else if (piece.StartsWith(boundary) && inside)
					{
						// Done
						break;
					}
					else if (inside)
					{
                        section += piece + "\n";
					}
				}
			}
		}

		//Logging.WriteLogLine("DEBUG Found formula for Lane: " + lane + ": " + section.Replace('\n', ' '));
		return section.Trim();
	}

	public static decimal GetValueFromCondition(string condition, decimal startingValue)
	{
		decimal value = 0;
		var parts = condition.Split(' ');
		if ( parts.Length == 4)
		{
			var stringValue = parts[3];
			if (decimal.TryParse(stringValue, out value))
			{

			}
		}

		return value;
	}

    public static bool IsFormula(string formula)
    {
        var yes = false;

        var pieces = formula.Split('\n');

        foreach (var piece in pieces)
        {
            if (piece.StartsWith("Total =") && piece.Contains("+"))
            {
                yes = true;
                break;
            }
        }

        return yes;
    }

    public static decimal EvaluateFormulaPiece( string formula, decimal pieces, decimal weight, decimal mileage, bool isResultValueChecked)
	{
		decimal result = 0;
		/*
		 Cumulative = 0
		 Total = 0
		 If Weight < 3 And Pieces = 1 
		 Total = 5
		 Endif
		 Result = Total
		 */
		var isConditionTrue = false;
		var lines           = formula.Split( '\n' );

		for( var i = 0; i < lines.Length; i++ )
		{
			var line = lines[ i ].Trim();
			// Skip over the variable declarations
			if (line.Contains(CUMULATIVE_BASE)) 
			{
				//Logging.WriteLogLine("DEBUG Skipping line " + line);
				continue;
			}

			if (line.Contains(INIT_TOTAL) && line.Length == INIT_TOTAL.Length)
			{
				//Logging.WriteLogLine("DEBUG Skipping line " + line);
				continue;
			}

			if ( line.ToLower().StartsWith( "if" ) )
			{
				isConditionTrue = EvaluateConditionLine( line, pieces, weight, mileage );
				continue;
			}

			if( line.ToLower().StartsWith( "total" ) )
			{
				if( isConditionTrue )
					result = CalculateResult( line, pieces, weight, mileage, isResultValueChecked );
				else
				{
					//Logging.WriteLogLine( "DEBUG Condition was false" );
					break;
				}
			}
		}

		return result;
	}

	public static List<string> BreakFormulaIntoParts(string formula)
	{
		List<string> list = new();

		//Logging.WriteLogLine("DEBUG formula: " + formula);
		// "Lane = 1\nCumulative = 0\nTotal = 0\nIf Pieces <= 5 \nTotal = 10\nEndif\n\\ Else If Pieces <= 10 \nTotal = 20\nEndif\nResult = Total"
		//  ^^^^^^^^^^																 ^^^^^^^
		var segments = formula.Split(new[] { CONNECTOR }, StringSplitOptions.None);
		if (segments.Length > 0)
		{
			foreach (var segment in segments)
			{
				var seg = segment.Trim();

				//if( !seg.StartsWith( INIT_TOTAL ) )
				if (!seg.Contains(INIT_TOTAL))
					seg = INIT_TOTAL + "\n" + seg;

				if (!seg.Contains(RESULT))
					seg += "\n" + RESULT;

				//Logging.WriteLogLine("DEBUG segment: " + seg);
				list.Add(seg);
			}
		}

		return list;
	}
	public static List<string> BreakFormulaIntoParts_V1( string formula )
	{
		List<string> list = new();

		Logging.WriteLogLine( "DEBUG formula: " + formula );
		// "Cumulative = 0\nTotal = 0\nIf Pieces <= 5 \nTotal = 10\nEndif\n Else If Pieces <= 10 \nTotal = 20\nEndif\nResult = Total"
		var segments = formula.Split( new[] {CONNECTOR}, StringSplitOptions.None );
		if (segments.Length > 0)
		{
			foreach( var segment in segments )
			{
				var seg = segment.Trim();

				//if( !seg.StartsWith( INIT_TOTAL ) )
				if( !seg.Contains( INIT_TOTAL ) )
					seg = INIT_TOTAL + "\n" + seg;

				if( !seg.Contains( RESULT ) )
					seg += "\n" + RESULT;

				Logging.WriteLogLine( "DEBUG segment: " + seg );
				list.Add( seg );
			}
		}

		return list;
	}

	public static string BuildFullFormula( List<string> parts, bool isCumulative )
	{
		var full = string.Empty;
		var total  = INIT_TOTAL + "\n";
		//var result = "Result = Total";

		//foreach( var part in parts )
		for (int i = 0; i < parts.Count; i++)
		{
			string part = parts[i];
			if( full == string.Empty )
			{
				// Cumulative only occurs at beginning of full formula
				if (part.Contains(CUMULATIVE_TRUE) && !isCumulative)
				{
					part = part.Replace(CUMULATIVE_TRUE, CUMULATIVE_FALSE);
				}
				else if (part.Contains(CUMULATIVE_FALSE) && isCumulative)
				{
					part = part.Replace(CUMULATIVE_FALSE, CUMULATIVE_TRUE);
				}
				else if (!part.Contains(CUMULATIVE_BASE))
				{
					// No Cumulative
					if (isCumulative)
					{
						part = CUMULATIVE_TRUE + "\n" + part;
					}
					else
					{
						part = CUMULATIVE_FALSE + "\n" + part;
					}
				}
				var stripped = part.Replace( RESULT, "" );
				stripped = RemoveLane(stripped).Trim();
				full = stripped;
			}
			else
			{
				var stripped = part.Replace( total, "" );
				stripped =  stripped?.Replace( RESULT, "" );
				//full     += " Else " + stripped;
				//full += stripped;
				full += CONNECTOR + "\n" + stripped;
			}
		}

		if( full.IndexOf( RESULT ) == -1 )
			full += "\n" + RESULT;
		//Logging.WriteLogLine( "DEBUG full:\n" + full + "\n" );
		return full;
	}


	public static string RemoveLane(string part)
	{
		string result = part;
		if (part.IsNotNullOrWhiteSpace() && part.Contains(LANE))
		{
			string[] pieces = part.Split('\n');
			if (pieces.Length > 1)
			{
				foreach (string piece in pieces)
				{
					if (piece.StartsWith(LANE))
					{
						result = part.Replace(piece, "");
						break;
					}
				}
			}
		}

		return result;
	}

    private static bool EvaluateConditionLine(string condition, decimal pieces, decimal weight, decimal mileage, decimal pieces1 = 0, decimal weight1 = 0, decimal mileage1 = 0)
    {
        var isTrue = false;

        // If Weight < 3 Then
        var parts = condition.Split(' ');

        if (parts.Length == 4 || parts.Length == 5)
            isTrue = EvaluateCondition(parts[1], parts[2], parts[3], pieces, weight, mileage);
        // If Weight < 3 And Pieces = 1 Then
        else if (parts.Length >= 8)
        {
            var first = EvaluateCondition(parts[1], parts[2], parts[3], pieces, weight, mileage);
            var second = EvaluateCondition(parts[5], parts[6], parts[7], pieces1, weight1, mileage1);

            if (parts[4].ToLower() == "and")
                isTrue = first && second;
            else
                isTrue = first || second;
        }
        else
            Logging.WriteLogLine("ERROR can't break the condition line into 4 or 8+ pieces: " + condition);

        return isTrue;
    }

    private static bool EvaluateConditionLine_V1( string condition, decimal pieces, decimal weight, decimal mileage )
	{
		var isTrue = false;

		// If Weight < 3 Then
		var parts = condition.Split( ' ' );

		if( parts.Length == 4 || parts.Length == 5 )
			isTrue = EvaluateCondition( parts[ 1 ], parts[ 2 ], parts[ 3 ], pieces, weight, mileage );
		// If Weight < 3 And Pieces = 1 Then
		else if( parts.Length >= 8 )
		{
			var first  = EvaluateCondition( parts[ 1 ], parts[ 2 ], parts[ 3 ], pieces, weight, mileage );
			var second = EvaluateCondition( parts[ 5 ], parts[ 6 ], parts[ 7 ], pieces, weight, mileage );

			if( parts[ 4 ].ToLower() == "and" )
				isTrue = first && second;
			else
				isTrue = first || second;
		}
		else
			Logging.WriteLogLine( "ERROR can't break the condition line into 4 or 8+ pieces: " + condition );

		return isTrue;
	}

	private static bool EvaluateCondition( string pricingObject, string condition, string stringValue, decimal pieces, decimal weight, decimal mileage )
	{
		var isTrue = false;

		if( decimal.TryParse( stringValue, out var value ) )
		{
			//Logging.WriteLogLine( "DEBUG Found value: " + value );

			switch( condition )
			{
			case ">":
				//isTrue = pricingObject == "Pieces" ? pieces > value : weight > value;
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces > value;
					break;

				case "Weight":
					isTrue = weight > value;
					break;

				case "Mileage":
					isTrue = mileage > value;
					break;
				}
				break;

			case ">=":
				//isTrue = pricingObject == "Pieces" ? pieces >= value : weight >= value;
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces >= value;
					break;

				case "Weight":
					isTrue = weight >= value;
					break;

				case "Mileage":
					isTrue = mileage >= value;
					break;
				}
				break;

			case "=":
				//isTrue = pricingObject == "Pieces" ? pieces == value : weight == value;
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces == value;
					break;

				case "Weight":
					isTrue = weight == value;
					break;

				case "Mileage":
					isTrue = mileage == value;
					break;
				}
				break;

			case "<=":
				//isTrue = pricingObject == "Pieces" ? pieces <= value : weight <= value;
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces <= value;
					break;

				case "Weight":
					isTrue = weight <= value;
					break;

				case "Mileage":
					isTrue = mileage <= value;
					break;
				}
				break;

			case "<":
				//isTrue = pricingObject == "Pieces" ? pieces < value : weight < value;
				switch( pricingObject )
				{
				case "Pieces":
					isTrue = pieces < value;
					break;

				case "Weight":
					isTrue = weight < value;
					break;

				case "Mileage":
					isTrue = mileage < value;
					break;
				}
				break;
			}
		}
		else
			Logging.WriteLogLine( "ERROR Can't convert : " + stringValue );

		return isTrue;
	}


	private static decimal CalculateResult( string formula, decimal pieces, decimal weight, decimal mileage, bool isResultValueChecked )
	{
		decimal answer = 0;

		var parts = formula.Split( ' ' );

		if( isResultValueChecked && ( parts.Length == 3 ) )
		{
			// Simple case - Total = 5
			if( decimal.TryParse( parts[ 2 ].Trim(), out var value ) )
				answer = value;
			else
				Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 2 ].Trim() );
		}
		else if( !isResultValueChecked )
		{
			if( parts.Length == 7 )
				answer = CalculateResultSimple( parts, pieces, weight, mileage );
			else if( parts.Length > 7 )
				answer = CalculateResultComplex( parts, pieces, weight, mileage );
		}

		if (answer < 0)
		{
			answer = 0;
		}

		return answer;
	}

	private static decimal CalculateResultSimple( string[] parts, decimal pieces, decimal weight, decimal mileage )
	{
		decimal answer    = 0;
		decimal basePrice = 0;

		// Simple formula
		// 7 0     1 2  3 4      5 6
		//   Total = 10 + Pieces * 0.5
		if( decimal.TryParse( parts[ 2 ].Trim(), out var value ) )
		{
			//Logging.WriteLogLine( "DEBUG basePrice: " + value );
			basePrice = value;

			if( decimal.TryParse( parts[ 6 ], out value ) )
			{
				//Logging.WriteLogLine( "DEBUG Found value: " + value );
				var pricingObject = parts[ 4 ];
				var op            = parts[ 5 ];

				switch( op )
				{
				case "*":
					//answer = pricingObject == "Pieces" ? value * pieces : value * weight;
					switch( pricingObject )
					{
					case "Pieces":
						answer = pieces * value;
						break;

					case "Weight":
						answer = weight * value;
						break;

					case "Mileage":
						answer = mileage * value;
						break;
					}
					break;

				case "+":
					//answer = pricingObject == "Pieces" ? value + pieces : value + weight;
					switch( pricingObject )
					{
					case "Pieces":
						answer = pieces + value;
						break;

					case "Weight":
						answer = weight + value;
						break;

					case "Mileage":
						answer = mileage + value;
						break;
					}
					break;

				case "/":
					//answer = pricingObject == "Pieces" ? value / pieces : value / weight;
					switch( pricingObject )
					{
					case "Pieces":
						answer = pieces / value;
						break;

					case "Weight":
						answer = weight / value;
						break;

					case "Mileage":
						answer = mileage / value;
						break;
					}
					break;

				case "-":
					//answer = pricingObject == "Pieces" ? value - pieces : value - weight;
					switch( pricingObject )
					{
					case "Pieces":
						answer = pieces - value;
						break;

					case "Weight":
						answer = weight - value;
						break;

					case "Mileage":
						answer = mileage - value;
						break;
					}
					break;
				}
			}
			else
				Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 6 ].Trim() );
		}
		else
			Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 2 ].Trim() );

		answer += basePrice;

		return answer;
	}

	private static decimal CalculateResultComplex( string[] parts, decimal pieces, decimal weight, decimal mileage )
	{
		decimal answer    = 0;
		decimal basePrice = 0;

		if( decimal.TryParse( parts[ 2 ].Trim(), out var value ) )
		{
			//Logging.WriteLogLine( "DEBUG basePrice: " + value );
			basePrice = value;
		}
		else
			Logging.WriteLogLine( "ERROR This isn't a number: " + parts[ 2 ].Trim() );

		if( parts[ 4 ].IndexOf( "((" ) == -1 )
		{
			switch( parts.Length )
			{
			case 9:
				//    0     1 2  3 4       5 6  7 8
				//  9 Total = 10 + (Pieces - 1) * 0.5
				var pricingObject = parts[ 4 ].Substring( 1 );
				var op            = parts[ 5 ];
				var part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );
					Logging.WriteLogLine( "DEBUG answer: " + answer );
					op   = parts[ 7 ];
					part = parts[ 8 ].Trim();

					if( decimal.TryParse( part, out value ) )
						answer = SimpleCalc( answer, op, value );
				}
				break;

			case 11:
				//    0     1 2  3 4        5 6    7 8       9 0
				// 11 Total = 10 + (Pieces  * 0.5) + (Weight * 0.5)
				pricingObject = parts[ 4 ].Substring( 1 );
				op            = parts[ 5 ];
				part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

					// Second clause
					// + (Weight * 0.5)
					pricingObject = parts[ 8 ].Substring( 1 );
					op            = parts[ 9 ];
					part          = parts[ 10 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						var clause = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

						op     = parts[ 7 ];
						answer = SimpleCalc( answer, op, clause );
					}
				}
				break;

			case 13:
				//    0     1 2  3 4       5 6    7 8        9 0  1 2
				// 13 Total = 10 + (Pieces * 0.5) + ((Weight - 1) * 0.5)
				pricingObject = parts[ 4 ].Substring( 1 );
				op            = parts[ 5 ];
				part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

					// Second clause
					// + ((Weight - 1) * 0.5)
					pricingObject = parts[ 8 ].Substring( 2 );
					op            = parts[ 9 ];
					part          = parts[ 10 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						var clause = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

						op   = parts[ 11 ];
						part = parts[ 12 ].Replace( ")", "" );

						if( decimal.TryParse( part, out value ) )
						{
							clause = SimpleCalc( clause, op, value );

							op     = parts[ 7 ];
							answer = SimpleCalc( answer, op, clause );
						}
					}
				}
				break;
			}
		}
		else
		{
			switch( parts.Length )
			{
			case 13:
				//    0     1 2  3 4        5 6  7 8     9 0       1 2
				// 13 Total = 10 + ((Pieces - 1) * 0.26) + (Weight * 0.5)
				var pricingObject = parts[ 4 ].Substring( 1 );
				var op            = parts[ 5 ];
				var part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

					op   = parts[ 7 ];
					part = parts[ 8 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						answer = SimpleCalc( answer, op, value );

						// Second clause
						// + (Weight * 0.5)
						pricingObject = parts[ 10 ].Substring( 1 );
						op            = parts[ 11 ];
						part          = parts[ 12 ].Replace( ")", "" );

						if( decimal.TryParse( part, out value ) )
						{
							var clause = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

							op     = parts[ 9 ];
							answer = SimpleCalc( answer, op, clause );
						}
					}
				}
				break;

			case 15:
				//    0     1 2  3 4        5 6  7 8     9 0        1 2  3 4
				// 15 Total = 10 + ((Weight - 1) * 0.26) + ((Pieces - 1) * 5)
				pricingObject = parts[ 4 ].Substring( 2 );
				op            = parts[ 5 ];
				part          = parts[ 6 ].Replace( ")", "" );

				if( decimal.TryParse( part, out value ) )
				{
					answer = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

					op   = parts[ 7 ];
					part = parts[ 8 ].Replace( ")", "" );

					if( decimal.TryParse( part, out value ) )
					{
						answer = SimpleCalc( answer, op, value );

						// Second clause
						//  9 0        1 2  3 4
						//  + ((Pieces - 1) * 5)
						pricingObject = parts[ 10 ].Substring( 2 );
						op            = parts[ 11 ];
						part          = parts[ 12 ].Replace( ")", "" );

						if( decimal.TryParse( part, out value ) )
						{
							var clause = CalcPiecesWeightMileage( pricingObject, pieces, weight, mileage, op, value );

							op   = parts[ 13 ];
							part = parts[ 14 ].Replace( ")", "" );

							if( decimal.TryParse( part, out value ) )
							{
								clause = SimpleCalc( clause, op, value );

								op     = parts[ 9 ];
								answer = SimpleCalc( answer, op, clause );
							}
						}
					}
				}
				break;
			}
		}

		answer += basePrice;

		return answer;
	}

	private static decimal CalcPiecesWeightMileage( string pricingObject, decimal pieces, decimal weight, decimal mileage, string op, decimal value )
	{
		decimal answer = 0;

		switch( op )
		{
		case "*":
			//answer = pricingObject == "Pieces" ? pieces * value : weight * value;
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces * value;
				break;

			case "Weight":
				answer = weight * value;
				break;

			case "Mileage":
				answer = mileage * value;
				break;
			}
			break;

		case "+":
			//answer = pricingObject == "Pieces" ? pieces + value : weight + value;
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces + value;
				break;

			case "Weight":
				answer = weight + value;
				break;

			case "Mileage":
				answer = mileage + value;
				break;
			}
			break;

		case "/":
			//answer = pricingObject == "Pieces" ? pieces / value : weight / value;
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces / value;
				break;

			case "Weight":
				answer = weight / value;
				break;

			case "Mileage":
				answer = mileage / value;
				break;
			}
			break;

		case "-":
			//answer = pricingObject == "Pieces" ? pieces - value : weight - value;
			switch( pricingObject )
			{
			case "Pieces":
				answer = pieces - value;
				break;

			case "Weight":
				answer = weight - value;
				break;

			case "Mileage":
				answer = mileage - value;
				break;
			}
			break;
		}

		return answer;
	}

	private static decimal SimpleCalc( decimal previous, string op, decimal value )
	{
		var answer = previous;

		switch( op )
		{
		case "*":
			answer *= value;
			break;

		case "+":
			answer += value;
			break;

		case "/":
			answer /= value;
			break;

		case "-":
			answer -= value;
			break;
		}

		return answer;
	}
}