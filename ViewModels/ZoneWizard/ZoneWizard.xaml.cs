﻿using System.IO;
using System.Threading;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Input;
using DocumentFormat.OpenXml.EMMA;
using Microsoft.Win32;
using Protocol.Data._Customers.Pml;
using Cursors = System.Windows.Input.Cursors;
using File = System.IO.File;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

//using Utils;

namespace ViewModels.ZoneWizard;

/// <summary>
///     Interaction logic for ZoneWizard.xaml
/// </summary>
public partial class ZoneWizard : Page
{
	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public ZoneWizard()
	{
		SetWaitCursor();
		if( DataContext is ZoneWizardModel Model )
		{
			Model.SetView(this);
			Model.IsPml = Model.GetPmlPreference();
			Model.Loaded = false;

			
		}
		InitializeComponent();

		//dgZones.SelectedIndex = 0;

		ClearWaitCursor();
	}

	public void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	public void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}
	private void btnOpenHelp_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
			Model.Execute_ShowHelp();
	}

	private void BtnBrowse_Click( object sender, RoutedEventArgs e )
	{
		var filter = "CSV Files (*.csv)|*.csv|Text Files (*.txt)|*txt";

		var ofd = new OpenFileDialog
		          {
			          Filter = filter
		          };
		ofd.Multiselect = false;
		var result = ofd.ShowDialog();

		if( result == true )
		{
			Logging.WriteLogLine( "Selected " + ofd.FileName );
			var file = ofd.FileName;

			if( File.Exists( file ) )
			{
				if( DataContext is ZoneWizardModel Model )
				{
					tbFileName.Text     = Model.SelectedImportFile    = file;
					btnImport.IsEnabled = Model.IsImportButtonEnabled = true;
				}
			}
			else
			{
				if( DataContext is ZoneWizardModel Model )
				{
					tbFileName.Text     = Model.SelectedImportFile    = string.Empty;
					btnImport.IsEnabled = Model.IsImportButtonEnabled = false;
				}
			}
		}
	}

	private void BtnImport_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
		{
			Logging.WriteLogLine( "About to import zone file: " + Model.SelectedImportFile );

			Mouse.OverrideCursor = Cursors.Wait;
			var (success, error) = Model.Execute_ImportZonesFile();

			if( !success )
			{
				// Display error
				var title   = FindStringResource( "ZoneWizardImportErrorTitle" );
				var message = FindStringResource( "ZoneWizardImportErrorMessage" );
				message += "\n\n" + error;

				MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
			}
			else
			{
				if (error.IsNullOrWhiteSpace())
				{
					var title = FindStringResource("ZoneWizardImportFinishedTitle");
					var message = FindStringResource("ZoneWizardImportFinishedMessage");

					MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
				}
				else
				{
                    // Display error
                    var title = FindStringResource("ZoneWizardImportErrorTitle");
                    var message = FindStringResource("ZoneWizardImportErrorLinesDuplicateSkippedMessage");
					message += "\n" + FindStringResource( "ZoneWizardImportErrorLinesDuplicateSkippedMessage1" );
                    message += "\n\n" + error;

                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
                }
			}
			tbFileName.Text     = Model.SelectedImportFile    = string.Empty;
			btnImport.IsEnabled = Model.IsImportButtonEnabled = false;

			Model.GetRoutes();

			Mouse.OverrideCursor = null;
		}
	}
		
	private void BtnNew_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
		{
			//Model.SelectedRoute = new PmlRoute();
			//Model.SelectedRoute = new PostcodesToZones();
			Model.SelectedRoute = new();

			Model.IsEdit = true;
			Model.IsNew  = true;

			ToggleEdit( Model.IsEdit );

			//tbRoute.Focus();
			CbRoute.Focus();
		}
	}

	private void BtnEdit_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
		{
			if( Model.SelectedRoute != null )
				Model.IsEdit = true;
			else
			{
				if( Model.IsPml && dgPmlZones.SelectedIndex > -1 )
				{
					// For some reason, SelectedRoute is null so finding it again
					var route = ( from R in Model.RawRoutesPml
					                        //where R.IdsRoute == Model.IdsRoute
					                        where R.IdsRoute == Model.RouteName
					                        select R ).First();
					if ( route != null )
					{
						Model.SelectedRoute = Model.ConvertRoutePml( route );
					}
					Model.IsEdit = true;
				}
				else if (Model.IsGeneric && dgGenericZones.SelectedIndex > -1)
				{
					// For some reason, SelectedRoute is null so finding it again
					var route = (from R in Model.RawRoutesGeneric
											   //where R.IdsRoute == Model.IdsRoute
										   where R.RouteName == Model.RouteName
										   select R).First();
					if (route != null)
					{
						Model.SelectedRoute = Model.ConvertRouteGeneric( route );
					}
					Model.IsEdit = true;
				}
			}

			ToggleEdit( Model.IsEdit );

			//tbRoute.Focus();
			CbRoute?.Focus();
		}
	}

	private void BtnDelete_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
		{
			if( Model.SelectedRoute == null )
			{
				if( Model.IsPml && dgPmlZones.SelectedIndex > -1 )
				{
					// For some reason, SelectedRoute is null so finding it again
					var route = ( from R in Model.RawRoutesPml
					                        //where R.IdsRoute == Model.IdsRoute
					                        where R.IdsRoute == Model.RouteName
					                        select R ).First();
					if (route != null)
					{
						Model.SelectedRoute = Model.ConvertRoutePml(route);
					}
				}
				else if (!Model.IsPml && dgGenericZones.SelectedIndex > -1)
				{
					// For some reason, SelectedRoute is null so finding it again
					var route = (from R in Model.RawRoutesGeneric
									 //where R.IdsRoute == Model.IdsRoute
								 where R.RouteName == Model.RouteName
								 select R).First();
					if (route != null)
					{
						Model.SelectedRoute = Model.ConvertRouteGeneric(route);
					}
				}
			}

			//if( ( Model.SelectedRoute != null ) && Model.SelectedRoute.IdsRoute.IsNotNullOrWhiteSpace() )
			if( ( Model.SelectedRoute != null ) && Model.SelectedRoute.Name.IsNotNullOrWhiteSpace() )
			{
				var title   = FindStringResource( "ZoneWizardDeleteTitle" );
				var message = FindStringResource( "ZoneWizardDeleteMessageQuestion" );
				//message = message.Replace( "@1", Model.SelectedRoute.IdsRoute );
				message = message.Replace( "@1", Model.SelectedRoute.Name );
				var mbr = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );

				if( mbr == MessageBoxResult.Yes )
				{
					var success = Model.DeleteRoute();

					if( success )
					{
						title   = FindStringResource( "ZoneWizardDeleteTitle" );
						message = FindStringResource( "ZoneWizardDeleteMessageDeleted" );
						MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );
					}

					Thread.Sleep( 500 );
					Model.GetRoutes();
				}
			}
		}
	}

	private void BtnSave_Click( object sender, RoutedEventArgs e )
	{
		var (ok, error) = ValidateFields();

		if( ok && DataContext is ZoneWizardModel Model )
		{
			var (success, error2) = Model.SaveRoute();

			if( success )
			{
				// Clear
				//Model.SelectedRoute = new PmlRoute();
				Model.SelectedRoute = new();

				Model.AreTextboxesReadOnly = true;
				Model.IsNew                = false;
				Model.IsEdit               = false;

				ClearErrorConditions();

				ToggleEdit( Model.IsEdit );
				Thread.Sleep( 500 );
				Model.GetRoutes();

				var title   = FindStringResource( "ZoneWizardSavedTitle" );
				var message = FindStringResource( "ZoneWizardSavedMessage" );
				MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );
			}
			else
			{
				Logging.WriteLogLine( "Error saving route: " + error2 );
				var title   = FindStringResource( "ZoneWizardErrorTitle" );
				var message = FindStringResource( "ZoneWizardErrorMessage" ) + "\r\n";
				message += error2;
				MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
			}
		}
		else
		{
			Logging.WriteLogLine( "Error saving route: " + error );
			var title   = FindStringResource( "ZoneWizardErrorTitle" );
			var message = FindStringResource( "ZoneWizardErrorMessage" ) + "\r\n";
			message += error;
			MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
		}
	}

	private (bool ok, string error) ValidateFields()
	{
		var ok    = true;
		var error = string.Empty;

		if (DataContext is ZoneWizardModel Model)
		{
			// CD1-T133 Route is no longer required
			//if (tbRoute.Text.Trim().Length == 0)
			//{
			//	ok                 =  false;
			//	error              += "\r\n" + FindStringResource("ZoneWizardErrorRouteCantBeEmpty");
			//	tbRoute.Background =  Brushes.Yellow;
			//}
			//if (tbOperation.Text.Trim().Length == 0)
			//{
			//	ok                     =  false;
			//	error                  += "\r\n" + FindStringResource("ZoneWizardErrorZoneCantBeEmpty");
			//	tbOperation.Background =  Brushes.Yellow;
			//}
			if (Model.IsPml && tbRegion.Text.Trim().Length == 0)
			{
				ok = false;
				error += "\r\n" + FindStringResource("ZoneWizardErrorRegionCantBeEmpty");
				tbRegion.Background = Brushes.Yellow;
			}
			if( tbPostalCode.Text.Trim().Length == 0 )
			{
				ok                      =  false;
				error                   += "\r\n" + FindStringResource( "ZoneWizardErrorPostalCodeCantBeEmpty" );
				tbPostalCode.Background =  Brushes.Yellow;
			}

			if( ok )
				ClearErrorConditions();

		}

		return ( ok, error );
	}

	private void ClearErrorConditions()
	{
		//tbRoute.Background      = Brushes.White;
		//tbOperation.Background  = Brushes.White;
		tbRegion.Background  = Brushes.White;
		tbPostalCode.Background = Brushes.White;
	}

	private void BtnCancel_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
		{
			// Reload values
			//Model.IdsRoute   = Model.SelectedRoute?.IdsRoute;
			Model.RouteName   = Model.SelectedRoute?.Name;
			Model.ZoneName	  = Model.SelectedRoute?.Zone;
			Model.Region     = Model.SelectedRoute?.Zone2;
			Model.PostalCode = Model.SelectedRoute?.PostalCode;

			Model.AreTextboxesReadOnly = true;
			Model.WhenSelectedRouteChanges();
			Model.IsNew  = false;
			Model.IsEdit = false;

			ClearErrorConditions();

			ToggleEdit( Model.IsEdit );
		}
	}

	private void ToggleEdit( bool editing )
	{
		//tbRoute.IsReadOnly      = !editing;
		CbRoute.IsReadOnly = !editing;
		CbRoute.IsEnabled  = editing;
		//tbOperation.IsReadOnly  = !editing;
		CbOperation.IsReadOnly  = !editing;
		CbOperation.IsEnabled   = editing;
		tbRegion.IsReadOnly = !editing;
		tbPostalCode.IsReadOnly = !editing;

		btnNew.IsEnabled    = !editing;
		btnEdit.IsEnabled   = !editing;
		btnDelete.IsEnabled = !editing;
	}

	private void tbAdhocFilter_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( DataContext is ZoneWizardModel Model )
		{
			Model.AdhocFilter = tbAdhocFilter.Text;
			//Logging.WriteLogLine( "DEBUG AdhocFilter: " + Model.AdhocFilter );
		}
	}

	private void dgZones_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		//BtnEdit_Click(sender, e);
	}

	private void CbOperation_GotFocus(object sender, RoutedEventArgs e)
	{
		CbOperation.IsDropDownOpen = true;
	}

	private void CbRoute_GotFocus(object sender, RoutedEventArgs e)
	{
		CbRoute.IsDropDownOpen= true;
	}

	private void tbAdhocFilter_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (DataContext is ZoneWizardModel Model)
		{
			if (e.Key == Key.Escape)
			{
				tbAdhocFilter.Text = string.Empty;
			}
			Model.AdhocFilter = tbAdhocFilter.Text;
			//Logging.WriteLogLine( "DEBUG AdhocFilter: " + Model.AdhocFilter );
		}
	}

    private void BtnExport_Click(object sender, RoutedEventArgs e)
    {
        if (DataContext is ZoneWizardModel Model)
		{
            var filter = "CSV Files (*.csv)|*.csv";
			SaveFileDialog sfd = new()
			{
				Filter     = filter,
                DefaultExt = ".csv",
			};

            bool? result = sfd.ShowDialog();
			if ((bool)result)
			{
				string fileName = sfd.FileName;
                var Fi = new FileInfo( fileName );

				(bool success, string error) = Model.ExportZonesFile(fileName);
				if (success)
				{
					string title = FindStringResource("ZoneWizardExportTitle");
					string message = FindStringResource("ZoneWizardExportMessage");
					message = message.Replace("@1", fileName);
					MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
				}
				else
				{
                    string title = FindStringResource("ZoneWizardExportErrorTitle");
                    string message = FindStringResource("ZoneWizardExportErrorMessage");
                    message = message.Replace("@1", fileName);
                    string message1 = FindStringResource("ZoneWizardExportErrorMessage1");
                    message1 = message1.Replace("@1", error);
                    MessageBox.Show(message + "\n" + message1, title, MessageBoxButton.OK, MessageBoxImage.Error);
                }
			}
			else
			{
				Logging.WriteLogLine("DEBUG Cancelled");
			}
		}
    }
}