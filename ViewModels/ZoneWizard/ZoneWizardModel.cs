﻿#nullable enable

using System.Diagnostics;
using Protocol.Data._Customers.Pml;
using File = System.IO.File;

//using PostcodesToZones = Protocol.Data.PostcodesToZones;

namespace ViewModels.ZoneWizard;

internal class ZoneWizardModel : ViewModelBase
{
	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		Task.Run( () =>
				  {
					  Task.WaitAll(
								   Task.Run( () =>
										     {
											     Dispatcher.Invoke( GetRoutes );
										     } ),
								   Task.Run( () =>
										     {
											     Dispatcher.Invoke( () =>
															        {
																        GetZones();
															        } );
										     } ),
								   Task.Run( () =>
										     {
											     Dispatcher.Invoke( () =>
															        {
																        GetDrivers();
															        } );
										     } )
								  );
				  } );

		Loaded = true;
		View?.ClearWaitCursor();
	}

	public ZoneWizard? View;

	public void SetView( ZoneWizard zw )
	{
		View = zw;
	}

#region Data and Private Helpers
	public class DisplayRoute
	{
		public string Name       { get; set; } = string.Empty;
		public string Zone       { get; set; } = string.Empty;
		public string Zone2      { get; set; } = string.Empty;
		public string PostalCode { get; set; } = string.Empty;
	}

	public ObservableCollection<DisplayRoute> DisplayRoutes
	{
		get { return Get( () => DisplayRoutes, new ObservableCollection<DisplayRoute>() ); }
		set { Set( () => DisplayRoutes, value ); }
	}

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1394540545/Auto+Dispatch+Import+File" ); }
	}

	public string SelectedImportFile
	{
		get { return Get( () => SelectedImportFile, "" ); }
		set { Set( () => SelectedImportFile, value ); }
	}

	public bool IsImportButtonEnabled
	{
		get { return Get( () => IsImportButtonEnabled, false ); }
		set { Set( () => IsImportButtonEnabled, value ); }
	}

	//public List<PmlRoute> Routes
	//{
	//	get { return Get( () => Routes, new List<PmlRoute>() ); }
	//	set { Set( () => Routes, value ); }
	//}
	//public List<PostcodesToZonesBase> Routes
	//{
	//	get { return Get(() => Routes, new List<PostcodesToZonesBase>()); }
	//	set { Set(() => Routes, value); }
	//}

	public PmlRoutes? RawRoutesPml
	{
		//get { return Get(() => RawRoutes, GetRoutes); }
		get { return Get( () => RawRoutesPml, new PmlRoutes() ); }
		set { Set( () => RawRoutesPml, value ); }
	}

	public PostcodesToZonesList? RawRoutesGeneric
	{
		//get { return Get(() => RawRoutes, GetRoutes); }
		get { return Get( () => RawRoutesGeneric, new PostcodesToZonesList() ); }
		set { Set( () => RawRoutesGeneric, value ); }
	}


	//public PmlRoute SelectedRoute
	//{
	//	get { return Get( () => SelectedRoute, new PmlRoute() ); }
	//	set { Set( () => SelectedRoute, value ); }

	//	//set
	//	//{
	//	//    if (!IsEdit)
	//	//    {
	//	//        SelectedRoute = value;
	//	//    }
	//	//}
	//}
	//public PostcodesToZonesBase SelectedRoute
	//{
	//	get { return Get(() => SelectedRoute, new PostcodesToZones()); }
	//	set { Set(() => SelectedRoute, value); }

	//	//set
	//	//{
	//	//    if (!IsEdit)
	//	//    {
	//	//        SelectedRoute = value;
	//	//    }
	//	//}
	//}
	public DisplayRoute? SelectedRoute
	{
		get { return Get( () => SelectedRoute, new DisplayRoute() ); }
		set { Set( () => SelectedRoute, value ); }

		//set
		//{
		//    if (!IsEdit)
		//    {
		//        SelectedRoute = value;
		//    }
		//}
	}

	//public PmlRoute SelectedRoute { get; set; } = null;

	[DependsUpon( nameof( SelectedRoute ) )]
	public void WhenSelectedRouteChanges()
	{
		Logging.WriteLogLine( "SelectedRoute has changed: " + SelectedRoute?.Name );

		if( ( SelectedRoute != null ) && !IsEdit )
		{
			RouteName = SelectedRoute.Name;
			ZoneName  = SelectedRoute.Zone;
			//Region     = SelectedRoute.Region;
			Region     = SelectedRoute.Zone2;
			PostalCode = SelectedRoute.PostalCode;
		}
	}


	public bool AreTextboxesReadOnly
	{
		get { return Get( () => AreTextboxesReadOnly, true ); }
		set { Set( () => AreTextboxesReadOnly, value ); }
	}


	public bool IsEdit
	{
		get { return Get( () => IsEdit, false ); }
		set { Set( () => IsEdit, value ); }
	}


	public bool IsNew
	{
		get { return Get( () => IsNew, false ); }
		set { Set( () => IsNew, value ); }
	}

	[DependsUpon( nameof( IsNew ) )]
	[DependsUpon( nameof( IsEdit ) )]
	public void WhenEditing()
	{
		Logging.WriteLogLine( "DEBUG IsNew: " + IsNew + ", IsEdit: " + IsEdit + ", AreTextboxesReadOnly: " + AreTextboxesReadOnly );

		if( IsNew || IsEdit )
			AreTextboxesReadOnly = false;
		else
			AreTextboxesReadOnly = true;
	}


	public string RouteName
	{
		get { return Get( () => RouteName, "" ); }
		set { Set( () => RouteName, value ); }
	}


	public string ZoneName
	{
		get { return Get( () => ZoneName, "" ); }
		set { Set( () => ZoneName, value ); }
	}


	public string Region
	{
		get { return Get( () => Region, "" ); }
		set { Set( () => Region, value ); }
	}


	public string PostalCode
	{
		get { return Get( () => PostalCode, "" ); }
		set { Set( () => PostalCode, value ); }
	}


	[DependsUpon( nameof( RawRoutesPml ) )]
	public void WhenRawRoutesPmlChanges()
	{
		if( RawRoutesPml is not null )
		{
			Logging.WriteLogLine( "RawRoutesPml has changed" );
			var NewRoutes = new List<PmlRoute>();
			//var newRoutes = new List<PostcodesToZonesBase>();

			foreach( var Pr in RawRoutesPml ) //Logging.WriteLogLine( "IdsRoute: " + rr.IdsRoute + " Operation: " + rr.Operation + " Region: " + rr.Region + " PostalCode: " + rr.PostalCode );
				// Doing this to maintain the fiction that there is no route
				//if( rr.IdsRoute == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
				//	rr.IdsRoute = string.Empty;
				//if (rr.RouteName == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE || rr.RouteName == "Unknown Route")
				//if (rr.RouteName == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE)
				//	rr.RouteName = string.Empty;
				NewRoutes.Add( Pr );
			//RawRoutesGeneric = newRoutes;
			RawRoutesPml.Clear();

			foreach( var Pr in NewRoutes )
				RawRoutesPml.Add( Pr );

			if( NewRoutes.Count > 0 )
			{
				//if( ( SelectedRoute != null ) && SelectedRoute.IdsRoute.IsNullOrWhiteSpace() )
				if( ( SelectedRoute != null ) && SelectedRoute.Name.IsNullOrWhiteSpace() )
				{
					//SelectedRoute = Routes[ 0 ];
					SelectedRoute = ConvertRoutePml( RawRoutesPml[ 0 ] );
				}
			}
		}
	}

	[DependsUpon( nameof( RawRoutesGeneric ) )]
	public void WhenRawRoutesGenericChanges()
	{
		if( RawRoutesGeneric != null )
		{
			Logging.WriteLogLine( "RawRoutesGeneric has changed" );
			//var newRoutes = new List<PmlRoute>();
			var NewRoutes = new List<PostcodesToZonesBase>();

			foreach( var Rr in RawRoutesGeneric ) //Logging.WriteLogLine( "IdsRoute: " + rr.IdsRoute + " Operation: " + rr.Operation + " Region: " + rr.Region + " PostalCode: " + rr.PostalCode );
			{
				// Doing this to maintain the fiction that there is no route
				//if( rr.IdsRoute == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
				//	rr.IdsRoute = string.Empty;
				//if (rr.RouteName == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE || rr.RouteName == "Unknown Route")
				if( Rr.RouteName == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
					Rr.RouteName = string.Empty;
				NewRoutes.Add( Rr );
			}

			//RawRoutesGeneric = newRoutes;
			RawRoutesGeneric.Clear();

			foreach( var Rr in NewRoutes )
				RawRoutesGeneric.Add( Rr );

			if( NewRoutes.Count > 0 )
			{
				//if( ( SelectedRoute != null ) && SelectedRoute.IdsRoute.IsNullOrWhiteSpace() )
				if( ( SelectedRoute != null ) && SelectedRoute.Name.IsNullOrWhiteSpace() )
				{
					//SelectedRoute = Routes[ 0 ];
					SelectedRoute = ConvertRouteGeneric( RawRoutesGeneric[ 0 ] );
				}
			}
		}
	}

	public List<Zone> Zones
	{
		get { return Get( () => Zones, GetZones ); }
		set { Set( () => Zones, value ); }
	}

	[DependsUpon( nameof( Zones ) )]
	public void WhenZonesChanges()
	{
		ZoneNames.Clear();

		foreach( var Zone in Zones )
			ZoneNames.Add( Zone.Name );
	}

	public List<string> ZoneNames
	{
		get { return Get( () => ZoneNames, new List<string>() ); }
		set { Set( () => ZoneNames, value ); }
	}

	public string AdhocFilter
	{
		get { return Get( () => AdhocFilter, "" ); }
		set { Set( () => AdhocFilter, value ); }
	}

	[DependsUpon( nameof( AdhocFilter ) )]
	public void WhenAdhocFilterChanges()
	{
		Logging.WriteLogLine( "DEBUG AdhocFilter: " + AdhocFilter );

		// Reload
		//var newRoutes = new List<PmlRoute>();

		if( IsPml && ( RawRoutesPml != null ) )
		{
			var NewRoutes = new List<PmlRoute>();

			foreach( var Pr in RawRoutesPml )
			{
				if( DoesRouteMatchFilterPml( Pr, AdhocFilter ) )
				{
					Logging.WriteLogLine( "Route matches filter: IdsRoute: " + Pr.IdsRoute + " Operation: " + Pr.Operation + " Region: " + Pr.Region + " PostalCode: " + Pr.PostalCode );
					NewRoutes.Add( Pr );
					//foreach (var route in newRoutes)
					//{
					//	DisplayRoutes.Add(ConvertRoutePml(route));
					//}
				}
			}
			var Matches = FindStringResource( "ZoneWizardMatches" );
			Matches       = Matches.Replace( "@1", NewRoutes.Count + "" );
			AdhocMatches = Matches;

			//Routes        = newRoutes;
			Dispatcher.Invoke( () =>
							   {
								   DisplayRoutes.Clear();

								   foreach( var Route in NewRoutes )
									   DisplayRoutes.Add( ConvertRoutePml( Route ) );
							   } );
		}
		else if( IsGeneric && ( RawRoutesGeneric != null ) )
		{
			var NewRoutes = new List<PostcodesToZonesBase>();

			foreach( var Rr in RawRoutesGeneric )
			{
				if( DoesRouteMatchFilterGeneric( Rr, AdhocFilter ) )
				{
					//Logging.WriteLogLine( "Route matches filter: IdsRoute: " + rr.IdsRoute + " Operation: " + rr.Operation + " Region: " + rr.Region + " PostalCode: " + rr.PostalCode );
					//newRoutes.Add( rr );
					//Logging.WriteLogLine("Route matches filter: RouteName: " + rr.RouteName + " Zone: " + rr.Zone1 + " PostalCode: " + rr.PostalCode);
					NewRoutes.Add( Rr );
				}
			}
			var Matches = FindStringResource( "ZoneWizardMatches" );
			Matches       = Matches.Replace( "@1", NewRoutes.Count + "" );
			AdhocMatches = Matches;

			//Routes        = newRoutes;
			Dispatcher.Invoke( () =>
							   {
								   DisplayRoutes.Clear();

								   foreach( var Route in NewRoutes )
									   DisplayRoutes.Add( ConvertRouteGeneric( Route ) );
							   } );
		}
	}


	public string AdhocMatches
	{
		get { return Get( () => AdhocMatches, "" ); }
		set { Set( () => AdhocMatches, value ); }
	}

	private bool DoesRouteMatchFilterPml( PmlRoute pr, string filter )
	{
		var Yes = false;
		filter = filter.TrimToLower();

		if( pr.IdsRoute.TrimToLower().Contains( filter ) || pr.Operation.TrimToLower().Contains( filter )
													     || pr.Region.TrimToLower().Contains( filter ) || pr.PostalCode.TrimToLower().Contains( filter ) )
			Yes = true;

		return Yes;
	}

	//private bool DoesRouteMatchFilter( PmlRoute pr, string filter )
	private bool DoesRouteMatchFilterGeneric( PostcodesToZonesBase pr, string filter )
	{
		var Yes = false;
		filter = filter.TrimToLower();

		//if( pr.IdsRoute.TrimToLower().Contains( filter ) || pr.Operation.TrimToLower().Contains( filter )
		//                                                 || pr.Region.TrimToLower().Contains( filter ) || pr.PostalCode.TrimToLower().Contains( filter ) )
		if( pr.RouteName.TrimToLower().Contains( filter ) || pr.Zone1.TrimToLower().Contains( filter ) || pr.PostalCode.TrimToLower().Contains( filter ) )
			Yes = true;

		return Yes;
	}

	public bool IsPml
	{
		get { return Get( () => IsPml, true ); }
		set { Set( () => IsPml, value ); }
	}


	public bool IsGeneric
	{
		get { return Get( () => IsGeneric, false ); }
		set { Set( () => IsGeneric, value ); }
	}

	[DependsUpon( nameof( IsPml ) )]
	public void WhenIsPmlChanges()
	{
		IsGeneric = !IsPml;
	}

	//public string PmlRegionHeaderLabel
	//{
	//	get
	//	{
	//		IsPml = GetPmlPreference();
	//		string label = IsPml ? FindStringResource("ZoneWizardLabelRegion") : string.Empty;
	//		return label;
	//	}
	//}


	public string PmlRegionVisibility
	{
		get
		{
			IsPml = GetPmlPreference();
			var Visibility = IsPml ? "Visible" : "Hidden";
			return Visibility;
		}
	}

	public int PmlRegionRowHeight
	{
		get
		{
			IsPml = GetPmlPreference();
			var Height = IsPml ? 28 : 0;
			return Height;
		}
	}

	public void GetRoutes()
	{
		DisplayRoutes.Clear();
		//Task.Run(async () =>
		//	{
		//		await GetPreferences();
		//	})

		IsPml = GetPmlPreference();

		if( !IsPml )
		{
			var Generic = GetRoutesGeneric();
			ConvertRoutesGeneric( Generic );
		}
		else
		{
			var Pml = GetRoutesPml();
			ConvertRoutesPml( Pml );
		}
	}

	private void ConvertRoutesGeneric( PostcodesToZonesList? list )
	{
		if( list != null )
		{
			foreach( var Route in list )
				DisplayRoutes.Add( ConvertRouteGeneric( Route ) );
		}
	}

	public DisplayRoute ConvertRouteGeneric( PostcodesToZonesBase route )
	{
		DisplayRoute Dr = new()
						  {
							  Name       = route.RouteName,
							  Zone       = route.Zone1,
							  Zone2      = string.Empty,
							  PostalCode = route.PostalCode
						  };
		return Dr;
	}


	private void ConvertRoutesPml( PmlRoutes? list )
	{
		if( list != null )
		{
			foreach( var Route in list )
				DisplayRoutes.Add( ConvertRoutePml( Route ) );
		}
	}

	public DisplayRoute ConvertRoutePml( PmlRoute route )
	{
		DisplayRoute Dr = new()
						  {
							  Name       = route.IdsRoute,
							  Zone       = route.Operation,
							  Zone2      = route.Region,
							  PostalCode = route.PostalCode
						  };
		return Dr;
	}

	public PostcodesToZonesList? GetRoutesGeneric()
		//public PostcodesToZonesList GetRoutes()
	{
		Logging.WriteLogLine( "Loading Generic Routes" );
		//PmlRoutes pr  = null;
		PostcodesToZonesList? PtozList = null;
		//PostcodesToZonesList PRoutes = null;
		//var       ccl = new CustomerCodeList();

		var Ccl = new CustomerCodeList
				  {
					  Globals.DataContext.MainDataContext.Account
				  };

		if( !IsInDesignMode )
		{
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   PtozList = await Azure.Client.RequestGetPostalZones( "*", "*", "*" );

									   //pr = await Azure.Client.RequestPML_GetRoutes();
									   //Logging.WriteLogLine( "Found routes: " + pr?.Count );
									   Logging.WriteLogLine( "Found routes: " + PtozList?.Count );

									   //PRoutes = await Azure.Client.RequestGetPostalZones("", "", "");
									   //Logging.WriteLogLine( "Found routes: " + PRoutes?.Count );

									   //if( pr != null )
									   //{
									   // foreach( var route in pr )
									   // {
									   //  Logging.WriteLogLine( "IdsRoute: " + route.IdsRoute + " Operation: " + route.Operation + " Region: " + route.Region + " PostalCode: " + route.PostalCode );
									   // }
									   //}
/*
									   var Crb = await Azure.Client.RequestGetCustomersRoutesBasic( Ccl );

									   Logging.WriteLogLine( "Found routes: " + Crb?.Count );

									   if( Crb != null )
									   {
										   foreach( var Route in Crb )
										   {
											   Logging.WriteLogLine( "Route for " + Route.CustomerCode + " has " + Route.Routes.Count + " routes" );

											   var Rl = new RouteLookup
													    {
														    CustomerCode  = Route.CustomerCode,
														    Key           = string.Empty,
														    Fetch         = 0,
														    PreFetchCount = 100
													    };
											   var Rlsl = await Azure.Client.RequestRouteLookup( Rl );

											   if( Rlsl != null )
											   {
												   foreach( var Rls in Rlsl )
													   Logging.WriteLogLine( "rls.RouteName: " + Rls.RouteName );
											   }

											   Ccl = new CustomerCodeList
												     {
													     Route.CustomerCode
												     };
											   var Crsl = await Azure.Client.RequestGetRoutesForCustomers( Ccl );

											   if( Crsl != null )
											   {
												   foreach( var Crs in Crsl )
												   {
													   Logging.WriteLogLine( "From RequestGetRoutesForCustomers: " + Crs.CustomerCode );

													   if( Crs.ScheduleAndRoutes.Count > 0 )
													   {
														   foreach( var Sar in Crs.ScheduleAndRoutes )
														   {
															   Logging.WriteLogLine( "sar: " + Crs.CustomerCode + " RouteName: " + Sar.RouteName );

															   if( Sar.RouteSummary.Count > 0 )
															   {
																   foreach( var Rs in Sar.RouteSummary )
																	   Logging.WriteLogLine( "rs: " + Crs.CustomerCode + " CompanyName" + Rs.CompanyName );
															   }
														   }
													   }
												   }
											   }
										   }
									   }
*/
									   //var crsl = await Azure.Client.RequestGetRoutesForCustomers(ccl);
									   //if (crsl != null)
									   //{
									   //    foreach (var crs in crsl)
									   //    {
									   //        Logging.WriteLogLine("From RequestGetRoutesForCustomers: " + crs.CustomerCode);
									   //        if (crs.ScheduleAndRoutes != null && crs.ScheduleAndRoutes.Count > 0)
									   //        {
									   //            foreach (var sar in crs.ScheduleAndRoutes)
									   //            {
									   //                Logging.WriteLogLine("RouteName: " + sar.RouteName);
									   //                if (sar.RouteSummary != null && sar.RouteSummary.Count > 0)
									   //                {
									   //                    foreach (var rs in sar.RouteSummary)
									   //                    {
									   //                        Logging.WriteLogLine("CompanyName" + rs.CompanyName);
									   //                    }
									   //                }
									   //            }
									   //        }
									   //    }
									   //}

									   //RawRoutes = pr;
									   RawRoutesGeneric = PtozList;
									   //RawRoutes = PRoutes;
								   } )
						);
		}

		//return pr;
		//return PRoutes;
		return PtozList;
	}

	public PmlRoutes? GetRoutesPml()
		//public PostcodesToZonesList GetRoutes()
	{
		Logging.WriteLogLine( "Loading PML Routes" );
		PmlRoutes? Pr = null;

		if( !IsInDesignMode )
		{
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   //ptozList = await Azure.Client.RequestGetPostalZones("*", "*", "*");
									   Loaded = false;
									   Pr     = await Azure.Client.RequestPML_GetRoutes();
									   Logging.WriteLogLine( "Found routes: " + Pr?.Count );

									   RawRoutesPml = Pr;
									   Loaded       = true;
								   } )
						);
		}

		//return PRoutes;
		//return ptozList;
		return Pr;
	}

	public List<Zone> GetZones()
	{
		Logging.WriteLogLine( "Loading Zones" );
		//Task.WaitAll(
		//	Task.Run(() => GetPreferences())
		//);

		var Zones = new List<Zone>();

		if( !IsInDesignMode )
		{
			Task.Run( async () =>
					  {
						  this.Zones = await Azure.Client.RequestZones();

						  Logging.WriteLogLine( "Found " + this.Zones.Count + " Zones" );

						  if( this.Zones.IsNotNull() && ( this.Zones.Count > 0 ) )
						  {
							  Zones = this.Zones.OrderBy( x => x.SortIndex ).ToList();
							  this.Zones.Clear();

							  // Blank zones sometimes appear
							  foreach( var Zone in Zones )
							  {
								  if( Zone.Name.IsNotNullOrWhiteSpace() )
									  this.Zones.Add( Zone );
								  else
									  Logging.WriteLogLine( "Found zone with empty name - skipping - Id: " + Zone.Id );
							  }
							  //Zones = zones;

							  //foreach( var zone in Zones )
							  //{
							  // Logging.WriteLogLine( "Zone: Name " + zone.Name + " Abbrev " + zone.Abbreviation );
							  //}
						  }
					  } );
		}
		return Zones;
	}

	public bool IsPmlPreferenceLoaded
	{
		get { return Get( () => IsPmlPreferenceLoaded, false ); }
		set { Set( () => IsPmlPreferenceLoaded, value ); }
	}

	//public async Task GetPreferences()
	public bool GetPmlPreference()
	{
		var IsPml = false;

		if( !IsInDesignMode )
		{
			Logging.WriteLogLine( "Getting Preferences" );

			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   var Prefs = await Azure.Client.RequestPreferences();
									   Logging.WriteLogLine( "Found " + Prefs?.Count + " preferences;" );
									   if (Prefs != null)
									   {
										   foreach( var Pref in Prefs )
										   {
											   if( ( ( Pref.Description.ToLower().Trim() == "pml" ) || ( Pref.Description.ToLower().Trim() == "pmltest" ) ) && Pref.Enabled )
											   {
												   Logging.WriteLogLine( "IsPml is true;" );
												   IsPml = true;
												   break;
											   }
										   }
									   }
								   } )
						);
			IsPmlPreferenceLoaded = true;
		}
		return IsPml;
	}

	public ObservableCollection<string> Drivers
	{
		get { return Get( () => Drivers, new ObservableCollection<string>() ); }
		set { Set( () => Drivers, value ); }
	}

	private ObservableCollection<string> GetDrivers()
	{
		SortedDictionary<string, string> Drivers = new( StringComparer.OrdinalIgnoreCase );
		this.Drivers.Clear();

		if( !IsInDesignMode )
		{
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   var Tmp = await Azure.Client.RequestGetDrivers();

									   if( Tmp != null )
									   {
										   foreach( var Driver in Tmp )
										   {
											   if( !Drivers.Keys.Contains( Driver.StaffId ) )
												   Drivers.Add( Driver.StaffId, Driver.StaffId );
										   }
									   }
									   // Just in case.  i found in the drivers board that not all drivers were found by RequestGetDrivers
									   // Also need to check that the NONE_DISPATCHING_ROUTE driver is set up

									   var NoneDispatchingRouteExists = false;
									   var Sars                       = await Azure.Client.RequestGetStaffAndRoles();

									   if( Sars != null )
									   {
										   foreach( var Sar in Sars )
										   {
											   if( Sar.StaffId == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
												   NoneDispatchingRouteExists = true;
											   else if( ( Sar.Roles != null ) && ( Sar.Roles.Count > 0 ) )
											   {
												   foreach( var Role in Sar.Roles )
												   {
													   if( Role.IsDriver && !Drivers.ContainsKey( Sar.StaffId ) )
													   {
														   Logging.WriteLogLine( "Adding driver from RequestGetStaffAndRoles: " + Sar.StaffId );
														   Drivers.Add( Sar.StaffId, Sar.StaffId );
														   break;
													   }
												   }
											   }
										   }
									   }

									   if( !NoneDispatchingRouteExists )
									   {
										   // Add user
										   AddNoneDispatchingRoute();
									   }
								   } )
						);
		}
		ObservableCollection<string> Result = new() {string.Empty};

		foreach( var Driver in Drivers.Keys )
			Result.Add( Driver );
		this.Drivers = Result;
		Logging.WriteLogLine( "Loaded " + Drivers.Count + " drivers as routes" );
		return Result;
	}

	private static void AddNoneDispatchingRoute()
	{
		Protocol.Data.Staff Sd = new()
								 {
									 StaffId  = Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE,
									 LastName = Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE,
									 Password = Encryption.ToTimeLimitedToken( Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE ),
									 Enabled  = false
								 };

		UpdateStaffMemberWithRolesAndZones UpdateData = new( "ZoneWizard", Sd )
														{
															Roles = new List<Role>()
														};

		Task.WaitAll(
					 Task.Run( async () =>
							   {
								   await Azure.Client.RequestUpdateStaffMember( UpdateData );
							   } )
					);
	}
#endregion

#region Actions
	public void Execute_ShowHelp()
	{
		Logging.WriteLogLine( "Opening " + HelpUri );
		var Uri = new Uri( HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	private const int CSV_ROUTE = 0;
	private const int CSV_ZONE  = 1;
	private const int CSV_PCODE = 2;

	public (bool success, string error) Execute_ImportZonesFile()
	{
		var Success = true;

		// Read the file into a string
		var Csv                                        = File.ReadAllLines( SelectedImportFile );
		var Error                                      = ValidateFileHeader( Csv );
		var LineSkippedMessageTemplate                 = FindStringResource( "ZoneWizardImportErrorLinesSkippedMessage" );
		var LineDuplicateSkippedMessageTemplate        = FindStringResource( "ZoneWizardImportErrorLinesDuplicateSkippedMessageDetail" );
		var LineDuplicateDuplicatePcodeMessageTemplate = FindStringResource( "ZoneWizardImportErrorLinesDuplicatePcodeMessageDetail" );

		if( !IsInDesignMode && ( Error.Length == 0 ) )
		{
			var CsvObj = global::Utils.Csv.Csv.ReadFromFile( SelectedImportFile );

			if( ( CsvObj != null ) && ( CsvObj.RowCount > 1 ) )
			{
				Task.WaitAll(
							 Task.Run( async () =>
									   {
										   Logging.WriteLogLine( "Importing Zones file: " + SelectedImportFile );

										   if( IsPml )
										   {
											   var CsvPml = File.ReadAllText( SelectedImportFile );
											   await Azure.Client.RequestPML_PostCodeToRouteImport( CsvPml );
										   }
										   else
										   {
											   // Skip over the header
											   for( var I = 1; I < CsvObj.RowCount; I++ )
											   {
												   var Line      = CsvObj[ I ];
												   var RouteName = Line[ CSV_ROUTE ].AsString.Trim();
												   var ZoneName  = Line[ CSV_ZONE ].AsString.Trim();
												   var Pcode     = Line[ CSV_PCODE ].AsString.Trim();

												   // Mandatory fields
												   if( ZoneName.IsNotNullOrWhiteSpace() && Pcode.IsNotNullOrWhiteSpace() )
												   {

													   if (!DoesRouteExist(RouteName, ZoneName, Pcode))
													   {
														   if (!IsPcodeUnique(Pcode))
														   {
															   // Exists
															   string tmp = LineDuplicateDuplicatePcodeMessageTemplate.Replace("@1", Pcode);
															   Error += tmp.Replace("@2", Csv[I]) + "\n";
														   }
														   else
														   {
															   Logging.WriteLogLine( "Adding zone to pcode - Line " + I + " - routeName: " + RouteName + ", zoneName: " + ZoneName + ", pcode: " + Pcode );
															   PostcodesToZones P = new()
																					{
																						RouteName  = RouteName,
																						Zone1      = ZoneName,
																						PostalCode = Pcode
																					};

															   await Azure.Client.RequestAddUpdatePostalZones( P );
														   }
													   }
													   else
													   {
														   // Duplicate
														   Error += LineDuplicateSkippedMessageTemplate.Replace("@1", Csv[I]) + "\n";
													   }
												   }
												   else
													   Error += LineSkippedMessageTemplate.Replace( "@1", Csv[ I ] ) + "\n";
											   }
										   }
									   } )
							);
			}
		}

		return ( Success, Error );
	}

	private bool IsPcodeUnique(string pcode)
	{
        bool exists = (from D in DisplayRoutes
                    where D.PostalCode.ToLower() == pcode.ToLower()
                    select D).Any();

        return !exists;
    }

	private bool DoesRouteExist(string route, string zone, string pcode)
	{
		bool exists = (from D in DisplayRoutes 
						   //where D.Name.ToLower() == route.ToLower() && D.Zone.ToLower() == zone.ToLower() && D.PostalCode.ToLower() == pcode.ToLower()
						   where D.Zone.ToLower() == zone.ToLower() && D.PostalCode.ToLower() == pcode.ToLower()
						   select D ).Any();

		return exists;
	}
	
	public (bool, string) ExportZonesFile(string fileName)
    {
        Logging.WriteLogLine("Exporting Zone to Postal Code mappings to csv file " + fileName);
		bool success = true;
		string error = string.Empty;
		StringBuilder lines = new();
		if (IsPml)
		{
			if (RawRoutesPml != null)
			{
				string line = "Postcode,Operation,Region,IDS Route";
				lines.AppendLine( line );
				foreach (var rawRoute in RawRoutesPml)
				{
					line = SanitizeForCsv(rawRoute.PostalCode) + "," + SanitizeForCsv(rawRoute.Operation) + "," + SanitizeForCsv(rawRoute.Region) + "," + SanitizeForCsv(rawRoute.IdsRoute);
					lines.AppendLine( line );
				}
			}
		}
		else
		{
			if (RawRoutesGeneric != null)
			{
				string line = SanitizeForCsv("RouteName") + "," + SanitizeForCsv("ZoneName") + "," + SanitizeForCsv("PostalCode/Zip");
				lines.AppendLine( line );
				foreach (var rawRoute in RawRoutesGeneric )
				{
					line = SanitizeForCsv(rawRoute.RouteName) + "," + SanitizeForCsv(rawRoute.Zone1) + "," + SanitizeForCsv(rawRoute.PostalCode);
					lines.AppendLine( line );
				}
			}
		}
		try
		{
			File.WriteAllText(fileName, lines.ToString());
		}
		catch (Exception e)
		{
			Logging.WriteLogLine("Failed to export routes to file: " + fileName + " Error:\n" + e.ToString() + "\n");
			error = e.Message;
			success = false;
		}

		return (success, error);
    }

    private static string SanitizeForCsv(string item, bool wrapInQuotes = false)
    {
        //if (item != null && item.Length > 0 && item.Contains('"'))
        if (!string.IsNullOrEmpty(item) && item.Contains("\""))
            item = item.Replace('"', '\'');

		if (wrapInQuotes)
		{
			item = "\"" + item + "\"";
		}
        return item;
    }

    private string ValidateFileHeader( string[] csv )
	{
		var Errors = string.Empty;

		if( !IsPml )
		{
			// "ZoneWizardImportErrorWrongFormatMessage"
			if( csv.Length > 0 )
			{
				if( csv[ 0 ].Contains( "RouteName" ) && csv[ 0 ].Contains( "ZoneName" ) && csv[ 0 ].Contains( "PostalCode/Zip" ) )
				{
				}
				else
				{
					Errors += FindStringResource( "ZoneWizardImportErrorWrongFormatMessage" ) + "\n" + FindStringResource( "ZoneWizardImportErrorWrongFormatMessage1" ) + "\n"
							  + FindStringResource( "ZoneWizardImportErrorWrongFormatMessage2" ) + "\n" + FindStringResource( "ZoneWizardImportErrorWrongFormatMessage3" );
				}
			}
		}

		return Errors;
	}


	public (bool success, string error) SaveRoute()
	{
		bool   Success;
		string Error;

		if( IsPml )
			( Success, Error ) = SaveRoutePml();
		else
			( Success, Error ) = SaveRouteGeneric();

		return ( Success, Error );
	}

	public (bool success, string error) SaveRoutePml()
	{
		var Success = true;
		var Error   = string.Empty;

		PmlRoute? PmlRoute = null;
		PmlRoute? Found;
		//PostcodesToZones RouteName = null;
		//PostcodesToZonesBase found;

		if( IsNew )
		{
			// Conditions
			// IdsRoute must be unique - No - See I2P-53
			// PostalCode must be unique
			//found = (from R in Routes where (R.IdsRoute == IdsRoute) select R).FirstOrDefault();
			//if (found != null)
			//{
			//    error += "\r\n" + FindStringResource("ZoneWizardErrorRouteMustBeUnique");
			//    success = false;
			//}
			Found = ( from R in RawRoutesPml
					  where R.PostalCode.TrimToLower() == PostalCode.TrimToLower()
					  select R ).FirstOrDefault();

			if( Found != null )
			{
				Error   += "\r\n" + FindStringResource( "ZoneWizardErrorPostalCodeMustBeUnique" );
				Success =  false;
			}

			if( Success )
			{
				//var route = Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE;

				//if (!this.RouteName.IsNullOrWhiteSpace())
				//	route = this.RouteName;

				PmlRoute = new PmlRoute
						   {
							   IdsRoute = RouteName.Trim(),
							   //IdsRoute = route,
							   Operation  = ZoneName.Trim(),
							   Region     = Region.Trim(), // SubZone
							   PostalCode = PostalCode.Trim()
						   };
				//RouteName = new PostcodesToZones
				//{
				//	//IdsRoute   = IdsRoute.Trim(),
				//	RouteName = route,
				//	Zone1 = ZoneName.Trim(),
				//	PostalCode = PostalCode.Trim()
				//};
			}
		}
		else if( SelectedRoute is not null )
		{
			// Editing
			// Conditions
			// IdsRoute must be unique - No - See I2P-53
			// PostalCode must be unique

			// Case - renaming route:
			if( SelectedRoute.Name != RouteName )
			{
				Found = ( from R in RawRoutesPml
						  where R.IdsRoute == RouteName
						  select R ).FirstOrDefault();

				if( Found != null )
				{
					Error   += "\r\n" + FindStringResource( "ZoneWizardErrorRouteMustBeUnique" );
					Success =  false;
				}
			}

			// Case - changing PostalCode
			if( SelectedRoute.PostalCode != PostalCode )
			{
				Found = ( from R in RawRoutesPml
						  where R.PostalCode.TrimToLower() == PostalCode.TrimToLower()
						  select R ).FirstOrDefault();

				if( Found != null )
				{
					Error   += "\r\n" + FindStringResource( "ZoneWizardErrorPostalCodeMustBeUnique" );
					Success =  false;
				}
			}

			if( Success )
			{
				PmlRoute = new PmlRoute
						   {
							   IdsRoute   = RouteName.Trim(),
							   Operation  = ZoneName.Trim(),
							   Region     = Region.Trim(),
							   PostalCode = PostalCode.Trim()
						   };
				//RouteName = new PostcodesToZones
				//{
				//	RouteName = this.RouteName.Trim(),
				//	Zone1 = ZoneName.Trim(),
				//	PostalCode = PostalCode.Trim()
				//};
			}
		}

		if( Success )
		{
			if( PmlRoute is not null )
			{
				Logging.WriteLogLine( "Saving : RouteName: " + RouteName + " ZoneName: " + ZoneName + " PostalCode: " + PostalCode );

				var Prs = new PmlRoutes
						  {
							  PmlRoute
						  };

				Task.WaitAll(
							 Task.Run( async () =>
									   {
										   await Azure.Client.RequestPML_AddUpdateRoutes( Prs );

										   //GetRoutes();
									   } )
							);
			}

			//var prs = new PostcodesToZonesList
			//		  {
			//			  pmlRoute
			//		  };

			//Task.WaitAll(
			//			 Task.Run(async () =>
			//					   {
			//						   //PostcodesToZones tmp = new();
			//						   //tmp.RouteName = pmlRoute.RouteName.Trim();
			//						   //tmp.Zone1 = pmlRoute.Zone1.Trim();
			//						   //tmp.PostalCode = pmlRoute.PostalCode.Trim();
			//						   await Azure.Client.RequestAddUpdatePostalZones(RouteName);
			//					   })
			//			);

			Dispatcher.Invoke( GetRoutes );
		}
		else
		{
			var Title   = FindStringResource( "ZoneWizardErrorTitle" );
			var Message = FindStringResource( "ZoneWizardErrorMessage" );
			Message += Error;

			MessageBox.Show( Title, Message, MessageBoxButton.OK, MessageBoxImage.Error );
		}

		return ( Success, Error );
	}

	public (bool success, string error) SaveRouteGeneric()
	{
		var Success = true;
		var Error   = string.Empty;

		PostcodesToZones?     PostcodesToZones = null;
		PostcodesToZonesBase? Found;

		if( IsNew )
		{
			// Conditions
			// IdsRoute must be unique - No - See I2P-53
			// PostalCode must be unique
			//found = (from R in Routes where (R.IdsRoute == IdsRoute) select R).FirstOrDefault();
			//if (found != null)
			//{
			//    error += "\r\n" + FindStringResource("ZoneWizardErrorRouteMustBeUnique");
			//    success = false;
			//}
			Found = ( from R in RawRoutesGeneric
					  where R.PostalCode.TrimToLower() == PostalCode.TrimToLower()
					  select R ).FirstOrDefault();

			if( Found != null )
			{
				Error   += "\r\n" + FindStringResource( "ZoneWizardErrorPostalCodeMustBeUnique" );
				Success =  false;
			}

			if( Success )
			{
				var Route = Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE;

				if( !this.RouteName.IsNullOrWhiteSpace() )
					Route = this.RouteName;

				PostcodesToZones = new PostcodesToZones
								   {
									   RouteName  = Route,
									   Zone1      = ZoneName.Trim(),
									   PostalCode = PostalCode.Trim()
								   };
			}
		}
		else if( SelectedRoute is not null )
		{
			// Editing
			// Conditions
			// IdsRoute must be unique - No - See I2P-53
			// PostalCode must be unique

			// Case - renaming route:
			//if (SelectedRoute.IdsRoute != IdsRoute)
			//{
			//    found = (from R in Routes where (R.IdsRoute == IdsRoute) select R).FirstOrDefault();
			//    if (found != null)
			//    {
			//        error += "\r\n" + FindStringResource("ZoneWizardErrorRouteMustBeUnique");
			//        success = false;
			//    }
			//}

			// Case - changing PostalCode
			if( SelectedRoute.PostalCode != PostalCode )
			{
				Found = ( from R in RawRoutesGeneric
						  where R.PostalCode.TrimToLower() == PostalCode.TrimToLower()
						  select R ).FirstOrDefault();

				if( Found != null )
				{
					Error   += "\r\n" + FindStringResource( "ZoneWizardErrorPostalCodeMustBeUnique" );
					Success =  false;
				}
			}

			if( Success )
			{
				PostcodesToZones = new PostcodesToZones
								   {
									   RouteName  = this.RouteName.Trim(),
									   Zone1      = ZoneName.Trim(),
									   PostalCode = PostalCode.Trim()
								   };
			}
		}

		if( Success )
		{
			Logging.WriteLogLine( "Saving : RouteName: " + this.RouteName + " ZoneName: " + ZoneName + " PostalCode: " + PostalCode );

			if (SelectedRoute != null )
			{
				// Editing - first, delete the route - the server doesn't remove the original
				_ = DeleteRoute();
			}
						
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   await Azure.Client.RequestAddUpdatePostalZones( PostcodesToZones );
								   } )
						);

			Dispatcher.Invoke( GetRoutes );
		}
		else
		{
			var Title   = FindStringResource( "ZoneWizardErrorTitle" );
			var Message = FindStringResource( "ZoneWizardErrorMessage" );
			Message += Error;

			MessageBox.Show( Title, Message, MessageBoxButton.OK, MessageBoxImage.Error );
		}

		return ( Success, Error );
	}

	public bool DeleteRoute()
	{
		if( SelectedRoute is not null )
		{
			Logging.WriteLogLine( "Deleting : RouteName: " + SelectedRoute.Name + " Zone: " + SelectedRoute.Zone + " Zone2: " + SelectedRoute.Zone2 + " PostalCode: " + SelectedRoute.PostalCode );

			if( IsPml )
			{
				var Prs = new PmlDeleteRoutes
						  {
							  SelectedRoute.Name
						  };

				Task.WaitAll(
							 Task.Run( async () =>
									   {
										   await Azure.Client.RequestPML_DeleteRoutes( Prs );
										   //await Azure.Client.RequestDeletePostalZones( SelectedRoute.RouteName, SelectedRoute.PostalCode );
									   } )
							);
			}
			else
			{
				Task.WaitAll(
							 Task.Run( async () =>
									   {
										   await Azure.Client.RequestDeletePostalZones( SelectedRoute.Name, SelectedRoute.PostalCode );
									   } )
							);
			}
		}

		return true;
	}

#endregion
}