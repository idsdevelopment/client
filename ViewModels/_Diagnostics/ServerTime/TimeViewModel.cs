﻿#nullable enable

namespace ViewModels._Diagnostics.ServerTime;

public static class Extensions
{
	// ReSharper disable once InconsistentNaming
	public static string AsMSM( this long value )
	{
		var Neg = value < 0 ? "-" : "";

		var T = new TimeSpan( Math.Abs( value ) );

		return $"{Neg}{T.Minutes:D2}:{T.Seconds:D2}.{T.Milliseconds:D4}";
	}
}

public class DiffItem
{
	public string? ServerUtcTime { get; set; }
	public string? LocalUtcTime  { get; set; }

	public long Diff { get; set; }

	public string DiffAsText => Diff.AsMSM();
}

public class TimeViewModel : ViewModelBase
{
	private const string START = "Start",
	                     STOP  = "Stop";

	public ObservableCollection<DiffItem> ErrorList
	{
		get { return Get( () => ErrorList, new ObservableCollection<DiffItem>() ); }
		set { Set( () => ErrorList, value ); }
	}


	public string DisplayDateTime
	{
		get { return Get( () => DisplayDateTime, "" ); }
		set { Set( () => DisplayDateTime, value ); }
	}

	public string ButtonCaption
	{
		get { return Get( () => ButtonCaption, START ); }
		set { Set( () => ButtonCaption, value ); }
	}

	public decimal Threshold
	{
		get { return Get( () => Threshold, 2 ); }
		set { Set( () => Threshold, value ); }
	}

	private bool Running;

	public void Execute_StartStop()
	{
		Running = !Running;

		if( Running )
		{
			ErrorList = new ObservableCollection<DiffItem>();

			ButtonCaption = STOP;

			var Thresh = new TimeSpan( 0, 0, (int)Threshold ).Ticks;

			Task.Run( async () =>
			          {
				          while( Running )
				          {
					          try
					          {
						          var STicks = await Azure.Client.RequestPingTime();

						          var LTime  = DateTime.UtcNow;
						          var LTicks = LTime.Ticks;

						          var Diff  = LTicks - STicks;
						          var ADiff = Math.Abs( Diff );

						          var ServerTime = new DateTime( STicks );
						          var STime      = ServerTime.ToString( "yyyy/MM/dd hh:mm:ss.fff" );

						          DisplayDateTime = STime;

						          if( ADiff > Thresh )
						          {
							          Dispatcher.Invoke( () =>
							                             {
								                             try
								                             {
									                             var E = ErrorList;

									                             E.Insert( 0, new DiffItem
									                                          {
										                                          ServerUtcTime = STime,
										                                          LocalUtcTime  = LTime.ToString( "G" ),

										                                          Diff = Diff
									                                          } );
								                             }
								                             catch( Exception Exception )
								                             {
									                             Console.WriteLine( Exception );
								                             }
							                             } );
						          }
					          }
					          catch( Exception Exception )
					          {
						          Console.WriteLine( Exception );
					          }
				          }

				          ButtonCaption = START;
			          } );
		}
	}

	// ReSharper disable once IdentifierTypo
	public async void Execute_eBol()
	{
		try
		{
			await Azure.Client.RequestPML_ResendeBOL( "AUDR0006-1", "terry@terry-watts.com" );
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
		}
	}

#region Auth Token
	public string AuthToken
	{
		get { return Get( () => AuthToken, "" ); }
		set { Set( () => AuthToken, value ); }
	}


	public string AuthTokenAsText
	{
		get { return Get( () => AuthTokenAsText, "" ); }
		set { Set( () => AuthTokenAsText, value ); }
	}

	[DependsUpon250( nameof( AuthToken ) )]
	public void WhenAuthTokenChanges()
	{
		var (Value, _, _) = Encryption.DecryptDetailed( AuthToken );
		var P = Value.IndexOf( '^' );

		if( P >= 0 )
		{
			var Ticks        = Value.Substring( 0, P );
			var ExpiresTicks = long.Parse( Ticks );
			var Expires      = new DateTime( ExpiresTicks ).ToString( "yyyy/MM/dd hh:mm:sss.fff" );

			string Content;

			try
			{
				Content = Value.Substring( P + 1 );
			}
			catch
			{
				Content = "";
			}

			AuthTokenAsText = $"{Ticks}     Expires: {Expires}\r\n{Encryption.Decrypt( Value.Substring( P + 1 ) )}\r\nContent: {Content}";
		}
		else
			AuthTokenAsText = Value;
	}
#endregion
}