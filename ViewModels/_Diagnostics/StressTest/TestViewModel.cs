﻿#nullable enable

namespace ViewModels._Diagnostics.StressTest;

public class TripDisplayEntry : Trip, INotifyPropertyChanged
{
	public new string TripId
	{
		get => base.TripId;
		set
		{
			base.TripId = value;
			OnPropertyChanged();
		}
	}

	public STATUS Status
	{
		get => Status1;
		set
		{
			Status1 = value;
			OnPropertyChanged();
			OnPropertyChanged( nameof( StatusAsString ) );
		}
	}

	public new string Driver
	{
		get => base.Driver;
		set
		{
			base.Driver = value;
			OnPropertyChanged();
		}
	}

	public string StatusAsString => Status1.AsString();


	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class TestViewModel : ViewModelBase
{
	private const string GO   = "Go",
	                     STOP = "Stop";


	public string Location
	{
		get { return Get( () => Location, "1234" ); }
		set { Set( () => Location, value ); }
	}


	public decimal TripCount
	{
		get { return Get( () => TripCount, 10 ); }
		set { Set( () => TripCount, value ); }
	}


	public decimal Pieces
	{
		get { return Get( () => Pieces, 1 ); }
		set { Set( () => Pieces, value ); }
	}

	public ObservableCollection<string> Drivers
	{
		get { return Get( () => Drivers, new ObservableCollection<string>() ); }
		set { Set( () => Drivers, value ); }
	}


	public IList<string>? SelectedDrivers
	{
		get { return Get( () => SelectedDrivers, (List<string>?)null ); }
		set { Set( () => SelectedDrivers, value ); }
	}


	public ObservableCollection<TripDisplayEntry> Trips
	{
		get { return Get( () => Trips, new ObservableCollection<TripDisplayEntry>() ); }
		set { Set( () => Trips, value ); }
	}


	public string GoButtonCaption
	{
		get { return Get( () => GoButtonCaption, GO ); }
		set { Set( () => GoButtonCaption, value ); }
	}


	public bool Running
	{
		get { return Get( () => Running, false ); }
		set { Set( () => Running, value ); }
	}

	private readonly Random DriverRand = new();

	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( !IsInDesignMode )
		{
			Task.Run( async () =>
			          {
				          var Drvs = await Azure.Client.RequestGetDrivers();

				          Dispatcher.Invoke( () =>
				                             {
					                             foreach( var Drv in Drvs )
						                             Drivers.Add( Drv.StaffId );
				                             } );
			          } );
		}
	}

	[DependsUpon( nameof( Running ) )]
	public void WhenRunningChanges()
	{
		if( Running )
		{
			GoButtonCaption = STOP;
			Trips           = new ObservableCollection<TripDisplayEntry>();

			Task.Run( async () =>
			          {
				          try
				          {
					          var P   = Pieces;
					          var Loc = Location;

					          var Dvrs = new List<string>( SelectedDrivers is null || ( SelectedDrivers.Count == 0 )
						                                       ? ( from D in Drivers
						                                           select D ).ToList()
						                                       : SelectedDrivers );
					          var MaxDriverRand = Dvrs.Count;

					          for( var I = TripCount; Running && ( I-- > 0 ); )
					          {
						          var TripId = await Azure.Client.RequestGetNextTripId();

						          var Ndx = DriverRand.Next( MaxDriverRand );
						          var Now = DateTimeOffset.Now;

						          var Trip = new TripDisplayEntry
						                     {
							                     TripId                   = TripId,
							                     PickupAddressBarcode     = $"P{Loc}",
							                     DeliveryAddressBarcode   = $"D{Loc}",
							                     Pieces                   = P,
							                     BroadcastToDispatchBoard = true,
							                     Driver                   = Dvrs[ Ndx ],
							                     PickupCompanyName        = "Pickup Company",
							                     DeliveryCompanyName      = "Delivery Company",
							                     CallTime                 = Now,
							                     DueTime                  = Now,
							                     ReadyTime                = Now,
							                     PickupZone               = "Pickup Zone",
							                     DeliveryZone             = "Delivery Zone",
							                     ServiceLevel             = "Service Level",
							                     Status1                  = STATUS.ACTIVE
						                     };

						          Dispatcher?.Invoke( () =>
						                              {
							                              Trips.Add( Trip );
						                              } );

						          await Azure.Client.RequestAddUpdateTrip( Trip );
					          }

					          Running = false;
				          }
				          catch( Exception Exception )
				          {
					          Console.WriteLine( Exception );
				          }
			          } );
		}
		else
			GoButtonCaption = GO;
	}
}