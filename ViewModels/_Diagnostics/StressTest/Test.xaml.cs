﻿using System.Windows.Controls;

namespace ViewModels._Diagnostics.StressTest;

/// <summary>
///     Interaction logic for Test.xaml
/// </summary>
public partial class Test : Page
{
	public Test()
	{
		InitializeComponent();

		if( Root.DataContext is TestViewModel Model )
		{
			DriversListView.SelectionChanged += ( _, _ ) =>
			                                    {
				                                    Model.SelectedDrivers = ( from object Item in DriversListView.SelectedItems
				                                                              let I = Item as string
				                                                              where I is not null
				                                                              select I ).ToList();
			                                    };
		}
	}
}