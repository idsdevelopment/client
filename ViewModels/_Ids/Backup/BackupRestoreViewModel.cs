﻿using System.Threading;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels._Ids.Backup;

public class BackupSelection
{
	public bool Selected
	{
		get => _Selected;
		set
		{
			_Selected = value;
			Owner.SelectionChanged();
		}
	}

	public string Name { get; set; }

	private readonly BackupRestoreViewModel Owner;

	private bool _Selected;

	public BackupSelection( BackupRestoreViewModel owner, string name )
	{
		Owner = owner;
		Name  = name;
	}
}

public class BackupRestoreViewModel : ViewModelBase
{
	public bool DeleteTestVisible
	{
		get { return Get( () => DeleteTestVisible, IsInDesignMode ); }
		set { Set( () => DeleteTestVisible, value ); }
	}

	public bool TestRestoreVisible
	{
		get { return Get( () => TestRestoreVisible, IsInDesignMode ); }
		set { Set( () => TestRestoreVisible, value ); }
	}

	public ObservableCollection<BackupSelection> BackupList
	{
		get { return Get( () => BackupList, new ObservableCollection<BackupSelection>() ); }
		set { Set( () => BackupList, value ); }
	}


	public string BackupName
	{
		get { return Get( () => BackupName, "" ); }
		set { Set( () => BackupName, value ); }
	}


	public bool EnableBackup
	{
		get { return Get( () => EnableBackup, false ); }
		set { Set( () => EnableBackup, value ); }
	}


	public bool ShowBusy
	{
		get { return Get( () => ShowBusy, false ); }
		set { Set( () => ShowBusy, value ); }
	}

	public string BackupStatus
	{
		get { return Get( () => BackupStatus, IsInDesignMode ? "Backup Status" : "" ); }
		set { Set( () => BackupStatus, value ); }
	}


	public bool BackupStatusVisible
	{
		get { return Get( () => BackupStatusVisible, IsInDesignMode ); }
		set { Set( () => BackupStatusVisible, value ); }
	}


	public bool BackupStatusBool
	{
		get { return Get( () => BackupStatusBool, true ); }
		set { Set( () => BackupStatusBool, value ); }
	}


	public bool EnableDelete
	{
		get { return Get( () => EnableDelete, false ); }
		set { Set( () => EnableDelete, value ); }
	}


	public bool EnableRestore
	{
		get { return Get( () => EnableRestore, false ); }
		set { Set( () => EnableRestore, value ); }
	}

	[DependsUpon250( nameof( BackupName ) )]
	public void WhenBackupNameChanges()
	{
		var Name = BackupName;

		EnableBackup = Name.IsNotNullOrEmpty() && ( from B in BackupList
		                                            where B.Name.Compare( Name, StringComparison.OrdinalIgnoreCase ) == 0
		                                            select B ).FirstOrDefault() is null;
	}

	protected override async void OnInitialised()
	{
		if( !IsInDesignMode )
		{
			var IsDebug = await Azure.Client.RequestIsDebug();
			DeleteTestVisible  = IsDebug;
			TestRestoreVisible = !IsDebug;
			await GetBackupList();
		}
	}

	public void SelectionChanged()
	{
		switch( ( from B in BackupList
		          where B.Selected
		          select B ).Count() )

		{
		case 0:
			EnableDelete  = false;
			EnableRestore = false;

			break;

		case 1:
			EnableDelete  = true;
			EnableRestore = true;

			break;

		default:
			EnableDelete  = true;
			EnableRestore = false;

			break;
		}
	}


	public void Execute_Delete()
	{
		ShowBusy            = true;
		BackupStatusVisible = false;

		Task.Run( async () =>
		          {
			          try
			          {
				          var ToDelete = new BackupList();

				          ToDelete.AddRange( from BackupSelection in BackupList
				                             where BackupSelection.Selected
				                             select BackupSelection.Name );

				          DoStatus( "BackupDeleteOk", "BackupDeleteFail", await Azure.Client.RequestDeleteBackups( ToDelete ) );
			          }
			          finally
			          {
				          EnableDelete = false;
				          await GetBackupList();
				          ShowBusy = false;
			          }
		          } );
	}

	public void Execute_Backup()
	{
		ShowBusy            = true;
		BackupStatusVisible = false;

		Task.Run( async () =>
		          {
			          try
			          {
				          var Pid = await Azure.Client.RequestBackupRestore( new Protocol.Data.BackupRestore
				                                                             {
					                                                             Name = BackupName
				                                                             } );

				          do
					          Thread.Sleep( 10_000 );
				          while( await Azure.Client.RequestIsProcessRunning( Pid ) );

				          var Result = await Azure.Client.RequestGetProcessingResponse( Pid );

				          DoStatus( "BackupOk", "BackupFail", Result.Compare( "OK", StringComparison.OrdinalIgnoreCase ) == 0 );
			          }
			          finally
			          {
				          EnableBackup = false;
				          await GetBackupList();
				          ShowBusy = false;
			          }
		          } );
	}


	public async Task Execute_Restore()
	{
		if( !await Azure.Client.RequestIsDebug() )
		{
			var Live = Globals.FindStringResource( "BackupLive" );
			var Warn = Globals.FindStringResource( "Warning" );

			if( MessageBox.Show( Live, Warn, MessageBoxButton.OKCancel ) == MessageBoxResult.Cancel )
				return;
		}

		await DoRestore( false );
	}

	public async Task Execute_TestRestore()
	{
		await DoRestore( true );
	}

	public async Task Execute_DeleteTestData()
	{
		var Live = Globals.FindStringResource( "BackupDeleteTestData" );
		var Warn = Globals.FindStringResource( "Warning" );

		if( MessageBox.Show( Live, Warn, MessageBoxButton.OKCancel ) == MessageBoxResult.Cancel )
			return;

		ShowBusy = true;

		var Pid = await Azure.Client.RequestDeleteTestCarrier( Globals.Company.CarrierId );

		await Task.Run( async () =>
		                {
			                while( await Azure.Client.RequestIsProcessRunning( Pid ) )
				                Thread.Sleep( 10_000 );

			                Dispatcher.Invoke( () =>
			                                   {
				                                   ShowBusy = false;
				                                   Globals.Login.SignOut();
			                                   } );
		                } );
	}

	private async Task GetBackupList()
	{
		var Backups = await Azure.Client.RequestBackupList();

		BackupList = new ObservableCollection<BackupSelection>( from B in Backups
		                                                        select new BackupSelection( this, B ) );
	}


	private void DoStatus( string okKey, string failKey, bool status )
	{
		// ReSharper disable once AssignmentInConditionalExpression
		BackupStatus        = Globals.FindStringResource( ( BackupStatusBool = status ) ? okKey : failKey );
		BackupStatusVisible = true;
	}


	private async Task DoRestore( bool isTest )
	{
		ShowBusy = true;

		await Task.Run( async () =>
		                {
			                try
			                {
				                var Name = ( from B in BackupList
				                             where B.Selected
				                             select B.Name ).FirstOrDefault();

				                if( Name.IsNotNullOrEmpty() )
				                {
					                var Pid = await Azure.Client.RequestBackupRestore( new Protocol.Data.BackupRestore
					                                                                   {
						                                                                   IsRestore     = true,
						                                                                   Name          = Name,
						                                                                   IsTestRestore = isTest
					                                                                   } );

					                do
						                Thread.Sleep( 10_000 );
					                while( await Azure.Client.RequestIsProcessRunning( Pid ) );

					                var Result = await Azure.Client.RequestGetProcessingResponse( Pid );

					                DoStatus( "BackupRestoreOk", "BackupRestoreFail", Result.Compare( "OK", StringComparison.OrdinalIgnoreCase ) == 0 );
				                }
			                }
			                finally
			                {
				                ShowBusy = false;
			                }
		                } );
	}
}