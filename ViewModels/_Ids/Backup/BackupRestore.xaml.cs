﻿using System.Windows.Controls;
using System.Windows.Input;

namespace ViewModels._Ids.Backup;

/// <summary>
///     Interaction logic for BackupRestore.xaml
/// </summary>
public partial class BackupRestore : Page
{
	private bool Shifted;

	public BackupRestore()
	{
		InitializeComponent();
	}

	private void TextBox_KeyDown( object sender, KeyEventArgs e )
	{
		var K = e.Key;

		if( ( K >= Key.A ) && ( K <= Key.Z ) )
			return;

		if( ( K >= Key.D0 ) && ( K <= Key.D9 ) )
		{
			if( Shifted && ( ( K == Key.D9 ) || ( K == Key.D0 ) ) ) // '(' ')'
				return;

			e.Handled = Shifted; // Ok if not shifted

			return;
		}

		switch( K )
		{
		case Key.Space:
		case Key.OemMinus: // Also Does Underscore
			return;

		case Key.LeftShift:
		case Key.RightShift:
			Shifted = true;

			return;
		}

		e.Handled = true;
	}

	private void TextBox_KeyUp( object sender, KeyEventArgs e )
	{
		switch( e.Key )
		{
		case Key.LeftShift:
		case Key.RightShift:
			Shifted = false;

			return;
		}
	}
}