﻿using System.Threading;

namespace ViewModels._Ids;

public class IdsCreateWizardViewModel : ViewModelBase
{
	public enum STATE
	{
		COMPANY_PAGE,
		ADMIN_PAGE,
		SUMMARY
	}


	public bool ShowBusy
	{
		get { return Get( () => ShowBusy, false ); }
		set { Set( () => ShowBusy, value ); }
	}


	public STATE State
	{
		get { return Get( () => State, STATE.COMPANY_PAGE ); }
		set { Set( () => State, value ); }
	}


	public bool CompanyPageVisible
	{
		get { return Get( () => CompanyPageVisible, true ); }
		set { Set( () => CompanyPageVisible, value ); }
	}


	public bool AdminPageVisible
	{
		get { return Get( () => AdminPageVisible, false ); }
		set { Set( () => AdminPageVisible, value ); }
	}


	public bool SummaryPageVisible
	{
		get { return Get( () => SummaryPageVisible, false ); }
		set { Set( () => SummaryPageVisible, value ); }
	}


	public bool PreviousButtonEnabled
	{
		get { return Get( () => PreviousButtonEnabled, false ); }
		set { Set( () => PreviousButtonEnabled, value ); }
	}


	public bool NextButtonEnabled
	{
		get { return Get( () => NextButtonEnabled, false ); }
		set { Set( () => NextButtonEnabled, value ); }
	}


	public bool ExecuteButtonEnabled
	{
		get { return Get( () => ExecuteButtonEnabled, false ); }
		set { Set( () => ExecuteButtonEnabled, value ); }
	}


	public string CarrierId
	{
		get { return Get( () => CarrierId, "" ); }
		set { Set( () => CarrierId, value ); }
	}


	public string AdministratorId
	{
		get { return Get( () => AdministratorId, "" ); }
		set { Set( () => AdministratorId, value ); }
	}


	public string AdministratorPassword
	{
		get { return Get( () => AdministratorPassword, "" ); }
		set { Set( () => AdministratorPassword, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		State     = STATE.COMPANY_PAGE;
		CarrierId = Globals.Security.CarrierId;
	}


	[DependsUpon350( nameof( CarrierId ) )]
	[DependsUpon350( nameof( AdministratorId ) )]
	[DependsUpon350( nameof( AdministratorPassword ) )]
	public void EnableNext()
	{
		var Enab = false;

		switch( State )
		{
		case STATE.COMPANY_PAGE:
			Enab = CarrierId.IsNotNullOrWhiteSpace();

			break;

		case STATE.ADMIN_PAGE:
			Enab = AdministratorId.IsNotNullOrWhiteSpace() && AdministratorPassword.IsNotNullOrWhiteSpace();

			break;
		}

		NextButtonEnabled = Enab;
	}

	public void EnableExecute()
	{
	}

	public void Execute_Next()
	{
		State++;
	}

	public void Execute_Previous()
	{
		State--;
	}

	public void Execute_Execute()
	{
		ShowBusy = true;

		Task.Run( async () =>
		          {
			          try
			          {
				          var Pid = await Azure.Client.RequestCreateCarrier( CarrierId, AdministratorId, AdministratorPassword );

				          do
					          Thread.Sleep( 10_000 );
				          while( await Azure.Client.RequestIsProcessRunning( Pid ) );
			          }
			          finally
			          {
				          Dispatcher.Invoke( () =>
				                             {
					                             ShowBusy = false;
					                             Globals.Login.SignOut();
				                             } );
			          }
		          } );
	}


	[DependsUpon( nameof( State ) )]
	public void WhenStateChanges()
	{
		PreviousButtonEnabled = false;
		NextButtonEnabled     = false;
		ExecuteButtonEnabled  = false;

		CompanyPageVisible = false;
		AdminPageVisible   = false;
		SummaryPageVisible = false;

		switch( State )
		{
		case STATE.COMPANY_PAGE:
			CompanyPageVisible = true;
			EnableNext();

			break;

		case STATE.ADMIN_PAGE:
			AdminPageVisible      = true;
			PreviousButtonEnabled = true;
			EnableNext();

			break;

		case STATE.SUMMARY:
			SummaryPageVisible    = true;
			ExecuteButtonEnabled  = true;
			PreviousButtonEnabled = true;

			break;
		}
	}
}