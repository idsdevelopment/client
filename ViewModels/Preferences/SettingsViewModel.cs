﻿#nullable enable

using System.Windows.Controls;
using IdsControlLibraryV2.Virtualisation;

namespace ViewModels.Settings;

public class DisplayPreferences : Preference
{
	public new bool Enabled
	{
		get => base.Enabled;
		set
		{
			base.Enabled = value;
			SetCurrent();
		}
	}

	public new string StringValue
	{
		get => base.StringValue;
		set
		{
			base.StringValue = value;
			SetCurrent();
		}
	}

	public new int IntValue
	{
		get => base.IntValue;
		set
		{
			base.IntValue = value;
			SetCurrent();
		}
	}

	public new DateTimeOffset DateTimeValue
	{
		get => base.DateTimeValue;
		set
		{
			base.DateTimeValue = value;
			SetCurrent();
		}
	}

	public new decimal DecimalValue
	{
		get => base.DecimalValue;
		set
		{
			base.DecimalValue = value;
			SetCurrent();
		}
	}

	public bool ShowBool     => DataType == BOOL;
	public bool ShowString   => DataType == STRING;
	public bool ShowInt      => DataType == INT;
	public bool ShowDecimal  => DataType == DECIMAL;
	public bool ShowDateTime => DataType == DATE_TIME;
	public bool ShowLabel    => DataType == LABEL;
	public bool ShowEdit     => DataType == 6; // BLOCK_TEXT;

	public FontWeight Bold => IdsOnly || ShowLabel ? FontWeights.Bold : FontWeights.Normal;

	public         SolidColorBrush  Colour => IdsOnly ? IdsSolidColorBrush : DefaultColorBrush;
	private static SolidColorBrush? _IdsSolidColorBrush;

	private static SolidColorBrush? _DefaultColorBrush;

	private static SolidColorBrush IdsSolidColorBrush
	{
		get { return _IdsSolidColorBrush ??= Globals.Dictionary.AsSolidColorBrush( "IdsOptionsBrush" ); }
	}

	private static SolidColorBrush DefaultColorBrush
	{
		get { return _DefaultColorBrush ??= Globals.Dictionary.AsSolidColorBrush( "IdsOptionsTextBrush" ); }
	}


	private readonly SettingsViewModel Owner;

	internal DisplayPreferences( SettingsViewModel owner, Preference preference ) : base( preference )
	{
		Owner = owner;
	}

	private void SetCurrent()
	{
		ViewModelBase.Dispatcher.InvokeAsync( () =>
		                                      {
			                                      Owner.SelectedPreferences = this;
		                                      } );
	}
}

public class OPreferences : AsyncObservableCollection<DisplayPreferences>
{
	private readonly SettingsViewModel Owner;

	public OPreferences( SettingsViewModel owner )
	{
		Owner = owner;
	}

	public void Add( Preference preference )
	{
		base.Add( new DisplayPreferences( Owner, preference ) );
	}
}

public class SettingsViewModel : ViewModelBase
{
	public OPreferences Items
	{
		get { return Get( () => Items, new OPreferences( this ) ); }
		set { Set( () => Items, value ); }
	}

	public OPreferences OrigItems
	{
		get { return Get( () => OrigItems, new OPreferences( this ) ); }
		set { Set( () => OrigItems, value ); }
	}

	public DisplayPreferences SelectedPreferences
	{
		get { return Get( () => SelectedPreferences, new DisplayPreferences( this, new Preference() ) ); }
		set { Set( () => SelectedPreferences, value ); }
	}


	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/649429095/How+to+Edit+Preferences" ); }
	}

	private readonly Dictionary<string, Preference> DictPreferences        = new();
	public readonly  Dictionary<string, Preference> DictChangedPreferences = new();

	public Settings? View;


	protected override async void OnInitialised()
	{
		base.OnInitialised();
		var Temp = Items = new OPreferences( this );

		Preferences Prefs;

		if( !IsInDesignMode )
			Prefs = await Azure.Client.RequestPreferences();
		else
			Prefs = new Preferences();

		foreach( var Preference in Prefs )
		{
			Logging.WriteLogLine( "Loading Preference: " + Preference.Description );

			// KLUDGE
			//if (Preference.Description == "Client: Automatically open shipment tab")
			//                     {
			// Preference.Enabled = true;
			//                     }

			if( !DictPreferences.ContainsKey( Preference.Description ) )
				DictPreferences.Add( Preference.Description, Preference );
			else
				Logging.WriteLogLine( "ERROR - Skipping Preference: " + Preference.Description + " already found" );

			Temp.Add( Preference );

			//OrigItems.Add(Preference);
		}

		OrigItems = new OPreferences( this );

		foreach( var Pref in Items )
			OrigItems.Add( Pref );

		Logging.WriteLogLine( "OrigItems.Count: " + OrigItems.Count );
	}


	//[DependsUpon350( nameof( SelectedPreferences ) )]
	//public async Task WhenSelectedPreferencesChanges()
	//{
	//	await Azure.Client.RequestUpdatePreference( SelectedPreferences );
	//}

	[DependsUpon250( nameof( SelectedPreferences ) )]
	public void WhenSelectedPreferencesChanges()
	{
		ArePendingChangesBool = true;
		Logging.WriteLogLine( "Preference changed: " + SelectedPreferences.Description );
		DictChangedPreferences[ SelectedPreferences.Description ] = SelectedPreferences;
	}

	public void SetView( Settings settings )
	{
		View = settings;
	}


#region Save and Cancel
	public bool ArePendingChangesBool
	{
		get { return Get( () => ArePendingChangesBool, false ); }
		set { Set( () => ArePendingChangesBool, value ); }
	}

	[DependsUpon( nameof( ArePendingChangesBool ) )]
	public string ArePendingChangesString
	{
		get
		{
			var Visibility = "Collapsed";

			if( ArePendingChangesBool )
				Visibility = "Visible";

			return Visibility;
		}
	}

	[DependsUpon( nameof( ArePendingChangesBool ) )]
	public void WhenArePendingChangesChanges()
	{
		Logging.WriteLogLine( "ArePendingChanges changed to " + ArePendingChangesBool );
	}

	public async void Execute_Save()
	{
		var Names = string.Empty;

		foreach( var Key in DictChangedPreferences.Keys )
			Names += Key + ", ";
		Names = Names.Substring( 0, Names.Length - 2 );

		Logging.WriteLogLine( "Saving preferences: " + DictChangedPreferences.Count + " changed: " + Names );

		if( !IsInDesignMode )
		{
			foreach( var KeyVales in DictChangedPreferences )
			{
				Logging.WriteLogLine( "Saving preference: " + KeyVales.Key );
				await Azure.Client.RequestUpdatePreference( KeyVales.Value );
			}
		}

		// Reload the backup collection for Execute_Cancel
		OrigItems = new OPreferences( this );

		foreach( var Item in Items )
			OrigItems.Add( Item );

		// KLUDGE - seems to be needed to get ArePendingChangesBool to wake up
		Items = new OPreferences( this );

		foreach( var Item in OrigItems )
			Items.Add( Item );

		// Finally, reset the collection and flag
		DictChangedPreferences.Clear();
		ArePendingChangesBool = false;

		Globals.Menu.ResetMenuPreferences();
	}

	public void Execute_Cancel()
	{
		Logging.WriteLogLine( "Reverting to last-loaded preferences" );

		Items = new OPreferences( this );

		foreach( var Item in OrigItems )
			Items.Add( Item );

		// Finally, reset the collection and flag
		DictChangedPreferences.Clear();
		ArePendingChangesBool = false;
	}

	private void CheckForTextBoxChanges()
	{
		if( View != null )
		{
			var Lb = View.PreferenceListBox;

			if( Lb != null )
			{
				foreach( var Child in Lb.GetChildren() )
				{
					if( Child is StackPanel Sp )
					{
						foreach( var SpChild in Sp.Children )
						{
							if( SpChild is Label Label )
								Logging.WriteLogLine( "DEBUG label: " + Label.Content );
							else if( SpChild is TextBox Tb )
								Logging.WriteLogLine( "\tDEBUG tb.Text: " + Tb.Text );
						}
					}
				}
			}
		}
	}
#endregion
}