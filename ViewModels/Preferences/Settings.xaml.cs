﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;

namespace ViewModels.Settings;

public partial class Settings : Page
{
	public void Connect( int connectionId, object target )
	{
	}

	public Settings()
	{
		InitializeComponent();
	}

	private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
	{
		if (sender is TextBox Sender)
		{
			Logging.WriteLogLine("DEBUG " + Sender.Text);
			var Text = Sender.Text;

			//var Txt = new StringBuilder();

			//Sender.Text = Txt.ToString();
			var description = string.Empty;

			if (Sender.Parent is StackPanel sp)
			{
				foreach (var child in sp.Children)
				{
					if (child is Label label)
					{
						description = (string)label.Content;
						break;
					}
				}
			}

			UpdatePreference(description, Text);

			e.Handled = true;
		}
	}

	private void TextBoxInt_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( sender is TextBox Sender )
		{
			Logging.WriteLogLine("DEBUG " + Sender.Text);
			var Text = Sender.Text;

			var Txt = new StringBuilder();

			foreach( var C in Text )
			{
				if( ( C >= '0' ) && ( C <= '9' ) )
					Txt.Append( C );
			}
			Sender.Text = Txt.ToString();
			var description = string.Empty;

			if( Sender.Parent is StackPanel sp )
			{
				foreach( var child in sp.Children )
				{
					if( child is Label label )
					{
						description = (string)label.Content;
						break;
					}
				}
			}

			UpdatePreference(description, Text);

			//if( DataContext is SettingsViewModel Model )
			//{
			//	//Logging.WriteLogLine("DEBUG label: " + description);
			//	Model.SetView(this);

			//	DisplayPreferences dp = null;

			//	foreach( var item in Model.Items )
			//	{
			//		if( item.Description == description )
			//		{
			//			//Logging.WriteLogLine("DEBUG found " + item.Description);

			//			//if (item.ShowBool)
			//			//{
			//			//    item.bool = bool.Parse(Text.Trim());
			//			//}
			//			if( item.ShowDateTime )
			//			{
			//				try
			//				{
			//					item.DateTimeValue = DateTime.Parse( Text.Trim() );
			//					dp                 = item;
			//				}
			//				catch
			//				{
			//					Logging.WriteLogLine( "Invalid string: " + Text.Trim() );
			//				}
			//			}
			//			else if( item.ShowDecimal )
			//			{
			//				try
			//				{
			//					item.DecimalValue = decimal.Parse( Text.Trim() );
			//					dp                = item;
			//				}
			//				catch
			//				{
			//					Logging.WriteLogLine( "Invalid string: " + Text.Trim() );
			//				}
			//			}
			//			else if( item.ShowInt )
			//			{
			//				try
			//				{
			//					item.IntValue = int.Parse( Text.Trim() );
			//					dp            = item;
			//				}
			//				catch
			//				{
			//					Logging.WriteLogLine( "Invalid string: " + Text.Trim() );
			//				}
			//			}
			//			//else if (item.ShowLabel)
			//			//                     {

			//			//                     }
			//			else if( item.ShowString )
			//			{
			//				item.StringValue = Sender.Text.Trim();
			//				dp               = item;
			//			}

			//			break;
			//		}
			//	}

			//	if( dp != null )
			//	{
			//		if( Model.DictChangedPreferences.ContainsKey( dp.Description ) )
			//			Model.DictChangedPreferences[ description ] = dp;
			//		else
			//			Model.DictChangedPreferences.Add( description, dp );
			//		Model.ArePendingChangesBool = true;
			//	}
			//}
			e.Handled = true;
			//if (e != null)
			//{
			//	e.Handled = true;
			//}
		}
	}

	private void UpdatePreference(string description, string text)
	{

		if (DataContext is SettingsViewModel Model)
		{
			//Logging.WriteLogLine("DEBUG label: " + description);
			Model.SetView(this);

			DisplayPreferences dp = null;

			foreach (var item in Model.Items)
			{
				if (item.Description == description)
				{
					//Logging.WriteLogLine("DEBUG found " + item.Description);

					//if (item.ShowBool)
					//{
					//    item.bool = bool.Parse(Text.Trim());
					//}
					if (item.ShowDateTime)
					{
						try
						{
							item.DateTimeValue = DateTime.Parse(text.Trim());
							dp = item;
						}
						catch
						{
							Logging.WriteLogLine("Invalid string: " + text.Trim());
						}
					}
					else if (item.ShowDecimal)
					{
						try
						{
							item.DecimalValue = decimal.Parse(text.Trim());
							dp = item;
						}
						catch
						{
							Logging.WriteLogLine("Invalid string: " + text.Trim());
						}
					}
					else if (item.ShowInt)
					{
						try
						{
							item.IntValue = int.Parse(text.Trim());
							dp = item;
						}
						catch
						{
							Logging.WriteLogLine("Invalid string: " + text.Trim());
						}
					}
					//else if (item.ShowLabel)
					//                     {

					//                     }
					else if (item.ShowString)
					{
						item.StringValue = text.Trim();
						dp = item;
					}

					break;
				}
			}

			if (dp != null)
			{
				if (Model.DictChangedPreferences.ContainsKey(dp.Description))
					Model.DictChangedPreferences[description] = dp;
				else
					Model.DictChangedPreferences.Add(description, dp);
				Model.ArePendingChangesBool = true;
			}
		}
	}

	private void BtnHelp_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is SettingsViewModel Model )
		{
			Logging.WriteLogLine( "Opening " + Model.HelpUri );
			var uri = new Uri( Model.HelpUri );
			Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
		}
	}

	private void BtnCancel_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is SettingsViewModel Model )
		{
			PreferenceListBox.ItemsSource = null;
			Model.Execute_Cancel();

			PreferenceListBox.ItemsSource = Model.Items;
		}
	}

	private void BtnSave_Click( object sender, RoutedEventArgs e )
	{
		var mbr = MessageBox.Show( (string)Application.Current.TryFindResource( "SettingsDialogSave" ),
		                           (string)Application.Current.TryFindResource( "SettingsDialogSaveTitle" ),
		                           MessageBoxButton.YesNo, MessageBoxImage.Question );

		if( mbr == MessageBoxResult.Yes )
		{
			if( DataContext is SettingsViewModel Model )
				Model.Execute_Save();
		}
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		
		//TextBox_TextChanged(sender, null);
		if (DataContext is SettingsViewModel model)
		{
			model.ArePendingChangesBool = true;
		}
	}

	private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
	{
		//TextBox_TextChanged(sender, null);
		if (DataContext is SettingsViewModel model)
		{
			model.ArePendingChangesBool = true;
		}
	}
}