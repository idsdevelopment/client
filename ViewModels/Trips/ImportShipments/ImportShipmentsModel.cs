﻿using Utils.Csv;

namespace ViewModels.Trips.ImportShipments;

public class ImportShipmentsModel : ViewModelBase
{
	public new static bool IsInDesignMode { get; private set; }

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/649756883/Address+Book" ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		var Prop = DesignerProperties.IsInDesignModeProperty;
		IsInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty( Prop, typeof( FrameworkElement ) ).Metadata.DefaultValue;

		Task.Run( () =>
				  {
					  Task.WaitAll(
								   GetAccountNames(),
								   GetServiceLevels(),
								   GetPackageTypes()
								  );

					  Loaded = true;
				  } );
	}

	private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

#region Data
	public List<Trip> Trips
	{
		get { return Get( () => Trips, new List<Trip>() ); }
		set { Set( () => Trips, value ); }
	}


	public bool AddShipmentsToDispatch
	{
		get { return Get( () => AddShipmentsToDispatch, false ); }
		set { Set( () => AddShipmentsToDispatch, value ); }
	}


	public List<CompanyAddress> Addresses

	{
		get { return Get( () => Addresses, new List<CompanyAddress>() ); }
		set { Set( () => Addresses, value ); }
	}

	public List<string> AccountNames
	{
		get { return Get( () => AccountNames, new List<string>() ); }
		set { Set( () => AccountNames, value ); }
	}

	public List<string> AccountIds
	{
		get { return Get( () => AccountIds, new List<string>() ); }
		set { Set( () => AccountIds, value ); }
	}

	private async Task GetAccountNames()
	{
		if( !IsInDesignMode )
		{
			try
			{
				if( Addresses.Count == 0 )
				{
					AccountNames.Clear();

					var Ccl = await Azure.Client.RequestGetCustomerCodeCompanyNameList();
					Logging.WriteLogLine( "Found " + Ccl.Count + " Customers" );

					AccountIds = ( from C in Ccl
								   orderby C.CustomerCode
								   select C.CustomerCode ).ToList();

					AccountNames = ( from C in Ccl
								     orderby C.CompanyName
								     select C.CompanyName ).ToList();

					// Load addresses
					foreach( var Account in AccountIds )
					{
						if( !DictAccountAddresses.ContainsKey( Account ) )
							DictAccountAddresses.Add( Account, new List<CompanyDetail>() );

						var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( Account );

						if( Details != null )
						{
							foreach( var Detail in Details )
								DictAccountAddresses[ Account ].Add( Detail );
						}
					}
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown fetching accounts:\n" + E );
			}
		}
	}


	public Dictionary<string, List<CompanyDetail>> DictAccountAddresses
	{
		get { return Get( () => DictAccountAddresses, new Dictionary<string, List<CompanyDetail>>() ); }
		set { Set( () => DictAccountAddresses, value ); }
	}


	public List<string> ServiceLevels
	{
		get { return Get( () => ServiceLevels, new List<string>() ); }
		set { Set( () => ServiceLevels, value ); }
	}

	private async Task GetServiceLevels()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var Sls = await Azure.Client.RequestGetServiceLevelsDetailed();

				if( Sls is not null )
				{
					var Levels = ( from S in Sls
								   orderby S.SortOrder, S.OldName
								   select S.OldName ).ToList();

					ServiceLevels.AddRange( Levels );
					//Dispatcher.Invoke(() =>
					//{
					//    ServiceLevels = Levels;
					//});
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Service Levels\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}

	public List<string> PackageTypes
	{
		get { return Get( () => PackageTypes, new List<string>() ); }
		set { Set( () => PackageTypes, value ); }
	}

	private async Task GetPackageTypes()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var Pts = await Azure.Client.RequestGetPackageTypes();

				if( Pts is not null )
				{
					var PTypes = ( from Pt in Pts
								   orderby Pt.SortOrder, Pt.Description
								   select Pt.Description ).ToList();

					PackageTypes.AddRange( PTypes );
					//Dispatcher.Invoke(() =>
					//{
					//    PackageTypes = PTypes;
					//});
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}


	public string SelectedImportFile
	{
		get { return Get( () => SelectedImportFile, "" ); }
		set { Set( () => SelectedImportFile, value ); }
	}


	public bool IsImportButtonEnabled
	{
		get { return Get( () => IsImportButtonEnabled, false ); }
		set { Set( () => IsImportButtonEnabled, value ); }
	}


	public string Results
	{
		get { return Get( () => Results, "" ); }
		set { Set( () => Results, value ); }
	}

	// Offsets
	private static readonly int CSV_ACCOUNT            = 0;
	private static readonly int CSV_REFERENCE          = 1;
	private static readonly int CSV_CALLERNAME         = 2;
	private static readonly int CSV_CALLERPHONE        = 3;
	private static readonly int CSV_CALLEREMAIL        = 4;
	private static readonly int CSV_PUCOMPANYNAME      = 5;
	private static readonly int CSV_PUCONTACT          = 6;
	private static readonly int CSV_PUPHONE            = 7;
	private static readonly int CSV_PULOCATIONBARCODE  = 8;
	private static readonly int CSV_PUSUITE            = 9;
	private static readonly int CSV_PUSTREET           = 10;
	private static readonly int CSV_PUCITY             = 11;
	private static readonly int CSV_PUREGION           = 12;
	private static readonly int CSV_PUCOUNTRY          = 13;
	private static readonly int CSV_PUPCODE            = 14;
	private static readonly int CSV_PUNOTES            = 15;
	private static readonly int CSV_DELCOMPANYNAME     = 16;
	private static readonly int CSV_DELCONTACT         = 17;
	private static readonly int CSV_DELPHONE           = 18;
	private static readonly int CSV_DELLOCATIONBARCODE = 19;
	private static readonly int CSV_DELSUITE           = 20;
	private static readonly int CSV_DELSTREET          = 21;
	private static readonly int CSV_DELCITY            = 22;
	private static readonly int CSV_DELREGION          = 23;
	private static readonly int CSV_DELCOUNTRY         = 24;
	private static readonly int CSV_DELPCODE           = 25;
	private static readonly int CSV_DELNOTES           = 26;
	private static readonly int CSV_SERVICELEVEL       = 27;
	private static readonly int CSV_READYBY            = 28;
	private static readonly int CSV_DUEBY              = 29;
	private static readonly int CSV_PACKAGE            = 30;
	private static readonly int CSV_PIECE              = 31;
	private static readonly int CSV_WEIGHT             = 32;
	private static readonly int CSV_LENGTH             = 33;
	private static readonly int CSV_WIDTH              = 34;
	private static readonly int CSV_HEIGHT             = 35;
	private static readonly int CSV_DRIVERROUTE        = 36;

	private readonly Dictionary<int, string> DictFieldNames = new()
															  {
																  {CSV_ACCOUNT, "Account"},
																  {CSV_REFERENCE, "Reference"},
																  {CSV_CALLERNAME, "Caller Name"},
																  {CSV_CALLERPHONE, "Caller Phone"},
																  {CSV_CALLEREMAIL, "Caller Email"},

																  {CSV_PUCOMPANYNAME, "PU Company Name"},
																  {CSV_PUCONTACT, "PU Contact"},
																  {CSV_PUPHONE, "PU Phone"},
																  {CSV_PULOCATIONBARCODE, "PU Location Bcode"},
																  {CSV_PUSUITE, "PU Suite"},
																  {CSV_PUSTREET, "PU Street"},
																  {CSV_PUCITY, "PU City"},
																  {CSV_PUREGION, "PU Prov/State"},
																  {CSV_PUCOUNTRY, "PU Country"},
																  {CSV_PUPCODE, "PU Postal/Zip"},
																  {CSV_PUNOTES, "PU Notes"},

																  {CSV_DELCOMPANYNAME, "DEL Company Name"},
																  {CSV_DELCONTACT, "DEL Contact"},
																  {CSV_DELPHONE, "DEL Phone"},
																  {CSV_DELLOCATIONBARCODE, "DEL Location Bcode"},
																  {CSV_DELSUITE, "DEL Suite"},
																  {CSV_DELSTREET, "DEL Street"},
																  {CSV_DELCITY, "DEL City"},
																  {CSV_DELREGION, "DEL Prov/State"},
																  {CSV_DELCOUNTRY, "DEL Country"},
																  {CSV_DELPCODE, "DEL Postal/Zip"},
																  {CSV_DELNOTES, "DEL Notes"},

																  {CSV_SERVICELEVEL, "Service Level"},
																  {CSV_READYBY, "Ready By"},
																  {CSV_DUEBY, "Due By"},
																  {CSV_PACKAGE, "Package"},
																  {CSV_PIECE, "Piece"},
																  {CSV_WEIGHT, "Weight"},
																  {CSV_LENGTH, "Length"},
																  {CSV_WIDTH, "Width"},
																  {CSV_HEIGHT, "Height"},
																  {CSV_DRIVERROUTE, "Driver/Route"}
															  };

	private readonly Dictionary<int, string> DictMandatoryFields = new()
																   {
																	   {CSV_ACCOUNT, "Account"},
																	   {CSV_PUCOMPANYNAME, "PU Company Name"},
																	   {CSV_PUSTREET, "PU Street"},
																	   {CSV_PUCITY, "PU City"},
																	   {CSV_PUREGION, "PU Prov/State"},
																	   {CSV_PUCOUNTRY, "PU Country"},
																	   {CSV_PUPCODE, "PU Postal/Zip"},
																	   {CSV_DELCOMPANYNAME, "DEL Company Name"},
																	   {CSV_DELSTREET, "DEL Street"},
																	   {CSV_DELCITY, "DEL City"},
																	   {CSV_DELREGION, "DEL Prov/State"},
																	   {CSV_DELCOUNTRY, "DEL Country"},
																	   {CSV_DELPCODE, "DEL Postal/Zip"},
																	   {CSV_SERVICELEVEL, "Service Level"},
																	   {CSV_PACKAGE, "Package"},
																	   {CSV_PIECE, "Piece"},
																	   {CSV_WEIGHT, "Weight"}
																   };


	public bool IsUpdateAddressesChecked
	{
		get { return Get( () => IsUpdateAddressesChecked, false ); }
		set { Set( () => IsUpdateAddressesChecked, value ); }
	}

	//[DependsUpon(nameof(IsUpdateAddressesChecked))]
	//public void WhenIsUpdateAddressesCheckedChanges()
	//{
	//    Logging.WriteLogLine("DEBUG " + IsUpdateAddressesChecked);
	//}
#endregion

#region Actions
	public (bool isCorrect, List<string> errors) CheckHeaders()
	{
		var          IsCorrect = true;
		List<string> Errors    = new();

		try
		{
			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			// Just the first line
			var Header = CsvObj[ 0 ];

			if( Header.Count == DictFieldNames.Count )
			{
				for( var I = 0; I < Header.Count; I++ )
					CheckHeaderColumn( Header[ I ], DictFieldNames[ I ], ref Errors );
			}
			else
			{
				// Wrong number of headers 
				var Error = FindStringResource( "ImportShipmentsHeadersWrongNumberError" );
				Error = Error.Replace( "@1", DictFieldNames.Count + "" );
				Error = Error.Replace( "@2", Header.Count + "" );
				Errors.Add( Error );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );
			IsCorrect = false;
		}

		if( Errors.Count > 0 )
			IsCorrect = false;

		return ( IsCorrect, Errors );
	}

	private static void CheckHeaderColumn( string got, string shouldBe, ref List<string> errors )
	{
		if( !string.Equals( got, shouldBe, StringComparison.CurrentCultureIgnoreCase ) )
		{
			var Error = FindStringResource( "ImportShipmentsHeadersError" );
			Error = Error.Replace( "@1", got );
			Error = Error.Replace( "@2", shouldBe );
			errors.Add( Error );
		}
	}

	public (bool isCorrect, List<string> errors) CheckFileContents()
	{
		var          IsCorrect = true;
		List<string> Errors    = new();

		try
		{
			var CsvObj = Csv.ReadFromFile( SelectedImportFile );

			var NumberOfLines = 1; // Include header

			// Skip the header
			for( var I = 1; I < CsvObj.RowCount; I++ )
			{
				++NumberOfLines;
				var Line   = CsvObj[ I ];
				var Values = ProcessLine( Line );

				CheckLineForMandatoryFields( I, Values, ref Errors );

				CheckAccount( Values[ CSV_ACCOUNT ], I, ref Errors );

				CheckServiceLevel( Values[ CSV_SERVICELEVEL ], I, ref Errors );

				CheckPackageType( Values[ CSV_PACKAGE ], I, ref Errors );
			}
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + E );
			Errors.Add( "Unable to read the file: " + SelectedImportFile );
			IsCorrect = false;
		}

		if( Errors.Count > 0 )
			IsCorrect = false;

		return ( IsCorrect, Errors );
	}

	private static List<string> ProcessLine( Row line )
	{
		List<string> Values = new() {line[ CSV_ACCOUNT ]};

		Logging.WriteLogLine( "Account: " + line[ CSV_ACCOUNT ] );
		Values.Add( line[ CSV_REFERENCE ] );
		Logging.WriteLogLine( "Reference: " + line[ CSV_REFERENCE ] );
		Values.Add( line[ CSV_CALLERNAME ] );
		Logging.WriteLogLine( "CallerName: " + line[ CSV_CALLERNAME ] );
		Values.Add( line[ CSV_CALLERPHONE ] );
		Logging.WriteLogLine( "CallerPhone: " + line[ CSV_CALLERPHONE ] );
		Values.Add( line[ CSV_CALLEREMAIL ] );
		Logging.WriteLogLine( "CallerEmail: " + line[ CSV_CALLEREMAIL ] );

		Values.Add( line[ CSV_PUCOMPANYNAME ] );
		Logging.WriteLogLine( "puCompanyName: " + line[ CSV_PUCOMPANYNAME ] );
		Values.Add( line[ CSV_PUCONTACT ] );
		Logging.WriteLogLine( "puContact: " + line[ CSV_PUCONTACT ] );
		Values.Add( line[ CSV_PUPHONE ] );
		Logging.WriteLogLine( "puPhone: " + line[ CSV_PUPHONE ] );
		Values.Add( line[ CSV_PULOCATIONBARCODE ] );
		Logging.WriteLogLine( "puLocationBarcode: " + line[ CSV_PULOCATIONBARCODE ] );
		Values.Add( line[ CSV_PUSUITE ] );
		Logging.WriteLogLine( "puSuite: " + line[ CSV_PUSUITE ] );
		Values.Add( line[ CSV_PUSTREET ] );
		Logging.WriteLogLine( "puStreet: " + line[ CSV_PUSTREET ] );
		Values.Add( line[ CSV_PUCITY ] );
		Logging.WriteLogLine( "puCity: " + line[ CSV_PUCITY ] );
		Values.Add( line[ CSV_PUREGION ] );
		Logging.WriteLogLine( "puRegion: " + line[ CSV_PUREGION ] );
		Values.Add( line[ CSV_PUCOUNTRY ] );
		Logging.WriteLogLine( "puCountry: " + line[ CSV_PUCOUNTRY ] );
		Values.Add( line[ CSV_PUPCODE ] );
		Logging.WriteLogLine( "puPcode: " + line[ CSV_PUPCODE ] );
		Values.Add( line[ CSV_PUNOTES ] );
		Logging.WriteLogLine( "puNotes: " + line[ CSV_PUNOTES ] );

		Values.Add( line[ CSV_DELCOMPANYNAME ] );
		Logging.WriteLogLine( "delCompanyName: " + line[ CSV_DELCOMPANYNAME ] );
		Values.Add( line[ CSV_DELCONTACT ] );
		Logging.WriteLogLine( "delContact: " + line[ CSV_DELCONTACT ] );
		Values.Add( line[ CSV_DELPHONE ] );
		Logging.WriteLogLine( "delPhone: " + line[ CSV_DELPHONE ] );
		Values.Add( line[ CSV_DELLOCATIONBARCODE ] );
		Logging.WriteLogLine( "delLocationBarcode: " + line[ CSV_DELLOCATIONBARCODE ] );
		Values.Add( line[ CSV_DELSUITE ] );
		Logging.WriteLogLine( "delSuite: " + line[ CSV_DELSUITE ] );
		Values.Add( line[ CSV_DELSTREET ] );
		Logging.WriteLogLine( "delStreet: " + line[ CSV_DELSTREET ] );
		Values.Add( line[ CSV_DELCITY ] );
		Logging.WriteLogLine( "delCity: " + line[ CSV_DELCITY ] );
		Values.Add( line[ CSV_DELREGION ] );
		Logging.WriteLogLine( "delRegion: " + line[ CSV_DELREGION ] );
		Values.Add( line[ CSV_DELCOUNTRY ] );
		Logging.WriteLogLine( "delCountry: " + line[ CSV_DELCOUNTRY ] );
		Values.Add( line[ CSV_DELPCODE ] );
		Logging.WriteLogLine( "delPcode: " + line[ CSV_DELPCODE ] );
		Values.Add( line[ CSV_DELNOTES ] );
		Logging.WriteLogLine( "delNotes: " + line[ CSV_DELNOTES ] );

		Values.Add( line[ CSV_SERVICELEVEL ] );
		Logging.WriteLogLine( "serviceLevel: " + line[ CSV_SERVICELEVEL ] );
		Values.Add( line[ CSV_READYBY ] );
		Logging.WriteLogLine( "readyBy: " + line[ CSV_READYBY ] );
		Values.Add( line[ CSV_DUEBY ] );
		Logging.WriteLogLine( "dueBy: " + line[ CSV_DUEBY ] );
		Values.Add( line[ CSV_PACKAGE ] );
		Logging.WriteLogLine( "package: " + line[ CSV_PACKAGE ] );
		Values.Add( line[ CSV_PIECE ] );
		Logging.WriteLogLine( "piece: " + line[ CSV_PIECE ] );
		Values.Add( line[ CSV_WEIGHT ] );
		Logging.WriteLogLine( "weight: " + line[ CSV_WEIGHT ] );
		Values.Add( line[ CSV_LENGTH ] );
		Logging.WriteLogLine( "length: " + line[ CSV_LENGTH ] );
		Values.Add( line[ CSV_WIDTH ] );
		Logging.WriteLogLine( "width: " + line[ CSV_WIDTH ] );
		Values.Add( line[ CSV_HEIGHT ] );
		Logging.WriteLogLine( "height: " + line[ CSV_HEIGHT ] );
		Values.Add( line[ CSV_DRIVERROUTE ] );
		Logging.WriteLogLine( "driverRoute: " + line[ CSV_DRIVERROUTE ] );

		return Values;
	}

	private bool CheckAccount( string account, int lineNumber, ref List<string> errors )
	{
		var IsValid = true;

		if( !AccountIds.Contains( account ) )
		{
			var Error = FindStringResource( "ImportShipmentsAccountNotFoundError" );
			Error = Error.Replace( "@1", lineNumber + "" );
			Error = Error.Replace( "@2", account );
			errors.Add( Error );
		}

		return IsValid;
	}

	private bool CheckServiceLevel( string serviceLevel, int lineNumber, ref List<string> errors )
	{
		var IsValid = true;

		if( !ServiceLevels.Contains( serviceLevel ) )
		{
			IsValid = false;
			var Error = FindStringResource( "ImportShipmentsServiceLevelNotFoundError" );
			Error = Error.Replace( "@1", lineNumber + "" );
			Error = Error.Replace( "@2", serviceLevel );
			errors.Add( Error );
		}

		return IsValid;
	}

	private bool CheckPackageType( string packageType, int lineNumber, ref List<string> errors )
	{
		var IsValid = true;

		if( !PackageTypes.Contains( packageType ) )
		{
			IsValid = false;
			var Error = FindStringResource( "ImportShipmentsPackageTypeNotFoundError" );
			Error = Error.Replace( "@1", lineNumber + "" );
			Error = Error.Replace( "@2", packageType );
			errors.Add( Error );
		}

		return IsValid;
	}

	private void CheckLineForMandatoryFields( int lineNumber, List<string> line, ref List<string> errors )
	{
		foreach( var Key in DictMandatoryFields.Keys )
		{
			var Value = line[ Key ];

			if( Value.Length == 0 )
			{
				var Error = FindStringResource( "ImportShipmentsMissingFieldError" );
				Error = Error.Replace( "@1", lineNumber + "" );
				Error = Error.Replace( "@2", DictFieldNames[ Key ] );
				errors.Add( Error );
			}
		}
	}

	public string ImportShipments()
	{
		var CsvObj = Csv.ReadFromFile( SelectedImportFile );

		var TripsByReference = GetTripsByReference( CsvObj );
		Logging.WriteLogLine( "DEBUG Found " + TripsByReference.Count + " references" );
		//var NumberOfLines = 1; // Include header

		StringBuilder Sb       = new();
		var           AddedMsg = FindStringResource( "ImportShipmentsResultsAddedTrip" );

		foreach( var Reference in TripsByReference.Keys )
		{
			Logging.WriteLogLine( "DEBUG reference: " + Reference );

			if( Reference != "" )
			{
				var Trip = BuildTrip( TripsByReference[ Reference ] );
				Trips.Add( Trip );
				Sb.Append( AddedMsg + " " + Trip.TripId + "\r\n" );
			}
			else
			{
				// Trips with no reference - 1 per line
				foreach( var Line in TripsByReference[ Reference ] )
				{
					var List = new List<List<string>>
							   {
								   Line
							   };
					var Trip = BuildTrip( List );
					Trips.Add( Trip );
					Sb.Append( AddedMsg + " " + Trip.TripId + "\r\n" );
				}
			}
		}
		var Msg = FindStringResource( "ImportShipmentsResultsTotalTrips" );
		Msg     =  Msg.Replace( "@1", Trips.Count + "" );
		Results += Msg + "\r\n" + Sb;

		//if (Trips.Count > 0 && AddShipmentsToDispatch)
		if( Trips.Count > 0 )
		{
			var Tul = new TripUpdateList
					  {
						  Program = "ImportShipmentsModel"
					  };
			Tul.Trips.AddRange( Trips );

			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   await Azure.Client.RequestAddUpdateTrips( Tul );
								   } )
						);
			//Trips.Clear();
		}

		IsImportButtonEnabled = false;
		SelectedImportFile    = string.Empty;
		RaisePropertyChanged( nameof( IsImportButtonEnabled ) );
		RaisePropertyChanged( nameof( SelectedImportFile ) );

		return Results;
	}

	/// <summary>
	///     Groups the trips together by reference.
	/// </summary>
	/// <param
	///     name="raw">
	/// </param>
	/// <returns></returns>
	private Dictionary<string, List<List<string>>> GetTripsByReference( Csv raw )
	{
		Dictionary<string, List<List<string>>> Dict = new();

		// Skip the header
		for( var I = 1; I < raw.RowCount; I++ )
		{
			var Line      = raw[ I ];
			var Values    = ProcessLine( Line );
			var Reference = Values[ CSV_REFERENCE ];

			if( !Dict.ContainsKey( Reference ) )
				Dict.Add( Reference, new List<List<string>>() );
			Dict[ Reference ].Add( Values );
		}

		return Dict;
	}

	/// <summary>
	///     Note: at this point we know that the account, service level, and package type are correct.
	/// </summary>
	/// <param
	///     name="raw">
	/// </param>
	/// <returns></returns>
	private Trip BuildTrip( List<List<string>> raw )
	{
		// Master trip
		var Trip = BuildBaseTrip( raw[ 0 ] );

		decimal TotalPieces = 0;
		decimal TotalWeight = 0;
		Trip.Packages = new List<TripPackage>();
		var Tps = new List<TripPackage>();

		for( var I = 0; I < raw.Count; I++ )
		{
			var Items = new List<TripItem>();

			var Ti = new TripItem
					 {
						 ItemCode    = raw[ 0 ][ CSV_SERVICELEVEL ],
						 Description = raw[ I ][ CSV_PACKAGE ],
						 Pieces      = decimal.Parse( raw[ I ][ CSV_PIECE ] ),
						 Weight      = decimal.Parse( raw[ I ][ CSV_WEIGHT ] )
					 };

			if( raw[ I ][ CSV_LENGTH ].Length > 0 )
				Ti.Length = decimal.Parse( raw[ I ][ CSV_LENGTH ] );

			if( raw[ I ][ CSV_WIDTH ].Length > 0 )
				Ti.Width = decimal.Parse( raw[ I ][ CSV_WIDTH ] );

			if( raw[ I ][ CSV_HEIGHT ].Length > 0 )
				Ti.Height = decimal.Parse( raw[ I ][ CSV_HEIGHT ] );
			Items.Add( Ti );
			TotalPieces += Ti.Pieces;
			TotalWeight += Ti.Weight;

			TripPackage Tp = new()
							 {
								 //PackageType = trip.PackageType,
								 PackageType = Ti.Description,
								 Items       = Items,
								 Pieces      = Ti.Pieces,
								 Weight      = Ti.Weight
							 };
			//trip.Packages.Add(tp);
			Tps.Add( Tp );
		}

		//var tps = new List<TripPackage> { new() { PackageType = trip.PackageType, Items = items, Pieces = totalPieces, Weight = totalWeight } };
		Trip.Packages = Tps;
		Trip.Pieces   = TotalPieces;
		Trip.Weight   = TotalWeight;

		return Trip;
	}

	private Trip BuildBaseTrip( List<string> raw )
	{
		Trip Trip = new()
					{
						// If an address already exists with the same location barcode, use it
						Program = "ImportShipmentsModel",
						TripId  = GetNewTripId()
					};

		if( AddShipmentsToDispatch )
			Trip.BroadcastToDispatchBoard = true;
		Trip.AccountId           = raw[ CSV_ACCOUNT ];
		Trip.CallerName          = raw[ CSV_CALLERNAME ];
		Trip.CallerPhone         = raw[ CSV_CALLERPHONE ];
		Trip.CallerEmail         = raw[ CSV_CALLEREMAIL ];
		Trip.CallTime            = DateTimeOffset.Now;
		Trip.IsCallTimeSpecified = true;

		if( raw[ CSV_READYBY ] != string.Empty )
		{
			var Tmp = DateTimeOffset.Parse( raw[ CSV_READYBY ] );

			Trip.ReadyTime = Tmp > DateTimeOffset.MinValue ? Tmp : Trip.CallTime;
		}
		else
			Trip.ReadyTime = DateTimeOffset.Now;
		Trip.ReadyTimeSpecified = true;

		if( raw[ CSV_DUEBY ] != string.Empty )
		{
			var Tmp = DateTimeOffset.Parse( raw[ CSV_DUEBY ] );

			Trip.DueTime = Tmp > DateTimeOffset.MinValue ? Tmp : Trip.CallTime;
		}
		else
			Trip.DueTime = Trip.CallTime;
		Trip.DueTimeSpecified = true;
		Trip.Reference        = raw[ CSV_REFERENCE ];
		Trip.ServiceLevel     = raw[ CSV_SERVICELEVEL ];
		Trip.PackageType      = raw[ CSV_PACKAGE ];
		Trip.Pieces           = decimal.Parse( raw[ CSV_PIECE ] );
		Trip.Weight           = decimal.Parse( raw[ CSV_WEIGHT ] );
		Trip.Driver           = raw[ CSV_DRIVERROUTE ];
		Trip.Status1          = STATUS.ACTIVE;

		// Only returns addresses if !IsUpdateAddressChecked
		var (Pu, Del) = CheckAddresses( raw );

		if( Pu == null )
		{
			Trip.PickupCompanyName         = raw[ CSV_PUCOMPANYNAME ];
			Trip.PickupAddressSuite        = raw[ CSV_PUSUITE ];
			Trip.PickupAddressAddressLine1 = raw[ CSV_PUSTREET ];
			Trip.PickupAddressCity         = raw[ CSV_PUCITY ];
			Trip.PickupAddressRegion       = raw[ CSV_PUREGION ];
			Trip.PickupAddressCountry      = raw[ CSV_PUCOUNTRY ];
			Trip.PickupAddressPostalCode   = raw[ CSV_PUPCODE ];
			Trip.PickupAddressNotes        = raw[ CSV_PUNOTES ];
		}
		else
			Trip = PopulateAddress( Trip, Pu, true );

		if( Del == null )
		{
			Trip.DeliveryCompanyName         = raw[ CSV_DELCOMPANYNAME ];
			Trip.DeliveryAddressSuite        = raw[ CSV_DELSUITE ];
			Trip.DeliveryAddressAddressLine1 = raw[ CSV_DELSTREET ];
			Trip.DeliveryAddressCity         = raw[ CSV_DELCITY ];
			Trip.DeliveryAddressRegion       = raw[ CSV_DELREGION ];
			Trip.DeliveryAddressCountry      = raw[ CSV_DELCOUNTRY ];
			Trip.DeliveryAddressPostalCode   = raw[ CSV_DELPCODE ];
			Trip.DeliveryAddressNotes        = raw[ CSV_DELNOTES ];
		}
		else
			Trip = PopulateAddress( Trip, Del, false );

		return Trip;
	}

	private static string GetNewTripId()
	{
		var TripId = string.Empty;

		if( !IsInDesignMode )
		{
			Task.WaitAll(
						 Task.Run( async () =>
								   {
									   TripId = await Azure.Client.RequestGetNextTripId();
								   } )
						);
		}

		return TripId;
	}

	private (CompanyDetail pu, CompanyDetail del) CheckAddresses( IReadOnlyList<string> raw )
	{
		CompanyDetail Pu  = null;
		CompanyDetail Del = null;

		if( !IsUpdateAddressesChecked && DictAccountAddresses.ContainsKey( raw[ CSV_ACCOUNT ] ) )
		{
			var Detail = ( from D in DictAccountAddresses[ raw[ CSV_ACCOUNT ] ]
						   where D.Address.LocationBarcode == raw[ CSV_PULOCATIONBARCODE ]
						   select D ).FirstOrDefault();
			Pu = Detail;

			Detail = ( from D in DictAccountAddresses[ raw[ CSV_ACCOUNT ] ]
					   where D.Address.LocationBarcode == raw[ CSV_DELLOCATIONBARCODE ]
					   select D ).FirstOrDefault();
			Del = Detail;
		}

		return ( Pu, Del );
	}

	private static Trip PopulateAddress( Trip trip, CompanyDetail cd, bool isPu )
	{
		if( isPu )
		{
			trip.PickupCompanyName         = cd.Company.CompanyName;
			trip.PickupAddressSuite        = cd.Address.Suite;
			trip.PickupAddressAddressLine1 = cd.Address.AddressLine1;
			trip.PickupAddressAddressLine2 = cd.Address.AddressLine2;
			trip.PickupAddressCity         = cd.Address.City;
			trip.PickupAddressRegion       = cd.Address.Region;
			trip.PickupAddressCountry      = cd.Address.Country;
			trip.PickupAddressPostalCode   = cd.Address.PostalCode;
		}
		else
		{
			trip.DeliveryCompanyName         = cd.Company.CompanyName;
			trip.DeliveryAddressSuite        = cd.Address.Suite;
			trip.DeliveryAddressAddressLine1 = cd.Address.AddressLine1;
			trip.DeliveryAddressAddressLine2 = cd.Address.AddressLine2;
			trip.DeliveryAddressCity         = cd.Address.City;
			trip.DeliveryAddressRegion       = cd.Address.Region;
			trip.DeliveryAddressCountry      = cd.Address.Country;
			trip.DeliveryAddressPostalCode   = cd.Address.PostalCode;
		}

		return trip;
	}
#endregion
}