﻿using Microsoft.Win32;

namespace ViewModels.Trips.ImportShipments;

/// <summary>
///     Interaction logic for ImportShipments.xaml
/// </summary>
public partial class ImportShipments : Window
{
	/// <summary>
	///     Used by Route to get trips.
	/// </summary>
	/// <returns></returns>
	public List<Trip> GetTrips()
	{
		if( DataContext is ImportShipmentsModel Model )
		{
			ShowDialog();

			return Model.Trips;
		}
		return null;
	}

	public ImportShipments( Window owner, bool addToDispatch = false )
	{
		Owner = owner;

		InitializeComponent();

		if( DataContext is ImportShipmentsModel Model )
			Model.AddShipmentsToDispatch = addToDispatch;

		if( addToDispatch )
			ShowDialog();
	}

	private void BtnBrowse_Click( object sender, RoutedEventArgs e )
	{
		var filter = "CSV Files (*.csv)|*.csv|Text Files (*.txt)|*txt";

		var ofd = new OpenFileDialog
		          {
			          Filter = filter
		          };
		ofd.Multiselect = false;
		var result = ofd.ShowDialog();

		if( result == true )
		{
			Logging.WriteLogLine( "Selected " + ofd.FileName );
			var file = ofd.FileName;

			if( DataContext is ImportShipmentsModel Model )
			{
				Model.SelectedImportFile = file;
				tbFileName.Text          = file;
				var (isCorrect, errors)  = Model.CheckHeaders();

				if( !isCorrect )
				{
					Model.SelectedImportFile    = string.Empty;
					Model.IsImportButtonEnabled = false;
					var title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeaderTitle" );
					var message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeader" );
					message = message.Replace( "@1", file );

					foreach( var error in errors )
						message += "\n\t" + error;
					message += "\n" + (string)Application.Current.TryFindResource( "AddressBookImportAddressesFormatErrorsInFileHeaderEnd" );

					//Model.Results = message;
					TbResults.Text = message;

					NotepadHelper.ShowMessage( message, "Errors" );

					MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
				}
				else
				{
					( isCorrect, errors ) = Model.CheckFileContents();

					if( !isCorrect )
					{
						Model.SelectedImportFile    = string.Empty;
						Model.IsImportButtonEnabled = false;
						var title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
						var message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFile" );
						message = message.Replace( "@1", file );

						foreach( var error in errors )
							message += "\n" + error;
						message += "\n" + (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileEnd" );

						//Model.Results = message;
						TbResults.Text = message;

						NotepadHelper.ShowMessage( message, "Errors" );

						MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
					}
					else
					{
						Model.IsImportButtonEnabled = true;
						BtnImport.IsEnabled         = true;
					}
				}
			}
		}
	}

	private void BtnImport_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is ImportShipmentsModel Model )
		{
			var results = Model.ImportShipments();
			TbResults.Text = results;
		}
	}

	private void BtnClose_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}
}