﻿#nullable enable

namespace ViewModels.Trips.Boards.Common;

public static class DisplayTripExtensions
{
	public static string AsString( this DisplayTrip.STATUS status )
	{
		return status switch
			   {
				   DisplayTrip.STATUS.UNPICKUPABLE      => "Cannot pick-up",
				   DisplayTrip.STATUS.UNDELIVERABLE     => "Undeliverable",
				   DisplayTrip.STATUS.UNDELIVERABLE_NEW => "Undeliverable",
				   _                                    => ( (STATUS)status ).AsString()
			   };
	}
}

public class Driver : INotifyPropertyChanged
{
	public string StaffId     { get; set; } = "";
	public string DisplayName { get; set; } = "";

	public bool IsVisible
	{
		get => _IsVisible && HasTrips;
		set
		{
			if( _IsVisible != value )
			{
				_IsVisible = value;
				OnPropertyChanged();
			}
		}
	}

	public bool HasTrips
	{
		get => _HasTrips;
		set
		{
			if( _HasTrips != value )
			{
				_HasTrips = value;
				OnPropertyChanged();
			}
		}
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	private bool _HasTrips = true;

	private bool _IsVisible = true;

	public override string ToString() => StaffId;

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class ServiceLevel
{
	public Protocol.Data.ServiceLevel RawServiceLevel { get; set; } = new();

	public ServiceLevel()
	{
		Foreground = new SolidColorBrush( Colors.Black );
		Background = new SolidColorBrush( Colors.White );
	}

	public ServiceLevel( Protocol.Data.ServiceLevel s )
	{
		RawServiceLevel = s;
		Foreground      = s.ForegroundARGB.ArgbToSolidColorBrush();
		Background      = s.BackgroundARGB.ArgbToSolidColorBrush();
	}

	public SolidColorBrush Foreground,
						   Background;

	public string Name = null!;
}

public class ReadyTimeCondition : INotifyPropertyChanged
{
	public string Text
	{
		get => _Text;
		set
		{
			_Text = value;
			OnPropertyChanged();
		}
	}

	public SolidColorBrush Foreground
	{
		get => _Foreground;
		set
		{
			_Foreground = value;
			OnPropertyChanged();
		}
	}

	public SolidColorBrush Background
	{
		get => _Background;
		set
		{
			_Background = value;
			OnPropertyChanged();
		}
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="s">
	/// </param>
	/// <param
	///     name="readyTime">
	/// </param>
	public ReadyTimeCondition( Protocol.Data.ServiceLevel s, DateTimeOffset? readyTime )
	{
		readyTime ??= DateTimeOffset.MinValue;

		//Logging.WriteLogLine( "ServiceLevel: " + s.NewName + ", readyTime: " + readyTime );
		_ServiceLevel = s;
		_ReadyTime    = (DateTimeOffset)readyTime;

		BuildCondition();
	}

	private SolidColorBrush _Background = Brushes.Transparent;

	private SolidColorBrush _Foreground = null!;

	private DateTimeOffset _ReadyTime;

	private readonly Protocol.Data.ServiceLevel _ServiceLevel;

	private string _Text = null!;

	/// <summary>
	///     From https://idsservices.atlassian.net/browse/I2P-11 and https://idsservices.atlassian.net/browse/I2P-12
	///     a. Looking at the Service Level Screen in the Rate Wizard you will see three fields labelled
	///     ”Warn”,”Critical”,”Beyond”.
	///     b.These fields are numerical fields and will contain a number equal to minutes.They can contain a negative
	///     number(Im thinking
	///     you will need a negative number to handle “Beyond” being late)
	///     c.The number of minutes entered into these fields will be compared to the “Ready By” field and the current
	///     time/date.
	///     d.If the current time difference between  “Ready By” is less than “Warn” but sooner than “Critical”, then in the
	///     “Condition” field
	///     for that shipment will display the word “WARN” in a dark yellow colour.
	///     e.If the current time difference between “Ready By” is less than “Critical” but still sooner than “Ready By” , then
	///     in the “Condition”
	///     field for that shipment display the word “CRITICAL” in a dark orange.
	///     f.If the current time is past the “Ready By” time but still before “Beyond” , then in the “Condition” field for
	///     that shipment,
	///     display the word “LATE” in a dark red.
	///     g.If the current time has passed “Beyond” , then in the “Condition” field for that shipment, display the word
	///     “BEYOND” in a dark yellow red.
	/// </summary>
	public void BuildCondition( bool isOdd = false )
	{
		var Set             = false;
		var WarningMinutes  = -1;
		var CriticalMinutes = -1;
		var BeyondMinutes   = -1;

		if( _ServiceLevel.WarningHours.Hours == 0 )
			WarningMinutes = (int)_ServiceLevel.WarningHours.TotalMinutes;

		if( _ServiceLevel.CriticalHours.Hours == 0 )
			CriticalMinutes = (int)_ServiceLevel.CriticalHours.TotalMinutes;

		if( _ServiceLevel.BeyondHours.Hours == 0 )
			BeyondMinutes = (int)_ServiceLevel.BeyondHours.TotalMinutes;

		if( ( WarningMinutes > -1 ) || ( CriticalMinutes > -1 ) || ( BeyondMinutes > -1 ) )
		{
			var Now = DateTimeOffset.Now;

			//Logging.WriteLogLine( "now: " + now + " - ReadyTime: " + _ReadyTime );

			//int diff = (int)(_ReadyTime - now).TotalMinutes;
			//               Logging.WriteLogLine("Time difference in minutes: " + diff);

			if( ( WarningMinutes > -1 ) && ( CriticalMinutes > -1 ) )
			{
				if( ( Now >= _ReadyTime.AddMinutes( -WarningMinutes ) ) && ( Now < _ReadyTime.AddMinutes( -CriticalMinutes ) ) )
				{
					Set = true;

					//Logging.WriteLogLine("Setting to WARN");
					Text       = "WARN";
					Foreground = Brushes.DarkGoldenrod;
					Background = Brushes.White; // Ensure that the foreground isn't masked by a service level background colour
				}
			}

			if( !Set && ( CriticalMinutes > -1 ) && ( BeyondMinutes > -1 ) )
			{
				//if( ( now >= _ReadyTime.AddMinutes( -criticalMinutes ) ) && ( now <= _ReadyTime.AddMinutes( -beyondMinutes ) ) )
				if( ( Now >= _ReadyTime.AddMinutes( -CriticalMinutes ) ) && ( Now < _ReadyTime ) )
				{
					Set = true;

					//Logging.WriteLogLine("Setting to CRITICAL");
					Text       = "CRITICAL";
					Foreground = Brushes.DarkOrange;
					Background = Brushes.White;
				}
			}

			if( !Set && ( BeyondMinutes > -1 ) )
			{
				if( Now >= _ReadyTime )
				{
					if( Now < _ReadyTime.AddMinutes( BeyondMinutes ) )
					{
						Set = true;

						//Logging.WriteLogLine("Setting to LATE");
						Text       = "LATE";
						Foreground = Brushes.DarkRed;
						Background = Brushes.White;
					}
					else if( Now >= _ReadyTime.AddMinutes( BeyondMinutes ) )
					{
						Set = true;

						//Logging.WriteLogLine("DEBUG Setting to BEYOND");
						Text = "BEYOND";

						//var brush = (SolidColorBrush)new BrushConverter().ConvertFrom("#fa5a05");
						//Foreground = brush;
						Foreground = Brushes.Red;
						Background = Brushes.White;
					}
				}
			}

			if( !Set )
			{
				//Logging.WriteLogLine( "DEBUG setting background to White or LightGray" );

				//Background = isOdd ? Brushes.LightGray : Brushes.White;
				//if( isOdd )
				//	Background = Brushes.LightGray;
				//else
				//	Background = Brushes.White;
				Background = isOdd ? Brushes.White : Brushes.LightGray;
			}

			//if (!set)
			//            {
			//                Background = Brushes.LightGray;
			//            }
			//if( set )
			//	Logging.WriteLogLine( "Condition was set to " + Text );
		}

		//else
		//{
		//    Background = Brushes.LightGray;
		//}
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class DueTimeCondition : INotifyPropertyChanged
{
	public string Text
	{
		get => _Text;
		set
		{
			_Text = value;
			OnPropertyChanged();
		}
	}

	public SolidColorBrush Foreground
	{
		get => _Foreground;
		set
		{
			_Foreground = value;
			OnPropertyChanged();
		}
	}

	public SolidColorBrush Background
	{
		get => _Background;
		set
		{
			_Background = value;
			OnPropertyChanged();
		}
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="s">
	/// </param>
	/// <param
	///     name="dueTime">
	/// </param>
	public DueTimeCondition( Protocol.Data.ServiceLevel s, DateTimeOffset? dueTime )
	{
		dueTime ??= DateTimeOffset.MinValue;

		//Logging.WriteLogLine( "ServiceLevel: " + s.NewName + ", dueTime: " + dueTime );
		_ServiceLevel = s;
		_DueTime      = (DateTimeOffset)dueTime;

		BuildCondition();
	}

	private SolidColorBrush _Background = Brushes.Transparent;

	private DateTimeOffset _DueTime;

	private SolidColorBrush _Foreground = null!;

	private readonly Protocol.Data.ServiceLevel _ServiceLevel;

	private string _Text = null!;

	/// <summary>
	///     From https://idsservices.atlassian.net/browse/I2P-11 and https://idsservices.atlassian.net/browse/I2P-12
	///     a. Looking at the Service Level Screen in the Rate Wizard you will see three fields labelled
	///     ”Warn”,”Critical”,”Beyond”.
	///     b.These fields are numerical fields and will contain a number equal to minutes.They can contain a negative
	///     number(Im thinking
	///     you will need a negative number to handle “Beyond” being late)
	///     c.The number of minutes entered into these fields will be compared to the “Ready By” field and the current
	///     time/date.
	///     d.If the current time difference between  “Ready By” is less than “Warn” but sooner than “Critical”, then in the
	///     “Condition” field
	///     for that shipment will display the word “WARN” in a dark yellow colour.
	///     e.If the current time difference between “Ready By” is less than “Critical” but still sooner than “Ready By” , then
	///     in the “Condition”
	///     field for that shipment display the word “CRITICAL” in a dark orange.
	///     f.If the current time is past the “Ready By” time but still before “Beyond” , then in the “Condition” field for
	///     that shipment,
	///     display the word “LATE” in a dark red.
	///     g.If the current time has passed “Beyond” , then in the “Condition” field for that shipment, display the word
	///     “BEYOND” in a dark yellow red.
	/// </summary>
	public void BuildCondition()
	{
		var WarningMinutes  = -1;
		var CriticalMinutes = -1;
		var BeyondMinutes   = -1;

		if( _ServiceLevel.WarningHours.Hours == 0 )
			WarningMinutes = (int)_ServiceLevel.WarningHours.TotalMinutes;

		if( _ServiceLevel.CriticalHours.Hours == 0 )
			CriticalMinutes = (int)_ServiceLevel.CriticalHours.TotalMinutes;

		if( _ServiceLevel.BeyondHours.Hours == 0 )
			BeyondMinutes = (int)_ServiceLevel.BeyondHours.TotalMinutes;

		if( ( WarningMinutes > -1 ) || ( CriticalMinutes > -1 ) || ( BeyondMinutes > -1 ) )
		{
			var Now = DateTimeOffset.Now;

			//Logging.WriteLogLine( "now: " + now + " - ReadyTime: " + _DueTime );

			//int diff = (int)(_ReadyTime - now).TotalMinutes;
			//               Logging.WriteLogLine("Time difference in minutes: " + diff);
			var Set = false;

			if( ( WarningMinutes > -1 ) && ( CriticalMinutes > -1 ) )
			{
				if( ( Now >= _DueTime.AddMinutes( -WarningMinutes ) ) && ( Now < _DueTime.AddMinutes( -CriticalMinutes ) ) )
				{
					Set = true;

					//Logging.WriteLogLine("Setting to WARN");
					Text       = "WARN";
					Foreground = Brushes.DarkGoldenrod;
					Background = Brushes.White; // Ensure that the foreground isn't masked by a service level background colour
				}
			}

			if( !Set && ( CriticalMinutes > -1 ) && ( BeyondMinutes > -1 ) )
			{
				//if( ( now >= _DueTime.AddMinutes( -criticalMinutes ) ) && ( now <= _DueTime.AddMinutes( -beyondMinutes ) ) )
				if( ( Now >= _DueTime.AddMinutes( -CriticalMinutes ) ) && ( Now < _DueTime ) )
				{
					Set = true;

					//Logging.WriteLogLine("Setting to CRITICAL");
					Text       = "CRITICAL";
					Foreground = Brushes.DarkOrange;
					Background = Brushes.White;
				}
			}

			if( !Set && ( BeyondMinutes > -1 ) )
			{
				if( Now >= _DueTime )
				{
					if( Now < _DueTime.AddMinutes( BeyondMinutes ) )
					{
						//Logging.WriteLogLine("Setting to LATE");
						Text       = "LATE";
						Foreground = Brushes.DarkRed;
						Background = Brushes.White;
					}
					else if( Now >= _DueTime.AddMinutes( BeyondMinutes ) )
					{
						//Logging.WriteLogLine("Setting to BEYOND");
						Text = "BEYOND";

						//var brush = (SolidColorBrush)new BrushConverter().ConvertFrom("#fa5a05");
						//Foreground = brush;
						Foreground = Brushes.Red;
						Background = Brushes.White;
					}
				}
			}

			//if( set )
			//	Logging.WriteLogLine( "Condition was set to " + Text );
		}
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class DisplayPackage : TripPackage, INotifyPropertyChanged
{
	public bool DetailsVisible
	{
		get => _DetailsVisible;
		set
		{
			_DetailsVisible = value;
			OnPropertyChanged();
		}
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public DisplayPackage( TripPackage p ) : base( p )
	{
	}

	private bool _DetailsVisible;

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class DisplayTrip : Trip, INotifyPropertyChanged
{
	// Must match Protocol Data STATUS
	public enum STATUS : sbyte
	{
		UNSET,
		NEW,
		ACTIVE,
		DISPATCHED,
		PICKED_UP,
		DELIVERED,
		VERIFIED,
		POSTED,
		INVOICED,
		SCHEDULED,
		PAID,
		DELETED,
		LIMBO,

		UNKNOWN,

		UNPICKUPABLE,
		UNDELIVERABLE,
		UNDELIVERABLE_NEW
	}

	public new ObservableCollection<DisplayPackage> Packages
	{
		get
		{
			if( _Packages is null )
			{
				var Temp = new ObservableCollection<DisplayPackage>();

				foreach( var Package in base.Packages )
					Temp.Add( new DisplayPackage( Package ) );

				_Packages = Temp;
			}

			return _Packages;
		}

		set
		{
			_Packages = value;
			OnPropertyChanged();
		}
	}

	public bool IsVisible
	{
		get => _IsVisible;
		set
		{
			_IsVisible = value;
			OnPropertyChanged();
		}
	}

	[JsonIgnore]
	public string Filter
	{
		get => _Filter;
		set
		{
			_Filter = value;

			bool StringContainsFilter( string v )
			{
				return v.Contains( value, StringComparison.OrdinalIgnoreCase );
			}

			bool DateContainsFilter( DateTimeOffset v )
			{
				var Str = v.ToString( "MMM d h:mm tt" );

				return StringContainsFilter( Str );
			}

			bool DecimalContainsFilter( decimal v )
			{
				var Str = v.ToString( CultureInfo.InvariantCulture );

				return StringContainsFilter( Str );
			}

			IsVisible = value.IsNullOrEmpty()
						|| StringContainsFilter( AccountId )
						|| StringContainsFilter( Driver )
						|| StringContainsFilter( TripId )
						|| StringContainsFilter( BillingCompanyName )
						|| StringContainsFilter( BillingNotes )
						|| StringContainsFilter( PickupZone )
						|| StringContainsFilter( PickupCompanyName )
						|| StringContainsFilter( PickupAddressSuite )
						|| StringContainsFilter( PickupAddressAddressLine1 )
						|| StringContainsFilter( PickupAddressCity )
						|| StringContainsFilter( PickupAddressRegion )
						|| StringContainsFilter( PickupAddressCountry )
						|| StringContainsFilter( PickupAddressPostalCode )
						|| StringContainsFilter( PickupNotes )
						|| StringContainsFilter( PickupAddressNotes )
						|| StringContainsFilter( DeliveryZone )
						|| StringContainsFilter( DeliveryCompanyName )
						|| StringContainsFilter( DeliveryAddressSuite )
						|| StringContainsFilter( DeliveryAddressAddressLine1 )
						|| StringContainsFilter( DeliveryAddressCity )
						|| StringContainsFilter( DeliveryAddressRegion )
						|| StringContainsFilter( DeliveryAddressCountry )
						|| StringContainsFilter( DeliveryAddressPostalCode )
						|| StringContainsFilter( DeliveryNotes )
						|| StringContainsFilter( DeliveryAddressNotes )
						|| StringContainsFilter( ServiceLevel )
						|| StringContainsFilter( PackageType )
						|| StringContainsFilter( Board )
						|| DateContainsFilter( CallTime ?? DateTimeOffset.MinValue )
						|| DateContainsFilter( ReadyTime ?? DateTimeOffset.MinValue )
						|| DateContainsFilter( DueTime ?? DateTimeOffset.MinValue )
						|| DecimalContainsFilter( Pieces )
						|| DecimalContainsFilter( Weight )
				;
		}
	}

	[JsonIgnore]
	public bool Selected
	{
		get => _Selected;
		set
		{
			if( value != _Selected )
			{
				_Selected = value;

				OnPropertyChanged( nameof( Foreground ) );
				OnPropertyChanged( nameof( Background ) );
			}
		}
	}

	public new string Driver
	{
		get => base.Driver;
		set
		{
			if( base.Driver != value )
			{
				base.Driver = value;
				OnPropertyChanged();
			}
		}
	}

	[JsonIgnore]
	public bool EnableAccept
	{
		get => _EnableAccept;
		set
		{
			if( value != _EnableAccept )
			{
				_EnableAccept = value;
				OnPropertyChanged();
			}
		}
	}

	[JsonIgnore]
	public ReadyTimeCondition ReadyTimeCondition => new( _ServiceLevel, ReadyTime );

	[JsonIgnore]
	public DueTimeCondition DueTimeCondition => new( _ServiceLevel, DueTime );

	[JsonIgnore]
	public STATUS Status
	{
		get => ToDisplayTripStatus( Status1, Status2 );

		set
		{
			Protocol.Data.STATUS NewStatus;
			var                  NewStatus1 = STATUS1.UNSET;

			switch( value )
			{
			case STATUS.UNDELIVERABLE:
				NewStatus  = Protocol.Data.STATUS.PICKED_UP;
				NewStatus1 = STATUS1.UNDELIVERED;

				break;

			case STATUS.UNPICKUPABLE:
				NewStatus  = Protocol.Data.STATUS.DISPATCHED;
				NewStatus1 = STATUS1.UNDELIVERED;

				break;

			case STATUS.UNDELIVERABLE_NEW:
				NewStatus  = Protocol.Data.STATUS.NEW;
				NewStatus1 = STATUS1.UNDELIVERED;

				break;

			default:
				NewStatus = (Protocol.Data.STATUS)value;

				break;
			}

			if( ( Status1 != NewStatus ) || ( Status2 != NewStatus1 ) )
			{
				Status1 = NewStatus;
				Status2 = NewStatus1;

				OnPropertyChanged( nameof( ReceivedByDevice ) );
				OnPropertyChanged( nameof( NotReceivedByDevice ) );
				OnPropertyChanged( nameof( ReadByDriver ) );
				OnPropertyChanged( nameof( PickedUp ) );
				OnPropertyChanged( nameof( Delivered ) );
				OnPropertyChanged();
			}
		}
	}

	[JsonIgnore]
	public bool PickedUpOrDelivered => PickedUp || Delivered;

	[JsonIgnore]
	public bool HasPackages => Packages.Count > 0;

	[JsonIgnore]
	public bool Undeliverable => Status2 == STATUS1.UNDELIVERED;


	[JsonIgnore]
	public bool DetailsVisible
	{
		get => _DetailsVisible;
		set
		{
			_DetailsVisible = value;
			OnPropertyChanged();
		}
	}

	[JsonIgnore]
	public SolidColorBrush Foreground => ( _Selected ? SelectedForeground : UnSelectedForeground )!;

	[JsonIgnore]
	public SolidColorBrush Background => ( _Selected ? SelectedBackground : UnSelectedBackground )!;

	[JsonIgnore]
	public string FormattedPickupAddress
	{
		get
		{
			if( _FormattedPickupAddress.IsNullOrEmpty() )
			{
				_FormattedPickupAddress = FormatAddress( PickupCompanyName, PickupAddressSuite, PickupAddressAddressLine1, PickupAddressAddressLine2, PickupAddressVicinity,
													     PickupAddressCity, PickupAddressRegion, BillingAddressPostalCode );
			}

			return _FormattedPickupAddress;
		}
	}

	[JsonIgnore]
	public string FormattedDeliveryAddress
	{
		get
		{
			if( _FormattedDeliveryAddress.IsNullOrEmpty() )
			{
				_FormattedDeliveryAddress = FormatAddress( DeliveryCompanyName, DeliveryAddressSuite, DeliveryAddressAddressLine1, DeliveryAddressAddressLine2, DeliveryAddressVicinity,
														   DeliveryAddressCity, DeliveryAddressRegion, BillingAddressPostalCode );
			}

			return _FormattedDeliveryAddress;
		}
	}


	[JsonIgnore]
	public SolidColorBrush UnSelectedBackground { get; set; }

	[JsonIgnore]
	public SolidColorBrush UnSelectedForeground { get; set; }

	// Used for DispatchBoard
	[JsonIgnore]
	public string DisplayAccountNotes { get; set; } = "";

	[JsonIgnore]
	public string DisplayPickupAddressNotes { get; set; } = "";

	[JsonIgnore]
	public string DisplayPickupNotes { get; set; } = "";

	[JsonIgnore]
	public string DisplayDeliveryAddressNotes { get; set; } = "";

	[JsonIgnore]
	public string DisplayDeliveryNotes { get; set; } = "";

	private string _FormattedPickupAddress { get; set; } = null!;


	private string _FormattedDeliveryAddress { get; set; } = null!;

	public DisplayTrip()
	{
		UnSelectedBackground = new SolidColorBrush( Colors.White );
		UnSelectedForeground = new SolidColorBrush( Colors.Black );
	}

	public DisplayTrip( Trip t, ServiceLevel s ) : base( t )
	{
		Status        = ToDisplayTripStatus( Status1, Status2 );
		_ServiceLevel = s.RawServiceLevel;

		if( ( t.Status3 & ( STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE ) ) != 0 )
		{
			UnSelectedBackground = StatusToSolidBrushConvertor.Undeliverable;
			UnSelectedForeground = Brushes.Black;
		}
		else
		{
			UnSelectedForeground = s.Foreground;
			UnSelectedBackground = s.Background;
		}
	}

	private bool _DetailsVisible;


	private bool _EnableAccept;

	private string _Filter = null!;

	private bool _IsVisible = true;

	private ObservableCollection<DisplayPackage>? _Packages;


	private bool _Selected;

	public                 Protocol.Data.ServiceLevel _ServiceLevel = new();
	public static readonly DisplayTrip                Empty         = new DetailedTrip();

	internal static SolidColorBrush? SelectedBackground,
									 SelectedForeground;


	private static string FormatAddress( string companyName, string suite, string addr1, string addr2, string vicinity, string city, string state, string postCode )
	{
		var Address = new StringBuilder();

		void AddLine( string text )
		{
			text = text.Trim();

			if( text.IsNotNullOrWhiteSpace() )
				Address.Append( text ).Append( "\r\n" );
		}

		void Add( string text, string extra = "" )
		{
			if( text.IsNotNullOrWhiteSpace() )
				Address.Append( text.Trim() ).Append( extra );
		}

		AddLine( companyName );

		Add( suite, "/" );
		AddLine( addr1 );
		AddLine( addr2 );
		AddLine( vicinity );
		Add( city, " " );
		Add( state, " " );
		Add( postCode, " " );

		return Address.ToString().Trim();
	}


	public static STATUS ToDisplayTripStatus( Protocol.Data.STATUS status, STATUS1 status1 )
	{
		return status switch
			   {
				   Protocol.Data.STATUS.NEW when status1 == STATUS1.UNDELIVERED        => STATUS.UNDELIVERABLE_NEW,
				   Protocol.Data.STATUS.DISPATCHED when status1 == STATUS1.UNDELIVERED => STATUS.UNPICKUPABLE,
				   Protocol.Data.STATUS.PICKED_UP when status1 == STATUS1.UNDELIVERED  => STATUS.UNDELIVERABLE,

				   Protocol.Data.STATUS.PICKED_UP => STATUS.PICKED_UP,
				   Protocol.Data.STATUS.DELIVERED => STATUS.DELIVERED,
				   Protocol.Data.STATUS.NEW       => STATUS.NEW,

				   _ => (STATUS)status
			   };
	}

	[NotifyPropertyChangedInvocator]
	public virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public event PropertyChangedEventHandler? PropertyChanged;

#region Icon Control
	[JsonIgnore]
	public bool NotReceivedByDevice
	{
		get => !DoNotSendToDriver && !base.ReceivedByDevice && !base.ReadByDriver && !PickedUpOrDelivered;
		set => throw new NotImplementedException();
	}

	[JsonIgnore]
	public new bool ReceivedByDevice
	{
		get => !DoNotSendToDriver && base.ReceivedByDevice && !base.ReadByDriver && !PickedUpOrDelivered;
		set
		{
			if( base.ReceivedByDevice != value )
			{
				base.ReceivedByDevice = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( NotReceivedByDevice ) );
			}
		}
	}

	public new bool ReadByDriver
	{
		get => !DoNotSendToDriver && base.ReadByDriver && !PickedUpOrDelivered;
		set
		{
			if( base.ReadByDriver != value )
			{
				base.ReadByDriver = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( ReceivedByDevice ) );
				OnPropertyChanged( nameof( NotReceivedByDevice ) );
			}
		}
	}

	[JsonIgnore]
	public bool PickedUp => !DoNotSendToDriver && ( Status1 == Protocol.Data.STATUS.PICKED_UP );

	[JsonIgnore]
	public bool Delivered => !DoNotSendToDriver && ( Status1 == Protocol.Data.STATUS.DELIVERED );
#endregion
}