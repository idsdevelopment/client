﻿#nullable enable

using System.Windows.Input;
using System.Windows.Threading;
using IdsControlLibraryV2.TabControl;
using ViewModels.BaseViewModels;
using ViewModels.RouteShipments;
using ViewModels.Trips.Boards.Common;
using File = System.IO.File;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

// ReSharper disable MemberCanBePrivate.Global

namespace ViewModels.Trips.Boards;

public class DispatchViewModel : AMessagingViewModel
{
	private const char IN_CONDITION_LIST_DELIMITER = ',';

	public const string BASE_BOARD_NAME = "DispatchBoard_";

	~DispatchViewModel()
	{
		Logging.WriteLogLine("Running destructor...");
		if (IsUpdateConditionsRunning())
		{
			StopUpdateConditions();
		}
	}

	//public void OnDispose()
	//{
	//	Logging.WriteLogLine($"Disposing of {BoardName}");
	//}

	/// <summary>
	///     Represents 1 filter for a dispatch board.
	/// </summary>
	public class DispatchFilter
	{
		public string? ColumnName { get; private set; }
		public string? Condition { get; private set; }
		public string? Value { get; private set; }
		public bool Active { get; private set; }
		private string? WindowName { get; set; }

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			var Converted = WindowName + "|" + ColumnName + "|" + Condition + "|" + Value + "|" + Active;
			return Converted;
		}

		/// <summary>
		/// </summary>
		/// <param
		///     name="windowName">
		/// </param>
		/// <param
		///     name="columnName">
		/// </param>
		/// <param
		///     name="condition">
		/// </param>
		/// <param
		///     name="value">
		/// </param>
		/// <param
		///     name="active">
		/// </param>
		public DispatchFilter(string windowName, string columnName, string condition, string value, bool active)
		{
			WindowName = windowName;
			ColumnName = columnName;
			Condition = condition;
			Value = value;
			Active = active;
		}

		/// <summary>
		/// </summary>
		/// <param
		///     name="toConvert">
		/// </param>
		public DispatchFilter(string toConvert)
		{
			FromString(toConvert);
		}

		/// <summary>
		/// </summary>
		/// <param
		///     name="toConvert">
		/// </param>
		private void FromString(string toConvert)
		{
			var Pieces = toConvert.Split('|');

			if (Pieces.Length == 5)
			{
				WindowName = Pieces[0];
				ColumnName = Pieces[1];
				Condition = Pieces[2];
				Value = Pieces[3];
				Active = bool.Parse(Pieces[4]);
			}
			else
				Logging.WriteLogLine("Invalid DispatchFilter: " + toConvert);
		}
	}

	// BoardName, Settings for board
	//public static readonly Dictionary<string, Settings> DispatchBoardSettings = new Dictionary<string, Settings>();

	public DispatchBoard? View { get; set; }

	public string BoardName
	{
		get { return Get(() => BoardName, ""); }
		set { Set(() => BoardName, value); }
	}

	public string BoardNumber
	{
		get { return Get(() => BoardNumber, ""); }
		set { Set(() => BoardNumber, value); }
	}


	public string FormattedBoardName
	{
		get { return Get(() => FormattedBoardName, ""); }
		set { Set(() => FormattedBoardName, value); }
	}


	public string HelpUri
	{
		get { return Get(() => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638451729/How+to+Dispatch+Trips"); }
	}


	public bool DataGridVisible
	{
		get { return Get(() => DataGridVisible, IsInDesignMode); }
		set { Set(() => DataGridVisible, value); }
	}

	public DisplayTrip? SelectedTrip
	{
		get { return Get(() => SelectedTrip, new DisplayTrip()); }
		set { Set(() => SelectedTrip, value); }
	}

	public DisplayTrip? PreviouslySelectedTrip
	{
		get { return Get(() => PreviouslySelectedTrip, SelectedTrip); }
		set { Set(() => PreviouslySelectedTrip, value); }
	}


	public List<DisplayTrip>? SelectedTrips
	{
		get { return Get(() => SelectedTrips, new List<DisplayTrip>()); }
		set { Set(() => SelectedTrips, value); }
	}


	public ObservableCollection<Driver> Drivers
	{
		get { return Get(() => Drivers, new ObservableCollection<Driver>()); }
		set { Set(() => Drivers, value); }
	}


	public bool EnableAssignDriver
	{
		get { return Get(() => EnableAssignDriver, false); }
		set { Set(() => EnableAssignDriver, value); }
	}


	public bool EnableDelete
	{
		get { return Get(() => EnableDelete, false); }
		set { Set(() => EnableDelete, value); }
	}

	public bool EnableRouteShipments
	{
		get { return Get(() => EnableRouteShipments, false); }
		set { Set(() => EnableRouteShipments, value); }
	}

	public string DisplayingTripsForBoardMessage
	{
		get { return Get(() => DisplayingTripsForBoardMessage, string.Empty); }
		set { Set(() => DisplayingTripsForBoardMessage, value); }
	}

	private static readonly string PROGRAM = "Dispatch Board (" + Globals.CurrentVersion.APP_VERSION + ")";

	private static readonly List<string> DispatchBoardNames = new();

	private static readonly List<string> AllDispatchColumnsInOrder = new()
																	 {
																		 "DialoguesDispatchColumnDriver",
																		 "DialoguesDispatchColumnTripId",
																		 "DialoguesDispatchColumnAcct",
																		 "DialoguesDispatchColumnCompanyName",
																		 "DialoguesDispatchColumnAcctNotes",
																		 "DialoguesDispatchColumnCondition",
																		 "DialoguesDispatchColumnCallDate",
																		 "DialoguesDispatchColumnCZ",
																		 "DialoguesDispatchColumnPickup",
																		 "DialoguesDispatchColumnPickupSuite",

																		 "DialoguesDispatchColumnPickupStreet",
																		 "DialoguesDispatchColumnPickupCity",
																		 "DialoguesDispatchColumnPickupProvState",
																		 "DialoguesDispatchColumnPickupCountry",
																		 "DialoguesDispatchColumnPickupPostalZip",
																		 "DialoguesDispatchColumnPickupAddressNotes",
																		 "DialoguesDispatchColumnPickupNotes",
																		 "DialoguesDispatchColumnPZ",
																		 "DialoguesDispatchColumnDelivery",
																		 "DialoguesDispatchColumnDeliverySuite",

																		 "DialoguesDispatchColumnDeliveryStreet",
																		 "DialoguesDispatchColumnDeliveryCity",
																		 "DialoguesDispatchColumnDeliveryProvState",
																		 "DialoguesDispatchColumnDeliveryCountry",
																		 "DialoguesDispatchColumnDeliveryPostalZip",
																		 "DialoguesDispatchColumnDeliveryAddressNotes",
																		 "DialoguesDispatchColumnDeliveryNotes",
																		 "DialoguesDispatchColumnDZ",
																		 "DialoguesDispatchColumnServ",
																		 "DialoguesDispatchColumnPkg",

																		 "DialoguesDispatchColumnReady",
																		 "DialoguesDispatchColumnDue",
																		 "DialoguesDispatchColumnPcs",
																		 "DialoguesDispatchColumnWt",

																		 "DialoguesDispatchColumnBoard"

		                                                                 //"DialoguesDispatchColumnPlts"					
	                                                                 };

	private static readonly Dictionary<string, string> DictionaryColumns = new()
																		   {
																			   {"DialoguesDispatchColumnDriver", "driver"},
																			   {"DialoguesDispatchColumnTripId", "tripId"},
																			   {"DialoguesDispatchColumnAcct", "accountId"},
																			   {"DialoguesDispatchColumnCompanyName", "BillingCompanyName"},
																			   {"DialoguesDispatchColumnAcctNotes", "BillingNotes"},
																			   {"DialoguesDispatchColumnCondition", "ReadyTimeCondition"},
																			   {"DialoguesDispatchColumnCallDate", "CallTime"},
																			   {"DialoguesDispatchColumnCZ", "CurrentZone"},

																			   {"DialoguesDispatchColumnPickup", "PickupCompanyName"},
																			   {"DialoguesDispatchColumnPickupSuite", "PickupAddressSuite"},
																			   {"DialoguesDispatchColumnPickupStreet", "PickupAddressAddressLine1"},
																			   {"DialoguesDispatchColumnPickupCity", "PickupAddressCity"},
																			   {"DialoguesDispatchColumnPickupProvState", "PickupAddressRegion"},
																			   {"DialoguesDispatchColumnPickupCountry", "PickupAddressCountry"},
																			   {"DialoguesDispatchColumnPickupPostalZip", "PickupAddressPostalCode"},
																			   {"DialoguesDispatchColumnPickupAddressNotes", "PickupAddressNotes"},
																			   {"DialoguesDispatchColumnPickupNotes", "PickupNotes"},
																			   {"DialoguesDispatchColumnPZ", "PickupZone"},

																			   {"DialoguesDispatchColumnDelivery", "DeliveryCompanyName"},
																			   {"DialoguesDispatchColumnDeliverySuite", "DeliveryAddressSuite"},
																			   {"DialoguesDispatchColumnDeliveryStreet", "DeliveryAddressAddressLine1"},
																			   {"DialoguesDispatchColumnDeliveryCity", "DeliveryAddressCity"},
																			   {"DialoguesDispatchColumnDeliveryProvState", "DeliveryAddressRegion"},
																			   {"DialoguesDispatchColumnDeliveryCountry", "DeliveryAddressCountry"},
																			   {"DialoguesDispatchColumnDeliveryPostalZip", "DeliveryAddressPostalCode"},
																			   {"DialoguesDispatchColumnDeliveryAddressNotes", "DeliveryAddressNotes"},
																			   {"DialoguesDispatchColumnDeliveryNotes", "DeliveryNotes"},
																			   {"DialoguesDispatchColumnDZ", "DeliveryZone"},

																			   {"DialoguesDispatchColumnServ", "ServiceLevel"},
																			   {"DialoguesDispatchColumnPkg", "PackageType"},
																			   {"DialoguesDispatchColumnReady", "ReadyTime"},
																			   {"DialoguesDispatchColumnDue", "DueTime"},
																			   {"DialoguesDispatchColumnPcs", "Pieces"},
																			   {"DialoguesDispatchColumnWt", "Weight"},

																			   {"DialoguesDispatchColumnBoard", "Board"}

		                                                                       //{ "DialoguesDispatchColumnPlts", "pallets" },
	                                                                       };

	private static readonly string EQUALS = FindStringResource("DialoguesDispatchConditionsEquals");
	private static readonly string NOT_EQUAL_TO = FindStringResource("DialoguesDispatchConditionsNotEqualTo");
	private static readonly string IN = FindStringResource("DialoguesDispatchConditionsIn");
	private static readonly string CONTAINS = FindStringResource("DialoguesDispatchConditionsContains");
	private static readonly string STARTS_WITH = FindStringResource("DialoguesDispatchConditionsStartsWith");
	private static readonly string ENDS_WITH = FindStringResource("DialoguesDispatchConditionsEndsWith");
	private static readonly List<string> AllConditions = new() { "=", "!=", ">", "<" };

	private static readonly List<string> StringConditions = new()
															{
																EQUALS,
																NOT_EQUAL_TO,
																IN,
																CONTAINS,
																STARTS_WITH,
																ENDS_WITH
															};

	private static Settings? DispatchBoardSettings { get; set; }


	//public DispatchViewModel() : base( Messaging.DISPATCH_BOARD, new[] { STATUS.ACTIVE, STATUS.DELETED, STATUS.DISPATCHED, STATUS.FINALISED } )
	//{
	//	//string name = BASE_BOARD_NAME + DispatchBoardNames.Count;
	//	var Name = BASE_BOARD_NAME + "0";
	//	//string Name = BASE_BOARD_NAME + "1";

	//	List<string> Names = new();
	//	var          Tis   = Globals.DataContext.MainDataContext.ProgramTabItems;

	//	if( Tis.Count > 0 )
	//	{
	//		foreach( var Ti in Tis )
	//		{
	//			if( Ti.Content is DispatchBoard Pti )
	//			{
	//				var Tmp = ( (DispatchViewModel)Pti.DataContext ).BoardName;
	//				Logging.WriteLogLine( "Found board: " + Tmp );
	//				Names.Add( Tmp );
	//			}
	//		}
	//	}

	//	// Now, check to see if there is a hole	
	//	if( Names.Count > 0 )
	//	{
	//		for( var I = 0; I < 100; I++ )
	//		{
	//			var Tmp = BASE_BOARD_NAME + I;
	//			//string Tmp = BASE_BOARD_NAME + (I + 1);

	//			if ( !Names.Contains( Tmp ) )
	//			{
	//				// Found a hole
	//				Logging.WriteLogLine( "Reusing boardname: " + Tmp );
	//				Name = Tmp;
	//				break;
	//			}
	//		}
	//	}

	//	if( !DispatchBoardNames.Contains( Name ) )
	//		DispatchBoardNames.Add( Name );
	//	BoardName = Name;
	//	Logging.WriteLogLine( "Opening " + Name );

	//	LoadSettings();
	//}

	private readonly Dictionary<string, List<DispatchFilter>> DictionaryFilters = new();

	//private static List<DispatchViewModel.DispatchFilter>? filters = null;

	private readonly Dictionary<string, List<string>> DictionaryConditionsForField = new()
																					 {
																						 {"DialoguesDispatchColumnDriver", StringConditions},
																						 {"DialoguesDispatchColumnTripId", StringConditions},
																						 {"DialoguesDispatchColumnAcct", StringConditions},
																						 {"DialoguesDispatchColumnCompanyName", StringConditions}, // New
		                                                                                 {"DialoguesDispatchColumnAcctNotes", StringConditions},   // New
		                                                                                 {"DialoguesDispatchColumnCZ", StringConditions},          // New

		                                                                                 {"DialoguesDispatchColumnPickup", StringConditions},
																						 {"DialoguesDispatchColumnPickupSuite", StringConditions}, // New
		                                                                                 {"DialoguesDispatchColumnPickupStreet", StringConditions},
																						 {"DialoguesDispatchColumnPickupCity", StringConditions},
																						 {"DialoguesDispatchColumnPickupProvState", StringConditions},    // New
		                                                                                 {"DialoguesDispatchColumnPickupCountry", StringConditions},      // New
		                                                                                 {"DialoguesDispatchColumnPickupPostalZip", StringConditions},    // New
		                                                                                 {"DialoguesDispatchColumnPickupAddressNotes", StringConditions}, // New
		                                                                                 {"DialoguesDispatchColumnPickupNotes", StringConditions},        // New
		                                                                                 {"DialoguesDispatchColumnPZ", StringConditions},

																						 {"DialoguesDispatchColumnDelivery", StringConditions},
																						 {"DialoguesDispatchColumnDeliverySuite", StringConditions}, // New
		                                                                                 {"DialoguesDispatchColumnDeliveryStreet", StringConditions},
																						 {"DialoguesDispatchColumnDeliveryCity", StringConditions},
																						 {"DialoguesDispatchColumnDeliveryProvState", StringConditions},    // New
		                                                                                 {"DialoguesDispatchColumnDeliveryCountry", StringConditions},      // New
		                                                                                 {"DialoguesDispatchColumnDeliveryPostalZip", StringConditions},    // New
		                                                                                 {"DialoguesDispatchColumnDeliveryAddressNotes", StringConditions}, // New
		                                                                                 {"DialoguesDispatchColumnDeliveryNotes", StringConditions},        // New
		                                                                                 {"DialoguesDispatchColumnDZ", StringConditions},

																						 {"DialoguesDispatchColumnServ", StringConditions},
																						 {"DialoguesDispatchColumnPkg", StringConditions},
																						 {"DialoguesDispatchColumnReady", AllConditions},
																						 {"DialoguesDispatchColumnDue", AllConditions},
																						 {"DialoguesDispatchColumnPcs", AllConditions},
																						 {"DialoguesDispatchColumnWt", AllConditions}

		                                                                                 //{ "DialoguesDispatchColumnBoard", allConditions }

		                                                                                 //{ "DialoguesDispatchColumnPlts", allConditions },
	                                                                                 };


	private DisplayTrip? PreviousSelectedTrip;

	private readonly Dictionary<string, ServiceLevel> ServiceLevels = new();

	public readonly Dictionary<string, Visibility> DictionaryColumnVisibility = new()
																				{
																					{"DialoguesDispatchColumnDriver", Visibility.Visible},
																					{"DialoguesDispatchColumnTripId", Visibility.Hidden},
																					{"DialoguesDispatchColumnAcct", Visibility.Hidden},
																					{"DialoguesDispatchColumnCompanyName", Visibility.Hidden}, // New
		                                                                            {"DialoguesDispatchColumnAcctNotes", Visibility.Hidden},   // New
		                                                                            {"DialoguesDispatchColumnCondition", Visibility.Hidden},   // New
		                                                                            {"DialoguesDispatchColumnCallDate", Visibility.Hidden},    // New
		                                                                            {"DialoguesDispatchColumnCZ", Visibility.Hidden},          // New
		                                                                            {"DialoguesDispatchColumnPickup", Visibility.Hidden},
																					{"DialoguesDispatchColumnPickupSuite", Visibility.Hidden}, // New

		                                                                            {"DialoguesDispatchColumnPickupStreet", Visibility.Hidden},
																					{"DialoguesDispatchColumnPickupCity", Visibility.Hidden},
																					{"DialoguesDispatchColumnPickupProvState", Visibility.Hidden},    // New
		                                                                            {"DialoguesDispatchColumnPickupCountry", Visibility.Hidden},      // New
		                                                                            {"DialoguesDispatchColumnPickupPostalZip", Visibility.Hidden},    // New
		                                                                            {"DialoguesDispatchColumnPickupAddressNotes", Visibility.Hidden}, // New
		                                                                            {"DialoguesDispatchColumnPickupNotes", Visibility.Hidden},        // New
		                                                                            {"DialoguesDispatchColumnPZ", Visibility.Hidden},                 // New
		                                                                            {"DialoguesDispatchColumnDelivery", Visibility.Hidden},
																					{"DialoguesDispatchColumnDeliverySuite", Visibility.Hidden}, // New

		                                                                            {"DialoguesDispatchColumnDeliveryStreet", Visibility.Hidden},
																					{"DialoguesDispatchColumnDeliveryCity", Visibility.Hidden},
																					{"DialoguesDispatchColumnDeliveryProvState", Visibility.Hidden},    // New
		                                                                            {"DialoguesDispatchColumnDeliveryCountry", Visibility.Hidden},      // New
		                                                                            {"DialoguesDispatchColumnDeliveryPostalZip", Visibility.Hidden},    // New
		                                                                            {"DialoguesDispatchColumnDeliveryAddressNotes", Visibility.Hidden}, // New
		                                                                            {"DialoguesDispatchColumnDeliveryNotes", Visibility.Hidden},        // New
		                                                                            {"DialoguesDispatchColumnDZ", Visibility.Hidden},                   // New
		                                                                            {"DialoguesDispatchColumnServ", Visibility.Hidden},
																					{"DialoguesDispatchColumnPkg", Visibility.Hidden},

																					{"DialoguesDispatchColumnReady", Visibility.Hidden},
																					{"DialoguesDispatchColumnDue", Visibility.Hidden},
																					{"DialoguesDispatchColumnPcs", Visibility.Hidden},
																					{"DialoguesDispatchColumnWt", Visibility.Hidden},

																					{"DialoguesDispatchColumnBoard", Visibility.Hidden} // New
		                                                                            //{ "DialoguesDispatchColumnPlts", Visibility.Hidden } // New	
	                                                                            };

	public override void OnArgumentChange(object? boardNumber)
	{
		if (boardNumber is string Number)
		{
			Logging.WriteLogLine("DEBUG " + boardNumber + " BoardName: " + BoardName + " BoardNumber: " + BoardNumber + " OnlyShowTripsForThisBoard: " + OnlyShowTripsForThisBoard);
			BoardName = BASE_BOARD_NAME + boardNumber;
			BoardNumber = Number;
			FormatBoardName();
			//_ = DisplayingTripsForBoardMessage;
			LoadSettings();

			// Load the settings tab
			if (View is { } V)
			{
				Dispatcher.Invoke(() =>
				{
					SetWaitCursor();
					V.PopulateSettingsColumnList();
					V.PopulateFilterList();
					DispatchBoard.PopulateOtherSettings();

					V.BtnApplyFilters_Click(null, null);
					V.BtnSaveColumns_Click(null, null);
					V.BtnSaveOtherSettings_Click(null, null);

					UpdateConditions_Tick(null, null);
					ClearWaitCursor();
				});
			}

		}
	}

	public void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	public void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}

	[DependsUpon(nameof(EnableDelete))]
	public void WhenEnableDeleteChanges()
	{
		// TODO Remove when attached to properties
		EnableRouteShipments = EnableDelete;
	}

	[DependsUpon(nameof(SelectedTrips))]
    public void WhenSelectedTripsChange()
    {
        EnableDelete = SelectedTrips is { Count: > 0 };

        var Trps = SelectedTrips;

        if (Trps?.Count == 1)
        {
            //if (SelectedTrip != null && SelectedTrip.TripId.IsNotNullOrWhiteSpace())
            if ((Trps[0] != null) && Trps[0].TripId.IsNotNullOrWhiteSpace())
            {
				//Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + SelectedTrip.TripId);
				//PreviouslySelectedTrip = SelectedTrip;
				if (PreviouslySelectedTrip != null)
				{
					PreviouslySelectedTrip.Selected = false;
				}
                Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + Trps[0].TripId);
                PreviouslySelectedTrip = Trps[0];
            }
            else
                Logging.WriteLogLine("DEBUG Leaving PreviouslySelectedTrip at " + PreviouslySelectedTrip?.TripId);

            SelectedTrip = Trps[0];

            //return;
        }
        else
		{
			// Make sure that only the newly selected trips are selected
            SelectedTrip = null;
			if (Trps != null && Trps.Count > 0)
			{
				List<DisplayTrip> selected = (from T in Trips where T.Selected select T).ToList();
				if (selected.Count > 0)
				{
					foreach (DisplayTrip dt in selected)
					{
						string id = dt.TripId;
					
						DisplayTrip inSelected = (from S in Trps where S.TripId == id select S).FirstOrDefault();
						dt.Selected = inSelected != null;
                    }
				}
			}
		}
    }
    public void WhenSelectedTripsChange_V1()
	{
		EnableDelete = SelectedTrips is { Count: > 0 };

		var Trps = SelectedTrips;

		if (Trps?.Count == 1)
		{
			//if (SelectedTrip != null && SelectedTrip.TripId.IsNotNullOrWhiteSpace())
			if ((Trps[0] != null) && Trps[0].TripId.IsNotNullOrWhiteSpace())
			{
				//Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + SelectedTrip.TripId);
				//PreviouslySelectedTrip = SelectedTrip;
				Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + Trps[0].TripId);
				PreviouslySelectedTrip = Trps[0];
			}
			else
				Logging.WriteLogLine("DEBUG Leaving PreviouslySelectedTrip at " + PreviouslySelectedTrip?.TripId);

			SelectedTrip = Trps[0];

			//return;
		}
		else
			SelectedTrip = null;
	}

	public async Task Execute_DeleteTrips()
	{
		await UpdateStatusAndRemove(SelectedTrips, STATUS.DELETED);
		SelectedTrip = new DisplayTrip();
		SelectedTrips = null;
	}

	public void Execute_RouteShipments()
	{
		if (SelectedTrips is { Count: > 0 })
		{
			var TabNumber = 0;
			var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;
			PageTabItem? Pti = null;

			if (Tis.Count > 0)
			{
				for (TabNumber = 0; TabNumber < Tis.Count; TabNumber++)
				{
					var Ti = Tis[TabNumber];

					if (Ti.Content is RouteShipments.RouteShipments)
					{
						Pti = Ti;
						break;
					}
				}
			}

			if (Pti == null)
			{
				Logging.WriteLogLine("Opening new " + Globals.ROUTE_SHIPMENTS + " tab and loading " + SelectedTrips.Count + " trips");
				Globals.RunProgram(Globals.ROUTE_SHIPMENTS, SelectedTrips);

				Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

				for (TabNumber = 0; TabNumber < Tis.Count; TabNumber++)
				{
					var Ti = Tis[TabNumber];

					if (Ti.Content is RouteShipments.RouteShipments)
					{
						Pti = Ti;
						break;
					}
				}
			}

			if (Pti != null)
			{
				Logging.WriteLogLine("Loading " + SelectedTrips.Count + " trips in " + Globals.ROUTE_SHIPMENTS);
				((RouteShipmentsModel)Pti.Content.DataContext).LoadTrips(SelectedTrips);
				////await ((RouteShipments.RouteShipmentsModel)pti.Content.DataContext).WhenTripsChange();
				((RouteShipmentsModel)Pti.Content.DataContext).OnArgumentChange(SelectedTrips);
			}

			Globals.DataContext.MainDataContext.TabControl.SelectedIndex = TabNumber;
		}
	}

    public void AssignToBoard_V1(string board)
    {
        if (SelectedTrips != null)
        {
            Logging.WriteLogLine("Assigning " + SelectedTrips.Count + " trip(s) to board " + board);

			List<DisplayTrip> tripList = (from T in Trips where T.Board != board && T.Selected select T).ToList();


            //foreach (var Dt in SelectedTrips)
            foreach (var Dt in tripList)
                Dt.Board = board;
            //SaveTrip(dt);
            SaveTrips(SelectedTrips);
            WhenTripsChange();
        }
    }
    public void AssignToBoard(string board)
	{
		if (SelectedTrips != null)
		{
			Logging.WriteLogLine("Assigning " + SelectedTrips.Count + " trip(s) to board " + board);

			foreach (var Dt in SelectedTrips)
				Dt.Board = board;
			//SaveTrip(dt);
			SaveTrips(SelectedTrips);
			WhenTripsChange();
		}
	}

	public void ClearBoard()
	{
		if (SelectedTrips is { Count: > 0 })
		{
			Logging.WriteLogLine("Clearing boards from " + SelectedTrips.Count + " trip(s)");

			foreach (var T in SelectedTrips)
				T.Board = string.Empty;
			SaveTrips(SelectedTrips);
			WhenTripsChange();
		}
	}

	[DependsUpon(nameof(SelectedTrip))]
	public void WhenSelectedTripChanges()
	{
		if (PreviousSelectedTrip is not null)
			PreviousSelectedTrip.Selected = false;

		PreviousSelectedTrip = SelectedTrip;
		DeselectAllTripsExceptCurrent(SelectedTrip);
		if (SelectedTrip is { } St) // Happens on tab change
		{
			St.Selected = true;
			var Drvr = SelectedTrip.Driver;

			var Ok = Drvr.IsNotNullOrWhiteSpace();

			if (Ok)
				Ok = Drivers.Any(driver => driver.StaffId == Drvr);

			St.EnableAccept = Ok;

			//EnableAssignDriver = true;
		}

		EnableAssignDriver = Trips.Any(displayTrip => displayTrip.EnableAccept);

		//EnableAssignDriver = true;
	}

	public void DeselectAllTripsExceptCurrent(DisplayTrip? keep)
	{
		if (keep != null)
		{
			foreach (DisplayTrip dt in Trips)
			{
				if (dt.TripId != keep.TripId)
				{
					dt.Selected = false;
				}
			}
		}
	}

	#region Dispatch Trips
	public ICommand DispatchTrips => Commands[nameof(DispatchTrips)];

	public void Execute_DispatchTrips()
	{
		Tasks.RunVoid(100, async () =>
							{
								if (SelectedTrips is { Count: > 0 } St)
								{
									var UpdateTrips = (from T in St
													   where T.EnableAccept && T.Driver.Trim() != ""
													   group T by new
													   {
														   T.Driver,
														   T.Status2,
														   T.Status3
													   }
														into G
													   select G).ToDictionary(g => g.Key, g => g.ToList());

									await Dispatcher.Invoke(async () =>
															 {
																 foreach (var T in UpdateTrips)
																 {
																	 var K = T.Key;
																	 await UpdateStatusAndRemove(T.Value, STATUS.DISPATCHED, K.Status2, K.Status3, K.Driver);
																 }

																 TripCount = Trips.Count;
															 });
								}
							});
	}
	#endregion

	protected override async void OnInitialised()
	{
		DisplayTrip.SelectedBackground = Globals.Dictionary.AsSolidColorBrush("DefaultSelectedBackgroundColourBrush");
		DisplayTrip.SelectedForeground = Globals.Dictionary.AsSolidColorBrush("DefaultSelectedForegroundColourBrush");

		base.OnInitialised();

		try
		{
			Trips.Clear();

			if (!IsInDesignMode)
			{
				SetWaitCursor();
				var STemp = await Azure.Client.RequestGetServiceLevelsDetailed();
				var Temp = await Azure.Client.RequestGetTripsByStatus(new StatusRequest { STATUS.ACTIVE });

				Logging.WriteLogLine("DEBUG BoardName: " + BoardName + " BoardNumber: " + BoardNumber + " OnlyShowTripsForThisBoard: " + OnlyShowTripsForThisBoard);

				//Drivers = new ObservableCollection<Driver>( from D in await Azure.Client.RequestGetDrivers()
				//                                            select new Driver
				//                                                   {
				//	                                                   StaffId     = D.StaffId,
				//	                                                   DisplayName = $"{D.FirstName} {D.LastName}".Trim().Capitalise()
				//                                                   } );
				Drivers = GetDrivers();

				foreach (var ServiceLevel in STemp)
					ServiceLevels.Add(ServiceLevel.NewName, new ServiceLevel(ServiceLevel));

				//            string boardNumber = BoardName.Substring(BoardName.IndexOf("_") + 1);
				//if (OnlyShowTripsForThisBoard && (boardNumber == "1" || boardNumber == "2" || boardNumber == "3"))
				//            {
				//                Trips = new ObservableCollection<DisplayTrip>(from T in Temp
				//												  where T.Board == boardNumber
				//                                                              orderby T.Status3 descending, T.CallTime
				//                                                              select new DisplayTrip(T, GetServiceLevel(T.ServiceLevel)));
				//            }
				//else
				//            {
				//                Trips = new ObservableCollection<DisplayTrip>(from T in Temp
				//                                                              orderby T.Status3 descending, T.CallTime
				//                                                              select new DisplayTrip(T, GetServiceLevel(T.ServiceLevel)));
				//            }

				Trips = new ObservableCollection<DisplayTrip>(from T in Temp
															  orderby T.Status3 descending, T.CallTime
															  select new DisplayTrip(T, GetServiceLevel(T.ServiceLevel)));
				var Message = FindStringResource("DispatchSortedByDescending");
				CurrentSortMessage = "    " + Message.Replace("@1", FindStringResource("DialoguesDispatchColumnCallDate")) + "    ";

				//var Name = BASE_BOARD_NAME + "0";
				////string Name = BASE_BOARD_NAME + "1";

				//List<string> Names = new();
				//var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

				//if (Tis.Count > 0)
				//{
				//    foreach (var Ti in Tis)
				//    {
				//        if (Ti.Content is DispatchBoard Pti)
				//        {
				//            var Tmp = ((DispatchViewModel)Pti.DataContext).BoardName;
				//            Logging.WriteLogLine("Found board: " + Tmp);
				//            Names.Add(Tmp);
				//        }
				//    }
				//}

				//// Now, check to see if there is a hole	
				//if (Names.Count > 0)
				//{
				//    for (var I = 0; I < 100; I++)
				//    {
				//        var Tmp = BASE_BOARD_NAME + I;
				//        //string Tmp = BASE_BOARD_NAME + (I + 1);

				//        if (!Names.Contains(Tmp))
				//        {
				//            // Found a hole
				//            Logging.WriteLogLine("Reusing boardname: " + Tmp);
				//            Name = Tmp;
				//            break;
				//        }
				//    }
				//}

				//if (!DispatchBoardNames.Contains(Name))
				//    DispatchBoardNames.Add(Name);
				//BoardName = Name;
				//Logging.WriteLogLine("Opening " + Name);

				////LoadSettings();

				OnArgumentChange(BoardNumber);

				DataGridVisible = true;

				ClearWaitCursor();
			}
		}
		catch (Exception E)
		{
			Logging.WriteLogLine(E);
		}
	}

	#region Setting Other Settings
	public void SaveOtherSettings()
	{
		OtherSettings Os = new()
		{
			ShowNotesOnOneLine = DisplayNotesOnOneLine
		};

		DispatchBoardSettings ??= new Settings();

		if (DispatchBoardSettings.OtherSettings.ContainsKey(BoardName))
			DispatchBoardSettings.OtherSettings[BoardName] = Os;
		else
			DispatchBoardSettings.OtherSettings.Add(BoardName, Os);

		SaveSettings(DispatchBoardSettings);
	}
	#endregion


	// ReSharper disable once UnusedParameter.Local
	public DispatchViewModel() : base(Messaging.DISPATCH_BOARD, new[] { STATUS.ACTIVE, STATUS.DELETED, STATUS.DISPATCHED, STATUS.FINALISED })
	{
		Logging.WriteLogLine("DEBUG BoardName: " + BoardName + " BoardNumber: " + BoardNumber + " OnlyShowTripsForThisBoard: " + OnlyShowTripsForThisBoard);
		//string name = BASE_BOARD_NAME + DispatchBoardNames.Count;
		var Name = BASE_BOARD_NAME + "0";
		//string Name = BASE_BOARD_NAME + "1";

		List<string> Names = new();
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if (Tis.Count > 0)
		{
			foreach (var Ti in Tis)
			{
				if (Ti.Content is DispatchBoard Pti)
				{
					var Tmp = ((DispatchViewModel)Pti.DataContext).BoardName;
					Logging.WriteLogLine("Found board: " + Tmp);
					Names.Add(Tmp);
				}
			}
		}

		// Now, check to see if there is a hole	
		if (Names.Count > 0)
		{
			for (var I = 0; I < 100; I++)
			{
				var Tmp = BASE_BOARD_NAME + I;
				//string Tmp = BASE_BOARD_NAME + (I + 1);

				if (!Names.Contains(Tmp))
				{
					// Found a hole
					Logging.WriteLogLine("Reusing boardname: " + Tmp);
					Name = Tmp;
					break;
				}
			}
		}

		if (!DispatchBoardNames.Contains(Name))
			DispatchBoardNames.Add(Name);
		BoardName = Name;
		Logging.WriteLogLine("Opening " + Name);

		//LoadSettings();
	}

	private static string FindStringResource(string name) => (string)Application.Current.TryFindResource(name);

	[DependsUpon(nameof(BoardName))]
	private void FormatBoardName()
	{
		if (BoardName.IsNotNullOrWhiteSpace())
		{
			var Pieces = BoardName.Split('_');

			if (Pieces.Length > 1)
			{
				if (Pieces[1] != "99")
				{
					var Name = Pieces[0] + " " + Pieces[1];
					FormattedBoardName = Name;
				}
				else
				{
					var Name = Pieces[0] + " New";
					FormattedBoardName = Name;
				}
			}
			else
				FormattedBoardName = BoardName;
		}

		var Message = FindStringResource("DispatchShowingTripsForAllBoards");

		//var boardNumber = BoardName.Substring(BASE_BOARD_NAME.Length);
		if (OnlyShowTripsForThisBoard)
		{
			if (BoardNumber is "1" or "2" or "3")
			{
				Message = FindStringResource("DispatchShowingTripsForBoard");
				Message = Message.Replace("@1", BoardNumber);
			}
		}
		else
		{
			if (BoardNumber is "99")
			{
				Message = FindStringResource("DispatchShowingTripsForBoard");
				Message = Message.Replace("@1", "New");

			}
		}
		Message = "    " + Message + "    ";
		Logging.WriteLogLine("DEBUG setting DisplayingTripsForBoardMessage to " + Message);
		DisplayingTripsForBoardMessage = Message;
	}

	private async Task UpdateStatusAndRemove(IReadOnlyCollection<DisplayTrip>? trips, STATUS status, STATUS1 status1 = STATUS1.UNSET, STATUS2 status2 = STATUS2.UNSET, string driver = "")
	{
		if (trips is not null)
		{
			var TripIds = new List<string>();

			for (var I = Trips.Count; --I >= 0;)
			{
				var TripId = Trips[I].TripId;

				if (trips.Any(trip => TripId == trip.TripId))
				{
					TripIds.Add(TripId);
					Trips.RemoveAt(I);
				}
			}

			switch (status)
			{
				case STATUS.DELETED:
					await UpdateStatus(PROGRAM, status, status1, status2, TripIds, driver, TripUpdateStatus.BROADCAST.DISPATCH_BOARD);
					break;

				case STATUS.DISPATCHED:
					await UpdateStatus(PROGRAM, status, status1, status2, TripIds, driver, TripUpdateStatus.BROADCAST.DRIVERS_BOARD | TripUpdateStatus.BROADCAST.DRIVER | TripUpdateStatus.BROADCAST.DISPATCH_BOARD);
					break;
			}

			RaisePropertyChanged(nameof(Trips));
		}
	}

	private ServiceLevel GetServiceLevel(string s)
	{
		if (!ServiceLevels.TryGetValue(s, out var Sl))
			Sl = new ServiceLevel();

		return Sl;
	}

	private static ObservableCollection<Driver> GetDrivers()
	{
		ObservableCollection<Driver> Drivers = new();

		if (!IsInDesignMode)
		{
			Task.WaitAll(
						 Task.Run(async () =>
								   {
									   //SetWaitCursor();
									   var StaffAndRoles = await Azure.Client.RequestGetStaffAndRoles();

									   if (StaffAndRoles != null)
									   {
										   foreach (var Sar in StaffAndRoles)
										   {
											   if (Sar.StaffId != Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE)
											   {
												   foreach (var R in Sar.Roles)
												   {
													   if (R.IsDriver)
													   {
														   var Exists = (from D in Drivers
																		 where D.StaffId == Sar.StaffId
																		 select D).FirstOrDefault();

														   if (Exists == null)
														   {
															   Driver Driver = new()
															   {
																   StaffId = Sar.StaffId,
																   DisplayName = $"{Sar.FirstName} {Sar.LastName}".Trim().Capitalise()
															   };

															   Drivers.Add(Driver);
														   }
														   break;
													   }
												   }
											   }
										   }
									   }
									   //ClearWaitCursor();
								   })
						);
		}
		return Drivers;
	}

	public bool IsDriverSelectionOpen
	{
		get { return Get(() => IsDriverSelectionOpen, false); }
		set { Set(() => IsDriverSelectionOpen, value); }
	}

	[DependsUpon(nameof(IsDriverSelectionOpen))]
	public void WhenIsDriverSelectionOpenChanges()
	{
		Logging.WriteLogLine("DEBUG IsDriverSelectionOpen: " + IsDriverSelectionOpen);
		if (!IsDriverSelectionOpen)
		{
			// Load the pending changes to Trips
			if (TripsToRemoveWhenDriverSelectionOver.Count > 0 || TripsToAddWhenDriverSelectionOver.Count > 0)
			{
				Dispatcher.Invoke(() =>
				{
					if (TripsToRemoveWhenDriverSelectionOver.Count > 0)
					{
						foreach (var trip in TripsToRemoveWhenDriverSelectionOver)
						{
							var Trps = Trips;

							for (var I = Trips.Count; --I >= 0;)
							{
								if (Trps[I].TripId == trip.TripId)
								{
									Logging.WriteLogLine("DEBUG Removing saved trip " + trip.TripId);
									Trips.RemoveAt(I);
									break;
								}
							}
						}

						TripsToRemoveWhenDriverSelectionOver.Clear();
					}
					if (TripsToAddWhenDriverSelectionOver.Count > 0)
					{
						foreach (var trip in TripsToAddWhenDriverSelectionOver)
						{
							Logging.WriteLogLine("DEBUG Adding saved trip " + trip.TripId);
							Trips.Add(trip);
						}

						TripsToAddWhenDriverSelectionOver.Clear();
					}

					WhenTripsChange();
				});
			}
		}
	}


	#region Abstracts

	public List<DisplayTrip> TripsToAddWhenDriverSelectionOver
	{
		get { return Get(() => TripsToAddWhenDriverSelectionOver, new List<DisplayTrip>()); }
		set { Set(() => TripsToAddWhenDriverSelectionOver, value); }
	}

	public List<DisplayTrip> TripsToRemoveWhenDriverSelectionOver
	{
		get { return Get(() => TripsToRemoveWhenDriverSelectionOver, new List<DisplayTrip>()); }
		set { Set(() => TripsToRemoveWhenDriverSelectionOver, value); }
	}

	//protected override void OnUpdateTrip(Trip trip)
	//{
	//	OnRemoveTrip(trip.TripId);

	//	if (trip.Status1 == STATUS.ACTIVE)
	//	{
	//		var Dt = new DisplayTrip(trip, GetServiceLevel(trip.ServiceLevel));
	//		Trips.Add(Dt);
	//	}
	//       // TODO 20221103 Eddy says to wait on this task
	//	//SortByCurrentColumn(); 

	//	RaisePropertyChanged(nameof(Trips));

	//}
	protected override void OnUpdateTrip(Trip trip)
	{
		if (!IsDriverSelectionOpen && View != null)
		{
			Logging.WriteLogLine("DEBUG Updating trip " + trip.TripId + " on board " + BoardName);
			List<string>? ids = View.GatherSelectedTrips();

			OnRemoveTrip(trip.TripId);

			if (trip.Status1 == STATUS.ACTIVE)
			{
				var Dt = new DisplayTrip(trip, GetServiceLevel(trip.ServiceLevel));
				Trips.Add(Dt);
			}

			RaisePropertyChanged(nameof(Trips));

			View.SetSelectedTrips(ids);

			Dispatcher.Invoke(() =>
			{
				UpdateConditions_Tick(null, null);
			});
		}
		else
		{
			// Save the trip for when the driver selection is finished
			Logging.WriteLogLine("DEBUG Driver selection dropdown is open - saving " + trip.TripId + " to ADD after");
			var Dt = new DisplayTrip(trip, GetServiceLevel(trip.ServiceLevel));
			TripsToAddWhenDriverSelectionOver.Add(Dt);
		}
	}

	//protected override void OnRemoveTrip(string tripId)
	//{
	//	Dispatcher.Invoke(() =>
	//						{
	//							var Trps = Trips;

	//							for (var I = Trips.Count; --I >= 0;)
	//							{
	//								if (Trps[I].TripId == tripId)
	//								{
	//									Trips.RemoveAt(I);
	//									break;
	//								}
	//							}
	//						});
	//}
	protected override void OnRemoveTrip(string tripId)
	{
		if (!IsDriverSelectionOpen && View != null)
		{
            List<string>? ids = View.GatherSelectedTrips();
			Dispatcher.Invoke(() =>
							   {
								   var Trps = Trips;

								   for (var I = Trips.Count; --I >= 0;)
								   {
									   if (Trps[I].TripId == tripId)
									   {
										   Trips.RemoveAt(I);
										   break;
									   }
								   }

								   View.SetSelectedTrips(ids);

                                   UpdateConditions_Tick(null, null);
							   });
		}
		else
		{
			// Save the trip for when the driver selection is finished
			Logging.WriteLogLine("DEBUG Driver selection dropdown is open - saving " + tripId + " to REMOVE after");
			DisplayTrip dt = (from T in Trips where T.TripId == tripId select T).FirstOrDefault();
			if (dt != null)
			{
				TripsToRemoveWhenDriverSelectionOver.Add(dt);
			}
		}
	}

	protected override async void OnUpdateStatus(string tripId, STATUS status, STATUS1 status1, STATUS2 status2, bool receivedByDevice, bool readByDriver)
	{
		switch (status)
		{
			case STATUS.DISPATCHED:
				OnRemoveTrip(tripId);
				break;

			case STATUS.ACTIVE:
			case STATUS.PICKED_UP:
				if ((status2 & STATUS2.WAS_UNDELIVERABLE) != 0)
				{
					var Trip = await Azure.Client.RequestGetTrip(new GetTrip
					{
						TripId = tripId
					});

					var Dt = new DisplayTrip(Trip, GetServiceLevel(Trip.ServiceLevel));
					//Trips.Insert(0, Dt);
					//RaisePropertyChanged(nameof(Trips));
					if (!IsDriverSelectionOpen && View != null)
					{
                        List<string>? ids = View.GatherSelectedTrips();
						Trips.Insert(0, Dt);
						RaisePropertyChanged(nameof(Trips));
						View.SetSelectedTrips(ids);

                        Dispatcher.Invoke(() =>
						{
							UpdateConditions_Tick(null, null);
						});
                    }
					else
					{
						// Save the trip for when the driver selection is finished
						TripsToAddWhenDriverSelectionOver.Add(Dt);
					}
				}
				break;
		}
	}
	#endregion

	#region Filter
	public string Filter
	{
		get { return Get(() => Filter, ""); }
		set { Set(() => Filter, value); }
	}

	[DependsUpon350(nameof(Filter))]
	public void WhenFilterChanges()
	{
		var F = Filter.Trim().ToLower();

		//WhenTripsChange(Trips);
		WhenTripsChange();

		var ShipmentCount = 0;

		foreach (var Trip in Trips)
		{
			// Check that the trip hasn't already been hidden by the Dispatch Filters
			if (Trip.IsVisible)
			{
				Trip.Filter = F;

				if (Trip.IsVisible)
					ShipmentCount++;
			}
		}

		TotalShipmentCount = ShipmentCount;
	}
	#endregion

	#region Trips
	private int LastTripCount { get; set; }

	public ObservableCollection<DisplayTrip> Trips
	{
		get { return Get(() => Trips, new ObservableCollection<DisplayTrip>()); }
		set { Set(() => Trips, value); }
		//set => Trips = SortByCurrentColumn(value);
		//set => {Trips = WhenTripsChange(value);
		//set
		//{
		//    Logging.WriteLogLine("DEBUG setting LastTripCount to " + Trips.Count);
		//    LastTripCount = Trips.Count;
		//    Trips = value;
		//    //WhenTripsChange();
		//}
	}


	[DependsUpon(nameof(Trips))]
	public void WhenTripsChange()
	{
		Logging.WriteLogLine("DEBUG Trips.Count: " + Trips.Count + " LastTripCount: " + LastTripCount);

		void ProcessNotes()
		{
			foreach (var Trip in Trips)
			{
				if (DisplayNotesOnOneLine)
				{
					// Had to do this in 2 passes because of Java line-endings
					Trip.DisplayAccountNotes = Trip.BillingNotes.Replace("\r", " ");
					Trip.DisplayAccountNotes = Trip.DisplayAccountNotes.Replace("\n", " ");
					Trip.DisplayPickupAddressNotes = Trip.PickupAddressNotes.Replace("\r", " ");
					Trip.DisplayPickupAddressNotes = Trip.DisplayPickupAddressNotes.Replace("\n", " ");
					Trip.DisplayPickupNotes = Trip.PickupNotes.Replace("\r", " ");
					Trip.DisplayPickupNotes = Trip.DisplayPickupNotes.Replace("\n", " ");
					Trip.DisplayDeliveryAddressNotes = Trip.DeliveryAddressNotes.Replace("\r", " ");
					Trip.DisplayDeliveryAddressNotes = Trip.DisplayDeliveryAddressNotes.Replace("\n", " ");
					Trip.DisplayDeliveryNotes = Trip.DeliveryNotes.Replace("\r", " ");
					Trip.DisplayDeliveryNotes = Trip.DisplayDeliveryNotes.Replace("\n", " ");
				}
				else
				{
					Trip.DisplayAccountNotes = Trip.BillingNotes;
					Trip.DisplayPickupAddressNotes = Trip.PickupAddressNotes;
					Trip.DisplayPickupNotes = Trip.PickupNotes;
					Trip.DisplayDeliveryAddressNotes = Trip.DeliveryAddressNotes;
					Trip.DisplayDeliveryNotes = Trip.DeliveryNotes;
				}

				Trip.IsVisible = true;
			}
		}


		if ((Trips.Count > 0) && (Trips.Count != LastTripCount))
		{
			ProcessNotes();

			Logging.WriteLogLine("DEBUG sorting Trips.Count: " + Trips.Count + ", LastTripCount: " + LastTripCount);
			LastTripCount = Trips.Count;
			//Trips = SortByCurrentColumn(Trips);
			SortByCurrentColumn();
		}
		var BNumber = BoardName.Substring(BoardName.IndexOf("_", StringComparison.Ordinal) + 1);

		foreach (var Trip in Trips)
		{
			Trip.IsVisible = true;
		}

		if (DictionaryFilters.ContainsKey(BoardName))
		{
			var BoardFilter = DictionaryFilters[BoardName];

			// Make sure that there is at least 1 active filter
			//var count = 0;

			var Active = (from Bf in BoardFilter
						  where Bf.Active
						  select Bf).ToList();

			if (Active.Count > 0)
			{
				//ObservableCollection<DisplayTrip> newTrips = new ObservableCollection<DisplayTrip>();
				foreach (var Trip in Trips)
				{
					//Logging.WriteLogLine( trip.TripId + " - DeliveryAddressRegion: " + trip.DeliveryAddressRegion );
					var Visible = false;
					Trip.IsVisible = false;

					foreach (var ActiveFilter in Active)
					{
						if (DoesTripMatchFilter(Trip, ActiveFilter))
						{
							Logging.WriteLogLine("Visible trip: " + Trip.TripId + " - matches filter: " + ActiveFilter);
							Visible = true;
							break;
						}
					}

					//if (Visible)
					//	Logging.WriteLogLine("Visible trip: " + Trip.TripId);
					//else
					//	Logging.WriteLogLine("Hiding trip: " + Trip.TripId);

					Trip.IsVisible = Visible;
				}
			}

			// At this point, filters have been applied - now apply board filter
			if (OnlyShowTripsForThisBoard)
			{
				foreach (var Trip in Trips)
				{
					if (Trip.IsVisible && (Trip.Board != BNumber))
						Trip.IsVisible = false;
				}
			}

			// Special - only trips without a board
			if (BNumber == "99")
			{
				foreach (var Trip in Trips)
				{
					if (Trip.IsVisible && Trip.Board.IsNotNullOrWhiteSpace() && (Trip.Board != "0"))
						Trip.IsVisible = false;
				}
			}
		}

		//WhenFilterChanges(false);
		var F = Filter.Trim().ToLower();

		foreach (var Trip in Trips)
		{
			// Check that the trip hasn't already been hidden by the Dispatch Filters
			if (Trip.IsVisible)
			{
				Trip.Filter = F;
			}
		}

		TripCount = Trips.Count;
		var ShipmentCount = Trips.Count(trip => trip.IsVisible);
		TotalShipmentCount = ShipmentCount;

		Dispatcher.Invoke(() =>
		{
			UpdateBackgrounds();
			//View?.OpenCurrentDriverSelectionComboBox();
		});
	}
	public void WhenTripsChange_V1()
	{
		Logging.WriteLogLine("DEBUG Trips.Count: " + Trips.Count + " LastTripCount: " + LastTripCount);

		//if( ( Trips.Count > 0 ) && ( Trips.Count != LastTripCount ) )
		//	Logging.WriteLogLine( "Trips changed - count: " + Trips.Count + " LastTripCount: " + LastTripCount );

		void ProcessNotes()
		{
			foreach (var Trip in Trips)
			{
				if (DisplayNotesOnOneLine)
				{
					// Had to do this in 2 passes because of Java line-endings
					Trip.DisplayAccountNotes = Trip.BillingNotes.Replace("\r", " ");
					Trip.DisplayAccountNotes = Trip.DisplayAccountNotes.Replace("\n", " ");
					Trip.DisplayPickupAddressNotes = Trip.PickupAddressNotes.Replace("\r", " ");
					Trip.DisplayPickupAddressNotes = Trip.DisplayPickupAddressNotes.Replace("\n", " ");
					Trip.DisplayPickupNotes = Trip.PickupNotes.Replace("\r", " ");
					Trip.DisplayPickupNotes = Trip.DisplayPickupNotes.Replace("\n", " ");
					Trip.DisplayDeliveryAddressNotes = Trip.DeliveryAddressNotes.Replace("\r", " ");
					Trip.DisplayDeliveryAddressNotes = Trip.DisplayDeliveryAddressNotes.Replace("\n", " ");
					Trip.DisplayDeliveryNotes = Trip.DeliveryNotes.Replace("\r", " ");
					Trip.DisplayDeliveryNotes = Trip.DisplayDeliveryNotes.Replace("\n", " ");
				}
				else
				{
					Trip.DisplayAccountNotes = Trip.BillingNotes;
					Trip.DisplayPickupAddressNotes = Trip.PickupAddressNotes;
					Trip.DisplayPickupNotes = Trip.PickupNotes;
					Trip.DisplayDeliveryAddressNotes = Trip.DeliveryAddressNotes;
					Trip.DisplayDeliveryNotes = Trip.DeliveryNotes;
				}

				Trip.IsVisible = true;
			}
		}

		ProcessNotes();

		if ((Trips.Count > 0) && (Trips.Count != LastTripCount))
		//if (Trips.Count > 0 && (Trips.Count != LastTripCount || ForceSort))
		{
			Logging.WriteLogLine("DEBUG sorting Trips.Count: " + Trips.Count + ", LastTripCount: " + LastTripCount);
			LastTripCount = Trips.Count;
			//Trips = SortByCurrentColumn(Trips);
			SortByCurrentColumn();
		}

		var BNumber = BoardName.Substring(BoardName.IndexOf("_", StringComparison.Ordinal) + 1);

		if (DictionaryFilters.ContainsKey(BoardName))
		{
			var BoardFilter = DictionaryFilters[BoardName];

			// Make sure that there is at least 1 active filter
			//var count = 0;

			var Active = (from Bf in BoardFilter
						  where Bf.Active
						  select Bf).ToList();

			if (Active.Count > 0)
			{
				//ObservableCollection<DisplayTrip> newTrips = new ObservableCollection<DisplayTrip>();
				foreach (var Trip in Trips)
				{
					//Logging.WriteLogLine( trip.TripId + " - DeliveryAddressRegion: " + trip.DeliveryAddressRegion );
					var Visible = false;
					Trip.IsVisible = false;

					foreach (var ActiveFilter in Active)
					{
						if (DoesTripMatchFilter(Trip, ActiveFilter))
						{
							Logging.WriteLogLine("Visible trip: " + Trip.TripId + " - matches filter: " + ActiveFilter);
							Visible = true;
							break;
						}
					}

					if (Visible)
						Logging.WriteLogLine("Visible trip: " + Trip.TripId);
					else
						Logging.WriteLogLine("Hiding trip: " + Trip.TripId);

					Trip.IsVisible = Visible;
				}
			}

			// At this point, filters have been applied - now apply board filter
			if (OnlyShowTripsForThisBoard)
			{
				foreach (var Trip in Trips)
				{
					if (Trip.IsVisible && (Trip.Board != BNumber))
						Trip.IsVisible = false;
				}
			}

			// Special - only trips without a board
			if (BNumber == "99")
			{
				foreach (var Trip in Trips)
				{
					if (Trip.IsVisible && Trip.Board.IsNotNullOrWhiteSpace() && (Trip.Board != "0"))
						Trip.IsVisible = false;
				}
			}
		}

		//WhenFilterChanges(false);
		var F = Filter.Trim().ToLower();

		foreach (var Trip in Trips)
		{
			// Check that the trip hasn't already been hidden by the Dispatch Filters
			if (Trip.IsVisible)
			{
				Trip.Filter = F;

				//if( Trip.IsVisible )
				//{
				//	Logging.WriteLogLine("DEBUG Visible trip: " + Trip.TripId);
				//}
			}
		}

		TripCount = Trips.Count;
		var ShipmentCount = Trips.Count(trip => trip.IsVisible);
		TotalShipmentCount = ShipmentCount;

		//        bool isOdd = false;
		//        foreach (DisplayTrip trip in Trips)
		//        {
		//if (trip.IsVisible)
		//            {
		//	ServiceLevel sl = GetServiceLevel(trip.ServiceLevel);
		//	if (sl.Background.ToString() != Brushes.White.ToString())
		//	{
		//		trip.UnSelectedBackground = sl.Background;
		//		trip.UnSelectedForeground = sl.Foreground;
		//	}
		//	else
		//	{
		//		//trip.UnSelectedBackground = isOdd ? Brushes.White : Brushes.LightGray;
		//		trip.UnSelectedBackground = isOdd ? Brushes.LightGray : Brushes.White;
		//	}
		//	isOdd = !isOdd;
		//            }
		//        }
		UpdateBackgrounds();

		//UpdateConditions_Tick(null, null);

		//if (Trips.Count > 0 && Trips.Count != LastTripCount)
		////if (Trips.Count > 0 && (Trips.Count != LastTripCount || ForceSort))
		//{
		//    Logging.WriteLogLine("DEBUG sorting Trips.Count: " + Trips.Count + ", LastTripCount: " + LastTripCount);
		//    LastTripCount = Trips.Count;
		//    //Trips = SortByCurrentColumn(Trips);
		//    SortByCurrentColumn();
		//    ForceSort = false;
		//}
		// This version causes StackOverflow
		//if (Trips.Count > 0 && Trips.Count != LastTripCount)
		//{
		//    //Trips = SortByCurrentColumn(Trips);
		//    SortByCurrentColumn();
		//}

		//if (View != null)
		//         {
		//	View.SetSortInfo(View.DataGrid, View.SortInfos);
		//	//View.PerformSort(View.LastColumnClicked);
		//	//View.PerformSort();
		//         }

		//return Trips;
	}

	public void UpdateBackgrounds()
	{
		var IsOdd = false;

		try
		{
			foreach (var Trip in Trips)
			{
				if (Trip.IsVisible && IsTripForThisBoard(Trip))
				{
					var Sl = GetServiceLevel(Trip.ServiceLevel);

					if (Sl.Background.ToString() != Brushes.White.ToString())
					{
						Trip.UnSelectedBackground = Sl.Background;
						Trip.UnSelectedForeground = Sl.Foreground;
					}
					else
						Trip.UnSelectedBackground = IsOdd ? Brushes.LightGray : Brushes.White;
					IsOdd = !IsOdd;
				}
			}
		}
		catch (Exception E)
		{
			Logging.WriteLogLine("Error updating backgrounds: " + E);
		}
	}

	private bool IsTripForThisBoard(TripUpdate dt)
	{
		var Yes = true;

		if (BoardNumber.IsNullOrWhiteSpace())
		{
			Logging.WriteLogLine("Setting BoardNumber to " + BoardNumber);
			BoardNumber = BoardName.Substring(BoardName.IndexOf("_", StringComparison.Ordinal) + 1);
		}

		////      ""		"0"
		//if (dt.Board == BoardNumber)
		if (OnlyShowTripsForThisBoard)
		{
			if (dt.Board != BoardNumber)
				Yes = false;
		}

		//Logging.WriteLogLine("DEBUG TripId: " + dt.TripId + " - IsTripForThisBoard: " + yes);
		return Yes;
	}

	public string CurrentSortMessage
	{
		get { return Get(() => CurrentSortMessage, string.Empty); }
		set { Set(() => CurrentSortMessage, value); }
	}

	public void SortByCurrentColumn()
	{
		if (View is { } V)
		{
			List<DisplayTrip> NewList = new();

			if (Trips is { Count: > 1 })
			{
				var LastSort = V.LastColumnClicked;
				var Ascending = V.AscendingSort;
				//Logging.WriteLogLine("DEBUG lastSort: " + lastSort + " ascending: " + ascending);
				var Message = Ascending ? FindStringResource("DispatchSortedByAscending") : FindStringResource("DispatchSortedByDescending");
				CurrentSortMessage = "    " + Message.Replace("@1", LastSort) + "    ";

				switch (LastSort)
				{
					case "Ok":
						NewList = (from T in Trips
								   orderby T.EnableAccept ^ Ascending
								   select T).ToList();
						break;

					case "Driver":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.Driver, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.Driver, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Shipment Id":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.TripId, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.TripId, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Acct":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.AccountId, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.AccountId, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Company Name":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.BillingCompanyName, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.BillingCompanyName, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Acct Notes":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.BillingAddressNotes, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.BillingAddressNotes, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Call Date":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															//Trips.OrderBy(n => n.CallTime, Comparer<DateTimeOffst>.Create((x, y) => DateTimeOffset.Compare(x, y))));
															Trips.OrderBy(n => n.CallTime.ToString(), Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															//Trips.OrderBy(n => n.CallTime, Comparer<DateTimeOffst>.Create((x, y) => DateTimeOffset.Compare(x, y))));
															Trips.OrderByDescending(n => n.CallTime.ToString(), Comparer<string>.Create(string.Compare)));
						}
						break;

					case "CZ":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.CurrentZone, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.CurrentZone, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Pickup":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAccountId, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAccountId, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Suite":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressSuite, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressSuite, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Street":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressAddressLine1, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressAddressLine1, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU City":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressCity, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressCity, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Prov/State":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressRegion, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressRegion, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Country":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressCountry, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressCountry, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Postal/Zip":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressPostalCode, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressPostalCode, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Addr Notes":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupAddressNotes, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupAddressNotes, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PU Notes":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupNotes, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupNotes, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "PZ":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PickupZone, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PickupZone, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Delivery":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAccountId, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAccountId, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Suite":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressSuite, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressSuite, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Street":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressAddressLine1, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressAddressLine1, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del City":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressCity, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressCity, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Prov/State":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressRegion, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressRegion, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Country":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressCountry, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressCountry, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Postal/Zip":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressPostalCode, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressPostalCode, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Addr Notes":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryAddressNotes, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryAddressNotes, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Del Notes":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryNotes, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryNotes, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "DZ":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.DeliveryZone, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.DeliveryZone, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Serv":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.ServiceLevel, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.ServiceLevel, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Pkg":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.PackageType, Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.PackageType, Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Ready":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															//Trips.OrderBy(n => n.ReadyTime, Comparer<DateTime>.Create((x, y) => DateTime.Compare(x, y))));
															Trips.OrderBy(n => n.ReadyTime.ToString(), Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															//Trips.OrderBy(n => n.ReadyTime, Comparer<DateTime>.Create((x, y) => DateTime.Compare(x, y))));
															Trips.OrderByDescending(n => n.ReadyTime.ToString(), Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Due":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															//Trips.OrderBy(n => n.DueTime, Comparer<DateTimeOffset>.Create((x, y) => DateTimeOffset.Compare(x, y))));
															Trips.OrderBy(n => n.DueTime.ToString(), Comparer<string>.Create(string.Compare)));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															//Trips.OrderBy(n => n.DueTime, Comparer<DateTimeOffset>.Create((x, y) => DateTimeOffset.Compare(x, y))));
															Trips.OrderByDescending(n => n.DueTime.ToString(), Comparer<string>.Create(string.Compare)));
						}
						break;

					case "Pcs":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.Pieces));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.Pieces));
						}
						break;

					case "Wt":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.Weight));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.Weight));
						}
						break;

					case "Board":
						if (Ascending)
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderBy(n => n.Board));
						}
						else
						{
							NewList = new List<DisplayTrip>(
															Trips.OrderByDescending(n => n.Board));
						}
						break;
				}

				Trips.Clear();

				foreach (var Trip in NewList)
					//Logging.WriteLogLine("DEBUG After Sorting - adding " + trip.TripId);
					Trips.Add(Trip);
				//WhenTripsChange();
				//UpdateBackgrounds();
				//UpdateConditions_Tick(null, null); // Stack overflow
			}
		}
	}

	private static bool DoesTripMatchFilter(DisplayTrip dt, DispatchFilter df)
	{
		var Yes = false;

		if (df.ColumnName is { } Column && df.Condition is { } DfCondition && df.Value is { } DfValue)
		{
			var Field = MapColumnToTripField(Column);

			Yes = Field.ToLower() switch //(colName.ToLower())
			{
				"driver" => DoesStringMatch(dt.Driver, DfCondition, DfValue),
				"tripid" => DoesStringMatch(dt.TripId, DfCondition, DfValue),
				"accountid" => DoesStringMatch(dt.AccountId, DfCondition, DfValue),
				"billingcompanyname" => DoesStringMatch(dt.BillingCompanyName, DfCondition, DfValue),
				"billingnotes" => DoesStringMatch(dt.BillingNotes, DfCondition, DfValue),
				"currentzone" => DoesStringMatch(dt.CurrentZone, DfCondition, DfValue),
				"pickupcompany" => DoesStringMatch(dt.PickupCompanyName, DfCondition, DfValue),
				"pickupaddresssuite" => DoesStringMatch(dt.PickupAddressSuite, DfCondition, DfValue),
				"pickupaddressaddressline1" => DoesStringMatch(dt.PickupAddressAddressLine1, DfCondition, DfValue),
				"pickupaddresscity" => DoesStringMatch(dt.PickupAddressCity, DfCondition, DfValue),
				"pickupaddressregion" => DoesStringMatch(dt.PickupAddressRegion, DfCondition, DfValue),
				"pickupaddresscountry" => DoesStringMatch(dt.PickupAddressCountry, DfCondition, DfValue),
				"pickupaddresspostalcode" => DoesStringMatch(dt.PickupAddressPostalCode, DfCondition, DfValue),
				"pickupaddressnotes" => DoesStringMatch(dt.PickupAddressNotes, DfCondition, DfValue),
				"pickupnotes" => DoesStringMatch(dt.PickupNotes, DfCondition, DfValue),
				"pickupzone" => DoesStringMatch(dt.PickupZone, DfCondition, DfValue),
				"deliverycompany" => DoesStringMatch(dt.DeliveryCompanyName, DfCondition, DfValue),
				"deliveryaddresssuite" => DoesStringMatch(dt.DeliveryAddressSuite, DfCondition, DfValue),
				"deliveryaddressaddressline1" => DoesStringMatch(dt.DeliveryAddressAddressLine1, DfCondition, DfValue),
				"deliveryaddresscity" => DoesStringMatch(dt.DeliveryAddressCity, DfCondition, DfValue),
				"deliveryaddressregion" => DoesStringMatch(dt.DeliveryAddressRegion, DfCondition, DfValue),
				"deliveryaddresscountry" => DoesStringMatch(dt.DeliveryAddressCountry, DfCondition, DfValue),
				"deliveryaddresspostalzip" => DoesStringMatch(dt.DeliveryAddressPostalBarcode, DfCondition, DfValue),
				"deliverynotes" => DoesStringMatch(dt.DeliveryNotes, DfCondition, DfValue),
				"deliveryzone" => DoesStringMatch(dt.DeliveryZone, DfCondition, DfValue),
				"servicelevel" => DoesStringMatch(dt.ServiceLevel, DfCondition, DfValue),
				"packagetype" => DoesStringMatch(dt.PackageType, DfCondition, DfValue),

				//case "readytime":
				//	yes = DoesDateTimeMatch(dt.readyTime, condition, value);
				//	break;
				//case "deadlinetime":
				//	yes = DoesDateTimeMatch(dt.deadlineTime, condition, value);
				//	break;
				"pieces" => DoesDecimalMatch(dt.Pieces, DfCondition, DfValue.Trim()),
				"weight" =>

					//yes = DoesDecimalMatch(new decimal(dt.Weight), condition, value.Trim());
					DoesDecimalMatch(dt.Weight, DfCondition, DfValue.Trim()),
				"board" =>

					//yes = DoesDecimalMatch(new decimal(dt.Board), condition, value.Trim());
					DoesStringMatch(dt.Board, DfCondition, DfValue),
				_ => Yes
			};
		}

		return Yes;
	}

	//public static string MapColumnToTripField(string column)
	//{
	//	var fieldName = string.Empty;

	//	if (dictionaryColumns == null || dictionaryColumns.Count == 0)
	//	{
	//		dictionaryColumns = new Dictionary<string, string>
	//		{
	//			{ "DialoguesDispatchColumnDriver", "driver" },
	//			{ "DialoguesDispatchColumnTripId", "tripId" },
	//			{ "DialoguesDispatchColumnAcct", "accountId" },
	//			//{ "DialoguesDispatchColumnCZ", "currentZone" },
	//			//{ "DialoguesDispatchColumnPZ", "pickupZone" },
	//			{ "DialoguesDispatchColumnPickup", "pickupCompany" },
	//			//{ "DialoguesDispatchColumnDZ", "deliveryZone" },
	//                  { "DialoguesDispatchColumnPickupStreet", "pickupStreet" },
	//			{ "DialoguesDispatchColumnPickupCity", "pickupCity" },
	//			{ "DialoguesDispatchColumnDelivery", "deliveryCompany" },
	//			{ "DialoguesDispatchColumnDeliveryStreet", "deliveryStreet" },
	//			{ "DialoguesDispatchColumnDeliveryCity", "deliveryCity" },
	//			{ "DialoguesDispatchColumnServ", "serviceLevel" },
	//			{ "DialoguesDispatchColumnPkg", "packageType" },
	//			{ "DialoguesDispatchColumnReady", "readyTime" },
	//			{ "DialoguesDispatchColumnDue", "deadlineTime" },
	//			{ "DialoguesDispatchColumnPcs", "pieces" },
	//			{ "DialoguesDispatchColumnWt", "weight" },
	//			//{ "DialoguesDispatchColumnBoard", "board" },
	//			//{ "DialoguesDispatchColumnPlts", "pallets" },
	//			{ "DialoguesDispatchColumnPN", "pickupNotes" },
	//			{ "DialoguesDispatchColumnDN", "deliveryNotes" },
	//			{ "DialoguesDispatchColumnDstate", "deliveryProvince" },
	//			{ "DialoguesDispatchColumnDzip", "deliveryPc" }
	//		};
	//	}


	//	if (column != null && column != string.Empty)
	//	{
	//		fieldName = dictionaryColumns[column];
	//	}

	//	return fieldName;
	//}


	public int TotalShipmentCount
	{
		get { return Get(() => TotalShipmentCount, 0); }
		set { Set(() => TotalShipmentCount, value); }
	}


	public int TripCount
	{
		get { return Get(() => TripCount, 0); }
		set { Set(() => TripCount, value); }
	}

	//[DependsUpon250(nameof(Trips))]
	//public void WhenTripsChange()
	//{
	//	var T = Trips;
	//	TripCount = T.Count;

	//	var ShipmentCount = T.Count(trip => trip.IsVisible);

	//	TotalShipmentCount = ShipmentCount;
	//}
	#endregion

	#region Pickup and Deliver note editing
	public bool IsEditingPickupNote
	{
		get { return Get(() => IsEditingPickupNote, false); }
		set { Set(() => IsEditingPickupNote, value); }
	}

	[DependsUpon(nameof(IsEditingPickupNote))]
	public string GetPickupSaveCancelVisiblity()
	{
		var Vis = Visibility.Collapsed;

		if (IsEditingPickupNote)
			Vis = Visibility.Visible;

		return Vis.ToString();
	}

	public string PickupSaveCancelVisiblity
	{
		get
		{
			var Vis = GetPickupSaveCancelVisiblity();
			return Vis;
		}
		//get { return Get(() => DeliverySaveCancelVisiblity, GetDeliverySaveCancelVisiblity()); }
		//set { Set(() => PickupSaveCancelVisiblity, value); }
	}

	public bool IsEditingDeliveryNote
	{
		get { return Get(() => IsEditingDeliveryNote, false); }
		set { Set(() => IsEditingDeliveryNote, value); }
	}

	[DependsUpon(nameof(IsEditingDeliveryNote))]
	public string GetDeliverySaveCancelVisiblity()
	{
		var Vis = Visibility.Collapsed;

		if (IsEditingDeliveryNote)
			Vis = Visibility.Visible;

		return Vis.ToString();
	}

	public string DeliverySaveCancelVisiblity
	{
		get
		{
			var Vis = GetDeliverySaveCancelVisiblity();
			return Vis;
		}
		//get { return Get(() => DeliverySaveCancelVisiblity, GetDeliverySaveCancelVisiblity()); }
		//set { Set(() => PickupSaveCancelVisiblity, value); }
	}

	public static void SaveTrip(DisplayTrip trip)
	{
		if (!IsInDesignMode)
		{
			Task.WaitAll(
						 Task.Run(async () =>
								   {
									   Logging.WriteLogLine("Saving trip " + trip.TripId);
									   Trip Tmp = new(trip);
									   await Azure.Client.RequestAddUpdateTrip(UpdateBroadcast(Tmp));
								   })
						);
		}
	}

	private static void SaveTrips(List<DisplayTrip> trips)
	{
		if (!IsInDesignMode)
		{
			foreach (var Trip in trips)
			{
				Task.Run(async () =>
						  {
							  Logging.WriteLogLine("Saving trip " + Trip.TripId);
							  Trip Tmp = new(Trip);
							  await Azure.Client.RequestAddUpdateTrip(UpdateBroadcast(Tmp));
						  });
			}
		}
	}

	private static Trip UpdateBroadcast(Trip t)
	{
		switch (t.Status1)
		{
			case STATUS.ACTIVE:
				t.BroadcastToDispatchBoard = true;
				break;

			case STATUS.DISPATCHED:
			case STATUS.PICKED_UP:
				t.BroadcastToDispatchBoard = true; // Shipment entry can also dispatch
				t.BroadcastToDriverBoard = true;
				t.BroadcastToDriver = true;
				break;
		}

		return t;
	}
	#endregion

	#region Menu
	// ReSharper disable once UnusedMember.Global
	public void Execute_ClearSelection()
	{
		//foreach( var DisplayTrip in Trips )
		if (SelectedTrips != null && SelectedTrips.Count > 0)
		{
			foreach (var DisplayTrip in SelectedTrips)
			{
				DisplayTrip.EnableAccept = false;
				DisplayTrip.Driver = string.Empty;
			}

			SelectedTrip = null;
		}
	}

	// ReSharper disable once UnusedMember.Global
	public void Execute_AuditTrip()
	{
		if (SelectedTrip is not null)
			Globals.RunProgram(Globals.TRIP_AUDIT_TRAIL, SelectedTrip.TripId);
	}
	#endregion

	#region Settings Persistence
	public class ColumnSettings
	{
		public List<string> Columns { get; init; } = new();
	}

	public class FiltersSettings
	{
		//public List<DispatchFilter> Filters { get; set; } = new List<DispatchFilter>();
		public List<string> Filters { get; init; } = new();
		public bool OnlyShowTripsForThisBoard { get; init; }
	}

	public class OtherSettings
	{
		public bool ShowNotesOnOneLine { get; init; }
	}

	public class Settings
	{
		public Dictionary<string, ColumnSettings> DispatchColumnSettings { get; } = new();
		public Dictionary<string, FiltersSettings> DispatchFilterSettings { get; } = new();

		// ReSharper disable once MemberHidesStaticFromOuterClass
		public Dictionary<string, OtherSettings> OtherSettings { get; } = new();
	}

	private new void LoadSettings()
	{
		if (!IsInDesignMode)
		{
			try
			{
				var FileName = MakeFileName();

				OnlyShowTripsForThisBoard = false;

				if (File.Exists(FileName))
				{
					Logging.WriteLogLine("Loading settings from " + FileName);
					var SerializedObject = File.ReadAllText(FileName);
					SerializedObject = Encryption.Decrypt(SerializedObject);
					DispatchBoardSettings = JsonConvert.DeserializeObject<Settings>(SerializedObject);

					if (DispatchBoardSettings is { } Ds)
					{
						if (Ds.DispatchColumnSettings.TryGetValue(BoardName, out var ColumnSettings))
						{
							var ActiveCols = ColumnSettings.Columns;

							foreach (var Col in ActiveCols)
								DictionaryColumnVisibility[Col] = Visibility.Visible;
						}
						else
						{
							var ActiveCols = GetAllDispatchColumnsInOrder();

							foreach (var Col in ActiveCols)
								DictionaryColumnVisibility[Col] = Visibility.Visible;
						}

						if (DispatchBoardSettings.DispatchFilterSettings.ContainsKey(BoardName))
						{
							List<DispatchFilter> Dfs = new();
							var ActiveFilters = DispatchBoardSettings.DispatchFilterSettings[BoardName].Filters;

							foreach (var ActiveFilter in ActiveFilters)
							{
								DispatchFilter Df = new(ActiveFilter);
								Dfs.Add(Df);
							}

							if (!DictionaryFilters.ContainsKey(BoardName))
								DictionaryFilters.Add(BoardName, Dfs);
							else
								DictionaryFilters[BoardName] = Dfs;

							OnlyShowTripsForThisBoard = BoardNumber is "1" or "2" or "3" && DispatchBoardSettings.DispatchFilterSettings[BoardName].OnlyShowTripsForThisBoard;
						}
						else
							OnlyShowTripsForThisBoard = BoardNumber is "1" or "2" or "3";

						if (DispatchBoardSettings.OtherSettings.ContainsKey(BoardName))
						{
							DisplayNotesOnOneLine = DispatchBoardSettings.OtherSettings[BoardName].ShowNotesOnOneLine;
							Logging.WriteLogLine("Setting DisplayNotesOnOneLine to " + DisplayNotesOnOneLine);
						}
					}
				}
				else
				{
					var ActiveCols = GetAllDispatchColumnsInOrder();

					foreach (var Col in ActiveCols)
						DictionaryColumnVisibility[Col] = Visibility.Visible;

					OnlyShowTripsForThisBoard = BoardNumber is "1" or "2" or "3";
				}
			}
			catch (Exception E)
			{
				Logging.WriteLogLine("ERROR: unable to load settings: " + E);

				// Doing this to ensure that the board will load if the file is corrupted
				Logging.WriteLogLine("Deleting bad settings file for DispatchBoard");
				DispatchBoardSettings = new Settings();
				SaveSettings(DispatchBoardSettings);
			}
		}
	}

	private void SaveSettings(Settings settings)
	{
		if (!IsInDesignMode)
		{
			try
			{
				var SerializedObject = JsonConvert.SerializeObject(settings);

				//Logging.WriteLogLine("Saved settings:\n" + SerializedObject);
				SerializedObject = Encryption.Encrypt(SerializedObject);
				var FileName = MakeFileName();
				Logging.WriteLogLine("Saving settings to " + FileName);
				File.WriteAllText(FileName, SerializedObject);
			}
			catch (Exception E)
			{
				Logging.WriteLogLine("ERROR: unable to save settings: " + E);
			}
		}
	}
	#endregion

	#region Settings Columns
	//
	// Visibility properties for columns
	//


	public Visibility AccountVisibility
	{
		get { return Get(() => AccountVisibility, Visibility.Visible); }
		set { Set(() => AccountVisibility, value); }
	}


	public Visibility ConditionVisibility
	{
		get { return Get(() => ConditionVisibility, Visibility.Visible); }
		set { Set(() => ConditionVisibility, value); }
	}


	public Visibility CompanyNameVisibility
	{
		get { return Get(() => CompanyNameVisibility, Visibility.Visible); }
		set { Set(() => CompanyNameVisibility, value); }
	}

	public Visibility AccountNotesVisibility
	{
		get { return Get(() => AccountNotesVisibility, Visibility.Visible); }
		set { Set(() => AccountNotesVisibility, value); }
	}

	// ReSharper disable once UnusedMember.Global
	public Visibility CallDateVisibility
	{
		get { return Get(() => CallDateVisibility, Visibility.Visible); }
		set { Set(() => CallDateVisibility, value); }
	}

	public Visibility CurrentZoneVisibility
	{
		get { return Get(() => CurrentZoneVisibility, Visibility.Visible); }
		set { Set(() => CurrentZoneVisibility, value); }
	}

	public Visibility PickupCompanyVisibility
	{
		get { return Get(() => PickupCompanyVisibility, Visibility.Visible); }
		set { Set(() => PickupCompanyVisibility, value); }
	}

	public Visibility PickupSuiteVisibility
	{
		get { return Get(() => PickupSuiteVisibility, Visibility.Visible); }
		set { Set(() => PickupSuiteVisibility, value); }
	}

	public Visibility PickupStreetVisibility
	{
		get { return Get(() => PickupStreetVisibility, Visibility.Visible); }
		set { Set(() => PickupStreetVisibility, value); }
	}

	public Visibility PickupCityVisibility
	{
		get { return Get(() => PickupCityVisibility, Visibility.Visible); }
		set { Set(() => PickupCityVisibility, value); }
	}

	public Visibility PickupProvStateVisibility
	{
		get { return Get(() => PickupProvStateVisibility, Visibility.Visible); }
		set { Set(() => PickupProvStateVisibility, value); }
	}

	public Visibility PickupCountryVisibility
	{
		get { return Get(() => PickupCountryVisibility, Visibility.Visible); }
		set { Set(() => PickupCountryVisibility, value); }
	}

	public Visibility PickupPostalZipVisibility
	{
		get { return Get(() => PickupPostalZipVisibility, Visibility.Visible); }
		set { Set(() => PickupPostalZipVisibility, value); }
	}

	public Visibility PickupAddressNotesVisibility
	{
		get { return Get(() => PickupAddressNotesVisibility, Visibility.Visible); }
		set { Set(() => PickupAddressNotesVisibility, value); }
	}

	public Visibility PickupNotesVisibility
	{
		get { return Get(() => PickupNotesVisibility, Visibility.Visible); }
		set { Set(() => PickupNotesVisibility, value); }
	}

	public Visibility PickupZoneVisibility
	{
		get { return Get(() => PickupZoneVisibility, Visibility.Visible); }
		set { Set(() => PickupZoneVisibility, value); }
	}

	public Visibility DeliveryCompanyVisibility
	{
		get { return Get(() => DeliveryCompanyVisibility, Visibility.Visible); }
		set { Set(() => DeliveryCompanyVisibility, value); }
	}

	public Visibility DeliverySuiteVisibility
	{
		get { return Get(() => DeliverySuiteVisibility, Visibility.Visible); }
		set { Set(() => DeliverySuiteVisibility, value); }
	}

	public Visibility DeliveryStreetVisibility
	{
		get { return Get(() => DeliveryStreetVisibility, Visibility.Visible); }
		set { Set(() => DeliveryStreetVisibility, value); }
	}

	public Visibility DeliveryCityVisibility
	{
		get { return Get(() => DeliveryCityVisibility, Visibility.Visible); }
		set { Set(() => DeliveryCityVisibility, value); }
	}

	public Visibility DeliveryProvStateVisibility
	{
		get { return Get(() => DeliveryProvStateVisibility, Visibility.Visible); }
		set { Set(() => DeliveryProvStateVisibility, value); }
	}

	public Visibility DeliveryCountryVisibility
	{
		get { return Get(() => DeliveryCountryVisibility, Visibility.Visible); }
		set { Set(() => DeliveryCountryVisibility, value); }
	}

	public Visibility DeliveryPostalZipVisibility
	{
		get { return Get(() => DeliveryPostalZipVisibility, Visibility.Visible); }
		set { Set(() => DeliveryPostalZipVisibility, value); }
	}

	public Visibility DeliveryAddressNotesVisibility
	{
		get { return Get(() => DeliveryAddressNotesVisibility, Visibility.Visible); }
		set { Set(() => DeliveryAddressNotesVisibility, value); }
	}

	public Visibility DeliveryNotesVisibility
	{
		get { return Get(() => DeliveryNotesVisibility, Visibility.Visible); }
		set { Set(() => DeliveryNotesVisibility, value); }
	}

	public Visibility DeliveryZoneVisibility
	{
		get { return Get(() => DeliveryZoneVisibility, Visibility.Visible); }
		set { Set(() => DeliveryZoneVisibility, value); }
	}

	public Visibility ServiceLevelVisibility
	{
		get { return Get(() => ServiceLevelVisibility, Visibility.Visible); }
		set { Set(() => ServiceLevelVisibility, value); }
	}

	public Visibility PackageTypeVisibility
	{
		get { return Get(() => PackageTypeVisibility, Visibility.Visible); }
		set { Set(() => PackageTypeVisibility, value); }
	}

	public Visibility ReadyTimeVisibility
	{
		get { return Get(() => ReadyTimeVisibility, Visibility.Visible); }
		set { Set(() => ReadyTimeVisibility, value); }
	}

	public Visibility DueTimeVisibility
	{
		get { return Get(() => DueTimeVisibility, Visibility.Visible); }
		set { Set(() => DueTimeVisibility, value); }
	}

	public Visibility PiecesVisibility
	{
		get { return Get(() => PiecesVisibility, Visibility.Visible); }
		set { Set(() => PiecesVisibility, value); }
	}

	public Visibility WeightVisibility
	{
		get { return Get(() => WeightVisibility, Visibility.Visible); }
		set { Set(() => WeightVisibility, value); }
	}

	public Visibility BoardVisibility
	{
		get { return Get(() => BoardVisibility, Visibility.Visible); }
		set { Set(() => BoardVisibility, value); }
	}

	public Visibility PalletsVisibility
	{
		get { return Get(() => PalletsVisibility, Visibility.Visible); }
		set { Set(() => PalletsVisibility, value); }
	}

	// End of Visibility

	public List<string> GetAllDispatchColumnsInOrder() => AllDispatchColumnsInOrder;


	/// <summary>
	/// </summary>
	/// <param
	///     name="column">
	/// </param>
	/// <returns></returns>
	public static string MapColumnToTripField(string column)
	{
		var FieldName = string.Empty;

		if ((column != string.Empty) && DictionaryColumns.ContainsKey(column))
			FieldName = DictionaryColumns[column];

		return FieldName;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="column">
	/// </param>
	/// <returns></returns>
	public IEnumerable<string>? MapColumnToConditions(string column)
	{
		List<string>? Conditions = null;

		if ((column != string.Empty) && DictionaryConditionsForField.ContainsKey(column))
			Conditions = DictionaryConditionsForField[column];

		return Conditions;
	}

	public void SaveDispatchColumns(List<string> newColumns)
	{
		List<string> Keys = new();

		foreach (var Key in DictionaryColumnVisibility.Keys)
			Keys.Add(Key);

		foreach (var Key in Keys)
		{
			if (newColumns.Contains(Key))
			{
				//Logging.WriteLogLine( "Setting column: " + Key + " to Visible" );
				DictionaryColumnVisibility[Key] = Visibility.Visible;
			}
			else
			{
				//Logging.WriteLogLine( "Setting column: " + Key + " to Hidden" );
				DictionaryColumnVisibility[Key] = Visibility.Hidden;
			}
		}

		foreach (var Key in Keys)
		{
			switch (Key)
			{
				case "DialoguesDispatchColumnDriver":
				case "DialoguesDispatchColumnTripId":
					break;

				case "DialoguesDispatchColumnAcct":
					AccountVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnCompanyName":
					CompanyNameVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnAcctNotes":
					AccountNotesVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnCZ":
					CurrentZoneVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickup":
					PickupCompanyVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupSuite":
					PickupSuiteVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupStreet":
					PickupStreetVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupCity":
					PickupCityVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupProvState":
					PickupProvStateVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupCountry":
					PickupCountryVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupPostalZip":
					PickupPostalZipVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupAddressNotes":
					PickupAddressNotesVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPickupNotes":
					PickupNotesVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPZ":
					PickupZoneVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDelivery":
					DeliveryCompanyVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliverySuite":
					DeliverySuiteVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryStreet":
					DeliveryStreetVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryCity":
					DeliveryCityVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryProvState":
					DeliveryProvStateVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryCountry":
					DeliveryCountryVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryPostalZip":
					DeliveryPostalZipVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryAddressNotes":
					DeliveryAddressNotesVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDeliveryNotes":
					DeliveryNotesVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDZ":
					DeliveryZoneVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnServ":
					ServiceLevelVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPkg":
					PackageTypeVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnReady":
					ReadyTimeVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnDue":
					DueTimeVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnPcs":
					PiecesVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnWt":
					WeightVisibility = DictionaryColumnVisibility[Key];
					break;

				case "DialoguesDispatchColumnBoard":
					BoardVisibility = DictionaryColumnVisibility[Key];
					break;

					//case "DialoguesDispatchColumnPlts":
					//	PalletsVisibility = dictionaryColumnVisiblity[key];
					//	break;
			}
		}

		var Cs = new ColumnSettings
		{
			Columns = newColumns
		};

		DispatchBoardSettings ??= new Settings();

		if (DispatchBoardSettings.DispatchColumnSettings.ContainsKey(BoardName))
			DispatchBoardSettings.DispatchColumnSettings[BoardName] = Cs;
		else
			DispatchBoardSettings.DispatchColumnSettings.Add(BoardName, Cs);

		SaveSettings(DispatchBoardSettings);
	}
	#endregion


	#region Settings Filters
	public bool OnlyShowTripsForThisBoard
	{
		get { return Get(() => OnlyShowTripsForThisBoard, true); }
		set { Set(() => OnlyShowTripsForThisBoard, value); }
	}

	public string OnlyShowTripsForThisBoardVisibility
	{
		get
		{
			var Vis = "Collapsed";

			if ((BoardName == (BASE_BOARD_NAME + "1")) || (BoardName == (BASE_BOARD_NAME + "2")) || (BoardName == (BASE_BOARD_NAME + "3")))
				Vis = "Visible";

			return Vis;
		}
	}

	public void SaveDispatchFilters(List<DispatchFilter> filters)
	{
		//if( filters.Count > 0 )
		//{
		// Update dictionaryFilters
		if (DictionaryFilters.ContainsKey(BoardName))
			DictionaryFilters[BoardName] = filters;
		else
			DictionaryFilters.Add(BoardName, filters);

		List<string> Dfs = new();

		foreach (var DispatchFilter in filters)
		{
			var Converted = DispatchFilter.ToString();
			Dfs.Add(Converted);
		}

		FiltersSettings Fs = new()
		{
			Filters = Dfs,
			OnlyShowTripsForThisBoard = OnlyShowTripsForThisBoard
		};

		DispatchBoardSettings ??= new Settings();

		if (DispatchBoardSettings.DispatchFilterSettings.ContainsKey(BoardName))
			DispatchBoardSettings.DispatchFilterSettings.Remove(BoardName);
		DispatchBoardSettings.DispatchFilterSettings.Add(BoardName, Fs);

		SaveSettings(DispatchBoardSettings);
		//}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="value1">
	/// </param>
	/// <param
	///     name="condition">
	/// </param>
	/// <param
	///     name="value2">
	/// </param>
	/// <returns></returns>
	private static bool DoesStringMatch(string value1, string condition, string value2)
	{
		var Yes = false;

		value1 = value1.TrimToLower();
		value2 = value2.TrimToLower();

		if (condition == EQUALS)
			Yes = value1 == value2;
		else if (condition == NOT_EQUAL_TO)
			Yes = value1 != value2;
		else if (condition == IN)
		{
			var Pieces = value2.Split(IN_CONDITION_LIST_DELIMITER);

			if (Pieces.Any(piece => piece.Trim() == value1))
				Yes = true;
		}
		else if (condition == CONTAINS)
		{
			if (value1.IndexOf(value2, StringComparison.Ordinal) > -1)
				Yes = true;
		}
		else if (condition == STARTS_WITH)
		{
			if (value1.StartsWith(value2))
				Yes = true;
		}
		else if (condition == ENDS_WITH)
		{
			if (value1.EndsWith(value2))
				Yes = true;
		}

		return Yes;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="value1">
	/// </param>
	/// <param
	///     name="condition">
	/// </param>
	/// <param
	///     name="value2">
	/// </param>
	/// <returns></returns>
	private static bool DoesDecimalMatch(decimal value1, string condition, string value2)
	{
		var Yes = false;

		try
		{
			var Val = decimal.Parse(value2);

			Yes = condition switch
			{
				"=" => value1 == Val,
				"!=" => value1 != Val,
				">" => value1 > Val,
				"<" => value1 < Val,
				_ => Yes
			};
		}
		catch (Exception E)
		{
			Logging.WriteLogLine("Match failed: " + E);
		}

		return Yes;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="column">
	/// </param>
	/// <returns></returns>
	public bool IsColumnNumeric(string column)
	{
		var Yes = false;

		var Field = MapColumnToTripField(column);

		return Field.ToLower() switch
		{
			"pieces" => true,
			"weight" => true,
			"board" => true,
			_ => Yes
		};
	}

	public DispatchFilter? LookUpFilter(string colName)
	{
		DispatchFilter? Df = null;

		if ((DictionaryFilters.Count > 0) && DictionaryFilters.ContainsKey(BoardName))
		{
			foreach (var Tmp in DictionaryFilters[BoardName])
			{
				if (Tmp.ColumnName == colName)
				{
					Logging.WriteLogLine("Found matching filter: " + Tmp);
					Df = Tmp;
					break;
				}
			}
		}

		return Df;
	}
	#endregion

	#region Settings Other
	public bool DisplayNotesOnOneLine
	{
		get { return Get(() => DisplayNotesOnOneLine, false); }
		set { Set(() => DisplayNotesOnOneLine, value); }
	}


	//[DependsUpon( nameof( DisplayNotesOnOneLine ) )]
	public string GetTextWrapForNotes
	{
		get
		{
			var TextWrap = "Wrap";

			if (DisplayNotesOnOneLine)
				TextWrap = "NoWrap";

			return TextWrap;
		}

		//set { Set(() => GetTextWrapForNotes, value); }
	}
	#endregion

	#region Update Conditions Timer
	private DispatcherTimer? UpdateConditionsTimer;

	public bool IsUpdateConditionsRunning() => UpdateConditionsTimer is not null && UpdateConditionsTimer.IsEnabled;

	/// <summary>
	///     Stops the update addresses timer.
	/// </summary>
	public void StopUpdateConditions()
	{
		if (UpdateConditionsTimer != null)
		{
			Logging.WriteLogLine("Stopping timer for Board " + BoardName);
			UpdateConditionsTimer.Stop();
		}
	}

	/// <summary>
	///     Starts the update conditions timer.
	/// </summary>
	public void StartUpdateConditions()
	{
		Logging.WriteLogLine("Starting timer for Board " + BoardName);

		// var updateTripsDelegate = new TimerCallback(UpdateTripsChecker);
		// var updateTripsChecker = new UpdateTripsChecker();

		// Run every 15 seconds
		// timerUpdateTrips = new Timer(updateTripsChecker.UpdateTrips, null, 0, 15 * 1000);

		UpdateConditionsTimer = new DispatcherTimer();
		UpdateConditionsTimer.Tick += UpdateConditions_Tick;
		UpdateConditionsTimer.Interval = new TimeSpan(0, 1, 0); // Every minute
																//UpdateConditionsTimer.Interval = new TimeSpan( 0, 0, 5 );
																//UpdateConditionsTimer.Interval = new TimeSpan( 0, 0, 15 );
																//UpdateConditions_Tick(null, null);
		UpdateConditionsTimer.Start();
	}

    /// <summary>
    /// </summary>
    /// <param
    ///     name="sender">
    /// </param>
    /// <param
    ///     name="e">
    /// </param>
    public void UpdateConditions_Tick(object? sender, EventArgs? e)
    {

        //if( !IsEditingDeliveryNote && !IsEditingPickupNote )
        if (!IsEditingDeliveryNote && !IsEditingPickupNote && !IsDriverSelectionOpen)
        {
            //Logging.WriteLogLine("DEBUG UpdateConditions fired: BoardName " + BoardName + " Trips.Count: " + Trips.Count);
            SaveSelectedTrips();
            if (View != null)
            {
                Dispatcher.Invoke(() =>
                                   {
                                       //View.DataGrid.Visibility = Visibility.Hidden;
                                       SortByCurrentColumn();
                                       //UpdateBackgrounds();
                                   });
            }
            var NewTrips = new ObservableCollection<DisplayTrip>();

            var IsOdd = false;
			List<DisplayTrip> trips = (from T in Trips where IsTripForThisBoard(T) select T).ToList();
			if (trips != null && trips.Count > 0)
			{
                Logging.WriteLogLine("DEBUG UpdateConditions fired: BoardName " + BoardName + " Trips.Count: " + trips.Count);
				foreach (var Trip in trips)
				{
					//Logging.WriteLogLine( "Setting condition Board: " + BoardName + " for trip: " + Trip.TripId + " isOdd: " + IsOdd + " Trip.IsVisible" + Trip.IsVisible );
					Trip.ReadyTimeCondition.BuildCondition(IsOdd);

					if (Trip.IsVisible && IsTripForThisBoard(Trip))
					{
						IsOdd = !IsOdd;
						// This works, but kills the service level colouring
						//Trip.UnSelectedBackground = isOdd ? Brushes.LightGray : Brushes.White;
						var Sl = GetServiceLevel(Trip.ServiceLevel);

						if (Sl.Background.ToString() != Brushes.White.ToString())
						{
							Trip.UnSelectedBackground = Sl.Background;
							Trip.UnSelectedForeground = Sl.Foreground;
						}
						else
						{
							Trip.UnSelectedBackground = IsOdd ? Brushes.White : Brushes.LightGray;
							//Trip.UnSelectedBackground = isOdd ? Brushes.LightGray : Brushes.White;
						}
					}

					NewTrips.Add(Trip);
				}
			}

            //NewTrips = SortByCurrentColumn(NewTrips);
            //SortByCurrentColumn(NewTrips);

            Dispatcher.Invoke(() =>
                               {
                                   //Trips = NewTrips;
                                   Trips.Clear();

                                   foreach (var Trip in NewTrips)
                                       Trips.Add(Trip);

                                   // Testing
                                   //SortByCurrentColumn();

                                   //if (View != null)
                                   //{
                                   // View.DataGrid.Visibility = Visibility.Visible;
                                   // View.DataGrid.InvalidateVisual();
                                   //}

                               });
            RestoreSelectedTrips();
        }
        //SortByCurrentColumn(Trips);

        //SortByCurrentColumn();

        //View?.PerformSort();
    }

    public void UpdateConditions_Tick_V1(object? sender, EventArgs? e)
	{

		//if( !IsEditingDeliveryNote && !IsEditingPickupNote )
		if (!IsEditingDeliveryNote && !IsEditingPickupNote && !IsDriverSelectionOpen)
		{
			Logging.WriteLogLine("DEBUG UpdateConditions fired: BoardName " + BoardName + " Trips.Count: " + Trips.Count);
			SaveSelectedTrips();
			if (View != null)
			{
				Dispatcher.Invoke(() =>
								   {
									   //View.DataGrid.Visibility = Visibility.Hidden;
									   SortByCurrentColumn();
									   //UpdateBackgrounds();
								   });
			}
			var NewTrips = new ObservableCollection<DisplayTrip>();

			var IsOdd = false;

			foreach (var Trip in Trips)
			{
				//Logging.WriteLogLine( "Setting condition for trip: " + Trip.TripId + " isOdd: " + isOdd + "Trip.IsVisible" + Trip.IsVisible );
				Trip.ReadyTimeCondition.BuildCondition(IsOdd);

				if (Trip.IsVisible && IsTripForThisBoard(Trip))
				{
					IsOdd = !IsOdd;
					// This works, but kills the service level colouring
					//Trip.UnSelectedBackground = isOdd ? Brushes.LightGray : Brushes.White;
					var Sl = GetServiceLevel(Trip.ServiceLevel);

					if (Sl.Background.ToString() != Brushes.White.ToString())
					{
						Trip.UnSelectedBackground = Sl.Background;
						Trip.UnSelectedForeground = Sl.Foreground;
					}
					else
					{
						Trip.UnSelectedBackground = IsOdd ? Brushes.White : Brushes.LightGray;
						//Trip.UnSelectedBackground = isOdd ? Brushes.LightGray : Brushes.White;
					}
				}

				NewTrips.Add(Trip);
			}
			//NewTrips = SortByCurrentColumn(NewTrips);
			//SortByCurrentColumn(NewTrips);

			Dispatcher.Invoke(() =>
							   {
								   //Trips = NewTrips;
								   Trips.Clear();

								   foreach (var Trip in NewTrips)
									   Trips.Add(Trip);

								   // Testing
								   //SortByCurrentColumn();

								   //if (View != null)
								   //{
									  // View.DataGrid.Visibility = Visibility.Visible;
									  // View.DataGrid.InvalidateVisual();
								   //}

							   });
			RestoreSelectedTrips();
		}
		//SortByCurrentColumn(Trips);

		//SortByCurrentColumn();

		//View?.PerformSort();
	}

	public List<string> SavedSelectedTrips { get; set; } = new();

	private void SaveSelectedTrips()
	{
		if (SelectedTrips != null && SelectedTrips.Count > 0)
		{
			string ids = string.Empty;
			SavedSelectedTrips.Clear();
			//SavedSelectedTrips.AddRange(SelectedTrips);
			foreach (var Trip in SelectedTrips)
			{
				SavedSelectedTrips.Add(Trip.TripId);
				ids += Trip.TripId + ", ";
			}
			Logging.WriteLogLine("DEBUG SavedSelectedTrips: " + ids);
		}
	}

	private void RestoreSelectedTrips()
	{
		Dispatcher.Invoke(() =>
		{
			SelectedTrips = new();
			if (SavedSelectedTrips.Count > 0 && View != null)
			{
				//View.SetSelectedTrips(SavedSelectedTrips);
				foreach (string id in SavedSelectedTrips)
				{
					var trip = (from T in Trips where T.TripId == id select T).FirstOrDefault();
					if (trip != null)
					{
						trip.Selected = true;
						SelectedTrips.Add(trip);
					}
				}
			}
            //SavedSelectedTrips.Clear();
        });
	}
#endregion
}