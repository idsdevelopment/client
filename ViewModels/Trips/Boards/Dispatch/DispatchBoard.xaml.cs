﻿#nullable enable

using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using ViewModels.Trips.Boards.Common;

// ReSharper disable UnusedParameter.Local

namespace ViewModels.Trips.Boards;

/// <summary>
///     Interaction logic for Dispatch.xaml
/// </summary>
public partial class DispatchBoard : Page, IDisposable
{
	private DisplayTrip?      CurrentTrip;
	private DispatchViewModel Model = null!;
	private bool              WasKeyPress;

	private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	private static void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	private static void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}

	public void Dispose()
	{
		Mouse.OverrideCursor = null;
		try
		{
			if ( Model != null )
			{
                Logging.WriteLogLine($"Disposing of {Model.BoardName}");
                Model.StopUpdateConditions();
				Model.Dispose();
			}
		}
		catch { }
	}

#region Dispatch
	//private DisplayTrip? FakeClickTrip { get; set; } = null;


	private void DataGrid_PreviewMouseDown( object? sender, MouseButtonEventArgs? e )
	{
		//if (FakeClickTrip != null)
		//{
		//	DataGrid.SelectedIndex = 0;
		//	DataGrid.SelectedItem = FakeClickTrip;
		//	FakeClickTrip = null;
		//}
		//else
		//{
		//	if (e != null)
		//	{
		//		e.Handled = false;
		//	}
		//}
	}


	private void DataGrid_BeginningEdit( object sender, DataGridBeginningEditEventArgs e )
	{
		// Only open if click is on first column
		if( e.Column.DisplayIndex == 0 )
		{
			// End the edit to allow the cell to be redrawn properly
			e.Cancel = true;
			Logging.WriteLogLine( "DEBUG Loading AssignDriver dialog" );

			var NewDriver     = SelectDriverForMultipleTrips();
			var SelectedTrips = Model.SelectedTrips;

			if( NewDriver.IsNotNullOrWhiteSpace() && ( SelectedTrips != null ) )
			{
				foreach( var Trip in SelectedTrips )
				{
					//Logging.WriteLogLine("TripId: " + Trip.TripId + " - driver: " + Trip.Driver);

					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - driver was: " + Trip.Driver + " - setting to " + NewDriver );
					Trip.Driver       = NewDriver;
					Trip.EnableAccept = true;
				}
			}
		}
	}


	public T? FindVisualChild<T>( DependencyObject? obj ) where T : DependencyObject
	{
		for( var I = 0; I < VisualTreeHelper.GetChildrenCount( obj ); I++ )
		{
			var DependencyObject = VisualTreeHelper.GetChild( obj, I );

			if( DependencyObject is T Child )
				return Child;
			var ChildOfChild = FindVisualChild<T>( DependencyObject );

			if( ChildOfChild is not null )
				return ChildOfChild;
		}
		return null;
	}


	public DataGridCell? GetCell( DataGrid dataGrid, DataGridRow? rowContainer, int column )
	{
		if( rowContainer != null )
		{
			var Presenter = FindVisualChild<DataGridCellsPresenter>( rowContainer );

			if( Presenter == null )
			{
				/* if the row has been virtualized away, call its ApplyTemplate() method
				 * to build its visual tree in order for the DataGridCellsPresenter
				 * and the DataGridCells to be created */
				rowContainer.ApplyTemplate();
				Presenter = FindVisualChild<DataGridCellsPresenter>( rowContainer );
			}

			if( Presenter != null )
			{
				DataGridCell? Cell = null;

				if( Presenter.ItemContainerGenerator.ContainerFromIndex( column ) is not DataGridCell )
				{
					/* bring the column into view
					 * in case it has been virtualized away */
					dataGrid.ScrollIntoView( rowContainer, dataGrid.Columns[ column ] );
					Cell = Presenter.ItemContainerGenerator.ContainerFromIndex( column ) as DataGridCell;
				}
				return Cell;
			}
		}
		return null;
	}


	private void DataGrid_CellEditEnding( object sender, DataGridCellEditEndingEventArgs e )
	{
		////DataGrid.CommitEdit();
		////DataGrid.CommitEdit();
		//DataGrid.CancelEdit();
		//DataGrid.CancelEdit();
		//      var View = CollectionViewSource.GetDefaultView(DataGrid.ItemsSource);
		//View.Refresh();

		//DataGrid_SelectionChanged(null, null);
		//DataGrid.InvalidateVisual();

		// Apply the driver to all selected trips
		//var SelectedTrips = Model.SelectedTrips;

		//if( SelectedTrips is {Count: > 0} )
		//{
		//	var Driver = string.Empty;

		//	foreach( var Trip in SelectedTrips )
		//	{
		//		Logging.WriteLogLine( "TripId: " + Trip.TripId + " - driver: " + Trip.Driver );

		//		if( Trip.Driver.IsNotNullOrWhiteSpace() )
		//		{
		//			Driver = Trip.Driver;
		//			Logging.WriteLogLine( "Setting trip's driver to " + Driver );
		//			break;
		//		}
		//	}

		//	foreach( var Trip in SelectedTrips )
		//	{
		//		Trip.Driver       = Driver;
		//		Trip.EnableAccept = true;
		//	}
		//}		
	}


	private void DataGrid_PreparingCellForEdit( object sender, DataGridPreparingCellForEditEventArgs e )
	{
		// Apply the driver to all selected trips
		//var SelectedTrips = Model.SelectedTrips;
		//if( e.Column.DisplayIndex == 0 )
		//{
		//	var StackPanel = e.EditingElement.FindChild<StackPanel>();

		//	if( StackPanel is not null )
		//	{
		//		DriverCombo.RemoveParent();
		//		StackPanel.Children.Add( DriverCombo );
		//		DriverCombo.DataContext = Model;
		//		DriverCombo.Visibility  = Visibility.Visible;
		//		DriverCombo.UpdateLayout();
		//		var TextBox = DriverCombo.FindChild<TextBox>( "PART_EditableTextBox" );

		//		if( TextBox is { } T && CurrentTrip is { } Ct )
		//		{
		//			T.Text = Ct.Driver;
		//			T.Focus();
		//		}
		//	}
		//}
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		if( Model.SelectedTrips is {Count: > 0} St && Model.Trips is { } Trips )
		{
			var Ndx  = 0;
			var Trip = St[ 0 ];

			foreach( var ModelTrip in Trips )
			{
				if( Trip == ModelTrip )
				{
					var Cell = DataGrid.GetCell( Ndx, 0 );

					if( Cell is { } C )
					{
						C.Focus();
						DataGrid.BeginEdit();
						break;
					}
				}
				++Ndx;
			}

			//var Trip = SelectedTrips[ Count - 1 ];

			//var Ndx = 0;

			//foreach( var ModelTrip in Model.Trips )
			//{
			//	if( Trip == ModelTrip )
			//	{
			//		var Cell = DataGrid.GetCell( Ndx, 0 );
			//		if (!(Cell is null))
			//                       {
			//			Cell.Focus();
			//			DataGrid.BeginEdit();
			//                       }
			//	}

			//	++Ndx;
			//}
		}
	}

	private int SelectedRow = -1;


	private void DataGrid_SelectedCellsChanged( object sender, SelectedCellsChangedEventArgs e )
	{
		if( ( e.AddedCells.Count > 0 ) && e.AddedCells[ 0 ].Item is DisplayTrip Trip )
			CurrentTrip = Trip;

		if( sender is DataGrid Dg )
			SelectedRow = Dg.SelectedIndex;
	}

	private void DataGrid_SelectionChanged( object? sender, SelectionChangedEventArgs? e )
	{
		if( sender is DataGrid Grid )
		{
			var Trips = Model.SelectedTrips;

			if( Trips is null )
				Trips = new List<DisplayTrip>();
			else
				Trips.Clear();

			Model.SelectedTrips = null;
			Trips.AddRange( Grid.SelectedItems.Cast<DisplayTrip>() );
			Model.SelectedTrips = Trips;
		}
	}

	private void ToggleButton_Checked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var DGrid = Tb.FindParent<DataGrid>();

			if( DGrid is not null )
			{
				var Ndx = DGrid.SelectedIndex;

				if( Ndx >= 0 )
				{
					var Itm = (DisplayTrip)DGrid.SelectedItem;
					Itm.DetailsVisible = true;

					var Row = DGrid.GetRow( Ndx );

					if( Row is not null )
						DGrid.ScrollIntoView( Row );
				}
			}
		}
	}

	private void ToggleButton_Unchecked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var Grid = Tb.FindParent<DataGrid>();

			if( Grid?.SelectedItem is DisplayTrip Itm )
				Itm.DetailsVisible = false;
		}
	}

	public DispatchBoard()
	{
		SetWaitCursor();

		Initialized += ( _, _ ) =>
					   {
						   if( DataContext is DispatchViewModel M )
						   {
							   Model      = M;
							   Model.View = this;
						   }

						   void DoSelectionChanged()
						   {
							   WasKeyPress = false;

							   if( DriverCombo.Visibility == Visibility.Visible )
							   {
								   if( DriverCombo.SelectedItem is Driver D && CurrentTrip is { } Ct )
								   {
									   Ct.Driver = D.StaffId.Trim();
									   DataGrid.CancelEdit();
									   Model.SelectedTrip = Ct;
									   Model.WhenSelectedTripChanges();
								   }
							   }
						   }

						   if( DriverCombo is not null )
						   {
							   DriverCombo.PreviewKeyDown += ( _, eventArgs ) =>
														     {
															     var Key = eventArgs.Key;

															     switch( Key )
															     {
															     case Key.Return:
															     case Key.Tab:
																     {
																	     WasKeyPress = false;
																	     var Txt = DriverCombo.Text;

																	     if( DriverCombo.ItemsSource is ObservableCollection<Driver> Drivers )
																	     {
																		     foreach( var Driver in Drivers )
																		     {
																			     if( Driver.StaffId == Txt )
																			     {
																				     DriverCombo.SelectedItem = Driver;
																				     DoSelectionChanged();
																				     break;
																			     }
																		     }
																	     }
																     }
																     break;

															     default:
																     WasKeyPress = true;
																     break;
															     }
														     };

							   DriverCombo.SelectionChanged += ( _, _ ) =>
															   {
																   if( !WasKeyPress )
																	   DoSelectionChanged();
																   else
																	   WasKeyPress = false;
															   };

							   DriverCombo.LostFocus += ( _, _ ) =>
													    {
														    DoSelectionChanged();
													    };

							   DriverCombo.DropDownClosed += ( _, _ ) =>
														     {
															     DoSelectionChanged();
														     };
						   }

						   //DriversPopup.OnSelectionChanged += ( o, s ) =>
						   //                                   {
						   //                                    try
						   //                                    {
						   //                                     var Driver = s.Trim();

						   //                                     foreach( var Trip in Model.SelectedTrips )
						   //                                     {
						   //                                      Trip.Driver = Driver;
						   //                                      Trip.EnableAccept = true;
						   //                                     }

						   //                                     DriversPopup.IsOpen = false;
						   //                                     DataGrid.CancelEdit();
						   //                                     Model.SelectedTrip = CurrentTrip;
						   //                                    }
						   //                                    catch( Exception Exception )
						   //                                    {
						   //                                     Console.WriteLine( Exception );
						   //                                    }
						   //                                   };

						   if( !Model.IsUpdateConditionsRunning() )
						   {
							   Model.UpdateConditions_Tick( null, null );
							   Model.StartUpdateConditions();
						   }
					   };
		InitializeComponent();

		var Columns = Model.GetAllDispatchColumnsInOrder();

		foreach( var Col in DataGrid.Columns )
		{
			foreach( var Key in Columns )
			{
				var Header = FindStringResource( Key );

				if( Col.Header.ToString() == Header )
				{
					Col.Visibility = Model.DictionaryColumnVisibility[ Key ];
					break;
				}
			}
		}

		// Load the settings tab
		PopulateSettingsColumnList();
		PopulateFilterList();
		PopulateOtherSettings();

		ClearWaitCursor();
	}

	private void DataGrid_MouseDoubleClick( object? sender, MouseButtonEventArgs? e )
	{
		DisplayTrip? SelectedTrip = null;

		if( DataGrid.CurrentCell.Column?.DisplayIndex > 0 )
			SelectedTrip = (DisplayTrip?)DataGrid.SelectedItem;
		else
			Logging.WriteLogLine( "DEBUG NOT opening trip - double-click is on driver column" );

		if( SelectedTrip is not null )
		{
			Logging.WriteLogLine( "DEBUG Selected trip: " + SelectedTrip?.TripId + " driver: " + SelectedTrip?.Driver );

			Logging.WriteLogLine( "Opening new " + Globals.TRIP_ENTRY + " tab" );
			Globals.RunNewProgram( Globals.TRIP_ENTRY, SelectedTrip );
		}
	}


	private void HelpButton_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is DispatchViewModel ViewModel )
		{
			Logging.WriteLogLine( "Opening " + ViewModel.HelpUri );
			var Uri = new Uri( ViewModel.HelpUri );
			Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
		}
	}

	private void MenuItem_ShowToolbar( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void SettingsButton_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Settings tab visible" );
		tabItemDispatch.Visibility = Visibility.Visible;
		tabItemSettings.Visibility = Visibility.Visible;
		tabItemSettings.Focus();
	}

	private void MiRouteShipments_Click( object sender, RoutedEventArgs e )
	{
		BtnRouteShipments_Click( null, null );
	}

	private void BtnRouteShipments_Click( object? sender, RoutedEventArgs? e )
	{
		if( DataContext is DispatchViewModel ViewModel )
		{
			SetWaitCursor();

			ViewModel.Execute_RouteShipments();

			ClearWaitCursor();
		}
	}

	private DisplayTrip? TripBeingEdited { get; set; }

	private string OldDeliveryNotes = string.Empty;

	private void BtnEditDeliveryNote_Click( object? sender, RoutedEventArgs? e )
	{
		if( Model.SelectedTrips is {Count: > 0} St )
		{
			var St0 = St[ 0 ];

			if( St0.TripId.IsNotNullOrWhiteSpace() )
			{
				Model.IsEditingDeliveryNote = true;
				TbDeliveryNote.IsReadOnly   = false;
				TbDeliveryNote.Focus();
				BtnCancelEditDeliveryNote.Visibility = BtnSaveDeliveryNote.Visibility = Visibility.Visible;
				BtnEditDeliveryNote.Visibility       = Visibility.Collapsed;
				BtnEditPickupNote.Visibility         = Visibility.Hidden;

				//OldDeliveryNotes = Model.SelectedTrips[0].DeliveryNotes;
				OldDeliveryNotes = TbDeliveryNote.Text;
				TripBeingEdited  = St0;
			}
		}
	}

	private void TbDeliveryNote_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		BtnEditDeliveryNote_Click( null, null );
	}

	private void BtnSaveDeliveryNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingDeliveryNote          = false;
		TbDeliveryNote.IsReadOnly            = true;
		BtnCancelEditDeliveryNote.Visibility = BtnSaveDeliveryNote.Visibility = Visibility.Collapsed;
		BtnEditDeliveryNote.Visibility       = Visibility.Visible;
		BtnEditPickupNote.Visibility         = Visibility.Visible;

		//Model.SelectedTrips[0].DeliveryNotes        = TbDeliveryNote.Text.Trim();
		if( TripBeingEdited is { } Tbe )
		{
			Tbe.DeliveryNotes = TbDeliveryNote.Text.Trim();
			//Model.SelectedTrips[0].DisplayDeliveryNotes = Model.DisplayNotesOnOneLine ? TbDeliveryNote.Text.Trim().Replace("\r\n", " ") : TbDeliveryNote.Text.Trim();
			Tbe.DisplayDeliveryNotes = Model.DisplayNotesOnOneLine ? TbDeliveryNote.Text.Trim().Replace( "\r", " " ).Replace( "\n", " " ) : TbDeliveryNote.Text.Trim();
			//SaveTrip(Model.SelectedTrips[0]);
			SaveTrip( Tbe );
			OldDeliveryNotes = string.Empty;
		}
		TripBeingEdited = null;
	}

	private void BtnCancelEditDeliveryNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingDeliveryNote          = false;
		TbDeliveryNote.IsReadOnly            = true;
		BtnCancelEditDeliveryNote.Visibility = BtnSaveDeliveryNote.Visibility = Visibility.Collapsed;
		BtnEditDeliveryNote.Visibility       = Visibility.Visible;
		BtnEditPickupNote.Visibility         = Visibility.Visible;

		if( Model.SelectedTrips is {Count: > 0} St )
			TbDeliveryNote.Text = St[ 0 ].DeliveryNotes = OldDeliveryNotes;
		OldDeliveryNotes = string.Empty;
		TripBeingEdited  = null;
	}

	private string OldPickupNotes = string.Empty;

	private void BtnEditPickupNote_Click( object? sender, RoutedEventArgs? e )
	{
		if( Model.SelectedTrips is {Count: > 0} St )
		{
			var St0 = St[ 0 ];

			if( St0.TripId.IsNotNullOrWhiteSpace() )
			{
				Model.IsEditingPickupNote = true;
				TbPickupNote.IsReadOnly   = false;
				TbPickupNote.Focus();
				BtnCancelEditPickupNote.Visibility = BtnSavePickupNote.Visibility = Visibility.Visible;
				BtnEditPickupNote.Visibility       = Visibility.Collapsed;
				BtnEditDeliveryNote.Visibility     = Visibility.Hidden;

				//OldPickupNotes = Model.SelectedTrips[0].PickupNotes;
				OldPickupNotes  = TbPickupNote.Text;
				TripBeingEdited = St0;
			}
		}
	}

	private void TbPickupNote_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		BtnEditPickupNote_Click( null, null );
	}


	private void BtnSavePickupNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingPickupNote          = false;
		TbPickupNote.IsReadOnly            = true;
		BtnCancelEditPickupNote.Visibility = BtnSavePickupNote.Visibility = Visibility.Collapsed;
		BtnEditPickupNote.Visibility       = Visibility.Visible;
		BtnEditDeliveryNote.Visibility     = Visibility.Visible;

		if( Model.SelectedTrips is not null )
		{
			Model.SelectedTrips[ 0 ].PickupNotes = TbPickupNote.Text.Trim();
			////TripBeingEdited.PickupNotes = TbPickupNote.Text.Trim();
			Model.SelectedTrips[ 0 ].DisplayPickupNotes = Model.DisplayNotesOnOneLine ? TbPickupNote.Text.Trim().Replace( "\r", " " ).Replace( "\n", " " ) : TbPickupNote.Text.Trim();
			////TripBeingEdited.DisplayPickupNotes = Model.DisplayNotesOnOneLine ? TbPickupNote.Text.Trim().Replace("\r", " ").Replace("\n", " ") : TbPickupNote.Text.Trim();
			SaveTrip( Model.SelectedTrips[ 0 ] );
		}
		////SaveTrip(TripBeingEdited);
		OldPickupNotes  = string.Empty;
		TripBeingEdited = null;
	}

	private void BtnCancelEditPickupNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingPickupNote          = false;
		TbPickupNote.IsReadOnly            = true;
		BtnCancelEditPickupNote.Visibility = BtnSavePickupNote.Visibility = Visibility.Collapsed;
		BtnEditPickupNote.Visibility       = Visibility.Visible;
		BtnEditDeliveryNote.Visibility     = Visibility.Visible;

		if( Model.SelectedTrips is {Count: > 0} St )
			TbPickupNote.Text = St[ 0 ].PickupNotes = OldPickupNotes;
		OldPickupNotes  = string.Empty;
		TripBeingEdited = null;
	}

	private void SaveTrip( DisplayTrip _ )
	{
		if( Model.SelectedTrips is {Count: > 0} St )
			DispatchViewModel.SaveTrip( St[ 0 ] );

		/*Model.SaveTrip(dt);

		for (int i = 0; i < Model.Trips.Count; i++)
        {
			if (Model.Trips[i].TripId == dt.TripId)
            {
				Model.Trips[i].DeliveryNotes = dt.DeliveryNotes;
				Model.Trips[i].PickupNotes = dt.PickupNotes;
				break;
            }
        }

		Model.WhenTripsChange();  // Force an update
        Model.SortByCurrentColumn();*/
		//Model.SortByCurrentColumn();
	}

#region Boards
	private void MiBoardClear_Click( object sender, RoutedEventArgs e )
	{
		if( Model.SelectedTrips is {Count: > 0} )
		{
			Logging.WriteLogLine( "Asking to clear board assignment " + Model.SelectedTrips[ 0 ].Board + " for " + Model.SelectedTrips.Count + " trips" );
			var Caption   = FindStringResource( "DispatchClearBoardTitle" );
			var Message = FindStringResource( "DispatchClearBoardMessage" );
			Message =  Message.Replace( "@1", " " + Model.SelectedTrips[ 0 ].Board + " " );
			Message =  Message.Replace( "@2", " " + Model.SelectedTrips.Count + " " );
			Message += "\n" + FindStringResource( "DispatchAssignToBoardMessage1" );

			var Mbr = MessageBox.Show( Message, Caption, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

			if( Mbr == MessageBoxResult.Yes )
				Model.ClearBoard();
			else
				Logging.WriteLogLine( "Clearing board assignment cancelled" );
		}
	}

	private void MiBoard1_Click( object sender, RoutedEventArgs e )
	{
		if( Model.SelectedTrips is {Count: > 0} )
		{
			var Proceed = AskToConfirmBoardAssign( FindStringResource( "DispatchAssignToBoard1" ), Model.SelectedTrips.Count );

			if( Proceed )
				Model.AssignToBoard( "1" );
			else
				Logging.WriteLogLine( "Moving trips to board 1 cancelled" );
		}
	}

	private void MiBoard2_Click( object sender, RoutedEventArgs e )
	{
		if( Model.SelectedTrips is {Count: > 0} )
		{
			var Proceed = AskToConfirmBoardAssign( FindStringResource( "DispatchAssignToBoard2" ), Model.SelectedTrips.Count );

			if( Proceed )
				Model.AssignToBoard( "2" );
			else
				Logging.WriteLogLine( "Moving trips to board 2 cancelled" );
		}
	}

	private void MiBoard3_Click( object sender, RoutedEventArgs e )
	{
		if( Model.SelectedTrips is {Count: > 0} )
		{
			var Proceed = AskToConfirmBoardAssign( FindStringResource( "DispatchAssignToBoard3" ), Model.SelectedTrips.Count );

			if( Proceed )
				Model.AssignToBoard( "3" );
			else
				Logging.WriteLogLine( "Moving trips to board 3 cancelled" );
		}
	}

	private bool AskToConfirmBoardAssign( string boardName, int count )
	{
		var Proceed = false;
		Logging.WriteLogLine( "Asking to move trips to " + boardName );
		var Title = FindStringResource( "DispatchAssignToBoardTitle" );
		Title = Title.Replace( "@1", boardName );
		var Message = FindStringResource( "DispatchAssignToBoardMessage" );
		Message =  Message.Replace( "@1", " " + count + " " );
		Message =  Message.Replace( "@2", boardName );
		Message += "\n" + FindStringResource( "DispatchAssignToBoardMessage1" );

		var Mbr = MessageBox.Show( Message, Title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

		if( Mbr == MessageBoxResult.Yes )
			Proceed = true;

		return Proceed;
	}

	private void BoardNoBoardButton_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening Board New" );
		OpenBoard( "99" );
	}


	private void Board1Button_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening Board 1" );
		OpenBoard( "1" );
	}

	private void Board2Button_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening Board 2" );
		OpenBoard( "2" );
	}

	private void Board3Button_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening Board 3" );
		OpenBoard( "3" );
	}

	private static void OpenBoard( string boardNumber )
	{
		var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( Tis.Count > 0 )
		{
			var          TargetBoardName = DispatchViewModel.BASE_BOARD_NAME + boardNumber;
			PageTabItem? Pti             = null;

			for( var I = 0; I < Tis.Count; I++ )
			{
				var Ti = Tis[ I ];

				if( Ti.Content is DispatchBoard )
				{
					if( Ti.Content.DataContext is DispatchViewModel Model )
					{
						if( Model.BoardName == TargetBoardName )
						{
							Pti = Ti;
							Logging.WriteLogLine( "Switching to extant " + Globals.DISPATCH_BOARD + " tab" );
							Globals.DataContext.MainDataContext.TabControl.SelectedIndex = I;

							break;
						}
					}
				}
			}

			if( Pti == null )
			{
				Logging.WriteLogLine( "Opening new " + TargetBoardName + " tab" );
				var (Tab, Page) = Globals.DataContext.MainDataContext.Execute_DispatchBoard();

				//if (Tab != null && Tab.Content is DispatchBoard)
				if( Tab is not null && Page is DispatchBoard && Page.DataContext is DispatchViewModel Model )
				{
					Model.OnArgumentChange( boardNumber );
					Globals.DataContext.MainDataContext.TabControl.SelectedIndex = Globals.DataContext.MainDataContext.TabControl.ItemsSource.Count;
					//if (Tab.Content.DataContext is DispatchViewModel model)
					//               {
					//                   model.OnArgumentChange(boardNumber);
					//                   Globals.DataContext.MainDataContext.TabControl.SelectedIndex = Globals.DataContext.MainDataContext.TabControl.ItemsSource.Count - 1;
					//               }
				}
				//Thread.Sleep(1000);
				//            tis = Globals.DataContext.MainDataContext.ProgramTabItems;
				//// Count down as new tab will be at end
				//for (var i = tis.Count - 1; i >= 0; i--)
				//{
				//	var ti = tis[i];
				//	if (ti.Content is DispatchBoard)
				//	{
				//		if (ti.Content.DataContext is DispatchViewModel model)
				//		{
				//			model.OnArgumentChange(boardNumber);
				//			Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;
				//			break;
				//		}
				//	}
				//}
			}
		}
	}


	private void Board1AssignButton_Click( object sender, RoutedEventArgs e )
	{
		MiBoard1_Click( sender, e );
	}

	private void Board2AssignButton_Click( object sender, RoutedEventArgs e )
	{
		MiBoard2_Click( sender, e );
	}

	private void Board3AssignButton_Click( object sender, RoutedEventArgs e )
	{
		MiBoard3_Click( sender, e );
	}

	private void BoardClearAssignButton_Click( object sender, RoutedEventArgs e )
	{
		MiBoardClear_Click( sender, e );
	}
#endregion

	private void MenuItem_Click_2( object sender, RoutedEventArgs e )
	{
		DataGrid_MouseDoubleClick( null, null );
	}

	//
	// From
	// https://stackoverflow.com/questions/6468488/retain-the-user-defined-sort-order-in-wpf-datagrid

	private void DataGrid_Sorting( object sender, DataGridSortingEventArgs e )
	{
		//SortDirection = (e.Column.SortDirection != ListSortDirection.Ascending) ?
		//						ListSortDirection.Ascending : ListSortDirection.Descending;

		//object column = e.Column.Header;
		AscendingSort = e.Column.SortDirection != ListSortDirection.Ascending;
		Logging.WriteLogLine( "DEBUG sorting AscendingSort: " + AscendingSort );

		// None of these perform the colouring correctly, although the Tick does work when it is triggered
		//Model.UpdateConditions_Tick(null, null);
		//Model.WhenTripsChange();
		//Model.UpdateBackgrounds();
		//SortInfos = GetSortInfo(DataGrid);
	}

	//public int LastColumnClicked { get; set; } = 0;
	public string LastColumnClicked { get; private set; } = "Shipment Id";
	public bool   AscendingSort     { get; private set; } = true;

	private void ColumnHeaderClicked( object sender, RoutedEventArgs e )
	{
		var ColumnHeader = (DataGridColumnHeader)sender;

		Logging.WriteLogLine( ColumnHeader.Content + " clicked" );

		if( (string)ColumnHeader.Content == LastColumnClicked )
			AscendingSort = !AscendingSort;
		else
			AscendingSort = true;
		Logging.WriteLogLine( "DEBUG sorting AscendingSort: " + AscendingSort );
		LastColumnClicked = (string)ColumnHeader.Content;

		Model.SortByCurrentColumn();
		Model.UpdateBackgrounds();
		e.Handled = true;

		//List<string> visibleCols = Model.GetVisibleColumns();
		//for (int i = 0; i < visibleCols.Count; i++)
		//            {
		//	if (visibleCols[i] == (string)columnHeader.Content)
		//                {
		//		Logging.WriteLogLine("DEBUG LastColumnClicked: " + LastColumnClicked);
		//		LastColumnClicked = i;
		//		break;
		//                }
		//            }
	}

	//public void PerformSort(int column)
	public void PerformSort()
	{
		//         MethodInfo performSortMethod = typeof(DataGrid).GetMethod("PerformSort", BindingFlags.Instance | BindingFlags.NonPublic);
		//Logging.WriteLogLine("DEBUG sorting on column " + column);
		//         performSortMethod.Invoke(DataGrid, new[] { DataGrid.Columns[column] });

		var View = CollectionViewSource.GetDefaultView( DataGrid.ItemsSource );
		View.SortDescriptions.Clear();
		//view.SortDescriptions.Add(new SortDescription("Shipment Id", ListSortDirection.Ascending));
		Logging.WriteLogLine( "DEBUG sorting on column " + LastColumnClicked );
		View.SortDescriptions.Add( new SortDescription( LastColumnClicked, ListSortDirection.Ascending ) );
		View.Refresh();
	}

	public ComboBox? CurrentDriverSelectionComboBox { get; private set; }

	private void DriverCombo_DropDownOpened( object sender, EventArgs e )
	{
		Logging.WriteLogLine( "DEBUG IsDriverSelectionDropdownOpen is true" );
		Model.IsDriverSelectionOpen = true;

		if( sender is ComboBox Cb )
			CurrentDriverSelectionComboBox = Cb;
	}

	private void DriverCombo_DropDownClosed( object sender, EventArgs e )
	{
		Logging.WriteLogLine( "DEBUG IsDriverSelectionDropdownOpen is false" );
		Model.IsDriverSelectionOpen = false;

		CurrentDriverSelectionComboBox = null;
		// In case trip updates have come in
		Model.RaisePropertyChanged( nameof( Model.Trips ) );
	}

	public void OpenCurrentDriverSelectionComboBox()
	{
		if( CurrentDriverSelectionComboBox != null )
		{
			DataGrid.SelectedIndex = SelectedRow;

			if( CurrentTrip != null )
				DataGrid.SelectedItem = CurrentTrip;

			Logging.WriteLogLine( "DEBUG Opening Current Driver Selection ComboBox" );
			CurrentDriverSelectionComboBox.IsDropDownOpen = true;
		}
	}

	private void TextBox_PreviewKeyDown( object sender, KeyEventArgs e )
	{
		if( e.Key == Key.Escape )
			Model.Filter = string.Empty;
	}

	private void SelectDriverForMultipleTrips_Click( object sender, RoutedEventArgs e )
	{
		// Grab them first
		//var SelectedTrips = Model.SelectedTrips;
		//List<string> ids = new();
		//if (Model.SelectedTrips != null)
		//{
		//	foreach (var trip in Model.SelectedTrips)
		//	{
		//		ids.Add(trip.TripId);
		//	}
		//}
		//if (SelectedTrips == null || SelectedTrips.Count == 0)
		//{
		//	SelectedTrips = (from T in Model.Trips where T.Selected select T).ToList();
		//}
		//List<string> ids = new();
		//      Logging.WriteLogLine("DEBUG SelectedTrips: " + SelectedTrips?.Count);
		//if (SelectedTrips != null)
		//{
		//	ids = (from T in SelectedTrips select T.TripId).ToList();
		//	foreach (var t in ids )
		//	{
		//              Logging.WriteLogLine("DEBUG TripId: " + t);
		//	}
		//}
		var NewDriver = SelectDriverForMultipleTrips();

		if( NewDriver.IsNotNullOrWhiteSpace() )
		{
			//if (ids.Count > 0)
			//{
			//	SelectedTrips = new();
			//	foreach (string id in ids)
			//	{
			//		DisplayTrip dt = (from T in Model.Trips where T.TripId== id select T).FirstOrDefault();
			//		if (dt != null)
			//		{
			//			SelectedTrips.Add(dt);
			//		}
			//	}
			//}
			// Apply the driver to all selected trips
			//Model.SelectedTrips = new();
			//if (ids is { Count: > 0 })
			if( Model.SelectedTrips is {Count: > 0} )
			{
				//foreach (string id in ids)
				//{
				//	var trip = (from T in Model.Trips where T.TripId == id select T).FirstOrDefault();
				//	if (trip != null)
				//	{
				//                    Logging.WriteLogLine("TripId: " + trip.TripId + " - driver was: " + trip.Driver + " - setting to " + newDriver);
				//		Model.SelectedTrips.Add(trip);
				//		trip.Driver = newDriver;
				//		trip.EnableAccept = true;
				//	}
				//}
				//Model.SelectedTrip = Model.SelectedTrips[0];

				foreach( var Trip in Model.SelectedTrips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - driver was: " + Trip.Driver + " - setting to " + NewDriver );
					Trip.Driver       = NewDriver;
					Trip.EnableAccept = true;
				}
			}
		}
		// Trying to prevent it being interpreted as a double-click
		e.Handled = true;
	}

	private string SelectDriverForMultipleTrips()
	{
		var Drivers = ( from D in Model.Drivers
					    orderby D.StaffId.ToLower()
					    select D.StaffId ).ToList();

		var Ids = GatherSelectedTrips();

		Model.IsDriverSelectionOpen = true;

		var NewDriver = new AssignDriver.AssignDriver( Application.Current.MainWindow ).ShowAssignDriver( Drivers );
		Model.IsDriverSelectionOpen = false;

		if( Ids.Count > 0 )
			SetSelectedTrips( Ids );

		return NewDriver;
	}

	public List<string> GatherSelectedTrips()
	{
		List<string> Ids = new();

		if( Model.SelectedTrips != null )
		{
			foreach( var Trip in Model.SelectedTrips )
				Ids.Add( Trip.TripId );
		}

		return Ids;
	}

	public void SetSelectedTrips( List<string>? ids )
	{
		if( ( ids != null ) && ( ids.Count > 0 ) )
		{
			// Clear selections
			foreach( var Trip in Model.Trips )
				Trip.Selected = false;
			Model.SelectedTrips = new List<DisplayTrip>();

			foreach( var Id in ids )
			{
				var Trip = ( from T in Model.Trips
						     where T.TripId == Id
						     select T ).FirstOrDefault();

				if( Trip != null )
				{
					Trip.Selected = true;
					Model.SelectedTrips.Add( Trip );
				}
			}
			//Model.SelectedTrip = Model.SelectedTrips[0];
		}
	}

	private void TextBlock_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
	{
		if( sender is TextBlock Tb )
		{
			Logging.WriteLogLine( "Copying TripId " + Tb.Text + " to clipboard" );
			Clipboard.SetText( Tb.Text );
		}
	}
#endregion

#region Settings
	private readonly List<Label>    FilterLabelList     = new();
	private readonly List<ComboBox> FilterConditionList = new();
	private readonly List<TextBox>  FilterValueList     = new();
	private readonly List<CheckBox> FilterIsActiveList  = new();

	private readonly List<CheckBox> CbColumns = new();
	//private Hashtable htColumns = new Hashtable();

	public void BtnSaveColumns_Click( object? sender, RoutedEventArgs? e )
	{
		//Logging.WriteLogLine("BtnSaveColumns clicked");
		var Columns    = Model.GetAllDispatchColumnsInOrder();
		var NewColumns = new List<string>();

		var I = 1; // Drivers column is always visible

		foreach( var Cb in CbColumns )
		{
			var Tmp = Cb.IsChecked ?? false;

			if( Tmp )
				NewColumns.Add( Columns[ I ] );
			++I;
		}

		Model.SaveDispatchColumns( NewColumns );

		foreach( var Col in DataGrid.Columns )
		{
			//Logging.WriteLogLine("DEBUG col.Header: " + col.Header);
			foreach( var Key in Columns )
			{
				if( ( Key != "DialoguesDispatchColumnDriver" ) && ( Key != "DialoguesDispatchColumnTripId" ) )
				{
					var Header = FindStringResource( Key );

					//Logging.WriteLogLine("DEBUG header: " + header);
					if( Col.Header.ToString() == Header )
					{
						Col.Visibility = Model.DictionaryColumnVisibility[ Key ];
						//Logging.WriteLogLine("DEBUG header: " + header + " Visibility: " + col.Visibility);
						break;
					}
				}
			}
		}

		//this.htColumns.Clear();
		PopulateSettingsColumnList();

		//this.AddColumns();

		tabControl.SelectedIndex = 0;
	}

	private void BtnSelectAll_Click( object sender, RoutedEventArgs e )
	{
		foreach( var Cb in CbColumns )
			Cb.IsChecked = true;
	}

	private void BtnSelectNone_Click( object sender, RoutedEventArgs e )
	{
		foreach( var Cb in CbColumns )
			Cb.IsChecked = false;
	}

	/// <summary>
	///     Only filters with something in the value field are saved.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	public void BtnApplyFilters_Click( object? sender, RoutedEventArgs? e )
	{
		Logging.WriteLogLine( "BtnApplyFilters clicked" );
		var List = new List<DispatchViewModel.DispatchFilter>();

		for( var I = 0; I < FilterIsActiveList.Count; I++ )
		{
			if( FilterValueList[ I ].Text.Trim().Length > 0 )
			{
				var Df = new DispatchViewModel.DispatchFilter( Model.BoardName,
															   FilterLabelList[ I ].Name,
															   (string)FilterConditionList[ I ].SelectedValue,
															   FilterValueList[ I ].Text.Trim(),
															   FilterIsActiveList[ I ].IsChecked ?? false );
				List.Add( Df );
			}
			else
			{
				// Make sure the active checkbox is turned off
				FilterIsActiveList[ I ].IsChecked = false;
			}
		}

		Model.SaveDispatchFilters( List );

		Dispatcher.Invoke( () =>
						   {
							   DataGrid.ItemsSource = null;

							   //Model.WhenTripsChange(Model.Trips);
							   Model.WhenTripsChange();

							   DataGrid.ItemsSource = Model.Trips;
						   } );

		//this.dgTrips.ItemsSource = null;

		//Model.Dispatch.SetFilters(this.windowName, list);

		//Model.Dispatch.RemoveDispatchWindowFromTrips(this.windowName);
		//Model.Dispatch.AddDispatchWindowToTrips(this.windowName);

		//try
		//{
		//	this.dgTrips.ItemsSource = Model.Dispatch.GetTripsForWindow(this.windowName);
		//}
		//catch (Exception exception)
		//{
		//	Logging.WriteLogLine("Exception: " + exception.ToString());
		//}

		tabControl.SelectedIndex = 0;
	}

	public void BtnSaveOtherSettings_Click( object? sender, RoutedEventArgs? e )
	{
		Logging.WriteLogLine( "BtnSaveOtherSettings clicked" );
		// TODO
		//UserSettings.SaveCollapseNotes(this.cbCollapseNotes.IsChecked);
		Model.SaveOtherSettings();

		//MainWindow.GetInstance().DispatchWindowsApplyCollapseNotes();
		tabControl.SelectedIndex = 0;
	}

	public void PopulateSettingsColumnList()
	{
		CbColumns.Clear();
		gridColumns.RowDefinitions.Clear();
		gridColumns.Children.Clear();

		//var currentCols = Model.Dispatch.GetSavedDispatchColumns(this.windowName);
		//var currentCols = Model.GetAllDispatchColumnsInOrder();
		//Model.LoadSettings();

		var Columns = Model.GetAllDispatchColumnsInOrder();

		if( Columns is {Count: > 0} )
		{
			gridColumns.RowDefinitions.Add( new RowDefinition() );

			var LabelVisible = new Label
							   {
								   FontWeight = FontWeights.Bold,
								   Content    = FindStringResource( "DialoguesDispatchLabelVisible" )
							   };

			gridColumns.Children.Add( LabelVisible );
			Grid.SetColumn( LabelVisible, 0 );
			Grid.SetRow( LabelVisible, 0 );

			var Row = 1;

			foreach( var Column in Columns )
			{
				gridColumns.RowDefinitions.Add( new RowDefinition() );

				var Resource = FindStringResource( Column );

				// These 2 columns must be visible
				//if (column != "DialoguesDispatchColumnDriver" && column != "DialoguesDispatchColumnTripId")
				if( Column != "DialoguesDispatchColumnDriver" )
				{
					var CheckBox = new CheckBox
								   {
									   Name    = Column,
									   Content = Resource
								   };
					gridColumns.Children.Add( CheckBox );
					Grid.SetColumn( CheckBox, 0 );
					Grid.SetRow( CheckBox, Row );

					if( Model.DictionaryColumnVisibility.ContainsKey( Column ) && ( Model.DictionaryColumnVisibility[ Column ] == Visibility.Visible ) )
						CheckBox.IsChecked = true;

					//if (currentCols.Contains(column))
					//{
					//	checkBox.IsChecked = true;
					//}

					CbColumns.Add( CheckBox );

					if( Column == "DialoguesDispatchColumnTripId" )
						CheckBox.Visibility = Visibility.Hidden;
					++Row;
				}
				//else
				//{
				//                   Label label = new Label { Content = "   " };
				//                   this.gridColumns.Children.Add(label);
				//	Grid.SetColumn(label, 0);
				//	Grid.SetRow(label, row);
				//}

				// ++row;
			}
		}
	}

	public void PopulateFilterList()
	{
		gridFilters.RowDefinitions.Clear();
		FilterLabelList.Clear();
		FilterConditionList.Clear();
		FilterValueList.Clear();
		FilterIsActiveList.Clear();

		var Columns = Model.GetAllDispatchColumnsInOrder();

		if( Columns is {Count: > 0} )
		{
			gridFilters.RowDefinitions.Add( new RowDefinition() );

			var LabelColumnName = new Label
								  {
									  FontWeight = FontWeights.Bold,
									  Content    = FindStringResource( "DialoguesDispatchLabelColumnName" )
								  };
			gridFilters.Children.Add( LabelColumnName );
			Grid.SetColumn( LabelColumnName, 0 );
			Grid.SetRow( LabelColumnName, 0 );

			var LabelCondition = new Label
								 {
									 FontWeight = FontWeights.Bold,
									 Content    = FindStringResource( "DialoguesDispatchLabelCondition" )
								 };
			gridFilters.Children.Add( LabelCondition );
			Grid.SetColumn( LabelCondition, 1 );
			Grid.SetRow( LabelCondition, 0 );

			var LabelValue = new Label
							 {
								 FontWeight = FontWeights.Bold,
								 Content    = FindStringResource( "DialoguesDispatchLabelValue" )
							 };
			gridFilters.Children.Add( LabelValue );
			Grid.SetColumn( LabelValue, 2 );
			Grid.SetRow( LabelValue, 0 );

			var LabelActive = new Label
							  {
								  FontWeight = FontWeights.Bold,
								  Content    = FindStringResource( "DialoguesDispatchLabelActive" )
							  };
			gridFilters.Children.Add( LabelActive );
			Grid.SetColumn( LabelActive, 3 );
			Grid.SetRow( LabelActive, 0 );

			// Now build the grid
			var BgGray  = Brushes.LightGray;
			var BgWhite = Brushes.White;
			var Bg      = BgGray;
			var Row     = 1;

			foreach( var Column in Columns )
			{
				// Don't display the Driver column or DateTime columns
				if( Column is "DialoguesDispatchColumnDriver" or "DialoguesDispatchColumnCallDate" or "DialoguesDispatchColumnCondition" or "DialoguesDispatchColumnReady" or "DialoguesDispatchColumnDue" or "DialoguesDispatchColumnBoard" )
					continue;

				gridFilters.RowDefinitions.Add( new RowDefinition() );
				var Resource = FindStringResource( Column );

				var Label = new Label
							{
								Content    = Resource,
								Background = Bg
							};
				FilterLabelList.Add( Label );
				gridFilters.Children.Add( Label );
				Grid.SetColumn( Label, 0 );
				Grid.SetRow( Label, Row );
				Label.Name = Column; // Used for lookup 

				var Cb = new ComboBox
						 {
							 ItemsSource   = Model.MapColumnToConditions( Column ),
							 SelectedIndex = 0,
							 Background    = Bg
						 };

				FilterConditionList.Add( Cb );
				gridFilters.Children.Add( Cb );
				Grid.SetColumn( Cb, 1 );
				Grid.SetRow( Cb, Row );

				var Tb = new TextBox
						 {
							 VerticalContentAlignment = VerticalAlignment.Center,
							 Background               = Bg
						 };
				FilterValueList.Add( Tb );
				gridFilters.Children.Add( Tb );
				Grid.SetColumn( Tb, 2 );
				Grid.SetRow( Tb, Row );

				if( Model.IsColumnNumeric( Column ) )
					Tb.AddHandler( PreviewTextInputEvent, new TextCompositionEventHandler( IntegerTextBox_PreviewTextInput ) );

				var CheckBox = new CheckBox
							   {
								   Name                = Column,
								   HorizontalAlignment = HorizontalAlignment.Center,
								   VerticalAlignment   = VerticalAlignment.Center
							   };
				FilterIsActiveList.Add( CheckBox );
				gridFilters.Children.Add( CheckBox );
				Grid.SetColumn( CheckBox, 3 );
				Grid.SetRow( CheckBox, Row );

				var Df = Model.LookUpFilter( Label.Name );

				if( Df != null )
				{
					Cb.SelectedItem    = Df.Condition;
					Tb.Text            = Df.Value;
					CheckBox.IsChecked = Df.Active;
				}

				++Row;

				Bg = ( Row % 2 ) == 0 ? BgWhite : BgGray;
			}
		}
	}

	public static void PopulateOtherSettings()
	{
		// TODO 
		//this.cbCollapseNotes.IsChecked = UserSettings.IsCollapseNotes();
	}

	private static void IntegerTextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		var          ToCheck = e.Text.Substring( e.Text.Length - 1 );
		const string DECIMAL = ".";

		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) && !ToCheck.Equals( DECIMAL ) )
			e.Handled = true;
	}

	private void BtnCloseSettings_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Closing settings tab" );
		tabItemDispatch.Visibility = Visibility.Collapsed;
		tabItemSettings.Visibility = Visibility.Collapsed;
		tabItemDispatch.IsSelected = true;
		tabItemDispatch.Focus();
	}

    
    // TODO Turned this off because it kills all timers
    //  private void Page_Unloaded(object sender, RoutedEventArgs e)
    //  {
    //if (Model != null)
    //{
    //	Logging.WriteLogLine("Unloading Dispatch Board " + Model.BoardName);
    //	Model.StopUpdateConditions();
    //}
    //  }
    #endregion

    #region Unmatched
    #endregion

    //   private void DataGrid_Unloaded(object sender, RoutedEventArgs e)
    //   {
    //	Logging.WriteLogLine("DEBUG Running Refresh()");
    //	var grid = (DataGrid)sender;
    //	grid.CommitEdit(DataGridEditingUnit.Cell, true );
    //       var View = CollectionViewSource.GetDefaultView(DataGrid.ItemsSource);
    //	View.Refresh();

    //}
}