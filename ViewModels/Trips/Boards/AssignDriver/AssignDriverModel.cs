﻿namespace ViewModels.Trips.Boards.AssignDriver;

public class AssignDriverModel : ViewModelBase
{
#region Actions
	public void ApplyFilter( string filter )
	{
		if( filter.IsNotNullOrEmpty() )
		{
			Logging.WriteLogLine( "DEBUG Filter: " + filter );

			var tmp = ( from D in Drivers
			            where D.ToLower().Contains( filter )
			            select D ).ToList();
			FilteredDrivers.Clear();

			foreach( var driver in tmp )
				//Logging.WriteLogLine("DEBUG Matching driver: " + driver);
				FilteredDrivers.Add( driver );
		}
		else
		{
			FilteredDrivers.Clear();

			//FilteredDrivers.AddRange(Drivers);
			foreach( var driver in Drivers )
				FilteredDrivers.Add( driver );
		}

		RaisePropertyChanged( nameof( FilteredDrivers ) );
	}
#endregion

#region Data
	public string CurrentDriver
	{
		get => Get( () => CurrentDriver, "" );
		set => Set( () => CurrentDriver, value );
	}


	public string NewDriver
	{
		get => Get( () => NewDriver, "" );
		set => Set( () => NewDriver, value );
	}

	[DependsUpon( nameof( NewDriver ) )]
	public void WhenNewDriverChanges()
	{
		IsAllocateButtonEnabled = NewDriver.IsNotNullOrWhiteSpace();
	}


	public bool IsAllocateButtonEnabled
	{
		get => Get( () => IsAllocateButtonEnabled, false );
		set => Set( () => IsAllocateButtonEnabled, value );
	}


	public List<string> Drivers
	{
		get => Get( () => Drivers, new List<string>() );
		set => Set( () => Drivers, value );
	}

	[DependsUpon( nameof( Drivers ) )]
	public void WhenDriversChanges()
	{
		FilteredDrivers.Clear();

		//FilteredDrivers.AddRange(Drivers);
		foreach( var driver in Drivers )
			FilteredDrivers.Add( driver );
	}


	public ObservableCollection<string> FilteredDrivers
	{
		get => Get( () => FilteredDrivers, new ObservableCollection<string>() );
		set => Set( () => FilteredDrivers, value );
	}
#endregion
}