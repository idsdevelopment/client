﻿using System.Windows.Input;

namespace ViewModels.Trips.Boards.AssignDriver;

/// <summary>
///     Interaction logic for AssignDriver.xaml
/// </summary>
public partial class AssignDriver : Window
{
	private AssignDriverModel Model { get; }

    private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public string ShowAssignDriver( List<string> drivers, bool isDriversBoard = false )
	{
		Model.Drivers = drivers;
		if (isDriversBoard)
		{
			// Change strings to *Assign* from *Allocate*
			ChangeStringsFromAllocateToAssign();
		}

		_ = ShowDialog();

		return Model.NewDriver;
	}

	private void ChangeStringsFromAllocateToAssign()
	{
		//"DialoguesAssignDriverTitle"
		Title = FindStringResource("DialoguesAssignDriverTitle");
		LblInstructions.Content = FindStringResource("DialoguesAssignDriverLabelInstructions");
		BtnAllocate.Content = FindStringResource("DialoguesAssignDriverButtonAssign");
	}

	public AssignDriver( Window owner )
	{
		Owner = owner;
		InitializeComponent();

		if( DataContext is AssignDriverModel m )
			Model = m;
		TbFilter.Focus();
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}

	private void Button_Click_1( object sender, RoutedEventArgs e )
	{
		Model.NewDriver = string.Empty;
		Close();
	}

	private void TbFilter_PreviewKeyDown( object sender, KeyEventArgs e )
	{
		//Model.ApplyFilter(TbFilter.Text.Trim());
		if (e.Key == Key.Enter)
		{
			if (Model.FilteredDrivers.Count == 1)
			{
				Model.NewDriver = Model.FilteredDrivers[0];
				Model.IsAllocateButtonEnabled = true;
				//Model.NewDriver = (string)LbDrivers.SelectedItem;
				e.Handled = true;  // Ensure that the key isn't passed to the datagrid
				Close();
			}
		}
	}

	private void TbFilter_SelectionChanged( object sender, RoutedEventArgs e )
	{
		LbDrivers.ItemsSource = null;
		Model.ApplyFilter( TbFilter.Text.Trim() );
		LbDrivers.ItemsSource = Model.FilteredDrivers;
		if (LbDrivers.Items.Count == 1 )
		{
			Model.IsAllocateButtonEnabled= true;
		}
		else
		{
			Model.IsAllocateButtonEnabled= false;
		}
	}

	private void LbDrivers_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.Enter)
		{
			Model.NewDriver = (string)LbDrivers.SelectedItem;
			Close();
		}
	}

	private void LbDrivers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	{
		Model.NewDriver = (string)LbDrivers.SelectedItem;
		Close();
	}
}