﻿#nullable enable

using System.IO;
using System.Windows.Threading;
using IdsControlLibraryV2.TabControl;
using ViewModels.BaseViewModels;
using ViewModels.RouteShipments;
using ViewModels.Trips.Boards.Common;
using File = System.IO.File;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

namespace ViewModels.Trips.Boards;

public class TripsByDriver : INotifyPropertyChanged
{
	public string Driver { get; set; } = "";

	public string DriverAndName { get; set; } = "";

	public bool Expanded
	{
		get => _Expanded;
		set
		{
			if( _Expanded != value )
			{
				_Expanded = value;
				OnPropertyChanged();
			}
		}
	}

	public ObservableCollection<DisplayTrip> Trips { get; set; } = new();

	private bool _Expanded = true;

	[NotifyPropertyChangedInvocator]
	public virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class DetailedTrip : DisplayTrip, INotifyPropertyChanged
{
	public string DriverAndName { get; set; } = "";

	public string PiecesAsString { get; set; } = "";
	public string WeightAsString { get; set; } = "";

	public bool Expanded
	{
		get => _Expanded;
		set
		{
			if( _Expanded != value )
			{
				_Expanded = value;
				OnPropertyChanged();
			}
		}
	}

	public bool MyNotReceivedByDevice
	{
		get => _MyNotReceivedByDevice;
		set
		{
			if( _MyNotReceivedByDevice != value )
			{
				_MyNotReceivedByDevice = value;
				OnPropertyChanged();
			}
		}
	}

	public bool MyReceivedByDevice
	{
		get => _MyReceivedByDevice;
		set
		{
			if( _MyReceivedByDevice != value )
			{
				_MyReceivedByDevice = value;
				OnPropertyChanged();
			}
		}
	}

	public bool MyReadByDriver
	{
		get => _MyReadByDriver;
		set
		{
			if( _MyReadByDriver != value )
			{
				_MyReadByDriver = value;
				OnPropertyChanged();
			}
		}
	}

	public bool MyPickedUp
	{
		get => _MyPickedUp;
		set
		{
			if( _MyPickedUp )
			{
				_MyPickedUp = value;
				OnPropertyChanged();
			}
		}
	}

	public bool MyDelivered
	{
		get => _MyDelivered;
		set
		{
			if( _MyDelivered != value )
			{
				_MyDelivered = value;
				OnPropertyChanged();
			}
		}
	}


	public bool MyEnableAccept { get; set; } = true;


	public bool MyDoNotSendToDriver { get; set; } = true;

	public bool MyShowExpand
	{
		get => _MyShowExpand;
		set
		{
			_MyShowExpand = value;
			OnPropertyChanged();
		}
	}

	public bool MyShowCollapse
	{
		get => _MyShowCollapse;
		set
		{
			_MyShowCollapse = value;
			OnPropertyChanged();
		}
	}
	//public string Driver { get; set; } = "";

	//public DisplayTrip(Trip t) : base(t, new ServiceLevel())

	public DetailedTrip()
	{
	}

	public DetailedTrip( DisplayTrip dt ) : base( dt, new ServiceLevel() )
	{
		PiecesAsString = Pieces.ToString( "0" );
		WeightAsString = Weight.ToString( "0.00" );
	}

	//public ObservableCollection<DisplayTrip> Trips { get; set; } = new ObservableCollection<DisplayTrip>();


	private bool _Expanded = true;

	private bool _MyDelivered = true;

	//[NotifyPropertyChangedInvocator]
	//public virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
	//{
	//	PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	//}

	//public event PropertyChangedEventHandler? PropertyChanged;


	private bool _MyNotReceivedByDevice = true;

	private bool _MyPickedUp = true;

	private bool _MyReadByDriver = true;

	private bool _MyReceivedByDevice = true;

	private bool _MyShowCollapse = true;

	private bool _MyShowExpand = true;
}

public class DriversBoardViewModel : AMessagingViewModel
{
	// BoardName, Settings for board
	//public static readonly Dictionary<string, Settings> DispatchBoardSettings = new Dictionary<string, Settings>();
	public static List<string> CurrentlySelectedColumns { get; set; } = new();

	public static Settings? DriversBoardSettings { get; set; }

	public string BoardName
	{
		get { return Get( () => BoardName, "" ); }
		set { Set( () => BoardName, value ); }
	}


	public string FormattedBoardName
	{
		get { return Get( () => FormattedBoardName, "" ); }
		set { Set( () => FormattedBoardName, value ); }
	}


	public string CurrentlySortedColumn
	{
		get { return Get( () => CurrentlySortedColumn, "" ); }
		set { Set( () => CurrentlySortedColumn, value ); }
	}


	public bool CurrentlySortedColumnAscending
	{
		get { return Get( () => CurrentlySortedColumnAscending, true ); }
		set { Set( () => CurrentlySortedColumnAscending, value ); }
	}

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1037927177/How+to+use+the+Drivers+Tab" ); }
	}

	//public ObservableCollection<DisplayTrip> Trips{ get; set; }
	public ObservableCollection<DisplayTrip> Trips
	{
		get { return Get( () => Trips, new ObservableCollection<DisplayTrip>() ); }
		set { Set( () => Trips, value ); }
	}


	/// <summary>
	///     Holds the trips for the new DataGrid.
	///     Grouped by driver.
	/// </summary>
	public ObservableCollection<DetailedTrip> DisplayTrips
	{
		get { return Get( () => DisplayTrips, new ObservableCollection<DetailedTrip>() ); }
		set { Set( () => DisplayTrips, value ); }
	}

	public ObservableCollection<TripsByDriver> TripsByDrivers
	{
		get { return Get( () => TripsByDrivers, new ObservableCollection<TripsByDriver>() ); }
		set { Set( () => TripsByDrivers, value ); }
	}

	public int TripCount
	{
		get { return Get( () => TripCount, 0 ); }
		set { Set( () => TripCount, value ); }
	}

	public DisplayTrip? SelectedTrip
	{
		get { return Get( () => SelectedTrip, new DisplayTrip() ); }
		set { Set( () => SelectedTrip, value ); }
	}

	//public DisplayTrip PreviouslySelectedTrip { get; set; }

	public DisplayTrip? PreviouslySelectedTrip
	{
		get { return Get( () => PreviouslySelectedTrip, SelectedTrip ); }
		set { Set( () => PreviouslySelectedTrip, value ); }
	}

	public List<DisplayTrip>? SelectedTrips
	{
		get { return Get( () => SelectedTrips, new List<DisplayTrip>() ); }
		set { Set( () => SelectedTrips, value ); }
	}


	public bool TripDetailEnabled
	{
		get { return Get( () => TripDetailEnabled, true ); }
		set { Set( () => TripDetailEnabled, value ); }
	}


	public ObservableCollection<Driver> Drivers
	{
		get { return Get( () => Drivers, new ObservableCollection<Driver>() ); }
		set { Set( () => Drivers, value ); }
	}


	public Driver SelectedDriver
	{
		get { return Get( () => SelectedDriver, new Driver() ); }
		set { Set( () => SelectedDriver, value ); }
	}


	public TripsByDriver SelectedTripsByDriver
	{
		get { return Get( () => SelectedTripsByDriver, new TripsByDriver() ); }
		set { Set( () => SelectedTripsByDriver, value ); }
	}


	public bool DataGridVisible
	{
		get { return Get( () => DataGridVisible, IsInDesignMode ); }
		set { Set( () => DataGridVisible, value ); }
	}

	[Setting]
	public bool DriverSearchVisible
	{
		get { return Get( () => DriverSearchVisible, IsInDesignMode ); }
		set { Set( () => DriverSearchVisible, value ); }
	}


	public string DriversFilter
	{
		get { return Get( () => DriversFilter, "" ); }
		set { Set( () => DriversFilter, value ); }
	}


	public bool EnableBounce
	{
		get { return Get( () => EnableBounce, false ); }
		set { Set( () => EnableBounce, value ); }
	}


	public Trip SelectedTripDetails
	{
		get { return Get( () => SelectedTripDetails, new Trip() ); }
		set { Set( () => SelectedTripDetails, value ); }
	}

	protected override async void OnInitialised()
	{
		DisplayTrip.SelectedBackground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedBackgroundColourBrush" );
		DisplayTrip.SelectedForeground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedForegroundColourBrush" );

		base.OnInitialised();

		try
		{
			Trips.Clear();

			if( !IsInDesignMode )
			{
				var STemp = await Azure.Client.RequestGetServiceLevelsDetailed();
				var Temp  = await Azure.Client.RequestGetTripsByStatus( new StatusRequest {STATUS.DISPATCHED, STATUS.PICKED_UP, STATUS.DELIVERED} );

				//
				// 20211104 cjt This was including duplicate drivers
				// 
				//Drivers = new ObservableCollection<Driver>(from D in await Azure.Client.RequestGetDrivers()
				//                                                          select new Driver
				//                                                          {
				//                                                              StaffId = D.StaffId,
				//                                                              DisplayName = $"{D.FirstName} {D.LastName}".Trim().Capitalise()

				//                                                              //DisplayName = $"{D.FirstName} {D.LastName}".Trim()
				//                                                          });

				/*
				 * NOTE: This request doesn't always return all drivers - i found this with Phoenixtest
                StaffLookupSummaryList? raw = await Azure.Client.RequestGetDrivers();
				//List<Driver> distinct = new();
				Drivers = new();
									foreach (StaffLookupSummary sls in raw)
									{
										string sid = sls.StaffId.Trim();
										//Driver exists = (from D in distinct where D.StaffId == sid select D).FirstOrDefault();
										Driver exists = (from D in Drivers where D.StaffId == sid select D).FirstOrDefault();
										if (exists == null)
										{
											Driver driver = new()
											{
												StaffId = sid,
												DisplayName = $"{sls.FirstName} {sls.LastName}".Trim().Capitalise()
											};
											//distinct.Add(driver);
											Drivers.Add(driver);
										}
									}
				*/
				Drivers = GetDrivers();

				if( STemp is not null )
				{
					foreach( var ServiceLevel in STemp )
						ServiceLevels.Add( ServiceLevel.NewName, new ServiceLevel( ServiceLevel ) );
				}

				var Trps = new ObservableCollection<DisplayTrip>( from T in Temp
				                                                  select new DisplayTrip( T, GetServiceLevel( T.ServiceLevel ) ) );
				TripCount = Trps.Count;
				Trips     = Trps;

				TripsByDrivers = new ObservableCollection<TripsByDriver>( from T in Trps
				                                                          orderby T.Driver
				                                                          group T by T.Driver
				                                                          into G
				                                                          select new TripsByDriver {Driver = G.Key, Trips = new ObservableCollection<DisplayTrip>( G )} );

				lock( UpdateLock )
				{
					//DriverDict = Drivers.ToDictionary( driver => driver.StaffId, driver => driver.DisplayName );
					DriverDict.Clear();

					foreach( var driver in Drivers )
					{
						//if (driver != null && !DriverDict.ContainsKey(driver.StaffId))
						if( !DriverDict.ContainsKey( driver.StaffId ) )
							DriverDict.Add( driver.StaffId, driver.DisplayName );
					}

					foreach( var TripsByDriver in TripsByDrivers )
					{
						if( DriverDict.TryGetValue( TripsByDriver.Driver, out var DriverName ) )
							DriverName = $" -- {DriverName}";

						TripsByDriver.DriverAndName = $"{TripsByDriver.Driver}{DriverName}";
					}
				}
				UpdateDriverList();

				//UpdateDisplayTrips(Trips.ToList());

				DataGridVisible   = true;
				IgnoreTripReceive = false;

				SetWindowSort();
			}
		}
		catch( Exception Exception )
		{
			Logging.WriteLogLine( Exception + Exception.ToString() );
		}
	}

	public DriversBoardViewModel() : base( Messaging.DRIVERS_BOARD, new[]
	                                                                {
		                                                                STATUS.ACTIVE,
		                                                                STATUS.DISPATCHED,
		                                                                STATUS.PICKED_UP,
		                                                                STATUS.DELIVERED,
		                                                                STATUS.VERIFIED,
		                                                                STATUS.DELETED,
		                                                                STATUS.FINALISED
	                                                                } )
	{
		//string name = BASE_BOARD_NAME + DispatchBoardNames.Count;
		var name = BASE_BOARD_NAME + "0";

		List<string> names = new();
		var          tis   = Globals.DataContext.MainDataContext.ProgramTabItems;

		if( tis.Count > 0 )
		{
			for( var i = 0; i < tis.Count; i++ )
			{
				var ti = tis[ i ];

				if( ti.Content is DriversBoard pti )
				{
					var tmp = ( (DriversBoardViewModel)pti.DataContext ).BoardName;
					//Logging.WriteLogLine("Found board: " + tmp);
					names.Add( tmp );
				}
			}
		}

		// Now, check to see if there is a hole
		if( names.Count > 0 )
		{
			for( var i = 0; i < 100; i++ )
			{
				var tmp = BASE_BOARD_NAME + i;

				if( !names.Contains( tmp ) )
				{
					// Found a hole
					//Logging.WriteLogLine("Reusing boardname: " + tmp);
					name = tmp;
					break;
				}
			}
		}

		if( !DriversBoardNames.Contains( name ) )
			DriversBoardNames.Add( name );
		BoardName = name;

		//Logging.WriteLogLine("Opening " + name);
		Dispatcher.Invoke( LoadSettings );
	}

	public static List<string> AllDriversColumnsInOrder = new()
	                                                      {
		                                                      //"DialoguesDriversColumnDriver",
		                                                      "DialoguesDriversColumnTripId",
		                                                      "DialoguesDriversColumnAcct",
		                                                      "DialoguesDriversColumnCompanyName",
		                                                      "DialoguesDriversColumnAcctNotes",
		                                                      "DialoguesDriversColumnCZ",

		                                                      "DialoguesDriversColumnReadyTimeCondition",
		                                                      "DialoguesDriversColumnPickup",
		                                                      "DialoguesDriversColumnPickupSuite",
		                                                      "DialoguesDriversColumnPickupStreet",
		                                                      "DialoguesDriversColumnPickupCity",
		                                                      "DialoguesDriversColumnPickupProvState",
		                                                      "DialoguesDriversColumnPickupCountry",
		                                                      "DialoguesDriversColumnPickupPostalZip",
		                                                      "DialoguesDriversColumnPickupAddressNotes",
		                                                      "DialoguesDriversColumnPickupNotes",
		                                                      "DialoguesDriversColumnPZ",

		                                                      "DialoguesDriversColumnDueTimeCondition",
		                                                      "DialoguesDriversColumnDelivery",
		                                                      "DialoguesDriversColumnDeliverySuite",
		                                                      "DialoguesDriversColumnDeliveryStreet",
		                                                      "DialoguesDriversColumnDeliveryCity",
		                                                      "DialoguesDriversColumnDeliveryProvState",
		                                                      "DialoguesDriversColumnDeliveryCountry",
		                                                      "DialoguesDriversColumnDeliveryPostalZip",
		                                                      "DialoguesDriversColumnDeliveryAddressNotes",
		                                                      "DialoguesDriversColumnDeliveryNotes",
		                                                      "DialoguesDriversColumnDZ",

		                                                      "DialoguesDriversColumnPkg",
		                                                      "DialoguesDriversColumnServ",
		                                                      "DialoguesDriversColumnCallDate",
		                                                      "DialoguesDriversColumnReady",
		                                                      "DialoguesDriversColumnDue",
		                                                      "DialoguesDriversColumnPcs",
		                                                      "DialoguesDriversColumnWt" //,
		                                                      //"DialoguesDriversColumnBoard"

		                                                      //"DialoguesDriversColumnPlts"					
	                                                      };

	public static readonly string BASE_BOARD_NAME = "DriversBoard_";

	private static readonly Dictionary<string, string> dictionaryColumns = new()
	                                                                       {
		                                                                       //{ "DialoguesDriversColumnDriver", "driver" },
		                                                                       //{ "DialoguesDriversColumnTripId", "TripId" },
		                                                                       {"DialoguesDriversColumnCallDate", "CallDate"},
		                                                                       {"DialoguesDriversColumnAcct", "AccountId"},
		                                                                       {"DialoguesDriversColumnCompanyName", "BillingCompany"},
		                                                                       {"DialoguesDriversColumnAcctNotes", "BillingNotes"},
		                                                                       {"DialoguesDriversColumnCZ", "CurrentZone"},

		                                                                       {"DialoguesDriversColumnReadyTimeCondition", "ReadyTimeCondition"},
		                                                                       {"DialoguesDriversColumnPickup", "PickupCompany"},
		                                                                       {"DialoguesDriversColumnPickupSuite", "PickupAddressSuite"},
		                                                                       {"DialoguesDriversColumnPickupStreet", "PickupAddressAddressLine1"},
		                                                                       {"DialoguesDriversColumnPickupCity", "PickupAddressCity"},
		                                                                       {"DialoguesDriversColumnPickupProvState", "PickupAddressRegion"},
		                                                                       {"DialoguesDriversColumnPickupCountry", "PickupAddressCountry"},
		                                                                       {"DialoguesDriversColumnPickupPostalZip", "PickupAddressPostalCode"},
		                                                                       {"DialoguesDriversColumnPickupAddressNotes", "PickupAddressNotes"},
		                                                                       {"DialoguesDriversColumnPickupNotes", "PickupNotes"},
		                                                                       {"DialoguesDriversColumnPZ", "PickupZone"},

		                                                                       {"DialoguesDriversColumnDueTimeCondition", "DueTimeCondition"},
		                                                                       {"DialoguesDriversColumnDelivery", "DeliveryCompany"},
		                                                                       {"DialoguesDriversColumnDeliverySuite", "DeliveryAddressSuite"},
		                                                                       {"DialoguesDriversColumnDeliveryStreet", "DeliveryAddressAddressLine1"},
		                                                                       {"DialoguesDriversColumnDeliveryCity", "DeliveryAddressCity"},
		                                                                       {"DialoguesDriversColumnDeliveryProvState", "DeliveryAddressRegion"},
		                                                                       {"DialoguesDriversColumnDeliveryCountry", "DeliveryAddressCountry"},
		                                                                       {"DialoguesDriversColumnDeliveryPostalZip", "DeliveryAddressPostalCode"},
		                                                                       {"DialoguesDriversColumnDeliveryAddressNotes", "DeliveryAddressNotes"},
		                                                                       {"DialoguesDriversColumnDeliveryNotes", "DeliveryNotes"},
		                                                                       {"DialoguesDriversColumnDZ", "DeliveryZone"},

		                                                                       {"DialoguesDriversColumnPkg", "PackageType"},
		                                                                       {"DialoguesDriversColumnServ", "ServiceLevel"},
		                                                                       {"DialoguesDriversColumnReady", "ReadyTime"},
		                                                                       {"DialoguesDriversColumnDue", "DueTime"},
		                                                                       {"DialoguesDriversColumnPcs", "Pieces"},
		                                                                       {"DialoguesDriversColumnWt", "Weight"}
		                                                                       //{ "DialoguesDriversColumnBoard", "Board" }

		                                                                       //{ "DialoguesDriversColumnPlts", "pallets" },
	                                                                       };

	public static readonly  List<string> DriversBoardNames = new();
	public static readonly  string       PROGRAM           = "Drivers Board (" + Globals.CurrentVersion.APP_VERSION + ")";
	private static readonly string       EQUALS            = FindStringResource( "DialoguesDriversConditionsEquals" );
	private static readonly string       NOT_EQUAL_TO      = FindStringResource( "DialoguesDriversConditionsNotEqualTo" );
	private static readonly string       IN                = FindStringResource( "DialoguesDriversConditionsIn" );
	private static readonly string       CONTAINS          = FindStringResource( "DialoguesDriversConditionsContains" );
	private static readonly string       STARTS_WITH       = FindStringResource( "DialoguesDriversConditionsStartsWith" );
	private static readonly string       ENDS_WITH         = FindStringResource( "DialoguesDriversConditionsEndsWith" );
	public static readonly  List<string> allConditions     = new() {"=", "!=", ">", "<"};


	private readonly Dictionary<string, bool> dictDisplayTripsVisibility = new();


	//private DisplayTrip? PreviousSelectedTrip;

	public readonly Dictionary<string, Visibility> dictionaryColumnVisiblity = new()
	                                                                           {
		                                                                           //{ "DialoguesDriversColumnDriver", Visibility.Visible },
		                                                                           //{ "DialoguesDriversColumnTripId", Visibility.Hidden },
		                                                                           {"DialoguesDriversColumnAcct", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnCompanyName", Visibility.Hidden}, // New
		                                                                           {"DialoguesDriversColumnAcctNotes", Visibility.Hidden},   // New
		                                                                           {"DialoguesDriversColumnCZ", Visibility.Hidden},          // New

		                                                                           {"DialoguesDriversColumnReadyTimeCondition", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnPickup", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnPickupSuite", Visibility.Hidden}, // New
		                                                                           {"DialoguesDriversColumnPickupStreet", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnPickupCity", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnPickupProvState", Visibility.Hidden},    // New
		                                                                           {"DialoguesDriversColumnPickupCountry", Visibility.Hidden},      // New
		                                                                           {"DialoguesDriversColumnPickupPostalZip", Visibility.Hidden},    // New
		                                                                           {"DialoguesDriversColumnPickupAddressNotes", Visibility.Hidden}, // New
		                                                                           {"DialoguesDriversColumnPickupNotes", Visibility.Hidden},        // New
		                                                                           {"DialoguesDriversColumnPZ", Visibility.Hidden},                 // New

		                                                                           {"DialoguesDriversColumnDueTimeCondition", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnDelivery", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnDeliverySuite", Visibility.Hidden}, // New
		                                                                           {"DialoguesDriversColumnDeliveryStreet", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnDeliveryCity", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnDeliveryProvState", Visibility.Hidden},    // New
		                                                                           {"DialoguesDriversColumnDeliveryCountry", Visibility.Hidden},      // New
		                                                                           {"DialoguesDriversColumnDeliveryPostalZip", Visibility.Hidden},    // New
		                                                                           {"DialoguesDriversColumnDeliveryAddressNotes", Visibility.Hidden}, // New
		                                                                           {"DialoguesDriversColumnDeliveryNotes", Visibility.Hidden},        // New
		                                                                           {"DialoguesDriversColumnDZ", Visibility.Hidden},                   // New

		                                                                           {"DialoguesDriversColumnServ", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnPkg", Visibility.Hidden},
		                                                                           //{ "DialoguesDriversColumnCall", Visibility.Hidden },
		                                                                           {"DialoguesDriversColumnCallDate", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnReady", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnDue", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnPcs", Visibility.Hidden},
		                                                                           {"DialoguesDriversColumnWt", Visibility.Hidden}
		                                                                           //{ "DialoguesDriversColumnBoard", Visibility.Hidden } // New
		                                                                           //{ "DialoguesDriversColumnPlts", Visibility.Hidden } // New	
	                                                                           };

	//private Dictionary<string, string> dictionaryAdhocFilters = new Dictionary<string, string>();

	//private static List<DispatchViewModel.DispatchFilter>? filters = null;

	private readonly Dictionary<string, List<string>> dictionaryConditionsForField = new()
	                                                                                 {
		                                                                                 //{ "DialoguesDriversColumnDriver", stringConditions },
		                                                                                 //{ "DialoguesDriversColumnTripId", stringConditions },
		                                                                                 //{ "DialoguesDriversColumnCallDate", allConditions },
		                                                                                 {"DialoguesDriversColumnAcct", stringConditions},
		                                                                                 {"DialoguesDriversColumnCompanyName", stringConditions}, // New
		                                                                                 {"DialoguesDriversColumnAcctNotes", stringConditions},   // New
		                                                                                 {"DialoguesDriversColumnCZ", stringConditions},          // New

		                                                                                 {"DialoguesDriversColumnPickup", stringConditions},
		                                                                                 {"DialoguesDriversColumnPickupSuite", stringConditions}, // New
		                                                                                 {"DialoguesDriversColumnPickupStreet", stringConditions},
		                                                                                 {"DialoguesDriversColumnPickupCity", stringConditions},
		                                                                                 {"DialoguesDriversColumnPickupProvState", stringConditions},    // New
		                                                                                 {"DialoguesDriversColumnPickupCountry", stringConditions},      // New
		                                                                                 {"DialoguesDriversColumnPickupPostalZip", stringConditions},    // New
		                                                                                 {"DialoguesDriversColumnPickupAddressNotes", stringConditions}, // New
		                                                                                 {"DialoguesDriversColumnPickupNotes", stringConditions},        // New
		                                                                                 {"DialoguesDriversColumnPZ", stringConditions},

		                                                                                 {"DialoguesDriversColumnDelivery", stringConditions},
		                                                                                 {"DialoguesDriversColumnDeliverySuite", stringConditions}, // New
		                                                                                 {"DialoguesDriversColumnDeliveryStreet", stringConditions},
		                                                                                 {"DialoguesDriversColumnDeliveryCity", stringConditions},
		                                                                                 {"DialoguesDriversColumnDeliveryProvState", stringConditions},    // New
		                                                                                 {"DialoguesDriversColumnDeliveryCountry", stringConditions},      // New
		                                                                                 {"DialoguesDriversColumnDeliveryPostalZip", stringConditions},    // New
		                                                                                 {"DialoguesDriversColumnDeliveryAddressNotes", stringConditions}, // New
		                                                                                 {"DialoguesDriversColumnDeliveryNotes", stringConditions},        // New
		                                                                                 {"DialoguesDriversColumnDZ", stringConditions},

		                                                                                 {"DialoguesDriversColumnPkg", stringConditions},
		                                                                                 {"DialoguesDriversColumnServ", stringConditions},
		                                                                                 {"DialoguesDriversColumnReady", allConditions},
		                                                                                 {"DialoguesDriversColumnDue", allConditions},
		                                                                                 {"DialoguesDriversColumnPcs", allConditions},
		                                                                                 {"DialoguesDriversColumnWt", allConditions}
		                                                                                 //{ "DialoguesDriversColumnBoard", allConditions }

		                                                                                 //{ "DialoguesDriversColumnPlts", allConditions },
	                                                                                 };


	private readonly Dictionary<string, List<DriversBoardFilter>> dictionaryFilters = new();

	private Dictionary<string, string> DriverDict = new();


	private bool IgnoreTripReceive;


	public static readonly char InConditionListDelimeter = ',';

	public int LastTripCount;


	private readonly Dictionary<string, ServiceLevel> ServiceLevels = new();


	public static readonly List<string> stringConditions = new()
	                                                       {
		                                                       EQUALS,
		                                                       NOT_EQUAL_TO,
		                                                       IN,
		                                                       CONTAINS,
		                                                       STARTS_WITH,
		                                                       ENDS_WITH
	                                                       };

	private readonly object UpdateLock = new();

	public DriversBoard? view;

	private bool DoesTripMatchAdHocFilter( DisplayTrip trip, string filter )
	{
		var yes = false;

		foreach( var key in dictionaryColumnVisiblity.Keys )
		{
			if( dictionaryColumnVisiblity[ key ] == Visibility.Visible )
			{
				if( DoesColumnMatchAdHocFilter( trip, key, filter ) )
				{
					//Logging.WriteLogLine("Trip: " + trip.TripId + " key: " + key + " filter: " + filter);
					yes = true;
					break;
				}
			}
		}

		if( !yes )
		{
			// Also check the TripId
			if( DoesColumnMatchAdHocFilter( trip, "DialoguesDriversColumnTripId", filter ) )
				yes = true;
		}

		return yes;
	}

	private static bool DoesColumnMatchAdHocFilter( DisplayTrip trip, string column, string filter )
	{
		var yes = false;

		// TODO convert column to actual column
		switch( column )
		{
		case "DialoguesDriversColumnDriver":
			if( trip.Driver.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnTripId":
			if( trip.TripId.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnAcct":
			if( trip.AccountId.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnCallDate":
			if( trip.CallTime.ToString().ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnCompanyName":
			if( trip.BillingCompanyName.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnAcctNotes":
			if( trip.BillingNotes.ToLower().Contains( filter ) )
				yes = true;
			break;

		//           case "DialoguesDriversColumnCondition":
		//// TODO
		//break;
		case "DialoguesDriversColumnCZ":
			if( trip.CurrentZone.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickup":
			if( trip.PickupCompanyName.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupSuite":
			if( trip.PickupAddressSuite.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupStreet":
			if( trip.PickupAddressAddressLine1.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupCity":
			if( trip.PickupAddressCity.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupProvState":
			if( trip.PickupAddressRegion.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupCountry":
			if( trip.PickupAddressCountry.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupPostalZip":
			if( trip.PickupAddressPostalCode.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupAddressNotes":
			if( trip.PickupAddressNotes.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPickupNotes":
			if( trip.PickupNotes.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPZ":
			if( trip.PickupZone.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDelivery":
			if( trip.DeliveryCompanyName.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliverySuite":
			if( trip.DeliveryAddressSuite.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryStreet":
			if( trip.DeliveryAddressAddressLine1.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryCity":
			if( trip.DeliveryAddressCity.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryProvState":
			if( trip.DeliveryAddressRegion.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryCountry":
			if( trip.DeliveryAddressCountry.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryPostalZip":
			if( trip.DeliveryAddressPostalCode.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDZ":
			if( trip.DeliveryZone.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryAddressNotes":
			if( trip.DeliveryAddressNotes.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDeliveryNotes":
			if( trip.DeliveryNotes.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnServ":
			if( trip.ServiceLevel.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPkg":
			if( trip.PackageType.ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnReady":
			if( trip.ReadyTime.ToString().ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnDue":
			if( trip.DueTime.ToString().ToLower().Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnPcs":
			var pieces = trip.Pieces + "";

			if( pieces.Contains( filter ) )
				yes = true;
			break;

		case "DialoguesDriversColumnWt":
			var weight = trip.Weight + "";

			if( weight.Contains( filter ) )
				yes = true;
			break;
		//case "DialoguesDriversColumnBoard":
		//    break;
		//case "DialoguesDriversColumnPlts":
		//    break;
		//case "DialoguesDriversColumnPN":
		//    break;
		//case "DialoguesDriversColumnDN":
		//    break;
		//case "DialoguesDriversColumnDstate":
		//    break;
		//case "DialoguesDriversColumnDzip":
		//    break;
		}

		return yes;
	}


	private bool DoesTripMatchFilter( DisplayTrip dt, DriversBoardFilter df )
	{
		var yes = false;

		if( ( df != null ) && ( df.columnName != null ) && ( df.condition != null ) && ( df.value != null ) )
		{
			var colName   = df.columnName;
			var condition = df.condition;
			var value     = df.value;

			var field = MapColumnToTripField( colName );

			yes = field.ToLower() switch //(colName.ToLower())
			      {
				      "driver"                      => DoesStringMatch( dt.Driver, condition, value ),
				      "tripid"                      => DoesStringMatch( dt.TripId, condition, value ),
				      "accountid"                   => DoesStringMatch( dt.AccountId, condition, value ),
				      "billingcompanyname"          => DoesStringMatch( dt.BillingCompanyName, condition, value ),
				      "billingnotes"                => DoesStringMatch( dt.BillingNotes, condition, value ),
				      "currentzone"                 => DoesStringMatch( dt.CurrentZone, condition, value ),
				      "pickupcompany"               => DoesStringMatch( dt.PickupCompanyName, condition, value ),
				      "pickupaddresssuite"          => DoesStringMatch( dt.PickupAddressSuite, condition, value ),
				      "pickupaddressaddressline1"   => DoesStringMatch( dt.PickupAddressAddressLine1, condition, value ),
				      "pickupaddresscity"           => DoesStringMatch( dt.PickupAddressCity, condition, value ),
				      "pickupaddressregion"         => DoesStringMatch( dt.PickupAddressRegion, condition, value ),
				      "pickupaddresscountry"        => DoesStringMatch( dt.PickupAddressCountry, condition, value ),
				      "pickupaddresspostalcode"     => DoesStringMatch( dt.PickupAddressPostalCode, condition, value ),
				      "pickupaddressnotes"          => DoesStringMatch( dt.PickupAddressNotes, condition, value ),
				      "pickupnotes"                 => DoesStringMatch( dt.PickupNotes, condition, value ),
				      "pickupzone"                  => DoesStringMatch( dt.PickupZone, condition, value ),
				      "deliverycompany"             => DoesStringMatch( dt.DeliveryCompanyName, condition, value ),
				      "deliveryaddresssuite"        => DoesStringMatch( dt.DeliveryAddressSuite, condition, value ),
				      "deliveryaddressaddressline1" => DoesStringMatch( dt.DeliveryAddressAddressLine1, condition, value ),
				      "deliveryaddresscity"         => DoesStringMatch( dt.DeliveryAddressCity, condition, value ),
				      "deliveryaddressregion"       => DoesStringMatch( dt.DeliveryAddressRegion, condition, value ),
				      "deliveryaddresscountry"      => DoesStringMatch( dt.DeliveryAddressCountry, condition, value ),
				      "deliveryaddresspostalzip"    => DoesStringMatch( dt.DeliveryAddressPostalBarcode, condition, value ),
				      "deliverynotes"               => DoesStringMatch( dt.DeliveryNotes, condition, value ),
				      "deliveryzone"                => DoesStringMatch( dt.DeliveryZone, condition, value ),
				      "servicelevel"                => DoesStringMatch( dt.ServiceLevel, condition, value ),
				      "packagetype"                 => DoesStringMatch( dt.PackageType, condition, value ),
				      //case "readytime":
				      //	yes = DoesDateTimeMatch(dt.readyTime, condition, value);
				      //	break;
				      //case "deadlinetime":
				      //	yes = DoesDateTimeMatch(dt.deadlineTime, condition, value);
				      //	break;
				      "pieces" => DoesDecimalMatch( dt.Pieces, condition, value.Trim() ),
				      "weight" =>
					      //yes = DoesDecimalMatch(new decimal(dt.Weight), condition, value.Trim());
					      DoesDecimalMatch( dt.Weight, condition, value.Trim() ),
				      "board" =>
					      //yes = DoesDecimalMatch(new decimal(dt.Board), condition, value.Trim());
					      DoesStringMatch( dt.Board, condition, value ),
				      _ => yes
			      };
		}

		//if (yes)
		//         {
		//	yes = DoesTripMatchAdHocFilter(dt, Filter);
		//         }

		//Logging.WriteLogLine("Trip: " + dt.TripId + " matches: " + yes);
		return yes;
	}

	private async Task UpdateStatusAndRemove( STATUS status )
	{
		if( SelectedTrips is not null )
		{
			SelectedTrip = new DisplayTrip();

			var TripsToProcess = new List<DisplayTrip>( SelectedTrips );

			SelectedTrips = null;

			for( var I = TripsToProcess.Count; --I >= 0; )
			{
				var T = TripsToProcess[ I ];
				--TripCount;
				RemoveTrip( T.TripId );

				T.Program = PROGRAM;
			}

			var Broadcast = status switch
			                {
				                STATUS.DELETED => TripUpdateStatus.BROADCAST.DRIVER | TripUpdateStatus.BROADCAST.DRIVERS_BOARD,
				                STATUS.ACTIVE  => TripUpdateStatus.BROADCAST.ALL,
				                _              => TripUpdateStatus.BROADCAST.NONE
			                };

			var Updates = ( from T in TripsToProcess
			                group T.TripId by new
			                                  {
				                                  T.Driver,
				                                  T.Status,
				                                  T.Status2,
				                                  T.Status3,
				                                  T.ReadByDriver,
				                                  T.ReceivedByDevice
			                                  }
			                into G
			                select G ).ToDictionary( g => g.Key, g => g.ToList() );

			foreach( var Update in Updates )
			{
				var K      = Update.Key;
				var Device = TripUpdateStatusBase.DEVICE_STATUS.UNSET;

				if( K.ReceivedByDevice )
					Device |= TripUpdateStatusBase.DEVICE_STATUS.RECEIVED_BY_DEVICE;

				if( K.ReadByDriver )
					Device |= TripUpdateStatusBase.DEVICE_STATUS.READ_BY_DRIVER;

				await UpdateStatus( PROGRAM, status, K.Status2, K.Status3, Update.Value, K.Driver, Broadcast, Device );
			}
		}
	}

	private void UpdateDriverList()
	{
		var Hash = new HashSet<string>();

		foreach( var TripsByDriver in TripsByDrivers )
		{
			//Logging.WriteLogLine("DEBUG TripsByDrivers: driver: '" + TripsByDriver.Driver + "'");
			Hash.Add( TripsByDriver.Driver );

			var exists = ( from D in Drivers
			               where D.StaffId == TripsByDriver.Driver
			               select D ).FirstOrDefault();

			if( exists == null )
			{
				// This driver doesn't exist - add it to the list
				Driver driver = new()
				                {
					                StaffId     = TripsByDriver.Driver,
					                HasTrips    = true,
					                DisplayName = "Not in Staff List"
				                };
				Drivers.Add( driver );
				DriverDict.Add( driver.StaffId, driver.DisplayName );
				Logging.WriteLogLine( "Driver with trips isn't a driver in Stafflist: " + driver.StaffId );
			}
		}

		foreach( var driver in Drivers )
		{
			//Logging.WriteLogLine("DEBUG Checking driver: '" + Driver.StaffId + "'");
			driver.IsVisible = Hash.Contains( driver.StaffId );
			//if (Driver.IsVisible)
			//{
			//    Logging.WriteLogLine("DEBUG Visible driver: " + Driver.StaffId);
			//}

			SetDriversVisibility( driver.StaffId, true );
		}
	}

	private void RemoveTrip( string tripId )
	{
		try
		{
			var Trps   = Trips;
			var Driver = "";

			for( var I = Trips.Count; --I >= 0; )
			{
				var T = Trps[ I ];

				if( T.TripId == tripId )
				{
					Driver = T.Driver;
					Trips.RemoveAt( I );

					break;
				}
			}

			if( Driver.IsNotNullOrWhiteSpace() )
			{
				var TTemp = TripsByDrivers;

				for( var J = TTemp.Count; --J >= 0; )
				{
					var DTrips = TTemp[ J ];

					if( DTrips.Driver == Driver )
					{
						var Tps = DTrips.Trips;

						for( var I = Tps.Count; --I >= 0; )
						{
							var Trip = Tps[ I ];

							if( Trip.TripId == tripId )
							{
								Tps.Remove( Trip );

								break;
							}
						}

						if( DTrips.Trips.Count == 0 )
							TTemp.RemoveAt( J );

						break;
					}
				}
			}

			UpdateDriverList();
			SortTrips();
		}
		catch( Exception Exception )
		{
			Logging.WriteLogLine( Exception );
		}
	}

	private ServiceLevel GetServiceLevel( string s )
	{
		if( !ServiceLevels.TryGetValue( s, out var Sl ) )
			Sl = new ServiceLevel();

		return Sl;
	}

	private ObservableCollection<Driver> GetDrivers()
	{
		ObservableCollection<Driver> drivers = new();

		if( !IsInDesignMode )
		{
			Task.WaitAll(
			             Task.Run( async () =>
			                       {
				                       var staffAndRoles = await Azure.Client.RequestGetStaffAndRoles();

				                       if( staffAndRoles != null )
				                       {
					                       foreach( var sar in staffAndRoles )
					                       {
						                       if( sar.StaffId != Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
						                       {
							                       foreach( var r in sar.Roles )
							                       {
								                       if( r.IsDriver )
								                       {
									                       var exists = ( from D in drivers
									                                      where D.StaffId == sar.StaffId
									                                      select D ).FirstOrDefault();

									                       if( exists == null )
									                       {
										                       Driver driver = new()
										                                       {
											                                       StaffId     = sar.StaffId,
											                                       DisplayName = $"{sar.FirstName} {sar.LastName}".Trim().Capitalise()
										                                       };

										                       drivers.Add( driver );
									                       }
									                       break;
								                       }
							                       }
						                       }
					                       }
				                       }
			                       } )
			            );
		}

		return drivers;
	}


	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	[DependsUpon( nameof( BoardName ) )]
	public string FormatBoardName()
	{
		var fbn = string.Empty;

		if( BoardName.IsNotNullOrWhiteSpace() )
		{
			var pieces = BoardName.Split( '_' );

			if( pieces.Length > 1 )
			{
				var name = pieces[ 0 ] + " " + pieces[ 1 ];
				FormattedBoardName = name;
				fbn                = name;
			}
			else
			{
				FormattedBoardName = BoardName;
				fbn                = BoardName;
			}
		}

		return fbn;
	}

	public void SetDriversVisibility( string driver, bool visibility )
	{
		if( !dictDisplayTripsVisibility.ContainsKey( driver ) )
			dictDisplayTripsVisibility.Add( driver, true );
		dictDisplayTripsVisibility[ driver ] = visibility;
	}

	public void SetAllDriversVisibility( bool visibility )
	{
		//foreach (var driver in dictDisplayTripsVisibility.Keys)
		foreach( var driver in Drivers )
			//dictDisplayTripsVisbility[driver] = visibility;
			ShowHideDriversTrips( driver.StaffId, visibility, false );

		//UpdateDisplayTrips(Trips.ToList());
		SortTrips();
	}

	public void UpdateDisplayTrips( List<DisplayTrip> trips )
	{
		//Logging.WriteLogLine("DEBUG Starting update");
		DisplayTrips = new ObservableCollection<DetailedTrip>();
		var oc         = new ObservableCollection<DetailedTrip>();
		var lastDriver = string.Empty;

		foreach( var t in trips )
		{
			if( DriverDict.TryGetValue( t.Driver, out var DriverName ) )
				DriverName = $" -- {DriverName}";

			if( lastDriver != t.Driver )
			{
				lastDriver = t.Driver;

				var spacer = new DetailedTrip
				             {
					             DriverAndName         = $"{t.Driver}{DriverName}",
					             ReadyTime             = null,
					             DueTime               = null,
					             PiecesAsString        = "",
					             WeightAsString        = "",
					             MyNotReceivedByDevice = false,
					             MyReceivedByDevice    = false,
					             MyReadByDriver        = false,
					             MyPickedUp            = false,
					             MyDelivered           = false,
					             MyDoNotSendToDriver   = false,
					             MyEnableAccept        = true,
					             CallTime              = null
					             //MyShowExpand = false,
					             //MyShowCollapse = true
				             };

				if( dictDisplayTripsVisibility.ContainsKey( t.Driver ) )
				{
					if( dictDisplayTripsVisibility[ t.Driver ] )
					{
						spacer.MyShowExpand   = false;
						spacer.MyShowCollapse = true;
					}
					else
					{
						spacer.MyShowExpand   = true;
						spacer.MyShowCollapse = false;
					}
				}
				else
				{
					spacer.MyShowExpand   = false;
					spacer.MyShowCollapse = true;
				}
				oc.Add( spacer );
			}

			if( t.IsVisible )
			{
				//Logging.WriteLogLine("DEBUG Adding " + t.TripId);
				var newTrip = new DetailedTrip( t )
				              {
					              MyNotReceivedByDevice = t.NotReceivedByDevice,
					              MyReceivedByDevice    = t.ReceivedByDevice,
					              MyReadByDriver        = t.ReadByDriver,
					              MyPickedUp            = t.PickedUp,
					              MyDelivered           = t.Delivered,
					              MyDoNotSendToDriver   = t.DoNotSendToDriver,
					              MyEnableAccept        = false,
					              MyShowExpand          = false,
					              MyShowCollapse        = false
				              };
				oc.Add( newTrip );
			}
			//else
			//            {
			//	Logging.WriteLogLine("DEBUG NOT Adding " + t.TripId);
			//}
		}

		Dispatcher.Invoke( () =>
		                   {
			                   DisplayTrips = oc;
		                   } );
	}

	public void ShowHideDriversTrips( string driverAndName, bool isVisible, bool updateDisplay = true )
	{
		Dispatcher.Invoke( () =>
		                   {
			                   string? driver;

			                   driver = driverAndName.IndexOf( "--", StringComparison.Ordinal ) > -1 ? driverAndName.SubStr( 0, driverAndName.IndexOf( "--" ) ).Trim() : driverAndName;

			                   SetDriversVisibility( driver, isVisible );

			                   // TODO Take trips out of collection before passing to UpdateDisplayTrips.
			                   // This avoids having to run the filtering again.
			                   foreach( var trip in Trips )
			                   {
				                   if( trip.Driver == driver )
					                   trip.IsVisible = isVisible;
			                   }

			                   if( updateDisplay )
			                   {
				                   //UpdateDisplayTrips(Trips.ToList());
				                   SortTrips();
			                   }
		                   } );
	}

	[DependsUpon( nameof( Trips ) )]
	public void WhenTripsChanges()
	{
		Logging.WriteLogLine( "Trips changed - count: " + Trips.Count + " LastTripCount: " + LastTripCount + " - applying filters" );
		LastTripCount = Trips.Count;

		// Reload the trips
		ObservableCollection<DisplayTrip> tmpTrips = new();
		tmpTrips = Trips;
		//Task.WaitAll(
		//	Task.Run(() =>
		//	{
		//		var Temp = Azure.Client.RequestGetTripsByStatus(new StatusRequest { STATUS.DISPATCHED, STATUS.PICKED_UP, STATUS.DELIVERED }).Result;
		//		tmpTrips = new ObservableCollection<DisplayTrip>(from T in Temp
		//														 select new DisplayTrip(T, GetServiceLevel(T.ServiceLevel)));
		//	})
		//);

		if( dictionaryFilters.ContainsKey( BoardName ) || Filter.IsNotNullOrWhiteSpace() )
		{
			List<DisplayTrip> toRemove = new();
			List<DisplayTrip> toAdd    = new();

			var filters = dictionaryFilters[ BoardName ];

			List<DriversBoardFilter> active = new();

			foreach( var filter in filters )
			{
				if( filter.active )
					active.Add( filter );
			}

			if( active.Count > 0 )
			{
				foreach( var trip in tmpTrips )
				{
					var visible = false;
					trip.IsVisible = false;

					foreach( var filter in active )
					{
						if( DoesTripMatchFilter( trip, filter ) )
						{
							//Logging.WriteLogLine("Trip: " + trip.TripId + " - matches filter: " + filter);
							trip.IsVisible = true;
							visible        = true;

							if( !toAdd.Contains( trip ) )
								toAdd.Add( trip );
							break;
						}
						//Logging.WriteLogLine("Trip: " + trip.TripId + " - doesn't match filter: " + filter);
						trip.IsVisible = false;
						visible        = false;
						toRemove.Add( trip );
					}

					if( ( Filter != null ) && ( Filter.Length > 0 ) )
					{
						if( DoesTripMatchAdHocFilter( trip, Filter.TrimToLower() ) )
						{
							//Logging.WriteLogLine("Trip: " + trip.TripId + " - matches adhoc filter: " + Filter);
							if( !toAdd.Contains( trip ) )
								toAdd.Add( trip );
							visible = true;
						}
						else
						{
							if( toAdd.Contains( trip ) )
							{
								//Logging.WriteLogLine("Trip: " + trip.TripId + " - doesn't match adhoc filter: " + Filter);
								trip.IsVisible = false;
								visible        = false;
								toAdd.Remove( trip );
							}
						}
					}

					//if (visible)
					//	Logging.WriteLogLine("Visible trip: " + trip.TripId);
					//else
					//	Logging.WriteLogLine("Hiding trip: " + trip.TripId);
					trip.IsVisible = visible;
				}
			}
			else
			{
				// No active filters - set all visible
				foreach( var trip in tmpTrips )
				{
					trip.IsVisible = true;
					toAdd.Add( trip );
				}
			}

			if( ( toAdd.Count > 0 ) && Filter.IsNotNullOrWhiteSpace() )
			{
				List<DisplayTrip> doRemove = new();

				foreach( var trip in toAdd )
				{
					//if (DoesTripMatchAdHocFilter(trip, Filter.TrimToLower()))
					//{
					//	Logging.WriteLogLine("Trip: " + trip.TripId + " - matches adhoc filter: " + Filter);
					//}
					//else
					if( !DoesTripMatchAdHocFilter( trip, Filter.TrimToLower() ) )
					{
						//Logging.WriteLogLine("Trip: " + trip.TripId + " - doesn't match adhoc filter: " + Filter);
						trip.IsVisible = false;
						toRemove.Add( trip );
					}
				}

				if( toRemove.Count > 0 )
				{
					foreach( var trip in toRemove )
						toAdd.Remove( trip );
				}
			}

			/*Logging.WriteLogLine("Adding " + toAdd.Count + " trips to Trips");
			Dispatcher.Invoke(() =>
			{
                Trips.Clear();
                //Trips = new ObservableCollection<DisplayTrip>();
				foreach (var trip in toAdd)
				{
					Trips.Add(trip);
				}

				Logging.WriteLogLine("Done adding");
			});*/
			//if (toAdd.Count > 0)
			//{
			//	Dispatcher.Invoke(() =>
			//	{
			//		Trips.Clear();
			//		foreach (var trip in toAdd)
			//		{
			//			Trips.Add(trip);
			//		}

			//		Logging.WriteLogLine("Done adding");
			//	});
			//}
		}

		var Trps = new List<DisplayTrip>( from T in Trips
		                                  orderby T.Driver
		                                  select T ).ToList();

		Dispatcher.Invoke( () =>
		                   {
			                   //UpdateDisplayTrips(Trps);
			                   Trips.Clear();

			                   //Trips = new ObservableCollection<DisplayTrip>();
			                   foreach( var tmp in Trps )
				                   Trips.Add( tmp );
			                   TripCount = Trips.Count;

			                   SortTrips();
			                   // Turned off as it is called in SortTrips
			                   //UpdateDisplayTrips(Trps);

			                   //Trips.Clear();
			                   //var ShipmentCount = Trips.Count(trip => trip.IsVisible);

			                   //Logging.WriteLogLine("Setting expander titles");
			                   //if (view != null)
			                   //{
			                   //	view.SetExpanderTitles();
			                   //}
		                   } );
	}

	[DependsUpon( nameof( SelectedTrips ) )]
	public void WhenSelectedTripsChanges()
	{
		var Trps = SelectedTrips;

		if( Trps is not null )
		{
			TripDetailEnabled = true;

			if( Trps.Count == 1 )
			{
				//if (SelectedTrip != null && SelectedTrip.TripId.IsNotNullOrWhiteSpace())
				if( ( Trps[ 0 ] != null ) && Trps[ 0 ].TripId.IsNotNullOrWhiteSpace() )
				{
					//Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + SelectedTrip.TripId);
					//PreviouslySelectedTrip = SelectedTrip;
					//Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + Trps[0].TripId);
					PreviouslySelectedTrip = Trps[ 0 ];
				}
				//else
				//{
				//	Logging.WriteLogLine("DEBUG Leaving PreviouslySelectedTrip at " + PreviouslySelectedTrip?.TripId);
				//}

				SelectedTrip = Trps[ 0 ];

				//return;
			}
			else
			{
				TripDetailEnabled = false;
				SelectedTrip      = null;
			}
		}
	}

	public void WhenSelectedTripsChanges_V1()
	{
		var Trps = SelectedTrips;

		if( Trps is not null )
		{
			TripDetailEnabled = true;
			var TrpsCount = Trps.Count;

			if( TrpsCount == 1 )
			{
				SelectedTrip = Trps[ 0 ];

				return;
			}

			TripDetailEnabled = false;
			SelectedTrip      = null;
		}
	}


	[DependsUpon350( nameof( SelectedTrip ) )]
	public void WhenSelectedTripChangesDelayed()
	{
		if( !IsInDesignMode )
		{
			var S = SelectedTrip;

			//if( !( S is null ) )
			// Only change if it is a valid trip, not a spacer
			if( ( S != null ) && S.TripId.IsNotNullOrWhiteSpace() )
			{
				SelectedTripDetails = new Trip();

				var TripId = S.TripId;

				Task.Run( async () =>
				          {
					          var Trip = await Azure.Client.RequestGetTrip( new GetTrip
					                                                        {
						                                                        Signatures = true,
						                                                        TripId     = TripId
					                                                        } );

					          Dispatcher.Invoke( () =>
					                             {
						                             SelectedTripDetails = Trip;
					                             } );
				          } );
			}
		}
	}

	[DependsUpon150( nameof( SelectedTrip ) )]
	public void WhenSelectedTripChanges()
	{
		EnableBounce = SelectedTrips is not null && ( SelectedTrips.Count > 0 ) && SelectedTrips[ 0 ].TripId.IsNotNullOrWhiteSpace();
		//if (SelectedTrip != null && SelectedTrip.TripId.IsNotNullOrWhiteSpace())
		//         {
		//	Logging.WriteLogLine("DEBUG Setting PreviouslySelectedTrip to " + SelectedTrip.TripId);
		//	PreviouslySelectedTrip = SelectedTrip;
		//         }
		//else
		//         {
		//	Logging.WriteLogLine("DEBUG Leaving PreviouslySelectedTrip at " + PreviouslySelectedTrip.TripId);
		//}
	}

	[DependsUpon( nameof( SelectedDriver ) )]
	public void WhenSelectedDriverChanges()
	{
		var Driver = SelectedDriver.StaffId;

		foreach( var TripsByDriver in TripsByDrivers )
		{
			if( TripsByDriver.Driver == Driver )
			{
				SelectedTripsByDriver  = TripsByDriver;
				TripsByDriver.Expanded = true;
			}
			else
				TripsByDriver.Expanded = false;
		}
	}

	[DependsUpon( nameof( DriverSearchVisible ) )]
	public void WhenDriverSearchVisibleChanges()
	{
		SaveSettings();
	}

	[DependsUpon( nameof( DriversFilter ) )]
	public void WhenDriversFilterChanges()
	{
		var Filter  = DriversFilter;
		var IsBlank = Filter.IsNullOrWhiteSpace();

		var Hash = new HashSet<string>();

		foreach( var TripsByDriver in TripsByDrivers )
			//Logging.WriteLogLine("DEBUG TripsByDrivers: driver: '" + TripsByDriver.Driver + "'");
			Hash.Add( TripsByDriver.Driver );

		if( !IsBlank )
		{
			foreach( var Driver in Drivers )
			{
				// Ensure that the driver has trips
				if( Hash.Contains( Driver.StaffId ) )
				{
					Driver.IsVisible = IsBlank || ( Driver.StaffId.IndexOf( Filter, StringComparison.CurrentCultureIgnoreCase ) >= 0 )
					                           || ( Driver.DisplayName.IndexOf( Filter, StringComparison.InvariantCultureIgnoreCase ) >= 0 );
				}
			}
		}
		else
			UpdateDriverList();
	}

	public void SetView( DriversBoard driversBoard )
	{
		//Logging.WriteLogLine("Setting view for DriversBoard");
		view = driversBoard;
	}


	//[DependsUpon(nameof(SelectedTrip))]
	//public void WhenSelectedTripChanges()
	//      {
	//	if (SelectedTrip != null)
	//          {
	//		PreviouslySelectedTrip = SelectedTrip;
	//          }
	//      }

#region Pickup and Deliver note editing
	public bool IsEditingPickupNote
	{
		get { return Get( () => IsEditingPickupNote, false ); }
		set { Set( () => IsEditingPickupNote, value ); }
	}

	public bool IsPickupSaveCancelEnabled
	{
		get { return Get( () => IsPickupSaveCancelEnabled, IsEditingPickupNote ); }
		//set { Set(() => IsPickupSaveCancelEnabled, value); }
	}

	[DependsUpon( nameof( IsEditingPickupNote ) )]
	public string GetPickupSaveCancelVisiblity()
	{
		var vis = Visibility.Collapsed;

		if( IsEditingPickupNote )
			vis = Visibility.Visible;

		return vis.ToString();
	}

	public string PickupSaveCancelVisiblity
	{
		get
		{
			var vis = GetPickupSaveCancelVisiblity();
			return vis;
		}
		//get { return Get(() => DeliverySaveCancelVisiblity, GetDeliverySaveCancelVisiblity()); }
		//set { Set(() => PickupSaveCancelVisiblity, value); }
	}

	public bool IsEditingDeliveryNote
	{
		get { return Get( () => IsEditingDeliveryNote, false ); }
		set { Set( () => IsEditingDeliveryNote, value ); }
	}

	public bool IsDeliverySaveCancelEnabled
	{
		get { return Get( () => IsDeliverySaveCancelEnabled, IsEditingDeliveryNote ); }
		//set { Set(() => IsDeliverySaveCancelEnabled, value); }
	}

	[DependsUpon( nameof( IsEditingDeliveryNote ) )]
	public string GetDeliverySaveCancelVisiblity()
	{
		var vis = Visibility.Collapsed;

		if( IsEditingDeliveryNote )
			vis = Visibility.Visible;

		return vis.ToString();
	}

	public string DeliverySaveCancelVisiblity
	{
		get
		{
			var vis = GetDeliverySaveCancelVisiblity();
			return vis;
		}
		//get { return Get(() => DeliverySaveCancelVisiblity, GetDeliverySaveCancelVisiblity()); }
		//set { Set(() => PickupSaveCancelVisiblity, value); }
	}


	private static Trip UpdateBroadcast( Trip t )
	{
		switch( t.Status1 )
		{
		case STATUS.ACTIVE:
			t.BroadcastToDispatchBoard = true;
			break;

		case STATUS.DISPATCHED:
		case STATUS.PICKED_UP:
			t.BroadcastToDispatchBoard = true; // Shipment entry can also dispatch
			t.BroadcastToDriverBoard   = true;
			t.BroadcastToDriver        = true;
			break;
		}

		return t;
	}
#endregion

#region Sorting
	private readonly char   SEPARATOR           = '|';
	public readonly  string SORT_DIRECTION_UP   = "up";
	public readonly  string SORT_DIRECTION_DOWN = "down";

	public readonly int SORT_COLUMN    = 0;
	public readonly int SORT_DIRECTION = 1;

	public string SortColumnDescription
	{
		//get { return Get(() => SortColumnDescription, "Shipment Id|up"); }
		get { return Get( () => SortColumnDescription, "Shipment Id" + SEPARATOR + SORT_DIRECTION_DOWN ); }
		set { Set( () => SortColumnDescription, value ); }
	}


	public string SortDescriptionLabelText
	{
		get { return Get( () => SortDescriptionLabelText, "Shipment Id" ); }
		set { Set( () => SortDescriptionLabelText, value ); }
	}


	public string SortColumn
	{
		get { return Get( () => SortColumn, "" ); }
		set { Set( () => SortColumn, value ); }
	}

	public string SortDirection
	{
		get { return Get( () => SortDirection, SORT_DIRECTION_UP ); }
		set { Set( () => SortDirection, value ); }
	}

	public void ToggleSortDirection()
	{
		if( SortDirection == SORT_DIRECTION_UP )
			SortDirection = SORT_DIRECTION_DOWN;
		else
			SortDirection = SORT_DIRECTION_UP;
	}

	public void SetWindowSort( string columnName = "Shipment Id", string direction = "up" )
	{
		SortColumn            = columnName;
		SortDirection         = direction;
		SortColumnDescription = columnName + SEPARATOR + direction;
		var formattedSortDirection = FindStringResource( "DialoguesDriversCurrentSortAscending" );

		if( SortDirection != SORT_DIRECTION_UP )
			formattedSortDirection = FindStringResource( "DialoguesDriversCurrentSortDescending" );
		SortDescriptionLabelText = columnName + " (" + formattedSortDirection + ")";
		//Logging.WriteLogLine("DEBUG SortDescriptionLabelText: " + SortDescriptionLabelText);
		//Logging.WriteLogLine("DEBUG SortColumnDescription: " + SortColumnDescription);

		SortTrips();
	}

	/// <summary>
	///     Note: This toggles the sort direction.
	/// </summary>
	/// <param
	///     name="columnName">
	/// </param>
	public void SetWindowSort_V1( string columnName = "Shipment Id" )
	{
		//Logging.WriteLogLine("DEBUG Sorting on " + columnName);
		var direction              = SORT_DIRECTION_UP;
		var formattedSortDirection = FindStringResource( "DialoguesDriversCurrentSortAscending" );

		if( SortColumnDescription.IsNotNullOrWhiteSpace() )
		{
			var pieces = SortColumnDescription.Split( SEPARATOR );

			if( pieces.Length == 2 )
			{
				if( pieces[ SORT_COLUMN ] == columnName )
				{
					if( pieces[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						direction              = SORT_DIRECTION_DOWN;
						formattedSortDirection = FindStringResource( "DialoguesDriversCurrentSortDescending" );
					}
				}

				SortDescriptionLabelText = columnName + " (" + formattedSortDirection + ")";
			}
		}
		SortColumnDescription = columnName + SEPARATOR + direction;
		//Logging.WriteLogLine("DEBUG SortDescriptionLabelText: " + SortDescriptionLabelText);
		//Logging.WriteLogLine("DEBUG SortColumnDescription: " + SortColumnDescription);

		SortTrips();
	}

	public string GetSortDescription()
	{
		var desc = string.Empty;

		return desc;
	}

	public string[] GetWindowSort()
	{
		var result = new string[ 2 ];

		if( SortColumnDescription.IsNotNullOrWhiteSpace() )
		{
			var pieces = SortColumnDescription.Split( SEPARATOR );

			if( pieces.Length == 2 )
				result = pieces;
		}
		else
		{
			result[ SORT_COLUMN ]    = "Shipment Id";
			result[ SORT_DIRECTION ] = SORT_DIRECTION_UP;
			SortColumnDescription    = result[ SORT_COLUMN ] + SEPARATOR + result[ SORT_DIRECTION ];
		}

		return result;
	}

	public void SortTrips()
	{
		var sort = GetWindowSort();
		var dts  = new List<DisplayTrip>();
		var sd   = BuildDictionaryOfDriversAndTrips();

		//Logging.WriteLogLine("DEBUG sorting on " + sort[SORT_COLUMN] + " " + sort[SORT_DIRECTION]);
		foreach( var driver in sd.Keys )
		{
			// TODO Check for visibility
			if( sd[ driver ].IsNotNull() && ( sd[ driver ].Count > 0 ) )
			{
				var newOrder = new List<DisplayTrip>();

				switch( sort[ SORT_COLUMN ] )
				{
				case "Shipment Id":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.TripId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.TripId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Acct":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.AccountId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.AccountId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Company Name":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.BillingCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.BillingCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Acct Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.BillingNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.BillingNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "CZ":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.CurrentZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.CurrentZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Pickup":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Suite":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Street":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU City":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Prov/State":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Country":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Postal/Zip":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Addr Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PZ":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PickupZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PickupZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Delivery":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Suite":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Street":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del City":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Prov/State":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Country":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Postal/Zip":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Addr Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "DZ":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DeliveryZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DeliveryZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Pkg":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.PackageType, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.PackageType, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Serv":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.ServiceLevel, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.ServiceLevel, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Call Time":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.CallTime ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.CallTime ) );
					}
					break;

				case "Ready":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.ReadyTime ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.ReadyTime ) );
					}
					break;

				case "Due":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( n => n.DueTime ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( n => n.DueTime ) );
					}
					break;

				case "Pcs":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( v => v.Pieces ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( v => v.Pieces ) );
					}
					break;

				case "Wt":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderBy( v => v.Weight ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 sd[ driver ].OrderByDescending( v => v.Weight ) );
					}
					break;

				// Just in case
				default:
					newOrder = new List<DisplayTrip>(
					                                 sd[ driver ].OrderBy( n => n.TripId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					break;
				}

				//Logging.WriteLogLine("DEBUG newOrder.Count: " + newOrder.Count);
				dts.AddRange( newOrder );
			}
		}

		Dispatcher.Invoke( () =>
		                   {
			                   //Logging.WriteLogLine("DEBUG dts.Count: " + dts.Count);
			                   UpdateDisplayTrips( dts );
		                   } );
	}

	public void SortTrips_V1()
	{
		var sort = GetWindowSort();
		var dts  = new List<DisplayTrip>();

		foreach( var tbd in TripsByDrivers )
		{
			// TODO Check for visibility
			if( tbd.Trips.IsNotNull() && ( tbd.Trips.Count > 0 ) )
			{
				var newOrder = new List<DisplayTrip>();

				//Logging.WriteLogLine("DEBUG sorting on " + sort[SORT_COLUMN] + sort[SORT_DIRECTION]);
				switch( sort[ SORT_COLUMN ] )
				{
				case "Shipment Id":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.TripId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.TripId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Acct":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.AccountId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.AccountId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Company Name":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.BillingCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.BillingCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Acct Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.BillingNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.BillingNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "CZ":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.CurrentZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.CurrentZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Pickup":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Suite":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Street":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU City":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Prov/State":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Country":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Postal/Zip":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Addr Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PU Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "PZ":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PickupZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PickupZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Delivery":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryCompanyName, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Suite":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressSuite, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Street":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressAddressLine1, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del City":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressCity, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Prov/State":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressRegion, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Country":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressCountry, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Postal/Zip":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressPostalCode, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Addr Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryAddressNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Del Notes":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryNotes, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "DZ":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DeliveryZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DeliveryZone, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Pkg":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.PackageType, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.PackageType, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Serv":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.ServiceLevel, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.ServiceLevel, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					}
					break;

				case "Call Time":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.CallTime ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.CallTime ) );
					}
					break;

				case "Ready":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.ReadyTime ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.ReadyTime ) );
					}
					break;

				case "Due":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( n => n.DueTime ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( n => n.DueTime ) );
					}
					break;

				case "Pcs":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( v => v.Pieces ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( v => v.Pieces ) );
					}
					break;

				case "Wt":
					if( sort[ SORT_DIRECTION ] == SORT_DIRECTION_UP )
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderBy( v => v.Weight ) );
					}
					else
					{
						newOrder = new List<DisplayTrip>(
						                                 tbd.Trips.OrderByDescending( v => v.Weight ) );
					}
					break;

				// Just in case
				default:
					newOrder = new List<DisplayTrip>(
					                                 tbd.Trips.OrderBy( n => n.TripId, Comparer<string>.Create( ( x, y ) => string.Compare( x, y ) ) ) );
					break;
				}

				//Logging.WriteLogLine("DEBUG newOrder.Count: " + newOrder.Count);
				dts.AddRange( newOrder );
			}
		}

		Dispatcher.Invoke( () =>
		                   {
			                   //Logging.WriteLogLine("DEBUG dts.Count: " + dts.Count);
			                   UpdateDisplayTrips( dts );
		                   } );
	}

	private SortedDictionary<string, List<DisplayTrip>> BuildDictionaryOfDriversAndTrips()
	{
		var sd = new SortedDictionary<string, List<DisplayTrip>>();

		lock( UpdateLock )
		{
			foreach( var t in Trips )
			{
				var driver = t.Driver;

				if( !sd.ContainsKey( driver ) )
					sd.Add( driver, new List<DisplayTrip>() );
				sd[ driver ].Add( t );
			}
		}

		return sd;
	}
#endregion

#region Filter
	public string Filter
	{
		get { return Get( () => Filter, "" ); }
		set { Set( () => Filter, value ); }
	}

	[DependsUpon350( nameof( Filter ) )]
	public void WhenFilterChanges()
	{
		WhenTripsChanges();
		var visible = 0;

		foreach( var t in Trips )
		{
			if( t.IsVisible )
				++visible;
		}
		TripCount = visible; // Count;
	}

	public void WhenFilterChanges_V2()
	{
		//WhenTripsChange();
		//TripCount = Count;
		var adhocFilter = Filter.Trim().ToLower();

		List<DisplayTrip> toRemove = new();

		if( adhocFilter.IsNotNullOrWhiteSpace() )
		{
			foreach( var trip in Trips )
			{
				var visible = false;
				trip.IsVisible = false;

				if( ( adhocFilter != null ) && ( adhocFilter.Length > 0 ) )
				{
					if( DoesTripMatchAdHocFilter( trip, adhocFilter ) )
					{
						//Logging.WriteLogLine("Trip: " + trip.TripId + " - matches adhoc filter: " + adhocFilter);
						visible = true;
					}
					else
					{
						if( !toRemove.Contains( trip ) )
						{
							//Logging.WriteLogLine("Trip: " + trip.TripId + " - doesn't match adhoc filter: " + adhocFilter);
							visible = false;
							toRemove.Add( trip );
						}
						//else
						//{
						//	Logging.WriteLogLine("toRemove already contains: " + trip.TripId);

						//}
					}
				}

				//if (visible)
				//	Logging.WriteLogLine("Visible trip: " + trip.TripId);
				//else
				//	Logging.WriteLogLine("Hiding trip: " + trip.TripId);
				trip.IsVisible = visible;
			}
		}

		//Logging.WriteLogLine("Removing " + toRemove.Count + " trips from Trips");
		if( toRemove.Count > 0 )
		{
			//            List<DisplayTrip> tmp = new List<DisplayTrip>();
			//            foreach (var trip in Trips)
			//            {
			//	if (trip.IsVisible)
			//                {
			//		tmp.Add(trip);
			//                }
			//            }

			//Trips.Clear();
			//foreach (var trip in tmp)
			//            {
			//	Trips.Add(trip);
			//            }

			//foreach (var trip in toRemove)
			//{
			//	Logging.WriteLogLine("Removing " + trip.TripId);
			//	Trips.Remove(trip);
			//}
		}
		else
		{
			// No active filters - set all visible
			foreach( var tbd in TripsByDrivers )
			{
				if( ( tbd.Trips != null ) && ( tbd.Trips.Count > 0 ) )
				{
					foreach( var trip in tbd.Trips )
						trip.IsVisible = true;
				}
			}
		}

		//foreach (var trip in Trips)
		//{
		//	if (trip.IsVisible)
		//             {
		//		Logging.WriteLogLine("Still left: " + trip.TripId);
		//             }
		//}

		var Trps = new List<DisplayTrip>( from T in Trips
		                                  orderby T.Driver
		                                  where T.IsVisible = true
		                                  select T ).ToList();

		//Logging.WriteLogLine("Trips left: " + Trps.Count);
		var oc = new ObservableCollection<DisplayTrip>();

		foreach( var trip in Trps )
			oc.Add( trip );

		//Trips.Clear();
		Trips = new ObservableCollection<DisplayTrip>();
		//foreach (var tmp in Trps)
		//{
		//	Logging.WriteLogLine("Adding trip: " + tmp.TripId);
		//	Trips.Add(tmp);
		//}
		Trips = oc;

		TripCount = Trips.Count;

		var ShipmentCount = Trips.Count( trip => trip.IsVisible );

		//TotalShipmentCount = ShipmentCount;

		//Logging.WriteLogLine("Setting expander titles");
		//if (view != null)
		//{
		//	view.SetExpanderTitles();
		//}
	}


	public void WhenFilterChanges_V1()
	{
		var F = Filter.Trim().ToLower();

		var Count = 0;

		foreach( var Trip in Trips )
		{
			Trip.Filter = F;

			if( Trip.IsVisible )
				Count++;
		}

		TripCount = Count;
	}
#endregion

#region Actions
	public void Execute_CollapseAll()
	{
		foreach( var TripsByDriver in TripsByDrivers )
			TripsByDriver.Expanded = false;
	}

	public void Execute_ExpandAll()
	{
		foreach( var TripsByDriver in TripsByDrivers )
			TripsByDriver.Expanded = true;
	}

	public async void Execute_BounceTrip()
	{
		await UpdateStatusAndRemove( STATUS.ACTIVE );
	}


	public async void Execute_DeleteTrip()
	{
		await UpdateStatusAndRemove( STATUS.DELETED );
	}


	public void Execute_AuditTrip()
	{
		Globals.RunProgram( Globals.TRIP_AUDIT_TRAIL, SelectedTrip?.TripId ?? "" );
	}

	public void Execute_ShowSettings()
	{
	}

	public void Execute_RouteShipments()
	{
		if( ( SelectedTrips != null ) && ( SelectedTrips.Count > 0 ) )
		{
			var          tabNumber = 0;
			var          tis       = Globals.DataContext.MainDataContext.ProgramTabItems;
			PageTabItem? pti       = null;

			if( tis.Count > 0 )
			{
				for( tabNumber = 0; tabNumber < tis.Count; tabNumber++ )
				{
					var ti = tis[ tabNumber ];

					if( ti.Content is RouteShipments.RouteShipments )
					{
						pti = ti;
						break;
					}
				}
			}

			if( pti == null )
			{
				Logging.WriteLogLine( "Opening new " + Globals.ROUTE_SHIPMENTS + " tab and loading " + SelectedTrips.Count + " trips" );
				Globals.RunProgram( Globals.ROUTE_SHIPMENTS, SelectedTrips );

				tis = Globals.DataContext.MainDataContext.ProgramTabItems;

				for( tabNumber = 0; tabNumber < tis.Count; tabNumber++ )
				{
					var ti = tis[ tabNumber ];

					if( ti.Content is RouteShipments.RouteShipments )
					{
						pti = ti;

						break;
					}
				}
			}

			if( pti != null )
			{
				Logging.WriteLogLine( "Loading " + SelectedTrips.Count + " trips in " + Globals.ROUTE_SHIPMENTS );
				( (RouteShipmentsModel)pti.Content.DataContext ).LoadTrips( SelectedTrips );
				( (RouteShipmentsModel)pti.Content.DataContext ).OnArgumentChange( SelectedTrips );
			}

			Globals.DataContext.MainDataContext.TabControl.SelectedIndex = tabNumber;
		}
	}

	public void SaveTrip( DisplayTrip trip )
	{
		Tasks.RunVoid( async () =>
		               {
			               Logging.WriteLogLine( "Saving trip " + trip.TripId );
			               Trip tmp = new( trip );
			               await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( tmp ) );
		               } );
	}

	public static void AssignTripToNewDriver( string newDriver, DisplayTrip trip )
	{
		Tasks.RunVoid( async () =>
		               {
			               async Task SetStatus( string driver, STATUS status )
			               {
				               var Update = new TripUpdateStatus
				                            {
					                            Status       = status,
					                            DeviceStatus = TripUpdateStatusBase.DEVICE_STATUS.UNSET,
					                            Broadcast    = TripUpdateStatus.BROADCAST.DRIVER | TripUpdateStatus.BROADCAST.DRIVERS_BOARD | TripUpdateStatus.BROADCAST.SEARCH_TRIPS,
					                            Driver       = driver,
					                            Program      = PROGRAM,
					                            S1           = trip.S2,
					                            S2           = trip.S3
				                            };
				               Update.TripIdList.Add( trip.TripId );
				               await Azure.Client.RequestUpdateTripStatus( Update );
			               }

			               await SetStatus( trip.Driver, STATUS.ACTIVE );          // Remove from device
			               await SetStatus( newDriver, (STATUS)(int)trip.Status ); // To new device
		               } );
	}

	public void AssignNewDriverToSelectedTrips( string newDriver )
	{
		if( SelectedTrips is {Count: > 0} )
		{
			foreach( var Dt in SelectedTrips )
			{
				Logging.WriteLogLine("Changing assigned driver from " + Dt.Driver + " to " + newDriver + " for trip " + Dt.TripId);
				AssignTripToNewDriver( newDriver, Dt );
			}
		}
	}
#endregion

#region Settings Persistence
	public class ColumnSettings
	{
		public string       BoardName { get; set; } = string.Empty;
		public List<string> Columns   { get; set; } = new();
	}

	public class FiltersSettings
	{
		public string BoardName { get; set; } = string.Empty;

		//public List<DispatchFilter> Filters { get; set; } = new List<DispatchFilter>();
		public List<string> Filters { get; set; } = new();
	}

	public class Settings
	{
		public Dictionary<string, ColumnSettings>  DriversColumnSettings { get; set; } = new();
		public Dictionary<string, FiltersSettings> DriversFilterSettings { get; set; } = new();
	}

	private string MakeSettingsFileName()
	{
		var AppData    = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		return $"{BaseFolder}\\DriversBoard.{SETTINGS_FILE_EXTENSION}";
	}

	public new void LoadSettings()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var FileName = MakeSettingsFileName();

				if( File.Exists( FileName ) )
				{
					Logging.WriteLogLine( "Loading settings from " + FileName );
					var SerializedObject = File.ReadAllText( FileName );
					//SerializedObject = Encryption.Decrypt(SerializedObject);
					DriversBoardSettings = JsonConvert.DeserializeObject<Settings>( SerializedObject );

					if( DriversBoardSettings is not null )
					{
						if( DriversBoardSettings.DriversColumnSettings.ContainsKey( BoardName ) )
						{
							var activeCols = DriversBoardSettings.DriversColumnSettings[ BoardName ].Columns;

							foreach( var col in activeCols )
							{
								if( !dictionaryColumnVisiblity.ContainsKey( col ) )
									dictionaryColumnVisiblity.Add( col, Visibility.Visible );
								else
									dictionaryColumnVisiblity[ col ] = Visibility.Visible;
							}
						}
						else
						{
							var activeCols = GetAllDriversColumnsInOrder();

							foreach( var col in activeCols )
							{
								//dictionaryColumnVisiblity[col] = Visibility.Visible;
								if( !dictionaryColumnVisiblity.ContainsKey( col ) )
									dictionaryColumnVisiblity.Add( col, Visibility.Visible );
								else
									dictionaryColumnVisiblity[ col ] = Visibility.Visible;
							}
						}

						if( DriversBoardSettings.DriversFilterSettings.ContainsKey( BoardName ) )
						{
							List<DriversBoardFilter> dfs           = new();
							var                      activeFilters = DriversBoardSettings.DriversFilterSettings[ BoardName ].Filters;

							foreach( var filter in activeFilters )
							{
								DriversBoardFilter df = new( filter );
								dfs.Add( df );
							}

							if( !dictionaryFilters.ContainsKey( BoardName ) )
								dictionaryFilters.Add( BoardName, dfs );
							else
								dictionaryFilters[ BoardName ] = dfs;
						}
					}
				}
				else
				{
					var activeCols = GetAllDriversColumnsInOrder();

					foreach( var col in activeCols )
					{
						if( !dictionaryColumnVisiblity.ContainsKey( col ) )
							dictionaryColumnVisiblity[ col ] = Visibility.Visible;
						else
							dictionaryColumnVisiblity[ col ] = Visibility.Visible;
					}
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "ERROR: unable to load settings: " + e );

				// Doing this to ensure that the board will load if the file is corrupted
				Logging.WriteLogLine( "Deleting bad settings file for Drivers Board" );
				DriversBoardSettings = new Settings();
				SaveSettings( DriversBoardSettings );
			}
		}
	}

	private void SaveSettings( Settings settings )
	{
		if( !IsInDesignMode )
		{
			try
			{
				var SerializedObject = JsonConvert.SerializeObject( settings );

				//Logging.WriteLogLine("Saved settings:\n" + SerializedObject);
				//SerializedObject = Encryption.Encrypt(SerializedObject);
				var fileName = MakeSettingsFileName();
				Logging.WriteLogLine( "Saving settings to " + fileName );
				File.WriteAllText( fileName, SerializedObject );
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "ERROR: unable to save settings: " + e );
			}
		}
	}
#endregion

#region Settings Columns
	//
	// Visibility properties for columns
	//


	public Visibility AccountVisibility
	{
		get { return Get( () => AccountVisibility, Visibility.Visible ); }
		set { Set( () => AccountVisibility, value ); }
	}

	public bool BoolAccountVisibility
	{
		get { return Get( () => BoolAccountVisibility, true ); }
		set { Set( () => BoolAccountVisibility, value ); }
	}

	public Visibility CompanyNameVisibility
	{
		get { return Get( () => CompanyNameVisibility, Visibility.Visible ); }
		set { Set( () => CompanyNameVisibility, value ); }
	}

	public Visibility AccountNotesVisibility
	{
		get { return Get( () => AccountNotesVisibility, Visibility.Visible ); }
		set { Set( () => AccountNotesVisibility, value ); }
	}

	public Visibility CurrentZoneVisibility
	{
		get { return Get( () => CurrentZoneVisibility, Visibility.Visible ); }
		set { Set( () => CurrentZoneVisibility, value ); }
	}

	public Visibility ReadyTimeConditionVisibility
	{
		get { return Get( () => ReadyTimeConditionVisibility, Visibility.Visible ); }
		set { Set( () => ReadyTimeConditionVisibility, value ); }
	}

	public Visibility PickupCompanyVisibility
	{
		get { return Get( () => PickupCompanyVisibility, Visibility.Visible ); }
		set { Set( () => PickupCompanyVisibility, value ); }
	}

	public Visibility PickupSuiteVisibility
	{
		get { return Get( () => PickupSuiteVisibility, Visibility.Visible ); }
		set { Set( () => PickupSuiteVisibility, value ); }
	}

	public Visibility PickupStreetVisibility
	{
		get { return Get( () => PickupStreetVisibility, Visibility.Visible ); }
		set { Set( () => PickupStreetVisibility, value ); }
	}

	public Visibility PickupCityVisibility
	{
		get { return Get( () => PickupCityVisibility, Visibility.Visible ); }
		set { Set( () => PickupCityVisibility, value ); }
	}

	public Visibility PickupProvStateVisibility
	{
		get { return Get( () => PickupProvStateVisibility, Visibility.Visible ); }
		set { Set( () => PickupProvStateVisibility, value ); }
	}

	public Visibility PickupCountryVisibility
	{
		get { return Get( () => PickupCountryVisibility, Visibility.Visible ); }
		set { Set( () => PickupCountryVisibility, value ); }
	}

	public Visibility PickupPostalZipVisibility
	{
		get { return Get( () => PickupPostalZipVisibility, Visibility.Visible ); }
		set { Set( () => PickupPostalZipVisibility, value ); }
	}

	public Visibility PickupAddressNotesVisibility
	{
		get { return Get( () => PickupAddressNotesVisibility, Visibility.Visible ); }
		set { Set( () => PickupAddressNotesVisibility, value ); }
	}

	public Visibility PickupNotesVisibility
	{
		get { return Get( () => PickupNotesVisibility, Visibility.Visible ); }
		set { Set( () => PickupNotesVisibility, value ); }
	}

	public Visibility PickupZoneVisibility
	{
		get { return Get( () => PickupZoneVisibility, Visibility.Visible ); }
		set { Set( () => PickupZoneVisibility, value ); }
	}

	public Visibility DueTimeConditionVisibility
	{
		get { return Get( () => DueTimeConditionVisibility, Visibility.Visible ); }
		set { Set( () => DueTimeConditionVisibility, value ); }
	}

	public Visibility DeliveryCompanyVisibility
	{
		get { return Get( () => DeliveryCompanyVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryCompanyVisibility, value ); }
	}

	public Visibility DeliverySuiteVisibility
	{
		get { return Get( () => DeliverySuiteVisibility, Visibility.Visible ); }
		set { Set( () => DeliverySuiteVisibility, value ); }
	}

	public Visibility DeliveryStreetVisibility
	{
		get { return Get( () => DeliveryStreetVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryStreetVisibility, value ); }
	}

	public Visibility DeliveryCityVisibility
	{
		get { return Get( () => DeliveryCityVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryCityVisibility, value ); }
	}

	public Visibility DeliveryProvStateVisibility
	{
		get { return Get( () => DeliveryProvStateVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryProvStateVisibility, value ); }
	}

	public Visibility DeliveryCountryVisibility
	{
		get { return Get( () => DeliveryCountryVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryCountryVisibility, value ); }
	}

	public Visibility DeliveryPostalZipVisibility
	{
		get { return Get( () => DeliveryPostalZipVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryPostalZipVisibility, value ); }
	}

	public Visibility DeliveryAddressNotesVisibility
	{
		get { return Get( () => DeliveryAddressNotesVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryAddressNotesVisibility, value ); }
	}

	public Visibility DeliveryNotesVisibility
	{
		get { return Get( () => DeliveryNotesVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryNotesVisibility, value ); }
	}

	public Visibility DeliveryZoneVisibility
	{
		get { return Get( () => DeliveryZoneVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryZoneVisibility, value ); }
	}

	public Visibility ServiceLevelVisibility
	{
		get { return Get( () => ServiceLevelVisibility, Visibility.Visible ); }
		set { Set( () => ServiceLevelVisibility, value ); }
	}

	public Visibility PackageTypeVisibility
	{
		get { return Get( () => PackageTypeVisibility, Visibility.Visible ); }
		set { Set( () => PackageTypeVisibility, value ); }
	}

	public Visibility CallTimeVisibility
	{
		get { return Get( () => CallTimeVisibility, Visibility.Visible ); }
		set { Set( () => CallTimeVisibility, value ); }
	}

	public Visibility ReadyTimeVisibility
	{
		get { return Get( () => ReadyTimeVisibility, Visibility.Visible ); }
		set { Set( () => ReadyTimeVisibility, value ); }
	}

	public Visibility DueTimeVisibility
	{
		get { return Get( () => DueTimeVisibility, Visibility.Visible ); }
		set { Set( () => DueTimeVisibility, value ); }
	}

	public Visibility PiecesVisibility
	{
		get { return Get( () => PiecesVisibility, Visibility.Visible ); }
		set { Set( () => PiecesVisibility, value ); }
	}

	public Visibility WeightVisibility
	{
		get { return Get( () => WeightVisibility, Visibility.Visible ); }
		set { Set( () => WeightVisibility, value ); }
	}

	public bool BoolWeightVisibility
	{
		get { return Get( () => BoolWeightVisibility, true ); }
		set { Set( () => BoolWeightVisibility, value ); }
	}

	public Visibility BoardVisibility
	{
		get { return Get( () => BoardVisibility, Visibility.Visible ); }
		set { Set( () => BoardVisibility, value ); }
	}

	public Visibility PalletsVisibility
	{
		get { return Get( () => PalletsVisibility, Visibility.Visible ); }
		set { Set( () => PalletsVisibility, value ); }
	}

	//[DependsUpon(nameof(AccountVisibility))]
	//[DependsUpon(nameof(CompanyNameVisibility))]
	//[DependsUpon(nameof(AccountNotesVisibility))]
	//[DependsUpon(nameof(CurrentZoneVisibility))]
	//[DependsUpon(nameof(PickupCompanyVisibility))]
	//[DependsUpon(nameof(PickupSuiteVisibility))]
	//[DependsUpon(nameof(PickupStreetVisibility))]
	//[DependsUpon(nameof(PickupCityVisibility))]
	//[DependsUpon(nameof(PickupProvStateVisibility))]
	//[DependsUpon(nameof(PickupCountryVisibility))]
	//[DependsUpon(nameof(PickupPostalZipVisibility))]
	//[DependsUpon(nameof(PickupAddressNotesVisibility))]
	//[DependsUpon(nameof(PickupNotesVisibility))]
	//[DependsUpon(nameof(PickupZoneVisibility))]
	//[DependsUpon(nameof(DeliveryCompanyVisibility))]
	//[DependsUpon(nameof(DeliverySuiteVisibility))]
	//[DependsUpon(nameof(DeliveryStreetVisibility))]
	//[DependsUpon(nameof(DeliveryCityVisibility))]
	//[DependsUpon(nameof(DeliveryProvStateVisibility))]
	//[DependsUpon(nameof(DeliveryCountryVisibility))]
	//[DependsUpon(nameof(DeliveryPostalZipVisibility))]
	//[DependsUpon(nameof(DeliveryAddressNotesVisibility))]
	//[DependsUpon(nameof(DeliveryNotesVisibility))]
	//[DependsUpon(nameof(DeliveryZoneVisibility))]
	//[DependsUpon(nameof(ServiceLevelVisibility))]
	//[DependsUpon(nameof(PackageTypeVisibility))]
	//[DependsUpon(nameof(ReadyTimeVisibility))]
	//[DependsUpon(nameof(DueTimeVisibility))]
	//[DependsUpon(nameof(PiecesVisibility))]
	//[DependsUpon(nameof(WeightVisibility))]
	//[DependsUpon(nameof(BoardVisibility))]
	//[DependsUpon(nameof(PalletsVisibility))]
	//public void WhenVisibilityChanges()
	//      {
	//	string line = "AccountVisibility: " + AccountVisibility;
	//	line += " CompanyNameVisibility: " + CompanyNameVisibility;
	//	line += " CurrentZoneVisibility: " + CurrentZoneVisibility;
	//	line += " PickupCompanyVisibility: " + PickupCompanyVisibility;
	//	line += " PickupSuiteVisibility: " + PickupSuiteVisibility;
	//	line += " PickupStreetVisibility: " + PickupStreetVisibility;
	//	line += " PickupCityVisibility: " + PickupCityVisibility;
	//	line += " PickupProvStateVisibility: " + PickupProvStateVisibility;
	//	line += " PickupCountryVisibility: " + PickupCountryVisibility;
	//	line += " PickupPostalZipVisibility: " + PickupPostalZipVisibility;
	//	line += " PickupAddressNotesVisibility: " + PickupAddressNotesVisibility;
	//	line += " PickupNotesVisibility: " + PickupNotesVisibility;
	//	line += " PickupZoneVisibility: " + PickupZoneVisibility;
	//	line += " DeliveryCompanyVisibility: " + DeliveryCompanyVisibility;
	//	line += " DeliverySuiteVisibility: " + DeliverySuiteVisibility;
	//	line += " DeliveryStreetVisibility: " + DeliveryStreetVisibility;
	//	line += " DeliveryCityVisibility: " + DeliveryCityVisibility;
	//	line += " DeliveryProvStateVisibility: " + DeliveryProvStateVisibility;
	//	line += " DeliveryCountryVisibility: " + DeliveryCountryVisibility;
	//	line += " DeliveryPostalZipVisibility: " + DeliveryPostalZipVisibility;
	//	line += " DeliveryAddressNotesVisibility: " + DeliveryAddressNotesVisibility;
	//	line += " DeliveryNotesVisibility: " + DeliveryNotesVisibility;
	//	line += " DeliveryZoneVisibility: " + DeliveryZoneVisibility;
	//	line += " ServiceLevelVisibility: " + ServiceLevelVisibility;
	//	line += " PackageTypeVisibility: " + PackageTypeVisibility;
	//	line += " ReadyTimeVisibility: " + ReadyTimeVisibility;
	//	line += " DueTimeVisibility: " + DueTimeVisibility;
	//	line += " PiecesVisibility: " + PiecesVisibility;
	//	line += " WeightVisibility: " + WeightVisibility;
	//	line += " BoardVisibility: " + BoardVisibility;
	//	line += " PalletsVisibility: " + PalletsVisibility;

	//	//Logging.WriteLogLine("Column visibility changed : " + line);
	//}

	// End of Visibility

	public List<string> GetAllDriversColumnsInOrder() => AllDriversColumnsInOrder;


	/// <summary>
	/// </summary>
	/// <param
	///     name="column">
	/// </param>
	/// <returns></returns>
	public string MapColumnToTripField( string column )
	{
		var fieldName = string.Empty;

		if( ( column != null ) && ( column != string.Empty ) && dictionaryColumns.ContainsKey( column ) )
			fieldName = dictionaryColumns[ column ];

		return fieldName;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="column">
	/// </param>
	/// <returns></returns>
	public List<string>? MapColumnToConditions( string column )
	{
		List<string>? conditions = null;

		if( ( column != string.Empty ) && dictionaryConditionsForField.ContainsKey( column ) )
			conditions = dictionaryConditionsForField[ column ];

		return conditions;
	}

	public Visibility MapColumnToColumnVisibility( string column )
	{
		var vis = Visibility.Visible;

		if( column.IsNotNullOrWhiteSpace() && dictionaryColumnVisiblity.ContainsKey( column ) )
			vis = dictionaryColumnVisiblity[ column ];

		return vis;
	}

	public void SaveDriversColumns( List<string> newColumns )
	{
		List<string> keys = new();

		foreach( var key in dictionaryColumnVisiblity.Keys )
			keys.Add( key );

		foreach( var key in keys )
		{
			if( newColumns.Contains( key ) )
			{
				//Logging.WriteLogLine("Setting column :" + key + " to Visible");
				dictionaryColumnVisiblity[ key ] = Visibility.Visible;
			}
			else
			{
				//Logging.WriteLogLine("Setting column :" + key + " to Hidden");
				dictionaryColumnVisiblity[ key ] = Visibility.Hidden;
			}
		}

		foreach( var key in keys )
		{
			switch( key )
			{
			case "DialoguesDriversColumnDriver":
			case "DialoguesDriversColumnTripId":
				break;

			case "DialoguesDriversColumnAcct":
				AccountVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnCompanyName":
				CompanyNameVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnAcctNotes":
				AccountNotesVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnCZ":
				CurrentZoneVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickup":
				PickupCompanyVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupSuite":
				PickupSuiteVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupStreet":
				PickupStreetVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupCity":
				PickupCityVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupProvState":
				PickupProvStateVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupCountry":
				PickupCountryVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupPostalZip":
				PickupPostalZipVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupAddressNotes":
				PickupAddressNotesVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPickupNotes":
				PickupNotesVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPZ":
				PickupZoneVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDelivery":
				DeliveryCompanyVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliverySuite":
				DeliverySuiteVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryStreet":
				DeliveryStreetVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryCity":
				DeliveryCityVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryProvState":
				DeliveryProvStateVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryCountry":
				DeliveryCountryVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryPostalZip":
				DeliveryPostalZipVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryAddressNotes":
				DeliveryAddressNotesVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDeliveryNotes":
				DeliveryNotesVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDZ":
				DeliveryZoneVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnServ":
				ServiceLevelVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPkg":
				PackageTypeVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnCall":
				CallTimeVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnReady":
				ReadyTimeVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnDue":
				DueTimeVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnPcs":
				PiecesVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnWt":
				WeightVisibility = dictionaryColumnVisiblity[ key ];
				break;

			case "DialoguesDriversColumnBoard":
				BoardVisibility = dictionaryColumnVisiblity[ key ];
				break;

			//case "DialoguesDriversColumnPlts":
			//	PalletsVisibility = dictionaryColumnVisiblity[key];
			//	break;
			}
		}

		ColumnSettings cs = new()
		                    {
			                    BoardName = BoardName,
			                    Columns   = newColumns
		                    };

		if( DriversBoardSettings == null )
			DriversBoardSettings = new Settings();

		if( DriversBoardSettings.DriversColumnSettings.ContainsKey( BoardName ) )
			DriversBoardSettings.DriversColumnSettings[ BoardName ] = cs;
		else
			DriversBoardSettings.DriversColumnSettings.Add( BoardName, cs );

		SaveSettings( DriversBoardSettings );
	}
#endregion

#region Abstracts
	protected override void OnUpdateTrip( Trip trip )
	{
		if( !IgnoreTripReceive )
		{
			var TripId = trip.TripId;

			switch( trip.Status1 )
			{
			case STATUS.PICKED_UP:
			case STATUS.DISPATCHED:
			case STATUS.DELIVERED:
				Dispatcher.Invoke( () =>
				                   {
					                   lock( UpdateLock )
					                   {
						                   try
						                   {
							                   var Dt = new DisplayTrip( trip, GetServiceLevel( trip.ServiceLevel ) );

							                   var Ndx   = 0;
							                   var Found = false;

							                   foreach( var Trip in Trips )
							                   {
								                   if( Trip.TripId == TripId )
								                   {
									                   Found = true;

									                   break;
								                   }

								                   ++Ndx;
							                   }

							                   if( Found )
							                   {
								                   Trips.RemoveAt( Ndx );
								                   Trips.Insert( Ndx, Dt );
							                   }
							                   else
								                   Trips.Add( Dt );

							                   TripCount = Trips.Count;

							                   var Driver = trip.Driver;
							                   Found = false;

							                   foreach( var TripsByDriver in TripsByDrivers )
							                   {
								                   if( TripsByDriver.Driver == Driver )
								                   {
									                   Found = true;
									                   var TFound = false;
									                   var Trps   = TripsByDriver.Trips;

									                   for( var I = Trps.Count; --I >= 0; )
									                   {
										                   if( Trps[ I ].TripId == TripId )
										                   {
											                   TFound    = true;
											                   Trps[ I ] = Dt;

											                   break;
										                   }
									                   }

									                   if( !TFound )
										                   Trps.Add( Dt );

									                   break;
								                   }
							                   }

							                   if( !Found )
							                   {
								                   if( DriverDict.TryGetValue( Driver, out var DriverName ) )
									                   DriverName = $" -- {DriverName}";

								                   var NewDriver = new TripsByDriver
								                                   {
									                                   Driver        = Driver,
									                                   DriverAndName = $"{Driver}{DriverName}"
								                                   };
								                   NewDriver.Trips.Add( Dt );
								                   TripsByDrivers.Add( NewDriver );
							                   }

							                   UpdateDriverList();
						                   }
						                   catch( Exception Exception )
						                   {
							                   Logging.WriteLogLine( Exception );
						                   }
						                   RaisePropertyChanged( nameof( Trips ) );
					                   }
				                   } );
				break;

			default:
				lock( UpdateLock )
					RemoveTrip( trip.TripId );
				break;
			}
		}
	}

	protected override void OnRemoveTrip( string tripId )
	{
		Dispatcher.Invoke( () =>
		                   {
			                   var Trps = Trips;

			                   for( var I = Trps.Count; --I >= 0; )
			                   {
				                   if( Trps[ I ].TripId == tripId )
				                   {
					                   Trps.RemoveAt( I );
					                   break;
				                   }
			                   }

			                   if( SelectedTrip is not null && ( SelectedTrip.TripId == tripId ) )
			                   {
				                   SelectedTrip = null;
				                   WhenSelectedTripChangesDelayed();
			                   }

			                   var Found   = false;
			                   var Removed = false;

			                   var Tbd = TripsByDrivers;

			                   for( var I = Tbd.Count; --I >= 0; )
			                   {
				                   var Tps = Tbd[ I ].Trips;

				                   for( var J = Tps.Count; --J >= 0; )
				                   {
					                   if( Tps[ J ].TripId == tripId )
					                   {
						                   Tps.RemoveAt( J );
						                   Found = true;
						                   break;
					                   }
				                   }

				                   if( Tps.Count == 0 )
				                   {
					                   Tbd.RemoveAt( I );
					                   Removed = true;
				                   }

				                   if( Found )
					                   break;
			                   }

			                   if( Found || Removed )
			                   {
				                   RaisePropertyChanged( nameof( Trips ) );
				                   RaisePropertyChanged( nameof( TripsByDrivers ) );
			                   }
		                   } );
	}

	private void FixUpDisplayTrips( string tripId, STATUS status, bool received, bool read )
	{
		var Trip = ( from T in DisplayTrips
		             where T.TripId == tripId
		             select T ).FirstOrDefault();

		if( Trip is not null )
		{
			switch( status )
			{
			case < STATUS.PICKED_UP:
				bool NotReceived;

				if( read )
				{
					received    = false;
					NotReceived = false;
				}
				else
					NotReceived = !received;

				Trip.MyReceivedByDevice    = received;
				Trip.MyNotReceivedByDevice = NotReceived;
				Trip.MyReadByDriver        = read;
				break;

			default:
				switch( status )
				{
				case STATUS.PICKED_UP:
					Trip.MyDelivered = false;
					Trip.MyPickedUp  = true;
					break;

				case <= STATUS.VERIFIED:
					Trip.MyDelivered = true;
					Trip.MyPickedUp  = false;
					break;

				default:
					Trip.MyDelivered = false;
					Trip.MyPickedUp  = false;
					break;
				}
				Trip.MyReceivedByDevice    = false;
				Trip.MyNotReceivedByDevice = false;
				Trip.MyReadByDriver        = false;
				break;
			}
		}
	}

	protected override void OnUpdateStatus( string tripId, STATUS status, STATUS1 status1, STATUS2 status2, bool receivedByDevice, bool readByDriver )
	{
		void Update( bool delete )
		{
			lock( UpdateLock )
			{
				var Driver = "";
				var Trps   = Trips;

				for( var I = Trps.Count; --I >= 0; )
				{
					var T = Trps[ I ];

					if( T.TripId == tripId )
					{
						Driver = T.Driver;

						if( delete )
							Trps.RemoveAt( I );
						break;
					}
				}

				if( Driver.IsNotNullOrWhiteSpace() )
				{
					var ByDrivers = TripsByDrivers;

					for( var I = ByDrivers.Count; --I >= 0; )
					{
						var TripsByDriver = ByDrivers[ I ];

						if( TripsByDriver.Driver == Driver )
						{
							var Tps = TripsByDriver.Trips;

							for( var J = Tps.Count; --J >= 0; )
							{
								var T = Tps[ J ];

								if( T.TripId == tripId )
								{
									if( delete )
										Tps.RemoveAt( J );
									else
									{
										T.ReceivedByDevice = receivedByDevice;
										T.ReadByDriver     = readByDriver;
										T.Status           = DisplayTrip.ToDisplayTripStatus( status, status1 );

										if( SelectedTrip is not null && ( SelectedTrip.TripId == tripId ) )
											WhenSelectedTripChangesDelayed();

										FixUpDisplayTrips( tripId, status, receivedByDevice, readByDriver );
									}
									break;
								}
							}
						}
					}
				}
				UpdateDriverList();
			}
		}

		Dispatcher.Invoke( () =>
		                   {
			                   switch( status )
			                   {
			                   case STATUS.ACTIVE when ( status2 & STATUS2.WAS_UNDELIVERABLE ) != 0:
				                   Update( true );
				                   break;

			                   default:
				                   if( status != STATUS.ACTIVE )
					                   Update( status >= STATUS.DELIVERED );
				                   break;
			                   }
		                   } );
	}
#endregion

#region Settings Filters
	public ObservableCollection<DisplayTrip> SaveDriversFilters( List<DriversBoardFilter> filters )
	{
		ObservableCollection<DisplayTrip> Trps = new();

		if( ( filters != null ) && ( filters.Count > 0 ) )
		{
			// Update dictionaryFilters
			if( dictionaryFilters.ContainsKey( BoardName ) )
				dictionaryFilters[ BoardName ] = filters;
			else
				dictionaryFilters.Add( BoardName, filters );

			List<string> dfs = new();

			foreach( var filter in filters )
			{
				var converted = filter.ToString();
				dfs.Add( converted );
			}

			FiltersSettings fs = new()
			                     {
				                     BoardName = BoardName,
				                     Filters   = dfs
			                     };

			if( DriversBoardSettings == null )
				DriversBoardSettings = new Settings();

			if( DriversBoardSettings.DriversFilterSettings.ContainsKey( BoardName ) )
				DriversBoardSettings.DriversFilterSettings.Remove( BoardName );
			DriversBoardSettings.DriversFilterSettings.Add( BoardName, fs );

			SaveSettings( DriversBoardSettings );

			Task.WaitAll(
			             Task.Run( () =>
			                       {
				                       // Get all trips again
				                       var Temp = Azure.Client.RequestGetTripsByStatus( new StatusRequest {STATUS.DISPATCHED, STATUS.PICKED_UP, STATUS.DELIVERED} ).Result;

				                       Trps = new ObservableCollection<DisplayTrip>( from T in Temp
				                                                                     select new DisplayTrip( T, GetServiceLevel( T.ServiceLevel ) ) );
				                       TripCount = Trps.Count;

				                       TripsByDrivers = new ObservableCollection<TripsByDriver>( from T in Trps
				                                                                                 orderby T.Driver
				                                                                                 group T by T.Driver
				                                                                                 into G
				                                                                                 select new TripsByDriver {Driver = G.Key, Trips = new ObservableCollection<DisplayTrip>( G )} );

				                       lock( UpdateLock )
				                       {
					                       //Trips = Trps;
					                       //Trips.Clear();
					                       //foreach (var trip in Trps)
					                       //                     {
					                       //	Trips.Add(trip);
					                       //                     }
					                       DriverDict = Drivers.ToDictionary( driver => driver.StaffId, driver => driver.DisplayName );

					                       foreach( var TripsByDriver in TripsByDrivers )
					                       {
						                       if( DriverDict.TryGetValue( TripsByDriver.Driver, out var DriverName ) )
							                       DriverName = $" -- {DriverName}";

						                       TripsByDriver.DriverAndName = $"{TripsByDriver.Driver}{DriverName}";
					                       }
				                       }
				                       UpdateDriverList();
			                       } )
			            );
			SortTrips();
		}
		return Trps;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="value1">
	/// </param>
	/// <param
	///     name="condition">
	/// </param>
	/// <param
	///     name="value2">
	/// </param>
	/// <returns></returns>
	private static bool DoesStringMatch( string value1, string condition, string value2 )
	{
		var yes = false;

		if( value1 != null )
		{
			value1 = value1.TrimToLower();
			value2 = value2.TrimToLower();

			if( condition == EQUALS )
				yes = value1 == value2;
			else if( condition == NOT_EQUAL_TO )
				yes = value1 != value2;
			else if( condition == IN )
			{
				var pieces = value2.Split( InConditionListDelimeter );

				foreach( var piece in pieces )
				{
					if( piece.Trim() == value1 )
					{
						yes = true;
						break;
					}
				}
			}
			else if( condition == CONTAINS )
			{
				if( value1.IndexOf( value2 ) > -1 )
					yes = true;
			}
			else if( condition == STARTS_WITH )
			{
				if( value1.StartsWith( value2 ) )
					yes = true;
			}
			else if( condition == ENDS_WITH )
			{
				if( value1.EndsWith( value2 ) )
					yes = true;
			}
		}

		return yes;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="value1">
	/// </param>
	/// <param
	///     name="condition">
	/// </param>
	/// <param
	///     name="value2">
	/// </param>
	/// <returns></returns>
	private static bool DoesDecimalMatch( decimal value1, string condition, string value2 )
	{
		var yes = false;

		try
		{
			var val = decimal.Parse( value2 );

			switch( condition )
			{
			case "=":
				yes = value1 == val;
				break;

			case "!=":
				yes = value1 != val;
				break;

			case ">":
				yes = value1 > val;
				break;

			case "<":
				yes = value1 < val;
				break;
			}
		}
		catch( Exception e )
		{
			Logging.WriteLogLine( "Match failed: " + e );
		}

		return yes;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="value1">
	/// </param>
	/// <param
	///     name="condition">
	/// </param>
	/// <param
	///     name="value2">
	/// </param>
	/// <returns></returns>
	private static bool DoesDateTimeMatch( DateTime value1, string condition, string value2 )
	{
		var yes = false;

		// TODO

		return yes;
	}

	public bool IsColumnNumeric( string column )
	{
		var yes = false;

		var field = MapColumnToTripField( column );

		switch( field.ToLower() )
		{
		case "pieces":
			yes = true;
			break;

		case "weight":
			yes = true;
			break;

		case "board":
			yes = true;
			break;
		}

		return yes;
	}

	public DriversBoardFilter? LookUpFilter( string colName )
	{
		DriversBoardFilter? df = null;

		if( ( dictionaryFilters != null ) && ( dictionaryFilters.Count > 0 ) && dictionaryFilters.ContainsKey( BoardName ) )
		{
			foreach( var tmp in dictionaryFilters[ BoardName ] )
			{
				if( tmp.columnName == colName )
				{
					//Logging.WriteLogLine("Found matching filter: " + tmp);
					df = tmp;
					break;
				}
			}
		}

		return df;
	}

	/// <summary>
	///     Represents 1 filter for a dispatch board.
	/// </summary>
	public class DriversBoardFilter
	{
		public string? windowName { get; set; }
		public string? columnName { get; set; }
		public string? condition  { get; set; }
		public string? value      { get; set; }
		public bool    active     { get; set; }

		/// <summary>
		/// </summary>
		/// <param
		///     name="windowName">
		/// </param>
		/// <param
		///     name="columnName">
		/// </param>
		/// <param
		///     name="condition">
		/// </param>
		/// <param
		///     name="value">
		/// </param>
		/// <param
		///     name="active">
		/// </param>
		public DriversBoardFilter( string windowName, string columnName, string condition, string value, bool active )
		{
			this.windowName = windowName;
			this.columnName = columnName;
			this.condition  = condition;
			this.value      = value;
			this.active     = active;
		}

		/// <summary>
		/// </summary>
		/// <param
		///     name="value">
		/// </param>
		public DriversBoardFilter( string toConvert )
		{
			FromString( toConvert );
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			var converted = windowName + "|" + columnName + "|" + condition + "|" + value + "|" + active;
			return converted;
		}

		/// <summary>
		/// </summary>
		/// <param
		///     name="toConvert">
		/// </param>
		public void FromString( string toConvert )
		{
			var pieces = toConvert.Split( '|' );

			if( pieces.Length == 5 )
			{
				windowName = pieces[ 0 ];
				columnName = pieces[ 1 ];
				condition  = pieces[ 2 ];
				value      = pieces[ 3 ];
				active     = bool.Parse( pieces[ 4 ] );
			}
			else
				Logging.WriteLogLine( "Invalid DriverFilter: " + toConvert );
		}
	}
#endregion

#region Update Conditions Timer
	private DispatcherTimer? updateConditionsTimer;

	public bool IsUpdateConditionsRunning()
	{
		var yes = false;

		if( ( updateConditionsTimer != null ) && updateConditionsTimer.IsEnabled )
			yes = true;

		return yes;
	}

	/// <summary>
	///     Stops the update addresses timer.
	/// </summary>
	public void StopUpdateConditions()
	{
		if( updateConditionsTimer != null )
		{
			Logging.WriteLogLine( "Stopping timer" );
			updateConditionsTimer.Stop();
		}
	}

	/// <summary>
	///     Starts the update conditions timer.
	/// </summary>
	public void StartUpdateConditions()
	{
		Logging.WriteLogLine( "Starting timer" );

		// var updateTripsDelegate = new TimerCallback(UpdateTripsChecker);
		// var updateTripsChecker = new UpdateTripsChecker();

		// Run every 15 seconds
		// timerUpdateTrips = new Timer(updateTripsChecker.UpdateTrips, null, 0, 15 * 1000);

		updateConditionsTimer          =  new DispatcherTimer();
		updateConditionsTimer.Tick     += UpdateConditions_Tick;
		updateConditionsTimer.Interval =  new TimeSpan( 0, 1, 0 );
		updateConditionsTimer.Start();

		Trips.CollectionChanged += ( e, o ) => TripCountMonitor();
	}

	public void TripCountMonitor()
	{
		Logging.WriteLogLine( "DEBUG Trips.Count: " + Trips.Count + " - was: " + LastTripCount );
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void UpdateConditions_Tick( object sender, EventArgs e )
	{
		//Logging.WriteLogLine("UpdateConditions fired");

		var newTrips = new ObservableCollection<DisplayTrip>();

		foreach( var trip in Trips )
		{
			//Logging.WriteLogLine("Setting conditions for trip: " + trip.TripId);
			// KLUDGE for the moment
			try
			{
				trip.ReadyTimeCondition.BuildCondition();
				trip.DueTimeCondition.BuildCondition();
			}
			catch( Exception ex )
			{
				Logging.WriteLogLine( "ERROR - Exception thrown building conditions" + ex );
			}
			newTrips.Add( trip );
		}

		Logging.WriteLogLine( "DEBUG newTrips.Count: " + newTrips.Count + " - was: " + LastTripCount );
		LastTripCount = newTrips.Count;

		Dispatcher.Invoke( () =>
		                   {
			                   TripCount = newTrips.Count;

			                   Trips.Clear();

			                   //Trips = newTrips;
			                   foreach( var Trip in newTrips )
				                   Trips.Add( Trip );
		                   } );
	}

	private void UpdateConditions_Tick_V1( object sender, EventArgs e )
	{
		//Logging.WriteLogLine("UpdateConditions fired");

		var newTrips = new ObservableCollection<DisplayTrip>();

		foreach( var trip in Trips )
		{
			//Logging.WriteLogLine("Setting conditions for trip: " + trip.TripId);
			// KLUDGE for the moment
			try
			{
				trip.ReadyTimeCondition.BuildCondition();
				trip.DueTimeCondition.BuildCondition();
			}
			catch( Exception ex )
			{
				Logging.WriteLogLine( "ERROR - Exception thrown building conditions" + ex );
			}
			newTrips.Add( trip );
		}

		Dispatcher.Invoke( () =>
		                   {
			                   Trips = newTrips;
		                   } );
	}
#endregion
}