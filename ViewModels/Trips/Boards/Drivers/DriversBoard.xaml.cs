﻿#nullable enable

using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.Boards;

/// <summary>
///     Interaction logic for DriversBoard.xaml
/// </summary>
public partial class DriversBoard : Page, IDisposable
{
	private string OldDeliveryNotes = string.Empty;

	private string OldPickupNotes = string.Empty;

	private readonly DriversBoardViewModel Model;

	public void SetColumnSpanForDriverNameRows()
	{
		/*
		foreach( var D in Model.DisplayTrips )
		{
			if( D.DriverAndName.IsNotNullOrWhiteSpace() )
			{
				// Set ColumnSpan for this row
				foreach( var Col in DataGrid.Columns )
				{
					// col.con
				}
			}
		}
		*/
	}

	public void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	public void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}

	public DriversBoard()
	{
		Initialized += ( _, _ ) =>
					   {
						   var M = (DriversBoardViewModel)DataContext;
						   M.SetView( this );

						   DataGrid.Initialized += ( _, _ ) =>
												   {
												   };

						   DataGrid.Loaded += ( _, _ ) =>
										      {
											      var Temp = DataGrid.FindChildren<ScrollViewer>();
										      };

						   //	               ScrollViewer = Temp[ 0 ];

						   if( !M.IsUpdateConditionsRunning() )
							   M.StartUpdateConditions();
					   };
		InitializeComponent();
		Model = (DriversBoardViewModel)DataContext;

		var Columns = Model.GetAllDriversColumnsInOrder();

		foreach( var Col in DataGrid.Columns )
		{
			//Logging.WriteLogLine("Looking at column: " + col.Header);
			foreach( var Key in Columns )
			{
				var Header = FindStringResource( Key );

				if( ( Col.Header != null ) && ( Col.Header.ToString() == Header ) )
				{
					Col.Visibility = Model.dictionaryColumnVisiblity[ Key ];
					break;
				}
			}
		}

		DataGrid.FrozenColumnCount = 3;

		// Load the settings tab
		Model.LoadSettings();
		PopulateSettingsColumnList();
		PopulateFilterList();

		//DataGrid_Sorting(null, null);

		//ICollectionView cvTrips = CollectionViewSource.GetDefaultView(DataGrid.ItemsSource);
		//if (cvTrips != null && cvTrips.CanGroup == true)
		//{
		//	cvTrips.GroupDescriptions.Clear();
		//	cvTrips.GroupDescriptions.Add(new PropertyGroupDescription("Driver"));
		//}

		//SetExpanderTitles();
	}

	public void Dispose()
	{
        Mouse.OverrideCursor = null;
        try
        {
            if (Model != null)
            {
                Logging.WriteLogLine($"Disposing of {Model.BoardName}");
                Model.StopUpdateConditions();
                Model.Dispose();
            }
        }
        catch { }
    }

	private void DataGrid_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( sender is DataGrid Grid )
		{
			var Trips = Model.SelectedTrips;

			if( Trips is null )
				Trips = new List<DisplayTrip>();
			else
				Trips.Clear();

			Model.SelectedTrips = null;
			Trips.AddRange( Grid.SelectedItems.Cast<DisplayTrip>() );
			Model.SelectedTrips = Trips;
		}
	}


	private void ToggleButton_Checked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var Grid = Tb.FindParent<DataGrid>();

			if( Grid is not null )
			{
				var Ndx = Grid.SelectedIndex;

				if( Ndx >= 0 )
				{
					var Itm = (DisplayTrip)Grid.SelectedItem;
					Itm.DetailsVisible = true;

					var Row = Grid.GetRow( Ndx );

					if( Row is not null )
						Grid.ScrollIntoView( Row );
				}
			}
		}
	}

	private void ToggleButton_Unchecked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var Grid = Tb.FindParent<DataGrid>();

			if( Grid?.SelectedItem is DisplayTrip Itm )
				Itm.DetailsVisible = false;
		}
	}

	private void DataGrid_LostFocus( object sender, RoutedEventArgs e )
	{
		if( sender is DataGrid Grid )
			Grid.UnselectAll();
	}

	//private async void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	private void DataGrid_MouseDoubleClick( object? sender, MouseButtonEventArgs? e )
	{
		//DisplayTrip selectedTrip = (DisplayTrip)this.DataGrid.SelectedItem;
		if( Model.SelectedTrip is not null )
		{
			Logging.WriteLogLine( "Selected trip: " + Model.SelectedTrip.TripId );

			// Check to see if user is double-clicking the expand/collapse row
			if( Model.SelectedTrip is DetailedTrip Dt )
			{
				if( Dt.DriverAndName.IsNotNullOrWhiteSpace() )
				{
					if( e is not null )
						e.Handled = true;
				}
				else
				{
					//DisplayTrip latestVersion = GetLatestVersion(selectedTrip.TripId);
					//// TODO Checking for TripId in case the trip is in Storage
					//if (latestVersion.TripId.IsNotNullOrWhiteSpace() && latestVersion != null && selectedTrip.LastModified.ToLongTimeString() != latestVersion.LastModified.ToLongTimeString())
					//{
					//	Logging.WriteLogLine("Latest version - was : " + selectedTrip.LastModified.ToLongTimeString() + ", now: " + latestVersion.LastModified.ToLongTimeString());
					//	selectedTrip = latestVersion;
					//	if (DataContext is SearchTripsModel Model)
					//	{
					//		Dispatcher.Invoke(() =>
					//		{
					//			Model.UpdateTripsWithLatestVersion(selectedTrip);
					//		});
					//	}
					//}

					var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

					if( Tis.Count > 0 )
					{
						PageTabItem? Pti = null;
						// CD1-T24 Open in new tab
						//for (var i = 0; i < tis.Count; i++)
						//{
						//	var ti = tis[i];
						//	if (ti.Content is TripEntry.TripEntry)
						//	{
						//		pti = ti;
						//		// Load the trip into the first TripEntry tab
						//		Logging.WriteLogLine("Displaying trip in extant " + Globals.TRIP_ENTRY + " tab");

						//		//( (TripEntry.TripEntryModel)pti.Content.DataContext ).Execute_ClearTripEntry();
						//		((TripEntry.TripEntryModel)pti.Content.DataContext).CurrentTrip = Model.SelectedTrip;
						//		await ((TripEntry.TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
						//		Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

						//		break;
						//	}
						//}

						if( Pti == null )
						{
							// Need to create a new Trip Entry tab
							Logging.WriteLogLine( "Opening new " + Globals.TRIP_ENTRY + " tab" );
							Globals.RunNewProgram( Globals.TRIP_ENTRY, Model.SelectedTrip );
						}
					}
				}
			}
		}
	}

	private void BtnSaveColumns_Click( object sender, RoutedEventArgs e )
	{
		//Logging.WriteLogLine("BtnSaveColumns clicked");
		var Columns    = Model.GetAllDriversColumnsInOrder();
		var NewColumns = new List<string>();

		var I = 1; // TripId column is always visible

		foreach( var Cb in CbColumns )
		{
			var Tmp = Cb.IsChecked ?? false;

			if( Tmp )
				NewColumns.Add( Columns[ I ] );
			++I;
		}

		Model.SaveDriversColumns( NewColumns );

		foreach( var Col in DataGrid.Columns )
		{
			//Logging.WriteLogLine("col.Header: " + col.Header);
			foreach( var Name in Columns )
			{
				var Header = FindStringResource( Name );

				if( ( Header != null ) && ( Col.Header != null ) && ( Col.Header.ToString() == Header ) )
				{
					Col.Visibility = Model.dictionaryColumnVisiblity[ Name ];
					break;
				}
			}
		}

		PopulateSettingsColumnList();

		//this.AddColumns();
		//ICollectionView cvTrips = CollectionViewSource.GetDefaultView(DataGrid.ItemsSource);
		//if (cvTrips != null && cvTrips.CanGroup == true)
		//{
		//	cvTrips.GroupDescriptions.Clear();
		//	cvTrips.GroupDescriptions.Add(new PropertyGroupDescription("Driver"));
		//}

		tabControl.SelectedIndex = 0;
	}

	private void BtnSelectAll_Click( object sender, RoutedEventArgs e )
	{
		foreach( var Cb in CbColumns )
			Cb.IsChecked = true;
	}

	private void BtnSelectNone_Click( object sender, RoutedEventArgs e )
	{
		foreach( var Cb in CbColumns )
			Cb.IsChecked = false;
	}

	private void BtnApplyFilters_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Saving Filters" );
		var List = new List<DriversBoardViewModel.DriversBoardFilter>();

		for( var I = 0; I < FilterIsActiveList.Count; I++ )
		{
			if( FilterValueList[ I ].Text.Trim().Length > 0 )
			{
				var Df = new DriversBoardViewModel.DriversBoardFilter( Model.BoardName,
																	   FilterLabelList[ I ].Name,
																	   (string)FilterConditionList[ I ].SelectedValue,
																	   FilterValueList[ I ].Text.Trim(),
																	   FilterIsActiveList[ I ].IsChecked ?? false );
				List.Add( Df );
			}
			else
			{
				// Make sure the active checkbox is turned off
				FilterIsActiveList[ I ].IsChecked = false;
			}
		}

		Dispatcher.Invoke( () =>
						   {
							   //DataGrid.ItemsSource = null;

							   var Trps = Model.SaveDriversFilters( List );

							   Model.Trips = Trps;
							   //DataGrid.ItemsSource = Model.Trips;
							   //DataGrid.ItemsSource = Model.TripsByDrivers;
						   } );

		tabControl.SelectedIndex = 0;
	}

	private void DataGrid_Sorting( object sender, DataGridSortingEventArgs e )
	{
		var ColumnHeader = string.Empty;

		if( e.IsNull() )
			ColumnHeader = "Shipment Id";
		else
			ColumnHeader = (string)e.Column.Header;

		Logging.WriteLogLine( "Sorting by column: " + ColumnHeader );

		DataGrid.ItemsSource = null;

		//var header = FindStringResource("DialoguesDriversColumnTripId");
		var Trps = RemoveDriversLines( Model.Trips );

		switch( ColumnHeader )
		{
		case "Shipment Id":
			Trps = new List<DisplayTrip>( from T in Trps
										  orderby T.Driver, T.TripId
										  select T ).ToList();
			break;

		case "Acct":
			Trps = new List<DisplayTrip>( from T in Trps
										  orderby T.Driver, T.AccountId.ToLower()
										  select T ).ToList();
			break;

		case "Company Name":
			Trps = new List<DisplayTrip>( from T in Trps
										  orderby T.Driver, T.BillingCompanyName.ToLower()
										  select T ).ToList();
			break;

		case "Pickup":
			Trps = new List<DisplayTrip>( from T in Trps
										  orderby T.Driver, T.PickupCompanyName.ToLower()
										  select T ).ToList();
			break;
		}

		Model.CurrentlySortedColumn = ColumnHeader;
		Model.Trips.Clear();
		var Driver = string.Empty;

		foreach( var Tmp in Trps )
		{
			if( Tmp.Driver != Driver )
			{
				Driver = Tmp.Driver;

				var Dt = new DisplayTrip
						 {
							 Driver = Driver
						 };
				Model.Trips.Add( Dt );
			}
			//Logging.WriteLogLine("driver: " + tmp.Driver + " - PickupCompanyName: " + tmp.PickupCompanyName);
			Model.Trips.Add( Tmp );
		}

		e.Handled = true;

		//DataGrid.ItemsSource = Model.Trips;
		DataGrid.ItemsSource = Model.TripsByDrivers;
	}

	private static List<DisplayTrip> RemoveDriversLines( IEnumerable<DisplayTrip> trips )
	{
		return trips.Where( trip => trip.TripId.IsNotNullOrWhiteSpace() ).ToList();
	}


	private void MiCollapseAll_Click( object? sender, RoutedEventArgs? e )
	{
		//HandleExpanders(false);
		DataGrid.Visibility = Visibility.Hidden;

		Model.SetAllDriversVisibility( false );

		DataGrid.Visibility = Visibility.Visible;
		DataGrid.InvalidateVisual();
	}

	private void MiExpandAll_Click( object? sender, RoutedEventArgs? e )
	{
		//HandleExpanders(true);
		DataGrid.Visibility = Visibility.Hidden;

		Model.SetAllDriversVisibility( true );

		DataGrid.Visibility = Visibility.Visible;
		DataGrid.InvalidateVisual();
	}

	//private void HandleExpanders(bool isExpanded)
	//{
	//	var children = DataGrid.GetChildren();

	//	foreach (var ctrl in children)
	//	{
	//		if (ctrl is Expander expander)
	//		{
	//			expander.IsExpanded = isExpanded;
	//		}
	//	}

	//}

	private void LvDrivers_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		//Logging.WriteLogLine("SelectedDriver: " + Model.SelectedDriver);

		var Children = DataGrid.GetChildren();
		var Found    = false;

		foreach( var Ctrl in Children )
		{
			if( Ctrl is Expander Expander )
			{
				var Children2 = Expander.GetChildren();

				foreach( var Ctrl2 in Children2 )
				{
					if( Ctrl2 is DataGridCellsPanel Panel )
					{
						var Children3 = Panel.GetChildren();

						foreach( var Ctrl3 in Children3 )
						{
							if( Ctrl3 is DataGridCell Cell )
							{
								if( Cell.Content is ContentPresenter Pres )
								{
									if( Pres.Content is DisplayTrip Trip )
									{
										if( Trip.Driver == Model.SelectedDriver.StaffId )
										{
											//Logging.WriteLogLine("found driver in trip: " + trip.TripId);
											//expander.Header = trip.Driver;
											Expander.IsExpanded = true;
											Panel.Focus();

											// Make sure that the target is visible
											DataGrid.ScrollIntoView( Expander );

											Found = true;
											break;
										}
									}
								}
							}
						}
					}

					if( Found )
						break;
				}
			}

			if( Found )
				break;
		}

		//SetExpanderTitles();
	}

	//public void SetExpanderTitles()
	//{
	//	var children = DataGrid.GetChildren();
	//	//bool found = false;
	//	foreach (var ctrl in children)
	//	{
	//		if (ctrl is Expander expander)
	//		{
	//			var children2 = expander.GetChildren();
	//			foreach (var ctrl2 in children2)
	//			{
	//				if (ctrl2 is DataGridCellsPanel panel)
	//				{
	//					var children3 = panel.GetChildren();
	//					foreach (var ctrl3 in children3)
	//					{
	//						if (ctrl3 is DataGridCell cell)
	//						{
	//							if (cell.Content is ContentPresenter pres)
	//							{
	//								if (pres.Content is DisplayTrip trip)
	//								{
	//									var header = trip.Driver;
	//									foreach (var driver in Model.Drivers)
	//									{
	//										if (driver.StaffId == header)
	//										{
	//											header += " -- " + driver.DisplayName;
	//										}
	//									}
	//									expander.Header = header;

	//									//found = true;
	//									break;
	//								}
	//							}
	//						}
	//					}
	//				}
	//				// This expander is done
	//				//if (found)
	//				//{
	//				//	break;
	//				//}
	//			}
	//		}
	//	}
	//}


	private void DataGrid_LoadingRowDetails( object sender, DataGridRowDetailsEventArgs e )
	{
		//SetExpanderTitles();
		//Logging.WriteLogLine("DEBUG");
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		MiCollapseAll_Click( null, null );
	}

	private void Button_Click_1( object sender, RoutedEventArgs e )
	{
		MiExpandAll_Click( null, null );
	}

	private void Help_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Opening " + Model.HelpUri );
		var Uri = new Uri( Model.HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	private void DataGridColumnHeader_Click( object sender, RoutedEventArgs e )
	{
		var ColumnHeader = (DataGridColumnHeader)sender;

		if( !ColumnHeader.IsFrozen || ( (string)ColumnHeader.Content == "Shipment Id" ) )
		{
			//Logging.WriteLogLine("DEBUG Sorting on " + columnHeader.Content);
			if( DataContext is DriversBoardViewModel Model )
			{
				if( Model.SortColumn == (string)ColumnHeader.Content )
				{
					// Same column - toggle
					Model.ToggleSortDirection();
					Model.SetWindowSort( (string)ColumnHeader.Content, Model.SortDirection );
				}
				else
				{
					// Sorting on different column - set to ascending
					Model.SetWindowSort( (string)ColumnHeader.Content, Model.SORT_DIRECTION_UP );
				}
			}
		}

		e.Handled = true;
	}

	private void Label_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		var Dc = ( (Label)sender ).DataContext;

		if( Dc is DetailedTrip Dt )
		{
			//Logging.WriteLogLine("DEBUG Double-clicked: " + ((Label)sender).Content + " DriverAndName: " + dt.DriverAndName);
			if( Dt.DriverAndName.IsNotNullOrWhiteSpace() )
			{
				e.Handled = true;

				if( DataContext is DriversBoardViewModel Model )
				{
					if( Dt.MyShowCollapse )
					{
						Dt.MyShowCollapse = false;
						Dt.MyShowExpand   = true;
						Model.ShowHideDriversTrips( Dt.DriverAndName, false );
					}
					else
					{
						Dt.MyShowCollapse = true;
						Dt.MyShowExpand   = false;
						Model.ShowHideDriversTrips( Dt.DriverAndName, true );
					}
				}
			}
		}
	}

	private void LvDrivers_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		//SortedDictionary<string, string> sd = new();
		Logging.WriteLogLine( "DEBUG: SelectedDriver: " + Model.SelectedDriver );
		//var name = Model.SelectedDriver.StaffId + " -- " + Model.SelectedDriver.DisplayName;
		var DriverStaffId = Model.SelectedDriver.StaffId;

		//for (int i = 0; i < Model.DisplayTrips.Count; i++)
		for( var I = 0; I < DataGrid.Items.Count; I++ )
		{
			var Dt = Model.DisplayTrips[ I ];

			//if (!sd.ContainsKey(dt.Driver))
			//               {
			//	sd.Add(dt.Driver, dt.DriverAndName);
			//               }
			//if (dt.DriverAndName == name)
			if( Dt.Driver == DriverStaffId )
			{
				//
				// From https://social.technet.microsoft.com/wiki/contents/articles/21202.wpf-programmatically-selecting-and-focusing-a-row-or-cell-in-a-datagrid.aspx
				//
				DataGrid.SelectedItems.Clear();
				/* set the SelectedItem property */
				var RowIndex = 0;

				// If at top of list, make sure that the driver is visible, otherwise move to first trip for driver
				if( I > 0 )
				{
					//rowIndex = i + 1;
					RowIndex = I - 1;
				}
				var Item = DataGrid.Items[ RowIndex ];
				DataGrid.SelectedItem = Item;

				if( DataGrid.ItemContainerGenerator.ContainerFromIndex( RowIndex ) is not DataGridRow )
				{
					DataGrid.Visibility = Visibility.Hidden;
					/* bring the data item into view
					 * in case it has been virtualized away */
					DataGrid.ScrollIntoView( Item );

					DataGrid.Visibility = Visibility.Visible;
					DataGrid.InvalidateVisual();
				}

				break;
			}
		}

		//foreach (string key in sd.Keys)
		//            {
		//	Logging.WriteLogLine("DEBUG driver: " + key + " driverAndName: " + sd[key]);
		//            }
	}

	private void RouteShipments_Click( object sender, RoutedEventArgs e )
	{
		//if (DataContext is DriversBoardViewModel Model)
		//         {
		//	Model.RouteShipments();
		//         }
		RouteShipmentsButton_Click( null, null );
	}

	private void RouteShipmentsButton_Click( object? sender, RoutedEventArgs? e )
	{
		SetWaitCursor();

		Model.Execute_RouteShipments();

		ClearWaitCursor();
	}

	private void MiShowHideDrivers_Click( object sender, RoutedEventArgs e )
	{
		Model.DriverSearchVisible = !Model.DriverSearchVisible;
	}

	private void ShowToolbar_Click( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void Toolbar_MouseDoubleClick( object? sender, MouseButtonEventArgs? e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void HideToolbar_Click( object sender, RoutedEventArgs e )
	{
		Toolbar_MouseDoubleClick( null, null );
	}

	private void MiShipmentsBounce_Click( object sender, RoutedEventArgs e )
	{
		Model.Execute_BounceTrip();
	}

	private void MiShipmentsDelete_Click( object sender, RoutedEventArgs e )
	{
		Model.Execute_DeleteTrip();
	}

	private void MiShipmentsRoute_Click( object sender, RoutedEventArgs e )
	{
		RouteShipmentsButton_Click( null, null );
	}

	private void BtnEditDeliveryNote_Click( object? sender, RoutedEventArgs? e )
	{
		if( Model.SelectedTrips is {Count: > 0} && Model.SelectedTrips[ 0 ].TripId.IsNotNullOrWhiteSpace() )
		{
			Model.IsEditingDeliveryNote = true;
			TbDeliveryNote.IsReadOnly   = false;
			TbDeliveryNote.Focus();
			BtnCancelEditDeliveryNote.Visibility = BtnSaveDeliveryNote.Visibility = Visibility.Visible;
			BtnEditDeliveryNote.Visibility       = Visibility.Collapsed;
			BtnEditPickupNote.Visibility         = Visibility.Hidden;

			//OldDeliveryNotes = Model.SelectedTrips[0].DeliveryNotes;
			OldDeliveryNotes = TbDeliveryNote.Text;
		}
	}

	private void TbDeliveryNote_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		BtnEditDeliveryNote_Click( null, null );
	}

	private void BtnSaveDeliveryNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingDeliveryNote          = false;
		TbDeliveryNote.IsReadOnly            = true;
		BtnCancelEditDeliveryNote.Visibility = BtnSaveDeliveryNote.Visibility = Visibility.Collapsed;
		BtnEditDeliveryNote.Visibility       = Visibility.Visible;
		BtnEditPickupNote.Visibility         = Visibility.Visible;

		if( Model.SelectedTrips is {Count: > 0} )
		{
			Model.SelectedTrips[ 0 ].DeliveryNotes = TbDeliveryNote.Text.Trim();
			OldDeliveryNotes                       = string.Empty;
			SaveTrip( Model.SelectedTrips[ 0 ] );
		}
	}

	private void BtnCancelEditDeliveryNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingDeliveryNote          = false;
		TbDeliveryNote.IsReadOnly            = true;
		BtnCancelEditDeliveryNote.Visibility = BtnSaveDeliveryNote.Visibility = Visibility.Collapsed;
		BtnEditDeliveryNote.Visibility       = Visibility.Visible;
		BtnEditPickupNote.Visibility         = Visibility.Visible;

		if( Model.SelectedTrips is {Count: > 0} )
			TbDeliveryNote.Text = Model.SelectedTrips[ 0 ].DeliveryNotes = OldDeliveryNotes;
	}

	private void BtnEditPickupNote_Click( object? sender, RoutedEventArgs? e )
	{
		if( Model.SelectedTrips is {Count: > 0} && Model.SelectedTrips[ 0 ].TripId.IsNotNullOrWhiteSpace() )
		{
			Model.IsEditingPickupNote = true;
			TbPickupNote.IsReadOnly   = false;
			TbPickupNote.Focus();
			BtnCancelEditPickupNote.Visibility = BtnSavePickupNote.Visibility = Visibility.Visible;
			BtnEditPickupNote.Visibility       = Visibility.Collapsed;
			BtnEditDeliveryNote.Visibility     = Visibility.Hidden;

			//OldPickupNotes = Model.SelectedTrips[0].PickupNotes;
			OldPickupNotes = TbPickupNote.Text;
		}
	}

	private void TbPickupNote_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		BtnEditPickupNote_Click( null, null );
	}

	private void BtnSavePickupNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingPickupNote          = false;
		TbPickupNote.IsReadOnly            = true;
		BtnCancelEditPickupNote.Visibility = BtnSavePickupNote.Visibility = Visibility.Collapsed;
		BtnEditPickupNote.Visibility       = Visibility.Visible;
		BtnEditDeliveryNote.Visibility     = Visibility.Visible;

		if( Model.SelectedTrips is {Count: > 0} )
		{
			Model.SelectedTrips[ 0 ].PickupNotes = TbPickupNote.Text.Trim();
			OldPickupNotes                       = string.Empty;
			SaveTrip( Model.SelectedTrips[ 0 ] );
		}
	}

	private void BtnCancelEditPickupNote_Click( object sender, RoutedEventArgs e )
	{
		Model.IsEditingPickupNote          = false;
		TbPickupNote.IsReadOnly            = true;
		BtnCancelEditPickupNote.Visibility = BtnSavePickupNote.Visibility = Visibility.Collapsed;
		BtnEditPickupNote.Visibility       = Visibility.Visible;
		BtnEditDeliveryNote.Visibility     = Visibility.Visible;

		if( Model.SelectedTrips is {Count: > 0} )
			TbPickupNote.Text = Model.SelectedTrips[ 0 ].PickupNotes = OldPickupNotes;
	}

	private void SaveTrip( DisplayTrip dt )
	{
		//Model.SaveTrip(Model.SelectedTrips[0]);
		Model.SaveTrip( dt );
	}

	private void MiAssignDriver( object sender, RoutedEventArgs e )
	{
		var Drivers = ( from D in Model.Drivers
					    orderby D.StaffId.ToLower()
					    select D.StaffId ).ToList();

		var NewDriver = new AssignDriver.AssignDriver( Application.Current.MainWindow ).ShowAssignDriver( Drivers, true );

		if( NewDriver.IsNotNullOrWhiteSpace() )
		{
			Model.AssignNewDriverToSelectedTrips( NewDriver );

			Model.WhenFilterChanges();
		}
	}

	private void DataGrid_MouseEnter( object sender, MouseEventArgs e )
	{
		//         if (sender is DataGridRow row)
		//         {
		//             int rowNumber = DataGrid.ItemContainerGenerator.IndexFromContainer(row);
		//             if (rowNumber > -1)
		//             {
		//                 DisplayTrip dt = Model.DisplayTrips[rowNumber];
		//                 if (dt != null)
		//                 {
		//                     string toolTip = CreateToolTip(dt);
		//                     row.ToolTip = toolTip;
		//                 }
		//             }
		//         }
		//else if (sender is DataGrid dg)
		//         {
		//	//dg.mouseo

		//	if (dg.CurrentItem is DisplayTrip dt)
		//             {
		//		dg.ToolTip = CreateToolTip(dt);
		//	}
		//         }
	}

	private string CreateToolTip( DisplayTrip dt )
	{
		string Tip;

		Tip = "TripId: " + dt.TripId;

		Logging.WriteLogLine( "DEBUG ToolTip: " + Tip );
		return Tip;
	}

	private void DataGrid_MouseEnter_1( object sender, MouseEventArgs e )
	{
		if( sender is DataGridRow Row )
		{
			var RowNumber = DataGrid.ItemContainerGenerator.IndexFromContainer( Row );

			if( RowNumber > -1 )
			{
				DisplayTrip Dt = Model.DisplayTrips[ RowNumber ];

				if( Dt != null )
				{
					var ToolTip = CreateToolTip( Dt );
					Row.ToolTip = ToolTip;
				}
			}
		}
	}

	private void DataGrid_MouseDown( object sender, MouseButtonEventArgs e )
	{
		if( sender is DataGridRow Row )
		{
			var RowNumber = DataGrid.ItemContainerGenerator.IndexFromContainer( Row );

			if( RowNumber > -1 )
			{
				DisplayTrip Dt = Model.DisplayTrips[ RowNumber ];

				if( Dt != null )
				{
					var ToolTip = CreateToolTip( Dt );
					Row.ToolTip = ToolTip;
				}
			}
		}
	}

	private void DataGrid_MouseMove( object sender, MouseEventArgs e )
	{
	}

	private void TextBlock_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
	{
		if( sender is TextBlock Tb )
		{
			Logging.WriteLogLine( "Copying TripId " + Tb.Text + " to clipboard" );
			Clipboard.SetText( Tb.Text );
		}
	}

	private void MenuItem_Click( object sender, RoutedEventArgs e )
	{
		DataGrid_MouseDoubleClick( null, null );
	}


#region Settings
	private readonly List<Label>    FilterLabelList     = new();
	private readonly List<ComboBox> FilterConditionList = new();
	private readonly List<TextBox>  FilterValueList     = new();
	private readonly List<CheckBox> FilterIsActiveList  = new();
	private readonly List<CheckBox> CbColumns           = new();

	private void Settings_Click( object sender, RoutedEventArgs e )
	{
		//Logging.WriteLogLine("Opening settings tab");
		tabItemDrivers.Visibility  = Visibility.Visible;
		tabItemSettings.Visibility = Visibility.Visible;
		tabItemSettings.IsSelected = true;
		tabItemSettings.Focus();
	}

	private void BtnCloseSettings_Click( object sender, RoutedEventArgs e )
	{
		//Logging.WriteLogLine("Closing settings tab");
		tabItemDrivers.Visibility  = Visibility.Collapsed;
		tabItemSettings.Visibility = Visibility.Collapsed;
		tabItemDrivers.IsSelected  = true;
		tabItemDrivers.Focus();
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	private void PopulateSettingsColumnList()
	{
		CbColumns.Clear();
		gridColumns.RowDefinitions.Clear();
		gridColumns.Children.Clear();

		var Columns = Model.GetAllDriversColumnsInOrder();

		if( ( Columns != null ) && ( Columns.Count > 0 ) )
		{
			gridColumns.RowDefinitions.Add( new RowDefinition() );

			var LabelVisible = new Label
							   {
								   FontWeight = FontWeights.Bold,
								   Content    = FindStringResource( "DriversBoardLabelVisible" )
							   };

			gridColumns.Children.Add( LabelVisible );
			Grid.SetColumn( LabelVisible, 0 );
			Grid.SetRow( LabelVisible, 0 );

			var Row = 1;

			foreach( var Column in Columns )
			{
				if( ( Column != "DialoguesDriversColumnDriver" ) && ( Column != "DialoguesDriversColumnTripId" ) && ( Column != "DialoguesDriversColumnBoard" ) )
				{
					gridColumns.RowDefinitions.Add( new RowDefinition() );

					var Name = FindStringResource( Column );

					var CheckBox = new CheckBox
								   {
									   Name    = Column,
									   Content = Name
								   };
					gridColumns.Children.Add( CheckBox );
					Grid.SetColumn( CheckBox, 0 );
					Grid.SetRow( CheckBox, Row );

					if( Model.dictionaryColumnVisiblity.ContainsKey( Column ) && ( Model.dictionaryColumnVisiblity[ Column ] == Visibility.Visible ) )
						CheckBox.IsChecked = true;

					CbColumns.Add( CheckBox );
					++Row;
				}
				//else
				//{
				//	Label label = new Label { Content = "   " };
				//	this.gridColumns.Children.Add(label);
				//	Grid.SetColumn(label, 0);
				//	Grid.SetRow(label, row);
				//	++row;
				//}
			}
		}
	}

	private void IntegerTextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		var          ToCheck = e.Text.Substring( e.Text.Length - 1 );
		const string DECIMAL = ".";

		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) && !ToCheck.Equals( DECIMAL ) )
			e.Handled = true;
	}

	private void PopulateFilterList()
	{
		gridFilters.RowDefinitions.Clear();
		FilterLabelList.Clear();
		FilterConditionList.Clear();
		FilterValueList.Clear();
		FilterIsActiveList.Clear();

		var Columns = Model.GetAllDriversColumnsInOrder();

		if( ( Columns != null ) && ( Columns.Count > 0 ) )
		{
			gridFilters.RowDefinitions.Add( new RowDefinition() );

			var LabelColumnName = new Label
								  {
									  FontWeight = FontWeights.Bold,
									  Content    = FindStringResource( "DialoguesDriversLabelColumnName" )
								  };
			gridFilters.Children.Add( LabelColumnName );
			Grid.SetColumn( LabelColumnName, 0 );
			Grid.SetRow( LabelColumnName, 0 );

			var LabelCondition = new Label
								 {
									 FontWeight = FontWeights.Bold,
									 Content    = FindStringResource( "DialoguesDriversLabelCondition" )
								 };
			gridFilters.Children.Add( LabelCondition );
			Grid.SetColumn( LabelCondition, 1 );
			Grid.SetRow( LabelCondition, 0 );

			var LabelValue = new Label
							 {
								 FontWeight = FontWeights.Bold,
								 Content    = FindStringResource( "DialoguesDriversLabelValue" )
							 };
			gridFilters.Children.Add( LabelValue );
			Grid.SetColumn( LabelValue, 2 );
			Grid.SetRow( LabelValue, 0 );

			var LabelActive = new Label
							  {
								  FontWeight = FontWeights.Bold,
								  Content    = FindStringResource( "DialoguesDriversLabelActive" )
							  };
			gridFilters.Children.Add( LabelActive );
			Grid.SetColumn( LabelActive, 3 );
			Grid.SetRow( LabelActive, 0 );

			// Now build the grid
			var BgGray  = Brushes.LightGray;
			var BgWhite = Brushes.White;
			var Bg      = BgGray;
			var Row     = 1;

			foreach( var Column in Columns )
			{
				// Don't display these columns
				if( ( Column == "DialoguesDriversColumnTripId" ) || ( Column == "DialoguesDriversColumnDriver" ) || ( Column == "DialoguesDriversColumnReady" )
				    || ( Column == "DialoguesDriversColumnDue" ) || ( Column == "DialoguesDriversColumnCallDate" )
				    || ( Column == "DialoguesDriversColumnReadyTimeCondition" ) || ( Column == "DialoguesDriversColumnDueTimeCondition" )
				    || ( Column == "DialoguesDriversColumnBoard" ) )
					continue;
				gridFilters.RowDefinitions.Add( new RowDefinition() );
				var Name = FindStringResource( Column );

				var Label = new Label
							{
								Content    = Name,
								Background = Bg
							};
				FilterLabelList.Add( Label );
				gridFilters.Children.Add( Label );
				Grid.SetColumn( Label, 0 );
				Grid.SetRow( Label, Row );
				Label.Name = Column; // Used for lookup 

				var Cb = new ComboBox
						 {
							 ItemsSource   = Model.MapColumnToConditions( Column ),
							 SelectedIndex = 0,
							 Background    = Bg
						 };

				FilterConditionList.Add( Cb );
				gridFilters.Children.Add( Cb );
				Grid.SetColumn( Cb, 1 );
				Grid.SetRow( Cb, Row );

				var Tb = new TextBox
						 {
							 VerticalContentAlignment = VerticalAlignment.Center
						 };
				FilterValueList.Add( Tb );
				gridFilters.Children.Add( Tb );
				Grid.SetColumn( Tb, 2 );
				Grid.SetRow( Tb, Row );

				if( Model.IsColumnNumeric( Column ) )
					Tb.AddHandler( PreviewTextInputEvent, new TextCompositionEventHandler( IntegerTextBox_PreviewTextInput ) );

				var CheckBox = new CheckBox
							   {
								   Name                = Column,
								   HorizontalAlignment = HorizontalAlignment.Center,
								   VerticalAlignment   = VerticalAlignment.Center
							   };
				FilterIsActiveList.Add( CheckBox );
				gridFilters.Children.Add( CheckBox );
				Grid.SetColumn( CheckBox, 3 );
				Grid.SetRow( CheckBox, Row );

				var Df = Model.LookUpFilter( Label.Name );

				if( Df != null )
				{
					Cb.SelectedItem    = Df.condition;
					Tb.Text            = Df.value;
					CheckBox.IsChecked = Df.active;
				}

				++Row;

				if( ( Row % 2 ) == 0 )
					Bg = BgWhite;
				else
					Bg = BgGray;
			}
		}
	}
#endregion
}