﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using ViewModels.Trips.Boards.Common;
//using ViewModels.Trips.SearchTrips;

namespace ViewModels.Trips.PricingPosting;

public static class Extensions
{
    public static STATUS ToProtocolStatus(this Boards.Common.DisplayTrip.STATUS status)
    {
        return status switch
        {
            Boards.Common.DisplayTrip.STATUS.NEW => STATUS.NEW,
            Boards.Common.DisplayTrip.STATUS.ACTIVE => STATUS.ACTIVE,
            Boards.Common.DisplayTrip.STATUS.DISPATCHED => STATUS.DISPATCHED,
            Boards.Common.DisplayTrip.STATUS.PICKED_UP => STATUS.PICKED_UP,
            Boards.Common.DisplayTrip.STATUS.DELIVERED => STATUS.DELIVERED,
            Boards.Common.DisplayTrip.STATUS.VERIFIED => STATUS.VERIFIED,

            Boards.Common.DisplayTrip.STATUS.PAID => STATUS.FINALISED,
            Boards.Common.DisplayTrip.STATUS.DELETED => STATUS.DELETED,

            Boards.Common.DisplayTrip.STATUS.INVOICED => STATUS.INVOICED,
            Boards.Common.DisplayTrip.STATUS.POSTED => STATUS.POSTED,

            Boards.Common.DisplayTrip.STATUS.LIMBO => STATUS.LIMBO,

            _ => STATUS.UNKNOWN
        };
    }
}

public class PricingPostingModel : ViewModelBase
{
    private PricingPosting? View { get; set; }

    protected override async void OnInitialised()
    {
        base.OnInitialised();

        await Task.WhenAll(
                            //GetCompanyNames()
                          );
        WhenNumberOfLoadedTripsChanges();
    }

    public void SetView(PricingPosting? view)
    {
        View = view;
    }

    private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

    #region Data

    private readonly List<string> ALL_COLUMNS = new()
                                                {
        "AccountId", "Reference", "Driver", "CallTime", "PickupTime", "DeliveryTime", "VerifiedTime", "TripId",
        "PickupCompanyName", "CurrentZone", "PickupZone", "PickupAddressCity", "PickupAddressAddressLine1", "DeliveryCompanyName",
        "DeliveryZone", "DeliveryAddressCity", "DeliveryAddressAddressLine1", "POD", "ServiceLevel", "PackageType",
        "Pieces", "Weight", "Status1", "TotalPayrollAmount", "TotalAmount", "TotalTaxAmount", "Pallets"
                                                };
    public int NumberOfLoadedTrips
    {
        get => Get(() => NumberOfLoadedTrips, Trips.Count);
        set => Set(() => NumberOfLoadedTrips, value);
    }

    public string NumberOfLoadedTripsString
    {
        get => Get(() => NumberOfLoadedTripsString, "");
        set => Set(() => NumberOfLoadedTripsString, value);
    }

    [DependsUpon(nameof(NumberOfLoadedTrips))]
    public void WhenNumberOfLoadedTripsChanges()
    {
        NumberOfLoadedTripsString = FindStringResource("PricingPostingNumberOfTrips").Replace("@1", NumberOfLoadedTrips.ToString());
    }

    public string TripAdhocFilter
    {
        get => Get(() => TripAdhocFilter, "");
        set => Set(() => TripAdhocFilter, value);
    }

    public ObservableCollection<DisplayTrip> Trips
    {
        get { return Get(() => Trips, new ObservableCollection<DisplayTrip>()); }
        set { Set(() => Trips, value); }
    }

    private readonly List<DisplayTrip> UnFilteredTrips = new();

    public DisplayTrip? SelectedTrip
    {
        get { return Get(() => SelectedTrip, (DisplayTrip?)null); }
        set { Set(() => SelectedTrip, value); }
    }


    #endregion
    #region Actions

    public void Execute_Filter()
    {
        Dispatcher.Invoke(() =>
        {
            View?.SetWaitCursor();
        });
        if (TripAdhocFilter != string.Empty)
        {
            string filter = TripAdhocFilter.Trim().ToLower();
            Logging.WriteLogLine($"DEBUG filter: {filter}");

            Dictionary<string, DisplayTrip> filtered = new ();
            List<string> columns = GetActiveColumns();
            foreach (DisplayTrip dt in UnFilteredTrips)
            {
                foreach (var column in columns)
                {
                    string tmp = column switch
                    {
                        "AccountId"                   => dt.AccountId.ToLower(),
                        "Reference"                   => dt.Reference.ToLower(),
                        "Driver"                      => dt.Driver.ToLower(),
                        "CallTime"                    => dt.CallTime.ToString().ToLower(),
                        "PickupTime"                  => dt.PickupTime.ToString().ToLower(),
                        "DeliveryTime"                => dt.DeliveryTime.ToString().ToLower(),
                        "VerifiedTime"                => dt.VerifiedTime.ToString().ToLower(),
                        "TripId"                      => dt.TripId.ToLower(),
                        "PickupCompanyName"           => dt.PickupCompanyName.ToLower(),
                        "CurrentZone"                 => dt.CurrentZone.ToLower(),
                        "PickupZone"                  => dt.PickupZone.ToLower(),
                        "PickupAddressCity"           => dt.PickupAddressCity.ToLower(),
                        "PickupAddressAddressLine1"   => dt.PickupAddressAddressLine1.ToLower(),
                        "DeliveryCompanyName"         => dt.DeliveryCompanyName.ToLower(),
                        "DeliveryZone"                => dt.DeliveryZone.ToLower(),
                        "DeliveryAddressCity"         => dt.DeliveryAddressCity.ToLower(),
                        "DeliveryAddressAddressLine1" => dt.DeliveryAddressAddressLine1.ToLower(),
                        "POD"                         => dt.POD.ToLower(),
                        "ServiceLevel"                => dt.ServiceLevel.ToLower(),
                        "PackageType"                 => dt.PackageType.ToLower(),
                        "Pieces"                      => dt.Pieces.ToString( CultureInfo.InvariantCulture ).ToLower(),
                        "Weight"                      => dt.Weight.ToString( CultureInfo.InvariantCulture ).ToLower(),
                        "Status1"                     => dt.Status1.ToString().ToLower(),
                        //"DeliveryAmount", // TODO
                        //"TotalAmount", // TODO
                        //"TotalTaxAmount", // TODO
                        //"Pallets" // TODO
                        _ => string.Empty
                    };

                    if (tmp != string.Empty)
                    {
                        if (tmp.Contains(filter))
                        {
                            var tripId = dt.TripId.ToLower();

                            if (!filtered.ContainsKey(tripId))
                                filtered.Add(tripId, dt);

                            break;
                        }
                    }
                }
            }
            Dispatcher.Invoke(() =>
            {
                Trips.Clear();

                foreach (var trip in filtered.Values)
                    Trips.Add(trip);

                View?.ClearWaitCursor();
            });
        }
        else
        {
            // Was a filter active?
            if (Trips.Count != UnFilteredTrips.Count)
            {
                Logging.WriteLogLine("Clearing filter");

                Dispatcher.Invoke(() =>
                {
                    Trips.Clear();
                    foreach (var dt in UnFilteredTrips)
                        Trips.Add(dt);

                    View?.ClearWaitCursor();
                });

            }
        }

        Dispatcher.Invoke(() =>
        {
            View?.ClearWaitCursor();
        });
        Logging.WriteLogLine("DEBUG Done");
        
    }

    private List<string> GetActiveColumns()
    {
        //var columns = new List<string>();
        var columns = ALL_COLUMNS;

        return columns;
    }

    public void Execute_Post(List<DisplayTrip> selected)
    {
        if (selected?.Count > 0)
        {
            Logging.WriteLogLine("About to Post " + selected.Count + " shipments");

            foreach (DisplayTrip dt in selected)
            {
                Logging.WriteLogLine($"Setting Shipment {dt.TripId} status to Posted");
                dt.Status = DisplayTrip.STATUS.POSTED;
            }
            var tul = new TripUpdateList();
            tul.Trips.AddRange(FixStatusAndBroadcast(selected));

            Tasks.RunVoid(async () =>
            {
                await Azure.Client.RequestAddUpdateTrips(tul);
            });
            Dispatcher.Invoke(() =>
            {
                View?.SetWaitCursor();
                foreach (DisplayTrip dt in selected)
                {
                    Trips.Remove(dt);
                }

                NumberOfLoadedTrips = Trips.Count;

                View?.ClearWaitCursor();
            });
        }
    }

    private static IEnumerable<DisplayTrip> FixStatusAndBroadcast(List<DisplayTrip> changed)
    {
        foreach (var DisplayTrip in changed)
        {
            var Trip = (Trip)DisplayTrip;

            Trip.Status1 = DisplayTrip.Status.ToProtocolStatus();
            DisplayTrip.BroadcastToDispatchBoard = true;
            DisplayTrip.BroadcastToDriver = true;
            DisplayTrip.BroadcastToDriverBoard = true;
        }
        return changed;
    }

    private void DumpCharges(DisplayTrip dt)
    {
        if (dt.TripCharges?.Count > 0)
        {
            Logging.WriteLogLine($"DEBUG {dt.TripId}: TripCharges.Count: {dt.TripCharges.Count}");
        }
    }

    public void Execute_CalcUpdate()
    {
        // TODO There must be something else in IDS1 for this
        Execute_Load();
        Execute_Filter();
    }

    public void Execute_Load()
    {
        Logging.WriteLogLine("Loading Verified shipments");
        Dispatcher.Invoke(() =>
        {
            View?.SetWaitCursor();
            UnFilteredTrips.Clear();
        });
        Task.Run(async() =>
        {
            var result = new ObservableCollection<DisplayTrip>();

            var trips = await Azure.Client.RequestSearchTrips(new Protocol.Data.SearchTrips
            {
                UniqueUserId = Globals.DataContext.MainDataContext.UserName,
                StartStatus = STATUS.VERIFIED,
                EndStatus = STATUS.VERIFIED,
                FromDate = DateTime.Now.AddYears(-2),  // HACK 
            });

            Logging.WriteLogLine("Found " + trips.Count + " shipments");
            if (trips?.Count > 0 )
            {

                Dispatcher.Invoke(() =>
                {
                    foreach (var trip in trips)
                    {
                        var dt = new DisplayTrip(trip, new Boards.Common.ServiceLevel());

                        result.Add(dt);
                        UnFilteredTrips.Add(dt);
                        DumpCharges(dt);
                    }

                    Trips = result;
                    NumberOfLoadedTrips = Trips.Count;
                });
            }
            else
            {
                string title = FindStringResource("PricingPostingNoVerifiedTitle");
                string message = FindStringResource("PricingPostingNoVerifiedMessage");
                Dispatcher.Invoke(() =>
                {
                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
                });
            }

            Dispatcher.Invoke(() =>
            {
                View?.ClearWaitCursor();
            });
        });
    }

    public void Execute_Delete(List<DisplayTrip> selected)
    {
        if (selected?.Count > 0)
        {
            Logging.WriteLogLine($"Deleting {selected.Count} shipments");

            foreach (DisplayTrip dt in selected)
            {
                Logging.WriteLogLine($"Setting Shipment {dt.TripId} status to Deleted");
                dt.Status = DisplayTrip.STATUS.DELETED;
            }
            var tul = new TripUpdateList();
            tul.Trips.AddRange(FixStatusAndBroadcast(selected));

            Tasks.RunVoid(async () =>
            {
                await Azure.Client.RequestAddUpdateTrips(tul);
            });
            Dispatcher.Invoke(() =>
            {
                foreach (DisplayTrip dt in selected)
                {
                    Trips.Remove(dt);
                }

                NumberOfLoadedTrips = Trips.Count;
            });
        }
    }
    #endregion
}
