﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IdsControlLibraryV2.Utils;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.PricingPosting
{
    /// <summary>
    /// Interaction logic for PricingPosting.xaml
    /// </summary>
    public partial class PricingPosting : Page
    {
        private readonly PricingPostingModel? Model;

        public PricingPosting()
        {
            InitializeComponent();

            Model = (PricingPostingModel)DataContext;

            Model.SetView(this);
        }

        private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

        public void SetWaitCursor()
        {
            Mouse.OverrideCursor = Cursors.Wait;
        }

        public void ClearWaitCursor()
        {
            Mouse.OverrideCursor = null;
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Model != null && e.Key == Key.Escape && sender is TextBox tb)
            {
                tb.Text = string.Empty;
                Model.TripAdhocFilter = string.Empty;
                //Model.Execute_Filter("");
                Model.Execute_Filter();
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Model is not null && sender is TextBox Tb)
            {
                //Model.Execute_Filter(Tb.Text.Trim());
                Model.Execute_Filter();
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DataGrid_MouseDoubleClick(object? sender, MouseButtonEventArgs? e)
        {
            if (DataGrid.SelectedItem is DisplayTrip SelectedTrip)
            {
                Logging.WriteLogLine("Selected trip: " + SelectedTrip.TripId);

                // Need to create a new Trip Entry tab
                Logging.WriteLogLine("Opening new " + Globals.TRIP_ENTRY + " tab");
                Globals.RunNewProgram(Globals.TRIP_ENTRY, SelectedTrip);
            }
        }

        private void MiOpenInShipmentEntry_Click(object sender, RoutedEventArgs e)
        {
            DataGrid_MouseDoubleClick(null, null);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            // Binding sometimes doesn't work
            if (sender is ToggleButton Tb)
            {
                var DGrid = Tb.FindParent<DataGrid>();

                if (DGrid is not null)
                {
                    var Ndx = DGrid.SelectedIndex;

                    if (Ndx >= 0)
                    {
                        var Itm = (DisplayTrip)DGrid.SelectedItem;
                        Itm.DetailsVisible = true;

                        var Row = DGrid.GetRow(Ndx);

                        if (Row is not null)
                            DGrid.ScrollIntoView(Row);
                    }
                }
            }
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            // Binding sometimes doesn't work
            if (sender is ToggleButton Tb)
            {
                var Grid = Tb.FindParent<DataGrid>();

                if (Grid?.SelectedItem is DisplayTrip Itm)
                    Itm.DetailsVisible = false;
            }
        }

        private void BtnPost_Click(object sender, RoutedEventArgs e)
        {
            string title = FindStringResource("PricingPostingPostTitle");
            if( DataGrid.SelectedItems.Count > 0 )
            {
                string message = FindStringResource("PricingPostingPostMessage");
                message = message.Replace("@1", DataGrid.SelectedItems.Count.ToString()) + "\n" + FindStringResource("PricingPostingPostMessage1");
                MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if( mbr == MessageBoxResult.Yes )
                {
                    List<DisplayTrip> selected = new();
                    foreach (DisplayTrip dt in DataGrid.SelectedItems)
                    {
                        selected.Add(dt);
                    }
                    Model?.Execute_Post(selected);
                }
            }
            else
            {
                string message = FindStringResource("PricingPostingPostNonSelectedMessage");
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            string title = FindStringResource("PricingPostingDeleteTitle");
            if (DataGrid.SelectedItems.Count > 0)
            {
                string message = FindStringResource("PricingPostingDeleteMessage");
                message = message.Replace("@1", DataGrid.SelectedItems.Count.ToString()) + "\n" + FindStringResource("PricingPostingDeleteMessage1");
                MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (mbr == MessageBoxResult.Yes)
                {
                    List<DisplayTrip> selected = new();
                    foreach (DisplayTrip dt in DataGrid.SelectedItems)
                    {
                        selected.Add(dt);
                    }
                    Model?.Execute_Delete(selected);
                }
            }
            else
            {
                string message = FindStringResource("PricingPostingPostNonSelectedMessage");
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
