﻿#nullable enable

using System.Windows.Controls;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Trips.TripEntryV2;

/// <summary>
///     Interaction logic for TripEntryV2.xaml
/// </summary>
public partial class TripEntryV2 : Page
{
	private static readonly string? TripEntrySaveTripFirst,
									Warning;

	static TripEntryV2()
	{
		TripEntrySaveTripFirst ??= Globals.Dictionary.AsString( "TripEntrySaveTripFirst" );
		Warning                ??= Globals.Dictionary.AsString( "Warning" );
	}

	public TripEntryV2()
	{
		InitializeComponent();
		var M = (TripEntryV2ViewModel)Root.DataContext;

		M.OnHelpAction = () =>
						 {
							 Help.Show();
						 };

		M.OnConfirmCancel = () => MessageBox.Show( TripEntrySaveTripFirst, Warning, MessageBoxButton.OKCancel, MessageBoxImage.Warning ) == MessageBoxResult.OK;
	}
}