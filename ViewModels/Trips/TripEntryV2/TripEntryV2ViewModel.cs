﻿#nullable enable

using System.Windows.Input;
using ViewModels.Trips.Boards;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.TripEntryV2;

internal class TripEntryV2ViewModel : ViewModelBase
{
#region Help
	private const string HELP_URI = "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638222337/How+to+Use+the+Shipment+Entry+Tab";

	public string HelpUri
	{
		get { return Get( () => HelpUri, HELP_URI ); }
	}

	public Action? OnHelpAction;

	public ICommand OnHelp => Commands[ nameof( OnHelp ) ];

	public void Execute_OnHelp()
	{
		OnHelpAction?.Invoke();
	}
#endregion

#region Editing
	public bool Editing
	{
		get { return Get( () => Editing, false ); }
		set { Set( () => Editing, value ); }
	}

	public bool NotEditing
	{
		get { return Get( () => NotEditing, true ); }
		set { Set( () => NotEditing, value ); }
	}

	[DependsUpon( nameof( Editing ) )]
	public void WhenEditingChanges()
	{
		NotEditing = !Editing;
	}

	public bool TextBoxesReadonly
	{
		get { return Get( () => TextBoxesReadonly, false ); }
		set { Set( () => TextBoxesReadonly, value ); }
	}
#endregion

#region Trip
	public DisplayTrip EditTrip
	{
		get { return Get( () => EditTrip, DisplayTrip.Empty ); }
		set { Set( () => EditTrip, value ); }
	}

	public bool IsTripIdNotEditable
	{
		get { return Get( () => IsTripIdNotEditable, true ); }
		set { Set( () => IsTripIdNotEditable, value ); }
	}
#endregion

#region Actions
	public Func<bool>? OnConfirmCancel;

#region New Trip
	public ICommand OnNewTrip => Commands[ nameof( OnNewTrip ) ];

	public async void Execute_OnNewTrip()
	{
		var TripId = await Azure.Client.RequestGetNextTripId();

		EditTrip = new DetailedTrip
				   {
					   TripId = TripId
				   };

		Editing = true;
	}
#endregion

#region Clear
	public ICommand OnClear => Commands[ nameof( OnClear ) ];

	public void Execute_OnClear()
	{
		if( OnConfirmCancel is null || ( EditTrip.TripId.IsNotNullOrWhiteSpace() && OnConfirmCancel() ) )
		{
			EditTrip = new DetailedTrip();
			Editing  = false;
		}
	}
#endregion

#region Save Trip
	public bool OkToSave
	{
		get { return Get( () => OkToSave, false ); }
		set { Set( () => OkToSave, value ); }
	}

	public ICommand OnSaveTrip => Commands[ nameof( OnSaveTrip ) ];

	public void Execute_OnSaveTrip()
	{
		Editing = false;
	}
#endregion
#endregion
}