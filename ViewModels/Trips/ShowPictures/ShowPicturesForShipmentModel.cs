﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels.Trips.ShowPictures
{
	internal class ShowPicturesForShipmentModel: ViewModelBase
	{
		public ShowPicturesForShipment View = null;

		public string TripId
		{
			get { return Get(() => TripId, ""); }
			set { Set(() => TripId, value); }
		}
	}
}
