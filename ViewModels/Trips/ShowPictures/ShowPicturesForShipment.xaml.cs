﻿namespace ViewModels.Trips.ShowPictures
{
	/// <summary>
	/// Interaction logic for ShowPicturesForShipment.xaml
	/// </summary>
	public partial class ShowPicturesForShipment : Window
	{
		private readonly ShowPicturesForShipmentModel Model; 

		public ShowPicturesForShipment(Window owner)
		{
			Owner = owner;
			InitializeComponent();

			if (DataContext is ShowPicturesForShipmentModel model)
			{
				Model = model;
				Model.View = this;
			}
		}

		public void ShowPictures(string tripId)
		{
			Model.TripId = tripId;

			if (tripId.IsNotNullOrWhiteSpace())
			{
				ImagesControl.ImageName = tripId;
				ImagesControl.ShowDetails = true;
			}

			Show();
		}

		private void BtnClose_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
