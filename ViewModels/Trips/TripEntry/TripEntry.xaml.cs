﻿#nullable enable

using Protocol.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using ViewModels.Customers;
using ViewModels.Trips.Boards.Common;
using ViewModels.Trips.ShowPictures;
using Xceed.Wpf.Toolkit;
using static ViewModels.Globals;
using static ViewModels.Trips.TripEntry.TripEntryModel;
using Company = Protocol.Data.Company;
using Image = System.Windows.Controls.Image;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using Uri = System.Uri;

namespace ViewModels.Trips.TripEntry;

/// <summary>
///     Interaction logic for TripEntry.xaml
/// </summary>
public partial class TripEntry : Page, IDisposable
{
	public TripEntry()
	{
		InitializeComponent();

		var M = Model = (TripEntryModel)DataContext;
		M.View = this;

		M.OnInvalidTripId = _ =>
							{
								Globals.Dialogues.Error.Show("TripEntryInvalidTripId");
							};

		//this.BuildNewPackageTypeRow(Model);

		//this.Clear();

		// TODO Turned off as it was resetting the pickup region
		//if (!Model.IsUpdateAddressesRunning())
		//{
		//    Model.StartUpdateAddresses();
		//}

		//Model.StartUpdateCompanyData();

		//if( !M.IsUpdateGlobalAddressesTimerRunning() )
		//	M.StartUpdateGlobalAddressesTimer();

		ClearChargesAndTotals();

		JumpBillingCommandBinding_Executed(null, null);
		cbAccount.Focus();
	}

	// Used to name charges Tb___GST or Lbl___GST___Result
	public readonly string ChargeControlNameDivider = "___";
	private readonly Dictionary<string, Control> DictControls = new();

	private bool Disposed;

	private readonly TripEntryModel Model;

	//private bool EnableSurcharges = false;

	private void BtnClear_Click(object? sender, RoutedEventArgs? e)
	{
		Clear();
	}

	private void ClearErrorConditions()
	{
		foreach (var Ctrl in this.GetChildren())
		{
			switch (Ctrl)
			{
				case TextBox Tb when Tb.Background == Brushes.Yellow:
					Tb.Background = Brushes.White;
					break;

				case ComboBox Cb when Cb.Background == Brushes.Yellow:
					Cb.Background = Brushes.White;
					break;

				case DateTimePicker Dtp when Dtp.Background == Brushes.Yellow:
					Dtp.Background = Brushes.White;
					break;

				case TimePicker Tp when Tp.Background == Brushes.Yellow:
					Tp.Background = Brushes.White;
					break;
			}
		}
	}

	private async void BtnNew_Click(object? sender, RoutedEventArgs? e)
	{
		if (DataContext is TripEntryModel Model && !ViewModelBase.IsInDesignMode)
		{
			Clear();
			var TripId = await Model.GetNewTripId();
			Model.IsNewTrip = true;
			Model.TripId = TripId;
		}
	}

	/*
			private async void BtnNew_Click_Latest( object sender, RoutedEventArgs e )
			{
				if( DataContext is TripEntryModel Model )
				{
					const bool ONLY_TRIP_ID = false;
					var        Cancelled    = false;
					var        ErrorsFound  = false;

					if( Model.IsNewTrip )
					{
						await Dispatcher.Invoke( async () =>
												 {
													 var Message = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst" )
																   + Environment.NewLine
																   + (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst2" );
													 var Caption = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirstTitle" );
													 var Choice  = MessageBox.Show( Message, Caption, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

													 switch( Choice )
													 {
													 case MessageBoxResult.Yes:
														 {
															 ErrorsFound = await Save();

															 if( !ErrorsFound )
																 BtnClear_Click( sender, e );
															 break;
														 }

													 case MessageBoxResult.Cancel:
														 Cancelled = true;
														 break;
													 }
												 } );
					}

					if( !Cancelled && !ErrorsFound )
					{
						// KLUDGE

						var Now = DateTime.Now;

						//DumpDateTime("BtnNew_Click");

						//if (dtpCallDate.Text.Contains("0001"))
						//{
						//    Logging.WriteLogLine("ERROR - CallDate: " + dtpCallDate.Text);
						//}
						//Logging.WriteLogLine("ERROR - now: " + now.ToString());
						Dispatcher.Invoke( () =>
										   {
											   Model.CallDate = Now;

											   //Model.CallTime = now;

											   dtpCallDate.Value      = Now;
											   tpCallTime.Value       = Now;
											   dtpReadyTimeDate.Value = Now;
											   dtpReadyTimeTime.Value = Now;
											   dtpDueTimeDate.Value   = Now;
											   dtpDueTimeTime.Value   = Now;
										   } );

						BtnClear_Click( sender, e );

						await Model.Execute_NewTrip();

						var List = new ObservableCollection<DisplayPackage>
								   {
									   new( new TripPackage
											{
												Pieces = 1,
												Weight = 1
											} )
								   };
						Model.TripPackages = List;
						BuildPackageRows( Model );

						// TODO TripItemsInventory still contains the Inventory for the previous trip
						// TODO This is because Model.TripPackages isn't empty
						//Model.TripItemsInventory = new SortedDictionary<int, List<ExtendedInventory>>();
						//BuildPackageRows(Model);

						//DumpDateTime("BtnNew_Click-after Execute_NewTrip");

						//Dispatcher.Invoke(() =>
						//{
						//    dtpCallDate.Value = now;
						//    dtpCallDate.Text = now.ToString("dd/MM/yyyy");
						//    tpCallTime.Value = now;
						//    tpCallTime.Text = now.ToString("HH:mm z");                    
						//    dtpReadyTimeDate.Value = now;
						//    dtpReadyTimeDate.Text = now.ToString("dd/MM/yyyy");
						//    dtpReadyTimeTime.Value = now;
						//    dtpReadyTimeTime.Text = now.ToString("HH:mm z");
						//    dtpDueTimeDate.Value = now;
						//    dtpDueTimeDate.Text = now.ToString("dd/MM/yyyy");
						//    dtpDueTimeTime.Value = now;
						//    dtpDueTimeTime.Text = now.ToString("HH:mm z");
						//});
						//EnsureDateTimesArentBeginningOfTime();
					}
				}
			}
	*/

	/*
	private async void BtnNew_Click_orig( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
		{
			var onlyTripId  = false;
			var cancelled   = false;
			var errorsFound = false;

			// This is the case if viewing a trip
			if( !Model.IsTripIdNotEditable )
			{
				// Was
				//BtnClear_Click( sender, e );
				if( Model.SelectedAccount.IsNotNullOrEmpty() && Model.TripId.IsNullOrEmpty() )
				{
					Dispatcher.Invoke( () =>
					                   {
						                   var message = (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripId" )
						                                 + Environment.NewLine
						                                 + (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripId2" );
						                   var title  = (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripIdTitle" );
						                   var choice = MessageBox.Show( message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

						                   if( choice == MessageBoxResult.No )
						                   {
							                   //Model.Execute_ClearTripEntry();
							                   BtnClear_Click( sender, e );
						                   }
						                   else if( choice == MessageBoxResult.Cancel )
							                   cancelled = true;
						                   else
						                   {
							                   Logging.WriteLogLine( "NOT clearing the screen" );
							                   onlyTripId = true;
						                   }
					                   } );
				}
				else
					BtnClear_Click( sender, e );
			}
			else
			{
				if( Model.IsNewTrip )
				{
					// 
					await Dispatcher.Invoke( async () =>
					                         {
						                         var message = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst" )
						                                       + Environment.NewLine
						                                       + (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst2" );
						                         var title  = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirstTitle" );
						                         var choice = MessageBox.Show( message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

						                         switch( choice )
						                         {
						                         case MessageBoxResult.Yes:
							                         {
								                         //Model.StartNewTrip = true;

								                         //errorsFound = BtnSave_Click( sender, e );
								                         errorsFound = await Save();

								                         if( !errorsFound )
									                         BtnClear_Click( sender, e );
								                         break;
							                         }

						                         case MessageBoxResult.Cancel:
							                         cancelled = true;
							                         break;
						                         }
					                         } );
				}

				//if( !cancelled && Model.Account.IsNotNullOrEmpty() && Model.TripId.IsNullOrEmpty() )
				if( !cancelled && Model.SelectedAccount.IsNotNullOrEmpty() && Model.TripId.IsNullOrEmpty() )
				{
					Dispatcher.Invoke( () =>
					                   {
						                   var message = (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripId" )
						                                 + Environment.NewLine
						                                 + (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripId2" );
						                   var title  = (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripIdTitle" );
						                   var choice = MessageBox.Show( message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

						                   if( choice == MessageBoxResult.No )
						                   {
							                   //Model.Execute_ClearTripEntry();
							                   BtnClear_Click( sender, e );
						                   }
						                   else if( choice == MessageBoxResult.Cancel )
							                   cancelled = true;
						                   else
						                   {
							                   Logging.WriteLogLine( "NOT clearing the screen" );
							                   onlyTripId = true;
						                   }
					                   } );
				}

				//else
				//{
				//    //Model.Execute_ClearTripEntry();
				//    //BtnClear_Click(sender, e);
				//    // TODO does this work?
				//    Model.TripPackages = new ObservableCollection<DisplayPackage>();
				//    BuildPackageRows(Model);
				//}
			}

			if( !cancelled && !errorsFound )
			{
				// KLUDGE

				var now = DateTime.Now;

				//DumpDateTime("BtnNew_Click");

				//if (dtpCallDate.Text.Contains("0001"))
				//{
				//    Logging.WriteLogLine("ERROR - CallDate: " + dtpCallDate.Text);
				//}
				//Logging.WriteLogLine("ERROR - now: " + now.ToString());
				Dispatcher.Invoke( () =>
				                   {
					                   Model.CallDate = now;

					                   //Model.CallTime = now;

					                   dtpCallDate.Value      = now;
					                   tpCallTime.Value       = now;
					                   dtpReadyTimeDate.Value = now;
					                   dtpReadyTimeTime.Value = now;
					                   dtpDueTimeDate.Value   = now;
					                   dtpDueTimeTime.Value   = now;
				                   } );

				await Model.Execute_NewTrip( onlyTripId );

				// TODO TripItemsInventory still contains the Inventory for the previous trip
				// TODO This is because Model.TripPackages isn't empty
				//Model.TripItemsInventory = new SortedDictionary<int, List<ExtendedInventory>>();
				//BuildPackageRows(Model);

				//DumpDateTime("BtnNew_Click-after Execute_NewTrip");

				//Dispatcher.Invoke(() =>
				//{
				//    dtpCallDate.Value = now;
				//    dtpCallDate.Text = now.ToString("dd/MM/yyyy");
				//    tpCallTime.Value = now;
				//    tpCallTime.Text = now.ToString("HH:mm z");                    
				//    dtpReadyTimeDate.Value = now;
				//    dtpReadyTimeDate.Text = now.ToString("dd/MM/yyyy");
				//    dtpReadyTimeTime.Value = now;
				//    dtpReadyTimeTime.Text = now.ToString("HH:mm z");
				//    dtpDueTimeDate.Value = now;
				//    dtpDueTimeDate.Text = now.ToString("dd/MM/yyyy");
				//    dtpDueTimeTime.Value = now;
				//    dtpDueTimeTime.Text = now.ToString("HH:mm z");
				//});
				//EnsureDateTimesArentBeginningOfTime();
			}
		}
	}
	*/

	/*
	private void DumpDateTime( string method )
	{
		if( DataContext is TripEntryModel Model )
		{
			Logging.WriteLogLine( "ERROR - " + method + " Model.CallDate: " + Model.CallDate );
			Logging.WriteLogLine( "ERROR - " + method + " Model.CallTime: " + Model.CallDate );
			Logging.WriteLogLine( "ERROR - " + method + " Model.ReadyDate: " + Model.ReadyDate );
			Logging.WriteLogLine( "ERROR - " + method + " Model.ReadyTime: " + Model.ReadyDate );
			Logging.WriteLogLine( "ERROR - " + method + " Model.DueDate: " + Model.DueDate );
			Logging.WriteLogLine( "ERROR - " + method + " Model.DueTime: " + Model.DueDate );
		}
		Logging.WriteLogLine( "ERROR - " + method + " CallDate: " + dtpCallDate.Value );
		Logging.WriteLogLine( "ERROR - " + method + " CallDate.Text: " + dtpCallDate.Text );
		Logging.WriteLogLine( "ERROR - " + method + " CallTime: " + tpCallTime.Value );
		Logging.WriteLogLine( "ERROR - " + method + " CallTime.Text: " + tpCallTime.Text );
		Logging.WriteLogLine( "ERROR - " + method + " ReadyDate: " + dtpReadyTimeDate.Value );
		Logging.WriteLogLine( "ERROR - " + method + " ReadyDate.Text: " + dtpReadyTimeDate.Text );
		Logging.WriteLogLine( "ERROR - " + method + " ReadyTime: " + dtpReadyTimeTime.Value );
		Logging.WriteLogLine( "ERROR - " + method + " ReadyTime.Text: " + dtpReadyTimeTime.Text );
		Logging.WriteLogLine( "ERROR - " + method + " DueTimeDate: " + dtpDueTimeDate.Value );
		Logging.WriteLogLine( "ERROR - " + method + " DueTimeDate.Text: " + dtpDueTimeDate.Text );
		Logging.WriteLogLine( "ERROR - " + method + " DueTimeTime: " + dtpDueTimeTime.Value );
		Logging.WriteLogLine( "ERROR - " + method + " DueTimeTime.Text: " + dtpDueTimeTime.Text );
	}
	*/

	private async void BtnSave_Click(object? sender, RoutedEventArgs? e)
	{
		BtnCalculate_Click(null, null);
		await Save();
	}

	private async Task Save()
	{
		if (DataContext is TripEntryModel Model && !ViewModelBase.IsInDesignMode)
		{
			if (DictControls.Count == 0)
				LoadMappingDictionary();

			// Load each time to catch the extra TripItem rows
			//LoadMappingDictionary(); // TODO This doesn't include the extra Package type rows

			var Errors = await Model.Execute_SaveTrip(DictControls);

			if (Errors.Count > 0)
			{
				var Message = (string)Application.Current.TryFindResource("TripEntryValidationErrors")
							  + Environment.NewLine;

				foreach (var Line in Errors)
					Message += "\t" + Line + Environment.NewLine;

				Message += (string)Application.Current.TryFindResource("TripEntryValidationErrors1");
				var Caption = (string)Application.Current.TryFindResource("TripEntryValidationErrorsTitle");

				MessageBox.Show(Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
	}

	/// <summary>
	///     Add widgets and their associated fields.
	///     Uses the GetChildren extension method defined in Widgets.cs.
	/// </summary>
	/// <returns></returns>
	private void LoadMappingDictionary()
	{
		var Children = this.GetChildren();

		foreach (var Ctrl in Children)
		{
			var Binding = Ctrl switch
			{
				TextBox Tb => BindingOperations.GetBinding(Tb, TextBox.TextProperty),
				ComboBox Cb => BindingOperations.GetBinding(Cb, ItemsControl.ItemsSourceProperty),
				DateTimePicker Dtp => BindingOperations.GetBinding(Dtp, DateTimePicker.ValueProperty),
				TimePicker Tp => BindingOperations.GetBinding(Tp, DateTimePicker.ValueProperty),
				_ => null
			};

			if (Binding is not null)
			{
				var Path = Binding.Path.Path;

				if (!DictControls.ContainsKey(Path))
					DictControls.Add(Path, (Control)Ctrl);
				else
					Logging.WriteLogLine("ERROR " + Path + " already exists");
			}
		}
	}

	private void Label_MouseDown(object sender, MouseButtonEventArgs e)
	{
		//Logging.WriteLogLine( "Clearing " + cbDrivers.Text );
		cbDrivers.SelectedIndex = -1;
	}

	/// <summary>
	///     Toggles the visibility of the toolbar and menu.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void MenuItem_Click(object sender, RoutedEventArgs e)
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility = Visibility.Visible;
	}

	/// <summary>
	///     Toggles the visibility of the toolbar and menu.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void MenuItem_Click_1(object sender, RoutedEventArgs e)
	{
		menuMain.Visibility = Visibility.Visible;
		menuMain.IsEnabled = true;
		toolbar.Visibility = Visibility.Collapsed;
	}

	private void Toolbar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
	{
		menuMain.Visibility = Visibility.Visible;
		menuMain.IsEnabled = true;
		toolbar.Visibility = Visibility.Collapsed;
	}

	private void BtnProduceWaybill_Click(object? sender, RoutedEventArgs? e)
	{
		if (DataContext is TripEntryModel Model)
		{
			var PrinterName = WeighBillPrinter.CurrentPrinterName;

			if (PrinterName == "No printer selected")
			{
				Logging.WriteLogLine("No printer selected");

				MessageBox.Show((string)Application.Current.TryFindResource("TripEntryToolBarSelectPrinterFirst"),
								 (string)Application.Current.TryFindResource("TripEntryToolBarSelectPrinterFirstTitle"),
								 MessageBoxButton.OK, MessageBoxImage.Warning);
			}
			else
			{
				Logging.WriteLogLine("Printer " + PrinterName + " selected");

				var ParentWindow = Application.Current.MainWindow;

				Model.Execute_ProduceWaybill(PrinterName, ParentWindow!);
			}
		}
	}

	private void BtnProduceTrack_Click(object sender, RoutedEventArgs e)
	{
		if (DataContext is TripEntryModel Model)
		{
			var ParentWindow = Application.Current.MainWindow;
			Model.Execute_ProduceTracking(ParentWindow!);
		}
	}

	private async void BtnProduceAudit_Click(object? sender, RoutedEventArgs? e)
	{
		if (DataContext is TripEntryModel Model)
		{
			var ParentWindow = Application.Current.MainWindow;
			await Model.Execute_ProduceAudit(ParentWindow!);
		}
	}

	private void BtnProduceDocs_Click(object sender, RoutedEventArgs e)
	{
		DisplayNotImplementedMessage();

		//Window parentWindow = Application.Current.MainWindow;
		// Model.Execute_ProduceAudit(parentWindow);
	}

	private static void DisplayNotImplementedMessage()
	{
		MessageBox.Show("Not implemented yet", "Not Implemented", MessageBoxButton.OK, MessageBoxImage.Information);
	}

	private void BtnAddressSwap_Click(object sender, RoutedEventArgs e)
	{
		Model.Execute_SwapAddresses();
	}

	private void BtnAddressBook_Click(object sender, RoutedEventArgs e)
	{
		RunProgram(ADDRESS_BOOK);
	}

	private void BtnFindCustomer_Click(object? sender, RoutedEventArgs? e)
	{
		var SelectedCompanyId = new FindCustomer(Application.Current.MainWindow).DisplayDialog();

		if (SelectedCompanyId.IsNotNullOrWhiteSpace())
		{
			Model.SelectedAccount = SelectedCompanyId;
			Model.WhenAccountChanges();
		}
	}

	private void BtnFindTrip_Click(object? sender, RoutedEventArgs? e)
	{
		if (spFindTrip.Visibility == Visibility.Collapsed)
		{
			// Display a small dialog to get the tripId
			spFindTrip.Visibility = Visibility.Visible;
			tbFindTripTripId.Focus();
		}
		else
			spFindTrip.Visibility = Visibility.Collapsed;
	}

	//private async void btnFindTripGo_Click( object sender, RoutedEventArgs e )
	private void BtnFindTripGo_Click(object sender, RoutedEventArgs e)
	{
		if (!string.IsNullOrEmpty(tbFindTripTripId.Text.Trim()))
		{
			//await Model.Execute_FindTrip( tbFindTripTripId.Text.Trim() );
			Model.Execute_FindTrip(tbFindTripTripId.Text.Trim());
		}
		spFindTrip.Visibility = Visibility.Collapsed;
	}


	private void TbFindTripTripId_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.Escape)
			spFindTrip.Visibility = Visibility.Collapsed;
	}

	private void BtnFindTrip_LostFocus(object sender, RoutedEventArgs e)
	{
		//     if (spFindTrip.Visibility == Visibility.Visible)
		//     {
		//spFindTrip.Visibility = Visibility.Collapsed;
		//     }
	}

	private async Task<bool> AskToSave()
	{
		var Errs = false;

		await Dispatcher.Invoke(async () =>
								 {
									 var Message = (string)Application.Current.TryFindResource("TripEntrySaveTripFirst")
												   + Environment.NewLine
												   + (string)Application.Current.TryFindResource("TripEntrySaveTripFirst2");
									 var Caption = (string)Application.Current.TryFindResource("TripEntrySaveTripFirstTitle");
									 var Choice = MessageBox.Show(Message, Caption, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

									 switch (Choice)
									 {
										 case MessageBoxResult.Yes:
											 {
												 if (DictControls.Count == 0)
													 LoadMappingDictionary();

												 BtnCalculate_Click(null, null);

												 var Errors = await Model.Execute_SaveTrip(DictControls);

												 if (Errors is { Count: > 0 })
												 {
													 Errs = true;
													 Message = (string)Application.Current.TryFindResource("TripEntryValidationErrors") + Environment.NewLine;

													 foreach (var Line in Errors)
														 Message += "\t" + Line + Environment.NewLine;

													 Message += (string)Application.Current.TryFindResource("TripEntryValidationErrors1");
													 Caption = (string)Application.Current.TryFindResource("TripEntryValidationErrorsTitle");

													 MessageBox.Show(Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error);
													 Errors?.Clear();
												 }
												 else
												 {
													 ClearErrorConditions();
													 Errs = false;
												 }
												 break;
											 }

										 case MessageBoxResult.Cancel:
											 Errs = true;
											 break;
									 }
								 });
		return Errs;
	}

	private async void BtnClone_Click(object? sender, RoutedEventArgs? e)
	{
		if (DataContext is TripEntryModel Model)
		{
			var Errors = await AskToSave();

			if (!Errors)
			{
				await Model.Execute_CloneTrip();

				// KLUDGE
				var Now = DateTime.Now;
				dtpCallDate.Value = Now;
				tpCallTime.Value = Now;
				dtpReadyTimeDate.Value = Now;
				dtpReadyTimeTime.Value = Now;
				dtpDueTimeDate.Value = Now;
				dtpDueTimeTime.Value = Now;

				//EnsureDateTimesArentBeginningOfTime();
			}
		}
	}

	private async void BtnClonePickup_Click(object? sender, RoutedEventArgs? e)
	{
		var Errors = await AskToSave();

		if (!Errors)
		{
			cbDeliveryAddresses.SelectedIndex = -1;
			await Model.Execute_CloneTripForPickup();
			imagePOP.Source = null;
			imagePOD.Source = null;
			BuildPackageRows(Model);
			ClearChargesAndTotals();

			// KLUDGE
			var Now = DateTime.Now;
			dtpCallDate.Value = Now;
			tpCallTime.Value = Now;
			dtpReadyTimeDate.Value = Now;
			dtpReadyTimeTime.Value = Now;
			dtpDueTimeDate.Value = Now;
			dtpDueTimeTime.Value = Now;

			cbDeliveryAddresses.SelectedIndex = -1;
			tbDeliveryAddressName.Text = string.Empty;
			tbDeliveryAddressPhone.Text = string.Empty;
			tbDeliveryAddressSuite.Text = string.Empty;
			tbDeliveryAddressStreet.Text = string.Empty;
			tbDeliveryAddressCity.Text = string.Empty;
			tbDeliveryAddressPostalZip.Text = string.Empty;
			cbDeliveryAddressZone.SelectedIndex = -1;
			tbDeliveryNotes.Text = string.Empty;

			//EnsureDateTimesArentBeginningOfTime();
		}
	}

	private async void BtnCloneReturn_Click(object? sender, RoutedEventArgs? e)
	{
		var Errors = await AskToSave();

		if (!Errors)
		{
			var PickupIndex = cbPickupAddresses.SelectedIndex;
			var DeliveryIndex = cbDeliveryAddresses.SelectedIndex;
			Model.Execute_CloneTripForReturn();
			cbPickupAddresses.SelectedIndex = DeliveryIndex;
			cbDeliveryAddresses.SelectedIndex = PickupIndex;

			// KLUDGE
			var Now = DateTime.Now;
			dtpCallDate.Value = Now;
			tpCallTime.Value = Now;
			dtpReadyTimeDate.Value = Now;
			dtpReadyTimeTime.Value = Now;
			dtpDueTimeDate.Value = Now;
			dtpDueTimeTime.Value = Now;

			//EnsureDateTimesArentBeginningOfTime();                
		}
	}

	//private void EnsureDateTimesArentBeginningOfTime()
	//{
	//    if (dtpCallDate.Value == DateTime.MinValue)
	//    {
	//        dtpCallDate.Value = DateTime.Now;
	//    }
	//    if (tpCallTime.Value == DateTime.MinValue)
	//    {
	//        tpCallTime.Value = DateTime.Now;
	//    }
	//    if (dtpReadyTimeDate.Value == DateTime.MinValue)
	//    {
	//        dtpReadyTimeDate.Value = DateTime.Now;
	//    }
	//    if (dtpReadyTimeTime.Value == DateTime.MinValue)
	//    {
	//        dtpReadyTimeTime.Value = DateTime.Now;
	//    }
	//    if (dtpDueTimeDate.Value == DateTime.MinValue)
	//    {
	//        dtpDueTimeDate.Value = DateTime.Now;
	//    }
	//    if (dtpDueTimeTime.Value == DateTime.MinValue)
	//    {
	//        dtpDueTimeTime.Value = DateTime.MinValue;
	//    }
	//}

	private void BtnSchedule_Click(object? sender, RoutedEventArgs? e)
	{
		DisplayNotImplementedMessage();
	}

	private void BtnPost_Click(object sender, RoutedEventArgs e)
	{
		DisplayNotImplementedMessage();
	}

	private (string chargeId, string label) GetChargeIdFromName(string controlName)
	{
		var ChargeId = string.Empty;
		var Label = string.Empty;
		//string[] pieces = controlName.Split('_');
		string[] Pieces = Regex.Split(controlName, ChargeControlNameDivider);

		switch (Pieces.Length)
		{
			case 2:
				// Cb_chargeId
				ChargeId = Pieces[1];
				break;

			case 3:
				// Lbl_chargeId_Result
				ChargeId = Pieces[1];
				Label = Pieces[2];
				break;

			default:
				Logging.WriteLogLine("ERROR ControlName splits into " + Pieces.Length + " parts");
				break;
		}

		return (ChargeId, Label);
	}

	private void Image_MouseUp(object sender, MouseButtonEventArgs e)
	{
		if (sender is Image Image)
		{
			//Logging.WriteLogLine( "Remove TripItem clicked: " + Image.Name );

			var RowToRemove = GetRowNumberFromName(Image.Name);

			//// Can't delete the first row
			//if( row > 1 )
			//{
			//}
			//Logging.WriteLogLine( "Deleting row: " + RowToRemove );

			var Tis = Model.TripPackages;

			//tis.RemoveAt(row - 2);
			Tis.RemoveAt(RowToRemove - 1);
			Model.TripPackages = Tis;

			//if (Model.TripItemsInventory.ContainsKey(row)) {
			//    Model.TripItemsInventory.Remove(row);
			//    Model.SelectedInventoryForTripItem = new List<ExtendedInventory>();
			//}

			Model.RemoveRowFromTripItemsInventory(RowToRemove);

			BuildPackageRows(Model);
		}
	}

	private static int GetRowNumberFromName(string name)
	{
		var Row = 0;

		var Pieces = name.ToLower().Split('_');

		if (Pieces.Length > 2)
		{
			if (!Pieces[1].ToLower().Contains("total"))
				Row = int.Parse(Pieces[1]);
		}

		return Row;
	}

	private void Image_MouseUp_1(object sender, MouseButtonEventArgs e)
	{
		//Logging.WriteLogLine( "Add TripPackage clicked" );

		//BuildNewPackageTypeRow(Model);
		//BuildNewPackageTotalRow(Model);
		//TripPackage tp = new TripPackage();
		var Tp = new DisplayPackage(new TripPackage())
		{
			Weight = Model.GetWeightDefault(), // 1,
			Pieces = Model.GetPiecesDefault()  //1
		};

		var Tis = Model.TripPackages;
		Tis.Add(Tp);
		Model.TripPackages = Tis;

		BuildPackageRows(Model);
	}

	private void CbPickupAddressCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if (!Disposed && DataContext is TripEntryModel Model)
		{
			Model.PickupCountry = (string)cbPickupAddressCountry.SelectedItem;
			Model.WhenPickupCountryChanges();
			cbPickupAddressProv.SelectedIndex = 0;
		}
	}

	private void CbDeliveryAddressCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if (!Disposed && DataContext is TripEntryModel Model)
		{
			Model.DeliveryCountry = (string)cbDeliveryAddressCountry.SelectedItem;
			Model.WhenDeliveryCountryChanges();
			cbDeliveryAddressProv.SelectedIndex = 0;
		}
	}

	private void BtnHelp_Click(object sender, RoutedEventArgs e)
	{
		if (DataContext is TripEntryModel Model)
		{
			//Logging.WriteLogLine( "Opening " + Model.HelpUri );
			var Uri = new Uri(Model.HelpUri);
			Process.Start(new ProcessStartInfo(Uri.AbsoluteUri));
		}
	}

	private void SelectAllText(RoutedEventArgs e)
	{
		if (!Disposed && e.OriginalSource is TextBox TextBox)
		{
			Keyboard.Focus(TextBox);
			TextBox.SelectAll();

			var Row = GetRowNumberFromName(TextBox.Name);

			if (DataContext is TripEntryModel Model)
				Model.LoadInventoryForTripItem(Row);
		}
	}
	/*
	        /// <summary>
	        ///     Takes an integer.
	        ///     /// Either attach this to the TextBox in XAML:
	        ///     PreviewTextInput="IntegerTextBox_PreviewTextInput"
	        ///     or in code as:
	        ///     this.pieces[row].AddHandler(TextBox.PreviewTextInputEvent, new
	        ///     TextCompositionEventHandler(this.IntegerTextBox_PreviewTextInput));
	        /// </summary>
	        /// <param name="sender">sender</param>
	        /// <param name="e">e</param>
	        private void IntegerTextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	        {
	            if( !char.IsDigit( e.Text, e.Text.Length - 1 ) )
	                e.Handled = true;
	        }
	*/

	/// <summary>
	///     Takes a float. Numbers and '.' only. Only allows 1 decimal.
	///     Either attach this to the TextBox in XAML:
	///     PreviewTextInput="FloatTextBox_PreviewTextInput"
	///     or in code as:
	///     this.pieces[row].AddHandler(TextBox.PreviewTextInputEvent, new
	///     TextCompositionEventHandler(this.FloatTextBox_PreviewTextInput));
	/// </summary>
	/// <param
	///     name="sender">
	///     sender
	/// </param>
	/// <param
	///     name="e">
	///     e
	/// </param>
	//private static void FloatTextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	private void FloatTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
	{
		if ((e.Text != null) && (e.Text.Trim().Length > 0))
		{
			var ToCheck = e.Text.Substring(e.Text.Length - 1);
			const string DECIMAL_CHAR = ".";

			switch (sender)
			{
				case TextBox Box:
					{
						if (Box.Text.IndexOf(DECIMAL_CHAR, StringComparison.Ordinal) > -1)
						{
							if (ToCheck.Equals(DECIMAL_CHAR))
								e.Handled = true;
							else if (!char.IsDigit(e.Text, e.Text.Length - 1) && !ToCheck.Equals(DECIMAL_CHAR))
								e.Handled = true;
						}
						break;
					}

				case DecimalUpDown when DataContext is TripEntryModel { IsMinimumWeightChangeLessThanOne: false }:
					{
						if (ToCheck == DECIMAL_CHAR)
							e.Handled = true;
						break;
					}
			}
		}
	}

	/// <summary>
	///     Opens a dialog where the Inventory items can be selected.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void EditContentsOfPackage(object sender, MouseButtonEventArgs e)
	{
		if (DataContext is TripEntryModel Model && sender is Image Image)
		{
			//Logging.WriteLogLine( "Editing Contents of row with " + Image.Name );

			var Row = GetRowNumberFromName(Image.Name);

			// Try to load
			var Inventory = Model.TripItemsInventory.ContainsKey(Row) ? new List<ExtendedInventory>(Model.TripItemsInventory[Row]) : new List<ExtendedInventory>();

			var (IsCancelled, Selected) = new EditPackagesInventory(Application.Current.MainWindow, Inventory).ShowEditPackagesInventory(Inventory);

			if (!IsCancelled)
			{
				//Logging.WriteLogLine( "Finished Editing Contents: selected " + Selected.Count + " items in row " + Row );

				Model.AddUpdateInventoryToPackageRow(Row, Selected);
				Model.LoadInventoryForTripItem(Row);

				var Pieces = "Row_" + Row + "_Pieces";

				if ((Selected.Count > 0) && (Row > 0))
				{
					var NewPieceCount = GetTotalPiecesForInventoryItems(Selected);

					// Lock the Pieces TextBox
					SetPiecesReadonlyAndEnabled(true, Pieces, NewPieceCount);

					// And set the pieces count to the total of the selected items' quantities
					//Model.TripPackages[row].Pieces = Model.Pieces = GetTotalPiecesForInventoryItems(selected);
					//Model.TripPackages[row].Pieces = GetTotalPiecesForInventoryItems(selected);
					Model.TripPackages[Row - 1].Pieces = NewPieceCount;

					RecalculateTripItemTotals();
				}
				else
				{
					// Unlock the Pieces TextBox
					SetPiecesReadonlyAndEnabled(false, Pieces, 1);

					// Remove any value
					//Model.TripPackages[row].Pieces = 0;
					Model.TripPackages[Row - 1].Pieces = 1;
				}
			}
		}
	}

	private void SetPiecesReadonlyAndEnabled(bool isReadonly, string name, int count)
	{
		foreach (var Tmp in gridTripItems.Children)
		{
			if (Tmp is TextBox Tb)
			{
				if (Tb.Name == name)
				{
					Tb.IsReadOnly = isReadonly;
					Tb.IsEnabled = !isReadonly;
					Tb.Text = count.ToString();
					break;
				}
			}
			else if (Tmp is IntegerUpDown Iud)
			{
				if (Iud.Name == name)
				{
					Iud.IsReadOnly = isReadonly;
					Iud.IsEnabled = !isReadonly;
					Iud.Value = count;
				}
			}
		}
	}

	private static int GetTotalPiecesForInventoryItems(IReadOnlyCollection<ExtendedInventory> eis)
	{
		var Total = 0;

		if (eis.Count > 0)
			Total += eis.Sum(ei => ei.Quantity);

		return Total;
	}

	private void GridTripItems_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
	{
		var Point = Mouse.GetPosition(gridTripItems);
		var Row = 0;
		var AccumulatedHeight = 0.0;

		foreach (var Rd in gridTripItems.RowDefinitions)
		{
			AccumulatedHeight += Rd.ActualHeight;

			if (AccumulatedHeight >= Point.Y)
				break;
			Row++;
		}
		Model.LoadInventoryForTripItem(Row);
	}

	private void TbCustomerPhone_TextChanged(object sender, TextChangedEventArgs e)
	{
		Model.CustomerPhone = tbCustomerPhone.Text.Trim();
	}

	private void DtpCallDate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//Logging.WriteLogLine( "CallDate changed: " + dtpCallDate.Value );
	}

	private void DpCallTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//Logging.WriteLogLine( "CallTime changed: " + tpCallTime.Value );
	}

	private void DtpReadyTimeDate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//Logging.WriteLogLine( "ReadyDate changed: " + dtpReadyTimeDate.Value );
	}

	private void DtpReadyTimeTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//Logging.WriteLogLine( "ReadyTime changed: " + dtpReadyTimeTime.Value );
	}

	private void DtpDueTimeDate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//Logging.WriteLogLine( "DueDate changed: " + dtpDueTimeDate.Value );
	}

	private void DtpDueTimeTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
	{
		//Logging.WriteLogLine( "DueTime changed: " + dtpDueTimeTime.Value );
	}

	private void Charge_TextChanged(object sender, TextChangedEventArgs e)
	{
		if (sender is TextBox tb)
		{
			//Logging.WriteLogLine($"Text changed {tb.Name} to {tb.Text}");
			OnLostFocus(sender, e);
		}
	}

	private void ClearChargesAndTotals()
	{
		if (DataContext is TripEntryModel Model)
		{
			foreach (var obj in GridCharges.Children)
			{
				if (obj is StackPanel sp)
				{
					foreach (var child in sp.Children)
					{
						switch (child)
						{
							case CheckBox cb:
								cb.IsChecked = false;
								break;

							case TextBox tb:
								tb.Text = string.Empty;
								// Check to see if there is a default value for this charge
								var (chargeId, _) = GetChargeIdFromName(tb.Name);

								if (chargeId.IsNotNullOrWhiteSpace())
								{
									var c = (from C in Model.Charges
											 where (C.ChargeId == chargeId) && (C.Value > 0)
											 select C).FirstOrDefault();

									if (c != null)
									{
										tb.Text = "0";
										//tb.Text = c.Value.ToString( "F2" );
									}
								}
								break;

							case Label lbl when lbl.Name.IsNotNullOrWhiteSpace() && lbl.Name.Contains("_"):
								if (!lbl.Name.Contains(Model.SURCHARGE_PREFIX))
								{
									decimal value = 0;
									lbl.Content = value.ToString("C");
								}
								else
								{
									lbl.Visibility = Visibility.Collapsed;
								}
								break;
						}
					}
				}
			}

			Model.TotalDeliveryCharges = Model.TotalCharges = Model.TotalFuelSurcharges = Model.TotalDiscount = Model.TotalTaxes = Model.TotalBeforeTax = Model.TotalPayroll = Model.TotalTotal;
		}
	}

	private void PopulateCharge(Charge charge, TripCharge tc)
	{
		//Logging.WriteLogLine("DEBUG Loading charge: " + tc.ChargeId);
		//string idPortion = "_" + charge.ChargeId; // + "_"; // TextBoxes don't have the trailing _
		var idPortion = ChargeControlNameDivider + charge.ChargeId; // + "_"; // TextBoxes don't have the trailing _

		foreach (var obj in GridCharges.Children)
		{
			if (obj is StackPanel sp)
			{
				foreach (var child in sp.Children)
				{
					if (child is CheckBox cb && cb.Name.IsNotNullOrWhiteSpace() && cb.Name.Contains(idPortion))
						cb.IsChecked = true;
					else if (child is TextBox tb && tb.Name.IsNotNullOrWhiteSpace() && tb.Name.Contains(idPortion))
					{
						if (!charge.IsText)
							tb.Text = charge.Value.ToString("F2");
						else
							tb.Text = tc.Text;
					}
					else if (child is Label lbl && lbl.Name.IsNotNullOrWhiteSpace() && lbl.Name.Contains(idPortion))
						lbl.Content = tc.Value.ToString("C");
				}

				//break;
			}
		}
	}

	public void ApplyDefaultChargesAlwaysApplied()
	{
		//var idPortion = ChargeControlNameDivider + charge.ChargeId; // + "_"; // TextBoxes don't have the trailing _

		foreach (var obj in GridCharges.Children)
		{
			if (obj is StackPanel sp)
			{
				string idPortion = string.Empty;
				foreach (var child in sp.Children)
				{
					if (child is CheckBox cb && cb.Name.IsNotNullOrWhiteSpace())
					{
						cb.IsChecked = false;
						(idPortion, _) = GetChargeIdFromName(cb.Name);
						(bool check, _) = CheckForDefaultChargeAlwaysApplied(idPortion);
						if (check)
						{
							cb.IsChecked = true;
						}

					}
					else if (child is TextBox tb && tb.Name.IsNotNullOrWhiteSpace() && tb.Name.Contains(idPortion))
					{
						(bool check, Charge? dc) = CheckForDefaultChargeAlwaysApplied(idPortion);
						if (check && dc != null)
						{
							if (!dc.IsText)
								tb.Text = dc.Value.ToString("F2");
							else
								tb.Text = dc.Text;
						}
					}
					//else if (child is Label lbl && lbl.Name.IsNotNullOrWhiteSpace() && lbl.Name.Contains(idPortion))
					//    lbl.Content = tc.Value.ToString("C");
				}

				//break;
			}
		}
	}

	public void ApplyTripCharges(List<TripCharge> tcs)
	{
		if (tcs?.Count > 0 && DataContext is TripEntryModel model)
		{
			lock (tcs)
			{
				foreach (TripCharge tc in tcs)
				{
					Logging.WriteLogLine($"DEBUG Found charge: {tc.ChargeId} to apply");
					bool isDefaultCharge = tc.ChargeId.StartsWith(model.ACCOUNT_CHARGE_PREFIX);
					string id = isDefaultCharge ? PriceTripHelper.GetChargeIdFromDefaultTripCharge(tc.ChargeId, model.SelectedAccount) : tc.ChargeId;
					foreach (var obj in GridCharges.Children)
					{
						if (obj is StackPanel sp)
						{
							foreach (var child in sp.Children)
							{
								if (child is CheckBox cb && cb.Name.IsNotNullOrWhiteSpace() && cb.Name.Contains(id))
								{
									cb.IsChecked = true;
								}
								else if (child is TextBox tb && tb.Name.IsNotNullOrWhiteSpace() && tb.Name.Contains(id))
								{
									tb.Text = tc.Text.IsNullOrWhiteSpace() ? tc.Value.ToString("F2") : tc.Text;
								}
							}
						}
					}
				}
			}
		}
	}


	private (bool, Charge?) CheckForDefaultChargeAlwaysApplied(string chargeId)
	{
		bool check = false;
		Charge? dc = null;

		if (DataContext is TripEntryModel model && model.DefaultCharges.Count > 0)
		{
			dc = (from c in model.DefaultCharges
				  where c.ChargeId == PriceTripHelper.MakeDefaultChargeId(model.SelectedAccount, chargeId)
				  select c).FirstOrDefault();
			if (dc != null && (dc.SortIndex == -1 || dc.SortIndex == -3))
			{
				Logging.WriteLogLine($"Found Default Charge {dc.ChargeId}");
				check = true;
			}
		}

		return (check, dc);
	}

	private void ChargeCheckBox_Checked(object sender, RoutedEventArgs e)
	{
		if (sender is CheckBox Cb)
		{
			var ChargeId = Cb.Name.Substring(Cb.Name.IndexOf("_", StringComparison.Ordinal) + 1);
			var ToFindTb = "Tb_" + ChargeId;
			var ToFindLbl = "Lbl_" + ChargeId + "_Result";

			foreach (var Obj in GridCharges.Children)
			{
				if (Obj is StackPanel Sp)
				{
					foreach (var Child in Sp.Children)
					{
						switch (Child)
						{
							case TextBox Tb when (Tb.Name == ToFindTb):
								Tb.IsEnabled = true;
								break;

							case Label Lbl when (Lbl.Name == ToFindLbl):
								Lbl.IsEnabled = true;
								break;
						}
					}
				}
			}
		}
	}

	private void ChargeCheckBox_Unchecked(object sender, RoutedEventArgs e)
	{
		if (sender is CheckBox cb)
		{
			var chargeId = cb.Name.Substring(cb.Name.IndexOf("_", StringComparison.Ordinal) + 1);
			var toFindTb = "Tb_" + chargeId;
			var toFindLbl = "Lbl_" + chargeId + "_Result";

			foreach (var obj in GridCharges.Children)
			{
				if (obj is StackPanel sp)
				{
					foreach (var child in sp.Children)
					{
						if (child is TextBox tb && (tb.Name == toFindTb))
							tb.IsEnabled = false;
						else if (child is Label lbl && (lbl.Name == toFindLbl))
							lbl.IsEnabled = false;
					}
				}
			}
		}
	}

	private void BtnLoadCalculator_Click(object? sender, RoutedEventArgs? e)
	{
		Logging.WriteLogLine("Loading Windows calculator");
		Process.Start("calc.exe");
	}

	private void ComboBox_GotFocus(object sender, RoutedEventArgs e)
	{
		if (sender is ComboBox cb)
			cb.IsDropDownOpen = true;
	}

	private void CbAccount_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		switch (e.Key)
		{
			case Key.Enter:
			case Key.Tab:
				{
					if (cbAccount.SelectedIndex > -1 && DataContext is TripEntryModel Model)
					{
						Model.WhenAccountChanges();
						tbCallerName.Focus();
						e.Handled = true;
					}
					break;
				}

			case Key.Escape:
				cbAccount.IsDropDownOpen = false;
				cbAccount.SelectedIndex = -1;
				e.Handled = true;
				break;

			default:
				cbAccount.IsDropDownOpen = true;
				break;
		}
	}

	private void CbCustomer_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		switch (e.Key)
		{
			case Key.Enter:
				{
					if (cbCustomer.SelectedIndex > -1 && DataContext is TripEntryModel Model)
					{
						Model.WhenRawCustomerChanges();
						Model.WhenCustomerChanges();
						tbCallerName.Focus();
						//e.Handled = true;
					}
					break;
				}

			case Key.Tab:
				{
					if (cbCustomer.SelectedIndex > -1 && DataContext is TripEntryModel Model)
					{
						Model.WhenRawCustomerChanges();
						Model.WhenCustomerChanges();
						tbCallerName.Focus();
						e.Handled = true;
					}
					break;
				}

			case Key.Escape:
				{
					cbCustomer.IsDropDownOpen = false;
					cbCustomer.SelectedIndex = -1;
					e.Handled = true;
					break;
				}

			default:
				cbCustomer.IsDropDownOpen = true;
				break;
		}
	}

	private async void BtnEditAddressPickup_Click(object sender, RoutedEventArgs e)
	{
		//Logging.WriteLogLine("Editing Pickup address");
		var CurrentAddress = Model.SelectedPickupAddress;
		var (Cancelled, Result) = await EditAddress(CurrentAddress, Model.PickupName, Model.PickupPhone, Model.PickupSuite, Model.PickupStreet, Model.PickupCity, Model.PickupProvState, Model.PickupCountry, Model.PickupPostalZip);

		if (!Cancelled && (Result != null))
		{
			Model.PickupName = Result.Company.ContactName;
			Model.PickupPhone = Result.Company.Phone;
			Model.PickupSuite = Result.Company.Suite;
			Model.PickupStreet = Result.Company.AddressLine1;
			Model.PickupCity = Result.Company.City;
			Model.PickupCountry = Result.Company.Country;
			Model.PickupProvState = Result.Company.Region;
			Model.PickupPostalZip = Result.Company.PostalCode;

			Model.PickupAddressUpdate = Result;
		}
	}

	private async void BtnEditAddressDelivery_Click(object sender, RoutedEventArgs e)
	{
		//Logging.WriteLogLine("Editing Delivery address");
		var CurrentAddress = Model.SelectedDeliveryAddress;
		var (Cancelled, Result) = await EditAddress(CurrentAddress, Model.DeliveryName, Model.DeliveryPhone, Model.DeliverySuite, Model.DeliveryStreet, Model.DeliveryCity, Model.DeliveryProvState, Model.DeliveryCountry, Model.DeliveryPostalZip);

		if (!Cancelled && (Result != null))
		{
			Model.DeliveryName = Result.Company.ContactName;
			Model.DeliveryPhone = Result.Company.Phone;
			Model.DeliverySuite = Result.Company.Suite;
			Model.DeliveryStreet = Result.Company.AddressLine1;
			Model.DeliveryCity = Result.Company.City;
			Model.DeliveryCountry = Result.Company.Country;
			Model.DeliveryProvState = Result.Company.Region;
			Model.DeliveryPostalZip = Result.Company.PostalCode;

			Model.DeliveryAddressUpdate = Result;
		}
	}

	private async Task<(bool cancelled, UpdateCustomerCompany? result)> EditAddress(string currentAddress, string contactName, string phone,
																					 string suite, string addressLine1, string city,
																					 string region, string country, string postalCode)
	{
		var Cancelled = false;
		UpdateCustomerCompany? Result = null;

		{
			var Tmp = (from A in Model.Addresses
					   where string.Equals(A.CompanyName, currentAddress, StringComparison.CurrentCultureIgnoreCase)
					   select A).FirstOrDefault();

			if (Tmp != null)
			{
				//if (string.Compare(Tmp.CompanyName, currentAddress, StringComparison.CurrentCultureIgnoreCase) == 0)
				//{
				CompanyDetail? Cd = null;

				var AccountId = string.Empty;

				if (Model.DetailedAddresses.ContainsKey(currentAddress.ToLower()))
				{
					Cd = Model.DetailedAddresses[currentAddress.ToLower()];
					AccountId = Model.SelectedAccount;
				}
				else if (Model.GlobalDetailedAddresses.ContainsKey(currentAddress.ToLower()))
				{
					Cd = Model.GlobalDetailedAddresses[currentAddress.ToLower()];
					AccountId = Model.GlobalAddressBookId;
				}

				if (Cd != null)
				{
					//Company? latest = Model.GetLatestCompany(Tmp.CompanyName, accountId);
					//if (latest != null)
					//{
					//	cd.Company = latest;
					//	cd.Address.ContactName = latest.UserName;
					//	cd.Address.Phone = latest.Phone;
					//	cd.Address.LocationBarcode = latest.LocationBarcode;
					//	cd.Address.Suite = latest.Suite;
					//	cd.Address.AddressLine1 = latest.AddressLine1;
					//	cd.Address.City = latest.City;
					//	cd.Address.Region = latest.Region;
					//	cd.Address.Country = latest.Country;
					//	cd.Address.PostalCode = latest.PostalCode;
					//}
					var Latest = await Model.GetLatestCompany(Tmp.CompanyName, AccountId);

					if (Latest != null)
					{
						Cd.Company = Latest;
						Cd.Company.ContactName = contactName;
						Cd.Company.Phone = phone;
						Cd.Company.Suite = suite;
						Cd.Company.AddressLine1 = addressLine1;
						Cd.Company.City = city;
						Cd.Company.Region = region;
						Cd.Company.Country = country;
						Cd.Company.PostalCode = postalCode;
						Cd.Address.ContactName = Latest.UserName;
						Cd.Address.Phone = Latest.Phone;
						Cd.Address.LocationBarcode = Latest.LocationBarcode;
						Cd.Address.Suite = Latest.Suite;
						Cd.Address.AddressLine1 = Latest.AddressLine1;
						Cd.Address.City = Latest.City;
						Cd.Address.Region = Latest.Region;
						Cd.Address.Country = Latest.Country;
						Cd.Address.PostalCode = Latest.PostalCode;
					}

					(Cancelled, Cd) = new EditAddress.EditAddress(Application.Current.MainWindow).ShowEditAddress(Cd, contactName, phone, suite, addressLine1, city, region, country, postalCode);

					//Logging.WriteLogLine("DEBUG cancelled: " + cancelled);
					if (!Cancelled && (Cd != null))
					{
						Tmp.Suite = Cd.Company.Suite;
						Tmp.AddressLine1 = Cd.Company.AddressLine1;
						Tmp.City = Cd.Company.City;
						Tmp.Region = Cd.Company.Region;
						Tmp.PostalCode = Cd.Company.PostalCode;

						Company Company = new()
						{
							//ContactName = cd.Company.ContactName,
							ContactName = Cd.Company.UserName,
							LocationBarcode = Tmp.LocationBarcode,
							Suite = Tmp.Suite,
							AddressLine1 = Tmp.AddressLine1,
							City = Tmp.City,
							Country = Cd.Company.Country,
							CompanyName = Tmp.CompanyName,
							PostalCode = Tmp.PostalCode,
							Region = Tmp.Region,
							UserName = Cd.Company.UserName,
							Phone = Cd.Company.Phone,
							EmailAddress = Cd.Company.EmailAddress,
							Notes = Cd.Company.Notes,
							Latitude = Cd.Company.Latitude,
							Longitude = Cd.Company.Longitude
						};

						// Is it a global address or one belonging to the SelectedAccount?
						Result = new UpdateCustomerCompany(PROGRAM)
						{
							Company = Company,
							CustomerCode = Model.GlobalAddressBookId
						};

						if (!Model.GlobalDetailedAddresses.ContainsKey(currentAddress))
							Result.CustomerCode = Model.SelectedAccount;
					}
				}

				//break;
				//}
			}
		}
		return (Cancelled, Result);
	}


	private async Task<(bool cancelled, UpdateCustomerCompany? result)> EditAddress_V1(string currentAddress, string contactName, string phone,
																						string suite, string addressLine1, string city,
																						string region, string country, string postalCode)
	{
		var cancelled = false;
		UpdateCustomerCompany? result = null;

		if (DataContext is TripEntryModel Model)
		{
			foreach (var Tmp in Model.Addresses)
			{
				if (string.Compare(Tmp.CompanyName, currentAddress, StringComparison.CurrentCultureIgnoreCase) == 0)
				{
					CompanyDetail? cd = null;

					var accountId = string.Empty;

					if (Model.DetailedAddresses.ContainsKey(currentAddress.ToLower()))
					{
						cd = Model.DetailedAddresses[currentAddress.ToLower()];
						accountId = Model.SelectedAccount;
					}
					else if (Model.GlobalDetailedAddresses.ContainsKey(currentAddress.ToLower()))
					{
						cd = Model.GlobalDetailedAddresses[currentAddress.ToLower()];
						accountId = Model.GlobalAddressBookId;
					}

					if (cd != null)
					{
						//Company? latest = Model.GetLatestCompany(Tmp.CompanyName, accountId);
						//if (latest != null)
						//{
						//	cd.Company = latest;
						//	cd.Address.ContactName = latest.UserName;
						//	cd.Address.Phone = latest.Phone;
						//	cd.Address.LocationBarcode = latest.LocationBarcode;
						//	cd.Address.Suite = latest.Suite;
						//	cd.Address.AddressLine1 = latest.AddressLine1;
						//	cd.Address.City = latest.City;
						//	cd.Address.Region = latest.Region;
						//	cd.Address.Country = latest.Country;
						//	cd.Address.PostalCode = latest.PostalCode;
						//}
						var latest = await Model.GetLatestCompany(Tmp.CompanyName, accountId);

						if (latest != null)
						{
							cd.Company = latest;
							cd.Company.ContactName = contactName;
							cd.Company.Phone = phone;
							cd.Company.Suite = suite;
							cd.Company.AddressLine1 = addressLine1;
							cd.Company.City = city;
							cd.Company.Region = region;
							cd.Company.Country = country;
							cd.Company.PostalCode = postalCode;
							cd.Address.ContactName = latest.UserName;
							cd.Address.Phone = latest.Phone;
							cd.Address.LocationBarcode = latest.LocationBarcode;
							cd.Address.Suite = latest.Suite;
							cd.Address.AddressLine1 = latest.AddressLine1;
							cd.Address.City = latest.City;
							cd.Address.Region = latest.Region;
							cd.Address.Country = latest.Country;
							cd.Address.PostalCode = latest.PostalCode;
						}

						(cancelled, cd) = new EditAddress.EditAddress(Application.Current.MainWindow).ShowEditAddress(cd, contactName, phone, suite, addressLine1, city, region, country, postalCode);

						//Logging.WriteLogLine("DEBUG cancelled: " + cancelled);
						if (!cancelled && (cd != null))
						{
							Tmp.Suite = cd.Company.Suite;
							Tmp.AddressLine1 = cd.Company.AddressLine1;
							Tmp.City = cd.Company.City;
							Tmp.Region = cd.Company.Region;
							Tmp.PostalCode = cd.Company.PostalCode;

							Company company = new()
							{
								//ContactName = cd.Company.ContactName,
								ContactName = cd.Company.UserName,
								LocationBarcode = Tmp.LocationBarcode,
								Suite = Tmp.Suite,
								AddressLine1 = Tmp.AddressLine1,
								City = Tmp.City,
								Country = cd.Company.Country,
								CompanyName = Tmp.CompanyName,
								PostalCode = Tmp.PostalCode,
								Region = Tmp.Region,
								UserName = cd.Company.UserName,
								Phone = cd.Company.Phone,
								EmailAddress = cd.Company.EmailAddress,
								Notes = cd.Company.Notes,
								Latitude = cd.Company.Latitude,
								Longitude = cd.Company.Longitude
							};

							// Is it a global address or one belonging to the SelectedAccount?
							result = new UpdateCustomerCompany(PROGRAM)
							{
								Company = company,
								CustomerCode = Model.GlobalAddressBookId
							};

							if (!Model.GlobalDetailedAddresses.ContainsKey(currentAddress))
								result.CustomerCode = Model.SelectedAccount;
						}
					}

					break;
				}
			}
		}
		return (cancelled, result);
	}

	private void CbPickupAddressProvCountry_DropDownOpened(object sender, EventArgs e)
	{
		if (DataContext is TripEntryModel { ArePickupAddressFieldsReadOnly: true })
		{
			if (sender is ComboBox Cb)
				Cb.IsDropDownOpen = false;
		}
	}

	private void CbDeliveryAddressProvCountry_DropDownOpened(object sender, EventArgs e)
	{
		if (DataContext is TripEntryModel { AreDeliveryAddressFieldsReadOnly: true })
		{
			if (sender is ComboBox Cb)
				Cb.IsDropDownOpen = false;
		}
	}

	private void CbPickupAddresses_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.Tab)
		{
			if ((cbPickupAddresses.SelectedIndex > -1) && DataContext is TripEntryModel { ArePickupAddressFieldsReadOnly: true })
			{
				tbPickupAddressPostalZip.Focus();
				e.Handled = true;
			}
		}
	}

	private void CbDeliveryAddresses_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.Tab)
		{
			if ((cbDeliveryAddresses.SelectedIndex > -1) && DataContext is TripEntryModel { AreDeliveryAddressFieldsReadOnly: true })
			{
				tbDeliveryAddressPostalZip.Focus();
				e.Handled = true;
			}
		}
	}

	private void MenuItem_Click_2(object sender, RoutedEventArgs e)
	{
		BtnProducePictures_Click(sender, e);
	}


	private void BtnProducePictures_Click(object sender, RoutedEventArgs e)
	{
		if (Model.TripId.IsNotNullOrWhiteSpace())
		{
			var TripId = Model.TripId;
			Logging.WriteLogLine("Display pictures for TripId: " + TripId);
			new ShowPicturesForShipment(Application.Current.MainWindow).ShowPictures(TripId);
		}
		else
			Logging.WriteLogLine("No tripId");
	}

	/// <summary>
	/// </summary>
	/// <returns></returns>

	//private Trip CreateTripFromFields()
	//{
	//    Trip trip = new Trip(new Trip());

	//    return trip;
	//}
	public void CbPickupAddresses_SelectionChanged(object? sender, SelectionChangedEventArgs? e)
	{
		if (!cbPickupAddresses.IsDropDownOpen)
		{
			LoadSelectedPickupAddress();
			/*
			var CurrentAddress = Model.SelectedPickupAddress;

			if( CurrentAddress.IsNotNullOrWhiteSpace() )
			{
				var found = false;

				foreach( var Tmp in Model.Addresses )
					//foreach( var Tmp in Model.DetailedAddresses.Keys )
				{
					if( string.Compare( Tmp.CompanyName, CurrentAddress, StringComparison.CurrentCultureIgnoreCase ) == 0 )
						//if( string.Compare( Model.DetailedAddresses[Tmp].Company.CompanyName, CurrentAddress, StringComparison.CurrentCultureIgnoreCase ) == 0 )
					{
						Dispatcher.Invoke( async () =>
						                   {
							                   found = true;

							                   Model.PickupCompany  = Tmp.CompanyName;
							                   Model.PickupSuite    = Tmp.Suite;
							                   Model.PickupStreet   = Tmp.AddressLine1;
							                   Model.PickupCity     = Tmp.City;
							                   Model.PickupLocation = Tmp.LocationBarcode;

							                   CompanyDetail? Da = null;

							                   string accountId = string.Empty;

							                   if( Model.DetailedAddresses.ContainsKey( CurrentAddress.ToLower() ) )
							                   {
								                   Da        = Model.DetailedAddresses[ CurrentAddress.ToLower() ];
								                   accountId = Model.SelectedAccount;
							                   }
							                   else if( Model.GlobalDetailedAddresses.ContainsKey( CurrentAddress.ToLower() ) )
							                   {
								                   Da        = Model.GlobalDetailedAddresses[ CurrentAddress.ToLower() ];
								                   accountId = Model.GlobalAddressBookId;
							                   }

							                   if( Da is not null )
							                   {
								                   Company? latest = await Model.GetLatestCompany( Tmp.CompanyName, accountId );

								                   if( latest != null )
								                   {
									                   Model.PickupSuite        = latest.Suite;
									                   Model.PickupStreet       = latest.AddressLine1;
									                   Model.PickupCity         = latest.City;
									                   Model.PickupLocation     = latest.LocationBarcode;
									                   Model.PickupAddressNotes = latest.Notes;
									                   Model.PickupCountry      = latest.Country;

									                   // Problem - addresses are still appearing with abbreviations for the region
									                   if( CountriesRegions.Countries.Contains( latest.Country ) )
									                   {
										                   var (ProvState, Found) = CountriesRegions.FindRegionForCountry( latest.Region, latest.Country );

										                   if( Found )
											                   Model.PickupProvState = ProvState;
										                   else
										                   {
											                   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( latest.Region, latest.Country );

											                   Model.PickupProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : latest.Region;
										                   }
									                   }
									                   else
									                   {
										                   // Can't find a match - use what is there
										                   Model.PickupProvState = latest.Region;
									                   }

									                   //Model.PickupProvState    = latest.Region;
									                   Model.PickupPostalZip = latest.PostalCode;
									                   Model.PickupName      = latest.UserName;
									                   Model.PickupPhone     = latest.Phone;

									                   Model.WhenPickupCountryChanges();

									                   var (Nme, _)
										                   //Model.PickupProvState ??= latest.Region;
										                   = CountriesRegions.FindRegionForCountry( latest.Region, Model.PickupCountry );

									                   Model.PickupProvState = Nme;

									                   if( Da != null )
									                   {
										                   //Model.PickupProvState = latest.Region;
										                   var (ProvState, _)    = CountriesRegions.FindRegionForCountry( latest.Region, Model.PickupCountry );
										                   Model.PickupProvState = ProvState;
									                   }
								                   }
								                   else
								                   {
									                   Model.PickupSuite        = Da.Company.Suite;
									                   Model.PickupStreet       = Da.Company.AddressLine1;
									                   Model.PickupCity         = Da.Company.City;
									                   Model.PickupAddressNotes = Da.Address.Notes;
									                   Model.PickupCountry      = Da.Address.Country;

									                   // Problem - addresses are still appearing with abbreviations for the region
									                   if( CountriesRegions.Countries.Contains( Da.Address.Country ) )
									                   {
										                   var (ProvState, Found) = CountriesRegions.FindRegionForCountry( Da.Address.Region, Da.Address.Country );

										                   if( Found )
											                   Model.PickupProvState = ProvState;
										                   else
										                   {
											                   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( Da.Address.Region, Da.Address.Country );

											                   Model.PickupProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : Da.Address.Region;
										                   }
									                   }
									                   else
									                   {
										                   // Can't find a match - use what is there
										                   Model.PickupProvState = Da.Address.Region;
									                   }

									                   //Model.PickupProvState    = da.Address.Region;
									                   Model.PickupPostalZip = Da.Address.PostalCode;
									                   Model.PickupName      = Da.Company.UserName;
									                   Model.PickupPhone     = Da.Company.Phone;
								                   }
							                   }
							                   else
							                   {
								                   Model.WhenPickupCountryChanges();

								                   if( Da is not null )
								                   {
									                   //Model.PickupProvState = da.Address.Region;
									                   //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
									                   var (ProvState, _)    = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
									                   Model.PickupProvState = ProvState;
								                   }

								                   if( Da is not null )
								                   {
									                   //Model.PickupProvState = da.Address.Region;
									                   //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
									                   var (ProvState, _)    = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
									                   Model.PickupProvState = ProvState;
								                   }
							                   }
							                   //else
							                   //{
							                   //    Model.PickupCountry = tmp.
							                   //}

							                   //Task.WaitAll( Task.Run( () =>
							                   //                        {
							                   //                         Model.WhenPickupCountryChanges();

							                   //                         if( Da is not null )
							                   //                         {
							                   //                          //Model.PickupProvState = da.Address.Region;
							                   //                          //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
							                   //                          var (ProvState, _)    = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
							                   //                          Model.PickupProvState = ProvState;
							                   //                         }
							                   //                        } ) );

							                   //if( Da is not null )
							                   //{
							                   // //Model.PickupProvState = da.Address.Region;
							                   // //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
							                   // var (ProvState, _)    = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
							                   // Model.PickupProvState = ProvState;
							                   //}
						                   } );
						break;
					}
				}

				if( !found )
				{
					Logging.WriteLogLine( "Can't find an address for " + CurrentAddress );
				}
				Model.ArePickupAddressFieldsReadOnly = found;
			}
			else
			{
				Model.ArePickupAddressFieldsReadOnly = false;
				// Empty the fields
				Model.PickupSuite        = string.Empty;
				Model.PickupStreet       = string.Empty;
				Model.PickupCity         = string.Empty;
				Model.PickupAddressNotes = string.Empty;
				//Model.PickupCountry = string.Empty;
				Model.PickupPostalZip               = string.Empty;
				Model.PickupName                    = string.Empty;
				Model.PickupPhone                   = string.Empty;
				Model.PickupLocationBarcode         = string.Empty;
				Model.PickupZone                    = string.Empty;
				tbPickupAddressLocationBarcode.Text = string.Empty;
			}
			*/
		}
	}

	public void LoadSelectedPickupAddress()
	{
		if (DataContext is TripEntryModel Model)
		{
			var CurrentAddress = Model.SelectedPickupAddress;

			if (CurrentAddress.IsNotNullOrWhiteSpace())
			{
				var found = false;

				foreach (var Tmp in Model.Addresses)
				//foreach( var Tmp in Model.DetailedAddresses.Keys )
				{
					if (string.Compare(Tmp.CompanyName, CurrentAddress, StringComparison.CurrentCultureIgnoreCase) == 0)
					//if( string.Compare( Model.DetailedAddresses[Tmp].Company.CompanyName, CurrentAddress, StringComparison.CurrentCultureIgnoreCase ) == 0 )
					{
						Dispatcher.Invoke(async () =>
										   {
											   found = true;

											   Model.PickupCompany = Tmp.CompanyName;
											   Model.PickupSuite = Tmp.Suite;
											   Model.PickupStreet = Tmp.AddressLine1;
											   Model.PickupCity = Tmp.City;
											   Model.PickupLocation = Tmp.LocationBarcode;

											   CompanyDetail? Da = null;

											   var accountId = string.Empty;

											   if (Model.DetailedAddresses.ContainsKey(CurrentAddress.ToLower()))
											   {
												   Da = Model.DetailedAddresses[CurrentAddress.ToLower()];
												   accountId = Model.SelectedAccount;
											   }
											   else if (Model.GlobalDetailedAddresses.ContainsKey(CurrentAddress.ToLower()))
											   {
												   Da = Model.GlobalDetailedAddresses[CurrentAddress.ToLower()];
												   accountId = Model.GlobalAddressBookId;
											   }

											   if (Da is not null)
											   {
												   var latest = await Model.GetLatestCompany(Tmp.CompanyName, accountId);

												   if (latest != null)
												   {
													   Model.PickupSuite = latest.Suite;
													   Model.PickupStreet = latest.AddressLine1;
													   Model.PickupCity = latest.City;
													   Model.PickupLocation = latest.LocationBarcode;
													   Model.PickupAddressNotes = latest.Notes;
													   Model.PickupCountry = latest.Country;

													   // Problem - addresses are still appearing with abbreviations for the region
													   if (CountriesRegions.Countries.Contains(latest.Country))
													   {
														   var (ProvState, Found) = CountriesRegions.FindRegionForCountry(latest.Region, latest.Country);

														   if (Found)
															   Model.PickupProvState = ProvState;
														   else
														   {
															   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry(latest.Region, latest.Country);

															   Model.PickupProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : latest.Region;
														   }
													   }
													   else
													   {
														   // Can't find a match - use what is there
														   Model.PickupProvState = latest.Region;
													   }

													   //Model.PickupProvState    = latest.Region;
													   Model.PickupPostalZip = latest.PostalCode;
													   Model.PickupName = latest.UserName;
													   Model.PickupPhone = latest.Phone;

													   Model.WhenPickupCountryChanges();

													   var (Nme, _)
														   //Model.PickupProvState ??= latest.Region;
														   = CountriesRegions.FindRegionForCountry(latest.Region, Model.PickupCountry);

													   Model.PickupProvState = Nme;

													   if (Da != null)
													   {
														   //Model.PickupProvState = latest.Region;
														   var (ProvState, _) = CountriesRegions.FindRegionForCountry(latest.Region, Model.PickupCountry);
														   Model.PickupProvState = ProvState;
													   }
												   }
												   else
												   {
													   Model.PickupSuite = Da.Company.Suite;
													   Model.PickupStreet = Da.Company.AddressLine1;
													   Model.PickupCity = Da.Company.City;
													   Model.PickupAddressNotes = Da.Address.Notes;
													   Model.PickupCountry = Da.Address.Country;

													   // Problem - addresses are still appearing with abbreviations for the region
													   if (CountriesRegions.Countries.Contains(Da.Address.Country))
													   {
														   var (ProvState, Found) = CountriesRegions.FindRegionForCountry(Da.Address.Region, Da.Address.Country);

														   if (Found)
															   Model.PickupProvState = ProvState;
														   else
														   {
															   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry(Da.Address.Region, Da.Address.Country);

															   Model.PickupProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : Da.Address.Region;
														   }
													   }
													   else
													   {
														   // Can't find a match - use what is there
														   Model.PickupProvState = Da.Address.Region;
													   }

													   //Model.PickupProvState    = da.Address.Region;
													   Model.PickupPostalZip = Da.Address.PostalCode;
													   Model.PickupName = Da.Company.UserName;
													   Model.PickupPhone = Da.Company.Phone;
												   }
											   }
											   else
											   {
												   Model.WhenPickupCountryChanges();

												   if (Da is not null)
												   {
													   //Model.PickupProvState = da.Address.Region;
													   //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
													   var (ProvState, _) = CountriesRegions.FindRegionForCountry(Da.Address.Region, Model.PickupCountry);
													   Model.PickupProvState = ProvState;
												   }

												   if (Da is not null)
												   {
													   //Model.PickupProvState = da.Address.Region;
													   //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
													   var (ProvState, _) = CountriesRegions.FindRegionForCountry(Da.Address.Region, Model.PickupCountry);
													   Model.PickupProvState = ProvState;
												   }
											   }
											   //else
											   //{
											   //    Model.PickupCountry = tmp.
											   //}

											   //Task.WaitAll( Task.Run( () =>
											   //                        {
											   //                         Model.WhenPickupCountryChanges();

											   //                         if( Da is not null )
											   //                         {
											   //                          //Model.PickupProvState = da.Address.Region;
											   //                          //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
											   //                          var (ProvState, _)    = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
											   //                          Model.PickupProvState = ProvState;
											   //                         }
											   //                        } ) );

											   //if( Da is not null )
											   //{
											   // //Model.PickupProvState = da.Address.Region;
											   // //Logging.WriteLogLine("DEBUG Da.Address.Region: " + Da.Address.Region + " Model.PickupCountry" + Model.PickupCountry);
											   // var (ProvState, _)    = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
											   // Model.PickupProvState = ProvState;
											   //}
										   });
						break;
					}
				}

				if (!found)
					Logging.WriteLogLine("Can't find an address for " + CurrentAddress);
				Model.ArePickupAddressFieldsReadOnly = found;
			}
			else
			{
				Model.ArePickupAddressFieldsReadOnly = false;
				// Empty the fields
				Model.PickupSuite = string.Empty;
				Model.PickupStreet = string.Empty;
				Model.PickupCity = string.Empty;
				Model.PickupAddressNotes = string.Empty;
				//Model.PickupCountry = string.Empty;
				Model.PickupPostalZip = string.Empty;
				Model.PickupName = string.Empty;
				Model.PickupPhone = string.Empty;
				Model.PickupLocationBarcode = string.Empty;
				Model.PickupZone = string.Empty;
				tbPickupAddressLocationBarcode.Text = string.Empty;
			}
		}
	}

	public void CbDeliveryAddresses_SelectionChanged(object? sender, SelectionChangedEventArgs? e)
	{
		if (!cbDeliveryAddresses.IsDropDownOpen)
		{
			LoadSelectedDeliveryAddress();
			/*var CurrentAddress = Model.SelectedDeliveryAddress;

			if( CurrentAddress.IsNotNullOrWhiteSpace() )
			{
				var found = false;

				foreach( var Tmp in Model.Addresses )
				{
					if( string.Compare( Tmp.CompanyName, CurrentAddress, StringComparison.CurrentCultureIgnoreCase ) == 0 )
					{
						Dispatcher.Invoke( async () =>
						                   {
							                   //Logging.WriteLogLine( "Loading address: " + CurrentAddress );
							                   found = true;

							                   Model.DeliveryCompany  = Tmp.CompanyName;
							                   Model.DeliverySuite    = Tmp.Suite;
							                   Model.DeliveryStreet   = Tmp.AddressLine1;
							                   Model.DeliveryCity     = Tmp.City;
							                   Model.DeliveryLocation = Tmp.LocationBarcode;

							                   CompanyDetail? Da = null;

							                   string accountId = string.Empty;

							                   if( Model.DetailedAddresses.ContainsKey( CurrentAddress.ToLower() ) )
							                   {
								                   Da        = Model.DetailedAddresses[ CurrentAddress.ToLower() ];
								                   accountId = Model.SelectedAccount;
							                   }
							                   else if( Model.GlobalDetailedAddresses.ContainsKey( CurrentAddress.ToLower() ) )
							                   {
								                   Da        = Model.GlobalDetailedAddresses[ CurrentAddress.ToLower() ];
								                   accountId = Model.GlobalAddressBookId;
							                   }

							                   if( Da != null )
							                   {
								                   Company? latest = await Model.GetLatestCompany( Tmp.CompanyName, accountId );

								                   if( latest != null )
								                   {
									                   Model.DeliverySuite        = latest.Suite;
									                   Model.DeliveryStreet       = latest.AddressLine1;
									                   Model.DeliveryCity         = latest.City;
									                   Model.DeliveryLocation     = latest.LocationBarcode;
									                   Model.DeliveryAddressNotes = latest.Notes;
									                   Model.DeliveryCountry      = latest.Country;

									                   // Problem - addresses are still appearing with abbreviations for the region
									                   if( CountriesRegions.Countries.Contains( latest.Country ) )
									                   {
										                   var (ProvState, Found) = CountriesRegions.FindRegionForCountry( latest.Region, latest.Country );

										                   if( Found )
											                   Model.DeliveryProvState = ProvState;
										                   else
										                   {
											                   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( latest.Region, latest.Country );

											                   Model.DeliveryProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : latest.Region;
										                   }
									                   }
									                   else
									                   {
										                   // Can't find a match - use what is there
										                   Model.DeliveryProvState = latest.Region;
									                   }

									                   //Model.DeliveryProvState    = latest.Region;
									                   Model.DeliveryPostalZip = latest.PostalCode;
									                   Model.DeliveryName      = latest.UserName;
									                   Model.DeliveryPhone     = latest.Phone;

									                   Model.WhenDeliveryCountryChanges();

									                   var (Nme, _)
										                   //Model.DeliveryProvState ??= latest.Region;
										                   = CountriesRegions.FindRegionForCountry( latest.Region, Model.PickupCountry );

									                   Model.DeliveryProvState = Nme;

									                   if( Da != null )
									                   {
										                   //Model.DeliveryProvState = latest.Region;
										                   var (ProvState, _)      = CountriesRegions.FindRegionForCountry( latest.Region, Model.PickupCountry );
										                   Model.DeliveryProvState = ProvState;
									                   }
								                   }
								                   else
								                   {
									                   Model.DeliverySuite        = Da.Address.Suite;
									                   Model.DeliveryStreet       = Da.Address.AddressLine1;
									                   Model.DeliveryCity         = Da.Address.City;
									                   Model.DeliveryLocation     = Da.Address.LocationBarcode;
									                   Model.DeliveryAddressNotes = Da.Address.Notes;
									                   Model.DeliveryCountry      = Da.Address.Country;

									                   // Problem - addresses are still appearing with abbreviations for the region
									                   if( CountriesRegions.Countries.Contains( Da.Address.Country ) )
									                   {
										                   var (ProvState, Found) = CountriesRegions.FindRegionForCountry( Da.Address.Region, Da.Address.Country );

										                   if( Found )
											                   Model.DeliveryProvState = ProvState;
										                   else
										                   {
											                   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( Da.Address.Region, Da.Address.Country );

											                   Model.DeliveryProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : Da.Address.Region;
										                   }
									                   }
									                   else
									                   {
										                   // Can't find a match - use what is there
										                   Model.DeliveryProvState = Da.Address.Region;
									                   }

									                   //Model.DeliveryProvState    = da.Address.Region;
									                   Model.DeliveryPostalZip = Da.Address.PostalCode;
									                   Model.DeliveryName      = Da.Company.UserName;
									                   Model.DeliveryPhone     = Da.Company.Phone;

									                   Model.WhenDeliveryCountryChanges();

									                   var (Nme, _)
										                   //Model.DeliveryProvState ??= da.Address.Region;
										                   = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );

									                   Model.DeliveryProvState = Nme;

									                   //Model.DeliveryProvState = da.Address.Region;
									                   var (PState, _)         = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
									                   Model.DeliveryProvState = PState;
								                   }

								                   //// Problem - addresses are still appearing with abbreviations for the region
								                   //if( CountriesRegions.Countries.Contains( Da.Address.Country ) )
								                   //{
								                   // var (ProvState, Found) = CountriesRegions.FindRegionForCountry( Da.Address.Region, Da.Address.Country );

								                   // if( Found )
								                   //  Model.DeliveryProvState = ProvState;
								                   // else
								                   // {
								                   //  ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( Da.Address.Region, Da.Address.Country );

								                   //  Model.DeliveryProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : Da.Address.Region;
								                   // }
								                   //}
								                   //else
								                   //{
								                   // // Can't find a match - use what is there
								                   // Model.DeliveryProvState = Da.Address.Region;
								                   //}

								                   ////Model.DeliveryProvState    = da.Address.Region;
								                   //Model.DeliveryPostalZip = Da.Address.PostalCode;
								                   //Model.DeliveryName      = Da.Company.UserName;
								                   //Model.DeliveryPhone     = Da.Company.Phone;
							                   }
							                   else
							                   {
								                   Model.WhenDeliveryCountryChanges();
								                   string Nme;

								                   //Model.DeliveryProvState ??= da.Address.Region;
								                   if( Da is not null )
									                   ( Nme, _ ) = CountriesRegions.FindRegionForCountry( Tmp.Region, Model.PickupCountry );
								                   else
									                   Nme = "";

								                   Model.DeliveryProvState = Nme;
							                   }

							                   //Task.WaitAll( Task.Run( () =>
							                   //                        {
							                   //                         Model.WhenDeliveryCountryChanges();
							                   //                         string Nme;

							                   //                         //Model.DeliveryProvState ??= da.Address.Region;
							                   //                         if( Da is not null )
							                   //                          ( Nme, _ ) = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
							                   //                         else
							                   //                          Nme = "";

							                   //                         Model.DeliveryProvState = Nme;
							                   //                        } ) );

							                   //if( Da != null )
							                   //{
							                   // //Model.DeliveryProvState = da.Address.Region;
							                   // var (ProvState, _)      = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
							                   // Model.DeliveryProvState = ProvState;
							                   //}
						                   } );

						break;
					}
				}

				if( !found )
				{
					Logging.WriteLogLine( "Can't find an address for " + CurrentAddress );
				}
				Model.AreDeliveryAddressFieldsReadOnly = found;
			}
			else
			{
				Model.AreDeliveryAddressFieldsReadOnly = false;
				// Empty the fields
				Model.DeliverySuite        = string.Empty;
				Model.DeliveryStreet       = string.Empty;
				Model.DeliveryCity         = string.Empty;
				Model.DeliveryAddressNotes = string.Empty;
				//Model.DeliveryCountry = string.Empty;
				Model.DeliveryPostalZip               = string.Empty;
				Model.DeliveryName                    = string.Empty;
				Model.DeliveryPhone                   = string.Empty;
				Model.DeliveryLocationBarcode         = string.Empty;
				Model.DeliveryZone                    = string.Empty;
				tbDeliveryAddressLocationBarcode.Text = string.Empty;
			}*/
		}
	}

	public void LoadSelectedDeliveryAddress()
	{
		if (DataContext is TripEntryModel Model)
		{
			var CurrentAddress = Model.SelectedDeliveryAddress;

			if (CurrentAddress.IsNotNullOrWhiteSpace())
			{
				var found = false;

				foreach (var Tmp in Model.Addresses)
				{
					if (string.Compare(Tmp.CompanyName, CurrentAddress, StringComparison.CurrentCultureIgnoreCase) == 0)
					{
						Dispatcher.Invoke(async () =>
										   {
											   //Logging.WriteLogLine( "Loading address: " + CurrentAddress );
											   found = true;

											   Model.DeliveryCompany = Tmp.CompanyName;
											   Model.DeliverySuite = Tmp.Suite;
											   Model.DeliveryStreet = Tmp.AddressLine1;
											   Model.DeliveryCity = Tmp.City;
											   Model.DeliveryLocation = Tmp.LocationBarcode;

											   CompanyDetail? Da = null;

											   var accountId = string.Empty;

											   if (Model.DetailedAddresses.ContainsKey(CurrentAddress.ToLower()))
											   {
												   Da = Model.DetailedAddresses[CurrentAddress.ToLower()];
												   accountId = Model.SelectedAccount;
											   }
											   else if (Model.GlobalDetailedAddresses.ContainsKey(CurrentAddress.ToLower()))
											   {
												   Da = Model.GlobalDetailedAddresses[CurrentAddress.ToLower()];
												   accountId = Model.GlobalAddressBookId;
											   }

											   if (Da != null)
											   {
												   var latest = await Model.GetLatestCompany(Tmp.CompanyName, accountId);

												   if (latest != null)
												   {
													   Model.DeliverySuite = latest.Suite;
													   Model.DeliveryStreet = latest.AddressLine1;
													   Model.DeliveryCity = latest.City;
													   Model.DeliveryLocation = latest.LocationBarcode;
													   Model.DeliveryAddressNotes = latest.Notes;
													   Model.DeliveryCountry = latest.Country;

													   // Problem - addresses are still appearing with abbreviations for the region
													   if (CountriesRegions.Countries.Contains(latest.Country))
													   {
														   var (ProvState, Found) = CountriesRegions.FindRegionForCountry(latest.Region, latest.Country);

														   if (Found)
															   Model.DeliveryProvState = ProvState;
														   else
														   {
															   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry(latest.Region, latest.Country);

															   Model.DeliveryProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : latest.Region;
														   }
													   }
													   else
													   {
														   // Can't find a match - use what is there
														   Model.DeliveryProvState = latest.Region;
													   }

													   //Model.DeliveryProvState    = latest.Region;
													   Model.DeliveryPostalZip = latest.PostalCode;
													   Model.DeliveryName = latest.UserName;
													   Model.DeliveryPhone = latest.Phone;

													   Model.WhenDeliveryCountryChanges();

													   var (Nme, _)
														   //Model.DeliveryProvState ??= latest.Region;
														   = CountriesRegions.FindRegionForCountry(latest.Region, Model.PickupCountry);

													   Model.DeliveryProvState = Nme;

													   if (Da != null)
													   {
														   //Model.DeliveryProvState = latest.Region;
														   var (ProvState, _) = CountriesRegions.FindRegionForCountry(latest.Region, Model.PickupCountry);
														   Model.DeliveryProvState = ProvState;
													   }
												   }
												   else
												   {
													   Model.DeliverySuite = Da.Address.Suite;
													   Model.DeliveryStreet = Da.Address.AddressLine1;
													   Model.DeliveryCity = Da.Address.City;
													   Model.DeliveryLocation = Da.Address.LocationBarcode;
													   Model.DeliveryAddressNotes = Da.Address.Notes;
													   Model.DeliveryCountry = Da.Address.Country;

													   // Problem - addresses are still appearing with abbreviations for the region
													   if (CountriesRegions.Countries.Contains(Da.Address.Country))
													   {
														   var (ProvState, Found) = CountriesRegions.FindRegionForCountry(Da.Address.Region, Da.Address.Country);

														   if (Found)
															   Model.DeliveryProvState = ProvState;
														   else
														   {
															   ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry(Da.Address.Region, Da.Address.Country);

															   Model.DeliveryProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : Da.Address.Region;
														   }
													   }
													   else
													   {
														   // Can't find a match - use what is there
														   Model.DeliveryProvState = Da.Address.Region;
													   }

													   //Model.DeliveryProvState    = da.Address.Region;
													   Model.DeliveryPostalZip = Da.Address.PostalCode;
													   Model.DeliveryName = Da.Company.UserName;
													   Model.DeliveryPhone = Da.Company.Phone;

													   Model.WhenDeliveryCountryChanges();

													   var (Nme, _)
														   //Model.DeliveryProvState ??= da.Address.Region;
														   = CountriesRegions.FindRegionForCountry(Da.Address.Region, Model.PickupCountry);

													   Model.DeliveryProvState = Nme;

													   //Model.DeliveryProvState = da.Address.Region;
													   var (PState, _) = CountriesRegions.FindRegionForCountry(Da.Address.Region, Model.PickupCountry);
													   Model.DeliveryProvState = PState;
												   }

												   //// Problem - addresses are still appearing with abbreviations for the region
												   //if( CountriesRegions.Countries.Contains( Da.Address.Country ) )
												   //{
												   // var (ProvState, Found) = CountriesRegions.FindRegionForCountry( Da.Address.Region, Da.Address.Country );

												   // if( Found )
												   //  Model.DeliveryProvState = ProvState;
												   // else
												   // {
												   //  ProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( Da.Address.Region, Da.Address.Country );

												   //  Model.DeliveryProvState = ProvState.IsNotNullOrWhiteSpace() ? ProvState : Da.Address.Region;
												   // }
												   //}
												   //else
												   //{
												   // // Can't find a match - use what is there
												   // Model.DeliveryProvState = Da.Address.Region;
												   //}

												   ////Model.DeliveryProvState    = da.Address.Region;
												   //Model.DeliveryPostalZip = Da.Address.PostalCode;
												   //Model.DeliveryName      = Da.Company.UserName;
												   //Model.DeliveryPhone     = Da.Company.Phone;
											   }
											   else
											   {
												   Model.WhenDeliveryCountryChanges();
												   string Nme;

												   //Model.DeliveryProvState ??= da.Address.Region;
												   if (Da is not null)
													   (Nme, _) = CountriesRegions.FindRegionForCountry(Tmp.Region, Model.PickupCountry);
												   else
													   Nme = "";

												   Model.DeliveryProvState = Nme;
											   }

											   //Task.WaitAll( Task.Run( () =>
											   //                        {
											   //                         Model.WhenDeliveryCountryChanges();
											   //                         string Nme;

											   //                         //Model.DeliveryProvState ??= da.Address.Region;
											   //                         if( Da is not null )
											   //                          ( Nme, _ ) = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
											   //                         else
											   //                          Nme = "";

											   //                         Model.DeliveryProvState = Nme;
											   //                        } ) );

											   //if( Da != null )
											   //{
											   // //Model.DeliveryProvState = da.Address.Region;
											   // var (ProvState, _)      = CountriesRegions.FindRegionForCountry( Da.Address.Region, Model.PickupCountry );
											   // Model.DeliveryProvState = ProvState;
											   //}
										   });

						break;
					}
				}

				if (!found)
					Logging.WriteLogLine("Can't find an address for " + CurrentAddress);
				Model.AreDeliveryAddressFieldsReadOnly = found;
			}
			else
			{
				Model.AreDeliveryAddressFieldsReadOnly = false;
				// Empty the fields
				Model.DeliverySuite = string.Empty;
				Model.DeliveryStreet = string.Empty;
				Model.DeliveryCity = string.Empty;
				Model.DeliveryAddressNotes = string.Empty;
				//Model.DeliveryCountry = string.Empty;
				Model.DeliveryPostalZip = string.Empty;
				Model.DeliveryName = string.Empty;
				Model.DeliveryPhone = string.Empty;
				Model.DeliveryLocationBarcode = string.Empty;
				Model.DeliveryZone = string.Empty;
				tbDeliveryAddressLocationBarcode.Text = string.Empty;
			}
		}
	}

	public void BtnCalculate_Click(object? sender, RoutedEventArgs? e)
	{
		if (DataContext is TripEntryModel Model)
		{
			Dictionary<string, string> activeCharges = new();
			//Dictionary<string, object> controlsByName = new();

			foreach (var panel in GridCharges.Children)
			{
				if (panel is StackPanel sp)
				{
					foreach (var control in sp.Children)
					{
						switch (control)
						{
							case CheckBox { IsChecked: true } cb:
								{
									//controlsByName.Add(cb.Name, cb);
									var (chargeId, _) = GetChargeIdFromName(cb.Name);

									if (chargeId.IsNotNullOrWhiteSpace())
									{
										//Logging.WriteLogLine("DEBUG Found checked charge: " + chargeId);
										if (!activeCharges.ContainsKey(chargeId))
											activeCharges.Add(chargeId, string.Empty);
									}
									break;
								}

							case TextBox { IsEnabled: true } tb:
								{
									//controlsByName.Add(tb.Name, tb);
									var (chargeId, _) = GetChargeIdFromName(tb.Name);

									if (chargeId.IsNotNullOrWhiteSpace())
									{
										//Logging.WriteLogLine("DEBUG Found enabled charge: " + chargeId + " text: " + tb.Text.Trim());
										if (!activeCharges.ContainsKey(chargeId))
											activeCharges.Add(chargeId, string.Empty);
										activeCharges[chargeId] = tb.Text.Trim();
									}
									break;
								}

							case Label lbl when lbl.Name.IsNotNullOrWhiteSpace():
								{
									// Only the labels that display values are named								
									var (chargeId, _) = GetChargeIdFromName(lbl.Name);

									if (chargeId.IsNotNullOrWhiteSpace())
									{
										//Logging.WriteLogLine("DEBUG Found Total label for charge: " + chargeId);
										if (!activeCharges.ContainsKey(chargeId))
											activeCharges.Add(chargeId, string.Empty);
									}
									break;
								}
						}
					}
				}
			}

			//Model.CalculateCharges(activeCharges);
			activeCharges = Model.CheckForDefaultCharges(activeCharges);
			activeCharges = Model.CheckForOverrides(activeCharges);
			PriceTripHelper.PriceTrip(Model, activeCharges);

			foreach (var tc in Model.TripCharges)
			{
				// Update the Lbl_<chargeId>_Result labels
				var name = "Lbl" + ChargeControlNameDivider + tc.ChargeId + ChargeControlNameDivider + "Result";
				//Logging.WriteLogLine( "DEBUG Looking for Label: " + name );

				foreach (var obj in GridCharges.Children)
				{
					if (obj is StackPanel sp)
					{
						var lbl = (Label)(from object O in sp.Children
										  where O is Label l && (l.Name == name)
										  select O).FirstOrDefault();

						if (lbl != null)
						{
							lbl.Content = tc.Value.ToString("C");
							break;
						}
						//foreach (object? control in sp.Children)
						//{
						//    if (control is Label lbl && lbl.Name == name)
						//    {
						//        lbl.Content = tc.Value.ToString("C");
						//        break;
						//    }
						//    //else if (control is TextBox tb && tb.Name.Contains(ChargeControlNameDivider + tc.ChargeId))
						//    //                        {
						//    //	// Need to modify the value if the result violates the charge's min/max settings
						//    //	tb.Text = tc.Value.ToString("F2");
						//    //}
						//}
					}
				}
			}
		}

		RecalculateTripItemTotals();
	}


	public void RecalculateTripItemTotals(bool suppressRate = false)
	{
		//Logging.WriteLogLine( "DEBUG Recalculating totals" );

		decimal Weight = 0;
		var Pieces = 0;
		decimal Original = 0;
		decimal Rate = 0;

		foreach (var Control in gridTripItems.Children)
		{
			switch (Control)
			{
				case StackPanel Sp:
					foreach (var Child in Sp.Children)
					{
						switch (Child)
						{
							case TextBox Tb:
								{
									if (!Tb.Name.Contains("Row_Totals"))
									{
										// Weight, Pieces, and Original
										if (Tb.Name.ToLower().Contains("weight"))
										{
											//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
											if (Tb.Text.IsNotNullOrWhiteSpace())
											{
												var Tmp = decimal.Parse(Tb.Text.Trim());
												Weight += Tmp;
											}
										}
										else if (Tb.Name.ToLower().Contains("pieces"))
										{
											//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
											if (Tb.Text.IsNotNullOrWhiteSpace())
											{
												var Tmp = decimal.ToInt32(decimal.Parse(Tb.Text.Trim()));
												Pieces += Tmp;
											}
										}
										else if (Tb.Name.ToLower().Contains("original"))
										{
											//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
											if (Tb.Text.IsNotNullOrWhiteSpace())
											{
												var Tmp = decimal.Parse(Tb.Text.Trim());
												Original += Tmp;
											}
										}
										else if (Tb.Name.ToLower().Contains("rate"))
										{
											decimal Tmp = 0;

											if (Tb.Text.IsNotNullOrWhiteSpace() && !suppressRate)
											{
												// PROBLEM Tb.Text is "$5.00" - Assuming all currency is preceded by 1 character
												var IsDigit = int.TryParse(Tb.Text.Substring(0, 1), out var _);
												var Raw = Tb.Text;

												if (!IsDigit)
													Raw = Tb.Text.Substring(1);
												decimal.TryParse(Raw, out Tmp);
												Rate += Tmp;
											}
											else
											{
												Rate = 0;
												Tb.Text = Rate.ToString("C");
											}
										}
									}
									break;
								}

							case IntegerUpDown Iud:
								{
									if (Iud.Name.ToLower().Contains("pieces"))
									{
										if (Iud.Value != null)
											Pieces += (int)Iud.Value;
									}
									break;
								}

							case DecimalUpDown Dud:
								{
									if (Dud.Name.ToLower().Contains("weight"))
									{
										if (Dud.Value != null)
											Weight += (decimal)Dud.Value;
									}
									break;
								}
						}
					}

					break;

				case TextBox Tb:
					{
						if (!Tb.Name.Contains("Row_Totals"))
						{
							// Weight, Pieces, and Original
							if (Tb.Name.ToLower().Contains("weight"))
							{
								//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									var Tmp = decimal.Parse(Tb.Text.Trim());
									Weight += Tmp;
								}
							}
							else if (Tb.Name.ToLower().Contains("pieces"))
							{
								//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									var Tmp = decimal.ToInt32(decimal.Parse(Tb.Text.Trim()));
									Pieces += Tmp;
								}
							}
							else if (Tb.Name.ToLower().Contains("original"))
							{
								//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									var Tmp = decimal.Parse(Tb.Text.Trim());
									Original += Tmp;
								}
							}
							else if (Tb.Name.ToLower().Contains("rate"))
							{
								if (Tb.Text.IsNotNullOrWhiteSpace() && !suppressRate)
								{
									// PROBLEM Tb.Text is "$5.00" - Assuming all currency is preceded by 1 character
									var IsDigit = int.TryParse(Tb.Text.Substring(0, 1), out var _);
									var Raw = Tb.Text;

									if (!IsDigit)
										Raw = Tb.Text.Substring(1);
									decimal.TryParse(Raw, out var Tmp);
									Rate += Tmp;
								}
								else
								{
                                    decimal tmp = 0;
                                    Tb.Text = tmp.ToString("C");
                                    Rate = 0;
                                }
							}
						}
						break;
					}

				case IntegerUpDown Iud:
					{
						if (Iud.Name.ToLower().Contains("pieces"))
						{
							if (Iud.Value != null)
								Pieces += (int)Iud.Value;
						}
						break;
					}

				case DecimalUpDown Dud:
					{
						if (Dud.Name.ToLower().Contains("weight"))
						{
							if (Dud.Value != null)
								Weight += (decimal)Dud.Value;
						}
						break;
					}
			}
		}

		if (DataContext is TripEntryModel Model)
		{
			Model.TotalWeight = Weight;
			Model.TotalPieces = Pieces;
			Model.TotalOriginal = Original;

			Dispatcher.Invoke(() =>
							   {
								   // Kludge the bound fields aren't showing up
								   foreach (var Control in gridTripItems.Children)
								   {
									   if (Control is TextBox Tb)
									   {
										   switch (Tb.Name)
										   {
											   case "Row_Totals_TotalWeight":
												   Tb.Text = Weight + "";
												   break;

											   case "Row_Totals_TotalPieces":
												   Tb.Text = Pieces + "";
												   break;

											   case "Row_Totals_TotalOriginal":
												   Tb.Text = Original + "";
												   break;

											   case "Row_Totals_TotalRate":

												   Tb.Text = Rate.ToString("C");

												   // Update the field in the Total section
												   //LblTotalDeliveryCharges.Content = Tb.Text;

												   //Model.TotalDeliveryCharges = Model.TotalBeforeTax = Model.TotalTotal = Rate;
												   Model.TotalDeliveryCharges = Model.TotalPayroll = Rate;
												   Model.TotalBeforeTax = Rate + Model.TotalCharges;
												   Model.TotalTotal = Model.TotalBeforeTax + Model.TotalTaxes;
												   //Logging.WriteLogLine("DEBUG Model.TotalTotal: " + Model.TotalTotal);
												   break;
										   }
									   }
								   }
							   });
		}
	}


	public void RecalculateTripItemTotals_V1()
	{
		//Logging.WriteLogLine( "Recalculating totals" );

		decimal Weight = 0;
		var Pieces = 0;
		decimal Original = 0;
		decimal Rate = 0;

		foreach (var Control in gridTripItems.Children)
		{
			switch (Control)
			{
				case TextBox Tb:
					{
						if (!Tb.Name.Contains("Row_Totals"))
						{
							// Weight, Pieces, and Original
							if (Tb.Name.ToLower().Contains("weight"))
							{
								//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									var Tmp = decimal.Parse(Tb.Text.Trim());
									Weight += Tmp;
								}
							}
							else if (Tb.Name.ToLower().Contains("pieces"))
							{
								//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									var Tmp = decimal.ToInt32(decimal.Parse(Tb.Text.Trim()));
									Pieces += Tmp;
								}
							}
							else if (Tb.Name.ToLower().Contains("original"))
							{
								//Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									var Tmp = decimal.Parse(Tb.Text.Trim());
									Original += Tmp;
								}
							}
							else if (Tb.Name.ToLower().Contains("rate"))
							{
								if (Tb.Text.IsNotNullOrWhiteSpace())
								{
									// PROBLEM Tb.Text is "$5.00" - Assuming all currency is preceded by 1 character
									var IsDigit = int.TryParse(Tb.Text.Substring(0, 1), out var _);
									var Raw = Tb.Text;

									if (!IsDigit)
										Raw = Tb.Text.Substring(1);
									decimal.TryParse(Raw, out var Tmp);
									Rate += Tmp;
								}
							}
						}
						break;
					}

				case IntegerUpDown Iud:
					{
						if (Iud.Name.ToLower().Contains("pieces"))
						{
							if (Iud.Value != null)
								Pieces += (int)Iud.Value;
						}
						break;
					}

				case DecimalUpDown Dud:
					{
						if (Dud.Name.ToLower().Contains("weight"))
						{
							if (Dud.Value != null)
								Weight += (decimal)Dud.Value;
						}
						break;
					}
			}
		}

		if (DataContext is TripEntryModel Model)
		{
			Model.TotalWeight = Weight;
			Model.TotalPieces = Pieces;
			Model.TotalOriginal = Original;

			Dispatcher.Invoke(() =>
							   {
								   // Kludge the bound fields aren't showing up
								   foreach (var Control in gridTripItems.Children)
								   {
									   if (Control is TextBox Tb)
									   {
										   switch (Tb.Name)
										   {
											   case "Row_Totals_TotalWeight":
												   Tb.Text = Weight + "";
												   break;

											   case "Row_Totals_TotalPieces":
												   Tb.Text = Pieces + "";
												   break;

											   case "Row_Totals_TotalOriginal":
												   Tb.Text = Original + "";
												   break;

											   case "Row_Totals_TotalRate":
												   Tb.Text = Rate.ToString("C");

												   // Update the field in the Total section
												   //LblTotalDeliveryCharges.Content = Tb.Text;

												   Model.TotalDeliveryCharges = Model.TotalBeforeTax = Model.TotalTotal = Rate;
												   Logging.WriteLogLine("DEBUG Model.TotalTotal: " + Model.TotalTotal);
												   break;
										   }
									   }
								   }
							   });
		}
	}

	public void Clear()
	{
		// KLUDGE
		//DateTime now = DateTime.Now;
		//dtpCallDate.Value = now;
		//tpCallTime.Value = now;
		//dtpReadyTimeDate.Value = now;
		//dtpReadyTimeTime.Value = now;
		//dtpDueTimeDate.Value = now;
		//dtpDueTimeTime.Value = now;

		if (DataContext is TripEntryModel Model)
		{
			Model.Execute_ClearTripEntry();
			ClearErrorConditions();

			cbAccount.SelectedIndex = -1;
			cbAccount.Text = string.Empty;
			cbCustomer.SelectedIndex = -1;
			cbCustomer.Text = string.Empty;

			cbPickupAddresses.SelectedIndex = -1;

			cbPickupAddressProv.SelectedIndex = -1;
			cbPickupAddressCountry.SelectedIndex = -1;
			cbDeliveryAddresses.SelectedIndex = -1;

			cbDeliveryAddressProv.SelectedIndex = -1;
			cbDeliveryAddressCountry.SelectedIndex = -1;

			//cbPackageTypes.SelectedIndex = 0;
			//row_0_PackageType.SelectedIndex = 0;
			//cbServiceLevel.SelectedIndex = -1;
			//cbServiceLevel.SelectedIndex = 0;
			// Will be either the first service level or the default
			cbServiceLevel.SelectedItem = Model.GetServiceDefault();

			cbDrivers.SelectedIndex = -1;

			//List<TripPackage> list = new List<TripPackage>() { 
			//List< TripPackage> list = new List<TripPackage>() { 
			//    new TripPackage() 
			//    {
			//        Pieces = 1,
			//        Weight = 1
			//    }
			//};
			var List = new ObservableCollection<DisplayPackage>
					   {
						   new( new TripPackage
								{
									Pieces = Model.GetPiecesDefault(), // 1,
							        Weight = Model.GetWeightDefault()  //1
						        } )
					   };

			Model.TripPackages = List;
			BuildPackageRows(Model);

			imagePOP.Source = null;
			imagePOD.Source = null;

			//BuildNewPackageTotalRow(Model);
		}

		// KLUDGE
		var Now = DateTime.Now;

		if (DataContext is TripEntryModel Model1)
		{
			Model1.CallDate = Now;

			//Model1.CallTime = now;

			dtpCallDate.Value = Now;
			tpCallTime.Value = Now;
			dtpReadyTimeDate.Value = Model1.GetReadyTimeDefault();
			dtpReadyTimeTime.Value = Model1.GetReadyTimeDefault();
			dtpDueTimeDate.Value = Model1.GetDueByTimeDefault();
			dtpDueTimeTime.Value = Model1.GetDueByTimeDefault();
		}

		//EnsureDateTimesArentBeginningOfTime();
		CbPickupAddressDefault.IsChecked = false;
		CbDeliveryAddressDefault.IsChecked = false;
		CbServiceLevelDefault.IsChecked = false;
		CbReadyTimeDefault.IsChecked = false;
		CbDueByTimeDefault.IsChecked = false;

		spFindTrip.Visibility = Visibility.Collapsed;

		ClearChargesAndTotals();

		DoJump(SECTIONS.BILLING, false);
	}

	public void RemovePackageRows()
	{
		var RemoveControls = new List<Control>();
		var RemoveImages = new List<Image>();
		var RemoveStackPanels = new List<StackPanel>();

		foreach (var Tmp in gridTripItems.Children)
		{
			switch (Tmp)
			{
				// These hold the default check boxes and control
				case StackPanel stackPanel:
					if (stackPanel.Name.StartsWith("SpDefault"))
					{
						var RemoveStackPanelChildren = new List<Control>();

						foreach (var child in stackPanel.Children)
						{
							if (child is Control control)
								RemoveStackPanelChildren.Add(control);
						}

						foreach (var control1 in RemoveStackPanelChildren)
							stackPanel.Children.Remove(control1);
					}
					RemoveStackPanels.Add(stackPanel);
					break;

				case Control Control:
					{
						if (Control.Name.IndexOf("Row_", StringComparison.Ordinal) > -1)
							RemoveControls.Add(Control);
						break;
					}

				case Image Image:
					{
						if ((Image.Name.IndexOf("Row_", StringComparison.Ordinal) > -1) && (Image.Name.IndexOf("Row_0", StringComparison.Ordinal) == -1))
							RemoveImages.Add(Image);
						break;
					}
			}
		}

		foreach (var C in RemoveControls)
			gridTripItems.Children.Remove(C);

		foreach (var I in RemoveImages)
			gridTripItems.Children.Remove(I);

		foreach (var C in RemoveStackPanels)
			gridTripItems.Children.Remove(C);

		if (gridTripItems.RowDefinitions.Count > 1)
		{
			gridTripItems.RowDefinitions.RemoveRange(1, gridTripItems.RowDefinitions.Count - 1);

			//gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 3 );
		}
	}

	/// <summary>
	///     Creates rows based upon Model.TripItems.
	/// </summary>
	public void BuildPackageRows(TripEntryModel model)
	{
		Logging.WriteLogLine("DEBUG model.TripPackages.Count: " + model.TripPackages.Count);
		RemovePackageRows();

		//if (model.TripPackages.Count == 0)
		//{
		//	Logging.WriteLogLine("DEBUG model.TripPackages is empty - adding 1");
		//	model.TripPackages.Add(new DisplayPackage(new TripPackage
		//	{
		//		Pieces = 1,
		//		Weight = 1
		//	}));
		//}

		// First, empty the collection
		/*		var RemoveControls    = new List<Control>();
				var RemoveImages      = new List<Image>();
				var RemoveStackPanels = new List<StackPanel>();

				foreach( var Tmp in gridTripItems.Children )
				{
					switch( Tmp )
					{
					// These hold the default check boxes and control
					case StackPanel stackPanel:
						if( stackPanel.Name.StartsWith( "SpDefault" ) )
						{
							var RemoveStackPanelChildren = new List<Control>();

							foreach( var child in stackPanel.Children )
							{
								if( child is Control control )
									RemoveStackPanelChildren.Add( control );
							}

							foreach( var control1 in RemoveStackPanelChildren )
								stackPanel.Children.Remove( control1 );
						}
						RemoveStackPanels.Add( stackPanel );
						break;

					case Control Control:
						{
							if( Control.Name.IndexOf( "Row_", StringComparison.Ordinal ) > -1 )
								RemoveControls.Add( Control );
							break;
						}

					case Image Image:
						{
							if( ( Image.Name.IndexOf( "Row_", StringComparison.Ordinal ) > -1 ) && ( Image.Name.IndexOf( "Row_0", StringComparison.Ordinal ) == -1 ) )
								RemoveImages.Add( Image );
							break;
						}
					}
				}

				foreach( var C in RemoveControls )
					gridTripItems.Children.Remove( C );

				foreach( var I in RemoveImages )
					gridTripItems.Children.Remove( I );

				foreach( var C in RemoveStackPanels )
					gridTripItems.Children.Remove( C );

				if( gridTripItems.RowDefinitions.Count > 1 )
				{
					gridTripItems.RowDefinitions.RemoveRange( 1, gridTripItems.RowDefinitions.Count - 1 );

					//gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 3 );
				}
		*/

		//if( gridTripItems.RowDefinitions.Count > 2 )
		//         {
		//	gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 2 );
		//	//gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 3 );
		//         }

		//if (gridTripItems.RowDefinitions.Count > 2)
		//if (Model.TripPackages.Count > 0)
		//if (Model.TripPackages.Count > 1)
		//{
		//    Row_0_AddRow.Visibility = Visibility.Hidden;
		//}
		//else
		//{
		//    Row_0_AddRow.Visibility = Visibility.Visible;
		//}

		//foreach( TripPackage tp in Model.TripPackages )
		//for (int i = 1; i < Model.TripPackages.Count; i++) // Skip over the first line - attached to the first row
		//var counter = 0;

		//foreach( var Tp in model.TripPackages )
		//{
		//	// Only the first row shows the default checkboxes
		//	if( counter++ == 0 )
		//		BuildNewPackageTypeRow( model, Tp, true );
		//	else
		//		BuildNewPackageTypeRow( model, Tp );
		//}

		// Add the Rows first, then build the contents
		for (var i = 0; i < model.TripPackages.Count; i++)
		{
			var Rd = new RowDefinition
			{
				Height = gridTripItems.RowDefinitions[0].Height
			};
			gridTripItems.RowDefinitions.Add(Rd);
		}

		for (var i = 0; i < model.TripPackages.Count; i++)
		{
			if (i == 0)
				BuildNewPackageTypeRow(model, i, model.TripPackages[i], true);
			else
				BuildNewPackageTypeRow(model, i, model.TripPackages[i]);
		}

		// Turn off all Add buttons except for the last TripItem 
		//for (int i = 1; i < gridTripItems.RowDefinitions.Count - 1; i++)
		for (var I = 1; I < gridTripItems.RowDefinitions.Count; I++)
		{
			// Turn off the first one if there are no other rows
			if ((I == 1) && (gridTripItems.RowDefinitions.Count == 2))
			{
				var Row = "Row_" + I + "_DeleteRow";

				foreach (var Tmp in gridTripItems.Children)
				{
					if (Tmp is Image Image)
					{
						if (Image.Name == Row)
						{
							Image.Visibility = Visibility.Hidden;
							break;
						}
					}
				}
			}

			// Turn off all but the last one
			else if (I < (gridTripItems.RowDefinitions.Count - 1))
			{
				var Row = "Row_" + I + "_AddRow";

				foreach (var Tmp in gridTripItems.Children)
				{
					if (Tmp is Image Image)
					{
						if (Image.Name == Row)
						{
							Image.Visibility = Visibility.Hidden;
							break;
						}
					}
				}
			}
		}

		// Finally
		//RecalculateTripItemTotals();
		BuildNewPackageTotalRow();
	}

	public void OnPackageTypeChange(object sender, RoutedEventArgs e)
	{
		if (sender is ComboBox cb)
		{
			var row = string.Empty;
			var pieces = cb.Name.Split('_');

			if (pieces.Length == 3)
				row = pieces[1];

			if (row == "1")
			{
				if (DataContext is TripEntryModel model)
					model.PackageType = (string)cb.SelectedItem;
			}

			Logging.WriteLogLine("DEBUG PackageType changed: SelectedIndex: " + cb.SelectedIndex + " in row " + row);
			RecalculateRates(int.Parse(row));
		}
	}

	public void OnWeightChange(object sender, RoutedEventArgs e)
	{
		if (sender is DecimalUpDown dud)
		{
			var row = string.Empty;
			var pieces = dud.Name.Split('_');

			if (pieces.Length == 3)
				row = pieces[1];
			//Logging.WriteLogLine("DEBUG Weight changed: " + dud.Value + " in row " + row);
			RecalculateRates(int.Parse(row));
		}
	}

	public void OnPiecesChange(object sender, RoutedEventArgs e)
	{
		if (sender is IntegerUpDown iud)
		{
			var row = string.Empty;
			var pieces = iud.Name.Split('_');

			if (pieces.Length == 3)
				row = pieces[1];

			//Logging.WriteLogLine("DEBUG Pieces changed: " + iud.Value + " in row " + row);
			RecalculateRates(int.Parse(row));
		}
	}

	public void RecalculateAllRates()
	{
		if (DataContext is TripEntryModel model)
		{
			for (int i = 0; i < model.TripPackages.Count; i++)
			{
				RecalculateRates(i + 1);
			}
		}
	}

	public void RecalculateRates(int row)
	{
		if (DataContext is TripEntryModel Model)
		{
			foreach (Charge c in Model.SurchargeCharges)
			{
				if (!Model.DictRowSurcharge.Values.Contains(c))
				{
					Label lbl = (from L in SurchargeLabels where L.Name.Contains(c.ChargeId) select L).FirstOrDefault();
					if (lbl != null)
					{
						lbl.Visibility = Visibility.Collapsed;
					}
				}
			}
			if (Model.TripPackages.Count >= row)
			{
				decimal rate = 0;
				decimal quotesValue = GetValueOfQuotesField();
				if ((!Model.DoesPackageTypeQuotesOverrideTripMatrix) || (Model.DoesPackageTypeQuotesOverrideTripMatrix && quotesValue == 0))
				{
					if (Model.DoesPackageTypeQuotesOverrideTripMatrix && quotesValue > 0)
					{
						rate = 0;
					}
					else
					{
						rate = PriceTripHelper.GetRateForTripPackage(Model, Model.TripPackages[row - 1]);
					}
					if (Model.AreSurchargesEnabled)
					{
						(decimal surcharges, Charge? match) = PriceTripHelper.CheckForApplicableSurcharge(Model.TripPackages[row - 1].PackageType, Model.PickupZone, Model.DeliveryZone, Model.SurchargeCharges);
						if (surcharges > 0)
						{
							rate += surcharges;

							if (!Model.DictRowSurcharge.ContainsKey(row - 1))
							{
								Model.DictRowSurcharge.Add(row - 1, match);
							}
							else
							{
								Model.DictRowSurcharge[row - 1] = match;
							}
							foreach (Charge? sc in Model.DictRowSurcharge.Values)
							{
								if (sc != null)
								{
									Label lbl = (from L in SurchargeLabels where L.Name.Contains(sc.ChargeId) select L).FirstOrDefault();
									if (lbl != null)
									{
										lbl.Visibility = Visibility.Visible;
									}
								}
							}
						}
						else
						{
							// Remove the previous surcharge
							if (Model.DictRowSurcharge.ContainsKey(row - 1))
							{
								Model.DictRowSurcharge[row - 1] = null;
							}
						}

						UpdateVisibleSurcharges();
					}

					Logging.WriteLogLine($"DEBUG Rate: {rate}");

					var name = "Row_" + row + "_Rate";

					foreach (var ctrl in gridTripItems.Children)
					{
						if (ctrl is TextBox tb && (tb.Name == name))
						{
							tb.Text = rate.ToString("C");
							break;
						}
					}
				}
				//else
				//{
				//	// Trip's rate comes from the QUOTES charge field
				//	// TODO If there is already a rate, it isn't removed
				//	rate = GetValueOfQuotesField();
				//}

				RecalculateTripItemTotals(); // Clear out the totals
				BtnCalculate_Click(null, null);
			}
		}
	}

	private decimal GetValueOfQuotesField()
	{
		decimal value = 0;

        Charge? charge = (from C in Model.Charges where C.ChargeId.Trim() == Model.QUOTES_CHARGE_AND_PACKAGETYPE_ID select C).FirstOrDefault();
        if (charge != null)
        {
            foreach (var panel in GridCharges.Children)
            {
                if (panel is StackPanel sp)
                {
                    foreach (var c in sp.Children)
                    {
                        if (c is TextBox tb)
                        {
                            var (chargeId, _) = GetChargeIdFromName(tb.Name);
                            if (chargeId != null && chargeId == Model.QUOTES_CHARGE_AND_PACKAGETYPE_ID)
                            {
                                bool worked = decimal.TryParse(tb.Text, out value);
                                if (!worked)
                                {
                                    value = 0;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        return value;
	}

	private void UpdateVisibleSurcharges()
	{
		if (Model != null)
		{
			foreach (Label lbl in SurchargeLabels)
			{
				Charge sc = (from C in Model.DictRowSurcharge.Values where C != null && lbl.Name.Contains(C.ChargeId) select C).FirstOrDefault();
				lbl.Visibility = sc != null ? Visibility.Visible : Visibility.Collapsed;
            }
		}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="model">
	/// </param>
	/// <param
	///     name="newRowNumber">
	/// </param>
	/// <param
	///     name="tp">
	/// </param>
	/// <param
	///     name="showDefaultCheckboxes">
	/// </param>
	public void BuildNewPackageTypeRow( TripEntryModel model, int newRowNumber, TripPackage? tp = null, bool showDefaultCheckboxes = false )
	{
		//var NewRowNumber = gridTripItems.RowDefinitions.Count;
		++newRowNumber;
		Logging.WriteLogLine( "DEBUG newRowNumber: " + newRowNumber );

		//var Rd = new RowDefinition
		//         {
		//	         Height = gridTripItems.RowDefinitions[ 0 ].Height
		//         };
		//gridTripItems.RowDefinitions.Add( Rd );

		if( tp == null )
		{
			var pieces = model.GetPiecesDefault();
			var weight = model.GetWeightDefault();

			tp = new DisplayPackage( new TripPackage() )
				 {
					 Weight = weight,
					 Pieces = pieces
				 };

			var Tis = model.TripPackages;
			Tis.Add( new DisplayPackage( tp ) );
			model.TripPackages = Tis;
		}
		else
		{
			if( tp.Pieces < 1 )
				tp.Pieces = 1;

			if( tp.Weight < 1 )
				tp.Weight = 1;
		}

		//
		// Build columns
		//
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");
		var Column = 0;

		// Package Type
		// Holds the Default CheckBox - only visible in first row
		StackPanel sp = new()
						{
							Name        = "SpDefaultPackageType",
							Orientation = Orientation.Horizontal
						};

		CheckBox checkBox = new()
							{
								Name                     = "CbPackageTypeDefault",
								Background               = Brushes.White,
								VerticalContentAlignment = VerticalAlignment.Center,
								VerticalAlignment        = VerticalAlignment.Center
							};
		checkBox.Checked   += ( o, e ) => CbPackageTypeDefault_Checked( o, e );
		checkBox.Unchecked += ( o, e ) => CbPackageTypeDefault_Unchecked( o, e );

		// Only shown on first row
		if( !showDefaultCheckboxes )
		{
			//checkBox.Visibility = Visibility.Collapsed;
			checkBox.Visibility = Visibility.Hidden;
		}
		sp.Children.Add( checkBox );

		var Cb = new ComboBox
				 {
					 Name        = "Row_" + newRowNumber + "_PackageType",
					 ItemsSource = model.PackageTypes,
					 Background  = Brushes.White,
					 BorderBrush = Brushes.White
				 };

		var Bind = new Binding
				   {
					   Path = new PropertyPath( "PackageType" ),

					   Source = model.TripPackages[ newRowNumber - 1 ] // Is 2 s/b 3
				   };
		//Logging.WriteLogLine("DEBUG - Checking progress for crash TripPackages.Count: " + model.TripPackages?.Count);
		Cb.SetBinding( Selector.SelectedItemProperty, Bind );

		Cb.SelectionChanged += OnPackageTypeChange;

		sp.Children.Add( Cb );

		gridTripItems.Children.Add( sp );
		Grid.SetRow( sp, newRowNumber );
		Grid.SetColumn( sp, Column++ );
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		if( model.TripPackages is {Count: > 0} && model.TripPackages[ newRowNumber - 1 ].PackageType.IsNotNullOrWhiteSpace() )
			Cb.SelectedItem = model.TripPackages[ newRowNumber - 1 ].PackageType;
		else
			Cb.SelectedItem = model.GetPackageDefault();

		sp = new StackPanel
			 {
				 Name        = "SpDefaultWeight",
				 Orientation = Orientation.Horizontal,
				 MinWidth    = 70
			 };

		checkBox = new CheckBox
				   {
					   Name                     = "CbWeightDefault",
					   Background               = Brushes.White,
					   VerticalContentAlignment = VerticalAlignment.Center,
					   VerticalAlignment        = VerticalAlignment.Center
				   };
		checkBox.Checked   += ( o, e ) => CbWeightDefault_Checked( o, e );
		checkBox.Unchecked += ( o, e ) => CbWeightDefault_Unchecked( o, e );

		// Only shown on first row
		if( !showDefaultCheckboxes )
		{
			//checkBox.Visibility = Visibility.Collapsed;
			checkBox.Visibility = Visibility.Hidden;
		}

		sp.Children.Add( checkBox );

		var DecimalSpinner = new DecimalUpDown
							 {
								 Name           = "Row_" + newRowNumber + "_Weight",
								 AllowTextInput = true,
								 MinWidth       = 70,
								 //Minimum = (decimal)0.1
								 Minimum   = model.IsMinimumWeightChangeLessThanOne ? (decimal)0.1 : 1,
								 Increment = model.IsMinimumWeightChangeLessThanOne ? (decimal)0.1 : 1
							 };

		if( ( model.TripPackages != null ) && ( newRowNumber > 0 ) )
		{
			Bind = new Binding
				   {
					   Path = new PropertyPath( "Weight" ),
					   Mode = BindingMode.TwoWay,

					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };

			DecimalSpinner.SetBinding( DecimalUpDown.ValueProperty, Bind );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		DecimalSpinner.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		DecimalSpinner.GotMouseCapture  += ( _, e ) => SelectAllText( e );
		DecimalSpinner.ValueChanged     += ( _, _ ) => RecalculateTripItemTotals();
		DecimalSpinner.PreviewTextInput += FloatTextBox_PreviewTextInput;

		DecimalSpinner.ValueChanged += OnWeightChange;

		sp.Children.Add( DecimalSpinner );
		gridTripItems.Children.Add( sp );
		Grid.SetRow( sp, newRowNumber );
		Grid.SetColumn( sp, Column++ );

		// Pieces
		sp = new StackPanel
			 {
				 Name        = "SpDefaultPieces",
				 Orientation = Orientation.Horizontal
			 };

		checkBox = new CheckBox
				   {
					   Name                     = "CbPiecesDefault",
					   Background               = Brushes.White,
					   VerticalContentAlignment = VerticalAlignment.Center,
					   VerticalAlignment        = VerticalAlignment.Center
				   };
		checkBox.Checked   += ( o, e ) => CbPiecesDefault_Checked( o, e );
		checkBox.Unchecked += ( o, e ) => CbPiecesDefault_Unchecked( o, e );

		// Only shown on first row
		if( !showDefaultCheckboxes )
		{
			//checkBox.Visibility = Visibility.Collapsed;
			checkBox.Visibility = Visibility.Hidden;
		}
		sp.Children.Add( checkBox );

		var IntegerSpinner = new IntegerUpDown
							 {
								 Name           = "Row_" + newRowNumber + "_Pieces",
								 AllowTextInput = true,
								 MinWidth       = 60,
								 Minimum        = 1
							 };

		if( model.TripPackages is {Count: > 0} )
		{
			Bind = new Binding
				   {
					   Path         = new PropertyPath( "Pieces" ),
					   StringFormat = "{0:#}",
					   Mode         = BindingMode.TwoWay,

					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };

			IntegerSpinner.SetBinding( IntegerUpDown.ValueProperty, Bind );
		}

		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		IntegerSpinner.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		IntegerSpinner.GotMouseCapture  += ( _, e ) => SelectAllText( e );

		IntegerSpinner.ValueChanged += ( _, _ ) => RecalculateTripItemTotals();

		IntegerSpinner.ValueChanged += OnPiecesChange;

		sp.Children.Add( IntegerSpinner );
		gridTripItems.Children.Add( sp );
		Grid.SetRow( sp, newRowNumber );
		Grid.SetColumn( sp, Column++ );

		if( model.TripItemsInventory.ContainsKey( newRowNumber ) && ( model.TripItemsInventory[ newRowNumber ].Count > 0 ) )
			IntegerSpinner.IsEnabled = false;

		// Length
		var Tb = new TextBox
				 {
					 Name                     = "Row_" + newRowNumber + "_Length",
					 VerticalContentAlignment = VerticalAlignment.Center
				 };

		if( model.TripPackages is {Count: > 0} )
		{
			Bind = new Binding
				   {
					   Path = new PropertyPath( "Length" ),
					   Mode = BindingMode.TwoWay,

					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };
			Tb.SetBinding( TextBox.TextProperty, Bind );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		Bind = new Binding
			   {
				   Path   = new PropertyPath( "IsDIM" ),
				   Source = model.IsDIM
			   };
		Tb.SetBinding( IsEnabledProperty, Bind );

		Tb.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		Tb.GotMouseCapture  += ( _, e ) => SelectAllText( e );

		Tb.IsEnabled = false; // TODO

		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, newRowNumber );
		Grid.SetColumn( Tb, Column++ );

		// Width
		Tb = new TextBox
			 {
				 Name                     = "Row_" + newRowNumber + "_Width",
				 VerticalContentAlignment = VerticalAlignment.Center
			 };

		if( model.TripPackages is {Count: > 0} )
		{
			Bind = new Binding
				   {
					   Path = new PropertyPath( "Width" ),
					   Mode = BindingMode.TwoWay,

					   // Source = Model.TripPackages[ newRowNumber - 2 ]
					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };
			Tb.SetBinding( TextBox.TextProperty, Bind );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		Bind = new Binding
			   {
				   Path   = new PropertyPath( "IsDIM" ),
				   Source = model.IsDIM
			   };
		Tb.SetBinding( IsEnabledProperty, Bind );

		Tb.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		Tb.GotMouseCapture  += ( _, e ) => SelectAllText( e );

		Tb.IsEnabled = false; // TODO

		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, newRowNumber );
		Grid.SetColumn( Tb, Column++ );

		// Height
		Tb = new TextBox
			 {
				 Name                     = "Row_" + newRowNumber + "_Height",
				 VerticalContentAlignment = VerticalAlignment.Center
			 };

		if( model.TripPackages is {Count: > 0} )
		{
			Bind = new Binding
				   {
					   Path = new PropertyPath( "Height" ),
					   Mode = BindingMode.TwoWay,

					   // Source = Model.TripPackages[ newRowNumber - 2 ]
					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };
			Tb.SetBinding( TextBox.TextProperty, Bind );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		Bind = new Binding
			   {
				   Path   = new PropertyPath( "IsDIM" ),
				   Source = model.IsDIM
			   };
		Tb.SetBinding( IsEnabledProperty, Bind );

		Tb.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		Tb.GotMouseCapture  += ( _, e ) => SelectAllText( e );

		Tb.IsEnabled = false; // TODO

		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, newRowNumber );
		Grid.SetColumn( Tb, Column++ );

		// Original
		Tb = new TextBox
			 {
				 Name                     = "Row_" + newRowNumber + "_Original",
				 VerticalContentAlignment = VerticalAlignment.Center
			 };

		if( model.TripPackages is {Count: > 0} )
		{
			Bind = new Binding
				   {
					   Path = new PropertyPath( "Original" ),
					   Mode = BindingMode.TwoWay,

					   // Source = Model.TripPackages[ newRowNumber - 2 ]
					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };
			Tb.SetBinding( TextBox.TextProperty, Bind );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		Bind = new Binding
			   {
				   Path   = new PropertyPath( "IsDIM" ),
				   Source = model.IsDIM
			   };
		Tb.SetBinding( IsEnabledProperty, Bind );

		Tb.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		Tb.GotMouseCapture  += ( _, e ) => SelectAllText( e );
		Tb.TextChanged      += ( _, _ ) => RecalculateTripItemTotals();

		Tb.IsEnabled = false; // TODO

		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, newRowNumber );
		Grid.SetColumn( Tb, Column++ );

		// Q/H
		Tb = new TextBox
			 {
				 Name                     = "Row_" + newRowNumber + "_QH",
				 VerticalContentAlignment = VerticalAlignment.Center
			 };

		if( model.TripPackages is {Count: > 0} )
		{
			Bind = new Binding
				   {
					   Path = new PropertyPath( "QH" ),
					   Mode = BindingMode.TwoWay,

					   // Source = Model.TripPackages[ newRowNumber - 2 ]
					   Source = model.TripPackages[ newRowNumber - 1 ]
				   };
			Tb.SetBinding( TextBox.TextProperty, Bind );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");

		Bind = new Binding
			   {
				   Path   = new PropertyPath( "IsDIM" ),
				   Source = model.IsDIM
			   };
		Tb.SetBinding( IsEnabledProperty, Bind );

		Tb.GotKeyboardFocus += ( _, e ) => SelectAllText( e );
		Tb.GotMouseCapture  += ( _, e ) => SelectAllText( e );

		Tb.IsEnabled = false; // TODO

		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, newRowNumber );
		Grid.SetColumn( Tb, Column++ );

		// Rate
		Tb = new TextBox
			 {
				 Name                     = "Row_" + newRowNumber + "_Rate",
				 VerticalContentAlignment = VerticalAlignment.Center,
				 IsReadOnly               = true,
				 Text                     = "0"
			 };

		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, newRowNumber );
		Grid.SetColumn( Tb, Column++ );

		// Add row
		var AddImage = new Image
					   {
						   Name = "Row_" + newRowNumber + "_AddRow",

						   //finalImage.Source = new BitmapImage(new Uri("pack://application:,,,/AssemblyName;component/Resources/logo.png"));
						   Source  = new BitmapImage( new Uri( "pack://application:,,,/ViewModels;component/Resources/Images/Buttons/Table-Add_16x16.scale-100.png" ) ),
						   ToolTip = (string)Application.Current.TryFindResource( "TripEntryPackageAddRowTooltip" ),
						   Margin  = new Thickness( 3, 0, 0, 0 )
					   };

		AddImage.MouseUp += Image_MouseUp_1;

		gridTripItems.Children.Add( AddImage );
		Grid.SetRow( AddImage, newRowNumber );
		Grid.SetColumn( AddImage, Column++ );

		// Delete row
		var DelImage = new Image
					   {
						   Name    = "Row_" + newRowNumber + "_DeleteRow",
						   Source  = new BitmapImage( new Uri( "pack://application:,,,/ViewModels;component/Resources/Images/Buttons/Table-Delete_16x16.scale-100.png" ) ),
						   ToolTip = (string)Application.Current.TryFindResource( "TripEntryPackageRemoveRowTooltip" ),
						   Margin  = new Thickness( 3, 0, 0, 0 )
					   };

		//DelImage.MouseUp += Image_MouseUp;

		gridTripItems.Children.Add( DelImage );
		Grid.SetRow( DelImage, newRowNumber );
		Grid.SetColumn( DelImage, Column++ );

		if( newRowNumber == 1 )
			DelImage.Visibility = Visibility.Hidden;
		else
			DelImage.MouseUp += Image_MouseUp;

		// Edit Inventory
		var InventoryImage = new Image
							 {
								 Name    = "Row_" + newRowNumber + "_EditInventory",
								 Source  = new BitmapImage( new Uri( "pack://application:,,,/ViewModels;component/Resources/Images/Buttons/Box_16x16.scale-150.png" ) ),
								 ToolTip = (string)Application.Current.TryFindResource( "TripEntryPackageEditContentsTooltip" ),
								 Margin  = new Thickness( 3, 0, 0, 0 )
							 };

		InventoryImage.MouseUp += EditContentsOfPackage;

		gridTripItems.Children.Add( InventoryImage );
		Grid.SetRow( InventoryImage, newRowNumber );
		Grid.SetColumn( InventoryImage, Column );

		if( model.TripPackages is {Count: > 0} )
		{
			for( var i = 0; i < model.TripPackages.Count; i++ )
				RecalculateRates( i + 1 );
		}
		//Logging.WriteLogLine("DEBUG - Checking progress for crash");
		//RecalculateTripItemTotals();
	}

	/// <summary>
	///     Appends a Totals row to the package types grid.
	/// </summary>
	public void BuildNewPackageTotalRow()
	{
		var NewRowNumber = gridTripItems.RowDefinitions.Count;

		//var newRowNumber = gridTripItems.RowDefinitions.Count + 1;
		var Rd = new RowDefinition
				 {
					 Height = gridTripItems.RowDefinitions[ 0 ].Height
				 };
		gridTripItems.RowDefinitions.Add( Rd );

		// <Label Grid.Column="0" Grid.Row="2" Content="{DynamicResource TripEntryPackageTotals}"/>
		var Column = 0;

		var Label = new Label
					{
						//Name = "Row_" + newRowNumber + "_Label",
						Name    = "Row_Totals_Label",
						Content = (string)Application.Current.TryFindResource( "TripEntryPackageTotals" )
					};
		gridTripItems.Children.Add( Label );
		Grid.SetRow( Label, NewRowNumber );
		Grid.SetColumn( Label, Column );

		// <TextBox Grid.Column="1" Grid.Row="2" x:Name="tbTotalWeight" Text="{Binding TotalWeight}" IsReadOnly="True"/>
		Column = 1;

		var Tb = new TextBox
				 {
					 //Name = "Row_" + newRowNumber + "_TotalWeight",
					 Name                     = "Row_Totals_TotalWeight",
					 IsReadOnly               = true,
					 VerticalContentAlignment = VerticalAlignment.Center
				 };

		//tb.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
		//tb.GotMouseCapture += (s, e) => SelectAllText(s, e);
		//Binding bind = new Binding
		//{
		//    Path = new PropertyPath("TotalWeight"),
		//    Mode = BindingMode.TwoWay,
		//    Source = Model.TotalWeight
		//};
		//tb.SetBinding(TextBox.TextProperty, bind);
		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, NewRowNumber );
		Grid.SetColumn( Tb, Column );

		// <TextBox Grid.Column="2" Grid.Row="2" x:Name="tbTotalPieces" Text="{Binding TotalPieces}" IsReadOnly="True"/>
		Column = 2;

		Tb = new TextBox
			 {
				 //Name = "Row_" + newRowNumber + "_TotalPieces",
				 Name                     = "Row_Totals_TotalPieces",
				 IsReadOnly               = true,
				 VerticalContentAlignment = VerticalAlignment.Center
			 };

		//tb.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
		//tb.GotMouseCapture += (s, e) => SelectAllText(s, e);
		//bind = new Binding
		//{
		//    Path = new PropertyPath("TotalPieces"),
		//    Mode = BindingMode.TwoWay,
		//    Source = Model.TotalPieces
		//};
		//tb.SetBinding(TextBox.TextProperty, bind);
		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, NewRowNumber );
		Grid.SetColumn( Tb, Column );

		//<TextBox Grid.Column="6" Grid.Row="2" x:Name="tbTotalOriginal" Text="{Binding TotalOriginal}" IsReadOnly="True"/>
		Column = 6;

		Tb = new TextBox
			 {
				 //Name = "Row_" + newRowNumber + "_TotalOriginal",
				 Name                     = "Row_Totals_TotalOriginal",
				 IsReadOnly               = true,
				 VerticalContentAlignment = VerticalAlignment.Center
			 };

		//tb.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
		//tb.GotMouseCapture += (s, e) => SelectAllText(s, e);
		//bind = new Binding
		//{
		//    Path = new PropertyPath("TotalOriginal"),
		//    Mode = BindingMode.TwoWay,
		//    Source = Model.TotalOriginal
		//};
		//tb.SetBinding(TextBox.TextProperty, bind);
		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, NewRowNumber );
		Grid.SetColumn( Tb, Column );

		Column = 8;

		Tb = new TextBox
			 {
				 //Name = "Row_" + newRowNumber + "_TotalOriginal",
				 Name                     = "Row_Totals_TotalRate",
				 IsReadOnly               = true,
				 VerticalContentAlignment = VerticalAlignment.Center
			 };
		gridTripItems.Children.Add( Tb );
		Grid.SetRow( Tb, NewRowNumber );
		Grid.SetColumn( Tb, Column );

		RecalculateTripItemTotals();
	}

    public void UpdatePackageTypesInComboBoxes(string package = "")
    {
        if (DataContext is TripEntryModel Model)
        {
            foreach (var Control in gridTripItems.Children)
            {
                if (Control is StackPanel sp)
                {
                    foreach (var child in sp.Children)
                    {
                        if (child is ComboBox cb)
                        {
                            if (cb.Name.Contains("_PackageType"))
                            {
								string currentPT = cb.Text;
                                cb.ItemsSource = null;
                                cb.ItemsSource = Model.PackageTypes;

								if (Model.PackageTypes.Contains(currentPT))
								{
									cb.Text = currentPT;
								}
								else
								{
									cb.SelectedIndex = 0;
								}
                            }
                        }
                    }
                }
            }
        }
    }

    public void UpdatePackageTypesInComboBoxes_V2( string package = "" )
	{
		if( DataContext is TripEntryModel Model )
		{
			foreach( var Control in gridTripItems.Children )
			{
				if( Control is StackPanel sp )
				{
					foreach( var child in sp.Children )
					{
						if( child is ComboBox cb )
						{
							if( cb.Name.Contains( "_PackageType" ) )
							{
								cb.ItemsSource = null;
								cb.ItemsSource = Model.PackageTypes;

								if( package.IsNullOrWhiteSpace() )
								{
									cb.SelectedIndex = 0;
								}
								else
								{
									cb.SelectedItem = package;
								}
							}
						}
					}
				}
			}
		}
	}

	public void UpdatePackageTypesInComboBoxes_V1()
	{
		if( DataContext is TripEntryModel Model )
		{
			foreach( var Control in gridTripItems.Children )
			{
				if( Control is ComboBox cb )
				{
					if( cb.Name.Contains( "_PackageType" ) )
					{
						cb.ItemsSource = null;
						cb.ItemsSource = Model.PackageTypes;
					}
				}
			}
		}
	}

	/// <summary>
	///     Trying to work around trip loading problem.
	/// </summary>
	/// <param
	///     name="trip">
	/// </param>
	public void LoadTrip( DisplayTrip trip )
	{
		cbPickupAddressCountry.Text = trip.PickupAddressCountry;
		cbPickupAddressProv.Text    = trip.PickupAddressRegion;
		cbPickupAddresses.Text      = trip.PickupCompanyName;
		tbPickupAddressSuite.Text   = trip.PickupAddressSuite;
		tbPickupAddressStreet.Text  = trip.PickupAddressAddressLine1;
		tbPickupAddressCity.Text    = trip.PickupAddressCity;

		cbDeliveryAddressCountry.Text = trip.DeliveryAddressCountry;
		cbDeliveryAddressProv.Text    = trip.DeliveryAddressRegion;
		cbDeliveryAddresses.Text      = trip.DeliveryCompanyName;
		tbDeliveryAddressSuite.Text   = trip.DeliveryAddressSuite;
		tbDeliveryAddressStreet.Text  = trip.DeliveryAddressAddressLine1;
		tbDeliveryAddressCity.Text    = trip.DeliveryAddressCity;

		if( DataContext is TripEntryModel Model )
		{
			if (trip.BillingCompanyName.IsNullOrWhiteSpace())
			{
				// Look up 
				string name = (from C in Model.Companies where C.CompanyNumber == trip.AccountId select C.CompanyName).FirstOrDefault();
				if (name != null)
				{
					trip.BillingCompanyName = name;
				}
			}
			Model.Customer = trip.BillingCompanyName;
			cbCustomer.Text = trip.BillingCompanyName;
			Logging.WriteLogLine( "DEBUG Setting CallTakerId to " + trip.CallTakerId );
			Model.CallTakerId = trip.CallTakerId;
			Logging.WriteLogLine( "Setting regions: PickupAddressRegion: " + trip.PickupAddressRegion + ", DeliverAddressRegion: " + trip.DeliveryAddressRegion );

			//var par = Model.FindItemInRegionsList( Model.PickupRegions, trip.PickupAddressRegion );
			//var dar = Model.FindItemInRegionsList( Model.DeliveryRegions, trip.DeliveryAddressRegion );
			var (Par, _) = CountriesRegions.FindRegionForCountry( trip.PickupAddressRegion, trip.PickupAddressCountry );
			var (Dar, _) = CountriesRegions.FindRegionForCountry( trip.DeliveryAddressRegion, trip.DeliveryAddressCountry );

			if( Par.IsNotNullOrWhiteSpace() )
				Model.PickupProvState = Par;
			else
			{
				// Try abbreviation
				var Region = CountriesRegions.FindRegionForAbbreviationAndCountry( trip.PickupAddressRegion, trip.PickupAddressCountry );

				Model.PickupProvState = Region.IsNotNullOrWhiteSpace() ? Region : trip.PickupAddressRegion;
			}

			if( Dar.IsNotNullOrWhiteSpace() )
				Model.DeliveryProvState = Dar;
			else
			{
				// Try abbreviation
				var Region = CountriesRegions.FindRegionForAbbreviationAndCountry( trip.DeliveryAddressRegion, trip.DeliveryAddressCountry );

				Model.DeliveryProvState = Region.IsNotNullOrWhiteSpace() ? Region : trip.DeliveryAddressRegion;
			}
		}

		tbDriverNotes.Text = trip.DriverNotes;

		if( trip.BillingAddressPhone.IsNotNullOrWhiteSpace() )
			tbCustomerPhone.Text = trip.BillingAddressPhone;

		LoadSignatures( trip );

		LoadCharges( trip.TripCharges );
		Logging.WriteLogLine( "Finished" );
	}


	public void LoadSignatures( DisplayTrip trip )
	{
		Logging.WriteLogLine( "Loading signatures: " + trip.Signatures.Count );
		var Converter = new SignatureListToImageSourceConvertor();
		imagePOP.Source = null;
		imagePOD.Source = null;

		if( trip.Signatures.Count > 0 )
		{
			if( trip.Signatures.Count > 1 )
			{
				List<Signature> First = new() {trip.Signatures[ 0 ]};

				var Image = (BitmapImage)Converter.Convert( First, null, null, null )!;

				switch( First[ 0 ].Status )
				{
				case STATUS.PICKED_UP:
					imagePOP.Source = Image;
					//SaveImage(Image, "C:\\tmp\\" + trip.TripId + "_imagePOP.png");
					break;

				case > STATUS.PICKED_UP:
					imagePOD.Source = Image;
					//SaveImage(Image, "C:\\tmp\\" + trip.TripId + "_imagePOD.png");
					break;
				}
				First.Clear();
				First.Add( trip.Signatures[ 1 ] );
				var Image2 = (BitmapImage)Converter.Convert( First, null, null, null )!;

				switch( First[ 0 ].Status )
				{
				case STATUS.PICKED_UP:
					imagePOP.Source = Image2;
					//SaveImage(Image2, "C:\\tmp\\" + trip.TripId + "_imagePOP.png");
					break;

				case > STATUS.PICKED_UP:
					imagePOD.Source = Image2;
					//SaveImage(Image2, "C:\\tmp\\" + trip.TripId + "_imagePOD.png");
					break;
				}
			}
			else
			{
				//var Image = Converter.Convert( trip.Signatures, null, null, null ) as BitmapImage;
				var Image = (BitmapImage)Converter.Convert( trip.Signatures, null, null, null )!;

				switch( trip.Signatures[ 0 ].Status )
				{
				case STATUS.PICKED_UP:
					imagePOP.Source = Image;
					//SaveImage(Image, "C:\\tmp\\" + trip.TripId + "_imagePOP.png");
					break;

				case > STATUS.PICKED_UP:
					imagePOD.Source = Image;
					//SaveImage(Image, "C:\\tmp\\" + trip.TripId + "_imagePOD.png");
					break;
				}
			}
		}
	}

	public void SaveImage( BitmapImage image, string filePath )
	{
		Logging.WriteLogLine( "DEBUG saving image to filePath" );
		BitmapEncoder Encoder = new PngBitmapEncoder();
		Encoder.Frames.Add( BitmapFrame.Create( image ) );

		using var FileStream = new FileStream( filePath, FileMode.Create );

		Encoder.Save( FileStream );
	}

	public void LoadCharges( List<TripCharge> tcs )
	{
		if( tcs is {Count: > 0} )
		{
			//Logging.WriteLogLine("DEBUG Loading " + tcs.Count + " charges");
			foreach( var Tc in tcs )
			{
				var Charge = ( from C in Model.Charges
							   where C.ChargeId == Tc.ChargeId
							   select C ).FirstOrDefault();

				if( Charge != null )
					PopulateCharge( Charge, Tc );
				else
					Logging.WriteLogLine( "ERROR Can't find a charge with the chargeId of " + Tc.ChargeId );
			}
		}
	}

	public void EnsureDecimal( object sender, TextCompositionEventArgs e )
	{
		var          ToCheck      = e.Text.Substring( e.Text.Length - 1 );
		const string DECIMAL_CHAR = ".";

		if( !char.IsDigit( e.Text, e.Text.Length - 1 ) && !ToCheck.Equals( DECIMAL_CHAR ) )
			e.Handled = true;
	}

	public void OnChargesChange( object sender, RoutedEventArgs e )
	{
	}

	public void OnLostFocus( object sender, RoutedEventArgs e )
	{
		if( sender is TextBox tb && tb.Name.StartsWith( "Tb" + ChargeControlNameDivider ) && DataContext is TripEntryModel Model )
		{
			//string chargeId = tb.Name.Substring(3);
			var (chargeId, _) = GetChargeIdFromName( tb.Name );
			var tps = (from P in Model.TripPackages where P.PackageType == Model.QUOTES_CHARGE_AND_PACKAGETYPE_ID select P).ToList();
			if (Model.DoesPackageTypeQuotesOverrideTripMatrix && chargeId == Model.QUOTES_CHARGE_AND_PACKAGETYPE_ID && tps != null && tps.Count > 0 && GetValueOfQuotesField() > 0)
			{
				RecalculateTripItemTotals(true);
                BtnCalculate_Click(null, null);
            }
			else
			{
				RecalculateAllRates();
				var value = tb.Text.Trim();

				//TripCharge tc = (from T in Model.TripCharges where T.ChargeId == chargeId select T).FirstOrDefault();
				var charge = ( from T in Model.Charges
								where T.ChargeId == chargeId
								select T ).FirstOrDefault();

				if( charge != null )
				{
					if( !charge.IsText )
					{
						var isNumber = decimal.TryParse( value, out var newValue );

						if( isNumber && ( charge.Value != newValue ) )
						{
							Logging.WriteLogLine( "DEBUG Changed " + chargeId + " from " + charge.Value + " to " + value + " - recalculating charges" );
							BtnCalculate_Click( null, null );
						}
					}
					else
						BtnCalculate_Click( null, null );

                    RecalculateTripItemTotals();
                }
			}
		}
	}

	private readonly List<Label> SurchargeLabels = new();

	/// <summary>
	///     DisplayTypes
	///     FindStringResource("ChargesWizardChargesTabDisplayOptionsNothing"),
	///     FindStringResource("ChargesWizardChargesTabDisplayOptionsCheckboxOnly"),
	///     FindStringResource("ChargesWizardChargesTabDisplayOptionsFieldOnly"),
	///     FindStringResource("ChargesWizardChargesTabDisplayOptionsCheckboxAndField"),
	///     FindStringResource("ChargesWizardChargesTabDisplayOptionsTotal"),
	/// </summary>
	public void BuildCharges()
	{
		//var NumberOfChargesPerColumn = 6;

		// First remove all children from grid
		GridCharges.Children.Clear();
		GridCharges.RowDefinitions.Clear();
		GridCharges.ColumnDefinitions.Clear();

		if( DataContext is TripEntryModel Model )
		{
			int NumberOfChargesPerColumn = Model.NumberOfChargesPerColumn;
            var currentRow = 0;
			var currentCol = 0;

			if( Model.Charges is {Count: > 0} )
			{
				GridCharges.ColumnDefinitions.Add( new ColumnDefinition() ); // Label
				GridCharges.ColumnDefinitions.Add( new ColumnDefinition() ); // Checkbox, etc.
				GridCharges.ColumnDefinitions.Add( new ColumnDefinition() ); // Divider

				Border spacer = new()
								{
									BorderBrush     = Brushes.DarkGray,
									BorderThickness = new Thickness( 1, 1, 1, 1 ),
									Margin          = new Thickness( 5, 5, 0, 5 )
								};
				GridCharges.Children.Add( spacer );
				Grid.SetColumn( spacer, 2 );
				Grid.SetRow( spacer, 0 );
				Grid.SetRowSpan( spacer, 5 );

				for( var i = 0; i < Model.Charges.Count; i++ )
				{
					var c = Model.Charges[ i ];
					Logging.WriteLogLine( "Found charge " + c.ChargeId );

					if( !c.IsOverride )
					{
						if( currentRow == NumberOfChargesPerColumn )
						{
							GridCharges.ColumnDefinitions.Add( new ColumnDefinition() );
							GridCharges.ColumnDefinitions.Add( new ColumnDefinition() );
							GridCharges.ColumnDefinitions.Add( new ColumnDefinition() );
							currentRow =  0;
							currentCol += 3;

							spacer = new Border
									 {
										 BorderBrush     = Brushes.DarkGray,
										 BorderThickness = new Thickness( 1, 1, 1, 1 ),
										 Margin          = new Thickness( 5, 5, 0, 5 )
									 };
							GridCharges.Children.Add( spacer );
							Grid.SetColumn( spacer, currentCol + 2 );
							Grid.SetRow( spacer, 0 );
							Grid.SetRowSpan( spacer, 5 );
						}

						// Only build rows in first column
						if( ( currentCol == 0 ) && ( GridCharges.RowDefinitions.Count < NumberOfChargesPerColumn ) )
						{
							for (int j = 0; j < NumberOfChargesPerColumn; j++)
							{
								GridCharges.RowDefinitions.Add( new RowDefinition() );
							}
						}

						var label = c.Label;

						if( ( c.Value > 0 ) && ( c.DisplayType != (short)ChargeDisplayType.Nothing ) )
							label += " (" + c.Value.ToString( "F2" ) + ")";

						Label lbl = new()
									{
										Content                  = label,
										VerticalAlignment        = VerticalAlignment.Center,
										VerticalContentAlignment = VerticalAlignment.Center
									};
						GridCharges.Children.Add( lbl );
						Grid.SetColumn( lbl, currentCol );
						Grid.SetRow( lbl, currentRow );

						// Now for the checkbox, etc.
						StackPanel sp;
						CheckBox   cb;
						TextBox    tb;
						decimal    tmp    = 0;
						var        result = tmp.ToString( "C" );

						switch( c.DisplayType )
						{
						case (short)ChargeDisplayType.Nothing:
							// Nothing
							//Logging.WriteLogLine("DEBUG chargeId: " + c.ChargeId);
							sp = new StackPanel
								 {
									 Orientation       = Orientation.Horizontal,
									 MinWidth          = 50,
									 VerticalAlignment = VerticalAlignment.Center
								 };

							lbl = new Label
								  {
									  //Name = "Lbl_" + c.ChargeId + "_Result",
									  Name                     = "Lbl" + ChargeControlNameDivider + c.ChargeId + ChargeControlNameDivider + "Result",
									  VerticalAlignment        = VerticalAlignment.Center,
									  VerticalContentAlignment = VerticalAlignment.Center,
									  IsEnabled                = false,
									  Content                  = result,
									  Visibility               = Visibility.Hidden
								  };
							sp.Children.Add( lbl );

							GridCharges.Children.Add( sp );
							Grid.SetColumn( sp, currentCol + 1 );
							Grid.SetRow( sp, currentRow );
							break;

						case (short)ChargeDisplayType.CheckboxOnly:
							// Checkbox 
							sp = new StackPanel
								 {
									 Orientation       = Orientation.Horizontal,
									 MinWidth          = 50,
									 VerticalAlignment = VerticalAlignment.Center
								 };

							cb = new CheckBox
								 {
									 //Name = "Cb_" + c.ChargeId,
									 Name                     = "Cb" + ChargeControlNameDivider + c.ChargeId,
									 VerticalAlignment        = VerticalAlignment.Center,
									 VerticalContentAlignment = VerticalAlignment.Center,
									 IsChecked                = false
								 };
							cb.Checked   += ChargeCheckBox_Checked;
							cb.Unchecked += ChargeCheckBox_Unchecked;
							sp.Children.Add( cb );

							// Holds the calculated value of the charge
							lbl = new Label
								  {
									  //Name = "Lbl_" + c.ChargeId + "_Result",
									  Name                     = "Lbl" + ChargeControlNameDivider + c.ChargeId + "_Result",
									  VerticalAlignment        = VerticalAlignment.Center,
									  VerticalContentAlignment = VerticalAlignment.Center,
									  IsEnabled                = false,
									  Content                  = result
								  };
							sp.Children.Add( lbl );

							GridCharges.Children.Add( sp );
							Grid.SetColumn( sp, currentCol + 1 );
							Grid.SetRow( sp, currentRow );

							break;

						case (short)ChargeDisplayType.FieldOnly:
							// Field
							sp = new StackPanel
								 {
									 Orientation       = Orientation.Horizontal,
									 MinWidth          = 50,
									 VerticalAlignment = VerticalAlignment.Center
								 };

							// Spacer to ensure alignment
							//cb = new()
							//{
							//	Visibility = Visibility.Hidden,
							//                        };
							//sp.Children.Add(cb);

							tb = new TextBox
								 {
									 //Name = "Tb_" + c.ChargeId,
									 Name                     = "Tb" + ChargeControlNameDivider + c.ChargeId,
									 VerticalContentAlignment = VerticalAlignment.Center,
									 IsEnabled                = true
								 };

							if( !c.IsText )
							{
								// Don't restrict text fields
								tb.PreviewTextInput += EnsureDecimal;
								tb.Width            =  50;
								tb.MinWidth         =  50;
							}
							else
							{
								tb.Width    = 100;
								tb.MinWidth = 100;
							}

							tb.LostFocus   += OnLostFocus;
							tb.TextChanged += Charge_TextChanged;
							sp.Children.Add( tb );

							if( !c.IsText )
							{
								// Holds the calculated value of the charge
								// TODO Need value
								lbl = new Label
									  {
										  //Name = "Lbl_" + c.ChargeId + "_Result",
										  Name                     = "Lbl" + ChargeControlNameDivider + c.ChargeId + ChargeControlNameDivider + "Result",
										  VerticalAlignment        = VerticalAlignment.Center,
										  VerticalContentAlignment = VerticalAlignment.Center,
										  IsEnabled                = false,
										  Content                  = result
									  };
								sp.Children.Add( lbl );
							}

							GridCharges.Children.Add( sp );
							Grid.SetColumn( sp, currentCol + 1 );
							Grid.SetRow( sp, currentRow );

							break;

						case (short)ChargeDisplayType.CheckboxAndField:
							//Checkbox and Field
							sp = new StackPanel
								 {
									 Orientation       = Orientation.Horizontal,
									 MinWidth          = 50,
									 VerticalAlignment = VerticalAlignment.Center
								 };

							cb = new CheckBox
								 {
									 //Name = "Cb_" + c.ChargeId,
									 Name                     = "Cb" + ChargeControlNameDivider + c.ChargeId,
									 VerticalAlignment        = VerticalAlignment.Center,
									 VerticalContentAlignment = VerticalAlignment.Center,
									 IsChecked                = false
								 };
							cb.Checked   += ChargeCheckBox_Checked;
							cb.Unchecked += ChargeCheckBox_Unchecked;
							sp.Children.Add( cb );

							tb = new TextBox
								 {
									 //Name = "Tb_" + c.ChargeId,
									 Name                     = "Tb" + ChargeControlNameDivider + c.ChargeId,
									 VerticalContentAlignment = VerticalAlignment.Center,
									 IsEnabled                = false,
									 Width                    = 50,
									 MinWidth                 = 50,
									 Margin                   = new Thickness( 5, 0, 0, 0 )
								 };

							tb.LostFocus += OnLostFocus;
                            tb.TextChanged += Charge_TextChanged;
                            sp.Children.Add( tb );

							// Holds the calculated value of the charge
							// TODO Need value
							lbl = new Label
								  {
									  //Name = "Lbl_" + c.ChargeId + "_Result",
									  Name                     = "Lbl" + ChargeControlNameDivider + c.ChargeId + ChargeControlNameDivider + "Result",
									  VerticalAlignment        = VerticalAlignment.Center,
									  VerticalContentAlignment = VerticalAlignment.Center,
									  IsEnabled                = false,
									  Content                  = result
								  };
							sp.Children.Add( lbl );

							GridCharges.Children.Add( sp );
							Grid.SetColumn( sp, currentCol + 1 );
							Grid.SetRow( sp, currentRow );

							break;

						case (short)ChargeDisplayType.Total:
							// Total
							sp = new StackPanel
								 {
									 Orientation       = Orientation.Horizontal,
									 MinWidth          = 50,
									 VerticalAlignment = VerticalAlignment.Center
								 };
							// Spacer to ensure alignment
							//cb = new()
							//{
							//	Visibility = Visibility.Hidden,
							//};
							//sp.Children.Add(cb);
							//tb = new()
							//{
							//	Name = "Tb_" + c.ChargeId,
							//	IsReadOnly = true,
							//	Text = c.Value + "",
							//	VerticalContentAlignment = VerticalAlignment.Center,
							//	Width = 50,
							//	MinWidth = 50,
							//};
							//sp.Children.Add(tb);

							lbl = new Label
								  {
									  //Name = "Lbl_" + c.ChargeId + "_Result",
									  Name                     = "Lbl" + ChargeControlNameDivider + c.ChargeId + ChargeControlNameDivider + "Result",
									  VerticalAlignment        = VerticalAlignment.Center,
									  VerticalContentAlignment = VerticalAlignment.Center,
									  Content                  = result
								  };
							sp.Children.Add( lbl );

							GridCharges.Children.Add( sp );
							Grid.SetColumn( sp, currentCol + 1 );
							Grid.SetRow( sp, currentRow );

							break;

						default:
							Logging.WriteLogLine( "No match for charge DisplayType: " + c.DisplayType );
							break;
						}

						++currentRow;
					}
					else
						Logging.WriteLogLine( "Skipping override charge " + c.ChargeId );
				}

				RlCharges.Width = GridCharges.ActualHeight * 2.5;
			}
			if (Model.AreSurchargesEnabled && Model.SurchargeCharges is { Count: > 0 })
			{
				GridCharges.ColumnDefinitions.Add(new ColumnDefinition());
				GridCharges.ColumnDefinitions.Add(new ColumnDefinition());
				GridCharges.ColumnDefinitions.Add(new ColumnDefinition());
				currentRow = 0;
				currentCol += 3;

				Border spacer = new()
				{
					BorderBrush = Brushes.DarkGray,
					BorderThickness = new Thickness(1, 1, 1, 1),
					Margin = new Thickness(5, 5, 0, 5)
				};
				GridCharges.Children.Add(spacer);
				Grid.SetColumn(spacer, currentCol + 2);
				Grid.SetRow(spacer, 0);
				Grid.SetRowSpan(spacer, 5);

				foreach (Charge charge in Model.SurchargeCharges)
				{
					if (currentRow == NumberOfChargesPerColumn)
					{
						GridCharges.ColumnDefinitions.Add(new ColumnDefinition());
						GridCharges.ColumnDefinitions.Add(new ColumnDefinition());
						GridCharges.ColumnDefinitions.Add(new ColumnDefinition());
						currentRow = 0;
						currentCol += 3;

						spacer = new Border
						{
							BorderBrush = Brushes.DarkGray,
							BorderThickness = new Thickness(1, 1, 1, 1),
							Margin = new Thickness(5, 5, 0, 5)
						};
						GridCharges.Children.Add(spacer);
						Grid.SetColumn(spacer, currentCol + 2);
						Grid.SetRow(spacer, 0);
						Grid.SetRowSpan(spacer, 5);
					}

					// Only build rows in first column
					if ((currentCol == 0) && (GridCharges.RowDefinitions.Count < NumberOfChargesPerColumn))
					{
						for (int j = 0; j < NumberOfChargesPerColumn; j++)
						{
							GridCharges.RowDefinitions.Add(new RowDefinition());
						}
					}

					StackPanel sp = new()
					{
						Orientation = Orientation.Horizontal,
						MinWidth = 50,
						VerticalAlignment = VerticalAlignment.Center
					};
					Label lbl = new()
					{
						Name = "Tb" + ChargeControlNameDivider + charge.ChargeId + ChargeControlNameDivider + "Name",
						VerticalAlignment = VerticalAlignment.Center,
						VerticalContentAlignment = VerticalAlignment.Center,
						Content = charge.Label,
					};
					sp.Children.Add(lbl);
					SurchargeLabels.Add(lbl);

					lbl = new()
					{
						Name = "Tb" + ChargeControlNameDivider + charge.ChargeId + ChargeControlNameDivider + "Result",
						VerticalAlignment = VerticalAlignment.Center,
						VerticalContentAlignment = VerticalAlignment.Center,
						Content = charge.Value.ToString("C"),
						BorderBrush = Brushes.Black,
						BorderThickness = new Thickness(1),
					};
					sp.Children.Add(lbl);
					SurchargeLabels.Add(lbl);

					GridCharges.Children.Add(sp);
					Grid.SetColumn(sp, currentCol + 1);
					Grid.SetRow(sp, currentRow);

					++currentRow;
				}
			}

			ClearChargesAndTotals();
		}
	}

#region Program Close
	public void Dispose()
	{
		try
		{
			Model.OnDispose();
		}
		catch
		{
		}
		finally
		{
			Disposed = true;
		}
	}
#endregion

#region Defaults
	private void BtnRemoveCheckedDefaults_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
		{
			if( Model.IsPickupAddressDefaultChecked || Model.IsDeliveryAddressDefaultChecked || Model.IsPackageTypeDefaultChecked || Model.IsWeightDefaultChecked
			    || Model.IsPiecesDefaultChecked || Model.IsServiceLevelDefaultChecked || Model.IsReadyTimeDefaultChecked || Model.IsDueByTimeDefaultChecked )
			{
				var title   = FindStringResource( "TripEntryClearCheckedDefaultsTitle" );
				var message = FindStringResource( "TripEntryClearCheckedDefaults" ) + "\n" + FindStringResource( "TripEntryClearCheckedDefaults2" );
				var mbr     = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );

				if( mbr == MessageBoxResult.Yes )
					Model.RemoveCheckedDefaults();
			}
			else
			{
				var title   = FindStringResource( "TripEntryClearCheckedDefaultsTitle" );
				var message = FindStringResource( "TripEntryClearCheckedDefaultsNoneChecked" );
				MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
			}
		}
	}

	private void CbPickupAddressDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsPickupAddressDefaultChecked = true;
	}

	private void CbPickupAddressDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsPickupAddressDefaultChecked = false;
	}

	private void CbDeliveryAddressDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsDeliveryAddressDefaultChecked = true;
	}


	private void CbDeliveryAddressDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsDeliveryAddressDefaultChecked = false;
	}

	private void CbServiceLevelDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsServiceLevelDefaultChecked = true;
	}

	private void CbServiceLevelDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsServiceLevelDefaultChecked = false;
	}

	private void CbPackageTypeDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsPackageTypeDefaultChecked = true;
	}

	private void CbPackageTypeDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsPackageTypeDefaultChecked = false;
	}

	private void CbWeightDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsWeightDefaultChecked = true;
	}

	private void CbWeightDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsWeightDefaultChecked = false;
	}

	private void CbPiecesDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsPiecesDefaultChecked = true;
	}

	private void CbPiecesDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsPiecesDefaultChecked = false;
	}

	private void CbReadyTime_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsReadyTimeDefaultChecked = false;
	}

	private void CbReadyTime_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsReadyTimeDefaultChecked = true;
	}

	private void CbDueByTimeDefault_Unchecked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsDueByTimeDefaultChecked = false;
	}

	private void CbDueByTimeDefault_Checked( object sender, RoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.IsDueByTimeDefaultChecked = true;
	}
#endregion

#region Commands
	private void NewTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnNew_Click( null, null );
	}

	private void UpdateTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnSave_Click( null, null );
	}

	private void CaclulateTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnCalculate_Click( null, null );
	}

	private void FindAccountCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnFindCustomer_Click( null, null );
	}

	private void CloneTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnClone_Click( null, null );
	}

	private void FindTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnFindTrip_Click( null, null );
	}

	private void ClearTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnClear_Click( null, null );
	}

	private void LoadCalculatorCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnLoadCalculator_Click( null, null );
	}

	private void ScheduleTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnSchedule_Click( null, null );
	}

	private void AuditTripCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnProduceAudit_Click( null, null );
	}

	private void SwapAddressesCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		if( DataContext is TripEntryModel Model )
			Model.Execute_SwapAddresses();
	}

	private void ShowWaybillCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		BtnProduceWaybill_Click( null, null );
	}

	private void CloneReturnCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		if( DataContext is TripEntryModel {IsShowCloneReturn: true} )
			BtnCloneReturn_Click( null, null );
	}

	private void ClonePickupCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		if( DataContext is TripEntryModel {IsShowClonePickup: true} )
			BtnClonePickup_Click( null, null );
	}

	private void SuperTabCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		//Logging.WriteLogLine("SuperTab");
		if( DataContext is TripEntryModel Model )
		{
			if( Model.CurrentSection < SECTIONS.DELIVERY_NOTES )
				DoJump( Model.CurrentSection + 1 );
			else
			{
				// Loop back
				DoJump( SECTIONS.BILLING );
			}
		}
	}

	private void JumpBillingCommandBinding_Executed( object? sender, ExecutedRoutedEventArgs? e )
	{
		DoJump( SECTIONS.BILLING, false );
	}

	private void JumpPickupCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.PICKUP, false );
	}

	private void JumpDeliveryCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.DELIVERY, false );
	}

	private void JumpPackageCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.PACKAGE, false );
	}

	private void JumpChargesCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.CHARGES );
	}

	private void JumpPickupNotesCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.PICKUP_NOTES );
	}

	private void JumpDeliveryNotesCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.DELIVERY_NOTES );
	}

	private void JumpWeightCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.WEIGHT );
	}

	private void JumpServicesCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.SERVICES, false );
	}

	private void JumpReferenceCommandBinding_Executed( object sender, ExecutedRoutedEventArgs e )
	{
		DoJump( SECTIONS.REFERENCE );
	}

	private void DoJump( SECTIONS section, bool showDropdown = true )
	{
		if( DataContext is TripEntryModel Model )
		{
			Model.CurrentSection = section;

			//Logging.WriteLogLine("Jumping to section " + section);
			switch( section )
			{
			case SECTIONS.BILLING:
				DrawAttention( cbAccount, showDropdown, true );
				//if (!cbAccount.IsFocused) 
				//{ 
				//	DrawAttention(cbAccount, showDropdown, true);
				//}
				//else
				//               {
				//	DrawAttention(cbCustomer, showDropdown, true);
				//               }
				break;

			case SECTIONS.PICKUP:
				DrawAttention( cbPickupAddresses, showDropdown, true );
				break;

			case SECTIONS.DELIVERY:
				DrawAttention( cbDeliveryAddresses, showDropdown, true );
				break;

			case SECTIONS.PACKAGE:
				foreach( var child in gridTripItems.Children )
				{
					var doBreak = false;

					if( child is StackPanel {Name: "SpDefaultPackageType"} sp )
					{
						foreach( var c in sp.Children )
						{
							if( c is ComboBox {Name: "Row_1_PackageType"} cb )
							{
								DrawAttention( cb, showDropdown, true );
								doBreak = true;
								break;
							}
						}

						if( doBreak )
							break;
					}
				}
				break;

			case SECTIONS.CHARGES:
				if( Model.Charges is {Count: > 0} )
				{
					foreach( var charge in Model.Charges )
					{
						var id    = charge.ChargeId;
						var found = false;

						foreach( var child in GridCharges.Children )
						{
							if( child is StackPanel sp )
							{
								foreach( var spChild in sp.Children )
								{
									if( spChild is CheckBox cb && cb.Name.Contains( id ) )
									{
										DrawAttention( cb );
										found = true;
										break;
									}

									if( spChild is TextBox tb && tb.Name.Contains( id ) )
									{
										DrawAttention( tb );
										found = true;
										break;
									}

									if( found )
										break;
								}
							}

							if( found )
								break;
						}

						if( found )
							break;
					}
				}

				break;

			case SECTIONS.PICKUP_NOTES:
				DrawAttention( tbPickupNotes, false, true );
				break;

			case SECTIONS.WEIGHT:
				foreach( var child in gridTripItems.Children )
				{
					var doBreak = false;

					if( child is StackPanel {Name: "SpDefaultWeight"} sp )
					{
						foreach( var c in sp.Children )
						{
							if( c is DecimalUpDown {Name: "Row_1_Weight"} dud )
							{
								DrawAttention( dud, false, true );
								doBreak = true;
								break;
							}
						}

						if( doBreak )
							break;
					}
				}
				break;

			case SECTIONS.SERVICES:
				DrawAttention( cbServiceLevel, showDropdown );
				break;

			case SECTIONS.DELIVERY_NOTES:
				DrawAttention( tbDeliveryNotes, false, true );
				break;

			case SECTIONS.REFERENCE:
				DrawAttention( tbReference, false, true );
				break;

			default:
				Logging.WriteLogLine( "ERROR Can't match jump section: " + section );
				break;
			}
		}
	}

	private static void DrawAttention( Control control, bool dropDownCombo = false, bool flashBackground = false )
	{
		control.Focus();

		if( control is ComboBox Cb )
		{
			//if (cb.SelectedIndex == -1)
			//{
			//    cb.SelectedIndex = 0;
			//}
			Cb.IsDropDownOpen = dropDownCombo;
			Cb.Focus();
		}

		if( flashBackground )
		{
			var Old = control.Background;
			control.Background = Brushes.Yellow;
			DispatcherTimer Dt = new()
								 {
									 Interval = TimeSpan.FromMilliseconds( 500 )
								 };

			Dt.Tick += delegate
					   {
						   control.Background = Old;
						   Dt.Stop();
					   };
			Dt.Start();
		}
	}
#endregion
}