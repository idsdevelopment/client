﻿namespace ViewModels.Trips.TripEntry;

public class FindCustomerModel : ViewModelBase
{
	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( !IsInDesignMode )
		{
			Task.WaitAll(
			             Task.Run( async () =>
			                       {
				                       var Companies = ( from Company in await Azure.Client.RequestGetCustomerCodeCompanyNameList()
				                                         let CompanyName = Company.CompanyName.Trim().ToLower()
				                                         orderby CompanyName
				                                         group Company by CompanyName
				                                         into G
				                                         select G.First() ).ToList();
				                       DictCustomerNamesIds = Companies.ToDictionary( a => a.CompanyName, a => a );
				                       Logging.WriteLogLine( "DEBUG: DictCustomerNamesIds.Count: " + DictCustomerNamesIds.Keys.Count );

				                       if( DictCustomerNamesIds.Count > 0 )
				                       {
					                       Names.Clear();
					                       SortedDictionary<string, string> sd = new();

					                       foreach( var name in DictCustomerNamesIds.Keys )
					                       {
						                       if( !sd.ContainsKey( name ) )
						                       {
							                       Logging.WriteLogLine( "Adding name: " + name );
							                       sd.Add( name, string.Empty );
						                       }
						                       else
							                       Logging.WriteLogLine( "Name already added: " + name );
					                       }

					                       Names.AddRange( sd.Keys );
					                       FilteredNames.Clear();

					                       foreach( var name in Names )
						                       FilteredNames.Add( name );
				                       }
			                       } )
			            );
		}
	}

#region Actions
	public void ApplyFilter( string filter )
	{
		Logging.WriteLogLine( "DEBUG Filter: " + filter );

		if( filter.IsNotNullOrWhiteSpace() )
		{
			var tmp = ( from N in Names
			            where N.ToLower().Contains( filter )
			            select N ).ToList();
			FilteredNames.Clear();

			foreach( var name in tmp )
				FilteredNames.Add( name );
		}
		else
		{
			FilteredNames.Clear();

			foreach( var name in Names )
				FilteredNames.Add( name );
		}
	}
#endregion

#region Data
	public Dictionary<string, CustomerCodeCompanyName> DictCustomerNamesIds
	{
		get => Get( () => DictCustomerNamesIds, new Dictionary<string, CustomerCodeCompanyName>() );
		set => Set( () => DictCustomerNamesIds, value );
	}


	//public List<CompanyAddress> Companies
	//{
	//    get => Get(() => Companies, new List<CompanyAddress>());
	//    set => Set(() => Companies, value);
	//}


	//public SortedDictionary<string, string> SortedCompanyNameId
	//{
	//    get => Get(() => SortedCompanyNameId, new SortedDictionary<string, string>());
	//    set => Set(() => SortedCompanyNameId, value);
	//}


	public List<string> Names
	{
		get => Get( () => Names, new List<string>() );
		set => Set( () => Names, value );
	}


	public ObservableCollection<string> FilteredNames
	{
		get => Get( () => FilteredNames, new ObservableCollection<string>() );
		set => Set( () => FilteredNames, value );
	}


	public string SelectedName
	{
		get => Get( () => SelectedName, "" );
		set => Set( () => SelectedName, value );
	}

	[DependsUpon( nameof( SelectedName ) )]
	public void WhenSelectedNameChanges()
	{
		if( SelectedName.IsNotNullOrEmpty() )
		{
			IsAssignButtonEnabled = true;
			SelectedCompanyId     = DictCustomerNamesIds[ SelectedName ].CustomerCode;
			Logging.WriteLogLine( "Selected " + SelectedName + " => " + SelectedCompanyId );
		}
		else
			IsAssignButtonEnabled = false;
	}


	public string SelectedCompanyId
	{
		get => Get( () => SelectedCompanyId, "" );
		set => Set( () => SelectedCompanyId, value );
	}


	public bool IsAssignButtonEnabled
	{
		get => Get( () => IsAssignButtonEnabled, false );
		set => Set( () => IsAssignButtonEnabled, value );
	}
#endregion
}