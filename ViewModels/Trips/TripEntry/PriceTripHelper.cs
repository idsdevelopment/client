﻿#nullable enable

using Protocol.Data;
using System.Text.RegularExpressions;
using System.Windows.Markup;
using System.Windows.Threading;
using ViewModels.ChargesWizard;
using ViewModels.RateWizard;

namespace ViewModels.Trips.TripEntry;

public static class PriceTripHelper
{
	private static TripEntryModel? Model;
	private static List<OverridePackage>? PackageOverrides;
	private static List<RedirectPackage>? PackageRedirects;
	private static List<Charge>? DefaultCharges;

	public static readonly int ACCOUNT_DISCOUNT_OVERRIDE = 0;
	public static readonly int ACCOUNT_DISCOUNT_PERCENTAGE = 1;

	private static PrimaryCompany? Company = null;

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

    #region Charges
    /// <summary>
    ///     Note: dict contains both charges and applicable overrides.
    /// </summary>
    /// <param
    ///     name="model">
    /// </param>
    /// <param
    ///     name="dict">
    /// </param>
    public static void PriceTrip( TripEntryModel model, Dictionary<string, string> dict )
	{
		Model = model;
		Model.TripCharges.Clear();
		//Logging.WriteLogLine("DEBUG TotalDeliveryCharges: " + Model.TotalDeliveryCharges);
		//TotalCharges = TotalFuelSurcharges = TotalDiscount = TotalTaxes = TotalBeforeTax = TotalTotal = TotalPayroll = 0;
		Model.TotalCharges   = Model.TotalFuelSurcharges = Model.TotalDiscount = Model.TotalTaxes = Model.TotalBeforeTax = Model.TotalPayroll = 0;
		Model.TotalBeforeTax = Model.TotalTotal          = Model.TotalDeliveryCharges;

		// TODO Do something with them - From IDS1 CustomTripCharge.calculate, the default charge takes precedence
		DefaultCharges = Model.GetDefaultChargesForAccount(Model.SelectedAccount);


		foreach( var c in Model.Charges )
		{
			// Don't include overrides
			if( c.AlwaysApplied && !c.IsOverride && !dict.ContainsKey( c.ChargeId ) )
				dict.Add( c.ChargeId, string.Empty );
		}

		List<Charge> taxes;
		List<Charge> includeInTaxes;
		List<Charge> fuelSurcharges;
		List<Charge> includeInFsc;
		List<Charge> includeInPayroll;
		List<Charge> others;

		if( dict.Count > 0 )
		{
			( taxes, includeInTaxes, fuelSurcharges, includeInFsc, includeInPayroll, others ) = GetChargesByType( dict );

			if( others.Count > 0 )
			{
				//foreach( var charge in others )
				for (int i = 0; i < others.Count; i++)				
				{
					Charge charge = others[i];
					// TODO Turned off for the moment
					
					Charge? defaultCharge = FindDefaultChargeForCharge( charge );
					if ( defaultCharge != null )
					{
						charge = defaultCharge;
						if (!dict.ContainsKey( charge.ChargeId ))
						{
							dict.Add(charge.ChargeId, string.Empty);
						}
					}
					
					if( !charge.IsText )
					{
						var tc = CalculateCharge( charge, dict[ charge.ChargeId ], null );
						Model.TripCharges.Add( tc );
						//results[(int)Slots.Others].Add(tc);
						Model.TotalCharges   += tc.Value;
						Model.TotalBeforeTax += tc.Value;
						Model.TotalTotal     += tc.Value;
					}
					else
					{
						TripCharge tc = new()
						                {
							                ChargeId = charge.ChargeId,
							                Text     = dict[ charge.ChargeId ]
						                };
						Model.TripCharges.Add( tc );
					}

					//                  foreach (TripPackage tp in Model.TripPackages)
					//                  {
					//                      Charge or = FindApplicableOverride(charge, tp);
					//                      if (or == null)
					//                      {
					//		or = charge;
					//                      }
					//	if (!or.IsText)
					//	{
					//		TripCharge tc = CalculateCharge(or, dict[charge.ChargeId], null);
					//		Model.TripCharges.Add(tc);
					//		//results[(int)Slots.Others].Add(tc);
					//		Model.TotalCharges += tc.Value;
					//		Model.TotalBeforeTax += tc.Value;
					//		Model.TotalTotal += tc.Value;
					//	}
					//	else
					//	{
					//		TripCharge tc = new()
					//		{
					//			ChargeId = charge.ChargeId,
					//			Text = dict[charge.ChargeId],
					//		};
					//		Model.TripCharges.Add(tc);
					//	}
					//}
				}
			}

			if( fuelSurcharges.Count > 0 )
				CalclulateFuelSurcharges( fuelSurcharges, includeInFsc, dict );

			if( taxes.Count > 0 )
				CalculateTaxes( taxes, includeInTaxes );
		}

		//Logging.WriteLogLine("DEBUG TotalCharges " + Model.TotalCharges.ToString("F2") + " TotalFuelSurcharges " + Model.TotalFuelSurcharges.ToString("F2") + " TotalDiscount " + Model.TotalDiscount.ToString("F2") + " TotalTaxes " + Model.TotalTaxes.ToString("F2") + " TotalBeforeTax " + Model.TotalBeforeTax.ToString("F2") + " TotalTotal " + Model.TotalTotal.ToString("F2") + " TotalPayroll " + Model.TotalPayroll.ToString("F2"));
	}

	//public static string MakeDefaultChargeId(string? accountId, string chargeId) => Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX + accountId + "_" + chargeId;
	public static string MakeDefaultChargeId(string? accountId, string chargeId)
	{
		string id;
		if (!chargeId.Contains(Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX))
		{
			id = Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX + accountId + "_" + chargeId;
		}
		else
		{
			id = chargeId;
		}
		return id;
	}
    
	private static Charge? FindDefaultChargeForCharge( Charge charge )
	{
		Charge? found = null;
		if (DefaultCharges?.Count > 0 )
		{
			string id = MakeDefaultChargeId(Model?.SelectedAccount, charge.ChargeId);
			if (charge.ChargeId.Contains(Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX))
			{
				id = charge.ChargeId;
			}

			found = (from C in DefaultCharges where C.ChargeId == id select C).FirstOrDefault();
		}

		return found;
	}

	public static decimal CheckMinMax( Charge charge, decimal result )
	{
		if( ( charge.Min > 0 ) && ( result < charge.Min ) )
		{
			Logging.WriteLogLine( "Result " + result + " is less than charge.Min: " + charge.Min + " setting to Min" );
			result = charge.Min;
		}

		if( ( charge.Max > 0 ) && ( result > charge.Max ) )
		{
			Logging.WriteLogLine( "Result " + result + " is greater than charge.Max: " + charge.Max + " setting to Max" );
			result = charge.Max;
		}
		return result;
	}
		
	private static Charge? FindApplicableOverride( Charge charge, TripPackage tp )
	{
		Charge? found = null;

		var packageMarker = FindStringResource( "ChargesWizardOverridesTabLabelPackageMarker" );
		var serviceMarker = FindStringResource( "ChargesWizardOverridesTabLabelServiceMarker" );

		var cid = charge.ChargeId + "_override";

		// cid could have more trailing characters
		var or = ( from C in Model?.Charges
		           where C.IsOverride && C.ChargeId.StartsWith( cid )
		           select C ).FirstOrDefault();

		if( or != null )
		{
			//Logging.WriteLogLine("DEBUG found override for chargeId: " + charge.ChargeId);
			// Check the conditions - package or service
			// string label = sourceCharge.ChargeId + " OVERRIDE for package " + SelectedOverridePackage
			// string label = sourceCharge.ChargeId + " OVERRIDE for service " + SelectedOverrideService;
			var isPackageOverride = or.Label.Contains( packageMarker );
			var isServiceOverride = or.Label.Contains( serviceMarker );

			if( isPackageOverride )
			{
				var pieces = Regex.Split( or.Label, packageMarker );

				if( pieces.Length == 2 )
				{
					var package = pieces[ 1 ].Trim();

					if( tp.PackageType == package )
					{
						// Found a match
						//Logging.WriteLogLine("DEBUG override matches PackageType: " + package);
						found = or;
					}
				}
			}
			else if( isServiceOverride )
			{
				var pieces = Regex.Split( or.Label, serviceMarker );

				if( pieces.Length == 2 )
				{
					var service = pieces[ 1 ].Trim();

					if( Model?.ServiceLevel == service )
					{
						// Found a match
						//Logging.WriteLogLine("DEBUG override matches ServiceLevel: " + service);
						found = or;
					}
				}
			}
		}

		return found;
	}

	private static Dictionary<string, string> CheckForOverrides( Dictionary<string, string> dict )
	{
		Dictionary<string, string> newDict = new();

		foreach( var chargeId in dict.Keys )
		{
			var charge = ( from C in Model?.Charges
			               where C.IsOverride && C.ChargeId.Contains( chargeId )
			               select C ).FirstOrDefault();

			if( charge != null )
			{
				//newDict.Add(charge.)
			}
		}

		return newDict;
	}

	private static TripCharge CalculateCharge( Charge charge, string fromTextBox, List<Charge>? include )
	{
		TripCharge tc = new()
		                {
			                ChargeId = charge.ChargeId
		                };

		if( charge != null && Model != null)
		{
			if( charge.IsText )
			{
				tc.Text = fromTextBox;
				Model.TripCharges.Add( tc );
			}
			else
			{
				var isNumber = decimal.TryParse( fromTextBox, out var field );

				if( isNumber )
				{
					switch( charge.DisplayType )
					{
					case (short)TripEntryModel.ChargeDisplayType.Nothing:
						// Nothing displayed
						tc.Value = GetResult( charge, 0 );
						break;

					case (short)TripEntryModel.ChargeDisplayType.CheckboxOnly:
						// checkbox - use the value in the charge or the formula
						tc.Value = GetResult( charge, decimal.MinValue );
						break;

					case (short)TripEntryModel.ChargeDisplayType.FieldOnly:
						// field - use the value in the field or the formula
						tc.Value = GetResult( charge, field );
						break;

					case (short)TripEntryModel.ChargeDisplayType.CheckboxAndField:
						// checkbox and field - use the value in the field or the formula
						tc.Value = GetResult( charge, field );
						break;

					case (short)TripEntryModel.ChargeDisplayType.Total:
						// total - only shows result
						tc.Value = charge.Value;
						break;

					default:
						Logging.WriteLogLine( "Found unrecognised DisplayType: " + charge.DisplayType + " in charge: " + charge.ChargeId );
						break;
					}

					//TotalCharges += tc.Value;

					////if (charge.IsFuelSurcharge)
					////{
					////	//TotalFuelSurcharges += tc.Value;
					////	TotalFuelSurcharges += (TotalDeliveryCharges + TotalCharges) * charge.Value;

					////}
					//if (!charge.IsTax)
					//{
					//	TotalBeforeTax += tc.Value;
					//	TotalTotal += tc.Value;
					//}
					////else
					////{
					////	taxes.Add(charge);
					////}
					//if (!charge.ExcludeFromTaxes && !charge.IsTax)
					//{
					//	includeInTaxes.Add(tc);
					//}
					//TripCharges.Add(tc);
				}
				else
				{
					// TODO Total fields - no checkbox or textbox - GST or Nothing
					field = decimal.MinValue;

					if( charge.DisplayType == (short)TripEntryModel.ChargeDisplayType.Nothing )
					{
						tc.Value = GetResult( charge, field );
						//TotalBeforeTax += tc.Value;
						//TotalTotal += tc.Value;
						//if (!charge.ExcludeFromTaxes)
						//{
						//	includeInTaxes.Add(tc);
						//}
						//if (!charge.IsMultiplier)
						//{
						//	TotalCharges += tc.Value;
						//}
						//else
						//{
						//	// TODO
						//}
					}
					//if (charge.AlwaysApplied)
					//{
					//	if (charge.IsTax)
					//	{
					//		taxes.Add(charge);
					//	}
					//	else
					//	{
					//		if (charge.DisplayType == (short)ChargeDisplayType.Nothing)
					//		{
					//			tc.Value = charge.Value;
					//			TotalBeforeTax += tc.Value;
					//			TotalTotal += tc.Value;
					//			if (!charge.ExcludeFromTaxes)
					//			{
					//				includeInTaxes.Add(tc);
					//			}
					//			if (!charge.IsMultiplier)
					//			{
					//				TotalCharges += tc.Value;
					//			}
					//			else
					//			{
					//				// TODO
					//			}
					//		}
					//	}
					//	TripCharges.Add(tc);
					//}
				}
			}
		}

		return tc;
	}

	private static (List<Charge> taxes, List<Charge> includeInTaxes, List<Charge> fuelSurcharges, List<Charge> includeInFsc, List<Charge> includeInPayroll, List<Charge> others) GetChargesByType( Dictionary<string, string> dict )
	{
		List<Charge> taxes = ( from C in Model?.Charges
		              where dict.ContainsKey( C.ChargeId ) && C.IsTax
		              select C ).ToList();

		List<Charge> includeInTaxes = ( from C in Model?.Charges
		                       where dict.ContainsKey( C.ChargeId ) && !C.IsTax && !C.ExcludeFromTaxes
		                       select C ).ToList();

		//List<Charge> excludeFromTax = (from C in Model.Charges where dict.ContainsKey(C.ChargeId) && C.ExcludeFromTaxes select C).ToList();
		List<Charge> fuelSurcharges = ( from C in Model?.Charges
		                       where dict.ContainsKey( C.ChargeId ) && C.IsFuelSurcharge
		                       select C ).ToList();

		List<Charge> includeInFsc = ( from C in Model?.Charges
		                     where dict.ContainsKey( C.ChargeId ) && !C.IsFuelSurcharge && !C.IsTax && !C.ExcludeFromFuelSurcharge
		                     select C ).ToList();

		List<Charge> includeInPayroll = ( from C in Model?.Charges
		                         where dict.ContainsKey( C.ChargeId ) && !C.IsTax
		                         select C ).ToList(); // TODO Need ExcludeFromPayroll

		List<Charge> others = ( from C in Model?.Charges
		               where dict.ContainsKey( C.ChargeId ) && !C.IsTax && !C.IsFuelSurcharge
		               select C ).ToList();

        List<Charge> dcs = (from C in Model?.GetDefaultChargesForAccount()
                        where !C.IsTax && !C.IsFuelSurcharge
                        select C).ToList();
		if (dcs.Count > 0)
		{
			// Replace the overridden charge
			foreach ( var dc in dcs )
			{
				// account_pref_charge_8thstreetliquorstore_Test
				string id = GetChargeIdFromDefaultTripCharge(dc.ChargeId, Model?.SelectedAccount);
				Charge found = (from C in others where C.ChargeId == id select C).FirstOrDefault();
				if (found != null)
				{
					Logging.WriteLogLine($"Replacing charge {id} with {dc.ChargeId}");
					others.Remove(found);
					others.Add(dc);
				}
				else
				{
                    others.Add(dc);
				}
			}
        }

		return ( taxes, includeInTaxes, fuelSurcharges, includeInFsc, includeInPayroll, others );
	}
	    
    public static string GetChargeIdFromDefaultTripCharge(string chargeId, string? accountId)
	{
		string id = chargeId.Trim();
		if (accountId != null)
		{
			string[] seps = { accountId.Trim() };
			string[] pieces = chargeId.Split(seps, StringSplitOptions.RemoveEmptyEntries);
			if (pieces.Length == 2 )
			{
				// _<id>
				id = pieces[1].Substring(1);
			}
		}

		return id;
	}       

	private static (List<Charge> taxes, List<Charge> fuelSurcharges, List<Charge> excludeFromTax, List<Charge> excludeFromFsc) GetChargesByType_V1( Dictionary<string, string> dict )
	{
		var taxes = ( from C in Model?.Charges
		              where dict.ContainsKey( C.ChargeId ) && C.IsTax
		              select C ).ToList();

		var fuelSurcharges = ( from C in Model?.Charges
		                       where dict.ContainsKey( C.ChargeId ) && C.IsFuelSurcharge
		                       select C ).ToList();

		var excludeFromTax = ( from C in Model?.Charges
		                       where dict.ContainsKey( C.ChargeId ) && C.ExcludeFromTaxes
		                       select C ).ToList();

		var excludeFromFsc = ( from C in Model?.Charges
		                       where dict.ContainsKey( C.ChargeId ) && C.ExcludeFromFuelSurcharge
		                       select C ).ToList();

		return ( taxes, fuelSurcharges, excludeFromTax, excludeFromFsc );
	}
	/*
	public static void CalculateCharges_V1(Dictionary<string, string> dict)
	{
		TripCharges.Clear();
		Logging.WriteLogLine("DEBUG TotalDeliveryCharges: " + TotalDeliveryCharges);
		//TotalCharges = TotalFuelSurcharges = TotalDiscount = TotalTaxes = TotalBeforeTax = TotalTotal = TotalPayroll = 0;
		TotalCharges = TotalFuelSurcharges = TotalDiscount = TotalTaxes = TotalBeforeTax = TotalPayroll = 0;
		TotalBeforeTax = TotalTotal = TotalDeliveryCharges;

		List<Charge> taxes = new();
		List<TripCharge> includeInTaxes = new();
		if (dict.Count > 0)
		{
			foreach (string chargeId in dict.Keys)
			{
				Logging.WriteLogLine("Harvested charge: chargeId: " + chargeId + " value: " + dict[chargeId]);

				Charge charge = (from C in Charges where C.ChargeId == chargeId select C).FirstOrDefault();
				if (charge != null)
				{
					TripCharge tc = new()
					{
						ChargeId = chargeId,

					};
					if (charge.IsText)
					{
						tc.Text = dict[chargeId];
						TripCharges.Add(tc);
					}
					else
					{
						bool isNumber = decimal.TryParse(dict[chargeId], out decimal field);
						if (isNumber)
						{
							switch (charge.DisplayType)
							{
								case (short)ChargeDisplayType.Nothing:
									// Nothing displayed
									tc.Value = GetResult(charge, 0);
									break;
								case (short)ChargeDisplayType.CheckboxOnly:
									// checkbox - use the value in the charge or the formula
									tc.Value = GetResult(charge, decimal.MinValue);
									//if (!charge.IsFormula)
									//{
									//    tc.Value = charge.Value;
									//}
									//else
									//{
									//    decimal result = EvaluateChargeFormulaHelper.EvaluateFullFormula(charge.Formula, TotalWeight, TotalPieces, 0);
									//    tc.Value = result;
									//}
									break;
								case (short)ChargeDisplayType.FieldOnly:
									// field - use the value in the field or the formula
									tc.Value = GetResult(charge, field);
									//if (!charge.IsFormula)
									//{
									//	tc.Value = charge.Value;
									//}
									//else
									//{
									//	decimal result = EvaluateChargeFormulaHelper.EvaluateFullFormula(charge.Formula, TotalWeight, TotalPieces, field);
									//	tc.Value = result;
									//}
									break;
								case (short)ChargeDisplayType.CheckboxAndField:
									// checkbox and field - use the value in the field or the formula
									tc.Value = GetResult(charge, field);
									//if (!charge.IsFormula)
									//{
									//	tc.Value = charge.Value;
									//}
									//else
									//{
									//	decimal result = EvaluateChargeFormulaHelper.EvaluateFullFormula(charge.Formula, TotalWeight, TotalPieces, field);
									//	tc.Value = result;
									//}
									break;
								case (short)ChargeDisplayType.Total:
									// total - only shows result
									tc.Value = charge.Value;
									break;
								default:
									Logging.WriteLogLine("Found unrecognised DisplayType: " + charge.DisplayType + " in charge: " + charge.ChargeId);
									break;
							}

							TotalCharges += tc.Value;
							//if (!charge.IsMultiplier)
							//                        {
							//	TotalCharges += tc.Value;
							//                        }
							//else
							//                        {
							//	// TODO
							//                        }

							if (charge.IsFuelSurcharge)
							{
								//TotalFuelSurcharges += tc.Value;
								TotalFuelSurcharges += (TotalDeliveryCharges + TotalCharges) * charge.Value;

							}
							if (!charge.IsTax)
							{
								TotalBeforeTax += tc.Value;
								TotalTotal += tc.Value;
							}
							else
							{
								taxes.Add(charge);
							}
							if (!charge.ExcludeFromTaxes && !charge.IsTax)
							{
								includeInTaxes.Add(tc);
							}
							TripCharges.Add(tc);
						}
						else
						{
							// TODO Total fields - no checkbox or textbox - GST or Nothing
							field = decimal.MinValue;
							if (charge.AlwaysApplied)
							{
								if (charge.IsTax)
								{
									taxes.Add(charge);
								}
								else
								{
									if (charge.DisplayType == (short)ChargeDisplayType.Nothing)
									{
										tc.Value = charge.Value;
										TotalBeforeTax += tc.Value;
										TotalTotal += tc.Value;
										if (!charge.ExcludeFromTaxes)
										{
											includeInTaxes.Add(tc);
										}
										if (!charge.IsMultiplier)
										{
											TotalCharges += tc.Value;
										}
										else
										{
											// TODO
										}
									}
								}
								TripCharges.Add(tc);
							}
						}
					}
				}
				else
				{
					Logging.WriteLogLine("Can't find a charge with the chargeId: " + chargeId);
				}
			}
		}
		// And update the totals
		CalculateTaxes(taxes, includeInTaxes);
	}
	*/

	private static decimal GetResult( Charge charge, decimal field )
	{
		decimal result = 0;

		if (Model != null)
		{
			switch( charge.ChargeType )
			{
			case (short)TripEntryModel.ChargeChargeType.CumulativeAddition:
				if( !charge.IsFormula )
				{
					//				s/b 12					10				10		12
					//result = !charge.IsMultiplier ? charge.Value : charge.Value * field;
					result = !charge.IsMultiplier ? field : charge.Value * field;
				}
				else
					result = EvaluateChargeFormulaHelper.EvaluateFullFormula( charge.Formula, Model.TotalWeight, Model.TotalPieces, field );
				break;

			case (short)TripEntryModel.ChargeChargeType.Percentage:
				if( !charge.IsTax && !charge.IsFuelSurcharge )
					result = charge.Value * Model.TotalDeliveryCharges;
				else if( charge.IsFuelSurcharge && ( field > decimal.MinValue ) )
					result = charge.Value * field;
				break;

			case (short)TripEntryModel.ChargeChargeType.Reference:
				// Nothing
				break;

			case (short)TripEntryModel.ChargeChargeType.Formula:
				if( !charge.IsFormula )
				{
					//result = !charge.IsMultiplier ? charge.Value : charge.Value * field;
					result = !charge.IsMultiplier ? field : charge.Value * field;
				}
				else
					result = EvaluateChargeFormulaHelper.EvaluateFullFormula( charge.Formula, Model.TotalWeight, Model.TotalPieces, field );
				break;

			default:
				Logging.WriteLogLine( "ERROR No match for " + charge.ChargeType + " in charge: " + charge.ChargeId );
				break;
			}

			result = CheckMinMax( charge, result );
		}

		return result;
	}

	/// <summary>
	///     DeliveryCharge + non-excluded charges.
	/// </summary>
	/// <param
	///     name="fscs">
	/// </param>
	/// <param
	///     name="exclude">
	/// </param>
	/// <param
	///     name="dict">
	/// </param>
	private static void CalclulateFuelSurcharges( List<Charge> fscs, List<Charge> include, Dictionary<string, string> dict )
	{
		if (Model != null)
		{
			decimal totalFsc = 0;

			if( fscs?.Count > 0 )
			{
				//totalFsc = Model.TotalDeliveryCharges;
				foreach( var fsc in fscs )
				{
					//foreach (string key in dict.Keys)
					var accumulate = Model.TotalDeliveryCharges;

					foreach( var c in include )
					{
						var tc = ( from T in Model.TripCharges
								   where T.ChargeId == c.ChargeId
								   select T ).FirstOrDefault();

						if( tc != null )
						{
							//totalFsc += tc.Value;
							accumulate += tc.Value;
							//Model.TotalTaxes += tc.Value * tax.Value;
							//accumulate += tc.Value * tax.Value;

							//                     decimal result = 0;
							//                     bool isNumber = decimal.TryParse(dict[c.ChargeId], out decimal field);
							//                     if (isNumber)
							//                     {
							//                         result = GetResult(fsc, field);
							//                     }
							//                     else
							//                     {
							//                         result = GetResult(fsc, decimal.MinValue);
							//                     }

							//totalFsc += result;
						}
						//Charge i = (from C in include where C.ChargeId == key select C).FirstOrDefault();
						//if (i != null)
						//{
						//	Charge c = (from C in Model.Charges where C.ChargeId == key select C).FirstOrDefault();
						//	decimal result = 0;
						//	bool isNumber = decimal.TryParse(dict[key], out decimal field);
						//	if (isNumber)
						//	{
						//		result = GetResult(fsc, field);
						//	}
						//	else
						//	{
						//		result = GetResult(fsc, decimal.MinValue);
						//	}

						//	totalFsc += result;
						//}
					}
					var result = GetResult( fsc, accumulate );
					totalFsc += result;

					TripCharge fscTc = new()
									   {
										   ChargeId = fsc.ChargeId,
										   Value    = result
									   };
					Model.TripCharges.Add( fscTc );

					//Logging.WriteLogLine("DEBUG Tax: " + tax.ChargeId + " value: " + tax.Value + " TotalDeliveryCharges: " + TotalDeliveryCharges + " Result: " + accumulate);
				}
			}

			Model.TotalFuelSurcharges =  totalFsc;
			Model.TotalBeforeTax      += totalFsc;
			Model.TotalTotal          += totalFsc;
		}
	}

	private static void CalclulateFuelSurcharges_V1( List<Charge> fscs, List<Charge> include, Dictionary<string, string> dict )
	{
		if (Model != null)
		{
			decimal totalFsc = 0;

			if( fscs?.Count > 0 )
			{
				totalFsc = Model.TotalDeliveryCharges;

				foreach( var fsc in fscs )
				{
					//foreach (string key in dict.Keys)
					foreach( var key in dict.Keys )
					{
						var i = ( from C in include
								  where C.ChargeId == key
								  select C ).FirstOrDefault();

						if( i != null )
						{
							var c = ( from C in Model.Charges
									  where C.ChargeId == key
									  select C ).FirstOrDefault();
							decimal result   = 0;
							var     isNumber = decimal.TryParse( dict[ key ], out var field );

							if( isNumber )
								result = GetResult( fsc, field );
							else
								result = GetResult( fsc, decimal.MinValue );

							totalFsc += result;
						}
					}
					//Logging.WriteLogLine("DEBUG Tax: " + tax.ChargeId + " value: " + tax.Value + " TotalDeliveryCharges: " + TotalDeliveryCharges + " Result: " + accumulate);
				}
			}

			Model.TotalFuelSurcharges = totalFsc;
		}
	}

	//private static void CalculateTaxes(List<Charge> taxes, List<TripCharge> include)
	private static void CalculateTaxes( List<Charge> taxes, List<Charge> include )
	{
		if (Model != null)
		{
			Model.TotalTaxes = 0;

			if( taxes.Count > 0 )
			{
				foreach( var tax in taxes )
				{
					// Handle the delivery charge first
					Model.TotalTaxes += Model.TotalDeliveryCharges * tax.Value;
					var accumulate = Model.TotalDeliveryCharges * tax.Value;

					//foreach (TripCharge tc in include)
					foreach( var c in include )
					{
						var tc = ( from T in Model.TripCharges
								   where T.ChargeId == c.ChargeId
								   select T ).FirstOrDefault();

						if( tc != null )
						{
							Model.TotalTaxes += tc.Value * tax.Value;
							accumulate       += tc.Value * tax.Value;
						}
					}

					TripCharge taxTc = new()
									   {
										   ChargeId = tax.ChargeId,
										   Value    = accumulate
									   };

					Model.TripCharges.Add( taxTc );
					//Logging.WriteLogLine("DEBUG Tax: " + tax.ChargeId + " value: " + tax.Value + " TotalDeliveryCharges: " + Model.TotalDeliveryCharges + " Result: " + accumulate);
				}
			}

			Model.TotalPayroll =  Model.TotalBeforeTax;
			Model.TotalTotal   += Model.TotalTaxes;
		}
	}

    #endregion
    #region Rates

	// Moved from TripEntryModel
	public static decimal GetRateForTripPackage(TripEntryModel model, TripPackage tp)
	{
		decimal result = 0;

        if (tp != null && model != null)
        {
			string PickupZone = model.PickupZone;
			string DeliveryZone = model.DeliveryZone;
			string ServiceLevel = model.ServiceLevel;
			List<PackageType> PackageTypeObjects = model.PackageTypeObjects;
			List<ServiceLevel> CompleteServiceLevels = model.CompleteServiceLevels;
			List<Zone> ZoneObjects = model.ZoneObjects; 
			List<RateMatrixItem> RateMatrixItems = model.RateMatrixItems;
			List<Charge> surcharges = model.SurchargeCharges;

            if (PickupZone.IsNotNullOrWhiteSpace() && DeliveryZone.IsNotNullOrWhiteSpace())
            {
				(List<OverridePackage> packageOverrides, List<RedirectPackage> packageRedirects) = GetOverridesAndRedirects(model);
				//(PackageOverrides, PackageRedirects) = GetOverridesAndRedirects(model);
                //Logging.WriteLogLine( "DEBUG Getting rate for zones PZ: " + PickupZone + " DZ: " + DeliveryZone + " PackageType: " + tp.PackageType + " ServiceLevel: " + ServiceLevel );

                var pt = (from P in PackageTypeObjects
                          where P.Description == tp.PackageType
                          select P).FirstOrDefault();

                var sl = (from S in CompleteServiceLevels
                          where S.OldName == ServiceLevel
                          select S).FirstOrDefault();

                var fz = (from Z in ZoneObjects
                          where Z.Name == PickupZone
                          select Z).FirstOrDefault();

                var tz = (from Z in ZoneObjects
                          where Z.Name == DeliveryZone
                          select Z).FirstOrDefault();

                if ((pt != null) && (sl != null) && (fz != null) && (tz != null))
                {
                    //Logging.WriteLogLine("DEBUG fz: " + fz.ZoneId + " tz: " + tz.ZoneId + " pt: " + pt.PackageTypeId + " sl: " + sl.ServiceLevelId);

					PackageType? overridePt = CheckForPackageOverride(pt, packageOverrides, PackageTypeObjects);
					if (overridePt != null)
					{
						Logging.WriteLogLine("Found Override Package - changing '" + pt.Description + "' to '" + overridePt.Description + "'");
						pt = overridePt;
					}

					RedirectPackage? discount = CheckForAccountDiscount(pt, sl, fz, tz, packageRedirects);
					// Can be either override or percentage - if percentage, apply at end
					if (discount != null && discount.OverrideId == ACCOUNT_DISCOUNT_OVERRIDE)
					{
						result = discount.Charge;
						Logging.WriteLogLine("Discount is an override: " + discount.Charge + " Result: " + result);
					}

					if (discount == null || (discount != null && discount.OverrideId == ACCOUNT_DISCOUNT_PERCENTAGE))
					{
						var rmi = (from R in RateMatrixItems
								   where (R.FromZoneId == fz.ZoneId) && (R.ToZoneId == tz.ZoneId)
																	   && (R.PackageTypeId == pt.PackageTypeId) && (R.ServiceLevelId == sl.ServiceLevelId)
								   select R).FirstOrDefault();

						// Try the other way
						rmi ??= (from R in RateMatrixItems
								 where (R.FromZoneId == tz.ZoneId) && (R.ToZoneId == fz.ZoneId)
																	 && (R.PackageTypeId == pt.PackageTypeId) && (R.ServiceLevelId == sl.ServiceLevelId)
								 select R).FirstOrDefault();

						if (rmi != null)
						{
							if (rmi.Formula.IsNullOrWhiteSpace())
							{
								// Fixed rate
								result = rmi.Value;
							}
							else
							{
								//Logging.WriteLogLine("DEBUG Formula: " + rmi.Formula);
								var lane = (int)rmi.Value;
								var laneFormula = EvaluateRateFormulaHelper.GetFormulaForLane(rmi.Formula, lane);

								if (laneFormula.IsNullOrWhiteSpace())
								{
									// Try full formula - Might happen if the 'Lane =' is missing
									Logging.WriteLogLine("Couldn't find a formula for lane " + lane + " in formula " + rmi.Formula);
									laneFormula = rmi.Formula;
								}
								// TODO It's possible to screw this up if one of the lane pieces uses a formula while the rest don't
								bool isFixedPrice = !EvaluateRateFormulaHelper.IsFormula(laneFormula);
								result = EvaluateRateFormulaHelper.EvaluateFullFormula(laneFormula, tp.Pieces, tp.Weight, 0, isFixedPrice);
							}

							// TODO Turn back on
							//(decimal value, List<Charge> matches) =  CheckForSurcharge(pt.Description, fz.Name, tz.Name, surcharges);
							//if (value > 0)
							//{
							//	result += value;
							//}

							// Now for the percentage discount
							if (discount != null)
							{
								decimal before = result;
								// From IDS1's RateManagerSession
								// rv = new RateValue(rv.resellerId, rv.fromZone, rv.toZone, rv.serviceLevel, rv.packageType, (float)(1 - override.charge) * rv.charge);
								result *= (1 - discount.Charge);
								Logging.WriteLogLine("Discount is a percentage: " + discount.Charge + " Result before: " + before + " - after: " + result);
							}
						}
					}
                }
            }
            //else
            //{
            //    Logging.WriteLogLine("DEBUG zones are empty");
            //}
        }
        else
            Logging.WriteLogLine("ERROR TripPackage is null");

        return result;
	}

    private static (List<OverridePackage>, List<RedirectPackage>) GetOverridesAndRedirects(TripEntryModel model)
    {
        List<OverridePackage> overrides = new();
        List<RedirectPackage> redirects = new();

        if (model != null)
        {
            Task.WaitAll(
                Task.Run(async () =>
                {
					if (Company == null || Company.Company.CompanyNumber != model.Account)
					{
						// Company has changed - Load new values
						Company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(model.Account);
						PackageOverrides = null;
						PackageRedirects = null;
					}
                    if (Company != null)
                    {
						if (PackageOverrides == null)
						{
                            PackageRedirects = await Azure.Client.RequestGetPackageRedirectsByCompanyId(Company.Company.CompanyId);
                            PackageOverrides = await Azure.Client.RequestGetPackageOverridesByCompanyId(Company.Company.CompanyId);
                        }

                        Logging.WriteLogLine("Found " + PackageOverrides?.Count + " overrides and " + PackageRedirects?.Count + " redirects");
                        if (PackageOverrides != null && PackageOverrides.Count > 0)
                        {
                            foreach (OverridePackage po in PackageOverrides)
                            {
                                overrides.Add(po);
                            }
                        }
                        if (PackageRedirects != null && PackageRedirects.Count > 0)
                        {
                            foreach (RedirectPackage pr in PackageRedirects)
                            {
                                redirects.Add(pr);
                            }
                        }
                    }
                })
            );
        }

        return (overrides, redirects);
    }


    private static PackageType? CheckForPackageOverride(PackageType package, List<OverridePackage> pos, List<PackageType> pts)
	{
		PackageType? found = null;
		if (pos != null && pos.Count > 0)
		{
			Logging.WriteLogLine("Looking for a PackageOverride for " + package.Description);
			OverridePackage po = (from P in pos where P.FromPackageTypeId == package.PackageTypeId select P).FirstOrDefault();
			if (po != null)
			{
                found = (from P in pts where P.PackageTypeId == po.ToPackageTypeId select P).FirstOrDefault();
				Logging.WriteLogLine("Found PackageOverride: " + found?.Description ?? string.Empty);
			}
		}

		return found;
	}

	/// <summary>
	/// NOTE - This is NOT commutative - fz->tz is not the same as tz->fz
	/// </summary>
	/// <param name="package"></param>
	/// <param name="serviceLevel"></param>
	/// <param name="fromZone"></param>
	/// <param name="toZone"></param>
	/// <param name="prs"></param>
	/// <returns></returns>
	private static RedirectPackage? CheckForAccountDiscount(PackageType package, ServiceLevel serviceLevel, Zone fromZone, Zone toZone, List<RedirectPackage> prs)
	{
		RedirectPackage? result = null;

		if (prs?.Count > 0)
		{
			result = (from P in prs where P.PackageTypeId == package.PackageTypeId && P.ServiceLevelId == serviceLevel.ServiceLevelId select P).FirstOrDefault();
			if (result != null)
			{
				// Now check the zones - All, All & ?, ?
				if ((result.FromZoneId == -1 && result.ToZoneId == -1) || (fromZone.ZoneId == result.FromZoneId && toZone.ZoneId == result.ToZoneId))
				{
					// Matches
				}
				else if (result.FromZoneId == -1 && toZone.ZoneId != result.ToZoneId) // All, ?
				{
					// No match
					result = null;
				}
				else if (result.ToZoneId == -1 && fromZone.ZoneId != result.FromZoneId) // ?, All
				{
                    // No match
                    result = null;
				}
				else
				{
					result = null;
				}
			}
		}

		return result;
	}

	public static (decimal result, Charge? matched) CheckForApplicableSurcharge(string package, string fromZone, string toZone, List<Charge> surcharges)
	{
		decimal result = 0;
		Charge? matched = null;

		foreach (Charge charge in surcharges)
		{
			(List<string> packageTypes, List<string> zones, decimal value) = ParseSurchargeFormula(charge.Formula);
			if (packageTypes.Contains(package) && (zones.Contains(fromZone) || zones.Contains(toZone)))
			{
				Logging.WriteLogLine($"Found surcharge {charge.Formula} that matches {package} and {fromZone} or {toZone}");
				result += value;
				matched = charge;
				break;
			}
		}

		return (result, matched);
	}

	private static (List<string> packageTypes, List<string> zones, decimal surchargeValue) ParseSurchargeFormula(string formula)
	{
        List<string> packageTypes = new();
        List<string> zones = new();
		decimal surchargeValue = 0;

        string[] pieces = formula.Split('(');
        if (pieces.Length == 3)
        {
            // 0 If PackageType in
            // 1 CORPORTE,Corp25) and Zone in
            // 2 2B,4B) add $10.00 to Rate
            string[] pts = pieces[1].Split(')');
            // 0 CORPORTE,Corp25
            // 1 and Zone in
            if (pts.Length == 2)
            {
                pts = pts[0].Split(',');
                // This will result in 1 or more 
                if (pts.Length == 1 && pts[0].Trim().Length > 0)
                {
                    packageTypes.Add(pts[0].Trim());
                }
                else if (pts.Length > 1)
                {
                    foreach (string pt in pts)
                    {
                        packageTypes.Add(pt.Trim());
                    }
                }

                pts = pieces[2].Split(')');
                // 0 2B,4B
                // 1  add $10.00 to Rate
                string valuePart = pts[1].Trim();
                pts = pts[0].Split(',');
                // This will result in 1 or more 
                if (pts.Length == 1 && pts[0].Trim().Length > 0)
                {
                    zones.Add(pts[0].Trim());
                }
                else if (pts.Length > 1)
                {
                    foreach (string pt in pts)
                    {
                        zones.Add(pt.Trim());
                    }
                }

                //  add $10.00 to Rate
                pts = valuePart.Split(' ');
                string valueString = pts[1].Substring(1); // Lop off the leading $
                bool success = decimal.TryParse(valueString, out decimal value);
                surchargeValue = success ? value : 0;
            }
            else
            {
                Logging.WriteLogLine($"ERROR Can't split {pieces[1]} successfully.");
            }
        }
        else
        {
            Logging.WriteLogLine($"ERROR Can't split {formula} successfully.");
        }

		return (packageTypes, zones, surchargeValue);
    }

    #endregion
}