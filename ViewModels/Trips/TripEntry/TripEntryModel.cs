﻿#nullable enable

using System.Collections;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml;
using Protocol.Data._Customers.Pml;
using ViewModels.Customers;
using ViewModels.Dialogues;
using ViewModels.RateWizard;
using ViewModels.Trips.Boards.Common;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;
using static ViewModels.Reporting.DataSets;
using File = System.IO.File;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using Selector = System.Windows.Controls.Primitives.Selector;
using ServiceLevel = Protocol.Data.ServiceLevel;

// ReSharper disable AccessToModifiedClosure

namespace ViewModels.Trips.TripEntry;

public static class Extensions
{
	public static string FixZone( this string? zone )
	{
		if( zone is not null )
		{
			var Builder = new StringBuilder();

			foreach( var C in zone )
			{
				if( C is (>= '0' and <= '9') or (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or ' ' )
					Builder.Append( C );
				else
					break;
			}
			return Builder.ToString().Trim();
		}
		return "";
	}
}

public class TripEntryModel : ViewModelBase
{
	private readonly CancellationTokenSource CancellationToken = new();

	public void OnDispose()
	{
		CancellationToken.Cancel();
	}

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public string GlobalAddressBookId = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;


	public GlobalAddressesModel GlobalAddressesModel
	{
		get => Get(() => GlobalAddressesModel, GlobalAddressesModel.GetInstance(this));
	}

	//~TripEntryModel()
	//{
	//	Worker.DoWork             -= Worker_DoWorkLoadAddressesForCompanies;
	//	Worker.RunWorkerCompleted -= Worker_Completed;
	//}

	protected override async void OnInitialised()
	{
		try
		{
			base.OnInitialised();
			Loaded = false;
            Guid tmp = Guid.NewGuid();
            GuidConverter gc = new();
            GUID = gc.ConvertToInvariantString(tmp);
            Logging.WriteLogLine( "Starting..." );

			if( !IsInDesignMode )
			{
                CallTakerId = Globals.DataContext.MainDataContext.UserName;
				//Countries = CountriesRegions.Countries;

				LoadCachedCompanyData();
				EnsureResellerAccountExistsForGlobalAddressBook();

				//GlobalAddressModel gam = GlobalAddressModel.Instance;

				//Logging.WriteLogLine("DEBUG Loading data"),
				await Task.WhenAll(
								   GetPreferences(),
								   GetSurcharges()
								  );

				await Task.WhenAll( // GetPreferences(),
								   GetServiceLevels(),
								   GetPackageTypes(),
								   GetCharges(),
								   GetInventory(),
								   GetDrivers(),
								   GetZoneNames(),
								   GetBingMapApiKey(),
								   GetRateMatrixItems(),
								   GetRoutes()//,
											  //LoadGlobalAddressBook()
								  );

				//Logging.WriteLogLine("DEBUG Done");
				//Logging.WriteLogLine("DEBUG Firing changes");
				WhenAddressNotesChange();
				WhenIsEditAddressButtonEnabledChanges();
				EnsurePredatesUserExistsIfEnabled();

				// Try this first
				//LookForCompanyDataInOtherTabs();

				//if( ( DictCompanySummaryListsByAccountId.Count == 0 ) || ( DictCompanyDetailListsByAccountId.Count == 0 ) )
				//{
				//	// Try cache
				//	//LoadCachedCompanyAddressData();
				//	// Run to ensure that latest data are loaded
				//	//Worker.DoWork             += Worker_DoWorkLoadAddressesForCompanies;
				//	//Worker.RunWorkerCompleted += Worker_Completed;
				//	//Worker.RunWorkerAsync();
				//}

				DictCompanySummaryListsByAccountId = GlobalAddressesModel.GetInstance(this).DictCompanySummaryListsByAccountId;
				DictCompanyDetailListsByAccountId  = GlobalAddressesModel.GetInstance(this).DictCompanyDetailListsByAccountId;

				//Logging.WriteLogLine("DEBUG Done");

				CompanyNames = GlobalAddressesModel.CompanyNames;
				Companies = GlobalAddressesModel.Companies;
/*				// If there aren't any locally cached companies, wait for load
				if( Companies.Count == 0 )
					await GetCompanyNames();
				else
				{
					//_ = GetCompanyNames();
					// ReSharper disable once MethodHasAsyncOverload
					Tasks.RunVoid( GetCompaniesAndUpdateCurrentCollectionAsync );
				}
*/

				LoadGlobalAddressBook();

				Dispatcher.Invoke( () =>
								   {
									   //View?.Clear();
									   //Logging.WriteLogLine("DEBUG");
									   //Execute_ClearTripEntry();
									   //TripPackages.Clear();
									   //TripPackages.Add(new DisplayPackage(new TripPackage
									   //{
									   // Pieces = 1,
									   // Weight = 1
									   //}));
									   //WhenTripPackagesChanges();

									   if( View != null )
									   {
										   View.dtpCallDate.IsEnabled = View.tpCallTime.IsEnabled = IsCallTimeEnabled;
										   View.cbDrivers.IsEnabled   = IsDispatchToDriverEnabled;

										   View.tbCustomerPhone.Focus();
										   View.cbAccount.Focus();

										   // IMPORTANT - required or else there won't be a package selection or total row until 'New' is clicked
										   /*var Rd = new RowDefinition
									   {
										   Height = View.gridTripItems.RowDefinitions[0].Height
									   };
									   View.gridTripItems.RowDefinitions.Add(Rd);
									   View.BuildNewPackageTypeRow(this, 0, null, true);

									   View.BuildNewPackageTotalRow();
									   */
										   /*TripPackage tp = new()
									   {
										   Pieces = 1,
										   Weight = 1,
										   PackageType = GetPackageDefault()
									   };
									   DisplayPackage dp = new(tp);
									   TripPackages.Add(dp);
									   WhenTripPackagesChanges();
									   */
										   //View.BuildPackageRows(this);

										   //_ = Execute_NewTrip(false, true);
									   }
									   Loaded = true;
									   Logging.WriteLogLine( "Done" );
								   } );
			}
		}
		catch // Could be closing tab while initialising
		{
		}
	}

	public                 Action?          OnCannotEditDeleted;
	public                 Action<string>?  OnInvalidTripId;
	public static readonly string           PROGRAM = "Shipment Entry (" + Globals.CurrentVersion.APP_VERSION + ")";
	private                DispatcherTimer? UpdateAddressesTimer;
	private                DispatcherTimer? UpdateCompanyDataTimer;

	private DispatcherTimer? UpdateGlobalAddressesTimer;

	public readonly string QUOTES_CHARGE_AND_PACKAGETYPE_ID = "QUOTES"; // Used for Phoenix to override rate matrix

    public TripEntry? View;

	public override async void OnArgumentChange( object? selectedTrip )
	{
		await GetPreferences();

		if( selectedTrip is DisplayTrip Trip )
		{
			Dispatcher.Invoke( () =>
							   {
								   TripPackages.Clear();
								   View?.RemovePackageRows();
								   Execute_ClearTripEntry();
								   //View?.Clear();

								   SelectedAccount = Trip.AccountId;
								   //Customer = Trip.BillingCompanyName;
								   PickupCountry   = Trip.PickupAddressCountry;
								   DeliveryCountry = Trip.DeliveryAddressCountry;
							   } );

			CurrentTrip = Trip;
			//await WhenCurrentTripChanges(false);
			await WhenCurrentTripChanges();

			Guid tmp = Guid.NewGuid();
			GuidConverter gc = new();
			GUID = gc.ConvertToInvariantString( tmp );
		}
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

#region Data
	// Used to hold these details to prevent having to load everything again.
	// Required for PML because all addresses are in the PMLOz account, not the global address book.
	public Dictionary<string, CompanySummaryList> DictCompanySummaryListsByAccountId = new();
	public Dictionary<string, CompanyDetailList>  DictCompanyDetailListsByAccountId  = new();

	public string? GUID { get; set; }

	private void LookForCompanyDataInOtherTabs()
	{
		var tis = Globals.DataContext.MainDataContext.ProgramTabItems;

		//for (var i = tis.Count - 1; i >= 0; i--)
		var found = false;

		foreach( var ti in tis )
		{
			if( ti.Content is TripEntry {DataContext: TripEntryModel tem} )
			{
				if( tem.DictCompanySummaryListsByAccountId.Count > 0 )
				{
					Logging.WriteLogLine( "Found company summary list in another tab" );
					DictCompanySummaryListsByAccountId = tem.DictCompanySummaryListsByAccountId;
					found                              = true;
				}

				if( tem.DictCompanyDetailListsByAccountId.Count > 0 )
				{
					Logging.WriteLogLine( "Found company detail list in another tab" );
					DictCompanyDetailListsByAccountId = tem.DictCompanyDetailListsByAccountId;
					found                             = true;
				}
			}

			if( found )
				break;
		}
	}


	public IList Countries
	{
		get => CountriesRegions.Countries;
		set { Set( () => Countries, value ); }
	}


	public bool StartNewTrip
	{
		get { return Get( () => StartNewTrip, false ); }
		set { Set( () => StartNewTrip, value ); }
	}


	public bool IsTripIdNotEditable
	{
		//get { return Get( () => IsTripIdNotEditable, CurrentTrip != null ); }
		get { return Get( () => IsTripIdNotEditable, true ); } // There are no situations where it should be editable
		set { Set( () => IsTripIdNotEditable, value ); }
	}

	public bool TextBoxesEditable
	{
		//get { return Get(() => TextBoxesEditable, false); }
		get { return Get( () => TextBoxesEditable, true ); }
		set { Set( () => TextBoxesEditable, value ); }
	}


	public bool TextBoxesReadonly
	{
		get { return Get( () => TextBoxesReadonly, false ); }
		set { Set( () => TextBoxesReadonly, value ); }
	}


	public bool WidgetEnabled
	{
		get { return Get( () => WidgetEnabled, false ); }
		set { Set( () => WidgetEnabled, value ); }
	}


	public DisplayTrip? CurrentTrip
	{
		get { return Get( () => CurrentTrip, new DisplayTrip() ); }
		set { Set( () => CurrentTrip, value ); }

		//set
		//         {
		//	if (value != null)
		//             {
		//		Logging.WriteLogLine("Setting PreviousTrip to CurrentTrip " + CurrentTrip.TripId);
		//		PreviousTrip = CurrentTrip;
		//             }
		//	CurrentTrip = value;
		//         }
	}


	public DisplayTrip? PreviousTrip
	{
		get { return Get( () => PreviousTrip, new DisplayTrip() ); }
		set { Set( () => PreviousTrip, value ); }
	}


	// ViewModels.Trips.Boards.Common.DisplayTrip
	public ObservableCollection<DisplayPackage> TripPackages
	{
		get { return Get( () => TripPackages, new ObservableCollection<DisplayPackage>() ); }
		set { Set( () => TripPackages, value ); }
	}

	[DependsUpon( nameof( TripPackages ) )]
	public void WhenTripPackagesChanges()
	{
		Logging.WriteLogLine( "DEBUG TripPackages changed - TripPackages.Count: " + TripPackages.Count() );

		if( View != null )
		{
			Dispatcher.Invoke( () =>
							   {
								   View.RemovePackageRows();
								   View.BuildPackageRows( this );
							   } );
		}
	}


	public decimal TotalTripRates
	{
		get { return Get( () => TotalTripRates, 0 ); }
		set { Set( () => TotalTripRates, value ); }
	}


	//public List<TripPackage> TripPackages
	//{
	//	get { return Get( () => TripPackages, new List<TripPackage>() ); }
	//	set { Set( () => TripPackages, value ); }
	//}

	/// <summary>
	///     Holds the inventory for each line item.
	///     The key is the row number.
	///     Note - they will be off by 1 as the first is for the
	///     first line in the package list.
	/// </summary>
	public SortedDictionary<int, List<ExtendedInventory>> TripItemsInventory = new();

	public void AddUpdateInventoryToPackageRow( int row, List<ExtendedInventory> eis )
	{
		if( row > -1 )
		{
			foreach( var Ei in eis )
				Ei.IsSelected = false; // Clear the flags

			if( TripItemsInventory.ContainsKey( row ) )
				TripItemsInventory[ row ] = eis;
			else
				TripItemsInventory.Add( row, eis );
		}
	}


	public List<ExtendedInventory> SelectedInventoryForTripItem
	{
		get { return Get( () => SelectedInventoryForTripItem, new List<ExtendedInventory>() ); }
		set { Set( () => SelectedInventoryForTripItem, value ); }
	}

	public void LoadInventoryForTripItem( int row )
	{
		//Logging.WriteLogLine( "Loading inventory for row: " + row );

		//Dispatcher.Invoke(() =>
		//{
		//    if (TripItemsInventory.ContainsKey(row))
		//    {
		//        SelectedInventoryForTripItem = TripItemsInventory[row];
		//    }
		//    else
		//    {
		//        SelectedInventoryForTripItem.Clear();
		//    }
		//});
		if( TripItemsInventory.ContainsKey( row ) )
			SelectedInventoryForTripItem = TripItemsInventory[ row ];
		else
		{
			var List = new List<ExtendedInventory>();
			SelectedInventoryForTripItem = List;
		}
	}

	public void RemoveRowFromTripItemsInventory( int rowToRemove )
	{
		var NewDict = new SortedDictionary<int, List<ExtendedInventory>>();

		for( var I = 1; I <= TripItemsInventory.Count; I++ )
		{
			var Payload = TripItemsInventory[ I ];

			if( I < rowToRemove )
				NewDict.Add( I, Payload );

			else if( I == rowToRemove )
				continue;

			if( I > rowToRemove )
				NewDict.Add( I - 1, Payload );
		}

		TripItemsInventory = NewDict;

		SelectedInventoryForTripItem = new List<ExtendedInventory>();
	}


	public decimal TotalWeight
	{
		get { return Get( () => TotalWeight, 0 ); }
		set { Set( () => TotalWeight, value ); }
	}


	public int TotalPieces
	{
		get { return Get( () => TotalPieces, 0 ); }
		set { Set( () => TotalPieces, value ); }
	}


	public decimal TotalOriginal
	{
		get { return Get( () => TotalOriginal, 0 ); }
		set { Set( () => TotalOriginal, value ); }
	}


	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	[DependsUpon350( nameof( TripId ) )]
	public async void WhenTripIdChanges()
	{
		if( !IsInDesignMode && !IsTripIdNotEditable )
		{
			var Tid = TripId;

			if( Tid.IsNotNullOrWhiteSpace() )
			{
				if( !await Azure.Client.RequestIsTripIdValid( Tid ) )
					OnInvalidTripId?.Invoke( Tid );
			}
		}
	}


	public int Status1
	{
		get { return Get( () => Status1, 0 ); }
		set { Set( () => Status1, value ); }
	}


	public string DriverCode
	{
		get { return Get( () => DriverCode, "" ); }
		set { Set( () => DriverCode, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public string TPU
	{
		get { return Get( () => TPU, "" ); }
		set { Set( () => TPU, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public string TDEL
	{
		get { return Get( () => TDEL, "" ); }
		set { Set( () => TDEL, value ); }
	}

	// ReSharper disable once InconsistentNaming
	public string TVER
	{
		get { return Get( () => TVER, "" ); }
		set { Set( () => TVER, value ); }
	}

	public string CallTakerId
	{
		get { return Get( () => CallTakerId, "" ); }
		set { Set( () => CallTakerId, value ); }
	}


	public string CallName
	{
		get { return Get( () => CallName, "" ); }
		set { Set( () => CallName, value ); }
	}


	public string CallPhone
	{
		get { return Get( () => CallPhone, "" ); }
		set { Set( () => CallPhone, value ); }
	}


	public string CallEmail
	{
		get { return Get( () => CallEmail, "" ); }
		set { Set( () => CallEmail, value ); }
	}


	//public DateTimeOffset CallDate
	//{
	//	get { return Get( () => CallDate, DateTime.Now ); }
	//	set { Set( () => CallDate, value ); }
	//}


	public DateTime CallTime
	{
		get { return Get( () => CallTime, DateTime.Now ); }
		set { Set( () => CallTime, value ); }
	}

	[DependsUpon( nameof( CallTime ) )]
	public void WhenCallTimeChanges()
	{
		//Logging.WriteLogLine( "CallTime: " + CallTime );
		var Tmp = new DateTime( CallDate.Year, CallDate.Month, CallDate.Day, CallTime.Hour, CallTime.Minute, CallTime.Second, CallTime.Millisecond );
		CallTime = Tmp;
	}

	public DateTime CallDate
	{
		get { return Get( () => CallDate, DateTime.Now ); }
		set { Set( () => CallDate, value ); }
	}

	[DependsUpon( nameof( CallDate ) )]
	public void WhenCallDateChanges()
	{
		//Logging.WriteLogLine( "CallDate: " + CallDate );
		var Tmp = new DateTime( CallDate.Year, CallDate.Month, CallDate.Day, CallTime.Hour, CallTime.Minute, CallTime.Second, CallTime.Millisecond );
		CallTime = Tmp;
	}


	//public DateTime CallTime
	//{
	//    get { return Get(() => CallTime, DateTime.Now); }
	//    set { Set(() => CallTime, value); }
	//}


	public bool IsQuote
	{
		get { return Get( () => IsQuote, false ); }
		set { Set( () => IsQuote, value ); }
	}


	public string UndeliverableNotes
	{
		get { return Get( () => UndeliverableNotes, "" ); }
		set { Set( () => UndeliverableNotes, value ); }
	}


	public string Account
	{
		get { return Get( () => Account, "" ); }
		set { Set( () => Account, value ); }
	}

	public KeyValuePair<string, CompanyAddress> RawCustomer
	{
		get { return Get( () => RawCustomer, new KeyValuePair<string, CompanyAddress>() ); }
		set { Set( () => RawCustomer, value ); }
	}

	/// <summary>
	///     Need to do this as RawCustomer is a KeyValuePair.
	/// </summary>
	[DependsUpon( nameof( RawCustomer ) )]
	public void WhenRawCustomerChanges()
	{
		Logging.WriteLogLine( "DEBUG RawCustomer: " + RawCustomer.Key );
		//Customer = RawCustomer.Key;
		//WhenCustomerChanges();
		if (!IsCustomerDropdownOpen && !IsSelectedAccountDropDownOpen)
		{
			Customer = RawCustomer.Key;
			//WhenCustomerChanges();
		}
	}

	public string Customer
	{
		get { return Get( () => Customer, "" ); }
		set { Set( () => Customer, value ); }
	}

	public bool IsCustomerDropdownOpen
	{
		get { return Get( () => IsCustomerDropdownOpen, false ); }
		set { Set( () => IsCustomerDropdownOpen, value ); }
	}

	[DependsUpon(nameof(IsCustomerDropdownOpen))]
	public void WhenIsCustomerDropdownOpenChanges()
	{
		//Logging.WriteLogLine($"DEBUG IsCustomerDropdownOpen: {IsCustomerDropdownOpen}");
		//if( !IsCustomerDropdownOpen && Customer.IsNotNullOrWhiteSpace() )
		if( !IsCustomerDropdownOpen )
		{
			Customer = RawCustomer.Key;
			WhenCustomerChanges();
		}
	}

	//[DependsUpon500( nameof( Customer ) )]
	public void WhenCustomerChanges()
	{
		Logging.WriteLogLine( "DEBUG Customer: " + Customer + ", SelectedAccount: " + SelectedAccount );

		if( Customer.IsNotNullOrWhiteSpace() )
		{
			if( CompanyFullNames.ContainsKey( Customer ) )
			{
				Logging.WriteLogLine( "DEBUG CompanyFullNames[Customer].CompanyNumber: " + CompanyFullNames[ Customer ].CompanyNumber );
				SelectedAccount = CompanyFullNames[ Customer ].CompanyNumber;

				Task.Run( WhenAccountChanges );
			}
		}
	}

	public CompanyAddress? CompanyAddress;


	public string Reference
	{
		get { return Get( () => Reference, "" ); }
		set { Set( () => Reference, value ); }
	}


	public string PickupCompany
	{
		get { return Get( () => PickupCompany, "" ); }
		set { Set( () => PickupCompany, value ); }
	}

	public bool ArePickupAddressFieldsReadOnly
	{
		get => Get( () => ArePickupAddressFieldsReadOnly, false );
		set => Set( () => ArePickupAddressFieldsReadOnly, value );
	}

	public string PickupName
	{
		get { return Get( () => PickupName, "" ); }
		set { Set( () => PickupName, value ); }
	}


	public string PickupPhone
	{
		get { return Get( () => PickupPhone, "" ); }
		set { Set( () => PickupPhone, value ); }
	}


	public string PickupLocationBarcode
	{
		get { return Get( () => PickupLocationBarcode, "" ); }
		set { Set( () => PickupLocationBarcode, value ); }
	}


	public bool PickupDefault
	{
		get { return Get( () => PickupDefault, false ); }
		set { Set( () => PickupDefault, value ); }
	}


	public string PickupSuite
	{
		get { return Get( () => PickupSuite, "" ); }
		set { Set( () => PickupSuite, value ); }
	}


	public string PickupStreet
	{
		get { return Get( () => PickupStreet, "" ); }
		set { Set( () => PickupStreet, value ); }
	}


	public string PickupCity
	{
		get { return Get( () => PickupCity, "" ); }
		set { Set( () => PickupCity, value ); }
	}

	public string PickupLocation
	{
		get { return Get( () => PickupLocation, "" ); }
		set { Set( () => PickupLocation, value ); }
	}


	public string PickupProvState
	{
		get { return Get( () => PickupProvState, "" ); }
		set { Set( () => PickupProvState, value ); }
	}

	public string PrevPickupProvState
	{
		get { return Get( () => PrevPickupProvState, PickupProvState ); }
		set { Set( () => PrevPickupProvState, value ); }
	}

	[DependsUpon( nameof( PickupProvState ) )]
	public void WhenPickupProvStateChanges()
	{
		var NewProvState = PickupProvState;

		if( NewProvState != PickupProvState )
		{
			var Tmp = string.Empty;

			foreach( string Name in PickupRegions )
			{
				if( string.Equals( Name, PickupProvState, StringComparison.CurrentCultureIgnoreCase ) )
				{
					Tmp = Name;
					break;
				}
			}

			//if( ( PickupRegions != null ) && !PickupRegions.Contains( PickupProvState ) )
			if( Tmp.IsNullOrWhiteSpace() )
			{
				// Problem - abbreviation rather than name
				var Name = CountriesRegions.FindRegionForAbbreviationAndCountry( PickupProvState, PickupCountry );

				if( Name.IsNotNullOrWhiteSpace() )
				{
					Logging.WriteLogLine( "PickupProvState: was abbreviation: " + NewProvState + ", now: " + Name );
					PickupProvState = Name;
				}
			}
			else
				PickupProvState = Tmp;
			PrevPickupProvState = PickupProvState;
		}
	}

	public string PickupCountry
	{
		get { return Get( () => PickupCountry, "" ); }
		set { Set( () => PickupCountry, value ); }
	}


	public IList PickupRegions
	{
		get { return Get( () => PickupRegions, new List<object>() ); }
		set { Set( () => PickupRegions, value ); }
	}

	//[DependsUpon(nameof(PickupCountry))]
	public void WhenPickupCountryChanges()
	{
		//Logging.WriteLogLine( "PickupCountry: " + PickupCountry );
		if( PickupCountry.IsNotNullOrWhiteSpace() )
		{
			var Regions = new List<string>();

			switch( PickupCountry?.ToLower() ?? "" )
			{
			case "australia":
				Regions = CountriesRegions.AustraliaRegions;
				break;

			case "canada":
				Regions = CountriesRegions.CanadaRegions;
				break;

			case "united states":
				Regions = CountriesRegions.USRegions;
				break;

			default:
				Logging.WriteLogLine( "ERROR - can't find " + PickupCountry );
				break;
			}

			PickupRegions = Regions;
		}
	}


	public string PickupPostalZip
	{
		get { return Get( () => PickupPostalZip, "" ); }
		set { Set( () => PickupPostalZip, value ); }
	}

	[DependsUpon( nameof( PickupPostalZip ) )]
	public void WhenPickupPostalZipChanges()
	{
		//if (PickupPostalZip.IsNotNullOrWhiteSpace() && IsNewTrip && PickupZone.IsNullOrWhiteSpace())
		if( PickupPostalZip.IsNotNullOrWhiteSpace() && IsNewTrip )
		{
			// Look up postal code from Routes to find a zone
			// If no match, clear
			Dispatcher.Invoke( () =>
							   {
								   PickupZone = GetZoneForPostalCode( PickupPostalZip );
							   } );
		}
	}

	public string PickupZone
	{
		get { return Get( () => PickupZone, "" ); }
		set { Set( () => PickupZone, value ); }
	}

	public string PickupAddressNotes
	{
		get { return Get( () => PickupAddressNotes, "" ); }
		set { Set( () => PickupAddressNotes, value ); }
	}

	[DependsUpon( nameof( PickupAddressNotes ) )]
	[DependsUpon( nameof( DeliveryAddressNotes ) )]
	public void WhenAddressNotesChange()
	{
		var result = false;

		if( Prefs?.Count > 0 )
		{
			var pref = ( from P in Prefs
					     where P.Description == "Trip Entry: Lock Address Notes"
					     select P ).FirstOrDefault();

			if( pref is {Enabled: true} )
				//Logging.WriteLogLine( "Found preference: " + pref.Description );
				result = true;
		}

		IsAddressNotesReadOnly = result;
	}

	public bool IsEditAddressButtonEnabled
	{
		get => Get( () => IsAddressNotesReadOnly, true );
		set => Set( () => IsAddressNotesReadOnly, value );
	}

	[DependsUpon( nameof( IsEditAddressButtonEnabled ) )]
	public void WhenIsEditAddressButtonEnabledChanges()
	{
		var result = true;

		if( Prefs?.Count > 0 )
		{
			var pref = ( from P in Prefs
					     where P.Description.Contains( "Lock Edit Address Buttons" )
					     select P ).FirstOrDefault();

			if( pref is {Enabled: true} )
				result = false;
		}
		IsEditAddressButtonEnabled = result;

		// KLUDGE 
		if( View != null )
		{
			View.BtnEditAddressPickup.IsEnabled   = result;
			View.BtnEditAddressDelivery.IsEnabled = result;
		}
	}

	public bool IsCallTimeEnabled
	{
		get { return Get( () => IsCallTimeEnabled, !IsCallTimeLocked ); }
		//set { Set(() => IsCallTimeEnabled, value ); }
	}

	public bool IsCallTimeLocked
	{
		//get { return Get( () => IsCallTimeLocked, false ); }
		get
		{
			var yes = false;

			if( Prefs?.Count > 0 )
			{
				//Preference? pref = (from P in Prefs where P.Description.Contains("Block Calltime Editing (Clones get current time)") select P).FirstOrDefault();
				var pref = ( from P in Prefs
						     where P.Description == "Trip Entry: Block Calltime Editing (Clones get current time)"
						     select P ).FirstOrDefault();

				if( pref is {Enabled: true} )
				{
					//Logging.WriteLogLine("DEBUG CallTime is locked");
					yes = true;
				}
				//else
				//               {
				//                   Logging.WriteLogLine("DEBUG CallTime is unlocked");
				//               }
			}

			return yes;
		}
		//set { IsCallTimeLocked = value; }
	}

	public bool IsDispatchToDriverEnabled
	{
		get { return Get( () => IsDispatchToDriverEnabled, !IsDispatchToDriverLocked ); }
	}

	public bool IsDispatchToDriverLocked
	{
		get
		{
			var yes = false;

			if( Prefs?.Count > 0 )
			{
				var pref = ( from P in Prefs
						     where P.Description.Contains( "Lock Dispatch to Driver" )
						     select P ).FirstOrDefault();

				if( pref is {Enabled: true} )
				{
					//Logging.WriteLogLine("DEBUG DispatchToDriver is locked");
					yes = true;
				}
				//else
				//{
				//    Logging.WriteLogLine("DEBUG DispatchToDriver is unlocked");
				//}
			}

			return yes;
		}
	}

	public bool IsAddressNotesReadOnly
	{
		get { return Get( () => IsAddressNotesReadOnly, false ); }
		set { Set( () => IsAddressNotesReadOnly, value ); }
	}


	public string PickupNotes
	{
		get { return Get( () => PickupNotes, "" ); }
		set { Set( () => PickupNotes, value ); }
	}

	//
	//
	//


	public string DeliveryCompany
	{
		get { return Get( () => DeliveryCompany, "" ); }
		set { Set( () => DeliveryCompany, value ); }
	}

	public bool AreDeliveryAddressFieldsReadOnly
	{
		get => Get( () => AreDeliveryAddressFieldsReadOnly, false );
		set => Set( () => AreDeliveryAddressFieldsReadOnly, value );
	}

	public string DeliveryName
	{
		get { return Get( () => DeliveryName, "" ); }
		set { Set( () => DeliveryName, value ); }
	}


	public string DeliveryPhone
	{
		get { return Get( () => DeliveryPhone, "" ); }
		set { Set( () => DeliveryPhone, value ); }
	}


	public string DeliveryLocationBarcode
	{
		get { return Get( () => DeliveryLocationBarcode, "" ); }
		set { Set( () => DeliveryLocationBarcode, value ); }
	}


	public bool DeliveryDefault
	{
		get { return Get( () => DeliveryDefault, false ); }
		set { Set( () => DeliveryDefault, value ); }
	}


	public string DeliverySuite
	{
		get { return Get( () => DeliverySuite, "" ); }
		set { Set( () => DeliverySuite, value ); }
	}


	public string DeliveryStreet
	{
		get { return Get( () => DeliveryStreet, "" ); }
		set { Set( () => DeliveryStreet, value ); }
	}


	public string DeliveryCity
	{
		get { return Get( () => DeliveryCity, "" ); }
		set { Set( () => DeliveryCity, value ); }
	}

	public string DeliveryLocation
	{
		get { return Get( () => DeliveryLocation, "" ); }
		set { Set( () => DeliveryLocation, value ); }
	}


	public string DeliveryProvState
	{
		get { return Get( () => DeliveryProvState, "" ); }
		set { Set( () => DeliveryProvState, value ); }
	}

	[DependsUpon( nameof( DeliveryProvState ) )]
	public void WhenDeliveryProvStateChanges()
	{
		var NewProvState = DeliveryProvState;

		if( DeliveryRegions is not null && !DeliveryRegions.Contains( DeliveryProvState ) )
		{
			// Problem - abbreviation rather than name
			var Name = CountriesRegions.FindRegionForAbbreviationAndCountry( DeliveryProvState, DeliveryCountry );

			if( Name.IsNotNullOrWhiteSpace() )
			{
				Logging.WriteLogLine( "DeliveryProvState: was abbreviation: " + NewProvState + ", now: " + Name );
				DeliveryProvState = Name;
			}
		}
	}

	public string DeliveryCountry
	{
		get { return Get( () => DeliveryCountry, "" ); }
		set { Set( () => DeliveryCountry, value ); }
	}


	public IList? DeliveryRegions
	{
		get { return Get( () => DeliveryRegions, new List<object>() ); }
		set { Set( () => DeliveryRegions, value ); }
	}

	//[DependsUpon(nameof(DeliveryCountry))]
	public void WhenDeliveryCountryChanges()
	{
		//Logging.WriteLogLine( "DeliveryCountry: " + DeliveryCountry );
		if( DeliveryCountry.IsNotNullOrWhiteSpace() )
		{
			DeliveryRegions = ( DeliveryCountry?.ToLower() ?? "" ) switch
							  {
								  "australia"     => CountriesRegions.AustraliaRegions,
								  "canada"        => CountriesRegions.CanadaRegions,
								  "united states" => CountriesRegions.USRegions,
								  _               => null
							  };
		}
	}


	public string DeliveryPostalZip
	{
		get { return Get( () => DeliveryPostalZip, "" ); }
		set { Set( () => DeliveryPostalZip, value ); }
	}


	public string DeliveryZone
	{
		get { return Get( () => DeliveryZone, "" ); }
		set { Set( () => DeliveryZone, value ); }
	}

	[DependsUpon( nameof( DeliveryPostalZip ) )]
	public void WhenDeliveryPostalZipChanges()
	{
		//if (DeliveryPostalZip.IsNotNullOrWhiteSpace() && IsNewTrip && DeliveryZone.IsNullOrWhiteSpace())
		if( DeliveryPostalZip.IsNotNullOrWhiteSpace() && IsNewTrip )
		{
			// Look up postal code from Routes to find a zone
			// If no match, clear
			Dispatcher.Invoke( () =>
							   {
								   DeliveryZone = GetZoneForPostalCode( DeliveryPostalZip );
							   } );
		}
	}

	public string DeliveryAddressNotes
	{
		get { return Get( () => DeliveryAddressNotes, "" ); }
		set { Set( () => DeliveryAddressNotes, value ); }
	}


	public string DeliveryNotes
	{
		get { return Get( () => DeliveryNotes, "" ); }
		set { Set( () => DeliveryNotes, value ); }
	}

	//[DependsUpon( nameof( DeliveryNotes ) )]
	public void WhenDeliveryNotesChanges()
	{
		Logging.WriteLogLine( "DEBUG DeliveryNotes: " + DeliveryNotes );
	}

	public string DriverNotes
	{
		get { return Get( () => DriverNotes, "" ); }
		set { Set( () => DriverNotes, value ); }
	}

	[DependsUpon( nameof( DriverNotes ) )]
	public void WhenDriverNotesChanges()
	{
		Logging.WriteLogLine( "DEBUG DriverNotes: " + DriverNotes );
	}

	public string PackageType
	{
		get { return Get( () => PackageType, "" ); }
		set { Set( () => PackageType, value ); }
	}


	//public decimal Weight
	//{
	//	get { return Get( () => Weight, 1 ); }
	//	set { Set( () => Weight, value ); }
	//}


	//public int Pieces
	//{
	//	get { return Get( () => Pieces, 1 ); }
	//	set { Set( () => Pieces, value ); }
	//}


	// ReSharper disable once InconsistentNaming
	public bool IsDIM
	{
		get { return Get( () => IsDIM, false ); }
		set { Set( () => IsDIM, value ); }
	}


	//public decimal Length
	//{
	//	get { return Get( () => Length, 0 ); }
	//	set { Set( () => Length, value ); }
	//}


	//public decimal Width
	//{
	//	get { return Get( () => Width, 0 ); }
	//	set { Set( () => Width, value ); }
	//}


	//public decimal Height
	//{
	//	get { return Get( () => Height, 0 ); }
	//	set { Set( () => Height, value ); }
	//}


	//public decimal Original
	//{
	//	get { return Get( () => Original, 0 ); }
	//	set { Set( () => Original, value ); }
	//}


	//public decimal QH
	//{
	//	get { return Get( () => QH, 0 ); }
	//	set { Set( () => QH, value ); }
	//}


	public string ServiceLevel
	{
		get { return Get( () => ServiceLevel, "" ); }
		set { Set( () => ServiceLevel, value ); }
	}

	[DependsUpon( nameof( ServiceLevel ) )]
	public void WhenServiceLevelChanges()
	{
		//Logging.WriteLogLine( "DEBUG selected ServiceLevel: " + ServiceLevel );

		var sl = ( from S in CompleteServiceLevels
				   where S.OldName == ServiceLevel
				   select S ).FirstOrDefault();

		if( sl != null )
		{
			string previousPackageType = PackageType;

			Logging.WriteLogLine( "Switching to packagetypes for ServiceLevel " + ServiceLevel );

			Dispatcher.Invoke( () =>
							   {
								   if( sl.PackageTypes.Count > 0 )
								   {
									   PackageTypes.Clear();

									   foreach( var pt in sl.PackageTypes )
										   PackageTypes.Add( pt );
								   }
								   else
								   {
									   PackageTypes.Clear();

									   foreach( var pt in AllPackageTypes )
										   PackageTypes.Add( pt );
								   }

								   if( previousPackageType.IsNotNullOrWhiteSpace() && PackageTypes.Contains( previousPackageType ) )
								   {
									   PackageType = previousPackageType;
									   View?.UpdatePackageTypesInComboBoxes( previousPackageType );
								   }
								   else
								   {
									   View?.UpdatePackageTypesInComboBoxes();
								   }

								   View?.RecalculateTripItemTotals();
								   View?.BtnCalculate_Click( null, null );
							   } );
		}
	}


	public List<string> ServiceLevels
	{
		get => Get( () => ServiceLevels, new List<string>() );
		set { Set( () => ServiceLevels, value ); }
	}

	/// <summary>
	///     Holds the service levels so that the associated PackageTypes can be found.
	/// </summary>
	public readonly List<ServiceLevel> CompleteServiceLevels = new();

	/// <summary>
	/// </summary>
	/// <returns></returns>
	public async Task GetServiceLevels()
	{
		// From Terry: Need to do this to avoid bad requests in the server log
		if( !IsInDesignMode )
		{
			try
			{
				var Sls = await Azure.Client.RequestGetServiceLevelsDetailed();
				//var Sls = await Azure.Client.RequestGetServiceLevels();

				if( Sls is not null )
				{
					var Levels = ( from S in Sls
								   orderby S.SortOrder, S.OldName
								   select S.OldName ).ToList();
					//SortedDictionary<string, string> sd = new();
					//foreach (var sl in Sls)
					//                  {
					//	if (sl != null)
					//                      {
					//		sd.Add(sl, sl);
					//                      }
					//                  }
					//List<string> Levels = (from S in sd.Keys select S).ToList();

					Dispatcher.Invoke( () =>
									   {
										   ServiceLevels = Levels;
									   } );
					CompleteServiceLevels.Clear();

					var s = ( from S in Sls
							  select S ).ToList();
					CompleteServiceLevels.AddRange( s );
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Service Levels\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}

	//public DateTimeOffset ReadyDate
	//{
	//	get { return Get( () => ReadyDate, DateTimeOffset.Now ); }
	//	set { Set( () => ReadyDate, value ); }
	//}


	//public DateTimeOffset ReadyTime
	//{
	//	get { return Get( () => ReadyTime, DateTimeOffset.Now ); }
	//	set { Set( () => ReadyTime, value ); }
	//}


	//public DateTimeOffset DueDate
	//{
	//	get { return Get( () => DueDate, DateTimeOffset.Now ); }
	//	set { Set( () => DueDate, value ); }
	//}


	//public DateTimeOffset DueTime
	//{
	//	get { return Get( () => DueTime, DateTimeOffset.Now ); }
	//	set { Set( () => DueTime, value ); }
	//}

	public DateTime ReadyDate
	{
		get { return Get( () => ReadyDate, DateTime.Now ); }
		set { Set( () => ReadyDate, value ); }
	}

	[DependsUpon( nameof( ReadyDate ) )]
	public void WhenReadyDateChanges()
	{
		//Logging.WriteLogLine( "ReadyDate: " + ReadyDate );
		var Tmp = new DateTime( ReadyDate.Year, ReadyDate.Month, ReadyDate.Day, ReadyTime.Hour, ReadyTime.Minute, ReadyTime.Second, ReadyTime.Millisecond );
		ReadyDate = Tmp;
	}


	public DateTime ReadyTime
	{
		get { return Get( () => ReadyTime, DateTime.Now ); }
		set { Set( () => ReadyTime, value ); }
	}

	[DependsUpon( nameof( ReadyTime ) )]
	public void WhenReadyTimeChanges()
	{
		//Logging.WriteLogLine( "ReadyTime: " + ReadyDate );
		var Tmp = new DateTime( ReadyDate.Year, ReadyDate.Month, ReadyDate.Day, ReadyTime.Hour, ReadyTime.Minute, ReadyTime.Second, ReadyTime.Millisecond );
		ReadyTime = Tmp;
	}


	public DateTime DueDate
	{
		get { return Get( () => DueDate, DateTime.Now ); }
		set { Set( () => DueDate, value ); }
	}

	[DependsUpon( nameof( DueDate ) )]
	public void WhenDueDateChanges()
	{
		//Logging.WriteLogLine( "DueDate: " + DueDate );
		var Tmp = new DateTime( DueDate.Year, DueDate.Month, DueDate.Day, DueTime.Hour, DueTime.Minute, DueTime.Second, DueTime.Millisecond );
		DueDate = Tmp;
	}

	public DateTime DueTime
	{
		get { return Get( () => DueTime, DateTime.Now ); }
		set { Set( () => DueTime, value ); }
	}

	[DependsUpon( nameof( DueTime ) )]
	public void WhenDueTimeChanges()
	{
		//Logging.WriteLogLine( "DueTime: " + DueDate );
		var Tmp = new DateTime( DueDate.Year, DueDate.Month, DueDate.Day, DueTime.Hour, DueTime.Minute, DueTime.Second, DueTime.Millisecond );
		DueTime = Tmp;
	}


	// ReSharper disable once InconsistentNaming
	public string POD
	{
		get { return Get( () => POD, "" ); }
		set { Set( () => POD, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public string POP
	{
		get { return Get( () => POP, "" ); }
		set { Set( () => POP, value ); }
	}

	//private string HiddenCustomerPhone;
	public string CustomerPhone
	{
		get { return Get( () => CustomerPhone, "" ); }
		set { Set( () => CustomerPhone, value ); }

		// TODO
		//get { return Get(() => HiddenCustomerPhone); }
		//set
		//{
		//    Logging.WriteLogLine("Setting CustomerPhone to: " + value);
		//    HiddenCustomerPhone = value;
		//    //if (value != HiddenCustomerPhone)
		//    //{
		//    //}
		//}
	}

	//public string NewCustomerPhone{ get; set; }

	public string? CustomerNotes
	{
		get { return Get( () => CustomerNotes, "" ); }
		set { Set( () => CustomerNotes, value ); }
	}


	public string FirstName
	{
		get { return Get( () => FirstName, "" ); }
		set { Set( () => FirstName, value ); }
	}


	public string LastName
	{
		get { return Get( () => LastName, "" ); }
		set { Set( () => LastName, value ); }
	}


	public string MiddleNames
	{
		get { return Get( () => MiddleNames, "" ); }
		set { Set( () => MiddleNames, value ); }
	}

	public IList CompanyNames
	{
		get { return Get( () => CompanyNames, new List<string>() ); }
		set { Set( () => CompanyNames, value ); }
	}


	public List<CompanyAddress> Companies
	{
		get { return Get( () => Companies, new List<CompanyAddress>() ); }
		set { Set( () => Companies, value ); }
	}

	public SortedDictionary<string, CompanyAddress> CompanyFullNames
	{
		get { return Get( () => CompanyFullNames, new SortedDictionary<string, CompanyAddress>( StringComparer.OrdinalIgnoreCase ) ); }
		set { Set( () => CompanyFullNames, value ); }
	}

	public void ReloadPrimaryCompanies()
	{
		Logging.WriteLogLine( "Reloading Primary Companies" );

		if( !IsInDesignMode )
		{
			Tasks.RunVoid( async () =>
						   {
							   var Names = await Azure.Client.RequestGetCustomerCodeList();

							   if( Names is not null )
							   {
								   var CoNames = ( from N in Names
											       let Ln = N.ToLower()
											       orderby Ln
											       select Ln ).ToList();

								   var CompanyAddresses = new List<CompanyAddress>();

								   await new Tasks.Task<string>().RunAsync( CoNames, async coName =>
																			         {
																				         try
																				         {
																					         var Company = await Azure.Client.RequestGetResellerCustomerCompany( coName );

																					         lock( CompanyAddresses )
																						         CompanyAddresses.Add( Company );
																				         }
																				         catch( Exception Exception )
																				         {
																					         Logging.WriteLogLine( "Exception thrown: " + Exception );
																				         }
																			         }, 5 );

								   Dispatcher.Invoke( () =>
												      {
													      CompanyNames = CoNames;
													      Companies    = CompanyAddresses;
												      } );
							   }
						   } );
		}
	}

	// [DependsUpon(nameof(ReloadAccounts))]
	public async Task GetCompanyNames()
	{
		if( !IsInDesignMode )
		{
			try
			{
				Logging.WriteLogLine( "Loading Company Names" );
				var Names = await Azure.Client.RequestGetCustomerCodeList();

				if( Names is not null )
				{
					// Don't include the Global Address Book
					var CoNames = ( from N in Names
								    where N != GlobalAddressBookId
								    orderby N.ToLower()
								    select N ).ToList();

					var NamesToDelete = ( from N in Names
										  where N != GlobalAddressBookId
										  orderby N.ToLower()
										  select N ).ToList();

					List<CompanyAddress>? CompanyAddresses = new();
					List<string>?         EnabledNames     = new();

					var CoNamesLock = new object();

					await new Tasks.Task<string>().RunAsync( CoNames, async coName =>
																      {
																	      try
																	      {
																		      var Company = await Azure.Client.RequestGetResellerCustomerCompany( coName );

																		      lock( CoNamesLock )
																		      {
																			      if( Company?.Enabled == true )
																			      {
																				      if( coName != GlobalAddressBookId )
																				      {
																					      if( Company.CompanyNumber.IsNullOrWhiteSpace() )
																					      {
																						      //Logging.WriteLogLine( "DEBUG Setting CompanyNumber to " + coName );
																						      Company.CompanyNumber = coName;
																					      }

																					      if( !EnabledNames.Contains( coName ) )
																						      EnabledNames.Add( coName );

																					      //Logging.WriteLogLine( "DEBUG Loading Company: " + Company.CompanyName );

																					      if( !CompanyAddresses.Contains( Company ) )
																						      CompanyAddresses.Add( Company );
																				      }
																			      }
																			      //else
																			      // Logging.WriteLogLine( "DEBUG - skipping disabled account: " + coName );

																			      lock( NamesToDelete )
																			      {
																				      NamesToDelete.Remove( coName );

																				      if( NamesToDelete.Count == 0 )
																				      {
																					      // Save locally
																					      CompanyData Cd = new();

																					      Cd.CompanyAddresses.AddRange( CompanyAddresses );
																					      //Cd.EnabledNames.AddRange((List<string>)CompanyNames);
																					      Cd.EnabledNames.AddRange( EnabledNames );
																					      SaveCompanyData( Cd );
																				      }
																			      }
																		      }
																	      }
																	      catch( Exception Exception )
																	      {
																		      Logging.WriteLogLine( "Exception thrown: " + Exception );
																	      }
																      }, 5 );

					EnabledNames = ( from Tmp in EnabledNames
								     where Tmp != GlobalAddressBookId
								     orderby Tmp.ToLower()
								     select Tmp ).ToList();

					Dispatcher.Invoke( () =>
									   {
										   CompanyNames = EnabledNames;
										   Companies    = CompanyAddresses;
										   CompanyFullNames.Clear();

										   foreach( var ca in Companies )
										   {
											   if( !CompanyFullNames.ContainsKey( ca.CompanyName ) && ( ca.CompanyNumber != GlobalAddressBookId ) && ( ca.CompanyName != GlobalAddressBookId ) )
												   CompanyFullNames.Add( ca.CompanyName, ca );
										   }
									   } );
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK );
			}
		}
		Logging.WriteLogLine( "DEBUG Done" );
	}

	public async Task GetCompanyNames_V1()
	{
		if( !IsInDesignMode )
		{
			try
			{
				Logging.WriteLogLine( "Loading Company Names" );
				var Names = await Azure.Client.RequestGetCustomerCodeList();

				if( Names is not null )
				{
					// Don't include the Global Address Book
					var CoNames = ( from N in Names
								    where N != GlobalAddressBookId
								    orderby N.ToLower()
								    select N ).ToList();

					var NamesToDelete = ( from N in Names
										  where N != GlobalAddressBookId
										  orderby N.ToLower()
										  select N ).ToList();

					List<CompanyAddress>? CompanyAddresses = new();
					List<string>?         EnabledNames     = new();

					var CoNamesLock = new object();

					await new Tasks.Task<string>().RunAsync( CoNames, async coName =>
																      {
																	      try
																	      {
																		      var Company = await Azure.Client.RequestGetResellerCustomerCompany( coName );

																		      lock( CoNamesLock )
																		      {
																			      if( Company.Enabled )
																			      {
																				      if( coName != GlobalAddressBookId )
																				      {
																					      if( Company.CompanyNumber.IsNullOrWhiteSpace() )
																					      {
																						      //Logging.WriteLogLine( "DEBUG Setting CompanyNumber to " + coName );
																						      Company.CompanyNumber = coName;
																					      }

																					      if( !CompanyNames.Contains( coName ) )
																						      // ReSharper disable once AccessToModifiedClosure
																						      EnabledNames.Add( coName );
																					      //Logging.WriteLogLine("DEBUG Loading Company: " + Company.CompanyName);
																					      CompanyAddresses.Add( Company );
																				      }
																			      }
																			      //else
																			      //Logging.WriteLogLine( "DEBUG - skipping disabled account: " + coName );

																			      lock( NamesToDelete )
																			      {
																				      NamesToDelete.Remove( coName );

																				      if( NamesToDelete.Count == 0 )
																				      {
																					      // Save locally
																					      CompanyData Cd = new();

																					      Cd.CompanyAddresses.AddRange( CompanyAddresses );
																					      Cd.EnabledNames.AddRange( (List<string>)CompanyNames );
																					      SaveCompanyData( Cd );
																				      }
																			      }
																		      }
																	      }
																	      catch( Exception Exception )
																	      {
																		      Logging.WriteLogLine( "Exception thrown: " + Exception );
																	      }
																      }, 5 );

					EnabledNames = ( from Tmp in EnabledNames
								     where Tmp != GlobalAddressBookId
								     orderby Tmp.ToLower()
								     select Tmp ).ToList();

					CompanyFullNames.Clear();

					foreach( var ca in Companies )
					{
						if( !CompanyFullNames.ContainsKey( ca.CompanyName ) && ( ca.CompanyNumber != GlobalAddressBookId ) )
							CompanyFullNames.Add( ca.CompanyName, ca );
					}

					Dispatcher.Invoke( () =>
									   {
										   CompanyNames = EnabledNames;
										   Companies    = CompanyAddresses;
									   } );
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}

	private static readonly SemaphoreSlim MultiEntrySem = new( 1, 1 );

	// Runs on Task
	public async void GetCompaniesAndUpdateCurrentCollectionAsync()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var Token = CancellationToken.Token;

				if( !Token.IsCancellationRequested )
				{
					await MultiEntrySem.WaitAsync( Token );

					try
					{
						Logging.WriteLogLine( "Loading Company Names" );
						var Names = await Azure.Client.RequestGetCustomerCodeList();

						if( !Token.IsCancellationRequested && Names is not null )
						{
							var CoNames = ( from N in Names
										    where N != GlobalAddressBookId
										    orderby N.ToLower()
										    select N ).ToList();

							List<CompanyAddress>? CompanyAddresses = new();
							List<string>          EnabledNames     = new();

							foreach( var CoName in CoNames )
							{
								if( Token.IsCancellationRequested )
									return;

								var Company = Azure.Client.RequestGetResellerCustomerCompany( CoName ).Result;

								if( Company != null )
								{
									if( Company.CompanyNumber.IsNullOrWhiteSpace() )
										Company.CompanyNumber = CoName;

									if( Company.Enabled )
									{
										CompanyAddresses.Add( Company );
										EnabledNames.Add( CoName );
									}
								}
								else
									Logging.WriteLogLine( "Can't get Company for " + CoName );
							}

							Logging.WriteLogLine( "Finished getting companies (found " + CompanyAddresses.Count + ")" );

							EnabledNames = ( from E in EnabledNames
										     orderby E.ToLower()
										     select E ).ToList();

							Dispatcher.Invoke( () =>
											   {
												   Companies    = CompanyAddresses;
												   CompanyNames = EnabledNames;
											   } );
							Logging.WriteLogLine( "Finished updating Companies" );

							// Save locally
							Logging.WriteLogLine( "Saving updated companies locally" );
							CompanyData Cd = new();

							Cd.CompanyAddresses.AddRange( Companies );
							Cd.EnabledNames.AddRange( (List<string>)CompanyNames );
							SaveCompanyData( Cd );
							//_ = Task.Run(async () =>
							//{
							//	object value = await SaveCompanyData(cd);

							//});
						}
					}
					catch( Exception E )
					{
						//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
						Logging.WriteLogLine( "Exception thrown: " + E );
						MessageBox.Show( "Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK );
					}
					finally
					{
						MultiEntrySem.Release();
					}
				}
			}
			catch // Probably closed tab druing initialisation
			{
			}
		}

		Logging.WriteLogLine( "DEBUG Done" );
	}

	public string SelectedAccount
	{
		get { return Get( () => SelectedAccount, "" ); }
		set { Set( () => SelectedAccount, value ); }
	}

	public bool IsSelectedAccountDropDownOpen
	{
		get { return Get( () => IsSelectedAccountDropDownOpen, false ); }
		set { Set( () => IsSelectedAccountDropDownOpen, value ); }
	}

	//[DependsUpon( nameof( SelectedAccount ) )]
	[DependsUpon( nameof( IsSelectedAccountDropDownOpen ) )]
	public void WhenIsSelectedAccountDropDownOpenChanges()
	{
		Logging.WriteLogLine( "DEBUG IsSelectedAccountDropDownOpen: " + IsSelectedAccountDropDownOpen );

		if( !IsSelectedAccountDropDownOpen && !IsCustomerDropdownOpen && SelectedAccount.IsNotNullOrWhiteSpace() )
		{
			WhenAccountChanges();
			//WhenSelectedAccountChanges();
		}
	}

	//[DependsUpon( nameof( SelectedAccount ) )]
	public async Task WhenSelectedAccountChanges( bool ignoreDropdown = false )
	{
		Logging.WriteLogLine( "DEBUG IsSelectedAccountDropDownOpen is " + IsSelectedAccountDropDownOpen );

		//Logging.WriteLogLine( "SelectedAccount: " + SelectedAccount );
		//if (TripId.IsNullOrWhiteSpace() && !IsInDesignMode)        
		//if( !IsSelectedAccountDropDownOpen || !IsCustomerDropdownOpen )
		if( ignoreDropdown || ( !IsSelectedAccountDropDownOpen && !IsCustomerDropdownOpen ) )
		{
			if( TripId.IsNullOrWhiteSpace() && ( ( CurrentTrip == null ) || CurrentTrip.TripId.IsNullOrWhiteSpace() ) && !IsInDesignMode )
			{
				try
				{
					var NewTripId = await Azure.Client.RequestGetNextTripId();
					IsNewTrip = true;
					//Logging.WriteLogLine( "DEBUG Setting IsNewTrip to " + IsNewTrip );

					Dispatcher.Invoke( () =>
									   {
										   //Logging.WriteLogLine( "Fetched new TripId: " + NewTripId );
										   TripId = NewTripId;

										   if( TripPackages.Count == 0 )
										   {
											   TripPackages.Add( new DisplayPackage( new TripPackage
																			         {
																				         Pieces      = 1,
																				         Weight      = 1,
																				         PackageType = GetPackageDefault()
																			         } ) );
											   WhenTripPackagesChanges();
										   }
									   } );
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown fetching new TripId: " + E );
				}
			}

			if( SelectedAccount.IsNotNullOrWhiteSpace() )
			{
				foreach( var Ca in Companies )
				{
					if( Ca.CompanyNumber == SelectedAccount )
					{
						CustomerPhone = Ca.Phone;
						CustomerNotes = Ca.Notes;

						// TODO Need account notes

						//DeliveryCountry = Ca.Country;
						//PickupCountry   = Ca.Country;
						//WhenDeliveryCountryChanges();
						//WhenPickupCountryChanges();

						break;
					}
				}
			}
		}
		else
			Logging.WriteLogLine( "DEBUG Dropdowns open: IsSelectedAccountDropDownOpen: " + IsSelectedAccountDropDownOpen + ", IsCustomerDropdownOpen: " + IsCustomerDropdownOpen );
	}

	public async Task WhenSelectedAccountChanges_V2()
	{
		Logging.WriteLogLine( "DEBUG IsSelectedAccountDropDownOpen is " + IsSelectedAccountDropDownOpen );

		//Logging.WriteLogLine( "SelectedAccount: " + SelectedAccount );
		//if (TripId.IsNullOrWhiteSpace() && !IsInDesignMode)        
		if( !IsSelectedAccountDropDownOpen && !IsCustomerDropdownOpen )
		{
			if( TripId.IsNullOrWhiteSpace() && ( ( CurrentTrip == null ) || CurrentTrip.TripId.IsNullOrWhiteSpace() ) && !IsInDesignMode )
			{
				try
				{
					var NewTripId = await Azure.Client.RequestGetNextTripId();
					IsNewTrip = true;
					//Logging.WriteLogLine( "DEBUG Setting IsNewTrip to " + IsNewTrip );

					Dispatcher.Invoke( () =>
									   {
										   //Logging.WriteLogLine( "Fetched new TripId: " + NewTripId );
										   TripId = NewTripId;

										   if( TripPackages.Count == 0 )
										   {
											   TripPackages.Add( new DisplayPackage( new TripPackage
																			         {
																				         Pieces      = 1,
																				         Weight      = 1,
																				         PackageType = GetPackageDefault()
																			         } ) );
											   WhenTripPackagesChanges();
										   }
									   } );
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown fetching new TripId: " + E );
				}
			}
			//if (CompanyFullNames.ContainsKey(Customer))
			//	SelectedAccount = CompanyFullNames[Customer].CompanyNumber;

			if( SelectedAccount.IsNotNullOrWhiteSpace() )
			{
				var ca = ( from C in Companies
						   where C.CompanyNumber == SelectedAccount
						   select C ).FirstOrDefault();

				if( ( ca != null ) && ( ca.CompanyName != Customer ) )
				{
					foreach( var Ca in Companies )
					{
						if( Ca.CompanyNumber == SelectedAccount )
						{
							CustomerPhone = Ca.Phone;
							CustomerNotes = Ca.Notes;

							// TODO Need account notes

							DeliveryCountry = Ca.Country;
							PickupCountry   = Ca.Country;
							WhenDeliveryCountryChanges();
							WhenPickupCountryChanges();

							break;
						}
					}
				}
			}
		}
	}

	public async Task WhenSelectedAccountChanges_V1()
	{
		//Logging.WriteLogLine( "SelectedAccount: " + SelectedAccount );
		//if (TripId.IsNullOrWhiteSpace() && !IsInDesignMode)        
		if( TripId.IsNullOrWhiteSpace() && ( ( CurrentTrip == null ) || CurrentTrip.TripId.IsNullOrWhiteSpace() ) && !IsInDesignMode )
		{
			try
			{
				var NewTripId = await Azure.Client.RequestGetNextTripId();
				IsNewTrip = true;
				//Logging.WriteLogLine( "DEBUG Setting IsNewTrip to " + IsNewTrip );

				Dispatcher.Invoke( () =>
								   {
									   //Logging.WriteLogLine( "Fetched new TripId: " + NewTripId );
									   TripId = NewTripId;
								   } );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown fetching new TripId: " + E );
			}
		}

		if( SelectedAccount.IsNotNullOrWhiteSpace() )
		{
			foreach( var Ca in Companies )
			{
				if( Ca.CompanyNumber == SelectedAccount )
				{
					CustomerPhone = Ca.Phone;
					CustomerNotes = Ca.Notes;

					// TODO Need account notes

					DeliveryCountry = Ca.Country;
					PickupCountry   = Ca.Country;
					WhenDeliveryCountryChanges();
					WhenPickupCountryChanges();

					break;
				}
			}
		}
	}

	public async Task LoadSelectedAccount()
	{
		if( TripId.IsNullOrWhiteSpace() && ( ( CurrentTrip == null ) || CurrentTrip.TripId.IsNullOrWhiteSpace() ) && !IsInDesignMode )
		{
			try
			{
				var NewTripId = await Azure.Client.RequestGetNextTripId();
				IsNewTrip = true;
				//Logging.WriteLogLine( "DEBUG Setting IsNewTrip to " + IsNewTrip );

				Dispatcher.Invoke( () =>
								   {
									   //Logging.WriteLogLine( "Fetched new TripId: " + NewTripId );
									   TripId = NewTripId;
								   } );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown fetching new TripId: " + E );
			}
		}

		if( SelectedAccount.IsNotNullOrWhiteSpace() )
		{
			foreach( var Ca in Companies )
			{
				if( Ca.CompanyNumber == SelectedAccount )
				{
					CustomerPhone = Ca.Phone;
					CustomerNotes = Ca.Notes;

					// TODO Need account notes

					DeliveryCountry = Ca.Country;
					PickupCountry   = Ca.Country;
					WhenDeliveryCountryChanges();
					WhenPickupCountryChanges();

					break;
				}
			}
		}
	}

	public async Task<Company?> GetLatestCompany( string companyName, string accountId )
	{
		Company? company = null;

		if( !IsInDesignMode && companyName.IsNotNullOrWhiteSpace() && accountId.IsNotNullOrWhiteSpace() )
		{
			var Ro = new CompanyByAccountAndCompanyNameList();

			var Company = new CompanyByAccountAndCompanyName
						  {
							  CompanyName  = companyName,
							  CustomerCode = accountId // ca.CompanyNumber
						  };
			Ro.Add( Company );

			try
			{
				var list = await Azure.Client.RequestGetCustomerCompanyAddressList( Ro );

				if( list is {Count: > 0} )
				{
					var companies = list[ 0 ];
					company = companies.Companies[ 0 ];
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception caught: " + e );
			}
		}

		return company;
	}

	public IList PackageTypes
	{
		get { return Get( () => PackageTypes, new List<string>() ); }
		set { Set( () => PackageTypes, value ); }
	}


	private readonly List<string> AllPackageTypes = new();

	public readonly List<PackageType> PackageTypeObjects = new();

	public async Task GetPackageTypes()
	{
		// From Terry: Need to do this to avoid bad requests in the server log
		if( !IsInDesignMode )
		{
			try
			{
				var Pts = await Azure.Client.RequestGetPackageTypes();

				if( Pts is not null )
				{
					var PTypes = ( from Pt in Pts
								   orderby Pt.SortOrder, Pt.Description
								   select Pt.Description ).ToList();

					Dispatcher.Invoke( () =>
									   {
										   PackageTypes = PTypes;
									   } );

					//AllPackageTypes = PTypes;
					AllPackageTypes.Clear();
					AllPackageTypes.AddRange( PTypes );
					PackageTypeObjects.Clear();

					PackageTypeObjects.AddRange( ( from P in Pts
											       select P ).ToList() );
				}
			}
			catch( Exception E )
			{
				//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
				Logging.WriteLogLine( "Exception thrown: " + E, "GetCompanyNames", "SearchTripsModel" );
				MessageBox.Show( "Error fetching Company Names\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}


	public List<TripCharge> TripCharges
	{
		get { return Get( () => TripCharges, new List<TripCharge>() ); }
		set { Set( () => TripCharges, value ); }
	}


	public List<Charge> Charges
	{
		get { return Get( () => Charges, new List<Charge>() ); }
		set { Set( () => Charges, value ); }
	}

	public int NumberOfChargesPerColumn { get; } = 6;

    public List<Charge> DefaultCharges
    {
        get { return Get(() => DefaultCharges, new List<Charge>()); }
        set { Set(() => DefaultCharges, value); }
    }

    public List<Charge> SurchargeCharges
    {
        get { return Get(() => SurchargeCharges, new List<Charge>()); }
        set { Set(() => SurchargeCharges, value); }
    }

    public List<Charge> ApplicableSurchargeCharges
    {
        get { return Get(() => ApplicableSurchargeCharges, new List<Charge>()); }
        set { Set(() => ApplicableSurchargeCharges, value); }
    }

	public Dictionary<int, Charge> DictRowSurcharge
	{
		get => Get(() => DictRowSurcharge, new Dictionary<int, Charge>());
		set => Set(() => DictRowSurcharge, value);
	}

    public List<Charge> GetDefaultChargesForAccount(string? accountId = null)
	{
		List<Charge> charges = new();

		if (accountId == null)
		{
			accountId = SelectedAccount;
		}

		charges = (from D in DefaultCharges where D.ChargeId.Contains($"_{accountId}_") select D).ToList();

		return charges;
	}

    [DependsUpon( nameof( Charges ) )]
	public void WhenChargesChanges()
	{
		Dispatcher.Invoke( () =>
						   {
							   View?.BuildCharges();
						   } );
	}

    public readonly string ACCOUNT_CHARGE_PREFIX = Globals.DataContext.MainDataContext.ACCOUNT_CHARGE_PREFIX;
	public readonly string SURCHARGE_PREFIX = Globals.DataContext.MainDataContext.SURCHARGE_PREFIX;

	public async Task GetCharges()
	{
		if( !IsInDesignMode )
		{
			try
			{
				Logging.WriteLogLine( "Getting charges..." );
				var Result = await Azure.Client.RequestGetAllCharges();
				//List<Charge> charges = Result;

				Charges = ( from C in Result
							where !C.ChargeId.StartsWith( ACCOUNT_CHARGE_PREFIX ) && !C.ChargeId.StartsWith(SURCHARGE_PREFIX)
						    orderby C.SortIndex
						    select C ).ToList();

                DefaultCharges = (from C in Result
                           where C.ChargeId.StartsWith(ACCOUNT_CHARGE_PREFIX)
                           orderby C.SortIndex
                           select C).ToList();

                SurchargeCharges = (from C in Result
									where C.ChargeId.StartsWith(SURCHARGE_PREFIX)
									orderby C.SortIndex
									select C).ToList();
                Logging.WriteLogLine($"Found {Charges.Count} charges, {DefaultCharges.Count} default charges, {SurchargeCharges.Count} surcharges");
            }
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown: " + e );
				MessageBox.Show( "Error fetching Charges\n" + e, "Error", MessageBoxButton.OK );
			}
		}
	}

	public async Task GetSurcharges()
	{
		List<Charge> charges = new();
		var result = await Azure.Client.RequestGetAllCharges();
        charges = (from C in result
                    where C.ChargeId.StartsWith(SURCHARGE_PREFIX)
                    orderby C.SortIndex
                    select C).ToList();
		Logging.WriteLogLine($"Found {charges.Count} surcharges");
		SurchargeCharges = charges;
	}


	//public string SelectedPackageType
	//{
	//	get { return Get( () => SelectedPackageType, "" ); }
	//	set { Set( () => SelectedPackageType, value ); }
	//}


	public IList Drivers
	{
		get { return Get( () => Drivers, new List<string>() ); }
		set { Set( () => Drivers, value ); }
	}

	private async Task GetDrivers()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var Drvs = await Azure.Client.RequestGetDrivers();

				if( Drvs is not null )
				{
					Drivers = ( from D in Drvs
							    orderby D.StaffId.ToLower()
							    select D.StaffId ).ToList();

					if( Drivers.Contains( Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE ) )
						Drivers.Remove( Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE );
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Drivers\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}


	public string SelectedDriver
	{
		get { return Get( () => SelectedDriver, "" ); }

		set { Set( () => SelectedDriver, value ); }
	}


	public List<CompanyByAccountSummary> Addresses
	{
		get { return Get( () => Addresses, new List<CompanyByAccountSummary>() ); }
		set { Set( () => Addresses, value ); }
	}


	public List<string> AddressNames
	{
		get { return Get( () => AddressNames, new List<string>() ); }
		set { Set( () => AddressNames, value ); }
	}


	public Dictionary<string, CompanyDetail> DetailedAddresses
	{
		get { return Get( () => DetailedAddresses, new Dictionary<string, CompanyDetail>() ); }
		set { Set( () => DetailedAddresses, value ); }
	}

    public async void WhenAccountChanges()
    {
        if (!IsInDesignMode && SelectedAccount.IsNotNullOrWhiteSpace())
        {
            Account = SelectedAccount;
            await WhenSelectedAccountChanges();

            var Company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(SelectedAccount);

            if (Company is not null)
            {
                if (Customer != Company.Company.CompanyName)
                    Customer = Company.Company.CompanyName;
                var i = 0;

                foreach (var key in CompanyFullNames.Keys)
                {
                    if (Customer == key)
                    {
                        if (View != null)
                        {
                            Dispatcher.Invoke(() =>
                            {
                                View.cbCustomer.SelectedIndex = i;
                                View.ApplyDefaultChargesAlwaysApplied();
                            });
                        }
                        break;
                    }
                    ++i;
                }

                // 20230111 - Eddy has decided that the convenience of having the country and region set based
                // upon the account is wrong, so let the users pick them.
                //
                if (PickupCountry.IsNullOrWhiteSpace() || (PickupCountry != CurrentTrip?.PickupAddressCountry) || (PickupCountry != Company.Company?.Country) || (PickupCountry != Company.BillingCompany?.Country))
                {
                    if ((CurrentTrip != null) && CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace())
                        PickupCountry = CurrentTrip.PickupAddressCountry;
                }

                if (DeliveryCountry.IsNullOrWhiteSpace() || (DeliveryCountry != CurrentTrip?.DeliveryAddressCountry) || (DeliveryCountry != Company.Company?.Country) || (DeliveryCountry != Company.BillingCompany?.Country))
                {
                    if ((CurrentTrip != null) && CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace())
                        DeliveryCountry = CurrentTrip.DeliveryAddressCountry;
                }

                if (PickupProvState.IsNullOrWhiteSpace() || (PickupProvState != CurrentTrip?.PickupAddressRegion) || (PickupProvState != Company.Company?.Region) || (PickupProvState != Company.BillingCompany?.Region))
                {
                    if ((CurrentTrip != null) && CurrentTrip.PickupAddressRegion.IsNotNullOrWhiteSpace())
                        PickupProvState = CurrentTrip.PickupAddressRegion;
                }

                if (DeliveryProvState.IsNullOrWhiteSpace() || (DeliveryProvState != CurrentTrip?.DeliveryAddressRegion) || (DeliveryProvState != Company.Company?.Region) || (DeliveryProvState != Company.BillingCompany?.Region))
                {
                    if ((CurrentTrip != null) && CurrentTrip.DeliveryAddressRegion.IsNotNullOrWhiteSpace())
                        DeliveryProvState = CurrentTrip.DeliveryAddressRegion;
                }
            }

            try
            {
                if (!string.IsNullOrEmpty(SelectedAccount))
                {   

                    await Dispatcher.Invoke(async () =>
                    {
                        var Addrs = new List<CompanyByAccountSummary>();
                        var Names = new List<string>();
                        var Sd = new SortedDictionary<string, CompanyByAccountSummary>();

						                        
                        DetailedAddresses = new();

						if (!UseGlobalAddressBook)
						{
							var Csl = await GetCompanySummaryForAccountId(Account);
							var Details = await GetCompanyDetailListForAccountId(Account);
							foreach (var Detail in Details)
							{
								if (Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && !DetailedAddresses.ContainsKey(Detail.Company.CompanyName.ToLower()))
									DetailedAddresses.Add(Detail.Company.CompanyName.ToLower(), Detail);
							}

							foreach (var Cs in Csl)
							{
								if (DoesCustomerCompaniesDetailedContainCompanySummaryListItem(Details, Cs))
								{
									if (Cs.CompanyName.IsNotNullOrWhiteSpace() && !Sd.ContainsKey(Cs.CompanyName.ToLower()))
										Sd.Add(Cs.CompanyName.ToLower(), Cs);
								}
							}

							foreach (var detail in Details)
							{
								CompanyByAccountSummary cbas = new()
								{
									AddressLine1 = detail.Address.AddressLine1,
									AddressLine2 = detail.Address.AddressLine2,
									City = detail.Address.City,
									CompanyName = detail.Address.CompanyName,
									CountryCode = detail.Address.CountryCode,
									CustomerCode = detail.Address.CompanyNumber,
									DisplayCompanyName = detail.Address.CompanyName,
									Enabled = detail.Company.Enabled,
									Found = false, // TODO
									LocationBarcode = detail.Address.LocationBarcode,
									PostalCode = detail.Address.PostalCode,
									Region = detail.Address.Region,
									Suite = detail.Address.Suite
								};

								if (!Sd.ContainsKey(cbas.CompanyName.ToLower()))
									Sd.Add(cbas.CompanyName.ToLower(), cbas);
							}
						}

						Sd = ChooseAddressBook(Sd);
						//(Sd, Addrs, Names) = ChooseAddressBook(Sd);

						foreach (var Key in Sd.Keys)
						{
							lock (Addrs)
								Addrs.Add(Sd[Key]);

							lock (Names)
								Names.Add(Sd[Key].CompanyName);
						}

						//                  foreach (string key in Sd.Keys)
						//{
						//                      Addrs.Add(Sd[key]);
						//                      Names.Add(Sd[key].CompanyName);
						//                  }

						Addresses = Addrs;
                        AddressNames = Names;
                        AddressNames.Insert(0, string.Empty);

                        RaisePropertyChanged(nameof(AddressNames));

                        //if (IsNewTrip)
                        if (Status1 < (int)STATUS.ACTIVE)
                        {
                            await ApplyDefaultsToAddressFields();
                        }

                        View?.CbPickupAddresses_SelectionChanged(null, null);
                        View?.CbDeliveryAddresses_SelectionChanged(null, null);
                    });
                }
            }
            catch (Exception E)
            {
                Logging.WriteLogLine("Exception thrown: " + E);
                MessageBox.Show("Error fetching Addresses\n" + E, "Error", MessageBoxButton.OK);
            }
        }
    }

    public async void WhenAccountChanges_V2()
	{
		if( !IsInDesignMode && SelectedAccount.IsNotNullOrWhiteSpace() )
		{
			Account = SelectedAccount;
			//PrimaryCompany? Company = null;
			//Task.WaitAll(
			//	Task.Run(async() =>
			//             {
			//		await WhenSelectedAccountChanges();
			//                 Company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccount );
			//             })	
			//);
			await WhenSelectedAccountChanges();

			var Company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccount );

			if( Company is not null )
			{
				if( Customer != Company.Company.CompanyName )
					Customer = Company.Company.CompanyName;
				var i = 0;

				foreach( var key in CompanyFullNames.Keys )
				{
					if( Customer == key )
					{
						if( View != null )
						{
							Dispatcher.Invoke( () =>
											   {
												   View.cbCustomer.SelectedIndex = i;
												   View.ApplyDefaultChargesAlwaysApplied();
											   } );
						}
						break;
					}
					++i;
				}

				// TODO This seems to be pointing to the wrong thing
				//Customer = Company.BillingCompany.CompanyName;

				//PickupCountry     = company.BillingCompany.Country;
				//PickupProvState   = company.BillingCompany.Region;
				//DeliveryCountry   = company.BillingCompany.Country;
				//DeliveryProvState = company.BillingCompany.Region;

				//PickupCountry = company.Company.Country;
				//PickupProvState = company.Company.Region;
				//DeliveryCountry = company.Company.Country;
				//DeliveryProvState = company.Company.Region;

				//if( PickupCountry.IsNullOrWhiteSpace() )
				//
				// 20230111 - Eddy has decided that the convenience of having the country and region set based
				// upon the account is wrong, so let the users pick them.
				//
				if( PickupCountry.IsNullOrWhiteSpace() || ( PickupCountry != CurrentTrip?.PickupAddressCountry ) || ( PickupCountry != Company.Company?.Country ) || ( PickupCountry != Company.BillingCompany?.Country ) )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace() )
						PickupCountry = CurrentTrip.PickupAddressCountry;
					//else if( ( Company.Company != null ) && Company.Company.Country.IsNotNullOrWhiteSpace() )
					//	PickupCountry = Company.Company.Country;
					//else if( ( Company.BillingCompany != null ) && Company.BillingCompany.Country.IsNotNullOrWhiteSpace() )
					//	PickupCountry = Company.BillingCompany.Country;
				}

				//if( DeliveryCountry.IsNullOrWhiteSpace() )
				if( DeliveryCountry.IsNullOrWhiteSpace() || ( DeliveryCountry != CurrentTrip?.DeliveryAddressCountry ) || ( DeliveryCountry != Company.Company?.Country ) || ( DeliveryCountry != Company.BillingCompany?.Country ) )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace() )
						DeliveryCountry = CurrentTrip.DeliveryAddressCountry;
					//else if( ( Company.Company != null ) && Company.Company.Country.IsNotNullOrWhiteSpace() )
					//	DeliveryCountry = Company.Company.Country;
					//else if( ( Company.BillingCompany != null ) && Company.BillingCompany.Country.IsNotNullOrWhiteSpace() )
					//	DeliveryCountry = Company.BillingCompany.Country;
				}

				//if( PickupProvState.IsNullOrWhiteSpace() )
				if( PickupProvState.IsNullOrWhiteSpace() || ( PickupProvState != CurrentTrip?.PickupAddressRegion ) || ( PickupProvState != Company.Company?.Region ) || ( PickupProvState != Company.BillingCompany?.Region ) )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.PickupAddressRegion.IsNotNullOrWhiteSpace() )
						PickupProvState = CurrentTrip.PickupAddressRegion;
					//else if( ( Company.Company != null ) && Company.Company.Region.IsNotNullOrWhiteSpace() )
					//	PickupProvState = Company.Company.Region;
					//else if( ( Company.BillingCompany != null ) && Company.BillingCompany.Region.IsNotNullOrWhiteSpace() )
					//	PickupProvState = Company.BillingCompany.Region;
				}

				if( DeliveryProvState.IsNullOrWhiteSpace() || ( DeliveryProvState != CurrentTrip?.DeliveryAddressRegion ) || ( DeliveryProvState != Company.Company?.Region ) || ( DeliveryProvState != Company.BillingCompany?.Region ) )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.DeliveryAddressRegion.IsNotNullOrWhiteSpace() )
						DeliveryProvState = CurrentTrip.DeliveryAddressRegion;
					//else if( ( Company.Company != null ) && Company.Company.Region.IsNotNullOrWhiteSpace() )
					//	DeliveryProvState = Company.Company.Region;
					//else if( ( Company.BillingCompany != null ) && Company.BillingCompany.Region.IsNotNullOrWhiteSpace() )
					//	DeliveryProvState = Company.BillingCompany.Region;
				}
			}

			try
			{
				if( !string.IsNullOrEmpty( SelectedAccount ) )
				{
					//CompanySummaryList? Csl = null;
					//Task.WaitAll(
					//	Task.Run(async() =>
					//                   {
					//                       Csl = await Azure.Client.RequestGetCustomerCompaniesSummary( Account );
					//                   })
					//);
					//var Csl = await Azure.Client.RequestGetCustomerCompaniesSummary( Account );

					var Csl = await GetCompanySummaryForAccountId( Account );

					await Dispatcher.Invoke( async () =>
										     {
											     var Addrs = new List<CompanyByAccountSummary>();
											     var Names = new List<string>();
											     var Sd    = new SortedDictionary<string, CompanyByAccountSummary>();

											     //CompanyDetailList? Details = null;
											     //                  Task.WaitAll(
											     //	Task.Run(async () =>
											     //	{
											     //		Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( Account );
											     //	})
											     //);

											     //var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( Account );
											     var Details = await GetCompanyDetailListForAccountId( Account );

											     //Sd = MergeGlobalAddressBook(Sd);
											     //Logging.WriteLogLine("DEBUG After merge, Sd.Count: " + Sd.Count);

											     DetailedAddresses = new Dictionary<string, CompanyDetail>();

											     foreach( var Detail in Details )
											     {
												     if( Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && !DetailedAddresses.ContainsKey( Detail.Company.CompanyName.ToLower() ) )
													     DetailedAddresses.Add( Detail.Company.CompanyName.ToLower(), Detail );

												     //if (Detail.Company.CompanyName.IsNotNullOrWhiteSpace() && !Sd.ContainsKey(Detail.Company.CompanyName.ToLower()))
												     //    Sd.Add(Cs.CompanyName.ToLower(), Cs);
											     }

											     foreach( var Cs in Csl )
											     {
												     if( DoesCustomerCompaniesDetailedContainCompanySummaryListItem( Details, Cs ) )
												     {
													     if( Cs.CompanyName.IsNotNullOrWhiteSpace() && !Sd.ContainsKey( Cs.CompanyName.ToLower() ) )
														     Sd.Add( Cs.CompanyName.ToLower(), Cs );
													     //else
													     //    Logging.WriteLogLine("Skipping - cs: " + Cs.CompanyName );
												     }
											     }

											     //if( ( Sd.Keys.Count == 1 ) && ( Details != null ) )
											     foreach( var detail in Details )
											     {
												     CompanyByAccountSummary cbas = new()
																			        {
																				        AddressLine1       = detail.Address.AddressLine1,
																				        AddressLine2       = detail.Address.AddressLine2,
																				        City               = detail.Address.City,
																				        CompanyName        = detail.Address.CompanyName,
																				        CountryCode        = detail.Address.CountryCode,
																				        CustomerCode       = detail.Address.CompanyNumber,
																				        DisplayCompanyName = detail.Address.CompanyName,
																				        Enabled            = detail.Company.Enabled,
																				        Found              = false, // TODO
																				        LocationBarcode    = detail.Address.LocationBarcode,
																				        PostalCode         = detail.Address.PostalCode,
																				        Region             = detail.Address.Region,
																				        Suite              = detail.Address.Suite
																			        };

												     //Sd.Add(cbas.CompanyName.ToLower(), cbas);
												     if( !Sd.ContainsKey( cbas.CompanyName.ToLower() ) )
													     Sd.Add( cbas.CompanyName.ToLower(), cbas );
											     }

											     Sd = ChooseAddressBook( Sd );
												 //(Sd, Addrs, Names) = ChooseAddressBook( Sd );

											     foreach( var Key in Sd.Keys )
											     {
												     lock( Addrs )
													     Addrs.Add( Sd[ Key ] );

												     lock( Names )
													     Names.Add( Sd[ Key ].CompanyName );
											     }

											     Addresses    = Addrs;
											     AddressNames = Names;
											     AddressNames.Insert( 0, string.Empty );

											     RaisePropertyChanged( nameof( AddressNames ) );

											     //if (IsNewTrip)
											     if( Status1 < (int)STATUS.ACTIVE )
											     {
												     await ApplyDefaultsToAddressFields();
												     //View?.CbPickupAddresses_SelectionChanged(null, null);
												     //View?.CbDeliveryAddresses_SelectionChanged(null, null);
											     }
											     //ApplyDefaultsToAddressFields();

											     View?.CbPickupAddresses_SelectionChanged( null, null );
											     View?.CbDeliveryAddresses_SelectionChanged( null, null );
										     } );
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Addresses\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}

	public async void WhenAccountChanges_V1()
		//public void WhenAccountChanges()
	{
		if( !IsInDesignMode && SelectedAccount.IsNotNullOrWhiteSpace() )
		{
			Account = SelectedAccount;
			//PrimaryCompany? Company = null;
			//Task.WaitAll(
			//	Task.Run(async() =>
			//             {
			//		await WhenSelectedAccountChanges();
			//                 Company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccount );
			//             })	
			//);
			await WhenSelectedAccountChanges();

			var Company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccount );

			if( Company is not null )
			{
				if( Customer != Company.Company.CompanyName )
					Customer = Company.Company.CompanyName;
				var i = 0;

				foreach( var key in CompanyFullNames.Keys )
				{
					if( Customer == key )
					{
						if( View != null )
						{
							Dispatcher.Invoke( () =>
											   {
												   View.cbCustomer.SelectedIndex = i;
											   } );
						}
						break;
					}
					++i;
				}

				// TODO This seems to be pointing to the wrong thing
				//Customer = Company.BillingCompany.CompanyName;

				//PickupCountry     = company.BillingCompany.Country;
				//PickupProvState   = company.BillingCompany.Region;
				//DeliveryCountry   = company.BillingCompany.Country;
				//DeliveryProvState = company.BillingCompany.Region;

				//PickupCountry = company.Company.Country;
				//PickupProvState = company.Company.Region;
				//DeliveryCountry = company.Company.Country;
				//DeliveryProvState = company.Company.Region;

				if( PickupCountry.IsNullOrWhiteSpace() )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace() )
						PickupCountry = CurrentTrip.PickupAddressCountry;
					else if( Company.Company.Country.IsNotNullOrWhiteSpace() )
						PickupCountry = Company.Company.Country;
					else if( Company.BillingCompany.Country.IsNotNullOrWhiteSpace() )
						PickupCountry = Company.BillingCompany.Country;
				}

				if( DeliveryCountry.IsNullOrWhiteSpace() )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace() )
						DeliveryCountry = CurrentTrip.DeliveryAddressCountry;
					else if( Company.Company.Country.IsNotNullOrWhiteSpace() )
						DeliveryCountry = Company.Company.Country;
					else if( Company.BillingCompany.Country.IsNotNullOrWhiteSpace() )
						DeliveryCountry = Company.BillingCompany.Country;
				}

				if( PickupProvState.IsNullOrWhiteSpace() )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.PickupAddressRegion.IsNotNullOrWhiteSpace() )
						PickupProvState = CurrentTrip.PickupAddressRegion;
					else if( Company.Company.Region.IsNotNullOrWhiteSpace() )
						PickupProvState = Company.Company.Region;
					else if( Company.BillingCompany.Region.IsNotNullOrWhiteSpace() )
						PickupProvState = Company.BillingCompany.Region;
				}

				if( DeliveryProvState.IsNullOrWhiteSpace() )
				{
					if( ( CurrentTrip != null ) && CurrentTrip.DeliveryAddressRegion.IsNotNullOrWhiteSpace() )
						DeliveryProvState = CurrentTrip.DeliveryAddressRegion;
					else if( Company.Company.Region.IsNotNullOrWhiteSpace() )
						DeliveryProvState = Company.Company.Region;
					else if( Company.BillingCompany.Region.IsNotNullOrWhiteSpace() )
						DeliveryProvState = Company.BillingCompany.Region;
				}
			}

			try
			{
				if( !string.IsNullOrEmpty( SelectedAccount ) )
				{
					//CompanySummaryList? Csl = null;
					//Task.WaitAll(
					//	Task.Run(async() =>
					//                   {
					//                       Csl = await Azure.Client.RequestGetCustomerCompaniesSummary( Account );
					//                   })
					//);
					var Csl = await Azure.Client.RequestGetCustomerCompaniesSummary( Account );

					if( Csl != null )
					{
						var Addrs = new List<CompanyByAccountSummary>();
						var Names = new List<string>();
						var Sd    = new SortedDictionary<string, CompanyByAccountSummary>();

						//CompanyDetailList? Details = null;
						//                  Task.WaitAll(
						//	Task.Run(async () =>
						//	{
						//		Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( Account );
						//	})
						//);

						var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( Account );

						//Sd = MergeGlobalAddressBook(Sd);
						//Logging.WriteLogLine("DEBUG After merge, Sd.Count: " + Sd.Count);

						if( Details != null )
						{
							DetailedAddresses = new Dictionary<string, CompanyDetail>();

							foreach( var Detail in Details )
							{
								if( Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && !DetailedAddresses.ContainsKey( Detail.Company.CompanyName.ToLower() ) )
									DetailedAddresses.Add( Detail.Company.CompanyName.ToLower(), Detail );
							}
						}

						foreach( var Cs in Csl )
						{
							if( DoesCustomerCompaniesDetailedContainCompanySummaryListItem( Details, Cs ) )
							{
								if( Cs.CompanyName.IsNotNullOrWhiteSpace() && !Sd.ContainsKey( Cs.CompanyName.ToLower() ) )
									Sd.Add( Cs.CompanyName.ToLower(), Cs );
								//else
								//    Logging.WriteLogLine("Skipping - cs: " + Cs.CompanyName );
							}
						}

						if( ( Sd.Keys.Count == 1 ) && ( Details != null ) )
						{
							foreach( var detail in Details )
							{
								CompanyByAccountSummary cbas = new()
															   {
																   AddressLine1       = detail.Address.AddressLine1,
																   AddressLine2       = detail.Address.AddressLine2,
																   City               = detail.Address.City,
																   CompanyName        = detail.Address.CompanyName,
																   CountryCode        = detail.Address.CountryCode,
																   CustomerCode       = detail.Address.CompanyNumber,
																   DisplayCompanyName = detail.Address.CompanyName,
																   Enabled            = detail.Company.Enabled,
																   Found              = false, // TODO
																   LocationBarcode    = detail.Address.LocationBarcode,
																   PostalCode         = detail.Address.PostalCode,
																   Region             = detail.Address.Region,
																   Suite              = detail.Address.Suite
															   };

								//Sd.Add(cbas.CompanyName.ToLower(), cbas);
								if( !Sd.ContainsKey( cbas.CompanyName.ToLower() ) )
									Sd.Add( cbas.CompanyName.ToLower(), cbas );
							}
						}

						Sd = ChooseAddressBook( Sd );
						//(Sd, Addrs, Names) = ChooseAddressBook( Sd );

						foreach( var Key in Sd.Keys )
						{
							lock( Addrs )
								Addrs.Add( Sd[ Key ] );

							lock( Names )
								Names.Add( Sd[ Key ].CompanyName );
						}

						await Dispatcher.Invoke( async () =>
											     {
												     Addresses    = Addrs;
												     AddressNames = Names;
												     AddressNames.Insert( 0, string.Empty );

												     RaisePropertyChanged( nameof( AddressNames ) );

												     // KLUDGE Try to load desired pu & del addresses 
												     // if (CurrentTrip != null)
												     //{
												     //    if (CurrentTrip.PickupCompanyName.IsNotNullOrEmpty())
												     //    {
												     //        SelectedPickupAddress = CurrentTrip.PickupCompanyName;
												     //    }
												     //    if (CurrentTrip.DeliveryCompanyName.IsNotNullOrEmpty())
												     //    {
												     //        SelectedDeliveryAddress = CurrentTrip.DeliveryCompanyName;
												     //    }
												     //}

												     await ApplyDefaultsToAddressFields();

												     View?.CbPickupAddresses_SelectionChanged( null, null );
												     View?.CbDeliveryAddresses_SelectionChanged( null, null );
											     } );
					}
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown: " + E );
				MessageBox.Show( "Error fetching Addresses\n" + E, "Error", MessageBoxButton.OK );
			}
		}
	}

	//public CompanySummaryList GlobalCompanySummaryList
	//   {
	//       get { return Get(() => GlobalCompanySummaryList, LoadGlobalSummaryList()); }
	//	set { Set(() => GlobalCompanySummaryList, value); }
	//   }

	//public CompanyDetailList GlobalCompanyDetailList
	//   {
	//	get { return Get(() => GlobalCompanyDetailList, LoadGlobalDetailList()); }
	//	set { Set( () => GlobalCompanyDetailList, value); }
	//   }

	public SortedDictionary<string, CompanyByAccountSummary> GlobalAddresses
	{
		get => Get( () => GlobalAddresses, new SortedDictionary<string, CompanyByAccountSummary>() );
		set => Set( () => GlobalAddresses, value );
	}

	public SortedDictionary<string, CompanyDetail> GlobalDetailedAddresses
	{
		get => Get( () => GlobalDetailedAddresses, new SortedDictionary<string, CompanyDetail>() );
		set => Set( () => GlobalDetailedAddresses, value );
	}


	private void LoadGlobalAddressBook()
	{
		GlobalAddresses = GlobalAddressesModel.GlobalAddresses;
		GlobalDetailedAddresses = GlobalAddressesModel.GlobalDetailedAddresses;
        Logging.WriteLogLine( "GlobalAddressBook has " + GlobalAddresses.Count + " addresses" );
	}

	/// <summary>
	///     If there are any other TripEntry tabs, grab their global addresses.
	/// </summary>
	/// <returns></returns>
	private async Task LoadGlobalAddressBook_V1( bool checkOtherTabs = true )
	{
		SortedDictionary<string, CompanyByAccountSummary> AddressBook = new();
		var                                               Tis         = Globals.DataContext.MainDataContext.ProgramTabItems;
		var                                               IsLoaded      = false;

		if( checkOtherTabs && ( Tis.Count > 0 ) )
		{
			foreach( var Ti in Tis )
			{
				if( Ti.Content is TripEntry )
				{
					if( Ti.Content.DataContext is TripEntryModel Model && ( Model.GlobalAddresses != null ) && ( Model.GlobalAddresses.Count > 0 ) )
					{
						Logging.WriteLogLine( "Loading global addresses from another board" );
						GlobalAddresses         = new SortedDictionary<string, CompanyByAccountSummary>( Model.GlobalAddresses );
						GlobalDetailedAddresses = new SortedDictionary<string, CompanyDetail>( Model.GlobalDetailedAddresses );
						IsLoaded                  = true;
						break;
					}
				}
			}
		}

		if( !IsLoaded )
		{
			Logging.WriteLogLine( "Loading global addresses from server" );

			var Tasks = new Task[]
						{
							Azure.Client.RequestGetCustomerCompaniesSummary( GlobalAddressBookId ),
							Azure.Client.RequestGetCustomerCompaniesDetailed( GlobalAddressBookId )
						};
			await Task.WhenAll( Tasks );

			var Csl = ( (Task<CompanySummaryList>)Tasks[ 0 ] ).Result;
			var Cdl = ( (Task<CompanyDetailList>)Tasks[ 1 ] ).Result;

			if( Cdl is not null )
			{
				// Already created and populated
				// DetailedAddresses = new Dictionary<string, CompanyDetail>();
				foreach( var Detail in Cdl )
				{
					// Don't include the artifacts of the global address book
					if( Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && ( Detail.Company.CompanyNumber != GlobalAddressBookId )
																				     && ( Detail.Company.CompanyName != GlobalAddressBookId ) && !GlobalDetailedAddresses.ContainsKey( Detail.Company.CompanyName.ToLower() ) )
						GlobalDetailedAddresses.Add( Detail.Company.CompanyName.ToLower(), Detail );
				}
			}

			if( Csl is not null )
			{
				foreach( var Cs in Csl )
				{
					//Logging.WriteLogLine("DEBUG Looking at Cs: " + Cs.CompanyName);
					if( DoesCustomerCompaniesDetailedContainCompanySummaryListItem( Cdl, Cs ) )
					{
						// Don't include the artifacts of the global address book
						if( Cs.CompanyName.IsNotNullOrWhiteSpace() && ( Cs.CompanyName != GlobalAddressBookId ) && !AddressBook.ContainsKey( Cs.CompanyName.ToLower() ) )
							AddressBook.Add( Cs.CompanyName.ToLower(), Cs );
						//else
						//	Logging.WriteLogLine( "Skipping - cs: " + Cs.CompanyName );
					}
					//else
					//{
					//	Logging.WriteLogLine("DEBUG Detailed doesn't contain cs");
					//}
				}
			}

			if( ( AddressBook.Keys.Count == 1 ) && ( Cdl != null ) )
			{
				foreach( var Detail in Cdl )
				{
					CompanyByAccountSummary Cbas = new()
												   {
													   AddressLine1       = Detail.Address.AddressLine1,
													   AddressLine2       = Detail.Address.AddressLine2,
													   City               = Detail.Address.City,
													   CompanyName        = Detail.Address.CompanyName,
													   CountryCode        = Detail.Address.CountryCode,
													   CustomerCode       = Detail.Address.CompanyNumber,
													   DisplayCompanyName = Detail.Address.CompanyName,
													   Enabled            = Detail.Company.Enabled,
													   Found              = false, // TODO
													   LocationBarcode    = Detail.Address.LocationBarcode,
													   PostalCode         = Detail.Address.PostalCode,
													   Region             = Detail.Address.Region,
													   Suite              = Detail.Address.Suite
												   };

					//Sd.Add(cbas.CompanyName.ToLower(), cbas);
					if( !AddressBook.ContainsKey( Cbas.CompanyName.ToLower() ) )
						AddressBook.Add( Cbas.CompanyName.ToLower(), Cbas );
				}
			}

			Logging.WriteLogLine( "GlobalAddressBook has " + GlobalAddresses.Count + " addresses" );

			GlobalAddresses = AddressBook;
		}
	}

    private SortedDictionary<string, CompanyByAccountSummary> ChooseAddressBook(SortedDictionary<string, CompanyByAccountSummary>? addressBook)
    {
        addressBook ??= new SortedDictionary<string, CompanyByAccountSummary>();

        if (UseGlobalAddressBook)
        {
            Logging.WriteLogLine("Using GlobalAddressBook - GlobalAddress.Count: " + GlobalAddresses.Count);
            addressBook = GlobalAddresses;
        }
        else
            Logging.WriteLogLine("NOT using GlobalAddressBook");


        return addressBook!;
    }
    private (SortedDictionary<string, CompanyByAccountSummary>, List<CompanyByAccountSummary>, List<string>) ChooseAddressBook_Test( SortedDictionary<string, CompanyByAccountSummary>? addressBook )
	{
		addressBook ??= new SortedDictionary<string, CompanyByAccountSummary>();
        var addrs = new List<CompanyByAccountSummary>();
        var names = new List<string>();

        if ( UseGlobalAddressBook )
		{
			Logging.WriteLogLine( "Using GlobalAddressBook - GlobalAddress.Count: " + GlobalAddresses.Count );
			addressBook = GlobalAddresses;
		}
		else
			Logging.WriteLogLine( "NOT using GlobalAddressBook" );

		foreach (string key in addressBook.Keys)
		{
			lock (addrs)
			{
				addrs.Add(addressBook[key]);
			}
			lock (names)
			{
				names.Add(addressBook[key].CompanyName);
			}
		}


		return (addressBook!, addrs, names);
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="cdl">
	/// </param>
	/// <param
	///     name="cbas">
	/// </param>
	/// <returns></returns>
	private static bool DoesCustomerCompaniesDetailedContainCompanySummaryListItem( CompanyDetailList? cdl, CompanyByAccountAndCompanyName cbas )
	{
		var Yes = false;

		if( cdl is not null )
		{
			//var found = (from Cd in cdl where Cd.Company.CompanyNumber == cbas.CustomerCode select Cd).FirstOrDefault();
			//if (found != null)
			//         {
			//	Yes = true;
			//         }
			foreach( var Cd in cdl )
			{
				//if( Cd.Company.CompanyName == cbas.CompanyName )
				if( Cd.Company.CompanyNumber == cbas.CustomerCode )
				{
					Yes = true;
					break;
				}
			}
		}
		return Yes;
	}

	public string SelectedPickupAddress
	{
		get { return Get( () => SelectedPickupAddress, "" ); }

		set { Set( () => SelectedPickupAddress, value ); }
	}

	public bool IsPickupAddressesDropDownOpen
	{
		get { return Get( () => IsPickupAddressesDropDownOpen, false ); }
		set { Set( () => IsPickupAddressesDropDownOpen, value ); }
	}

	[DependsUpon( nameof( IsPickupAddressesDropDownOpen ) )]
	public void WhenIsPickupAddressesDropDownOpenChanges()
	{
		Logging.WriteLogLine( "DEBUG IsPickupAddressesDropDownOpen: " + IsPickupAddressesDropDownOpen );

		if( !IsPickupAddressesDropDownOpen )
		{
			//View?.CbPickupAddresses_SelectionChanged(null, null);
			View?.LoadSelectedPickupAddress();
		}
	}


	public string SelectedDeliveryAddress
	{
		get { return Get( () => SelectedDeliveryAddress, "" ); }

		set { Set( () => SelectedDeliveryAddress, value ); }
	}

	public bool IsDeliveryAddressesDropDownOpen
	{
		get { return Get( () => IsDeliveryAddressesDropDownOpen, false ); }
		set { Set( () => IsDeliveryAddressesDropDownOpen, value ); }
	}

	[DependsUpon( nameof( IsDeliveryAddressesDropDownOpen ) )]
	public void WhenIsDeliveryAddressesDropDownOpenChanges()
	{
		Logging.WriteLogLine( "DEBUG IsDeliveryAddressesDropDownOpen: " + IsDeliveryAddressesDropDownOpen );

		if( !IsDeliveryAddressesDropDownOpen )
		{
			//View?.CbPickupAddresses_SelectionChanged(null, null);
			View?.LoadSelectedDeliveryAddress();
		}
	}

	public LOG Log
	{
		get { return Get( () => Log, LOG.TRIP ); }

		set { Set( () => Log, value ); }
	}

	public string User
	{
		get { return Get( () => User, "" ); }

		set { Set( () => User, value ); }
	}


	public bool IsNewTrip
	{
		get { return Get( () => IsNewTrip, false ); }

		set { Set( () => IsNewTrip, value ); }
	}

	//[DependsUpon(nameof(IsNewTrip))]
	public void WhenIsNewTripChanges()
	{
		Logging.WriteLogLine( "DEBUG IsNewTrip: " + IsNewTrip );
	}


	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638222337/How+to+Use+the+Shipment+Entry+Tab" ); }
	}


	public List<Zone> ZoneObjects
	{
		get { return Get( () => ZoneObjects, new List<Zone>() ); }
		set { Set( () => ZoneObjects, value ); }
	}

	public List<string> ZoneNames
	{
		get { return Get( () => ZoneNames, new List<string>() ); }
		set { Set( () => ZoneNames, value ); }
	}

	public async Task GetZoneNames()
	{
		if( !IsInDesignMode )
		{
			var Names = new List<string>();

			var Zones = await Azure.Client.RequestZones();

			//Logging.WriteLogLine( "Found " + Zones.Count + " Zones" );
			// Occasionally getting zones with empty names
			if( Zones.Count > 0 )
			{
				Names = ( from Z in Zones
						  orderby Z.Name.ToLower()
						  where Z.Name.IsNotNullOrWhiteSpace()
						  select Z.Name ).ToList();

				Names.Insert( 0, "" ); // So that an empty zone can be selected
			}

			ZoneNames = Names;

			ZoneObjects = ( from Z in Zones
						    where Z.Name.IsNotNullOrWhiteSpace()
						    select Z ).ToList();
		}
	}


	public List<RateMatrixItem> RateMatrixItems
	{
		get { return Get( () => RateMatrixItems, new List<RateMatrixItem>() ); }
		set { Set( () => RateMatrixItems, value ); }
	}

	public async Task GetRateMatrixItems()
	{
		if( !IsInDesignMode )
		{
			var rmis = await Azure.Client.RequestGetAllRateMatrixItems();

			if( rmis.Count > 0 )
			{
				Logging.WriteLogLine( "Found " + rmis.Count + " RateMatrixItems" );

				RateMatrixItems.AddRange( ( from R in rmis
										    select R ).ToList() );
			}
		}
	}

	[DependsUpon( nameof( PickupZone ) )]
	[DependsUpon( nameof( DeliveryZone ) )]
	[DependsUpon( nameof( ServiceLevel ) )]
	public void WhenZoneOrServiceLevelChanges()
	{
		Dispatcher.Invoke( () =>
						   {
							   for( var i = 0; i < TripPackages.Count; i++ )
								   View?.RecalculateRates( i + 1 );
						   } );
	}


	public List<PostcodesToZonesBase> RoutesGeneric
	{
		get { return Get( () => RoutesGeneric, new List<PostcodesToZonesBase>() ); }
		set { Set( () => RoutesGeneric, value ); }
	}

	public List<PmlRoute> RoutesPml
	{
		get => Get( () => RoutesPml, new List<PmlRoute>() );
		set => Set( () => RoutesPml, value );
	}


	public async Task GetRoutes()
	{
		if( !IsInDesignMode )
		{
			if( !IsPml )
			{
				var PtozList = await Azure.Client.RequestGetPostalZones( "*", "*", "*" );
				await Azure.Client.RequestPML_GetRoutes();
				Logging.WriteLogLine( "Found routes: " + PtozList?.Count );

				RoutesGeneric.Clear();

				if( PtozList?.Count > 0 )
				{
					//Routes.AddRange( ( from P in prs
					//                   select P ).ToList() );
					foreach( var ptzb in PtozList )
					{
						if( ptzb.RouteName == Globals.DataContext.MainDataContext.NONE_DISPATCHING_ROUTE )
							ptzb.RouteName = string.Empty;
						RoutesGeneric.Add( ptzb );
					}
				}
			}
			else
			{
				var prs = await Azure.Client.RequestPML_GetRoutes();
				Logging.WriteLogLine( "Found routes: " + prs?.Count );

				RoutesPml.Clear();

				if( prs?.Count > 0 )
				{
					//Routes.AddRange( ( from P in prs
					//                   select P ).ToList() );
					foreach( var pr in prs )
						RoutesPml.Add( pr );
				}
			}
		}
	}

	//public UpdateCustomerCompany? PickupAddressUpdate { get; set; } = null;
	public UpdateCustomerCompany? PickupAddressUpdate
	{
		get { return Get( () => PickupAddressUpdate, new UpdateCustomerCompany( PROGRAM ) ); }
		set { Set( () => PickupAddressUpdate, value ); }
	}

	//public UpdateCustomerCompany? DeliveryAddressUpdate { get; set; } = null;
	public UpdateCustomerCompany? DeliveryAddressUpdate
	{
		get { return Get( () => DeliveryAddressUpdate, new UpdateCustomerCompany( PROGRAM ) ); }
		set { Set( () => DeliveryAddressUpdate, value ); }
	}

	//   [DependsUpon(nameof(PickupAddressUpdate))]
	//[DependsUpon(nameof(DeliveryAddressUpdate))]
	//public void WhenAddressUpdateChanges()
	//   {
	//	Logging.WriteLogLine("DEBUG PickupAddressUpdate: " + PickupAddressUpdate + ", DeliveryAddressUpdate: " + DeliveryAddressUpdate);
	//   }

#region Preferences and Defaults
	public Preferences Prefs
	{
		get => Get( () => Prefs, new Preferences() );
		set => Set( () => Prefs, value );
	}

	public async Task GetPreferences()
	{
		if( !IsInDesignMode )
		{
			Logging.WriteLogLine( "Getting Preferences" );
			Prefs = await Azure.Client.RequestPreferences();

			Logging.WriteLogLine( "Found " + Prefs?.Count + " preferences;" );

			LoadDefaultsPreference();

			if( View != null )
			{
				View.dtpCallDate.IsEnabled = View.tpCallTime.IsEnabled     = IsCallTimeEnabled;
				View.cbDrivers.IsEnabled   = View.lblClearDriver.IsEnabled = IsDispatchToDriverEnabled;
			}

			var weightChangePref = string.Empty;

			if( Prefs?.Count > 0 )
			{
				if( View != null )
				{
					var pref = ( from P in Prefs
							     where P.Description.Contains( "Minimum weight change is 0.1" )
							     select P ).FirstOrDefault();
					IsMinimumWeightChangeLessThanOne = ( pref != null ) && pref.Enabled;

					pref = ( from P in Prefs
						     where P.Description.Contains( "hide default - Delivery" )
						     select P ).FirstOrDefault();
					IsDefaultCheckBoxDeliveryHidden = ( pref != null ) && pref.Enabled;

					if( IsDefaultCheckBoxDeliveryHidden )
						View.CbDeliveryAddressDefault.Visibility = Visibility.Collapsed;
					else
						View.CbDeliveryAddressDefault.Visibility = Visibility.Visible;

					pref = ( from P in Prefs
						     where P.Description.Contains( "hide default - Pickup" )
						     select P ).FirstOrDefault();
					IsDefaultCheckBoxPickupHidden = ( pref != null ) && pref.Enabled;

					if( IsDefaultCheckBoxPickupHidden )
						View.CbPickupAddressDefault.Visibility = Visibility.Collapsed;
					else
						View.CbPickupAddressDefault.Visibility = Visibility.Visible;

					pref = ( from P in Prefs
						     where P.Description.Contains( "Show Clone \"Pickup\"" )
						     select P ).FirstOrDefault();
					IsShowClonePickup = ( pref != null ) && pref.Enabled;

					if( !IsShowClonePickup )
						View.MiClonePickup.Visibility = View.btnClonePickup.Visibility = Visibility.Collapsed;
					else
						View.MiClonePickup.Visibility = View.btnClonePickup.Visibility = Visibility.Visible;

					pref = ( from P in Prefs
						     where P.Description.Contains( "Show Clone \"Return\"" )
						     select P ).FirstOrDefault();
					IsShowCloneReturn = ( pref != null ) && pref.Enabled;

					if( !IsShowCloneReturn )
						View.MiCloneReturn.Visibility = View.btnCloneReturn.Visibility = Visibility.Collapsed;
					else
						View.MiCloneReturn.Visibility = View.btnCloneReturn.Visibility = Visibility.Visible;

					pref = ( from P in Prefs
						     where P.Description.Contains( "Use Global Address Book" )
						     select P ).FirstOrDefault();
					UseGlobalAddressBook = ( pref != null ) && pref.Enabled;
					//Logging.WriteLogLine( "DEBUG UseGlobalAddressBook: " + UseGlobalAddressBook );

					// pref.Description.ToLower().Trim() == "pml"
					pref = ( from P in Prefs
						     where P.Description.ToLower().Trim() == "pml"
						     select P ).FirstOrDefault();
					IsPml = ( pref != null ) && pref.Enabled;
					//Logging.WriteLogLine( "DEBUG IsPml: " + IsPml );
					pref = (from P in Prefs
							where P.Description.Contains("Package Type QUOTES overrides RateMatrix")
							select P).FirstOrDefault();
					DoesPackageTypeQuotesOverrideTripMatrix = (pref != null) && pref.Enabled;

					pref = (from P in Prefs
							where P.Description.Contains("Enable Surcharges") && pref.Enabled
							select P).FirstOrDefault();
					AreSurchargesEnabled = ( pref != null ) && pref.Enabled;
					Logging.WriteLogLine($"DEBUG AreSurchargesEnabled: {AreSurchargesEnabled}");
				}
			}
		}
	}

	public readonly string DEFAULTS_PREFERENCE_NAME = "Trip Entry: Shipment Entry Defaults";

	public readonly string DEFAULT_PREFIX_PICKUP        = "pu|";
	public readonly string DEFAULT_PREFIX_DELIVERY      = "del|";
	public readonly string DEFAULT_PREFIX_PACKAGE_TYPE  = "_packagetype";
	public readonly string DEFAULT_PREFIX_SERVICE_LEVEL = "_servicelevel";
	public readonly string DEFAULT_PREFIX_PIECES        = "_pieces";
	public readonly string DEFAULT_PREFIX_WEIGHT        = "_weight";
	public readonly string DEFAULT_PREFIX_READY_TIME    = "_readytime";
	public readonly string DEFAULT_PREFIX_DUE_BY_TIME   = "_duebytime";


	public Preference? ShipmentEntryDefaultsPreference { get; set; }

	public SortedDictionary<string, string>? Defaults { get; set; } = new();

	public bool IsMinimumWeightChangeLessThanOne
	{
		get => Get( () => IsMinimumWeightChangeLessThanOne, false );
		set => Set( () => IsMinimumWeightChangeLessThanOne, value );
	}

	public bool IsDefaultCheckBoxDeliveryHidden
	{
		get => Get( () => IsDefaultCheckBoxDeliveryHidden, false );
		set => Set( () => IsDefaultCheckBoxDeliveryHidden, value );
	}

	[DependsUpon( nameof( IsDefaultCheckBoxDeliveryHidden ) )]
	public string IsDefaultCheckBoxDeliveryHiddenString
	{
		get
		{
			var visibility = "Visible";

			if( IsDefaultCheckBoxDeliveryHidden )
				visibility = "Hidden";

			return visibility;
		}
	}

	public bool IsDefaultCheckBoxPickupHidden
	{
		get => Get( () => IsDefaultCheckBoxPickupHidden, false );
		set => Set( () => IsDefaultCheckBoxPickupHidden, value );
	}

	[DependsUpon( nameof( IsDefaultCheckBoxPickupHidden ) )]
	public string IsDefaultCheckBoxPickupHiddenString
	{
		get
		{
			var visibility = "Visible";

			if( IsDefaultCheckBoxPickupHidden )
				visibility = "Hidden";

			return visibility;
		}
	}

	public bool IsShowClonePickup
	{
		get => Get( () => IsShowClonePickup, false );
		set => Set( () => IsShowClonePickup, value );
	}

	[DependsUpon( nameof( IsShowCloneReturn ) )]
	public string IsShowClonePickupString
	{
		get
		{
			var visibility = "Visible";

			if( IsShowClonePickup )
				visibility = "Hidden";

			return visibility;
		}
	}

	public bool IsShowCloneReturn
	{
		get => Get( () => IsShowCloneReturn, false );
		set => Set( () => IsShowCloneReturn, value );
	}

	[DependsUpon( nameof( IsShowCloneReturn ) )]
	public string IsShowCloneReturnString
	{
		get
		{
			var visibility = "Visible";

			if( IsShowCloneReturn )
				visibility = "Hidden";

			return visibility;
		}
	}

	public bool UseGlobalAddressBook
	{
		get => Get( () => UseGlobalAddressBook, false );
		set => Set( () => UseGlobalAddressBook, value );
	}

	public bool IsPml
	{
		get => Get( () => IsPml, false );
		set => Set( () => IsPml, value );
	}

	public bool DoesPackageTypeQuotesOverrideTripMatrix
	{
		get => Get(() => DoesPackageTypeQuotesOverrideTripMatrix, false);
		set => Set(() => DoesPackageTypeQuotesOverrideTripMatrix, value);
	}

	public bool AreSurchargesEnabled
	{
		get => Get(() => AreSurchargesEnabled, false);
		set => Set(() => AreSurchargesEnabled, value);
	}

	/// <summary>
	///     The preference contains a SortedDictionary
	///     <string,string>
	///         that has
	///         been serialised into a string.  i'm using a SortedDictionary to make
	///         debugging easier.
	///         The key is either the name of the field (_packagetype, _servicelevel, _pieces, _weight)
	///         or the accountid prefixed by either pu| or del|.
	/// </summary>
	/// <returns></returns>
	private async Task SaveDefaultsPreference( bool doCheck = true )
	{
		Logging.WriteLogLine( "Saving Defaults" );
		var changed = false;

		if( doCheck )
			changed = CheckForChangedDefaultsAndUpdate();

		//if (CheckForChangedDefaultsAndUpdate() && !IsInDesignMode)
		if( !doCheck || changed )
		{
			if( ShipmentEntryDefaultsPreference != null )
			{
				ShipmentEntryDefaultsPreference.Description = DEFAULTS_PREFERENCE_NAME;
				ShipmentEntryDefaultsPreference.Enabled     = true;
				ShipmentEntryDefaultsPreference.DataType    = Preference.STRING;
				ShipmentEntryDefaultsPreference.StringValue = JsonConvert.SerializeObject( Defaults );
			}

			await Azure.Client.RequestUpdatePreference( ShipmentEntryDefaultsPreference );
		}
	}


	private void LoadDefaultsPreference()
	{
		if( Prefs?.Count > 0 )
		{
			//foreach( var kvp in Prefs )
			//{
			//	if( kvp != null )
			//	{
			//		Logging.WriteLogLine( "DEBUG Pref " + kvp.Description + " Id: " + kvp.Id + " StringValue: " + kvp.StringValue );
			//	}
			//}

			var p = ( from P in Prefs
					  where P.Description == DEFAULTS_PREFERENCE_NAME
					  select P ).FirstOrDefault();

			if( p != null )
			{
				Logging.WriteLogLine( "Loading defaults" );
				ShipmentEntryDefaultsPreference = p;

				Logging.WriteLogLine( "Unpacking defaults" );

				if( p.StringValue != null )
				{
					try
					{
						Defaults = JsonConvert.DeserializeObject<SortedDictionary<string, string>>( p.StringValue );
					}
					catch( Exception e )
					{
						Logging.WriteLogLine( "ERROR unpacking the defaults: " + e );
					}
				}
				else
					Logging.WriteLogLine( "No defaults" );
			}
			else
			{
				ShipmentEntryDefaultsPreference = new Preference
												  {
													  Description = DEFAULTS_PREFERENCE_NAME
												  };
			}
		}
	}

	public string GetPackageDefault()
	{
		var packageDefault = string.Empty;

		if( PackageTypes.Count > 0 )
		{
			if( ( Defaults != null ) && Defaults.ContainsKey( DEFAULT_PREFIX_PACKAGE_TYPE ) )
				packageDefault = Defaults[ DEFAULT_PREFIX_PACKAGE_TYPE ];
			else
			{
				packageDefault = (string)PackageTypes[ 0 ];
				//WhenServiceLevelChanges();
			}
		}

		return packageDefault;
	}

	public decimal GetPiecesDefault()
	{
		decimal pieces = 1;

		if( ( Defaults != null ) && Defaults.ContainsKey( DEFAULT_PREFIX_PIECES ) )
			pieces = decimal.Parse( Defaults[ DEFAULT_PREFIX_PIECES ] );

		return pieces;
	}

	public string GetServiceDefault()
	{
		var serviceDefault = string.Empty;

		if( ( ServiceLevels != null ) && ( ServiceLevels.Count > 0 ) )
		{
			if( ( Defaults != null ) && Defaults.ContainsKey( DEFAULT_PREFIX_SERVICE_LEVEL ) )
				serviceDefault = Defaults[ DEFAULT_PREFIX_SERVICE_LEVEL ];
			else
				serviceDefault = ServiceLevels[ 0 ];
		}

		WhenServiceLevelChanges();

		return serviceDefault;
	}

	public decimal GetWeightDefault()
	{
		decimal weight = 1;

		if( ( Defaults != null ) && Defaults.ContainsKey( DEFAULT_PREFIX_WEIGHT ) )
			weight = decimal.Parse( Defaults[ DEFAULT_PREFIX_WEIGHT ] );

		return weight;
	}

	public DateTime GetReadyTimeDefault()
	{
		var readyTime = DateTime.Now;

		if( ( Defaults != null ) && Defaults.ContainsKey( DEFAULT_PREFIX_READY_TIME ) )
			readyTime = DateTime.Parse( Defaults[ DEFAULT_PREFIX_READY_TIME ] );

		return readyTime;
	}

	public DateTime GetDueByTimeDefault()
	{
		var dueByTime = DateTime.Now;

		if( ( Defaults != null ) && Defaults.ContainsKey( DEFAULT_PREFIX_DUE_BY_TIME ) )
			dueByTime = DateTime.Parse( Defaults[ DEFAULT_PREFIX_DUE_BY_TIME ] );

		return dueByTime;
	}

	private void ApplyDefaultsToNoneAccountFields()
	{
		if( Defaults is {Count: > 0} )
		{
			if( Defaults.ContainsKey( DEFAULT_PREFIX_PACKAGE_TYPE ) )
			{
				if( AllPackageTypes.Contains( Defaults[ DEFAULT_PREFIX_PACKAGE_TYPE ] ) )
				{
					TripPackages[ 0 ].PackageType = Defaults[ DEFAULT_PREFIX_PACKAGE_TYPE ];
					//Logging.WriteLogLine( "DEBUG Setting packagetype to " + Defaults[ DEFAULT_PREFIX_PACKAGE_TYPE ] );
				}
			}

			if( Defaults.ContainsKey( DEFAULT_PREFIX_SERVICE_LEVEL ) )
			{
				if( ServiceLevels.Contains( Defaults[ DEFAULT_PREFIX_SERVICE_LEVEL ] ) )
				{
					ServiceLevel = Defaults[ DEFAULT_PREFIX_SERVICE_LEVEL ];
					//Logging.WriteLogLine( "DEBUG Setting servicelevel to " + Defaults[ DEFAULT_PREFIX_SERVICE_LEVEL ] );
				}
			}

			if( Defaults.ContainsKey( DEFAULT_PREFIX_PIECES ) )
			{
				TripPackages[ 0 ].Pieces = decimal.Parse( Defaults[ DEFAULT_PREFIX_PIECES ] );
				//Logging.WriteLogLine( "DEBUG Setting pieces to " + Defaults[ DEFAULT_PREFIX_PIECES ] );
			}

			if( Defaults.ContainsKey( DEFAULT_PREFIX_WEIGHT ) )
			{
				TripPackages[ 0 ].Pieces = decimal.Parse( Defaults[ DEFAULT_PREFIX_WEIGHT ] );
				//Logging.WriteLogLine( "DEBUG Setting weight to " + Defaults[ DEFAULT_PREFIX_WEIGHT ] );
			}

			if( Defaults.ContainsKey( DEFAULT_PREFIX_READY_TIME ) )
			{
				var Success = DateTime.TryParse( Defaults[ DEFAULT_PREFIX_READY_TIME ], out var Time );

				if( Success )
				{
					ReadyDate = ReadyTime = Time;
					//Logging.WriteLogLine( "DEBUG Setting ready date/time to " + Defaults[ DEFAULT_PREFIX_READY_TIME ] );
				}
				else
				{
					ReadyDate = ReadyTime = DateTime.Now;
					//Logging.WriteLogLine( "Error converting ready date/time '" + Defaults[ DEFAULT_PREFIX_READY_TIME ] + "' - Setting to " + ReadyDate );
				}
			}

			if( Defaults.ContainsKey( DEFAULT_PREFIX_DUE_BY_TIME ) )
			{
				var Success = DateTime.TryParse( Defaults[ DEFAULT_PREFIX_DUE_BY_TIME ], out var Time );

				if( Success )
				{
					DueDate = DueTime = Time;
					//Logging.WriteLogLine( "DEBUG Setting ready date/time to " + Defaults[ DEFAULT_PREFIX_DUE_BY_TIME ] );
				}
				else
				{
					DueDate = DueTime = DateTime.Now;
					//Logging.WriteLogLine( "Error converting ready date/time '" + Defaults[ DEFAULT_PREFIX_DUE_BY_TIME ] + "' - Setting to " + DueDate );
				}
			}
		}
		//else
		//      {
		//	// Testing
		//	TripPackages[0].PackageType = "Tubs";
		//	ServiceLevel = "Hourly";
		//	TripPackages[0].Pieces = 5;
		//	TripPackages[0].Weight = 50;
		//      }
	}


	/// <summary>
	///     Defaults override the pickup address preference.
	/// </summary>
	private async Task ApplyDefaultsToAddressFields()
	{
		if( !await ApplyShippingAddressToPickupAddressPreference( SelectedAccount ) )
			ClearPickupAddress();

		if( Defaults is {Count: > 0} )
		{
			string name;
			name = DEFAULT_PREFIX_PICKUP + SelectedAccount;

			if( Defaults.ContainsKey( name ) )
				SelectedPickupAddress = Defaults[ name ];

			name = DEFAULT_PREFIX_DELIVERY + SelectedAccount;

			if( Defaults.ContainsKey( name ) )
				SelectedDeliveryAddress = Defaults[ name ];
			//else
			//{
			//	ClearDeliveryAddress();
			//}
		}
		else
			ClearDeliveryAddress();
	}
	//private void ApplyDefaultsToAddressFields()
	//{
	//	bool applied = ApplyShippingAddressToPickupAddressPreference(SelectedAccount);
	//	if( Defaults != null && Defaults.Count > 0 )
	//	{
	//		string name;
	//		if (!applied)
	//		{
	//			name = DEFAULT_PREFIX_PICKUP + SelectedAccount;

	//			if( Defaults.ContainsKey( name ) )
	//			{
	//				SelectedPickupAddress = Defaults[ name ];
	//			}
	//			else
	//			{
	//				ClearPickupAddress();
	//			}
	//		}

	//		name = DEFAULT_PREFIX_DELIVERY + SelectedAccount;

	//		if( Defaults.ContainsKey( name ) )
	//		{
	//			SelectedDeliveryAddress = Defaults[ name ];
	//		}
	//		else
	//		{
	//			ClearDeliveryAddress();
	//		}
	//	}
	//	else
	//       {
	//           ClearPickupAddress();
	//		ClearDeliveryAddress();
	//       }
	//}

	private async Task<bool> ApplyShippingAddressToPickupAddressPreference( string account )
	{
		var applied = false;

		if( Prefs is {Count: > 0} )
		{
			var pref = ( from P in Prefs
					     where P.Description.Contains( "Make Account’s Shipping Address default Pick Up Address" )
					     select P ).FirstOrDefault();

			if( pref is {Enabled: true} )
			{
				if( !IsInDesignMode )
				{
					await Task.Run( async () =>
								    {
									    var company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( account );

									    if( company != null )
									    {
										    SelectedPickupAddress = company.ShippingCompany.CompanyName;

										    if( !CompanyNames.Contains( SelectedPickupAddress ) )
										    {
											    var tmp = SelectedPickupAddress + "'";

											    if( UseGlobalAddressBook && GlobalAddresses.ContainsKey( tmp.ToLower() ) )
											    {
												    Logging.WriteLogLine( "Setting default pickup address to global address " + tmp );
												    SelectedPickupAddress = tmp;
											    }
										    }

										    Logging.WriteLogLine( "Setting default pickup address to " + account + "'s ShippingCompany" );
										    applied = true;
									    }
								    } );
				}
			}
		}

		return applied;
	}

	private void ClearPickupAddress()
	{
		PickupAddressNotes = string.Empty;
		PickupCity         = string.Empty;
		PickupCompany      = string.Empty;
		PickupLocation     = string.Empty;
		PickupName         = string.Empty;
		PickupNotes        = string.Empty;
		PickupPhone        = string.Empty;
		PickupPostalZip    = string.Empty;
		PickupProvState    = string.Empty;
		PickupStreet       = string.Empty;
		PickupSuite        = string.Empty;
		PickupZone         = string.Empty;
	}

	private void ClearDeliveryAddress()
	{
		DeliveryAddressNotes = string.Empty;
		DeliveryCity         = string.Empty;
		DeliveryCompany      = string.Empty;
		DeliveryLocation     = string.Empty;
		DeliveryName         = string.Empty;
		DeliveryNotes        = string.Empty;
		DeliveryPhone        = string.Empty;
		DeliveryPostalZip    = string.Empty;
		DeliveryProvState    = string.Empty;
		DeliveryStreet       = string.Empty;
		DeliverySuite        = string.Empty;
		DeliveryZone         = string.Empty;
	}

	/// <summary>
	///     NOTE: Assumes that the name has been properly constructed.
	///     That is, pickup address ids have been prefixed with DefaultPrefix_Pickup
	///     and the same with delivery address ids.
	/// </summary>
	/// <param
	///     name="name">
	/// </param>
	/// <param
	///     name="value">
	/// </param>
	private void UpdateDefault( string name, string value )
	{
		Logging.WriteLogLine( "Updating default " + name + " to " + value );

		if( ( Defaults != null ) && Defaults.ContainsKey( name ) )
			Defaults[ name ] = value;
		else
		{
			Defaults ??= new SortedDictionary<string, string>();
			Defaults.Add( name, value );
		}
	}

	public bool IsPickupAddressDefaultChecked
	{
		get => Get( () => IsPickupAddressDefaultChecked, false );
		set => Set( () => IsPickupAddressDefaultChecked, value );
	}

	public bool IsDeliveryAddressDefaultChecked
	{
		get => Get( () => IsDeliveryAddressDefaultChecked, false );
		set => Set( () => IsDeliveryAddressDefaultChecked, value );
	}

	public bool IsPackageTypeDefaultChecked
	{
		get => Get( () => IsPackageTypeDefaultChecked, false );
		set => Set( () => IsPackageTypeDefaultChecked, value );
	}

	public bool IsServiceLevelDefaultChecked
	{
		get => Get( () => IsServiceLevelDefaultChecked, false );
		set => Set( () => IsServiceLevelDefaultChecked, value );
	}

	public bool IsPiecesDefaultChecked
	{
		get => Get( () => IsPiecesDefaultChecked, false );
		set => Set( () => IsPiecesDefaultChecked, value );
	}

	public bool IsWeightDefaultChecked
	{
		get => Get( () => IsWeightDefaultChecked, false );
		set => Set( () => IsWeightDefaultChecked, value );
	}

	public bool IsReadyTimeDefaultChecked
	{
		get => Get( () => IsReadyTimeDefaultChecked, false );
		set => Set( () => IsReadyTimeDefaultChecked, value );
	}

	public bool IsDueByTimeDefaultChecked
	{
		get => Get( () => IsDueByTimeDefaultChecked, false );
		set => Set( () => IsDueByTimeDefaultChecked, value );
	}

	[DependsUpon( nameof( IsPickupAddressDefaultChecked ) )]
	[DependsUpon( nameof( IsDeliveryAddressDefaultChecked ) )]
	[DependsUpon( nameof( IsPackageTypeDefaultChecked ) )]
	[DependsUpon( nameof( IsServiceLevelDefaultChecked ) )]
	[DependsUpon( nameof( IsPiecesDefaultChecked ) )]
	[DependsUpon( nameof( IsWeightDefaultChecked ) )]
	[DependsUpon( nameof( IsReadyTimeDefaultChecked ) )]
	[DependsUpon( nameof( IsDueByTimeDefaultChecked ) )]
	public void WhenDefaultsChange()
	{
		var message = "DEBUG\n\tIsPickupAddressDefaultChecked: " + IsPickupAddressDefaultChecked
															     + "\n\tIsDeliverAddressDefaultChecked: " + IsDeliveryAddressDefaultChecked
															     + "\n\tIsPackageTypeDefaultChecked: " + IsPackageTypeDefaultChecked
															     + "\n\tIsServiceLevelDefaultChecked: " + IsServiceLevelDefaultChecked
															     + "\n\tIsPiecesDefaultChecked: " + IsPiecesDefaultChecked
															     + "\n\tIsWeightDefaultChecked: " + IsWeightDefaultChecked
															     + "\n\tIsReadyTimeDefaultChecked: " + IsReadyTimeDefaultChecked
															     + "\n\tIsDueByTimeDefaultChecked: " + IsDueByTimeDefaultChecked + "\n";
		Logging.WriteLogLine( message );
	}

	public bool CheckForChangedDefaultsAndUpdate()
	{
		var yes = false;

		if( Defaults == null )
		{
			Defaults = new SortedDictionary<string, string>();
			yes      = true;
		}

		if( IsPickupAddressDefaultChecked )
		{
			var name = DEFAULT_PREFIX_PICKUP + SelectedAccount;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != PickupCompany ) ) )
			{
				yes = true;
				UpdateDefault( name, PickupCompany );
			}
		}

		if( IsDeliveryAddressDefaultChecked )
		{
			var name = DEFAULT_PREFIX_DELIVERY + SelectedAccount;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != DeliveryCompany ) ) )
			{
				yes = true;
				UpdateDefault( name, DeliveryCompany );
			}
		}

		if( IsPackageTypeDefaultChecked )
		{
			var name = DEFAULT_PREFIX_PACKAGE_TYPE;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != PackageType ) ) )
			{
				yes = true;
				UpdateDefault( name, PackageType );
			}
		}

		if( IsServiceLevelDefaultChecked )
		{
			var name = DEFAULT_PREFIX_SERVICE_LEVEL;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != ServiceLevel ) ) )
			{
				yes = true;
				UpdateDefault( name, ServiceLevel );
			}
		}

		if( IsPiecesDefaultChecked )
		{
			var name = DEFAULT_PREFIX_PIECES;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != TripPackages[ 0 ].Pieces.ToString() ) ) )
			{
				yes = true;
				UpdateDefault( name, TripPackages[ 0 ].Pieces.ToString() );
			}
		}

		if( IsWeightDefaultChecked )
		{
			var name = DEFAULT_PREFIX_WEIGHT;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != TripPackages[ 0 ].Weight.ToString() ) ) )
			{
				yes = true;
				UpdateDefault( name, TripPackages[ 0 ].Weight.ToString() );
			}
		}

		if( IsReadyTimeDefaultChecked )
		{
			var time = new DateTime( ReadyDate.Year, ReadyDate.Month, ReadyDate.Day, ReadyTime.Hour, ReadyTime.Minute, ReadyTime.Second );
			var name = DEFAULT_PREFIX_READY_TIME;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != time.ToLongTimeString() ) ) )
			{
				yes = true;
				UpdateDefault( name, time.ToString() );
			}
		}

		if( IsDueByTimeDefaultChecked )
		{
			var time = new DateTime( DueDate.Year, DueDate.Month, DueDate.Day, DueTime.Hour, DueTime.Minute, DueTime.Second );
			var name = DEFAULT_PREFIX_DUE_BY_TIME;

			if( !Defaults.ContainsKey( name ) || ( Defaults.ContainsKey( name ) && ( Defaults[ name ] != time.ToLongTimeString() ) ) )
			{
				yes = true;
				UpdateDefault( name, time.ToString() );
			}
		}

		return yes;
	}
#endregion

#region Trip Totals
	public decimal TotalDeliveryCharges
	{
		//get { return Get(() => TotalDeliveryCharges, string.Parse("0", System.Globalization.NumberStyles.Currency)); }
		get { return Get( () => TotalDeliveryCharges, 0 ); }
		set { Set( () => TotalDeliveryCharges, value ); }
	}


	public string TotalDeliveryChargesString
	{
		get { return Get( () => TotalDeliveryChargesString, TotalDeliveryCharges.ToString( "C" ) ); }
		set { Set( () => TotalDeliveryChargesString, value ); }
	}

	public decimal TotalCharges
	{
		get { return Get( () => TotalCharges, 0 ); }
		set { Set( () => TotalCharges, value ); }
	}


	public string TotalChargesString
	{
		get { return Get( () => TotalChargesString, TotalCharges.ToString( "C" ) ); }
		set { Set( () => TotalChargesString, value ); }
	}


	public decimal TotalFuelSurcharges
	{
		get { return Get( () => TotalFuelSurcharges, 0 ); }
		set { Set( () => TotalFuelSurcharges, value ); }
	}


	public string TotalFuelSurchargesString
	{
		get { return Get( () => TotalFuelSurchargesString, TotalFuelSurcharges.ToString( "C" ) ); }
		set { Set( () => TotalFuelSurchargesString, value ); }
	}


	public decimal TotalDiscount
	{
		get { return Get( () => TotalDiscount, 0 ); }
		set { Set( () => TotalDiscount, value ); }
	}


	public string TotalDiscountString
	{
		get { return Get( () => TotalDiscountString, TotalDiscount.ToString( "C" ) ); }
		set { Set( () => TotalDiscountString, value ); }
	}

	public decimal TotalBeforeTax
	{
		get { return Get( () => TotalBeforeTax, 0 ); }
		set { Set( () => TotalBeforeTax, value ); }
	}


	public string TotalBeforeTaxString
	{
		get { return Get( () => TotalBeforeTaxString, TotalBeforeTax.ToString( "C" ) ); }
		set { Set( () => TotalBeforeTaxString, value ); }
	}


	public decimal TotalTaxes
	{
		get { return Get( () => TotalTaxes, 0 ); }
		set { Set( () => TotalTaxes, value ); }
	}


	public string TotalTaxesString
	{
		get { return Get( () => TotalTaxesString, TotalTaxes.ToString( "C" ) ); }
		set { Set( () => TotalTaxesString, value ); }
	}


	public decimal TotalTotal
	{
		get { return Get( () => TotalTotal, 0 ); }
		set { Set( () => TotalTotal, value ); }
	}


	public string TotalTotalString
	{
		get { return Get( () => TotalTotalString, TotalTotal.ToString( "C" ) ); }
		set { Set( () => TotalTotalString, value ); }
	}


	public decimal TotalPayroll
	{
		get { return Get( () => TotalPayroll, 0 ); }
		set { Set( () => TotalPayroll, value ); }
	}


	public string TotalPayrollString
	{
		get { return Get( () => TotalPayrollString, TotalPayroll.ToString( "C" ) ); }
		set { Set( () => TotalPayrollString, value ); }
	}

	[DependsUpon( nameof( TotalDeliveryCharges ) )]
	[DependsUpon( nameof( TotalCharges ) )]
	[DependsUpon( nameof( TotalFuelSurcharges ) )]
	[DependsUpon( nameof( TotalDiscount ) )]
	[DependsUpon( nameof( TotalBeforeTax ) )]
	[DependsUpon( nameof( TotalTaxes ) )]
	[DependsUpon( nameof( TotalTotal ) )]
	[DependsUpon( nameof( TotalPayroll ) )]
	public void WhenTotalsChange()
	{
		TotalDeliveryChargesString = TotalDeliveryCharges.ToString( "C" );
		TotalChargesString         = TotalCharges.ToString( "C" );
		TotalFuelSurchargesString  = TotalFuelSurcharges.ToString( "C" );
		TotalDiscountString        = TotalDiscount.ToString( "C" );
		TotalBeforeTaxString       = TotalBeforeTax.ToString( "C" );
		TotalTaxesString           = TotalTaxes.ToString( "C" );
		TotalTotalString           = TotalTotal.ToString( "C" );
		TotalPayrollString         = TotalPayroll.ToString( "C" );

		//Logging.WriteLogLine( "DEBUG TotalTotal: " + TotalTotal );
	}

	[DependsUpon( nameof( TotalDeliveryCharges ) )]
	public void WhenTotalDeliveryChargesChanges()
	{
		//if (TotalDeliveryCharges == 0)
		//         {
		//	Logging.WriteLogLine("DEBUG DeliveryCharges is 0 - setting other totals to 0 and clearing TripCharges");
		//	TotalCharges = TotalFuelSurcharges = TotalDiscount = TotalBeforeTax = TotalTaxes = TotalTotal = TotalPayroll = 0;
		//	TripCharges.Clear();
		//         }
		//else
		//         {
		//	View?.BtnCalculate_Click(null, null);
		//         }
		View?.BtnCalculate_Click( null, null );
	}
#endregion


#region Jumping
	public enum SECTIONS
	{
		BILLING,
		PICKUP,
		DELIVERY,
		PACKAGE,
		CHARGES,
		PICKUP_NOTES,
		WEIGHT,
		SERVICES,
		DELIVERY_NOTES,
		REFERENCE
	}


	public SECTIONS CurrentSection
	{
		// Setting it to this so that the first Alt-X goes to the Billing section
		get => Get( () => CurrentSection, SECTIONS.DELIVERY_NOTES );
		set => Set( () => CurrentSection, value );
	}
#endregion
#endregion

#region Actions
	/// <summary>
	///     Performs a deep clone of the source trip.
	/// </summary>
	/// <param
	///     name="src">
	/// </param>
	/// <returns></returns>
	private static DisplayTrip Clone( DisplayTrip src )
	{
		var Dt = new DisplayTrip
				 {
					 //Logging.WriteLogLine( "Cloning" );
					 AccountId = src.AccountId,
					 //dt.Background = src.Background;
					 Barcode                     = src.Barcode,
					 BillingAccountId            = src.BillingAccountId,
					 BillingAddressAddressLine1  = src.BillingAddressAddressLine1,
					 BillingAddressAddressLine2  = src.BillingAddressAddressLine2,
					 BillingAddressBarcode       = src.BillingAddressBarcode,
					 BillingAddressCity          = src.BillingAddressCity,
					 BillingAddressCountry       = src.BillingAddressCountry,
					 BillingAddressCountryCode   = src.BillingAddressCountryCode,
					 BillingAddressEmailAddress  = src.BillingAddressEmailAddress,
					 BillingAddressEmailAddress1 = src.BillingAddressEmailAddress1,
					 BillingAddressEmailAddress2 = src.BillingAddressEmailAddress2,
					 BillingAddressFax           = src.BillingAddressFax,
					 BillingAddressLatitude      = src.BillingAddressLatitude,
					 BillingAddressLongitude     = src.BillingAddressLongitude,
					 BillingAddressMobile        = src.BillingAddressMobile,
					 BillingAddressMobile1       = src.BillingAddressMobile1,
					 BillingAddressNotes         = src.BillingAddressNotes,
					 BillingAddressPhone         = src.BillingAddressPhone,
					 BillingAddressPhone1        = src.BillingAddressPhone1,
					 BillingAddressPostalBarcode = src.BillingAddressPostalBarcode,
					 BillingAddressPostalCode    = src.BillingAddressPostalCode,
					 BillingAddressRegion        = src.BillingAddressRegion,
					 BillingAddressSuite         = src.BillingAddressSuite,
					 BillingAddressVicinity      = src.BillingAddressVicinity,
					 BillingCompanyName          = src.BillingCompanyName,
					 BillingContact              = src.BillingContact,
					 BillingNotes                = src.BillingNotes,
					 Board                       = src.Board,
					 BroadcastToDispatchBoard    = src.BroadcastToDispatchBoard,
					 BroadcastToDriver           = src.BroadcastToDriver,
					 BroadcastToDriverBoard      = src.BroadcastToDriverBoard,
					 CallerEmail                 = src.CallerEmail,
					 CallerName                  = src.CallerName,
					 CallerPhone                 = src.CallerPhone,
					 CallTakerId                 = src.CallTakerId,
					 CallTime                    = src.CallTime,
					 ClaimLatitude               = src.ClaimLatitude,
					 ClaimLongitude              = src.ClaimLongitude,
					 ClaimTime                   = src.ClaimTime,
					 CurrentZone                 = src.CurrentZone,
					 DangerousGoods              = src.DangerousGoods,
					 //dt.Delivered = src.Delivered;
					 DeliveryAccountId            = src.DeliveryAccountId,
					 DeliveryAddressAddressLine1  = src.DeliveryAddressAddressLine1,
					 DeliveryAddressAddressLine2  = src.DeliveryAddressAddressLine2,
					 DeliveryAddressBarcode       = src.DeliveryAddressBarcode,
					 DeliveryAddressCity          = src.DeliveryAddressCity,
					 DeliveryAddressCountry       = src.DeliveryAddressCountry,
					 DeliveryAddressCountryCode   = src.DeliveryAddressCountryCode,
					 DeliveryAddressEmailAddress  = src.DeliveryAddressEmailAddress,
					 DeliveryAddressEmailAddress1 = src.DeliveryAddressEmailAddress1,
					 DeliveryAddressEmailAddress2 = src.DeliveryAddressEmailAddress2,
					 DeliveryAddressFax           = src.DeliveryAddressFax,
					 DeliveryAddressLatitude      = src.DeliveryAddressLatitude,
					 DeliveryAddressLongitude     = src.DeliveryAddressLongitude,
					 DeliveryAddressMobile        = src.DeliveryAddressMobile,
					 DeliveryAddressMobile1       = src.DeliveryAddressMobile1,
					 DeliveryAddressNotes         = src.DeliveryAddressNotes,
					 DeliveryAddressPhone         = src.DeliveryAddressPhone,
					 DeliveryAddressPhone1        = src.DeliveryAddressPhone1,
					 DeliveryAddressPostalBarcode = src.DeliveryAddressPostalBarcode,
					 DeliveryAddressPostalCode    = src.DeliveryAddressPostalCode,
					 DeliveryAddressRegion        = src.DeliveryAddressRegion,
					 DeliveryAddressSuite         = src.DeliveryAddressSuite,
					 DeliveryAddressVicinity      = src.DeliveryAddressVicinity,
					 DeliveryCompanyName          = src.DeliveryCompanyName,
					 DeliveryContact              = src.DeliveryContact,
					 DeliveryLatitude             = src.DeliveryLatitude,
					 DeliveryLongitude            = src.DeliveryLongitude,
					 DeliveryNotes                = src.DeliveryNotes,
					 DeliveryTime                 = src.DeliveryTime,
					 DeliveryZone                 = src.DeliveryZone,
					 DetailsVisible               = src.DetailsVisible,
					 Driver                       = src.Driver,
					 DueTime                      = src.DueTime,
					 EnableAccept                 = src.EnableAccept,
					 ExtensionAsJson              = src.ExtensionAsJson,
					 Filter                       = src.Filter,
					 HasDocuments                 = src.HasDocuments,
					 //dt.HasPackages = src.HasPackages;
					 IsQuote       = src.IsQuote,
					 IsVisible     = src.IsVisible,
					 LastModified  = src.LastModified,
					 Location      = src.Location,
					 Measurement   = src.Measurement,
					 MissingPieces = src.MissingPieces,
					 //dt.NotReceivedByDevice = src.NotReceivedByDevice;
					 Ok                 = src.Ok,
					 OriginalPieceCount = src.OriginalPieceCount
				 };

		// dt.Packages = src.Packages;
		if( src.Packages.Count > 0 )
		{
			foreach( var SrcDp in src.Packages )
			{
				var Dp = new DisplayPackage( new TripPackage() ) {DetailsVisible = SrcDp.DetailsVisible, HasDocuments = SrcDp.HasDocuments, Height = SrcDp.Height};

				foreach( var SrcTi in SrcDp.Items )
				{
					var Ti = new TripItem
							 {
								 Barcode      = SrcTi.Barcode,
								 Barcode1     = SrcTi.Barcode1,
								 Description  = SrcTi.Description,
								 HasDocuments = SrcTi.HasDocuments,
								 Height       = SrcTi.Height,
								 ItemCode     = SrcTi.ItemCode,
								 ItemCode1    = SrcTi.ItemCode1,
								 Length       = SrcTi.Length,
								 Original     = SrcTi.Original,
								 Pieces       = SrcTi.Pieces,
								 Qh           = SrcTi.Qh,
								 Reference    = SrcTi.Reference,
								 Tax1         = SrcTi.Tax1,
								 Tax2         = SrcTi.Tax2,
								 Value        = SrcTi.Value,
								 Volume       = SrcTi.Volume,
								 Weight       = SrcTi.Weight,
								 Width        = SrcTi.Weight
							 };
					Ti.Width = SrcTi.Width;

					Dp.Items.Add( Ti );
				}

				Dp.Length      = SrcDp.Length;
				Dp.Original    = SrcDp.Original;
				Dp.PackageType = SrcDp.PackageType;
				Dp.Pieces      = SrcDp.Pieces;
				Dp.Qh          = SrcDp.Qh;
				Dp.Tax1        = SrcDp.Tax1;
				Dp.Tax2        = SrcDp.Tax2;
				Dp.Value       = SrcDp.Value;
				Dp.Volume      = SrcDp.Volume;
				Dp.Weight      = SrcDp.Weight;
				Dp.Width       = SrcDp.Width;

				Dt.Packages.Add( Dp );
			}
		}

		Dt.PackageType                = src.PackageType;
		Dt.PickupAccountId            = src.PickupAccountId;
		Dt.PickupAddressAddressLine1  = src.PickupAddressAddressLine1;
		Dt.PickupAddressAddressLine2  = src.PickupAddressAddressLine2;
		Dt.PickupAddressBarcode       = src.PickupAddressBarcode;
		Dt.PickupAddressCity          = src.PickupAddressCity;
		Dt.PickupAddressCountry       = src.PickupAddressCountry;
		Dt.PickupAddressCountryCode   = src.PickupAddressCountryCode;
		Dt.PickupAddressEmailAddress  = src.PickupAddressEmailAddress;
		Dt.PickupAddressEmailAddress1 = src.PickupAddressEmailAddress1;
		Dt.PickupAddressEmailAddress2 = src.PickupAddressEmailAddress2;
		Dt.PickupAddressFax           = src.PickupAddressFax;
		Dt.PickupAddressLatitude      = src.PickupAddressLatitude;
		Dt.PickupAddressLongitude     = src.PickupAddressLongitude;
		Dt.PickupAddressMobile        = src.PickupAddressMobile;
		Dt.PickupAddressMobile1       = src.PickupAddressMobile1;
		Dt.PickupAddressNotes         = src.PickupAddressNotes;
		Dt.PickupAddressPhone         = src.PickupAddressPhone;
		Dt.PickupAddressPhone1        = src.PickupAddressPhone1;
		Dt.PickupAddressPostalBarcode = src.PickupAddressPostalBarcode;
		Dt.PickupAddressPostalCode    = src.PickupAddressPostalCode;
		Dt.PickupAddressRegion        = src.PickupAddressRegion;
		Dt.PickupAddressSuite         = src.PickupAddressSuite;
		Dt.PickupAddressVicinity      = src.PickupAddressVicinity;
		Dt.PickupCompanyName          = src.PickupCompanyName;
		Dt.PickupContact              = src.PickupContact;
		Dt.PickupLatitude             = src.PickupLatitude;
		Dt.PickupLongitude            = src.PickupLongitude;
		Dt.PickupNotes                = src.PickupNotes;
		Dt.PickupTime                 = src.PickupTime;
		Dt.PickupZone                 = src.PickupZone;
		Dt.Pieces                     = src.Pieces;
		Dt.POD                        = src.POD;
		Dt.POP                        = src.POP;
		Dt.Program                    = src.Program;
		Dt.ReadByDriver               = src.ReadByDriver;
		Dt.ReadyTime                  = src.ReadyTime;
		Dt.ReceivedByDevice           = src.ReceivedByDevice;
		Dt.Reference                  = src.Reference;
		Dt.Selected                   = src.Selected;
		Dt.ServiceLevel               = src.ServiceLevel;

		//dt.Signatures = src.Signatures;
		foreach( var SrcSig in src.Signatures )
		{
			var Sig = new Signature
					  {
						  Date    = SrcSig.Date,
						  Height  = SrcSig.Height,
						  Points  = SrcSig.Points,
						  Status  = SrcSig.Status,
						  Status1 = SrcSig.Status1,
						  Status2 = SrcSig.Status2,
						  Width   = SrcSig.Width
					  };

			Dt.Signatures.Add( Sig );
		}

		Dt.Status  = src.Status;
		Dt.Status1 = src.Status1;
		Dt.Status2 = src.Status2;

		//dt.TripCharges = src.TripCharges;
		foreach( var SrcTc in src.TripCharges )
		{
			var Tc = new TripCharge {ChargeId = SrcTc.ChargeId, Quantity = SrcTc.Quantity, Text = SrcTc.Text, Value = SrcTc.Value};

			Dt.TripCharges.Add( Tc );
		}

		Dt.TripId             = src.TripId;
		Dt.UNClass            = src.UNClass;
		Dt.UndeliverableNotes = src.UndeliverableNotes;
		Dt.VerifiedLatitude   = src.VerifiedLatitude;
		Dt.VerifiedLongitude  = src.VerifiedLongitude;
		Dt.VerifiedTime       = src.VerifiedTime;
		Dt.Volume             = src.Volume;
		Dt.Weight             = src.Weight;

		Dt.DeliveryArriveTime        = src.DeliveryArriveTime;
		Dt.DeliveryWaitTimeStart     = src.DeliveryWaitTimeStart;
		Dt.DeliveryWaitTimeInSeconds = src.DeliveryWaitTimeInSeconds;

		Dt.PickupArriveTime        = src.PickupArriveTime;
		Dt.PickupWaitTimeStart     = src.PickupWaitTimeStart;
		Dt.PickupWaitTimeInSeconds = src.PickupWaitTimeInSeconds;

		return Dt;
	}

#region Trip Change
	private bool InTripChange;

	//[DependsUpon(nameof(CurrentTrip))]
	//public async Task WhenCurrentTripChanges(DisplayTrip? dt = null)
	public async Task WhenCurrentTripChanges( bool loadNewCopy = true )
	{
		if( !InTripChange )
		{
			InTripChange = true;

			try
			{
				Loaded = false;

				//if (dt != null)
				//{
				//    CurrentTrip = dt;
				//}
				if( CurrentTrip != null )
				{
					IsNewTrip = false;
					Logging.WriteLogLine( "CurrentTrip has changed: " + CurrentTrip.TripId + " CallTakerId: " + CurrentTrip.CallTakerId );

					//Logging.WriteLogLine( "DEBUG CurrentTime CallDate/Time: " + CurrentTrip.Ct?.LocalDateTime );
					//Logging.WriteLogLine( "DEBUG CurrentTime ReadyTime: " + CurrentTrip.ReadyTime?.LocalDateTime );
					//Logging.WriteLogLine( "DEBUG CurrentTime DueTime: " + CurrentTrip.DueTime?.LocalDateTime );

					var DtCopy = Clone( CurrentTrip );

					SelectedAccount = DtCopy.AccountId;

					if( loadNewCopy )
					{
						Logging.WriteLogLine( "Loading new copy of trip" );

						var Latest = await Azure.Client.RequestGetTrip( new GetTrip
																	    {
																		    Signatures = true,
																		    TripId     = CurrentTrip.TripId
																	    } );

						if( Latest.Signatures.Count > 0 )
						{
							Logging.WriteLogLine( "Trip has signatures: " + Latest.Signatures.Count );
							DtCopy.Signatures = Latest.Signatures;
						}

						if( DtCopy.CallTakerId.IsNullOrWhiteSpace() && Latest.CallTakerId.IsNotNullOrWhiteSpace() )
						{
							Logging.WriteLogLine( "Setting CallTakerId to " + Latest.CallTakerId );
							DtCopy.CallTakerId = Latest.CallTakerId;
						}

						if( Latest.POP.IsNotNullOrWhiteSpace() )
							DtCopy.POP = Latest.POP;

						if( Latest.POD.IsNotNullOrWhiteSpace() )
							DtCopy.POD = Latest.POD;

						if( Latest.DriverNotes.IsNotNullOrWhiteSpace() )
							DtCopy.DriverNotes = Latest.DriverNotes;

                        TotalTotal = DtCopy.TotalAmount = Latest.TotalAmount;
                        TotalTaxes = DtCopy.TotalTaxAmount = Latest.TotalTaxAmount;
                        TotalPayroll = DtCopy.TotalPayrollAmount = Latest.TotalPayrollAmount;
                        TotalBeforeTax = DtCopy.TotalFixedAmount = Latest.TotalFixedAmount;
						Logging.WriteLogLine($"DEBUG Pallets: {Latest.Pallets}");

                        //Logging.WriteLogLine( "DEBUG CurrentTrip.TripId: " + CurrentTrip?.TripId + "DtCopy.TripId: " + DtCopy.TripId );
                        Logging.WriteLogLine( "DEBUG DtCopy.Packages.Count: " + DtCopy.Packages.Count );
						CurrentTrip = DtCopy;
					}
					else
						Logging.WriteLogLine( "NOT Loading new copy of trip - coming from another tab" );

					View?.LoadTrip( DtCopy );

					LookForCompanyDataInOtherTabs();

					//Task.WaitAll(Task.Run(() =>
					//{
					await Dispatcher.Invoke( async () =>
										     {
											     //Execute_ClearTripEntry(true);
											     //View?.LoadTrip( DtCopy );

											     // Need to force the loading in order that the address combos are populated
											     //WhenAccountChanges(dtCopy.AccountId);
											     Task.WaitAll(
														      Task.Run( async () =>
																        {
																	        Logging.WriteLogLine( "DEBUG Loading companies" );

																	        //var Csl     = await Azure.Client.RequestGetCustomerCompaniesSummary( DtCopy.AccountId );
																	        //var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( DtCopy.AccountId );
																	        var Csl     = await GetCompanySummaryForAccountId( DtCopy.AccountId );
																	        var Details = await GetCompanyDetailListForAccountId( DtCopy.AccountId );

																	        Logging.WriteLogLine( "DEBUG Finished downloading companies" );

																	        DetailedAddresses = new Dictionary<string, CompanyDetail>();

																	        foreach( var Detail in Details )
																	        {
																		        if( Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && !DetailedAddresses.ContainsKey( Detail.Company.CompanyName.ToLower() ) )
																		        {
																			        try
																			        {
																				        DetailedAddresses.Add( Detail.Company.CompanyName.ToLower(), Detail );
																			        }
																			        catch( Exception )
																			        {
																				        Logging.WriteLogLine( "Address already in collection: " + Detail.Company.CompanyName.ToLower() );
																			        }
																		        }
																	        }

																	        var Addrs = new List<CompanyByAccountSummary>();
																	        var Names = new List<string>();
																	        var Sd    = new SortedDictionary<string, CompanyByAccountSummary>();

																	        foreach( var Cs in Csl )
																	        {
																		        if( Cs.CompanyName.IsNotNullOrWhiteSpace() && !Sd.ContainsKey( Cs.CompanyName.ToLower() ) )
																			        Sd.Add( Cs.CompanyName.ToLower(), Cs );
																	        }

																	        foreach( var detail in Details )
																	        {
																		        CompanyByAccountSummary cbas = new()
																									           {
																										           AddressLine1       = detail.Address.AddressLine1,
																										           AddressLine2       = detail.Address.AddressLine2,
																										           City               = detail.Address.City,
																										           CompanyName        = detail.Address.CompanyName,
																										           CountryCode        = detail.Address.CountryCode,
																										           CustomerCode       = detail.Address.CompanyNumber,
																										           DisplayCompanyName = detail.Address.CompanyName,
																										           Enabled            = detail.Company.Enabled,
																										           Found              = false, // TODO
																										           LocationBarcode    = detail.Address.LocationBarcode,
																										           PostalCode         = detail.Address.PostalCode,
																										           Region             = detail.Address.Region,
																										           Suite              = detail.Address.Suite
																									           };
																																						        
																		        if( !Sd.ContainsKey( cbas.CompanyName.ToLower() ) )
																			        Sd.Add( cbas.CompanyName.ToLower(), cbas );
																	        }
																			Sd = ChooseAddressBook( Sd );
																			//(Sd, Addrs, Names) = ChooseAddressBook( Sd );

																			foreach (var Key in Sd.Keys)
																			{
																				Addrs.Add(Sd[Key]);
																				Names.Add(Sd[Key].CompanyName);
																			}

																			Addresses    = Addrs;
																	        AddressNames = Names;
																	        //Loaded       = true;
																	        Logging.WriteLogLine( "DEBUG Done loading companies" );
																        } )
														     );

											     
											     Logging.WriteLogLine( "DEBUG Loading the rest of the trip" );
											     Status1        = (int)DtCopy.Status1;
											     DriverCode     = DtCopy.Driver;
											     SelectedDriver = DriverCode.ToLower();
											     TPU            = DtCopy.PickupTime.ToString();
											     TDEL           = DtCopy.DeliveryTime.ToString();
											     TVER           = DtCopy.VerifiedTime.ToString();

											     TripId = DtCopy.TripId;

											     
											     Account  = DtCopy.AccountId;
											     Customer = DtCopy.AccountId;

											     if( CustomerPhone.IsNullOrWhiteSpace() && DtCopy.BillingAddressPhone.IsNullOrWhiteSpace() )
												     CustomerPhone = DtCopy.BillingAddressPhone;
											     CustomerNotes = DtCopy.BillingAddressNotes;
											     Reference     = DtCopy.Reference;
											     CallTakerId   = DtCopy.CallTakerId;

											     var Ca = await GetCompanyAddress( DtCopy.AccountId ); // Force the population of country and region

											     if( ( Ca != null ) && Ca.Notes.IsNotNullOrWhiteSpace() )
												     CustomerNotes = Ca.Notes;

											     Reference = DtCopy.Reference;

											     CallName  = DtCopy.CallerName;
											     CallPhone = DtCopy.CallerPhone;
											     CallEmail = DtCopy.CallerEmail;

											     if( !string.IsNullOrEmpty( DtCopy.CallTime.ToString() ) )
											     {
												     CallDate = DtCopy.Ct?.DateTime ?? DateTime.MinValue;

												     CallTime = DtCopy.Ct?.DateTime ?? DateTime.MinValue;
											     }

											     IsQuote            = DtCopy.IsQuote;
											     UndeliverableNotes = DtCopy.UndeliverableNotes;

											     // Pickup
											     PickupCompany = DtCopy.PickupCompanyName;

											     var TmpName = string.Empty;

											     var LcName = DtCopy.PickupCompanyName.Trim().ToLower();

											     foreach( var Name in AddressNames )
											     {
												     if( Name.Trim().ToLower() == LcName )
												     {
													     TmpName = Name;

													     break;
												     }
											     }
											     SelectedPickupAddress = !string.IsNullOrEmpty( TmpName ) ? TmpName : DtCopy.PickupCompanyName;

											     PickupName  = DtCopy.PickupContact;
											     PickupPhone = DtCopy.PickupAddressPhone;
											     PickupSuite = DtCopy.PickupAddressSuite;

											     PickupStreet = ( DtCopy.PickupAddressAddressLine1 + " " + DtCopy.PickupAddressAddressLine2 ).Trim();

											     PickupCity = DtCopy.PickupAddressCity;

											     if( DtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace() && ( DtCopy.PickupAddressCountry != PickupCountry ) )
												     PickupCountry = DtCopy.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
											     else if( DtCopy.DeliveryAddressCountry.IsNotNullOrWhiteSpace() )
											     {
												     // Empty - load the one from the delivery address
												     PickupCountry = DtCopy.DeliveryAddressCountry;
											     }

											     WhenPickupCountryChanges();

											     // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
											     PickupProvState = FindItemInRegionsList( PickupRegions, DtCopy.PickupAddressRegion );

											     if( PickupProvState.IsNullOrWhiteSpace() )
												     PickupProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( DtCopy.PickupAddressRegion, PickupCountry );

											     PickupPostalZip    = DtCopy.PickupAddressPostalCode;
											     PickupZone         = DtCopy.PickupZone;
											     PickupAddressNotes = DtCopy.PickupAddressNotes;
											     PickupNotes        = DtCopy.PickupNotes;

											     // Delivery
											     DeliveryCompany = DtCopy.DeliveryCompanyName;

											     // This is no longer finding the item in the combo
											     TmpName = string.Empty;

											     foreach( var Name in AddressNames )
											     {
												     if( Name == DtCopy.DeliveryCompanyName.ToLower() )
												     {
													     //SelectedDeliveryAddress = name;
													     TmpName = Name;
													     break;
												     }
											     }

											     SelectedDeliveryAddress = !string.IsNullOrEmpty( TmpName ) ? TmpName : DtCopy.DeliveryCompanyName;

											     DeliveryName  = DtCopy.DeliveryContact;
											     DeliveryPhone = DtCopy.DeliveryAddressPhone;
											     DeliverySuite = DtCopy.DeliveryAddressSuite;

											     DeliveryStreet = ( DtCopy.DeliveryAddressAddressLine1 + " " + DtCopy.DeliveryAddressAddressLine2 ).Trim();

											     //DeliveryStreet = dtCopy.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
											     DeliveryCity = DtCopy.DeliveryAddressCity;

											     if( DtCopy.DeliveryAddressCountry.IsNotNullOrWhiteSpace() && ( DtCopy.DeliveryAddressCountry != DeliveryCountry ) )
												     DeliveryCountry = DtCopy.DeliveryAddressCountry; // Had to move before prov so that the DeliverRegions was loaded
											     else if( DtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace() )
											     {
												     // Empty - load the one from the pickup address
												     DeliveryCountry = DtCopy.PickupAddressCountry;
											     }

											     WhenDeliveryCountryChanges();

											     // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
											     DeliveryProvState = FindItemInRegionsList( DeliveryRegions, DtCopy.DeliveryAddressRegion );

											     if( DeliveryProvState.IsNullOrWhiteSpace() )
												     DeliveryProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( DtCopy.DeliveryAddressRegion, DeliveryCountry );

											     DeliveryPostalZip    = DtCopy.DeliveryAddressPostalCode;
											     DeliveryZone         = DtCopy.DeliveryZone;
											     DeliveryAddressNotes = DtCopy.DeliveryAddressNotes;
											     DeliveryNotes        = DtCopy.DeliveryNotes;

											     DriverNotes = DtCopy.DriverNotes;

											     // Service
											     ServiceLevel = DtCopy.ServiceLevel;
											     PackageType  = DtCopy.PackageType;

											     ReadyDate = DtCopy.ReadyTime?.DateTime ?? DateTime.MinValue;

											     DueDate = DtCopy.DueTime?.DateTime ?? DateTime.MinValue;


											     // POP, POD & Docs
											     POP = DtCopy.POP;
											     POD = DtCopy.POD;

											     // TODO Signatures

											     //SelectedPackageType = dtCopy.PackageType;
											     SelectedDriver = DtCopy.Driver;

											     //TripPackages.Clear();
											     TripPackages = new ObservableCollection<DisplayPackage>();

											     //TripItems = dtCopy.TripItems;
											     TripPackages = DtCopy.Packages;
											     Logging.WriteLogLine( "DEBUG DtCopy.Packages.Count: " + DtCopy.Packages.Count );

											     if( TripPackages.Count == 0 )
											     {
												     TripPackages.Clear();

												     TripPackage tp = new()
																      {
																	      Pieces      = DtCopy.Pieces,
																	      Weight      = DtCopy.Weight,
																	      PackageType = DtCopy.PackageType
																      };
												     DisplayPackage dp = new( tp );
												     TripPackages.Add( dp );
											     }

											     // Update TripItemsInventory
											     TripItemsInventory.Clear();
											     var SDict = new SortedDictionary<int, List<ExtendedInventory>>();

											     for( var I = 0; I < TripPackages.Count; I++ )
											     {
												     TripPackage Tp = TripPackages[ I ];

												     if( Tp.Items.IsNotNull() && ( Tp.Items.Count > 0 ) )
												     {
													     //Logging.WriteLogLine( "Package " + Tp.PackageType + " contains " + Tp.Items.Count + " items" );
													     var Eis = new List<ExtendedInventory>();

													     foreach( var Ti in Tp.Items )
													     {
														     if( Ti is not null )
														     {
															     foreach( var Tmp in Inventory )
															     {
																     if( Tmp.Barcode == Ti.Barcode )
																     {
																	     var Pi = Tmp;
																	     var Ei = new ExtendedInventory( Ti.Description, (int)Ti.Pieces, Ti.ItemCode, Pi );
																	     Eis.Add( Ei );
																	     break;
																     }
															     }
														     }
													     }

													     SDict.Add( I + 1, Eis );
												     }
											     }

											     TripItemsInventory = SDict;

											     if( ( TripItemsInventory.Count > 0 ) && TripItemsInventory.ContainsKey( 1 ) )
												     SelectedInventoryForTripItem = TripItemsInventory[ 1 ];

											     // Charges

											     CallDate = DtCopy.Ct?.LocalDateTime ?? DateTime.MinValue;
											     CallTime = DtCopy.Ct?.LocalDateTime ?? DateTime.MinValue;

											     //Logging.WriteLogLine( "DEBUG dtCopy CallDate/Time: " + DtCopy.Ct?.LocalDateTime );

											     ReadyDate = DtCopy.ReadyTime?.LocalDateTime ?? DateTime.MinValue;
											     ReadyTime = DtCopy.ReadyTime?.LocalDateTime ?? DateTime.MinValue;
											     //Logging.WriteLogLine( "DEBUG dtCopy ReadyTime: " + DtCopy.ReadyTime?.LocalDateTime );

											     DueDate = DtCopy.DueTime?.LocalDateTime ?? DateTime.MinValue;
											     DueTime = DtCopy.DueTime?.LocalDateTime ?? DateTime.MinValue;
											     //Logging.WriteLogLine( "DEBUG dtCopy DueTime: " + DtCopy.DueTime?.LocalDateTime );

											     TmpName = string.Empty;

											     foreach( var Name in AddressNames )
											     {
												     if( Name == DtCopy.PickupCompanyName.ToLower() )
												     {
													     TmpName = Name;

													     break;
												     }
											     }

											     if( !string.IsNullOrEmpty( TmpName ) )
												     SelectedPickupAddress = TmpName;

											     TmpName = string.Empty;

											     foreach( var Name in AddressNames )
											     {
												     if( Name == DtCopy.DeliveryCompanyName.ToLower() )
												     {
													     TmpName = Name;

													     break;
												     }
											     }

											     if( !string.IsNullOrEmpty( TmpName ) )
												     SelectedDeliveryAddress = TmpName;

											     if( CustomerPhone.IsNullOrWhiteSpace() && DtCopy.BillingAddressPhone.IsNotNullOrWhiteSpace() )
												     CustomerPhone = DtCopy.BillingAddressPhone;
											     CustomerNotes = DtCopy.BillingAddressNotes;

											     try
											     {
												     if( !IsInDesignMode )
												     {
													     var Company = await Azure.Client.RequestGetResellerCustomerCompany( DtCopy.AccountId );
													     Customer = Company.CompanyName;

													     if( ( Company != null ) && Company.Notes.IsNotNullOrWhiteSpace() )
														     CustomerNotes = Company.Notes;
												     }
											     }
											     catch( Exception E )
											     {
												     Logging.WriteLogLine( "Exception thrown looking up Customer for AccountId: " + DtCopy.AccountId + " e->" + E );
											     }

											     Dispatcher.Invoke( () =>
															        {
																        PickupCountry   = DtCopy.PickupAddressCountry;
																        DeliveryCountry = DtCopy.DeliveryAddressCountry;
																        PickupProvState = FindItemInRegionsList( PickupRegions, DtCopy.PickupAddressRegion );

																        if( PickupProvState.IsNullOrWhiteSpace() )
																	        PickupProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( DtCopy.PickupAddressRegion, DtCopy.PickupAddressCountry );
																        DeliveryProvState = FindItemInRegionsList( DeliveryRegions, DtCopy.DeliveryAddressRegion );

																        if( DeliveryProvState.IsNullOrWhiteSpace() )
																	        DeliveryProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( DtCopy.DeliveryAddressRegion, DtCopy.DeliveryAddressCountry );


																		// Restore Charges and Pricing
																		List<TripCharge> tcs = new();
																		tcs.AddRange(TripCharges);
																		lock (TripCharges)
																		{
																			TripCharges = tcs;
																			//TripCharges = DtCopy.TripCharges;
																			//View?.ApplyTripCharges(TripCharges);
																			//View?.ApplyTripCharges(DtCopy.TripCharges);
																		}

																		// Set the totals on the screen
																		TotalTotal = DtCopy.TotalAmount;
																		TotalTaxes = DtCopy.TotalTaxAmount;
																		TotalBeforeTax = DtCopy.TotalFixedAmount;
																		TotalPayroll = DtCopy.TotalPayrollAmount;

                                                                        lock (TripCharges)
                                                                        {
                                                                            //TripCharges = DtCopy.TripCharges;
                                                                            //View?.ApplyTripCharges(TripCharges);
                                                                            View?.ApplyTripCharges(tcs);
                                                                        }
                                                                    } );
										     } );
					//}));
				}
			}
			finally
			{
				View?.BuildPackageRows( this );
				InTripChange = false;
				Loaded       = true;
			}
		}
	}
#endregion

	private async Task<CompanySummaryList> GetCompanySummaryForAccountId( string accountId )
	{
		Logging.WriteLogLine( "Loading CompanySummaryList for account " + accountId );
		CompanySummaryList Csl;

		if( !DictCompanySummaryListsByAccountId.ContainsKey( accountId ) )
		{
			Logging.WriteLogLine( "DEBUG Loading CompanySummaryList from server" );
			Csl = await Azure.Client.RequestGetCustomerCompaniesSummary( accountId );

			if( !DictCompanySummaryListsByAccountId.ContainsKey( accountId ) )
				DictCompanySummaryListsByAccountId.Add( accountId, Csl );
		}
		else
		{
			Logging.WriteLogLine( "DEBUG Loading CompanySummaryList from cache" );
			Csl = DictCompanySummaryListsByAccountId[ accountId ];
		}

		return Csl;
	}

	private async Task<CompanyDetailList> GetCompanyDetailListForAccountId( string accountId )
	{
		Logging.WriteLogLine( "Loading CompanyDetailList for account " + accountId );
		CompanyDetailList cdl;

		if( !DictCompanyDetailListsByAccountId.ContainsKey( accountId ) )
		{
			Logging.WriteLogLine( "DEBUG Loading CompanyDetailList from server" );
			cdl = await Azure.Client.RequestGetCustomerCompaniesDetailed( accountId );

			if( !DictCompanyDetailListsByAccountId.ContainsKey( accountId ) )
				DictCompanyDetailListsByAccountId.Add( accountId, cdl );
		}
		else
		{
			Logging.WriteLogLine( "DEBUG Loading CompanyDetailList from cache" );
			cdl = DictCompanyDetailListsByAccountId[ accountId ];
		}

		return cdl;
	}

	public async Task WhenCurrentTripChanges_V1()
	{
		//if (dt != null)
		//{
		//    CurrentTrip = dt;
		//}
		if( CurrentTrip != null )
		{
			Logging.WriteLogLine( "CurrentTrip has changed: " + CurrentTrip.TripId + " CallTakerId: " + CurrentTrip.CallTakerId );

			//Logging.WriteLogLine( "DEBUG CurrentTime CallDate/Time: " + CurrentTrip.Ct?.LocalDateTime );
			//Logging.WriteLogLine( "DEBUG CurrentTime ReadyTime: " + CurrentTrip.ReadyTime?.LocalDateTime );
			//Logging.WriteLogLine( "DEBUG CurrentTime DueTime: " + CurrentTrip.DueTime?.LocalDateTime );

			var DtCopy = Clone( CurrentTrip );

			var Latest = await Azure.Client.RequestGetTrip( new GetTrip
														    {
															    Signatures = true,
															    TripId     = CurrentTrip.TripId
														    } );

			if( Latest.Signatures.Count > 0 )
			{
				//Logging.WriteLogLine( "Trip has signatures: " + Latest.Signatures.Count );
				DtCopy.Signatures = Latest.Signatures;
			}

			if( DtCopy.CallTakerId.IsNullOrWhiteSpace() && Latest.CallTakerId.IsNotNullOrWhiteSpace() )
			{
				Logging.WriteLogLine( "Setting CallTakerId to " + Latest.CallTakerId );
				DtCopy.CallTakerId = Latest.CallTakerId;
			}

			if( Latest.POP.IsNotNullOrWhiteSpace() )
				DtCopy.POP = Latest.POP;

			if( Latest.POD.IsNotNullOrWhiteSpace() )
				DtCopy.POD = Latest.POD;
			CurrentTrip = DtCopy;

			//View?.LoadTrip( DtCopy );

			//Task.WaitAll(Task.Run(() =>
			//{
			await Dispatcher.Invoke( async () =>
								     {
									     //Execute_ClearTripEntry(true);
									     //View?.LoadTrip( DtCopy );

									     // Need to force the loading in order that the address combos are populated
									     //WhenAccountChanges(dtCopy.AccountId);
									     var Csl     = await Azure.Client.RequestGetCustomerCompaniesSummary( DtCopy.AccountId );
									     var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( DtCopy.AccountId );

									     if( Details is not null )
									     {
										     DetailedAddresses = new Dictionary<string, CompanyDetail>();

										     foreach( var Detail in Details )
										     {
											     if( Detail.Company.CompanyName.ToLower().IsNotNullOrWhiteSpace() && !DetailedAddresses.ContainsKey( Detail.Company.CompanyName.ToLower() ) )
												     DetailedAddresses.Add( Detail.Company.CompanyName.ToLower(), Detail );
										     }
									     }

									     if( Csl is not null )
									     {
										     //var Addrs = (from A in csl
										     //             let Cn = A.CompanyName.ToLower()
										     //             orderby Cn
										     //             group A by Cn
										     //                into G
										     //             select G.First()).ToList();

										     //var Names = (from A in Addrs
										     //             let Cn = A.CompanyName.ToLower()
										     //             orderby Cn
										     //             group Cn by Cn
										     //                into G
										     //             select G.First()).ToList();
										     var Addrs = new List<CompanyByAccountSummary>();
										     var Names = new List<string>();
										     var Sd    = new SortedDictionary<string, CompanyByAccountSummary>();

										     foreach( var Cs in Csl )
										     {
											     if( Cs.CompanyName.IsNotNullOrWhiteSpace() && !Sd.ContainsKey( Cs.CompanyName.ToLower() ) )
												     Sd.Add( Cs.CompanyName.ToLower(), Cs );
										     }

										     foreach( var Key in Sd.Keys )
										     {
											     Addrs.Add( Sd[ Key ] );
											     Names.Add( Sd[ Key ].CompanyName );
										     }

										     Addresses    = Addrs;
										     AddressNames = Names;
									     }

									     //Task.WaitAll(Task.Run(() =>
									     //{
									     //Logging.WriteLogLine( "Loading the rest of the trip" );
									     Status1        = (int)DtCopy.Status1;
									     DriverCode     = DtCopy.Driver;
									     SelectedDriver = DriverCode.ToLower();
									     TPU            = DtCopy.PickupTime.ToString();
									     TDEL           = DtCopy.DeliveryTime.ToString();
									     TVER           = DtCopy.VerifiedTime.ToString();

									     TripId = DtCopy.TripId;

									     SelectedAccount = DtCopy.AccountId;
									     Account         = DtCopy.AccountId;
									     Customer        = DtCopy.AccountId;
									     CustomerPhone   = DtCopy.BillingAddressPhone;
									     CustomerNotes   = DtCopy.BillingAddressNotes;
									     Reference       = DtCopy.Reference;
									     CallTakerId     = DtCopy.CallTakerId;

									     //MergeGlobalAddressBook();

									     var Ca = await GetCompanyAddress( DtCopy.AccountId ); // Force the population of country and region

									     CustomerNotes = Ca?.Notes ?? "";

									     Reference = DtCopy.Reference;

									     CallName  = DtCopy.CallerName;
									     CallPhone = DtCopy.CallerPhone;
									     CallEmail = DtCopy.CallerEmail;

									     if( !string.IsNullOrEmpty( DtCopy.CallTime.ToString() ) )
									     {
										     CallDate = DtCopy.Ct?.DateTime ?? DateTime.MinValue;

										     CallTime = DtCopy.Ct?.DateTime ?? DateTime.MinValue;

										     // Won't compile on my system
										     //CallDate = dtCopy.Ct.DateTime != null ? dtCopy.Ct.DateTime : DateTime.MinValue;

										     //CallTime = dtCopy.Ct.DateTime;
									     }

									     IsQuote            = DtCopy.IsQuote;
									     UndeliverableNotes = DtCopy.UndeliverableNotes;

									     // Pickup
									     PickupCompany = DtCopy.PickupCompanyName;

									     //GetCompanyAddress(dtCopy.AccountId); // Force the population of country and region

									     // This is no longer finding the item in the combo
									     //SelectedPickupAddress = dtCopy.PickupCompanyName;
									     //SelectedPickupAddress = string.Empty;
									     var TmpName = string.Empty;

									     var LcName = DtCopy.PickupCompanyName.Trim().ToLower();

									     foreach( var Name in AddressNames )
									     {
										     //if( name.Equals(lcName, StringComparison.OrdinalIgnoreCase ))
										     if( Name.Trim().ToLower() == LcName )
										     {
											     //SelectedPickupAddress = name;
											     TmpName = Name;

											     break;
										     }
									     }

									     //var match = from name in AddressNames
									     //            where name.ToLower() == lcName
									     //            select name;

									     //                          if ( match.Any() )
									     //tmpName = match.First();

									     //if (string.IsNullOrEmpty(SelectedPickupAddress))
									     //SelectedPickupAddress = dtCopy.PickupCompanyName;
									     SelectedPickupAddress = !string.IsNullOrEmpty( TmpName ) ? TmpName : DtCopy.PickupCompanyName;

									     PickupName  = DtCopy.PickupContact;
									     PickupPhone = DtCopy.PickupAddressPhone;
									     PickupSuite = DtCopy.PickupAddressSuite;

									     PickupStreet = ( DtCopy.PickupAddressAddressLine1 + " " + DtCopy.PickupAddressAddressLine2 ).Trim();

									     //PickupStreet = dtCopy.PickupAddressAddressLine1; // TODO Fix PickupAddressAddressLine2 returns PickupAddressAddressLine1
									     PickupCity = DtCopy.PickupAddressCity;

									     // PickupCountry = dtCopy.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
									     //if (dtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace())
									     if( DtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace() && ( DtCopy.PickupAddressCountry != PickupCountry ) )
									     {
										     // TODO Freezes here
										     PickupCountry = DtCopy.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
									     }
									     else if( DtCopy.DeliveryAddressCountry.IsNotNullOrWhiteSpace() )
									     {
										     // Empty - load the one from the pickup address
										     PickupCountry = DtCopy.DeliveryAddressCountry;
									     }

									     WhenPickupCountryChanges();

									     // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
									     PickupProvState = FindItemInRegionsList( PickupRegions, DtCopy.PickupAddressRegion );

									     if( PickupProvState.IsNullOrWhiteSpace() )
										     PickupProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( DtCopy.PickupAddressRegion, PickupCountry );

									     PickupPostalZip    = DtCopy.PickupAddressPostalCode;
									     PickupZone         = DtCopy.PickupZone;
									     PickupAddressNotes = DtCopy.PickupAddressNotes;
									     PickupNotes        = DtCopy.PickupNotes;

									     // Delivery
									     DeliveryCompany = DtCopy.DeliveryCompanyName;

									     // This is no longer finding the item in the combo
									     //SelectedDeliveryAddress = dtCopy.DeliveryCompanyName;
									     //SelectedDeliveryAddress = string.Empty;
									     TmpName = string.Empty;

									     foreach( var Name in AddressNames )
									     {
										     if( Name == DtCopy.DeliveryCompanyName.ToLower() )
										     {
											     //SelectedDeliveryAddress = name;
											     TmpName = Name;

											     break;
										     }
									     }

									     //match = from name in AddressNames
									     //        where name.ToLower() == lcName
									     //        select name;

									     //if( match.Any() )
									     // tmpName = match.First();

									     //if (string.IsNullOrEmpty(SelectedDeliveryAddress))
									     //SelectedDeliveryAddress = dtCopy.DeliveryCompanyName;
									     SelectedDeliveryAddress = !string.IsNullOrEmpty( TmpName ) ? TmpName : DtCopy.DeliveryCompanyName;

									     DeliveryName  = DtCopy.DeliveryContact;
									     DeliveryPhone = DtCopy.DeliveryAddressPhone;
									     DeliverySuite = DtCopy.DeliveryAddressSuite;

									     DeliveryStreet = ( DtCopy.DeliveryAddressAddressLine1 + " " + DtCopy.DeliveryAddressAddressLine2 ).Trim();

									     //DeliveryStreet = dtCopy.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
									     DeliveryCity = DtCopy.DeliveryAddressCity;

									     if( DtCopy.DeliveryAddressCountry.IsNotNullOrWhiteSpace() && ( DtCopy.DeliveryAddressCountry != DeliveryCountry ) )
										     DeliveryCountry = DtCopy.DeliveryAddressCountry; // Had to move before prov so that the DeliverRegions was loaded
									     else if( DtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace() )
									     {
										     // Empty - load the one from the pickup address
										     DeliveryCountry = DtCopy.PickupAddressCountry;
									     }

									     WhenDeliveryCountryChanges();

									     //else
									     //{
									     //    DeliveryCountry = Selected
									     //}

									     // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
									     DeliveryProvState = FindItemInRegionsList( DeliveryRegions, DtCopy.DeliveryAddressRegion );

									     if( DeliveryProvState.IsNullOrWhiteSpace() )
										     DeliveryProvState = CountriesRegions.FindRegionForAbbreviationAndCountry( DtCopy.DeliveryAddressRegion, DeliveryCountry );

									     DeliveryPostalZip    = DtCopy.DeliveryAddressPostalCode;
									     DeliveryZone         = DtCopy.DeliveryZone;
									     DeliveryAddressNotes = DtCopy.DeliveryAddressNotes;
									     DeliveryNotes        = DtCopy.DeliveryNotes;

									     // Package
									     //PackageType = dtCopy.PackageType;
									     //Weight = dtCopy.Weight;
									     //TotalWeight = Weight;
									     //Pieces = (int)dtCopy.Pieces;
									     //TotalPieces = Pieces;
									     //Length = 0;   // TODO Fix this
									     //Width = 0;    // TODO Fix this
									     //Height = 0;   // TODO Fix this
									     //Original = 0; // TODO Fix this
									     //TotalOriginal = Original;
									     //QH = 0; // TODO Fix this

									     // Service
									     ServiceLevel = DtCopy.ServiceLevel;
									     PackageType  = DtCopy.PackageType;

									     ReadyDate = DtCopy.ReadyTime?.DateTime ?? DateTime.MinValue;

									     // Won't compile on my system
									     //ReadyDate = dtCopy.ReadyTime.DateTime != null ? dtCopy.ReadyTime.DateTime : DateTime.MinValue;

									     //ReadyTime = dtCopy.ReadyTime.DateTime;

									     DueDate = DtCopy.DueTime?.DateTime ?? DateTime.MinValue;

									     //DueTime = dtCopy.DueTime.DateTime;

									     // POP, POD & Docs
									     POP = DtCopy.POP;
									     POD = DtCopy.POD;

									     // TODO Signatures

									     //SelectedPackageType = dtCopy.PackageType;
									     SelectedDriver = DtCopy.Driver;

									     TripPackages.Clear();

									     // Need at least 1 for the row to be built
									     View?.BuildPackageRows( this );

									     //TripItems = dtCopy.TripItems;
									     TripPackages = DtCopy.Packages;
									     //Logging.WriteLogLine( "Packages.Count: " + DtCopy.Packages.Count );

									     if( TripPackages.Count == 0 )
									     {
										     var Oc = new ObservableCollection<DisplayPackage>
												      {
													      new( new TripPackage
													           {
														           Pieces      = DtCopy.Pieces,
														           Weight      = DtCopy.Weight,
														           PackageType = DtCopy.PackageType
													           } )
												      };
										     TripPackages = Oc;
									     }

									     // TripPackages = new ObservableCollection<Boards.Common.DisplayTrip.DisplayPackage>( dtCopy.Packages );

									     // Update TripItemsInventory
									     TripItemsInventory.Clear();
									     var SDict = new SortedDictionary<int, List<ExtendedInventory>>();

									     for( var I = 0; I < TripPackages.Count; I++ )
									     {
										     TripPackage Tp = TripPackages[ I ];

										     if( Tp.Items.IsNotNull() && ( Tp.Items.Count > 0 ) )
										     {
											     //Logging.WriteLogLine( "Package " + Tp.PackageType + " contains " + Tp.Items.Count + " items" );
											     var Eis = new List<ExtendedInventory>();

											     foreach( var Ti in Tp.Items )
											     {
												     if( Ti is not null )
												     {
													     foreach( var Tmp in Inventory )
													     {
														     if( Tmp.Barcode == Ti.Barcode )
														     {
															     var Pi = Tmp;
															     var Ei = new ExtendedInventory( Ti.Description, (int)Ti.Pieces, Ti.ItemCode, Pi );
															     Eis.Add( Ei );
															     break;
														     }
													     }
												     }
											     }

											     //TripItemsInventory.Add(i + 1, eis);
											     SDict.Add( I + 1, Eis );
										     }
									     }

									     TripItemsInventory = SDict;

									     if( ( TripItemsInventory.Count > 0 ) && TripItemsInventory.ContainsKey( 1 ) )
										     SelectedInventoryForTripItem = TripItemsInventory[ 1 ];

									     View?.BuildPackageRows( this );

									     //if ((TripItems != null) && (TripItems.Count > 0))
									     //{
									     //    Logging.WriteLogLine("TripItem.Count: " + TripItems.Count);
									     //    foreach (var ti in TripItems)
									     //    {
									     //        Logging.WriteLogLine(TripItemToString(ti));

									     //        // KLUDGE
									     //        view.BuildNewPackageTypeRow(this, ti);

									     //        TotalOriginal += ti.Original;
									     //        TotalPieces += (int)ti.Pieces;
									     //        TotalWeight += ti.Weight;
									     //    }
									     //    view.BuildNewPackageTotalRow(this);
									     //}

									     // Charges

									     // Some of the fields aren't being updated, and this helps
									     //if (!string.IsNullOrEmpty(dtCopy.CallTime))

									     // Won't compile on my system
									     //CallDate = dtCopy.Ct.DateTime != null ? dtCopy.Ct.DateTime : DateTime.MinValue;
									     //CallDate = dtCopy.Ct?.DateTime ?? DateTime.MinValue;
									     //CallTime = dtCopy.Ct?.DateTime ?? DateTime.MinValue;
									     CallDate = DtCopy.Ct?.LocalDateTime ?? DateTime.MinValue;
									     CallTime = DtCopy.Ct?.LocalDateTime ?? DateTime.MinValue;

									     //Logging.WriteLogLine( "DEBUG dtCopy CallDate/Time: " + DtCopy.Ct?.LocalDateTime );

									     //ReadyDate = dtCopy.ReadyTime.DateTime != null ? dtCopy.ReadyTime.DateTime : DateTime.MinValue;
									     //ReadyDate = dtCopy.ReadyTime?.DateTime ?? DateTime.MinValue;
									     //ReadyTime = dtCopy.ReadyTime?.DateTime ?? DateTime.MinValue;
									     ReadyDate = DtCopy.ReadyTime?.LocalDateTime ?? DateTime.MinValue;
									     ReadyTime = DtCopy.ReadyTime?.LocalDateTime ?? DateTime.MinValue;
									     //Logging.WriteLogLine( "DEBUG dtCopy ReadyTime: " + DtCopy.ReadyTime?.LocalDateTime );

									     //DueDate = dtCopy.DueTime.DateTime != null ? dtCopy.DueTime.DateTime : DateTime.MinValue;
									     //DueDate = dtCopy.DueTime?.DateTime ?? DateTime.MinValue;
									     //DueTime = dtCopy.DueTime?.DateTime ?? DateTime.MinValue;
									     DueDate = DtCopy.DueTime?.LocalDateTime ?? DateTime.MinValue;
									     DueTime = DtCopy.DueTime?.LocalDateTime ?? DateTime.MinValue;
									     //Logging.WriteLogLine( "DEBUG dtCopy DueTime: " + DtCopy.DueTime?.LocalDateTime );

									     TmpName = string.Empty;

									     foreach( var Name in AddressNames )
									     {
										     if( Name == DtCopy.PickupCompanyName.ToLower() )
										     {
											     TmpName = Name;

											     break;
										     }
									     }

									     if( !string.IsNullOrEmpty( TmpName ) )
										     SelectedPickupAddress = TmpName;

									     TmpName = string.Empty;

									     foreach( var Name in AddressNames )
									     {
										     if( Name == DtCopy.DeliveryCompanyName.ToLower() )
										     {
											     //SelectedDeliveryAddress = name;
											     TmpName = Name;

											     break;
										     }
									     }

									     if( !string.IsNullOrEmpty( TmpName ) )
										     SelectedDeliveryAddress = TmpName;

									     CustomerPhone = DtCopy.BillingAddressPhone;
									     CustomerNotes = DtCopy.BillingAddressNotes;

									     try
									     {
										     if( !IsInDesignMode )
										     {
											     var Company = await Azure.Client.RequestGetResellerCustomerCompany( DtCopy.AccountId );
											     Customer      = Company.CompanyName;
											     CustomerNotes = Company.Notes;
										     }
									     }
									     catch( Exception E )
									     {
										     Logging.WriteLogLine( "Exception thrown looking up Customer for AccountId: " + DtCopy.AccountId + " e->" + E );
									     }

									     //Logging.WriteLogLine( "WhenCurrentTripChanges finished" );

									     //Logging.WriteLogLine( "Setting tbCustomerPhone.Text directly to " + DtCopy.BillingAddressPhone );

									     if( View is not null )
									     {
										     View.tbCustomerPhone.Text = DtCopy.BillingAddressPhone;

										     Dispatcher.Invoke( () =>
														        {
															        PickupCountry   = DtCopy.PickupAddressCountry;
															        DeliveryCountry = DtCopy.DeliveryAddressCountry;
														        } );
										     PickupProvState   = FindItemInRegionsList( PickupRegions, DtCopy.PickupAddressRegion );
										     DeliveryProvState = FindItemInRegionsList( DeliveryRegions, DtCopy.DeliveryAddressRegion );

										     //View.LoadTrip(DtCopy);

										     View.BuildPackageRows( this );
									     }

									     // 
									     var yes = IsCallTimeLocked;
									     yes = IsDispatchToDriverLocked;
								     } );
		}
	}

	//[DependsUpon(nameof(CurrentTrip))]
	//public void WhenCurrentTripChanges()
	//{
	//    if (CurrentTrip != null)
	//    {
	//        Logging.WriteLogLine("CurrentTrip has changed: " + CurrentTrip.TripId);

	//        //Task.WaitAll(Task.Run(() =>
	//        //{
	//        Dispatcher.Invoke(() =>
	//        {
	//            //Execute_ClearTripEntry(true);

	//            // Need to force the loading in order that the address combos are populated
	//            Task.WaitAll(Task.Run(() =>
	//                                {
	//                                //WhenAccountChanges(CurrentTrip.AccountId);
	//                                var csl = Azure.Client.RequestGetCustomerCompaniesSummary(CurrentTrip.AccountId).Result;

	//                                    if (csl != null)
	//                                    {
	//                                        var Addrs = (from A in csl
	//                                                     let Cn = A.CompanyName.ToLower()
	//                                                     orderby Cn
	//                                                     group A by Cn
	//                                                        into G
	//                                                     select G.First()).ToList();

	//                                        var Names = (from A in Addrs
	//                                                     let Cn = A.CompanyName.ToLower()
	//                                                     orderby Cn
	//                                                     group Cn by Cn
	//                                                        into G
	//                                                     select G.First()).ToList();

	//                                        Addresses = Addrs;
	//                                        AddressNames = Names;
	//                                    }
	//                                }));

	//            //Task.WaitAll(Task.Run(() =>
	//            //{
	//            Logging.WriteLogLine("Loading the rest of the trip");
	//            Status1 = (int)CurrentTrip.Status1;
	//            DriverCode = CurrentTrip.Driver;
	//            SelectedDriver = DriverCode.ToLower();
	//            TPU = CurrentTrip.PickupTime.ToString();
	//            TDEL = CurrentTrip.DeliveryTime.ToString();

	//            TripId = CurrentTrip.TripId;

	//            //SelectedAccount = Account = CurrentTrip.AccountId; // TODO Freezes here
	//            SelectedAccount = CurrentTrip.AccountId;
	//            Account = CurrentTrip.AccountId;
	//            Customer = CurrentTrip.AccountId;
	//            CustomerPhone = CurrentTrip.BillingAddressPhone;
	//            CustomerNotes = CurrentTrip.BillingAddressNotes;

	//            GetCompanyAddress(CurrentTrip.AccountId); // Force the population of country and region

	//            Reference = CurrentTrip.Reference;

	//            CallName = CurrentTrip.CallerName;
	//            CallPhone = CurrentTrip.CallerPhone;
	//            CallEmail = CurrentTrip.CallerEmail;

	//            if (!string.IsNullOrEmpty(CurrentTrip.CallTime.ToString()))
	//            {
	//                CallDate = CurrentTrip.Ct.DateTime;

	//                //CallTime = CurrentTrip.Ct.DateTime;
	//            }

	//            IsQuote = CurrentTrip.IsQuote;
	//            UndeliverableNotes = CurrentTrip.UndeliverableNotes;

	//            // Pickup
	//            PickupCompany = CurrentTrip.PickupCompanyName;

	//            //GetCompanyAddress(CurrentTrip.AccountId); // Force the population of country and region

	//            // This is no longer finding the item in the combo
	//            //SelectedPickupAddress = CurrentTrip.PickupCompanyName;
	//            //SelectedPickupAddress = string.Empty;
	//            string tmpName = string.Empty;

	//            string lcName = CurrentTrip.PickupCompanyName.Trim().ToLower();
	//            //foreach( string name in AddressNames )
	//            //{
	//            //	//if( name.Equals(lcName, StringComparison.OrdinalIgnoreCase ))
	//            //                   if (name.Trim().ToLower() == lcName)
	//            //	{
	//            //		//SelectedPickupAddress = name;
	//            //		tmpName = name;

	//            //		break;
	//            //	}
	//            //}

	//            IEnumerable<string> match = from name in AddressNames where name.ToLower() == lcName select name;
	//            if (match.Any())
	//            {
	//                tmpName = match.First();
	//            }

	//            //if (string.IsNullOrEmpty(SelectedPickupAddress))
	//            if (!string.IsNullOrEmpty(tmpName))
	//            {
	//                //SelectedPickupAddress = CurrentTrip.PickupCompanyName;
	//                SelectedPickupAddress = tmpName;
	//            }

	//            PickupName = CurrentTrip.PickupContact;
	//            PickupPhone = CurrentTrip.PickupAddressPhone;
	//            PickupSuite = CurrentTrip.PickupAddressSuite;

	//            PickupStreet = (CurrentTrip.PickupAddressAddressLine1 + " " + CurrentTrip.PickupAddressAddressLine2).Trim();

	//            //PickupStreet = CurrentTrip.PickupAddressAddressLine1; // TODO Fix PickupAddressAddressLine2 returns PickupAddressAddressLine1
	//            PickupCity = CurrentTrip.PickupAddressCity;

	//            // PickupCountry = CurrentTrip.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
	//            //if (CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace())
	//            if (CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace() && CurrentTrip.PickupAddressCountry != PickupCountry)
	//            {
	//                // TODO Freezes here
	//                PickupCountry = CurrentTrip.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
	//            }
	//            else if (CurrentTrip.DeliveryAddressCountry.IsNotNullOrWhiteSpace())
	//            {
	//                // Empty - load the one from the pickup address
	//                PickupCountry = CurrentTrip.DeliveryAddressCountry;
	//            }
	//            WhenPickupCountryChanges();

	//            // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
	//            PickupProvState = FindItemInRegionsList(PickupRegions, CurrentTrip.PickupAddressRegion);
	//            PickupPostalZip = CurrentTrip.PickupAddressPostalCode;
	//            PickupZone = CurrentTrip.PickupZone;
	//            PickupAddressNotes = CurrentTrip.PickupAddressNotes;
	//            PickupNotes = CurrentTrip.PickupNotes;

	//            // Delivery
	//            DeliveryCompany = CurrentTrip.DeliveryCompanyName;

	//            // This is no longer finding the item in the combo
	//            //SelectedDeliveryAddress = CurrentTrip.DeliveryCompanyName;
	//            //SelectedDeliveryAddress = string.Empty;
	//            tmpName = string.Empty;

	//            lcName = CurrentTrip.DeliveryCompanyName.Trim().ToLower();
	//            //foreach( string name in AddressNames )
	//            //{
	//            //	if( name == CurrentTrip.DeliveryCompanyName.ToLower() )
	//            //	{
	//            //		//SelectedDeliveryAddress = name;
	//            //		tmpName = name;

	//            //		break;
	//            //	}
	//            //}

	//            match = from name in AddressNames where name.ToLower() == lcName select name;
	//            if (match.Any())
	//            {
	//                tmpName = match.First();
	//            }

	//            //if (string.IsNullOrEmpty(SelectedDeliveryAddress))
	//            if (!string.IsNullOrEmpty(tmpName))
	//            {
	//                //SelectedDeliveryAddress = CurrentTrip.DeliveryCompanyName;
	//                SelectedDeliveryAddress = tmpName;
	//            }

	//            DeliveryName = CurrentTrip.DeliveryContact;
	//            DeliveryPhone = CurrentTrip.DeliveryAddressPhone;
	//            DeliverySuite = CurrentTrip.DeliveryAddressSuite;

	//            DeliveryStreet = (CurrentTrip.DeliveryAddressAddressLine1 + " " + CurrentTrip.DeliveryAddressAddressLine2).Trim();

	//            //DeliveryStreet = CurrentTrip.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
	//            DeliveryCity = CurrentTrip.DeliveryAddressCity;

	//            if (CurrentTrip.DeliveryAddressCountry.IsNotNullOrWhiteSpace() && CurrentTrip.DeliveryAddressCountry != DeliveryCountry)
	//            {
	//                DeliveryCountry = CurrentTrip.DeliveryAddressCountry; // Had to move before prov so that the DeliverRegions was loaded
	//            }
	//            else if (CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace())
	//            {
	//                // Empty - load the one from the pickup address
	//                DeliveryCountry = CurrentTrip.PickupAddressCountry;
	//            }
	//            WhenDeliveryCountryChanges();

	//            //else
	//            //{
	//            //    DeliveryCountry = Selected
	//            //}

	//            // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
	//            DeliveryProvState = FindItemInRegionsList(DeliveryRegions, CurrentTrip.DeliveryAddressRegion);
	//            DeliveryPostalZip = CurrentTrip.DeliveryAddressPostalCode;
	//            DeliveryZone = CurrentTrip.DeliveryZone;
	//            DeliveryAddressNotes = CurrentTrip.DeliveryAddressNotes;
	//            DeliveryNotes = CurrentTrip.DeliveryNotes;

	//            // Package
	//            //PackageType = CurrentTrip.PackageType;
	//            //Weight = CurrentTrip.Weight;
	//            //TotalWeight = Weight;
	//            //Pieces = (int)CurrentTrip.Pieces;
	//            //TotalPieces = Pieces;
	//            //Length = 0;   // TODO Fix this
	//            //Width = 0;    // TODO Fix this
	//            //Height = 0;   // TODO Fix this
	//            //Original = 0; // TODO Fix this
	//            //TotalOriginal = Original;
	//            //QH = 0; // TODO Fix this

	//            // Service
	//            ServiceLevel = CurrentTrip.ServiceLevel;
	//            PackageType = CurrentTrip.PackageType;
	//            ReadyDate = CurrentTrip.ReadyTime.DateTime;

	//            //ReadyTime = CurrentTrip.ReadyTime.DateTime;
	//            DueDate = CurrentTrip.DueTime.DateTime;

	//            //DueTime = CurrentTrip.DueTime.DateTime;

	//            // POP, POD & Docs
	//            POP = CurrentTrip.POP;
	//            POD = CurrentTrip.POD;

	//            // TODO Signatures

	//            //SelectedPackageType = CurrentTrip.PackageType;
	//            SelectedDriver = CurrentTrip.Driver;

	//            TripPackages.Clear();

	//            // Need at least 1 for the row to be built
	//            view.BuildPackageRows(this);

	//            //TripItems = CurrentTrip.TripItems;
	//            TripPackages = CurrentTrip.Packages;
	//            Logging.WriteLogLine("Packages.Count: " + CurrentTrip.Packages.Count);
	//            if (TripPackages.Count == 0)
	//            {
	//                ObservableCollection<DisplayPackage> oc = new ObservableCollection<DisplayPackage>
	//                    {
	//                        new DisplayPackage(new TripPackage())
	//                    };
	//                TripPackages = oc;
	//            }

	//            // TripPackages = new ObservableCollection<Boards.Common.DisplayTrip.DisplayPackage>( CurrentTrip.Packages );

	//            // Update TripItemsInventory
	//            TripItemsInventory.Clear();
	//            for (int i = 0; i < TripPackages.Count; i++)
	//            {
	//                TripPackage tp = TripPackages[i];
	//                if (tp.Items.IsNotNull() && tp.Items.Count > 0)
	//                {
	//                    Logging.WriteLogLine("Package " + tp.PackageType + " contains " + tp.Items.Count + " items");
	//                    List <ExtendedInventory> eis = new List<ExtendedInventory>();
	//                    for (int j = 0; j < tp.Items.Count; j++)
	//                    {
	//                        TripItem ti = tp.Items[j];
	//                        if (ti != null)
	//                        {
	//                            PmlInventory pi;
	//                            foreach (PmlInventory tmp in Inventory)
	//                            {
	//                                if (tmp.Barcode == ti.Barcode)
	//                                {
	//                                    pi = tmp;
	//                                    ExtendedInventory ei = new ExtendedInventory(ti.Description, (int)ti.Pieces, ti.ItemCode, pi);
	//                                    eis.Add(ei);
	//                                    break;
	//                                }
	//                            }

	//                        }
	//                    }
	//                    TripItemsInventory.Add(i + 1, eis);
	//                }
	//            }

	//            if (TripItemsInventory.Count > 0 && TripItemsInventory.ContainsKey(1))
	//            {
	//                SelectedInventoryForTripItem = TripItemsInventory[1];
	//            }

	//            view.BuildPackageRows(this);

	//            //if ((TripItems != null) && (TripItems.Count > 0))
	//            //{
	//            //    Logging.WriteLogLine("TripItem.Count: " + TripItems.Count);
	//            //    foreach (var ti in TripItems)
	//            //    {
	//            //        Logging.WriteLogLine(TripItemToString(ti));

	//            //        // KLUDGE
	//            //        view.BuildNewPackageTypeRow(this, ti);

	//            //        TotalOriginal += ti.Original;
	//            //        TotalPieces += (int)ti.Pieces;
	//            //        TotalWeight += ti.Weight;
	//            //    }
	//            //    view.BuildNewPackageTotalRow(this);
	//            //}

	//            // Charges

	//            // Some of the fields aren't being updated, and this helps
	//            //if (!string.IsNullOrEmpty(CurrentTrip.CallTime))
	//            if (CurrentTrip.Ct != null)
	//            {
	//                CallDate = CurrentTrip.Ct.DateTime;

	//                //CallTime = CurrentTrip.Ct.DateTime;
	//            }

	//            if (CurrentTrip.ReadyTime != null)
	//            {
	//                ReadyDate = CurrentTrip.ReadyTime.DateTime;

	//                //ReadyTime = CurrentTrip.ReadyTime.DateTime;
	//            }

	//            if (CurrentTrip.DueTime != null)
	//            {
	//                DueDate = CurrentTrip.DueTime.DateTime;

	//                //DueTime = CurrentTrip.DueTime.DateTime;
	//            }

	//            tmpName = string.Empty;

	//            foreach (string name in AddressNames)
	//            {
	//                if (name == CurrentTrip.PickupCompanyName.ToLower())
	//                {
	//                    tmpName = name;

	//                    break;
	//                }
	//            }

	//            if (!string.IsNullOrEmpty(tmpName))
	//            {
	//                SelectedPickupAddress = tmpName;
	//            }

	//            tmpName = string.Empty;

	//            foreach (string name in AddressNames)
	//            {
	//                if (name == CurrentTrip.DeliveryCompanyName.ToLower())
	//                {
	//                    //SelectedDeliveryAddress = name;
	//                    tmpName = name;

	//                    break;
	//                }
	//            }

	//            if (!string.IsNullOrEmpty(tmpName))
	//            {
	//                SelectedDeliveryAddress = tmpName;
	//            }

	//            CustomerPhone = CurrentTrip.BillingAddressPhone;
	//            CustomerNotes = CurrentTrip.BillingAddressNotes;

	//            Task.WaitAll(Task.Run(async () =>
	//                            {
	//                                try
	//                                {
	//                                        // From Terry: Need to do this to avoid bad requests in the server log
	//                                        if (!IsInDesignMode)
	//                                    {
	//                                        var ca = await Azure.Client.RequestGetResellerCustomerCompany(CurrentTrip.AccountId);
	//                                        Customer = ca.CompanyName;
	//                                    }
	//                                }
	//                                catch (Exception e)
	//                                {
	//                                    Logging.WriteLogLine("Exception thrown looking up Customer for AccountId: " + CurrentTrip.AccountId + " e->" + e);
	//                                }
	//                            }));


	//            //}));

	//        });

	//        //}));

	//        Dispatcher.Invoke(() =>
	//        {
	//            Logging.WriteLogLine("Setting tbCustomerPhone.Text directly to " + CurrentTrip.BillingAddressPhone);
	//            view.tbCustomerPhone.Text = CurrentTrip.BillingAddressPhone;

	//            view.BuildPackageRows(this);
	//        });
	//    }
	//}

	public string FindItemInRegionsList( IList? regions, string region )
	{
		var NewRegion = string.Empty;

		if( regions is not null )
		{
			foreach( string Tmp in regions )
			{
				if( string.Equals( Tmp, region, StringComparison.CurrentCultureIgnoreCase ) )
				{
					NewRegion = Tmp;
					break;
				}
			}
		}
		return NewRegion;
	}

	// private string TripItemToString( TripItem ti )
	// {
	// 	var sb = new StringBuilder();
	// 	sb.Append( "TripItem: " ).Append( ti.Description ).Append( ", ItemCode: " ).Append( ti.ItemCode ).Append( ", ItemCode1: " ).Append( ti.ItemCode1 );
	// 	sb.Append( ", Pieces: " ).Append( ti.Pieces ).Append( ", Weight" ).Append( ti.Weight );
	//
	// 	return sb.ToString();
	// }

	public async Task<string> GetNewTripId() => await Azure.Client.RequestGetNextTripId();

	/// <summary>
	/// </summary>
	/// <param
	///     name="onlyTripId">
	/// </param>
	public async Task Execute_NewTrip( bool onlyTripId = false )
	{
		Logging.WriteLogLine( "New Trip" );
		IsNewTrip = true;

		try
		{
			//Execute_ClearTripEntry();
			DeliveryAddressUpdate = null;
			PickupAddressUpdate   = null;

			var NewTripId = await Azure.Client.RequestGetNextTripId();
			Logging.WriteLogLine( "Got new tripid: " + NewTripId );

			Dispatcher.Invoke( () =>
							   {
								   TripId      = NewTripId;
								   CallTakerId = Globals.DataContext.MainDataContext.UserName;
								   var Now          = DateTime.Now;
								   var SavedAccount = SelectedAccount;

								   // Using this to lock the TripId field
								   CurrentTrip = new DisplayTrip
											     {
												     TripId    = NewTripId,
												     Packages  = new ObservableCollection<DisplayPackage>(),
												     CallTime  = Now,
												     ReadyTime = Now,
												     DueTime   = Now
											     };

								   var Dp = new DisplayPackage( new TripPackage
														        {
															        Pieces = 1,
															        Weight = 1
														        } );
								   CurrentTrip.Packages.Add( Dp );
								   var Dict = new SortedDictionary<int, List<ExtendedInventory>>();
								   TripItemsInventory           = Dict;
								   SelectedInventoryForTripItem = new List<ExtendedInventory>();

								   //TripItemsInventory.Add()

								   SelectedAccount = Account = SavedAccount;

								   SelectedPickupAddress   = "";
								   SelectedDeliveryAddress = "";

								   TotalWeight   = 1;
								   TotalPieces   = 1;
								   TotalOriginal = 1;

								   TripPackages.Clear();

								   //TripPackages.Add(new TripPackage()); // Need 1 for the first row
								   TripPackages.Add( new DisplayPackage( new TripPackage
																         {
																	         Pieces = 1,
																	         Weight = 1
																         } ) );

								   //CurrentTrip = new DisplayTrip(new Trip())
								   //{
								   //    TripId = newTripId,
								   //    Packages = new List<TripPackage>() { new TripPackage 
								   //                                               {
								   //                                                   Weight = 1,
								   //                                                   Pieces = 1
								   //                                               }
								   //                                       }

								   //};
							   } );

			if( !onlyTripId )
			{
				Dispatcher.Invoke( () =>
								   {
									   //Logging.WriteLogLine( "SelectedAccount: " + SelectedAccount + ", Account: " + Account );

									   WhenAccountChanges(); // Reload the addresses

									   //var now = DateTimeOffset.Now;
									   var Now = DateTime.Now;
									   CallDate = Now;

									   //CallTime = now;

									   ReadyDate = Now;

									   //ReadyTime = now;

									   DueDate = Now;

									   //DueTime = now;

									   //Pieces = 1;
									   //Weight = 1;
								   } );
			}

			if( ServiceLevel.IsNullOrWhiteSpace() )
				ServiceLevel = ServiceLevels[ 0 ];
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + E );
		}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="keepCurrentTrip">
	/// </param>
	/// <param
	///     name="country">
	/// </param>
	public void Execute_ClearTripEntry( bool keepCurrentTrip = false, string? country = null )
	{
		//Logging.WriteLogLine( "Clearing TripEntry" );
		IsNewTrip = false;

		CallTakerId = Globals.DataContext.MainDataContext.UserName;

		var Now = DateTime.Now;

		Account = string.Empty;

		AddressNames.Clear();

		//CallDate = DateTimeOffset.Now;
		CallDate  = Now;
		CallEmail = string.Empty;
		CallName  = string.Empty;
		CallPhone = string.Empty;

		//CallTime = DateTimeOffset.Now;
		//CallTime = now;
		if( !keepCurrentTrip )
		{
			PreviousTrip = CurrentTrip;
			CurrentTrip  = null;
		}

		if( Countries.Count < 3 )
		{
			Logging.WriteLogLine( "Reloading Countries" );
			Countries = CountriesRegions.Countries;
		}

		if( country == null )
		{
			WhenPickupCountryChanges();
			WhenDeliveryCountryChanges();
		}

		Customer              = string.Empty;
		CustomerNotes         = string.Empty;
		CustomerPhone         = string.Empty;
		DeliveryAddressNotes  = string.Empty;
		DeliveryAddressUpdate = null;
		DeliveryCity          = string.Empty;
		DeliveryCompany       = string.Empty;

		if( country.IsNotNullOrWhiteSpace() )
		{
			Dispatcher.Invoke( () =>
							   {
								   if( country != null )
								   {
									   DeliveryCountry = country;
									   WhenDeliveryCountryChanges();
								   }
							   } );
		}
		else
			DeliveryCountry = string.Empty;

		//DeliveryCountry      = string.Empty;
		DeliveryDefault   = false;
		DeliveryLocation  = string.Empty;
		DeliveryName      = string.Empty;
		DeliveryNotes     = string.Empty;
		DeliveryPhone     = string.Empty;
		DeliveryPostalZip = string.Empty;
		DeliveryProvState = string.Empty;
		DeliveryStreet    = string.Empty;
		DeliverySuite     = string.Empty;
		DeliveryZone      = string.Empty;
		DriverCode        = string.Empty;
		DriverNotes       = string.Empty;

		//DueDate = DateTimeOffset.Now;
		//DueTime = DateTimeOffset.Now;
		//DueDate = Now;
		DueDate = GetDueByTimeDefault();
		DueTime = GetDueByTimeDefault();

		//DueTime = now;
		FirstName = string.Empty;

		//Height = 0;
		IsDIM    = false;
		IsQuote  = false;
		LastName = string.Empty;

		//Length = 0;
		MiddleNames = string.Empty;

		//Original = 0;
		PackageType         = string.Empty;
		PickupAddressNotes  = string.Empty;
		PickupAddressUpdate = null;
		PickupCity          = string.Empty;
		PickupCompany       = string.Empty;

		if( country.IsNotNullOrWhiteSpace() )
		{
			Dispatcher.Invoke( () =>
							   {
								   if( country != null )
								   {
									   PickupCountry = country;
									   WhenPickupCountryChanges();
								   }
							   } );
		}
		else
			PickupCountry = string.Empty;

		//PickupCountry      = string.Empty;
		PickupDefault   = false;
		PickupLocation  = string.Empty;
		PickupName      = string.Empty;
		PickupNotes     = string.Empty;
		PickupPhone     = string.Empty;
		PickupPostalZip = string.Empty;
		PickupProvState = string.Empty;
		PickupStreet    = string.Empty;
		PickupSuite     = string.Empty;
		PickupZone      = string.Empty;

		//Pieces = 1;
		POD = string.Empty;
		POP = string.Empty;

		//QH = 0;

		//ReadyDate = DateTimeOffset.Now;
		//ReadyTime = DateTimeOffset.Now;
		// ReadyDate = Now;
		ReadyDate = GetReadyTimeDefault();
		ReadyTime = GetReadyTimeDefault();

		//ReadyTime = now;
		Reference               = string.Empty;
		SelectedDeliveryAddress = string.Empty;
		SelectedPickupAddress   = string.Empty;
		ServiceLevel            = string.Empty;
		Status1                 = -1;
		TDEL                    = string.Empty;
		TextBoxesEditable       = true;
		TPU                     = string.Empty;
		TotalOriginal           = 0;
		TotalPieces             = 0;
		TotalWeight             = 0;
		TripId                  = string.Empty;
		TripPackages.Clear();

		//TripPackages.Add(new TripPackage()); // Need 1 for the first row
		TripPackages.Add( new DisplayPackage( new TripPackage
										      {
											      Pieces = 1,
											      Weight = 1
										      } ) );
		UndeliverableNotes = string.Empty;

		//Weight = 1;

		// Inventory
		SelectedInventory            = null;
		SelectedInventoryBarcode     = string.Empty;
		SelectedInventoryName        = string.Empty;
		SelectedInventoryForTripItem = new List<ExtendedInventory>();
		TripItemsInventory           = new SortedDictionary<int, List<ExtendedInventory>>();

		TripCharges.Clear();

		TotalDeliveryCharges = TotalCharges = TotalFuelSurcharges = TotalDiscount = TotalBeforeTax = TotalTaxes = TotalTotal = TotalPayroll;

		//ApplyDefaultsToAddressFields();
		ApplyDefaultsToNoneAccountFields();

		DueDate   = GetDueByTimeDefault();
		DueTime   = GetDueByTimeDefault();
		ReadyDate = GetReadyTimeDefault();
		ReadyTime = GetReadyTimeDefault();

		if( View != null )
		{
			View.cbPickupAddressCountry.SelectedIndex   = -1;
			View.cbPickupAddressProv.SelectedIndex      = -1;
			View.cbDeliveryAddressCountry.SelectedIndex = -1;
			View.cbDeliveryAddressProv.SelectedIndex    = -1;
		}
	}


	/// <summary>
	/// </summary>
	/// <param
	///     name="controls">
	///     Contains pairs of Controls and the property name (eg. tbTripId, TripId)
	/// </param>
	/// <returns>List of errors</returns>
	public async Task<List<string>> Execute_SaveTrip( Dictionary<string, Control> controls )
	{
		var Errors              = await ValidateTrip( controls );
		var invalidAccountError = SetInvalidAccountErrorState( controls[ "CompanyNames" ], FindStringResource( "TripEntryValidationErrorsInvalidAccount" ) );

		if( invalidAccountError.IsNotNullOrWhiteSpace() )
		{
			Errors.Add( invalidAccountError );
			//SaveErrors.Add( invalidAccountError );
		}

		if( Errors.Count == 0 )
		{
			//Logging.WriteLogLine( "Trip is valid - saving" );			

			//_ = Task.Run(async () =>
			//  {
			//      await SaveDefaultsPreference();
			//  });
			// Do before any values are cleared
			await SaveDefaultsPreference();

			// Check for changed postalcodes/zips.  They can be changed
			var pickupPCode = PickupPostalZip.Trim();

			if( DoesAddressNameExist( SelectedPickupAddress ) )
			{
				var addr = ( from A in Addresses
						     where A.CompanyName == SelectedPickupAddress
						     select A ).FirstOrDefault();

				if( addr != null )
				{
					if( addr.PostalCode == pickupPCode )
						pickupPCode = string.Empty;
				}
			}
			var deliveryPCode = DeliveryPostalZip.Trim();

			if( DoesAddressNameExist( SelectedDeliveryAddress ) )
			{
				var addr = ( from A in Addresses
						     where A.CompanyName == SelectedDeliveryAddress
						     select A ).FirstOrDefault();

				if( addr != null )
				{
					if( addr.PostalCode == deliveryPCode )
						deliveryPCode = string.Empty;
				}
			}

			var Trip = BuildTrip();
			Trip.CallTakerId = CallTakerId;
			Trip             = CheckForPredateDispatch( Trip );

			IsNewTrip = false;

			//latestVersion = GetLatestVersion(TripId);
			var GetTrip = new GetTrip
						  {
							  Signatures = true,
							  TripId     = TripId
						  };
			var LatestVersion = await Azure.Client.RequestGetTrip( GetTrip );

			if( !CheckDeleted( LatestVersion ) )
				LatestVersion = null;

			if( LatestVersion is not null && ( LatestVersion.Status1 > STATUS.ACTIVE ) )
			{
				Trip.Status1      = LatestVersion.Status1;
				SelectedDriver    = Trip.Driver = LatestVersion.Driver;
				Trip.PickupTime   = LatestVersion.PickupTime;
				Trip.DeliveryTime = LatestVersion.DeliveryTime;
				Trip.POP          = LatestVersion.POP;
				Trip.POD          = LatestVersion.POD;
				Trip.Signatures.Clear();
				Trip.Signatures.AddRange( LatestVersion.Signatures );

				Trip.Reference = Reference.Trim();
			}

			AddCustomerCompany? Pickup = null;
			//UpdateCustomerCompany? PickupUpdate = null;

			//bool isPickupNew = false;
			AddCustomerCompany? Delivery = null;

			//bool isDeliveryNew = false;

			// Problem - every trip will result in a new Address
			// Solution - compare new address to old and only save if new

			// Check to see if the addresses exist - add if not
			// Note: this will mean that if an existing address is selected and then modified, 
			// the modified address will only exist in the trip; the addressbook is _not_ updated.			
			if( !DoesAddressNameExist( SelectedPickupAddress ) )
			{
				var Id = GenerateIdFromAddressName( SelectedPickupAddress );
				//Logging.WriteLogLine( "Generated pickup id: " + Id );

				var (latitude, longitude)   = GeolocateAddress( PickupStreet, PickupCity, PickupProvState, PickupCountry, PickupPostalZip );
				Trip.PickupAddressLatitude  = latitude;
				Trip.PickupAddressLongitude = longitude;

				var PuCompany = new Company
								{
									UserName        = PickupName,
									ContactName     = PickupName,
									AddressLine1    = PickupStreet.Trim(),
									City            = PickupCity.Trim(),
									CompanyName     = SelectedPickupAddress.Trim() + ( UseGlobalAddressBook ? "'" : "" ),
									CompanyNumber   = Id,
									Country         = PickupCountry?.Trim() ?? "",
									Phone           = PickupPhone.Trim(),
									LocationBarcode = PickupLocationBarcode.Trim(),
									PostalCode      = PickupPostalZip.Trim(),
									Region          = PickupProvState.Trim(),
									Suite           = PickupSuite.Trim(),
									Latitude        = latitude,
									Longitude       = longitude
								};

				Pickup = new AddCustomerCompany( PROGRAM )
						 {
							 Company = PuCompany,
							 //CustomerCode = Account
							 //CustomerCode = GlobalAddressBookId // New addresses are attached to the Global address book
							 CustomerCode = UseGlobalAddressBook ? GlobalAddressBookId : Account
						 };

				if( UseGlobalAddressBook )
					UpdateLocalGlobalAddresses( SelectedPickupAddress, PuCompany );
			}
			else
			{
				if( DetailedAddresses.ContainsKey( SelectedPickupAddress.ToLower() ) )
				{
					Trip.PickupAddressLatitude  = DetailedAddresses[ SelectedPickupAddress.ToLower() ].Company.Latitude;
					Trip.PickupAddressLongitude = DetailedAddresses[ SelectedPickupAddress.ToLower() ].Company.Longitude;
				}
				else if( GlobalDetailedAddresses.ContainsKey( SelectedPickupAddress.ToLower() ) )
				{
					Trip.PickupAddressLatitude  = GlobalDetailedAddresses[ SelectedPickupAddress.ToLower() ].Company.Latitude;
					Trip.PickupAddressLongitude = GlobalDetailedAddresses[ SelectedPickupAddress.ToLower() ].Company.Longitude;
				}

				if( PickupAddressUpdate is {Company: { }} && PickupAddressUpdate.CustomerCode.IsNotNullOrWhiteSpace() )
				{
					await Azure.Client.RequestUpdateCustomerCompany( PickupAddressUpdate );
					PickupAddressUpdate = null;
				}
				else if( pickupPCode.IsNotNullOrWhiteSpace() )
				{
					var id = string.Empty;

					if( DetailedAddresses.ContainsKey( SelectedPickupAddress.ToLower() ) )
						id = DetailedAddresses[ SelectedPickupAddress.ToLower() ].Company.CompanyNumber;
					else if( GlobalDetailedAddresses.ContainsKey( SelectedPickupAddress.ToLower() ) )
						id = GlobalDetailedAddresses[ SelectedPickupAddress.ToLower() ].Company.CompanyNumber;
					else
					{
						// Just in case
						id = GenerateIdFromAddressName( SelectedPickupAddress );
					}

					Company PuCompany = new()
										{
											CompanyNumber   = id,
											CompanyName     = SelectedPickupAddress.Trim(),
											ContactName     = PickupName,
											UserName        = PickupName,
											Phone           = PickupPhone.Trim(),
											LocationBarcode = PickupLocationBarcode.Trim(),
											Suite           = PickupSuite.Trim(),
											AddressLine1    = PickupStreet.Trim(),
											City            = PickupCity.Trim(),
											Country         = PickupCountry?.Trim() ?? "",
											Region          = PickupProvState.Trim(),
											PostalCode      = PickupPostalZip.Trim()
										};

					// Update - postal code has changed
					// Is it a global address or one belonging to the SelectedAccount?                    
					var PickupUpdate = new UpdateCustomerCompany( PROGRAM )
									   {
										   Company      = PuCompany,
										   CustomerCode = GlobalAddressBookId
									   };

					if( !GlobalDetailedAddresses.ContainsKey( SelectedPickupAddress.ToLower() ) )
						PickupUpdate.CustomerCode = SelectedAccount;
					else
						UpdateLocalGlobalAddresses( SelectedPickupAddress, PuCompany );
					await Azure.Client.RequestUpdateCustomerCompany( PickupUpdate );
				}
			}

			if( !DoesAddressNameExist( SelectedDeliveryAddress ) )
			{
				var Id = GenerateIdFromAddressName( SelectedDeliveryAddress );
				//Logging.WriteLogLine( "Generated delivery id: " + Id );

				var (latitude, longitude)     = GeolocateAddress( DeliveryStreet, DeliveryCity, DeliveryProvState, DeliveryCountry, DeliveryPostalZip );
				Trip.DeliveryAddressLatitude  = latitude;
				Trip.DeliveryAddressLongitude = longitude;

				var DelCompany = new Company
								 {
									 CompanyName     = SelectedDeliveryAddress.Trim() + ( UseGlobalAddressBook ? "'" : "" ),
									 CompanyNumber   = Id,
									 ContactName     = DeliveryName,
									 UserName        = DeliveryName,
									 Phone           = DeliveryPhone.Trim(),
									 LocationBarcode = DeliveryLocationBarcode.Trim(),
									 Suite           = DeliverySuite.Trim(),
									 AddressLine1    = DeliveryStreet.Trim(),
									 City            = DeliveryCity.Trim(),
									 Country         = DeliveryCountry?.Trim() ?? "",
									 Region          = DeliveryProvState.Trim(),
									 PostalCode      = DeliveryPostalZip.Trim(),
									 Latitude        = latitude,
									 Longitude       = longitude
								 };

				Delivery = new AddCustomerCompany( "TripEntryModel" )
						   {
							   Company = DelCompany,
							   //CustomerCode = Account
							   //CustomerCode = GlobalAddressBookId // New addresses are attached to the Global address book
							   CustomerCode = UseGlobalAddressBook ? GlobalAddressBookId : Account
						   };

				if( UseGlobalAddressBook )
					UpdateLocalGlobalAddresses( SelectedDeliveryAddress, DelCompany );
			}
			else
			{
				if( DetailedAddresses.ContainsKey( SelectedDeliveryAddress.ToLower() ) )
				{
					Trip.DeliveryAddressLatitude  = DetailedAddresses[ SelectedDeliveryAddress.ToLower() ].Company.Latitude;
					Trip.DeliveryAddressLongitude = DetailedAddresses[ SelectedDeliveryAddress.ToLower() ].Company.Longitude;
				}
				else if( GlobalDetailedAddresses.ContainsKey( SelectedDeliveryAddress.ToLower() ) )
				{
					Trip.DeliveryAddressLatitude  = GlobalDetailedAddresses[ SelectedDeliveryAddress.ToLower() ].Company.Latitude;
					Trip.DeliveryAddressLongitude = GlobalDetailedAddresses[ SelectedDeliveryAddress.ToLower() ].Company.Longitude;
				}

				// TODO Handle modified address
				if( ( DeliveryAddressUpdate != null ) && ( DeliveryAddressUpdate.Company != null ) && DeliveryAddressUpdate.CustomerCode.IsNotNullOrWhiteSpace() )
				{
					await Azure.Client.RequestUpdateCustomerCompany( DeliveryAddressUpdate );
					DeliveryAddressUpdate = null;
				}
				else if( deliveryPCode.IsNotNullOrWhiteSpace() )
				{
					var id = string.Empty;

					if( DetailedAddresses.ContainsKey( SelectedDeliveryAddress.ToLower() ) )
						id = DetailedAddresses[ SelectedDeliveryAddress.ToLower() ].Company.CompanyNumber;
					else if( GlobalDetailedAddresses.ContainsKey( SelectedDeliveryAddress.ToLower() ) )
						id = GlobalDetailedAddresses[ SelectedDeliveryAddress.ToLower() ].Company.CompanyNumber;
					else
					{
						// Just in case
						id = GenerateIdFromAddressName( SelectedDeliveryAddress );
					}

					Company DelCompany = new()
										 {
											 CompanyNumber   = id,
											 CompanyName     = SelectedDeliveryAddress.Trim(),
											 ContactName     = DeliveryName,
											 UserName        = DeliveryName,
											 Phone           = DeliveryPhone.Trim(),
											 LocationBarcode = DeliveryLocationBarcode.Trim(),
											 Suite           = DeliverySuite.Trim(),
											 AddressLine1    = DeliveryStreet.Trim(),
											 City            = DeliveryCity.Trim(),
											 Region          = DeliveryProvState.Trim(),
											 PostalCode      = DeliveryPostalZip.Trim(),
											 Country         = DeliveryCountry?.Trim() ?? ""
										 };

					// Update - postal code has changed
					// Is it a global address or one belonging to the SelectedAccount?                    
					var DeliveryUpdate = new UpdateCustomerCompany( PROGRAM )
										 {
											 Company      = DelCompany,
											 CustomerCode = GlobalAddressBookId
										 };

					if( !GlobalDetailedAddresses.ContainsKey( SelectedDeliveryAddress.ToLower() ) )
						DeliveryUpdate.CustomerCode = SelectedAccount;
					else
						UpdateLocalGlobalAddresses( SelectedDeliveryAddress, DelCompany );
					await Azure.Client.RequestUpdateCustomerCompany( DeliveryUpdate );
				}
			}

			if( Pickup != null )
				await Azure.Client.RequestAddCustomerCompany( Pickup );

			if( Delivery != null )
				await Azure.Client.RequestAddCustomerCompany( Delivery );

			await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( Trip ) );

			// Make copy for cloning
			var Sl = new Boards.Common.ServiceLevel
					 {
						 //RawServiceLevel = rsl
						 RawServiceLevel = new ServiceLevel
										   {
											   OldName = Trip.ServiceLevel
										   }
					 };
			PreviousTrip = new DisplayTrip( Trip, Sl );

			Dispatcher.Invoke( () =>
							   {
								   // Update the top bar
								   Status1 = (int)Trip.Status1;

								   MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryTripSaved" ),
											        (string)Application.Current.TryFindResource( "TripEntryTripSavedTitle" ),
											        MessageBoxButton.OK, MessageBoxImage.Asterisk );
							   } );
		}

		return Errors;
	}

	private Trip CheckForPredateDispatch( Trip trip )
	{
		// First check ReadyByDate
		if( ( ReadyDate != null ) && ( ReadyDate.Date > DateTime.Today ) )
		{
			Logging.WriteLogLine( "Found a future trip" );
			var predateDriver = string.Empty;

			if( ( Prefs != null ) && ( Prefs?.Count > 0 ) )
			{
				var pref = ( from P in Prefs
						     where P.Description.Contains( "Predates" )
						     select P ).FirstOrDefault();

				if( ( pref != null ) && pref.Enabled )
				{
					pref = ( from P in Prefs
						     where P.Description.Contains( "Predates Driver" )
						     select P ).FirstOrDefault();

					if( ( pref != null ) && pref.StringValue.IsNotNullOrWhiteSpace() )
						predateDriver = pref.StringValue;
				}
			}

			if( predateDriver.IsNotNullOrWhiteSpace() )
			{
				trip.Driver    = predateDriver;
				trip.Status1   = STATUS.DISPATCHED;
				Status1        = (int)STATUS.DISPATCHED;
				SelectedDriver = predateDriver;
			}
		}

		return trip;
	}

    private void UpdateLocalGlobalAddresses(string address, Company company)
    {
        if (GlobalAddresses.ContainsKey(address.ToLower()))
        {
            GlobalAddresses[address.ToLower()].CompanyName = address;
            //GlobalAddresses[address.ToLower()].u = company.ContactName;
            GlobalAddresses[address.ToLower()].Suite = company.Suite;
            GlobalAddresses[address.ToLower()].AddressLine1 = company.AddressLine1;
            GlobalAddresses[address.ToLower()].City = company.City;
            GlobalAddresses[address.ToLower()].Region = company.Region;
            //GlobalAddresses[address.ToLower()]. = company.Country;
            GlobalAddresses[address.ToLower()].PostalCode = company.PostalCode;
        }
		else
		{
			CompanyByAccountSummary cbas = new()
			{
				CompanyName  = address,
				Suite        = company.Suite,
				AddressLine1 = company.AddressLine1,
				City         = company.City,
				Region       = company.Region,
				PostalCode   = company.PostalCode
			};
			GlobalAddresses.Add(address.ToLower(), cbas);
		}

        if (GlobalDetailedAddresses.ContainsKey(address.ToLower()))
        {
            GlobalDetailedAddresses[address.ToLower()].Company.CompanyName = address;
            GlobalDetailedAddresses[address.ToLower()].Company.ContactName = company.UserName;
            GlobalDetailedAddresses[address.ToLower()].Company.UserName = company.UserName;
            GlobalDetailedAddresses[address.ToLower()].Company.Phone = company.Phone;
            GlobalDetailedAddresses[address.ToLower()].Company.Suite = company.Suite;
            GlobalDetailedAddresses[address.ToLower()].Company.AddressLine1 = company.AddressLine1;
            GlobalDetailedAddresses[address.ToLower()].Company.City = company.City;
            GlobalDetailedAddresses[address.ToLower()].Company.Region = company.Region;
            GlobalDetailedAddresses[address.ToLower()].Company.Country = company.Country;
            GlobalDetailedAddresses[address.ToLower()].Company.PostalCode = company.PostalCode;

            GlobalDetailedAddresses[address.ToLower()].Address.CompanyName = address;
            GlobalDetailedAddresses[address.ToLower()].Address.ContactName = company.UserName;
            GlobalDetailedAddresses[address.ToLower()].Address.Phone = company.Phone;
            GlobalDetailedAddresses[address.ToLower()].Address.Suite = company.Suite;
            GlobalDetailedAddresses[address.ToLower()].Address.AddressLine1 = company.AddressLine1;
            GlobalDetailedAddresses[address.ToLower()].Address.City = company.City;
            GlobalDetailedAddresses[address.ToLower()].Address.Region = company.Region;
            GlobalDetailedAddresses[address.ToLower()].Address.Country = company.Country;
            GlobalDetailedAddresses[address.ToLower()].Address.PostalCode = company.PostalCode;
        }
		else
		{
			CompanyDetail cd = new()
			{
                Company = company,
				Address = new()
				{
					CompanyName  = address,
					ContactName  = company.UserName,
					Phone        = company.Phone,
					Suite        = company.Suite,
					AddressLine1 = company.AddressLine1,
					City         = company.City,
					Region       = company.Region,
					Country      = company.Country,
					PostalCode   = company.PostalCode
				}
            };
			GlobalDetailedAddresses.Add(address.ToLower(), cd);
		}

		GlobalAddressesModel.UpdateGlobalAddresses(this, GlobalAddresses, GlobalDetailedAddresses);
    }

    private void UpdateLocalGlobalAddresses_V1( string address, Company company )
	{
		if( GlobalAddresses.ContainsKey( address.ToLower() ) )
		{
			GlobalAddresses[ address.ToLower() ].CompanyName = address;
			//GlobalAddresses[address.ToLower()].u = company.ContactName;
			GlobalAddresses[ address.ToLower() ].Suite        = company.Suite;
			GlobalAddresses[ address.ToLower() ].AddressLine1 = company.AddressLine1;
			GlobalAddresses[ address.ToLower() ].City         = company.City;
			GlobalAddresses[ address.ToLower() ].Region       = company.Region;
			//GlobalAddresses[address.ToLower()]. = company.Country;
			GlobalAddresses[ address.ToLower() ].PostalCode = company.PostalCode;
		}

		if( GlobalDetailedAddresses.ContainsKey( address.ToLower() ) )
		{
			GlobalDetailedAddresses[ address.ToLower() ].Company.CompanyName  = address;
			GlobalDetailedAddresses[ address.ToLower() ].Company.ContactName  = company.UserName;
			GlobalDetailedAddresses[ address.ToLower() ].Company.UserName     = company.UserName;
			GlobalDetailedAddresses[ address.ToLower() ].Company.Phone        = company.Phone;
			GlobalDetailedAddresses[ address.ToLower() ].Company.Suite        = company.Suite;
			GlobalDetailedAddresses[ address.ToLower() ].Company.AddressLine1 = company.AddressLine1;
			GlobalDetailedAddresses[ address.ToLower() ].Company.City         = company.City;
			GlobalDetailedAddresses[ address.ToLower() ].Company.Region       = company.Region;
			GlobalDetailedAddresses[ address.ToLower() ].Company.Country      = company.Country;
			GlobalDetailedAddresses[ address.ToLower() ].Company.PostalCode   = company.PostalCode;

			GlobalDetailedAddresses[ address.ToLower() ].Address.CompanyName  = address;
			GlobalDetailedAddresses[ address.ToLower() ].Address.ContactName  = company.UserName;
			GlobalDetailedAddresses[ address.ToLower() ].Address.Phone        = company.Phone;
			GlobalDetailedAddresses[ address.ToLower() ].Address.Suite        = company.Suite;
			GlobalDetailedAddresses[ address.ToLower() ].Address.AddressLine1 = company.AddressLine1;
			GlobalDetailedAddresses[ address.ToLower() ].Address.City         = company.City;
			GlobalDetailedAddresses[ address.ToLower() ].Address.Region       = company.Region;
			GlobalDetailedAddresses[ address.ToLower() ].Address.Country      = company.Country;
			GlobalDetailedAddresses[ address.ToLower() ].Address.PostalCode   = company.PostalCode;
		}
	}

	/*		private async Trip GetLatestVersion(string tripId)
			{
				Trip trip = null;
				Logging.WriteLogLine("Getting latest version of trip: " + tripId);
	
				//var getTrip = new GetTrip
				//{
				//    Signatures = true,
				//    TripId = tripId
				//};
				//trip = Azure.Client.RequestGetTrip(getTrip).Result;
	
				//Task.WaitAll(Task.Run(async () =>
				//{
				//    var getTrip = new GetTrip
				//    {
				//        Signatures = true,
				//        TripId = tripId
				//    };
				//    trip = await Azure.Client.RequestGetTrip(getTrip);
				//}));
	
				var getTrip = new GetTrip
				{
					Signatures = true,
					TripId = tripId
				};
				trip = await Azure.Client.RequestGetTrip(getTrip);
	
				return trip;
			}
	*/

	public async Task<List<string>> Execute_SaveTrip_orig2( Dictionary<string, Control> controls )
	{
		var Errors = await ValidateTrip( controls );

		if( Errors.Count == 0 )
		{
			//Logging.WriteLogLine( "Trip is valid - saving" );
			IsNewTrip = false;
			var Trip = BuildTrip();

			//bool isPickupNew = false;
			AddCustomerCompany? Delivery = null;

			//bool isDeliveryNew = false;

			// Problem - every trip will result in a new Address
			// Solution - compare new address to old and only save if new

			var Id = GenerateIdFromAddressName( SelectedPickupAddress );
			//Logging.WriteLogLine( "Generated pickup id: " + Id );

			var PuCompany = new Company
							{
								AddressLine1  = PickupStreet.Trim(),
								City          = PickupCity.Trim(),
								CompanyName   = SelectedPickupAddress.Trim(),
								CompanyNumber = Id,
								Country       = PickupCountry?.Trim() ?? "",
								Phone         = PickupPhone.Trim(),
								PostalCode    = PickupPostalZip.Trim(),
								Region        = PickupProvState.Trim(),
								Suite         = PickupSuite.Trim()
							};

			var Pickup = new AddCustomerCompany( "TripEntryModel" )
						 {
							 Company      = PuCompany,
							 CustomerCode = Account
						 };

			if( SelectedPickupAddress != SelectedDeliveryAddress )
			{
				Id = GenerateIdFromAddressName( SelectedDeliveryAddress );
				//Logging.WriteLogLine( "Generated delivery id: " + Id );

				var DelCompany = new Company
								 {
									 AddressLine1  = DeliveryStreet.Trim(),
									 City          = DeliveryCity.Trim(),
									 CompanyName   = SelectedDeliveryAddress.Trim(),
									 CompanyNumber = Id,
									 Country       = DeliveryCountry?.Trim() ?? "",
									 Phone         = DeliveryPhone.Trim(),
									 PostalCode    = DeliveryPostalZip.Trim(),
									 Region        = DeliveryProvState.Trim(),
									 Suite         = DeliverySuite.Trim()
								 };

				Delivery = new AddCustomerCompany( "TripEntryModel" )
						   {
							   Company      = DelCompany,
							   CustomerCode = Account
						   };
			}
			else
				Logging.WriteLogLine( "PickupAddress and DeliveryAddress are the same" );

			// Check to see if the addresses have been modified
			if( DoesAddressNameExist( SelectedPickupAddress ) )
			{
				foreach( var Ca in Companies )
				{
					if( Ca.CompanyName == SelectedPickupAddress )
					{
						if( CompareCompanies( Pickup.Company, Ca ) )
							Pickup = null;

						break;
					}
				}
			}

			if( DoesAddressNameExist( SelectedDeliveryAddress ) )
			{
				foreach( var Ca in Companies )
				{
					if( Ca.CompanyName == SelectedDeliveryAddress )
					{
						if( CompareCompanies( Delivery?.Company, Ca ) )
							Delivery = null;
						break;
					}
				}
			}

			if( Pickup is not null )
				await Azure.Client.RequestAddCustomerCompany( Pickup );

			if( Delivery is not null )
				await Azure.Client.RequestAddCustomerCompany( Delivery );

			await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( Trip ) );

			Dispatcher.Invoke( () =>
							   {
								   // Update the top bar
								   Status1 = (int)Trip.Status1;

								   MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryTripSaved" ),
											        (string)Application.Current.TryFindResource( "TripEntryTripSavedTitle" ),
											        MessageBoxButton.OK, MessageBoxImage.Asterisk );
							   } );
		}

		return Errors;
	}

	private static bool CompareCompanies( Company? newCompany, CompanyAddress origCompany )
	{
		var Same = true;

		if( newCompany?.AddressLine1 != origCompany.AddressLine1 )
			Same = false;
		else
		{
			switch( Same )
			{
			case true when newCompany.AddressLine2 != origCompany.AddressLine2:
			case true when newCompany.City != origCompany.City:
			case true when newCompany.CompanyNumber != origCompany.CompanyNumber:
			case true when newCompany.Country != origCompany.Country:
			case true when newCompany.EmailAddress != origCompany.EmailAddress:
			case true when newCompany.Notes != origCompany.Notes:
			case true when newCompany.Phone != origCompany.Phone:
			case true when newCompany.PostalCode != origCompany.PostalCode:
			case true when newCompany.Region != origCompany.Region:
			case true when newCompany.Suite != origCompany.Suite:
				Same = false;
				break;
			}
		}

		//else if (same && newCompany.UserName != origCompany.UserName)
		//{
		//    same = false;
		//}
		if( !Same )
			Logging.WriteLogLine( "Entered company " + newCompany?.CompanyName + " is different to the original" );

		return Same;
	}

	public async Task<List<string>> Execute_SaveTrip_orig( Dictionary<string, Control> controls )
	{
		var Errors = await ValidateTrip( controls );

		if( Errors.Count == 0 )
		{
			//Logging.WriteLogLine( "Trip is valid - saving" );
			IsNewTrip = false;
			var Trip = BuildTrip();

			AddCustomerCompany? Pickup   = null,
								Delivery = null;

			if( !DoesAddressNameExist( SelectedPickupAddress ) )
			{
				var Id = GenerateIdFromAddressName( SelectedPickupAddress );
				//Logging.WriteLogLine( "Generated id: " + Id );

				var Company = new Company
							  {
								  AddressLine1  = PickupStreet,
								  City          = PickupCity,
								  CompanyName   = SelectedPickupAddress,
								  CompanyNumber = Id,
								  Country       = PickupCountry,
								  Phone         = PickupPhone,
								  PostalCode    = PickupPostalZip,
								  Region        = PickupProvState,
								  Suite         = PickupSuite
							  };

				Pickup = new AddCustomerCompany( "TripEntryModel" )
						 {
							 Company      = Company,
							 CustomerCode = Account
						 };
			}

			if( !DoesAddressNameExist( SelectedDeliveryAddress ) )
			{
				if( SelectedPickupAddress != SelectedDeliveryAddress )
				{
					var Id = GenerateIdFromAddressName( SelectedDeliveryAddress );

					var Company = new Company
								  {
									  AddressLine1  = DeliveryStreet,
									  City          = DeliveryCity,
									  CompanyName   = SelectedDeliveryAddress,
									  CompanyNumber = Id,
									  Country       = DeliveryCountry,
									  Phone         = DeliveryPhone,
									  PostalCode    = DeliveryPostalZip,
									  Region        = DeliveryProvState,
									  Suite         = DeliverySuite
								  };

					Delivery = new AddCustomerCompany( "TripEntryModel" )
							   {
								   Company      = Company,
								   CustomerCode = Account
							   };
				}
				else
					Logging.WriteLogLine( "PickupAddress and DeliveryAddress are the same" );
			}

			if( Pickup is not null )
				await Azure.Client.RequestAddCustomerCompany( Pickup );

			if( Delivery is not null )
				await Azure.Client.RequestAddCustomerCompany( Delivery );

			await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( Trip ) );

			Dispatcher.Invoke( () =>
							   {
								   MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryTripSaved" ),
											        (string)Application.Current.TryFindResource( "TripEntryTripSavedTitle" ),
											        MessageBoxButton.OK, MessageBoxImage.Asterisk );
							   } );
		}

		return Errors;
	}

	private static Trip UpdateBroadcast( Trip t )
	{
		switch( t.Status1 )
		{
		case STATUS.ACTIVE:
			t.BroadcastToDispatchBoard = true;
			break;

		case STATUS.DISPATCHED:
		case STATUS.PICKED_UP:
			t.BroadcastToDispatchBoard = true; // Shipment entry can also dispatch
			t.BroadcastToDriverBoard   = true;
			t.BroadcastToDriver        = true;
			break;
		}

		return t;
	}

	private bool DoesAddressNameExist( string address )
	{
		var Exists = false;

		if( address.IsNotNullOrWhiteSpace() )
		{
			foreach( var Name in AddressNames )
			{
				if( string.Equals( Name, address.Trim(), StringComparison.CurrentCultureIgnoreCase ) )
				{
					Exists = true;
					break;
				}
			}
		}

		return Exists;
	}

	private bool DoesAddressIdExist( string id )
	{
		var Exists = false;

		if( id.IsNotNullOrWhiteSpace() )
		{
			foreach( var Comp in Companies )
			{
				if( Comp.CompanyNumber == id )
				{
					Exists = true;
					break;
				}
			}
		}

		return Exists;
	}

	/// <summary>
	///     Tries to create an id from the letters and digits in the name.
	///     If it exists, it tries to find a unique one by appending a digit.
	///     If, after 100000 attempts, one hasn't been found, it appends
	///     the current time (in ticks) to the name
	/// </summary>
	/// <param
	///     name="name">
	/// </param>
	/// <returns></returns>
	private string GenerateIdFromAddressName( string name )
	{
		if( name.IsNullOrWhiteSpace() )
		{
			name = SelectedAccount.IsNotNullOrWhiteSpace() ? SelectedAccount.Trim() + "_Id_" : "AddressId_";
			Logging.WriteLogLine( "name is null or empty - using " + name );
		}
		var Id      = name;
		var Attempt = 1;

		while( true )
		{
			Id = new string( Id.ToCharArray()
						       .Where( char.IsLetterOrDigit )
						       .ToArray() );

			if( !DoesAddressIdExist( Id ) )
				break;

			if( Attempt < 100000 )
			{
				Id = name + Attempt; // Append a digit
				++Attempt;
			}
			else
			{
				// Give up - append the timestamp
				Id = new string( name.ToCharArray()
								     .Where( char.IsLetterOrDigit )
								     .ToArray() );

				Id += "_" + DateTime.Now.Ticks;
				break;
			}
		}

		return Id;
	}

	public void Execute_ProduceWaybill( string printerName, Window parent )
	{
		Logging.WriteLogLine( "Produce waybill" );

		if( CurrentTrip is null && PreviousTrip is not null )
		{
			//Logging.WriteLogLine( "Copying PreviousTrip into CurrentTrip for cloning" );
			CurrentTrip = PreviousTrip;
		}

		if( CurrentTrip is not null )
		{
			//Logging.WriteLogLine( "TripId: " + CurrentTrip.TripId );
			Trip Trip = CurrentTrip;

			var ResellerId = Globals.DataContext.MainDataContext.Account;
			var Phone      = CurrentTrip.BillingAddressPhone;

			Dispatcher.Invoke( () =>
							   {
								   var WeighbillDialog = new WeighbillDialog( printerName, Trip, false, string.Empty, false, ResellerId, Phone )
													     {
														     Owner = parent
													     };
								   WeighbillDialog.ShowDialog();
							   } );
		}
		else
			Logging.WriteLogLine( "No trip selected" );
	}

	public void Execute_ProduceTracking( Window parent )
	{
		Logging.WriteLogLine( "Produce tracking" );

		if( ( CurrentTrip == null ) && ( PreviousTrip != null ) )
		{
			//Logging.WriteLogLine( "Copying PreviousTrip into CurrentTrip for cloning" );
			CurrentTrip = PreviousTrip;
		}

		if( CurrentTrip != null )
		{
			Trip Trip = CurrentTrip;

			var TrackingDialog = new TrackingDialog( Trip )
								 {
									 Owner = parent
								 };
			TrackingDialog.ShowDialog();
		}
		else
			Logging.WriteLogLine( "No trip selected" );
	}

	public async Task Execute_ProduceAudit( Window parent )
	{
		Logging.WriteLogLine( "Produce audit" );

		if( ( CurrentTrip == null ) && ( PreviousTrip != null ) )
		{
			//Logging.WriteLogLine( "Copying PreviousTrip into CurrentTrip for cloning" );
			CurrentTrip = PreviousTrip;
		}

		if( CurrentTrip is not null || TripId.IsNotNullOrWhiteSpace() )
		{
			DisplayTrip? Trip;

			if( CurrentTrip is not null )
				Trip = CurrentTrip;
			else
			{
				if( PreviousTrip is not null && ( PreviousTrip.TripId == TripId ) )
					Trip = PreviousTrip;
				else
				{
					// Load trip
					Trip = await GetLatestVersion( TripId );
				}
			}

			//Trip trip = this.CurrentTrip;
			////var lines = GetAuditTripLines();
			//PrintAuditDialogModel model = new PrintAuditDialogModel(trip);
			//var lines = model.Lines;

			await Dispatcher.Invoke( async () =>
								     {
									     // GetAuditLines();
									     AuditLines = await GetAuditLines();

									     //var dialog = new PrintAuditDialog( CurrentTrip, AuditLines )
									     var Dialog = new PrintAuditDialog( Trip, AuditLines )
												      {
													      Owner = parent
												      };
									     Dialog.ShowDialog();
								     } );
		}
		else
			Logging.WriteLogLine( "No trip selected" );
	}

	private bool CheckDeleted( TripUpdate trip )
	{
		switch( trip.Status1 )
		{
		case STATUS.DELETED:
		case STATUS.FINALISED:
			OnCannotEditDeleted?.Invoke();
			return false;

		default:
			return true;
		}
	}

	private async Task<DisplayTrip?> GetLatestVersion( string tripId )
	{
		DisplayTrip? Dt = null;
		//Logging.WriteLogLine( "Getting latest version of trip: " + tripId );

		var GetTrip = new GetTrip
					  {
						  Signatures = true,
						  TripId     = tripId
					  };
		var Trip = await Azure.Client.RequestGetTrip( GetTrip );

		if( CheckDeleted( Trip ) )
		{
			Dt = new DisplayTrip( Trip, new Boards.Common.ServiceLevel() )
				 {
					 ServiceLevel = Trip.ServiceLevel
				 };
		}

		return Dt;
	}

	//private ObservableCollection<string> GetAuditTripLines()
	//{
	//    var NewItems = new ObservableCollection<string>();
	//    if (CurrentTrip != null)
	//    {
	//        Utils.Logging.WriteLogLine("Loading log for " + CurrentTrip.TripId);
	//        if (!IsInDesignMode)
	//        {
	//            //MainWindow.MainWindowModel.
	//            var Lookup = new LogLookup
	//            {
	//                Log = Log,
	//                FromDate = DateTimeOffset.MinValue,
	//                ToDate = DateTimeOffset.Now,
	//                User = User
	//            };

	//            Task.Run(async () =>
	//                     {
	//                         try
	//                         {
	//                             var Lg = await Azure.Client.RequestGetLog(RequestOverride(Lookup));
	//                             foreach (var Entry in Lg)
	//                             {
	//                                 //Entry.Data = FormatData(Entry.Data);
	//                                 //NewItems.Add(new DisplayEntry(Entry));
	//                             }

	//                             Dispatcher.Invoke(() =>
	//                                                {
	//                                                    Lines = NewItems;
	//                                                });
	//                         }
	//                         catch (Exception Exception)
	//                         {
	//                             Console.WriteLine(Exception);
	//                         }
	//                     });
	//        }
	//    }

	//    return NewItems;
	//}
	public List<AuditLine> AuditLines
	{
		get { return Get( () => AuditLines, new List<AuditLine>() ); }
		set { Set( () => AuditLines, value ); }
	}


	public Func<string, string> FormatData = data => data;

	private async Task<List<AuditLine>> GetAuditLines( TripUpdate? trip = null )
	{
		var NewItems = new List<AuditLine>();

		var TripTripId = CurrentTrip is not null ? CurrentTrip.TripId : trip?.TripId;

		//if( CurrentTrip != null )
		if( TripTripId.IsNotNullOrWhiteSpace() )
		{
			Logging.WriteLogLine( "Loading log for " + CurrentTrip?.TripId );

			if( !IsInDesignMode )
			{
				// From LogViewerModel
				var Lookup = new LogLookup
							 {
								 Log      = Log,
								 FromDate = DateTime.Now,
								 ToDate   = DateTime.Now,

								 //Key      = CurrentTrip.TripId
								 Key = TripTripId!
							 };

				try
				{
					var Lg = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );
					//Logging.WriteLogLine( "Found " + Lg.Count + " log lines" );

					//var LogItems = new ObservableCollection<String>();

					foreach( var Entry in Lg )
					{
						Entry.Data = FormatData( Entry.Data );

						//LogItems.Add(new DisplayEntry(L, Entry));
						var Row   = Entry.RowKey;
						var P     = Row.LastIndexOf( "--", StringComparison.Ordinal );
						var UserA = P < 0 ? "????" : Row.Substring( P + 2 );

						var Operation = Entry.Operation;
						var Data      = Entry.Data;
						Entry.Timestamp = Entry.Timestamp.AddHours( Entry.TimeZoneOffset ); // -8 here
						var Line = new AuditLine( Entry.Timestamp, UserA, Operation, Data );

						//Logging.WriteLogLine("Found " + line.Description + ", " + line.ToString());
						// LogItems.Add(L.ToString());
						NewItems.Add( Line );
					}

					//Dispatcher.Invoke( () => { AuditLines = NewItems; } );

					//Dispatcher.Invoke(() =>
					//               {
					//                   //NothingFound = LogItems.Count == 0;

					//                   //Items = LogItems;
					//                   //SearchButtonEnabled = true;
					//                   //NewItems = LogItems;
					//               });
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}

				// Original
				//  var Lookup = new LogLookup
				//{
				// Log = Log,

				// // FromDate = DateTimeOffset.Now.AddDays( -1 ), //DateTimeOffset.MinValue,
				// FromDate = DateTimeOffset.Parse( CurrentTrip.CallTime ),
				// ToDate = DateTimeOffset.Now,
				// Key = User
				//};

				//Task.Run( async () =>
				//          {
				//	          try
				//	          {
				//		          var Lg = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );
				//		          Logging.WriteLogLine( "Found " + Lg.Count + " Entries" );

				//		          if( Lg.Count > 0 )
				//		          {
				//			          foreach( var Entry in Lg )
				//			          {
				//				          if( !string.IsNullOrEmpty( Entry.Data ) )
				//				          {
				//					          // Problem - Nav is getting this message
				//					          // 2019-07-11 14:17:41.1753; Found 9 Entries; Method: GetAuditLines; File: TripEntryModel.cs; Line: 1891
				//					          // Unhandled exception : Not enough quota is available to process this command
				//					          //var line = new AuditLine( Entry.Timestamp, Entry.PartitionKey, Entry.Operation, Entry.Data );

				//					          string partitionKey,
				//					                 operation,
				//					                 data = string.Empty;

				//					          if( Entry.PartitionKey.Length > 1024 )
				//					          {
				//						          partitionKey = Entry.PartitionKey.Substring( 0, 1024 );
				//						          Logging.WriteLogLine( "Entry.PartitionKey.Length: " + Entry.PartitionKey.Length + " - truncating to 1024" );
				//					          }
				//					          else
				//					          {
				//						          partitionKey = Entry.PartitionKey;
				//					          }

				//					          if( Entry.Operation.Length > 1024 )
				//					          {
				//						          operation = Entry.Operation.Substring( 0, 1024 );
				//						          Logging.WriteLogLine( "Entry.Operation.Length: " + Entry.Operation.Length + " - truncating to 1024" );
				//					          }
				//					          else
				//					          {
				//						          operation = Entry.Operation;
				//					          }

				//					          if( Entry.Data.Length > 1024 )
				//					          {
				//						          data = Entry.Data.Substring( 0, 1024 );
				//						          Logging.WriteLogLine( "Entry.Data.Length: " + Entry.Data.Length + " - truncating to 1024" );
				//					          }
				//					          else
				//					          {
				//						          data = Entry.Data;
				//					          }

				//					          var line = new AuditLine( Entry.Timestamp, partitionKey, operation, data );
				//					          NewItems.Add( line );
				//				          }
				//			          }

				//			          Dispatcher.Invoke( () =>
				//			                             {
				//				                             AuditLines = NewItems;
				//			                             } );
				//		          }
				//	          }
				//	          catch( Exception Exception )
				//	          {
				//		          Logging.WriteLogLine( "Exception: " + Exception );
				//	          }
				//          } );
			}
		}

		return NewItems;
	}

	//public ObservableCollection<string> Lines
	//{
	//    get { return Get(() => Lines, GetAuditTripLines()); }
	//    set { Set(() => Lines, value); }
	//}
	public Func<LogLookup, LogLookup> RequestOverride = lookup => lookup;

	/// <summary>
	///     Design:
	///     TripEntry.xaml.cs builds a dictionary of binding names and controls.
	///     Only controls with active bindings (TextBoxes) or the Tag set (ComboBoxes) are included.
	///     This method is responsible for deciding which controls are checked.
	///     Widgets.SetErrorState handles the marking or clearing of the error in the widget.
	/// </summary>
	/// <param
	///     name="controls">
	///     Contains pairs of property names (from the control's binding) and Controls (eg. TripId,
	///     tbTripId)
	/// </param>
	/// <returns></returns>
	private async Task<List<string>> ValidateTrip( Dictionary<string, Control> controls )
	{
		var Errors = new List<string>();

		if( controls.Count > 0 )
		{
			foreach( var FieldName in controls.Keys )
			{
				var Error = FieldName switch
							{
								"TripId"       => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsTripId" ) ),
								"CompanyNames" => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCompanyNames" ) ),
								"Customer"     => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCustomer" ) ),
								// Re: SD1-T100 Don't flag CustomerPhone as an error if not new trip
								"CustomerPhone" => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCustomerPhone" ) ),

								// Gord - I2P-4
								//case "CallName":
								//	error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallerName" ) );
								//	break;
								//case "CallPhone":
								//	error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallerPhone" ) );
								//	break;
								"CallDate"      => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallDate" ) ),
								"CallTime"      => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallTime" ) ),
								"PickupCompany" => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupCompany" ) ),
								"PickupStreet"  => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupStreet" ) ),
								"PickupCity"    => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupCity" ) ),

								//case "PickupProvState":
								"PickupRegions"   => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupProvState" ) ),
								"PickupCountry"   => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupCountry" ) ),
								"PickupPostalZip" => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupPostalZip" ) ),
								"DeliveryCompany" => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryCompany" ) ),
								"DeliveryStreet"  => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryStreet" ) ),
								"DeliveryCity"    => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryCity" ) ),

								//case "DeliveryProvState":
								"DeliveryRegions"   => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryProvState" ) ),
								"DeliveryCountry"   => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryCountry" ) ),
								"DeliveryPostalZip" => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryPostalZip" ) ),
								"PackageTypes"      => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPackagePackageType" ) ),
								"Weight"            => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPackageWeight" ) ),
								"Pieces"            => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPackagePieces" ) ),
								"ServiceLevels"     => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsServiceLevel" ) ),
								"ReadyDate"         => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsReadyDate" ) ),
								"ReadyTime"         => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsReadyTime" ) ),
								"DueDate"           => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsDueDate" ) ),
								"DueTime"           => SetErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsDueTime" ) ),

								"Reference" => await SetReferenceErrorState( controls[ FieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCustomerReference" ) ),
								_           => string.Empty
							};

				if( !string.IsNullOrEmpty( Error ) )
				{
					//Errors.Add( Error );
					// Re: SD1-T100 Don't flag CustomerPhone as an error if not new trip
					if( IsNewTrip && ( FieldName != "CustomerPhone" ) )
						Errors.Add( Error );
					else
					{
						// Remove yellow
						controls[ FieldName ].Background = Brushes.White;
					}
				}
			}

			var (ok, slCheck) = CheckReadyAndDueForServiceLevel( ServiceLevel, controls );

			if( !ok )
				Errors.AddRange( slCheck );
		}

		return Errors;
	}


	public (bool ok, List<string> errors) CheckReadyAndDueForServiceLevel( string serviceLevel, Dictionary<string, Control> controls )
	{
		var          ok = true;
		string       error;
		List<string> errors = new();

		var field = "ReadyDate";
		var dtp   = (DateTimePicker)controls[ field ];
		var dt    = dtp?.Value;

		if( dt != null )
		{
			( ok, error ) = CheckDayOfWeekForServiceLevel( field, serviceLevel, (DateTime)dt );

			if( !ok )
				errors.Add( error );
		}
		field = "ReadyTime";
		var tp = (TimePicker)controls[ field ];
		dt = tp?.Value;

		if( dt != null )
		{
			( ok, error ) = CheckTimeOfDayForServiceLevel( field, serviceLevel, (DateTime)dt );

			if( !ok )
				errors.Add( error );
		}
		field = "DueDate";
		dtp   = (DateTimePicker)controls[ field ];
		dt    = dtp?.Value;

		if( dt != null )
		{
			( ok, error ) = CheckDayOfWeekForServiceLevel( field, serviceLevel, (DateTime)dt );

			if( !ok )
				errors.Add( error );
		}
		field = "DueTime";
		dt    = dtp?.Value;

		if( dt != null )
		{
			( ok, error ) = CheckTimeOfDayForServiceLevel( field, serviceLevel, (DateTime)dt );

			if( !ok )
				errors.Add( error );
		}

		return ( ok, errors );
	}

	public (bool ok, string error) CheckDayOfWeekForServiceLevel( string field, string serviceLevel, DateTime time )
	{
		var ok       = true;
		var error    = string.Empty;
		var template = (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsDayOfWeekForServiceLevel" );
		template = template.Replace( "@1", serviceLevel );
		template = template.Replace( "@3", field );

		var sl = ( from S in CompleteServiceLevels
				   where S.OldName == serviceLevel
				   select S ).FirstOrDefault();

		if( sl != null )
		{
			var day = time.DayOfWeek;

			switch( (int)day )
			{
			case 0:
				if( !sl.Sunday )
				{
					ok    = false;
					error = template.Replace( "@2", "Sunday" );
				}
				break;

			case 1:
				if( !sl.Monday )
				{
					ok    = false;
					error = template.Replace( "@2", "Monday" );
				}
				break;

			case 2:
				if( !sl.Tuesday )
				{
					ok    = false;
					error = template.Replace( "@2", "Tuesday" );
				}
				break;

			case 3:
				if( !sl.Wednesday )
				{
					ok    = false;
					error = template.Replace( "@2", "Wednesday" );
				}
				break;

			case 4:
				if( !sl.Thursday )
				{
					ok    = false;
					error = template.Replace( "@2", "Thursday" );
				}
				break;

			case 5:
				if( !sl.Friday )
				{
					ok    = false;
					error = template.Replace( "@2", "Friday" );
				}
				break;

			case 6:
				if( !sl.Saturday )
				{
					ok    = false;
					error = template.Replace( "@2", "Saturday" );
				}
				break;

			default:
				ok    = false;
				error = "Invalid day of the week: " + day;
				break;
			}
		}

		if( !ok )
			Logging.WriteLogLine( error );

		return ( ok, error );
	}

	public (bool ok, string error) CheckTimeOfDayForServiceLevel( string field, string serviceLevel, DateTime time )
	{
		var ok       = true;
		var error    = string.Empty;
		var template = (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsTimeOfDayForServiceLevel" );
		template = template.Replace( "@1", serviceLevel );
		template = template.Replace( "@2", time.ToString() );
		template = template.Replace( "@3", field );

		var sl = ( from S in CompleteServiceLevels
				   where S.OldName == serviceLevel
				   select S ).FirstOrDefault();

		if( sl != null )
		{
			//if (sl.StartTime.Hours > time.Hour || sl.StartTime.Minutes > time.Minute)
			//            {
			//	ok = false;
			//	error = template;
			//            }
			//else if (sl.EndTime.Hours < time.Hour || sl.EndTime.Minutes < time.Minute)
			//{
			//	ok = false;
			//	error = template;
			//}
			//if (sl.StartTime.Hours <= time.Hour && sl.StartTime.Minutes <= time.Minute && sl.EndTime.Hours >= time.Hour && sl.EndTime.Minutes >= time.Minute)
			var st = new DateTime( time.Year, time.Month, time.Day, sl.StartTime.Hours, sl.StartTime.Minutes, 0 );
			var et = new DateTime( time.Year, time.Month, time.Day, sl.EndTime.Hours, sl.EndTime.Minutes, 0 );

			if( ( time < st ) || ( time > et ) )
			{
				ok    = false;
				error = template;
			}

			//if (sl.StartTime.Hours <= time.Hour && sl.EndTime.Hours >= time.Hour)
			//            {
			//	if (sl.StartTime.Minutes <= time.Minute && sl.EndTime.Minutes >= time.Minute)
			//                {
			//                }
			//}
			//else
			//            {
			//	ok = false;
			//                error = template;
			//            }
		}

		if( !ok )
			Logging.WriteLogLine( error );

		return ( ok, error );
	}


	/// <summary>
	///     Creates the trip object.
	/// </summary>
	/// <returns></returns>
	private Trip BuildTrip()
	{
		var Trip = new Trip();

		if( CurrentTrip != null )
			Trip = CurrentTrip;

		Trip.Program                  = PROGRAM;
		Trip.BroadcastToDispatchBoard = true;
		Trip.TripId                   = TripId.Trim();
		Trip.AccountId                = Account.Trim();
		Trip.Reference                = Reference.Trim();

		if( IsNewTrip )
			Trip.CallTakerId = Globals.DataContext.MainDataContext.UserName;
		// Only change if it is empty
		//if (!IsNewTrip && Trip.CallTakerId.IsNullOrWhiteSpace() )
		//      {
		//	Trip.CallTakerId              = Globals.DataContext.MainDataContext.UserName;
		//      }

		Trip.BillingCompanyName  = Customer.Trim();
		Trip.BillingAddressPhone = CustomerPhone;
		Trip.BillingAddressNotes = CustomerNotes ?? "";

		Trip.CallerName  = CallName.Trim();
		Trip.CallerPhone = CallPhone.Trim();
		Trip.CallerEmail = CallEmail.Trim();

		//TimeSpan ts = new TimeSpan(this.CallTime.Hour, this.CallTime.Minute, this.CallTime.Second);            
		//trip.CallTime = this.CallDate + ts;
		//trip.CallTime = this.CallDate.Add(new TimeSpan(this.CallTime.Hour, this.CallTime.Minute, this.CallTime.Second));
		//trip.CallTime = new DateTime( CallDate.Year, CallDate.Month, CallDate.Day, CallDate.Hour, CallDate.Minute, CallDate.Second );
		Trip.CallTime = new DateTime( CallDate.Year, CallDate.Month, CallDate.Day, CallTime.Hour, CallTime.Minute, CallTime.Second );

		//Logging.WriteLogLine( "trip.CallTime: " + trip.CallTime );

		Trip.IsQuote            = IsQuote;
		Trip.UndeliverableNotes = UndeliverableNotes.Trim();

		Trip.PickupCompanyName = !string.IsNullOrEmpty( SelectedPickupAddress ) ? SelectedPickupAddress.Trim() : PickupName.Trim();

		Trip.PickupAddressBarcode = PickupLocation.Trim();

		Trip.PickupContact             = PickupName.Trim();
		Trip.PickupAddressPhone        = PickupPhone.Trim();
		Trip.PickupAddressSuite        = PickupSuite.Trim();
		Trip.PickupAddressAddressLine1 = PickupStreet.Trim();
		Trip.PickupAddressCity         = PickupCity.Trim();
		Trip.PickupAddressRegion       = PickupProvState.Trim();
		Trip.PickupAddressCountry      = PickupCountry?.Trim() ?? "";
		Trip.PickupAddressPostalCode   = PickupPostalZip.Trim();
		Trip.PickupZone                = PickupZone.FixZone();

		Trip.PickupAddressNotes = PickupAddressNotes.Trim();

		Trip.PickupNotes = PickupNotes.Trim();

		Trip.DeliveryCompanyName = !string.IsNullOrEmpty( SelectedDeliveryAddress ) ? SelectedDeliveryAddress.Trim() : DeliveryName.Trim();

		Trip.DeliveryAddressBarcode = DeliveryLocation.Trim();

		Trip.DeliveryContact             = DeliveryName.Trim();
		Trip.DeliveryAddressPhone        = DeliveryPhone.Trim();
		Trip.DeliveryAddressSuite        = DeliverySuite.Trim();
		Trip.DeliveryAddressAddressLine1 = DeliveryStreet.Trim();
		Trip.DeliveryAddressCity         = DeliveryCity.Trim();
		Trip.DeliveryAddressRegion       = DeliveryProvState.Trim();
		Trip.DeliveryAddressCountry      = DeliveryCountry?.Trim() ?? "";
		Trip.DeliveryAddressPostalCode   = DeliveryPostalZip.Trim();
		Trip.DeliveryZone                = DeliveryZone.FixZone();

		Trip.DeliveryAddressNotes = DeliveryAddressNotes.Trim();

		Trip.DeliveryNotes = DeliveryNotes.Trim();

		//trip.PackageType = SelectedPackageType;
		if( TripPackages.IsNotNull() && TripPackages.Any() )
			Trip.PackageType = TripPackages[ 0 ].PackageType;

		//trip.Weight = Weight;
		//trip.Pieces = Pieces;
		Trip.ServiceLevel = ServiceLevel.Trim();
		Trip.POP          = POP.Trim();
		Trip.POD          = POD.Trim();

		//ts = new TimeSpan(this.ReadyTime.Hour, this.ReadyTime.Minute, this.ReadyTime.Second);
		//trip.ReadyTime = this.ReadyDate + ts;
		//trip.ReadyTime = this.ReadyDate.Add(new TimeSpan(this.ReadyTime.Hour, this.ReadyTime.Minute, this.ReadyTime.Second));
		//trip.ReadyTime = new DateTime( ReadyDate.Year, ReadyDate.Month, ReadyDate.Day, ReadyDate.Hour, ReadyDate.Minute, ReadyDate.Second );
		Trip.ReadyTime = new DateTime( ReadyDate.Year, ReadyDate.Month, ReadyDate.Day, ReadyTime.Hour, ReadyTime.Minute, ReadyTime.Second );

		//Logging.WriteLogLine( "trip.ReadyTime: " + trip.ReadyTime );

		//ts = new TimeSpan(this.DueTime.Hour, this.DueTime.Minute, this.DueTime.Second);
		//trip.DueTime = this.ReadyTime + ts;
		//trip.DueTime = this.DueDate.Add(new TimeSpan(this.DueTime.Hour, this.DueTime.Minute, this.DueTime.Second));
		//trip.DueTime = new DateTime( DueDate.Year, DueDate.Month, DueDate.Day, DueDate.Hour, DueDate.Minute, DueDate.Second );
		Trip.DueTime = new DateTime( DueDate.Year, DueDate.Month, DueDate.Day, DueTime.Hour, DueTime.Minute, DueTime.Second );

		//Logging.WriteLogLine( "trip.DueTime: " + trip.DueTime );

		if( !string.IsNullOrEmpty( SelectedDriver ) )
		{
			Trip.Driver = SelectedDriver;

			if( CurrentTrip == null )
				Trip.Status1 = STATUS.DISPATCHED;
			else
			{
				if( CurrentTrip.Status1 > STATUS.ACTIVE )
					Trip.Status1 = CurrentTrip.Status1;
				else
				{
					Logging.WriteLogLine( "Trip has driver assigned but status is " + Trip.Status1 + "  - changing to Dispatched" );
					Trip.Status1 = STATUS.DISPATCHED;
				}
			}
		}
		else
		{
			if( ( CurrentTrip == null ) || ( CurrentTrip.Status1 < STATUS.ACTIVE ) )
				Trip.Status1 = STATUS.ACTIVE;
		}

		if( TripPackages.Count > 0 )
		{
			// Only include TripItems with pieces and weight
			var Valid = new List<TripPackage>();

			/*
			 * From Terry:
			 * Pieces, weight fields in trip hold the totals of the pieces
			    in the Packages.
			    There is a Package for each row.
			    The row's Pieces is the total number of inventory items, or the entered
			    value if there are no items.
			    Therefore the row's pieces can't be edited if there are items attached.
			 */

			//foreach( var tp in TripPackages )
			//for (int i = 1; i < TripPackages.Count; i++)
			for( var I = 0; I < TripPackages.Count; I++ )
			{
				var Tp = TripPackages[ I ];

				if( ( Tp.PackageType != string.Empty ) && ( Tp.Pieces > 0 ) && ( Tp.Weight > 0 ) )
				{
					//if( ( ti.ItemCode != string.Empty ) && ( ti.Pieces > 0 ) && ( ti.Weight > 0 ) )
					//	valid.Add( ti );

					if( TripItemsInventory.ContainsKey( I + 1 ) )
					{
						Tp.Items.Clear();
						var Items = TripItemsInventory[ I + 1 ];

						foreach( var Ei in Items )
						{
							var Ti = new TripItem
									 {
										 Description = Ei.Inventory.Description,
										 Barcode     = Ei.Inventory.Barcode,
										 Pieces      = Ei.Quantity,
										 ItemCode    = Ei.ServiceLevel
									 };

							Tp.Items.Add( Ti );
						}

						Valid.Add( Tp );
					}
					else
					{
						// No inventory - just packages
						Valid.Add( Tp );
					}

					//if( ( tp.Items != null ) && ( tp.Items.Count > 0 ) )
					//	valid.Add( tp );
				}
			}

			if( Valid.Count > 0 )
				Trip.Packages = Valid;

			// These fields are totals, according to Terry
			Trip.Weight             = TotalWeight;
			Trip.Pieces             = TotalPieces;
			Trip.OriginalPieceCount = TotalOriginal;
		}

        // Pricing information
		Trip.TripCharges = TripCharges;
        Trip.TotalAmount        = TotalTotal;
        Trip.TotalFixedAmount   = TotalBeforeTax;
        Trip.TotalPayrollAmount = TotalPayroll;
        Trip.TotalTaxAmount     = TotalTaxes;

		// Testing
		//Trip.Pallets = 1;

        return Trip;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="ctrl">
	/// </param>
	/// <param
	///     name="message">
	/// </param>
	/// <returns></returns>
	private static string SetErrorState( Control ctrl, string message )
	{
		var Error = string.Empty;

		if( IsControlEmpty( ctrl ) )
		{
			ctrl.Background = Brushes.Yellow;
			Error           = message;
		}
		else
			ctrl.Background = Brushes.White;

		return Error;
	}

	private static string SetInvalidAccountErrorState( Control ctrl, string message )
	{
		var Error = string.Empty;

		if( IsControlEmpty( ctrl ) )
		{
			ctrl.Background = Brushes.Yellow;
			Error           = message;
		}
		else
			ctrl.Background = Brushes.White;

		return Error;
	}

	private async Task<string> SetReferenceErrorState( Control ctrl, string message )
	{
		var Error = string.Empty;

		if( !IsInDesignMode )
		{
			//CustomerLookupSummary? cls = null;

			var ResellerCompany = await Azure.Client.RequestGetResellerCustomerCompany( SelectedAccount );

			//cls = await Azure.Client.RequestCustomerSummaryLookup(SelectedAccount);

			if( ResellerCompany != null )
			{
				if( ( ResellerCompany.EmailAddress2 == "RequireReferenceField" ) && IsControlEmpty( ctrl ) )
				{
					ctrl.Background = Brushes.Yellow;
					Error           = message;
				}
				else
					ctrl.Background = Brushes.White;
			}
		}

		return Error;
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="ctrl">
	/// </param>
	/// <returns></returns>
	private static bool IsControlEmpty( Control ctrl )
	{
		return ctrl switch
			   {
				   TextBox Tb         => IsTextBoxEmpty( Tb ),
				   ComboBox Cb        => IsComboBoxSelected( Cb ),
				   DateTimePicker Dtp => IsDateTimePickerEmpty( Dtp ),
				   TimePicker Tp      => IsTimePickerEmpty( Tp ),
				   _                  => true
			   };
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="tb">
	/// </param>
	/// <returns></returns>
	private static bool IsTextBoxEmpty( TextBox tb ) => string.IsNullOrEmpty( tb.Text );

	/// <summary>
	/// </summary>
	/// <param
	///     name="cb">
	/// </param>
	/// <returns></returns>
	private static bool IsComboBoxSelected( Selector cb ) => !( cb.SelectedIndex > -1 );

	/// <summary>
	/// </summary>
	/// <param
	///     name="dtp">
	/// </param>
	/// <returns></returns>
	private static bool IsDateTimePickerEmpty( InputBase dtp ) => string.IsNullOrEmpty( dtp.Text );

	private static bool IsTimePickerEmpty( InputBase tp ) => string.IsNullOrEmpty( tp.Text );

	public async Task<CompanyAddress?> GetCompanyAddress( string accountId )
	{
		CompanyAddress? Ca = null;

		if( !IsInDesignMode )
		{
			try
			{
				Ca = await Azure.Client.RequestGetResellerCustomerCompany( accountId );

				Dispatcher.Invoke( () =>
								   {
									   Customer = Ca.CompanyName;

									   //               if (ca.Phone.IsNotNullOrWhiteSpace())
									   //               {
									   //CustomerPhone = ca.Phone;
									   //               }
									   //PickupCountry = ca.Country;
									   //DeliveryCountry = ca.Country;
									   CompanyAddress = Ca;
								   } );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown looking up Customer for AccountId: " + accountId + " e->" + E );
			}
		}

		return Ca;
	}

	public async void Execute_FindTrip( string? tripId = null )
	{
		var TId = tripId ?? TripId;
		Logging.WriteLogLine( "Finding trip: " + TId );
		IsNewTrip = false;

		if( !string.IsNullOrEmpty( TId ) && !IsInDesignMode )
		{
			try
			{
				var Trps = await Azure.Client.RequestSearchTrips( new Protocol.Data.SearchTrips
															      {
																      TripId   = TId,
																      ByTripId = true
															      } );

				//Logging.WriteLogLine("DEBUG Trps.Count: " + Trps?.Count);
				if( Trps?.Count > 0 )
				{
					Dispatcher.Invoke( () =>
									   {
										   Boards.Common.ServiceLevel Sl = new()
																		   {
																			   RawServiceLevel = new ServiceLevel
																							     {
																								     OldName = Trps[ 0 ].ServiceLevel
																							     }
																		   };
										   CurrentTrip = new DisplayTrip( Trps[ 0 ], Sl );
										   //DisplayTrip trip = new DisplayTrip( Trps[ 0 ], Sl );

										   _ = WhenCurrentTripChanges();
									   } );
				}
			}
			catch
			{
				MessageBox.Show( "Error fetching trip", "Error", MessageBoxButton.OK );
			}
		}
	}

	public void Execute_SwapAddresses()
	{
		Logging.WriteLogLine( "Swapping addresses for trip: " + TripId );

		if( !string.IsNullOrEmpty( TripId ) )
		{
			Dispatcher.Invoke( () =>
							   {
								   var paddr      = SelectedPickupAddress;
								   var pn         = PickupName;
								   var pphn       = PickupPhone;
								   var ploc       = PickupLocationBarcode;
								   var psuite     = PickupSuite;
								   var pstreet    = PickupStreet;
								   var pcity      = PickupCity;
								   var pprov      = PickupProvState;
								   var pcountry   = PickupCountry;
								   var ppcode     = PickupPostalZip;
								   var pzone      = PickupZone;
								   var paddrnotes = PickupAddressNotes;
								   var pnotes     = PickupNotes;

								   var daddr      = SelectedDeliveryAddress;
								   var dn         = DeliveryName;
								   var dphn       = DeliveryPhone;
								   var dloc       = DeliveryLocationBarcode;
								   var dsuite     = DeliverySuite;
								   var dstreet    = DeliveryStreet;
								   var dcity      = DeliveryCity;
								   var dprov      = DeliveryProvState;
								   var dcountry   = DeliveryCountry;
								   var dpcode     = DeliveryPostalZip;
								   var dzone      = DeliveryZone;
								   var daddrnotes = DeliveryAddressNotes;
								   var dnotes     = DeliveryNotes;

								   // For some reason, these get left behind
								   if( View != null )
								   {
									   View.tbPickupAddressLocationBarcode.Text   = string.Empty;
									   View.tbDeliveryAddressLocationBarcode.Text = string.Empty;
								   }

								   ( paddr, daddr )           = ( daddr, paddr );
								   ( pn, dn )                 = ( dn, pn );
								   ( pphn, dphn )             = ( dphn, pphn );
								   ( ploc, dloc )             = ( dloc, ploc );
								   ( psuite, dsuite )         = ( dsuite, psuite );
								   ( pstreet, dstreet )       = ( dstreet, pstreet );
								   ( pcity, dcity )           = ( dcity, pcity );
								   ( pcountry, dcountry )     = ( dcountry, pcountry );
								   ( ppcode, dpcode )         = ( dpcode, ppcode );
								   ( pzone, dzone )           = ( dzone, pzone );
								   ( paddrnotes, daddrnotes ) = ( daddrnotes, paddrnotes );
								   ( pnotes, dnotes )         = ( dnotes, pnotes );

								   SelectedPickupAddress = paddr;
								   PickupName            = pn;
								   PickupPhone           = pphn;
								   PickupLocationBarcode = ploc;
								   PickupSuite           = psuite;
								   PickupStreet          = pstreet;
								   PickupCity            = pcity;
								   PickupCountry         = pcountry;
								   PickupPostalZip       = ppcode;
								   PickupZone            = pzone;
								   PickupAddressNotes    = paddrnotes;
								   PickupNotes           = pnotes;

								   SelectedDeliveryAddress = daddr;
								   DeliveryName            = dn;
								   DeliveryPhone           = dphn;
								   DeliveryLocationBarcode = dloc;
								   DeliverySuite           = dsuite;
								   DeliveryStreet          = dstreet;
								   DeliveryCity            = dcity;
								   DeliveryCountry         = dcountry;
								   DeliveryPostalZip       = dpcode;
								   DeliveryZone            = dzone;
								   DeliveryAddressNotes    = daddrnotes;
								   DeliveryNotes           = dnotes;
							   } );
		}
	}

	public void Execute_SwapAddresses_V1()
	{
		Logging.WriteLogLine( "Swapping addresses for trip: " + TripId );

		if( !string.IsNullOrEmpty( TripId ) )
		{
			Dispatcher.Invoke( () =>
							   {
								   // These get overridden by the selection_changed methods triggered by changing
								   // the selected pickup and delivery addresses
								   var pn = PickupName;
								   var dn = DeliveryName;
								   var pp = PickupPhone;
								   var dp = DeliveryPhone;

								   ( SelectedDeliveryAddress, SelectedPickupAddress ) = ( SelectedPickupAddress, SelectedDeliveryAddress );

								   if( !IsNewTrip )
								   {
									   View?.CbPickupAddresses_SelectionChanged( null, null );
									   View?.CbDeliveryAddresses_SelectionChanged( null, null );
									   ( DeliverySuite, PickupSuite )         = ( PickupSuite, DeliverySuite );
									   ( DeliveryStreet, PickupStreet )       = ( PickupStreet, DeliveryStreet );
									   ( DeliveryCity, PickupCity )           = ( PickupCity, DeliveryCity );
									   ( DeliveryCountry, PickupCountry )     = ( PickupCountry, DeliveryCountry );
									   ( DeliveryProvState, PickupProvState ) = ( PickupProvState, DeliveryProvState );
									   ( DeliveryPostalZip, PickupPostalZip ) = ( PickupPostalZip, DeliveryPostalZip );
								   }
								   DeliveryName                   = pn;
								   PickupName                     = dn;
								   DeliveryPhone                  = pp;
								   PickupPhone                    = dp;
								   ( DeliveryZone, PickupZone )   = ( PickupZone, DeliveryZone );
								   ( DeliveryNotes, PickupNotes ) = ( PickupNotes, DeliveryNotes );
							   } );
		}
	}

	public async Task Execute_CloneTrip()
	{
		if( CurrentTrip is null && PreviousTrip is not null )
		{
			//Logging.WriteLogLine( "Copying PreviousTrip into CurrentTrip for cloning" );
			CurrentTrip = PreviousTrip;
		}

		if( CurrentTrip is not null && !IsInDesignMode )
		{
			Logging.WriteLogLine( "Cloning trip: " + CurrentTrip.TripId );

			try
			{
				var NewTripId = await Azure.Client.RequestGetNextTripId();
				Logging.WriteLogLine( "Got new tripid: " + NewTripId );
				TripId = NewTripId;
				var NewTrip = CopyTripIntoAnother( CurrentTrip );

				//Execute_NewTrip(true);
				NewTrip.TripId = TripId;
				CurrentTrip    = NewTrip;

				//WhenCurrentTripChanges();
				//var now = DateTimeOffset.Now;
				var Now = DateTime.Now;
				CallDate = Now;

				//CallTime = now;

				Status1            = (int)STATUS.NEW;
				CurrentTrip.Status = DisplayTrip.STATUS.NEW;
				// CD1-T180
				DriverCode = NewTrip.Driver = NewTrip.DriverNotes = string.Empty;
				POP        = POD            = string.Empty;

				if( View != null )
				{
					View.imagePOP.Source = null;
					View.imagePOD.Source = null;

					View.cbDrivers.SelectedIndex = -1;
				}
				// End of CD1-T180

				//if (!onlyTripId)
				//{
				//    DateTimeOffset now = DateTimeOffset.Now;
				//    this.CallDate = now;
				//    this.CallTime = now;

				//    this.ReadyDate = now;
				//    this.ReadyTime = now;

				//    this.DueDate = now;
				//    this.DueTime = now;
				//}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + E );
			}
		}
		else
			Logging.WriteLogLine( "No trip loaded" );
	}

	public async Task Execute_CloneTripForPickup()
	{
		if( CurrentTrip is null && PreviousTrip is not null )
		{
			Logging.WriteLogLine( "Copying PreviousTrip into CurrentTrip for cloning" );
			CurrentTrip = PreviousTrip;
		}

		if( CurrentTrip is not null && !IsInDesignMode )
		{
			Logging.WriteLogLine( "Cloning trip for pickup: " + CurrentTrip.TripId );

			try
			{
				var NewTripId = await Azure.Client.RequestGetNextTripId();
				Logging.WriteLogLine( "Got new tripid: " + NewTripId );
				TripId = NewTripId;
				var NewTrip = CopyTripIntoAnother( CurrentTrip );
				NewTrip.TripId = TripId;

				NewTrip.DeliveryAddressAddressLine1 = string.Empty;
				NewTrip.DeliveryAddressAddressLine2 = string.Empty;
				NewTrip.DeliveryAddressCity         = string.Empty;
				NewTrip.DeliveryAddressCountry      = string.Empty;
				NewTrip.DeliveryAddressCountryCode  = string.Empty;
				NewTrip.DeliveryAddressNotes        = string.Empty;
				NewTrip.DeliveryAddressPostalCode   = string.Empty;
				NewTrip.DeliveryAddressRegion       = string.Empty;
				NewTrip.DeliveryAddressSuite        = string.Empty;
				NewTrip.DeliveryCompanyName         = string.Empty;
				NewTrip.DeliveryZone                = string.Empty;
				SelectedDeliveryAddress             = string.Empty;

				//Execute_NewTrip(true);
				CurrentTrip = NewTrip;
				POP         = "";
				POD         = "";
				DriverNotes = "";
				PickupNotes = "";

				TripCharges = new List<TripCharge>();

				// CD1-T180
				TripPackages = NewTrip.Packages = new ObservableCollection<DisplayPackage>();

				TripPackages.Add( new DisplayPackage( new TripPackage
												      {
													      Pieces = 1,
													      Weight = 1
												      } ) );
				ServiceLevel = NewTrip.ServiceLevel = GetServiceDefault();
				Reference    = NewTrip.Reference    = string.Empty;
				DriverCode   = NewTrip.Driver       = NewTrip.DriverNotes = string.Empty;

				if( View != null )
				{
					View.RemovePackageRows();
					View.BuildPackageRows( this );
					View.cbDrivers.SelectedIndex = -1;
				}
				// End of CD1-T180

				//var now = DateTimeOffset.Now;
				var Now = DateTime.Now;
				CallDate = Now;

				//CallTime = now;
				Status1            = (int)STATUS.NEW;
				CurrentTrip.Status = DisplayTrip.STATUS.NEW;
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + E );
			}
		}
		else
			Logging.WriteLogLine( "No trip loaded" );
	}

	public async void Execute_CloneTripForReturn()
	{
		if( !IsInDesignMode && ( CurrentTrip != null ) )
		{
			Logging.WriteLogLine( "Cloning trip for return: " + CurrentTrip.TripId );

			//Logging.WriteLogLine("DEBUG Before swap:  SelectedPickupAddress: " + SelectedPickupAddress + ", SelectedDeliveryAddress: " + SelectedDeliveryAddress);
			Execute_SwapAddresses();
			//Logging.WriteLogLine("DEBUG After swap:  SelectedPickupAddress: " + SelectedPickupAddress + ", SelectedDeliveryAddress: " + SelectedDeliveryAddress);

			var NewTripId = await Azure.Client.RequestGetNextTripId();
			Logging.WriteLogLine( "Got new tripid: " + NewTripId );
			TripId = NewTripId;
			var Now               = DateTime.Now;
			CallDate  = CallTime  = Now;
			ReadyDate = ReadyTime = Now;
			DueDate   = DueTime   = Now;
			Status1   = (int)STATUS.NEW;

			// CD1-T180
			POP = string.Empty;
			POD = string.Empty;
			CurrentTrip.Signatures.Clear();
			TripPackages = CurrentTrip.Packages = new ObservableCollection<DisplayPackage>();

			TripPackages.Add( new DisplayPackage( new TripPackage
											      {
												      Pieces = 1,
												      Weight = 1
											      } ) );
			ServiceLevel = CurrentTrip.ServiceLevel = GetServiceDefault();
			Reference    = CurrentTrip.Reference    = string.Empty;
			DriverCode   = CurrentTrip.Driver       = CurrentTrip.DriverNotes = string.Empty;

			if( View != null )
			{
				View.RemovePackageRows();
				View.BuildPackageRows( this );
				View.cbDrivers.SelectedIndex = -1;
				View.imagePOP.Source         = null;
				View.imagePOD.Source         = null;
			}
			// End of CD1-T180
		}
	}

	public async Task Execute_CloneTripForReturn_V1()
	{
		if( CurrentTrip is null && PreviousTrip is not null )
		{
			Logging.WriteLogLine( "Copying PreviousTrip into CurrentTrip for cloning" );
			CurrentTrip = PreviousTrip;
		}

		if( CurrentTrip is not null && !IsInDesignMode )
		{
			Logging.WriteLogLine( "Cloning trip for return: " + CurrentTrip.TripId );

			try
			{
				var NewTripId = await Azure.Client.RequestGetNextTripId();
				Logging.WriteLogLine( "Got new tripid: " + NewTripId );
				TripId = NewTripId;
				//var NewTrip = CopyTripIntoAnother( CurrentTrip );
				//NewTrip.TripId = TripId;

				//Execute_NewTrip(true);
				//CurrentTrip = NewTrip;
				Logging.WriteLogLine( "DEBUG Before swap: PickupCompanyName: " + CurrentTrip.PickupCompanyName + ", DeliveryCompanyName: " + CurrentTrip.DeliveryCompanyName );

				//var now = DateTimeOffset.Now;
				var Now = DateTime.Now;
				CallDate = Now;

				//CallTime = now;
				//Execute_SwapAddresses_V1();
				Execute_SwapAddresses();
				var trip = BuildTrip();
				Logging.WriteLogLine( "DEBUG After BuildTrip:  PickupCompanyName: " + trip.PickupCompanyName + ", DeliveryCompanyName: " + trip.DeliveryCompanyName );

				var sl = new Boards.Common.ServiceLevel
						 {
							 RawServiceLevel = new ServiceLevel
											   {
												   OldName = CurrentTrip.ServiceLevel
											   }
						 };

				CurrentTrip = new DisplayTrip( trip, sl )
							  {
								  TripId   = TripId,
								  CallTime = CallDate,
								  Status   = DisplayTrip.STATUS.NEW
							  };
				Logging.WriteLogLine( "DEBUG After swap:  PickupCompanyName: " + CurrentTrip.PickupCompanyName + ", DeliveryCompanyName: " + CurrentTrip.DeliveryCompanyName );

				await WhenCurrentTripChanges();

				//Status1            = (int)STATUS.NEW;
				//CurrentTrip.Status = DisplayTrip.STATUS.NEW;
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + E );
			}
		}
		else
			Logging.WriteLogLine( "No trip loaded" );
	}

	/// <summary>
	///     Note: it drops the TripId and Driver.
	/// </summary>
	/// <param
	///     name="src">
	/// </param>
	/// <returns></returns>
	private DisplayTrip CopyTripIntoAnother( DisplayTrip src )
	{
		//result = new DisplayTrip( new Trip() );
		DisplayTrip Result = new()
							 {
								 Status1   = src.Status1,
								 Driver    = src.Driver,
								 AccountId = src.AccountId
							 };

		//result.TPU = src.PickupTime;
		//result.TDEL = src.DeliveryTime;

		//result.TripId = src.TripId;
		Result.AccountId = Account;
		Result.AccountId = src.AccountId;
		Result.Reference = src.Reference;

		Result.CallerName  = src.CallerName;
		Result.CallerPhone = src.CallerPhone;
		Result.CallerEmail = src.CallerEmail;

		Result.IsQuote            = src.IsQuote;
		Result.UndeliverableNotes = src.UndeliverableNotes;

		// Pickup
		Result.PickupCompanyName = src.PickupCompanyName;

		//GetAddresses();  // Only need to do once
		//SelectedPickupAddress = src.PickupCompanyName;

		//PickupName = string.Empty; // TODO Fix this
		Result.PickupAddressPhone = src.PickupAddressPhone;
		Result.PickupAddressSuite = src.PickupAddressSuite;

		//PickupStreet = (src.PickupAddressAddressLine1 + " " + src.PickupAddressAddressLine2).Trim();
		Result.PickupAddressAddressLine1 = src.PickupAddressAddressLine1; // TODO Fix PickupAddressAddressLine2 returns PickupAddressAddressLine1
		Result.PickupAddressCity         = src.PickupAddressCity;
		Result.PickupAddressRegion       = src.PickupAddressRegion;
		Result.PickupAddressCountry      = src.PickupAddressCountry;
		Result.PickupAddressPostalCode   = src.PickupAddressPostalCode;
		Result.PickupZone                = src.PickupZone;
		Result.PickupAddressNotes        = src.PickupAddressNotes;
		Result.PickupNotes               = src.PickupNotes;

		// Delivery
		Result.DeliveryCompanyName = src.DeliveryCompanyName;

		//SelectedDeliveryAddress = src.DeliveryCompanyName;
		//DeliveryName = string.Empty; // TODO Fix this
		Result.DeliveryAddressPhone = src.DeliveryAddressPhone;
		Result.DeliveryAddressSuite = src.DeliveryAddressSuite;

		//DeliveryStreet = (src.DeliveryAddressAddressLine1 + " " + src.DeliveryAddressAddressLine2).Trim();
		Result.DeliveryAddressAddressLine1 = src.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
		Result.DeliveryAddressCity         = src.DeliveryAddressCity;
		Result.DeliveryAddressRegion       = src.DeliveryAddressRegion;
		Result.DeliveryAddressCountry      = src.DeliveryAddressCountry;
		Result.DeliveryAddressPostalCode   = src.DeliveryAddressPostalCode;
		Result.DeliveryZone                = src.DeliveryZone;
		Result.DeliveryAddressNotes        = src.DeliveryAddressNotes;
		DeliveryNotes                      = src.DeliveryNotes;

		// Package
		Result.PackageType = src.PackageType;
		Result.Weight      = src.Weight;
		Result.Pieces      = (int)src.Pieces;

		//Length = 0;   // TODO Fix this
		//Width = 0;    // TODO Fix this
		//Height = 0;   // TODO Fix this
		Result.OriginalPieceCount = (int)src.OriginalPieceCount;

		//QH = 0;       // TODO Fix this

		// Service
		Result.ServiceLevel = src.ServiceLevel;

		//result.ReadyDate = src.ReadyTime;
		Result.ReadyTime = src.ReadyTime;

		//result.DueDate = src.DueTime;
		Result.DueTime = src.DueTime;

		//result.POP = src.POP;
		//result.POD = src.POD;

		//SelectedPackageType = src.PackageType;

		//SelectedDriver = src.Driver;

		Result.Packages = src.Packages;

		return Result;
	}

	/// <summary>
	///     Note: row is the index into TripPackages.
	/// </summary>
	/// <param
	///     name="row">
	/// </param>
	/// <returns></returns>
	public decimal GetRateForTripPackage( int row )
	{
		decimal     result = 0;
		TripPackage tp     = TripPackages[ row ];

		if( tp != null )
		{
			if( PickupZone.IsNotNullOrWhiteSpace() && DeliveryZone.IsNotNullOrWhiteSpace() )
			{
				//Logging.WriteLogLine( "DEBUG Getting rate for zones PZ: " + PickupZone + " DZ: " + DeliveryZone + " PackageType: " + tp.PackageType + " ServiceLevel: " + ServiceLevel );

				var pt = ( from P in PackageTypeObjects
						   where P.Description == tp.PackageType
						   select P ).FirstOrDefault();

				var sl = ( from S in CompleteServiceLevels
						   where S.OldName == ServiceLevel
						   select S ).FirstOrDefault();

				var fz = ( from Z in ZoneObjects
						   where Z.Name == PickupZone
						   select Z ).FirstOrDefault();

				var tz = ( from Z in ZoneObjects
						   where Z.Name == DeliveryZone
						   select Z ).FirstOrDefault();

				if( ( pt != null ) && ( sl != null ) && ( fz != null ) && ( tz != null ) )
				{
					Logging.WriteLogLine( "DEBUG fz: " + fz.ZoneId + " tz: " + tz.ZoneId + " pt: " + pt.PackageTypeId + " sl: " + sl.ServiceLevelId );

					var rmi = ( from R in RateMatrixItems
							    where ( R.FromZoneId == fz.ZoneId ) && ( R.ToZoneId == tz.ZoneId )
															        && ( R.PackageTypeId == pt.PackageTypeId ) && ( R.ServiceLevelId == sl.ServiceLevelId )
							    select R ).FirstOrDefault();

					// Try the other way
					rmi ??= ( from R in RateMatrixItems
							  where ( R.FromZoneId == tz.ZoneId ) && ( R.ToZoneId == fz.ZoneId )
															      && ( R.PackageTypeId == pt.PackageTypeId ) && ( R.ServiceLevelId == sl.ServiceLevelId )
							  select R ).FirstOrDefault();

					if( rmi != null )
					{
						if( rmi.Formula.IsNullOrWhiteSpace() )
						{
							// Fixed rate
							result = rmi.Value;
						}
						else
						{
							//Logging.WriteLogLine("DEBUG Formula: " + rmi.Formula);

							// Get lane from rmi
							var lane        = (int)rmi.Value;
							var laneFormula = EvaluateRateFormulaHelper.GetFormulaForLane( rmi.Formula, lane );

							if( laneFormula.IsNullOrWhiteSpace() )
							{
								// Try full formula - Might happen if the 'Lane =' is missing
								Logging.WriteLogLine( "Couldn't find a formula for lane " + lane + " in formula " + rmi.Formula );
								laneFormula = rmi.Formula;
							}

							//result = EvaluateRateFormulaHelper.EvaluateFullFormula( rmi.Formula, tp.Pieces, tp.Weight, 0, false );
							result = EvaluateRateFormulaHelper.EvaluateFullFormula( laneFormula, tp.Pieces, tp.Weight, 0, false );
						}
					}
				}
			}
			//else
			//{
			//    Logging.WriteLogLine("DEBUG zones are empty");
			//}
		}
		else
			Logging.WriteLogLine( "ERROR Can't find a TripPackage at row " + row + " There are " + TripPackages.Count + " items" );

		return result;
	}

	public string GetZoneForPostalCode( string postalZip )
	{
		var zone = string.Empty;

		if( !IsPml )
		{
			var found = ( from R in RoutesGeneric
						  where R.PostalCode.ToLower() == postalZip.Trim().ToLower()
						  select R ).FirstOrDefault();

			if( found != null )
				zone = found.Zone1;
		}
		else
		{
			var found = ( from R in RoutesPml
						  where R.PostalCode.ToLower() == postalZip.Trim().ToLower()
						  select R ).FirstOrDefault();

			if( found != null )
				zone = found.Operation + "+" + found.Region;
		}
		Logging.WriteLogLine( "Found zone " + zone + " for postal code " + postalZip );

		return zone;
	}

	public enum ChargeDisplayType
	{
		Nothing          = 0,
		CheckboxOnly     = 1,
		FieldOnly        = 2,
		CheckboxAndField = 3,
		Total            = 4
	}

	public enum ChargeChargeType
	{
		CumulativeAddition = 0,
		Percentage         = 1,
		Reference          = 2,
		Formula            = 3
	}

	public enum Slots
	{
		Taxes          = 0,
		FuelSurcharges = 1,
		Payroll        = 2,
		Others         = 3
	}

    public Dictionary<string, string> CheckForDefaultCharges( Dictionary<string, string> activeCharges )
	{
        Dictionary<string, string> result = new();
		if (DefaultCharges?.Count > 0)
		{
			foreach (string chargeId in activeCharges.Keys )
			{
				string dcId = PriceTripHelper.MakeDefaultChargeId(SelectedAccount, chargeId);
				Charge found = (from C in DefaultCharges where C.ChargeId == dcId  select C).FirstOrDefault();
				if (found != null)
				{
					Logging.WriteLogLine($"DEBUG Found default charge {dcId} for {chargeId}");

					// TODO ?
					result.Add(found.ChargeId, activeCharges[chargeId]);
				}
				else
				{
					result.Add( chargeId, activeCharges[ chargeId ] );
				}
			}
		}
		else
		{
			result = activeCharges;
		}

		return result;
	}

	public Dictionary<string, string> CheckForOverrides( Dictionary<string, string> activeCharges )
	{
		Dictionary<string, string> result = new();

		string packageMarker       = FindStringResource( "ChargesWizardOverridesTabLabelPackageMarker" );
		string serviceMarker       = FindStringResource( "ChargesWizardOverridesTabLabelServiceMarker" );

		foreach( var chargeId in activeCharges.Keys )
		{
			var cid = chargeId + "_override";

			var ors = ( from C in Charges
					    where C.IsOverride && C.ChargeId.StartsWith( cid )
					    select C ).ToList();

			if( ( ors != null ) && ( ors.Count > 0 ) )
			{
				var found = false;

				foreach( var or in ors )
				{
					//Logging.WriteLogLine( "DEBUG found override for chargeId: " + chargeId + " override.ChargeId: " + or.ChargeId );
					// Check the conditions - package or service
					// NOTE Using the label because the override object doesn't support service level
					// The label is updated whenever an override is changed
					var isPackageOverride = or.Label.Contains( packageMarker );
					var isServiceOverride = or.Label.Contains( serviceMarker );
					
					if( isPackageOverride )
					{
						string[] pieces = Regex.Split( or.Label, packageMarker );

						if( pieces.Length == 2 )
						{
							var package = pieces[ 1 ].Trim();

							TripPackage tp = ( from T in TripPackages
											   where T.PackageType == package
											   select T ).FirstOrDefault();

							if( tp != null )
							{
								// Found a match
								Logging.WriteLogLine( "Override matches PackageType: " + package );

								if( !result.ContainsKey( or.ChargeId ) )
								{
									if( !result.ContainsKey( or.ChargeId ) )
										result.Add( chargeId, string.Empty );
									result[ chargeId ] = or.Value.ToString( "F2" );
									found              = true;
								}
							}
						}
					}
					else if( isServiceOverride )
					{
						string[] pieces = Regex.Split( or.Label, serviceMarker );

						if( pieces.Length == 2 )
						{
							var service = pieces[ 1 ].Trim();

							if( ServiceLevel == service )
							{
								// Found a match
								Logging.WriteLogLine( "Override matches ServiceLevel: " + service );

								if( !result.ContainsKey( chargeId ) )
									result.Add( chargeId, string.Empty );
								result[ chargeId ] = or.Value.ToString( "F2" );
								found              = true;
							}
						}
					}
					else
					{
						// Override doesn't match
						result.Add( chargeId, activeCharges[ chargeId ] );
					}
				}

				if( !found )
					result.Add( chargeId, activeCharges[ chargeId ] );
			}
			else
			{
				// No override
				result.Add( chargeId, activeCharges[ chargeId ] );
			}
		}

		return result;
	}

	public Dictionary<string, string> CheckForOverrides_V1( Dictionary<string, string> activeCharges )
	{
		Dictionary<string, string> result = new();

		var packageMarker = FindStringResource( "ChargesWizardOverridesTabLabelPackageMarker" );
		var serviceMarker = FindStringResource( "ChargesWizardOverridesTabLabelServiceMarker" );

		foreach( var chargeId in activeCharges.Keys )
		{
			//result.Add(chargeId, activeCharges[chargeId]);

			var cid = chargeId + "_override";

			//Charge or = (from C in Charges where C.IsOverride && C.ChargeId == cid select C).FirstOrDefault();
			var or = ( from C in Charges
					   where C.IsOverride && C.ChargeId.StartsWith( cid )
					   select C ).FirstOrDefault();

			if( or != null )
			{
				Logging.WriteLogLine( "DEBUG found override for chargeId: " + chargeId + " override.ChargeId: " + or.ChargeId );
				// Check the conditions - package or service
				var isPackageOverride = or.Label.Contains( packageMarker );
				var isServiceOverride = or.Label.Contains( serviceMarker );

				if( isPackageOverride )
				{
					string[] pieces = Regex.Split( or.Label, packageMarker );

					if( pieces.Length == 2 )
					{
						var package = pieces[ 1 ].Trim();

						TripPackage tp = ( from T in TripPackages
										   where T.PackageType == package
										   select T ).FirstOrDefault();

						if( tp != null )
						{
							// Found a match
							Logging.WriteLogLine( "DEBUG override matches PackageType: " + package );

							if( !result.ContainsKey( or.ChargeId ) )
							{
								//result.Add(or.ChargeId, activeCharges[or.ChargeId]);
								//result.Add(or.ChargeId, activeCharges[chargeId]);
								result.Add( or.ChargeId, or.Value.ToString( "F2" ) );
							}
						}
						else
						{
							// No match
							result.Add( chargeId, activeCharges[ chargeId ] );
						}
					}
				}
				else if( isServiceOverride )
				{
					string[] pieces = Regex.Split( or.Label, serviceMarker );

					if( pieces.Length == 2 )
					{
						var service = pieces[ 1 ].Trim();

						if( ServiceLevel == service )
						{
							// Found a match
							Logging.WriteLogLine( "DEBUG override matches ServiceLevel: " + service );
							//result.Add(or.ChargeId, activeCharges[or.ChargeId]);
							//result.Add(or.ChargeId, activeCharges[chargeId]);
							result.Add( or.ChargeId, or.Value.ToString( "F2" ) );
						}
						else
						{
							// No match
							result.Add( chargeId, activeCharges[ chargeId ] );
						}
					}
				}
				else
				{
					// Override doesn't match
					result.Add( chargeId, activeCharges[ chargeId ] );
				}
			}
			else
			{
				// No override
				result.Add( chargeId, activeCharges[ chargeId ] );
			}
		}

		return result;
	}

	public void RemoveCheckedDefaults()
	{
		var doSave = false;

		void RemoveDefault( string name )
		{
			if( ( Defaults != null ) && Defaults.ContainsKey( name ) )
			{
				Logging.WriteLogLine( "Removing default: " + name + " with value: " + Defaults[ name ] );
				Defaults.Remove( name );
			}
		}

		if( IsPickupAddressDefaultChecked )
		{
			var name = DEFAULT_PREFIX_PICKUP + SelectedAccount;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsDeliveryAddressDefaultChecked )
		{
			var name = DEFAULT_PREFIX_DELIVERY + SelectedAccount;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsPackageTypeDefaultChecked )
		{
			var name = DEFAULT_PREFIX_PACKAGE_TYPE;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsWeightDefaultChecked )
		{
			var name = DEFAULT_PREFIX_WEIGHT;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsPiecesDefaultChecked )
		{
			var name = DEFAULT_PREFIX_PIECES;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsServiceLevelDefaultChecked )
		{
			var name = DEFAULT_PREFIX_SERVICE_LEVEL;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsReadyTimeDefaultChecked )
		{
			var name = DEFAULT_PREFIX_READY_TIME;
			RemoveDefault( name );
			doSave = true;
		}

		if( IsDueByTimeDefaultChecked )
		{
			var name = DEFAULT_PREFIX_DUE_BY_TIME;
			RemoveDefault( name );
			doSave = true;
		}

		if( doSave )
		{
			Task.Run( async () =>
					  {
						  Logging.WriteLogLine( "Saving defaults" );
						  await SaveDefaultsPreference( false );
					  } );
		}
	}

	public async void EnsureResellerAccountExistsForGlobalAddressBook()
	{
		if( GlobalAddressBookId != null )
		{
			if( GlobalAddressBookId.Contains( "/" ) )
				GlobalAddressBookId = GlobalAddressBookId.Substring( GlobalAddressBookId.IndexOf( "/" ) + 1 );
			var             found         = false;
			CompanyAddress? globalCompany = null;

			globalCompany = await Azure.Client.RequestGetResellerCustomerCompany( GlobalAddressBookId );

			//globalCompany = Azure.Client.RequestGetResellerCustomerCompany( GlobalAddressBookId ).Result;

			if( ( globalCompany != null ) && globalCompany.CompanyName.IsNotNullOrWhiteSpace() )
				found = true;
			//foreach (var c in CompanyNames)
			//         {
			//	if ((string)c == GlobalAddressBookId)
			//             {
			//		//Logging.WriteLogLine("DEBUG Found GlobalAddressBookId " + GlobalAddressBookId);
			//		found = true;
			//		break;
			//	}
			//}

			if( !found )
			{
				Logging.WriteLogLine( "Adding company " + GlobalAddressBookId + " for reseller to attach the global addresses to" );
				PrimaryCompany PrimaryCompany;
				var            BillingCompany  = new Company();
				var            ShippingCompany = new Company();
				CompanyAddress ResellerCompany;

				// Only update if NOT editing - new Account
				PrimaryCompany = new PrimaryCompany
								 {
									 Company =
									 {
										 CompanyNumber = GlobalAddressBookId + "_PrimaryCompany"
									 }
								 };

				ResellerCompany = new CompanyAddress
								  {
									  CompanyName   = GlobalAddressBookId.Trim(),
									  CompanyNumber = GlobalAddressBookId.Trim()
								  };

			#region Primary Company
				var Company   = PrimaryCompany.Company;
				var AccountId = GlobalAddressBookId.Trim();
				var PWord     = GlobalAddressBookId;

				//company.CompanyName = SelectedAccountName.Trim();
				ResellerCompany.CompanyNumber = Company.CompanyNumber = AccountId;
				ResellerCompany.CompanyName   = Company.CompanyName   = GlobalAddressBookId;
				Company.UserName              = GlobalAddressBookId;
				Company.Password              = PWord;
				Company.ContactName           = string.Empty;

				Company.Phone        = string.Empty;
				Company.Suite        = BillingCompany.Suite        = string.Empty;
				Company.AddressLine1 = BillingCompany.AddressLine1 = string.Empty;
				Company.City         = BillingCompany.City         = string.Empty;
				Company.Region       = BillingCompany.Region       = string.Empty;
				Company.PostalCode   = BillingCompany.PostalCode   = string.Empty;
				Company.Country      = BillingCompany.Country      = string.Empty;
				Company.Notes        = string.Empty;
				Company.ZoneName     = string.Empty;

				ResellerCompany.Phone = Company.Phone = string.Empty;
				Company.Notes         = string.Empty;

				Company.Enabled                = true;
				ResellerCompany.Notes          = string.Empty;
				ResellerCompany.EmailAddress2  = Company.EmailAddress2 = string.Empty;
				PrimaryCompany.BillingCompany  = BillingCompany;
				PrimaryCompany.ShippingCompany = ShippingCompany;
				PrimaryCompany.CustomerCode    = GlobalAddressBookId;
				PrimaryCompany.LoginCode       = GlobalAddressBookId;
				PrimaryCompany.UserName        = GlobalAddressBookId;
				PrimaryCompany.Password        = PWord;
			#endregion

			#region Billing Company
				BillingCompany.CompanyNumber = AccountId;
				BillingCompany.CompanyName   = GlobalAddressBookId + "_Billing";
				BillingCompany.UserName      = GlobalAddressBookId;
				BillingCompany.Password      = PWord;

				BillingCompany.Phone         = string.Empty;
				ResellerCompany.Suite        = BillingCompany.Suite        = string.Empty;
				ResellerCompany.AddressLine1 = BillingCompany.AddressLine1 = string.Empty;
				ResellerCompany.City         = BillingCompany.City         = string.Empty;
				ResellerCompany.Region       = BillingCompany.Region       = string.Empty;
				ResellerCompany.PostalCode   = BillingCompany.PostalCode   = string.Empty;
				ResellerCompany.Country      = BillingCompany.Country      = string.Empty;
				BillingCompany.Notes         = string.Empty;
				BillingCompany.ZoneName      = string.Empty;
			#endregion

			#region Shipping Address
				ShippingCompany.CompanyNumber = AccountId;
				ShippingCompany.CompanyName   = GlobalAddressBookId + "_Shipping";
				ShippingCompany.UserName      = GlobalAddressBookId;
				ShippingCompany.Password      = PWord;

				ShippingCompany.Phone        = string.Empty;
				ShippingCompany.Suite        = string.Empty;
				ShippingCompany.AddressLine1 = string.Empty;
				ShippingCompany.City         = string.Empty;
				ShippingCompany.Region       = string.Empty;
				ShippingCompany.PostalCode   = string.Empty;
				ShippingCompany.Country      = string.Empty;
				ShippingCompany.Notes        = string.Empty;
				ShippingCompany.ZoneName     = string.Empty;
			#endregion

				var UpdatePrimaryCompany = new AddUpdatePrimaryCompany( "TripEntryModel" )
										   {
											   Password           = Encryption.ToTimeLimitedToken( PWord ),
											   Company            = Company,
											   BillingCompany     = BillingCompany,
											   ShippingCompany    = ShippingCompany,
											   UserName           = GlobalAddressBookId,
											   CustomerCode       = BillingCompany.CompanyNumber.Trim(),
											   SuggestedLoginCode = ""
										   };

				var RCompany = BuildCompany( ResellerCompany );

				var Acc = new AddCustomerCompany( "TripEntryModel" )
						  {
							  Company      = RCompany,
							  CustomerCode = GlobalAddressBookId
						  };

				await Task.Run( async () =>
							    {
								    var Tmp = await Azure.Client.RequestAddUpdatePrimaryCompany( UpdatePrimaryCompany );
								    //Logging.WriteLogLine("Response: " + Tmp);

								    await Azure.Client.RequestAddCustomerCompany( Acc );

								    //UpdateTripEntryPagesWithNewAccounts();
							    } );
			}
		}
	}

	private void EnsurePredatesUserExistsIfEnabled()
	{
		var PredateDriver = string.Empty;

		if( Prefs?.Count > 0 )
		{
			var Pref = ( from P in Prefs
					     where P.Description.Contains( "Predates" )
					     select P ).FirstOrDefault();

			if( Pref is {Enabled: true} )
			{
				Pref = ( from P in Prefs
					     where P.Description.Contains( "Predates Driver" )
					     select P ).FirstOrDefault();

				if( ( Pref is not null ) && Pref.StringValue.IsNotNullOrWhiteSpace() )
				{
					PredateDriver = Pref.StringValue;

					// Now check for the driver
					var Found = false;

					if( Drivers?.Count > 0 )
					{
						// ReSharper disable once LoopCanBeConvertedToQuery
						foreach( string Driver in Drivers )
						{
							if( Driver == PredateDriver )
							{
								Found = true;
								break;
							}
						}

						if( !Found )
						{
							Protocol.Data.Staff Sd = new()
													 {
														 StaffId   = PredateDriver,
														 FirstName = "DO NOT DELETE",
														 LastName  = "DO NOT DELETE",
														 Password  = Encryption.ToTimeLimitedToken( PredateDriver ),

														 Enabled = true
													 };

							UpdateStaffMemberWithRolesAndZones UpdateData = new( "ZoneWizard", Sd )
																			{
																				Roles = new List<Role>()
																			};

							Task.WaitAll(
										 Task.Run( async () =>
												   {
													   var RolesResponse = await Azure.Client.RequestRoles();

													   if( RolesResponse != null )
													   {
														   foreach( var Role in RolesResponse )
														   {
															   if( Role.IsDriver )
																   UpdateData.Roles.Add( Role );
														   }
													   }

													   await Azure.Client.RequestUpdateStaffMember( UpdateData );
												   } )
										);
						}
					}
				}
			}
		}
	}

	private static Company BuildCompany( CompanyBase ca ) =>

		// Used by Execute_Delete
		new()
		{
			Phone1          = ca.Phone,
			LocationBarcode = ca.LocationBarcode,
			Suite           = ca.Suite,
			AddressLine1    = ca.AddressLine1,
			City            = ca.City,
			Country         = ca.Country,
			CompanyName     = ca.CompanyName,
			PostalCode      = ca.PostalCode,
			Region          = ca.Region,

			Phone         = ca.Phone,
			EmailAddress  = ca.EmailAddress,
			EmailAddress2 = ca.EmailAddress2,
			Notes         = ca.Notes
		};
#endregion

#region Geolocation methods
	public string BingMapApiKey
	{
		get { return Get( () => BingMapApiKey, "" ); }
		set { Set( () => BingMapApiKey, value ); }
	}

	public async Task GetBingMapApiKey()
	{
		//Logging.WriteLogLine("DEBUG Getting map api keys");
		string key;
		var    keys = await Azure.Client.RequestGetMapsApiKeys();

		if( keys != null )
		{
			key = keys.BingMapsKey;
			//Logging.WriteLogLine("DEBUG BingMapsKey: " + key);
			BingMapApiKey = key;
		}
	}


	public (decimal latitude, decimal longitude) GeolocateAddress( string addressLine1, string city, string region, string country, string postalCode )
	{
		var addressQuery = BuildAddressQuery( country, region, city, postalCode, addressLine1 );
		//Logging.WriteLogLine("DEBUG addressQuery: " + addressQuery);
		var geocodeRequest  = "http://dev.virtualearth.net/REST/v1/Locations?" + addressQuery + "&o=xml&key=" + BingMapApiKey;
		var geocodeResponse = GetXmlResponse( geocodeRequest );

		var (la, lo) = GetPointD( geocodeResponse );
		var latitude  = decimal.Parse( la );
		var longitude = decimal.Parse( lo );
		//Logging.WriteLogLine("DEBUG latitude: " + latitude + ", longitude: " + longitude);

		return ( latitude, longitude );
	}

	private static string BuildAddressQuery( string country, string region, string city, string postalCode, string addressLine )
	{
		// "countryRegion=AU&adminDistrict=Vic&locality=Kyabram";
		var query = "countryRegion=@1&adminDistrict=@2&locality=@3&postalCode=@4&addressLine=@5";

		if( country.IsNotNullOrWhiteSpace() )
			query = query.Replace( "@1", country );
		else
			query = query.Replace( "@1", "" );

		if( region.IsNotNullOrWhiteSpace() )
			query = query.Replace( "@2", region );
		else
			query = query.Replace( "@2", "" );

		if( city.IsNotNullOrWhiteSpace() )
			query = query.Replace( "@3", city );
		else
			query = query.Replace( "@3", "" );

		if( postalCode.IsNotNullOrWhiteSpace() )
			query = query.Replace( "@4", postalCode );
		else
			query = query.Replace( "@4", "" );

		if( addressLine.IsNotNullOrWhiteSpace() )
			query = query.Replace( "@5", addressLine );
		else
			query = query.Replace( "@5", "" );

		return query;
	}

	/// <summary>
	///     Submit a REST Services or Spatial Data Services request and return the response
	/// </summary>
	/// <param
	///     name="requestUrl">
	/// </param>
	/// <returns></returns>
	private XmlDocument GetXmlResponse( string requestUrl )
	{
		XmlDocument XmlDoc = new();
		//System.Diagnostics.Trace.WriteLine("Request URL (XML): " + requestUrl);

		if( WebRequest.Create( requestUrl ) is HttpWebRequest Request )
		{
			using var Response = Request.GetResponse() as HttpWebResponse;

			if( ( Response != null ) && ( Response.StatusCode != HttpStatusCode.OK ) )
				throw new Exception( $"Server error (HTTP {Response.StatusCode}: {Response.StatusDescription})." );

			var Stream = Response?.GetResponseStream();

			if( Stream != null )
				XmlDoc.Load( Stream );
		}
		return XmlDoc;
	}

	/// <summary>
	///     Grabs the values directly from the document.
	/// </summary>
	/// <param
	///     name="xmlDoc">
	/// </param>
	/// <returns></returns>
	private (string latitude, string longitude) GetPointD( XmlDocument xmlDoc )
	{
		var latitude  = "0.0";
		var longitude = "0.0";

		//Create namespace manager
		var nsmgr = new XmlNamespaceManager( xmlDoc.NameTable );
		nsmgr.AddNamespace( "rest", "http://schemas.microsoft.com/search/local/ws/rest/v1" );

		//Get all locations in the response and then extract the coordinates for the top location
		var locationElements = xmlDoc.SelectNodes( "//rest:Point", nsmgr );

		if( locationElements is not null && ( locationElements.Count > 0 ) )
		{
			latitude  = locationElements[ 0 ].SelectSingleNode( ".//rest:Latitude", nsmgr )?.InnerText ?? "0";
			longitude = locationElements[ 0 ].SelectSingleNode( ".//rest:Longitude", nsmgr )?.InnerText ?? "0";
		}
		else
			Logging.WriteLogLine( "Error: no location found" );

		return ( latitude, longitude );
	}
#endregion

#region Inventory
	public ObservableCollection<PmlInventory> Inventory
	{
		get { return Get( () => Inventory, new ObservableCollection<PmlInventory>() ); }
		set { Set( () => Inventory, value ); }
	}


	public ObservableCollection<string> InventoryNames
	{
		get { return Get( () => InventoryNames, new ObservableCollection<string>() ); }
		set { Set( () => InventoryNames, value ); }
	}

	public ICollection<PmlInventory>? SelectedInventory
	{
		get { return Get( () => SelectedInventory, new Collection<PmlInventory>() ); }
		set { Set( () => SelectedInventory, value ); }
	}


	public string SelectedInventoryName
	{
		get { return Get( () => SelectedInventoryName, "" ); }
		set { Set( () => SelectedInventoryName, value ); }
	}


	public string SelectedInventoryBarcode
	{
		get { return Get( () => SelectedInventoryBarcode, "" ); }
		set { Set( () => SelectedInventoryBarcode, value ); }
	}


	[DependsUpon( nameof( SelectedInventoryName ) )]
	public void WhenSelectedInventoryNameChanges()
	{
		Logging.WriteLogLine( "New Inventory name: " + SelectedInventoryName );

		if( SelectedInventoryName.IsNotNullOrWhiteSpace() )
		{
			foreach( var Pi in Inventory )
			{
				if( Pi.Description == SelectedInventoryName )
				{
					SelectedInventoryBarcode = Pi.Barcode;
					break;
				}
			}
		}
		else
			SelectedInventoryBarcode = string.Empty;
	}

	public async Task GetInventory()
	{
		if( !IsInDesignMode )
		{
			Logging.WriteLogLine( "Loading Inventory" );

			var Inv = await Azure.Client.RequestPML_GetInventory();

			if( Inv is not null )
			{
				var Names = new List<string>();
				var List  = new ObservableCollection<PmlInventory>();

				foreach( var I in Inv )
				{
					//Logging.WriteLogLine( "Adding item: " + I.Description );
					List.Add( I );
					Names.Add( I.Description );
				}

				if( Names.Count > 0 )
				{
					Names.Insert( 0, "" );

					Inventory      = List;
					InventoryNames = new ObservableCollection<string>( Names );
				}
			}
		}
	}
#endregion

#region Global Address Timer
	public void StartUpdateGlobalAddressesTimer()
	{
		Logging.WriteLogLine( "Starting UpdateGlobalAddressesTimer..." );
		UpdateGlobalAddressesTimer          =  new DispatcherTimer();
		UpdateGlobalAddressesTimer.Tick     += UpdateGlobalAddresses_Tick;
		UpdateGlobalAddressesTimer.Interval =  new TimeSpan( 0, 1, 0 );
		UpdateGlobalAddressesTimer.Start();
	}

	public void StopUpdateGlobalAddressesTimer()
	{
		Logging.WriteLogLine( "Stopping UpdateGlobalAddressesTimer..." );
		UpdateGlobalAddressesTimer?.Stop();
	}

	private void UpdateGlobalAddresses_Tick( object sender, EventArgs e )
	{
		//Logging.WriteLogLine("Reloading the Global Address Book...");
		//Task.Run( async () =>
		//		  {
					  //await LoadGlobalAddressBook( false );
					  //Logging.WriteLogLine("Reloading the Global Address Book - Done");
				  //} );
	}

	public bool IsUpdateGlobalAddressesTimerRunning() => UpdateGlobalAddressesTimer is {IsEnabled: true};
#endregion

#region Address Timer
	/// <summary>
	///     Stops the update addresses timer.
	/// </summary>
	public void StopUpdateAddresses()
	{
		Logging.WriteLogLine( "Stopping timer" );
		UpdateAddressesTimer?.Stop();
	}

	/// <summary>
	///     Starts the update trips timer.
	/// </summary>
	public void StartUpdateAddresses()
	{
		Logging.WriteLogLine( "Starting timer" );

		// var updateTripsDelegate = new TimerCallback(UpdateTripsChecker);
		// var updateTripsChecker = new UpdateTripsChecker();

		// Run every 15 seconds
		// timerUpdateTrips = new Timer(updateTripsChecker.UpdateTrips, null, 0, 15 * 1000);

		UpdateAddressesTimer          =  new DispatcherTimer();
		UpdateAddressesTimer.Tick     += UpdateAddresses_Tick;
		UpdateAddressesTimer.Interval =  new TimeSpan( 0, 0, 15 );
		UpdateAddressesTimer.Start();
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void UpdateAddresses_Tick( object sender, EventArgs e )
	{
		//Logging.WriteLogLine("UpdateAddresses fired");
		var PuRegion  = PickupProvState;
		var DelRegion = DeliveryProvState;

		WhenAccountChanges();

		PickupProvState   = PuRegion;
		DeliveryProvState = DelRegion;
	}
#endregion

#region Save CompanyData Timer
	public void StopUpdateCompanyData()
	{
		Logging.WriteLogLine( "Stopping timer" );

		UpdateCompanyDataTimer?.Stop();
	}

	public void StartUpdateCompanyData()
	{
		UpdateCompanyDataTimer          =  new DispatcherTimer();
		UpdateCompanyDataTimer.Tick     += UpdateCompanyData_Tick;
		UpdateCompanyDataTimer.Interval =  new TimeSpan( 0, 5, 0 );
		UpdateCompanyDataTimer.Start();
	}

	private void UpdateCompanyData_Tick( object sender, EventArgs e )
	{
		// Save locally
		CompanyData cd = new();

		//cd.CompanyAddresses = Companies;
		cd.CompanyAddresses.AddRange( Companies );
		cd.EnabledNames.AddRange( (List<string>)CompanyNames );
		SaveCompanyData( cd );

		StopUpdateCompanyData();
	}
#endregion

#region Load Addresses for Companies
	private readonly BackgroundWorker Worker = new();

	private async void Worker_DoWorkLoadAddressesForCompanies( object? sender, DoWorkEventArgs? e )
	{
		Logging.WriteLogLine( "Starting to load addresses for companies" );

		var Names = await Azure.Client.RequestGetCustomerCodeList();

		if( Names is not null )
		{
			var CoNames = ( from N in Names
						    where N != GlobalAddressBookId
						    orderby N.ToLower()
						    select N ).ToList();

			// TODO Arbitrary limit - PML only has 2 and all the addresses are in PmlOz
			if( CoNames.Count < 10 )
			{
				await new Tasks.Task<string>().RunAsync( CoNames, async coName =>
															      {
																      Logging.WriteLogLine( "DEBUG Loading addresses for " + coName );

																      bool HasName;

																      lock( DictCompanySummaryListsByAccountId )
																	      HasName = DictCompanySummaryListsByAccountId.ContainsKey( coName );

																      if( !HasName )
																      {
																	      //Logging.WriteLogLine("DEBUG Loading CompanySummaryList from server");

																	      if( await Azure.Client.RequestGetCustomerCompaniesSummary( coName ) is { } Csl )
																	      {
																		      lock( DictCompanySummaryListsByAccountId )
																		      {
																			      DictCompanySummaryListsByAccountId.Add( coName, Csl );
																			      Logging.WriteLogLine( "DEBUG Found " + Csl.Count + " summary addresses for " + coName );
																		      }
																	      }
																      }

																      lock( DictCompanyDetailListsByAccountId )
																	      HasName = DictCompanyDetailListsByAccountId.ContainsKey( coName );

																      if( !HasName )
																      {
																	      if( await Azure.Client.RequestGetCustomerCompaniesDetailed( coName ) is { } Cdl )
																	      {
																		      lock( DictCompanyDetailListsByAccountId )
																		      {
																			      DictCompanyDetailListsByAccountId.Add( coName, Cdl );
																			      Logging.WriteLogLine( "DEBUG Found " + Cdl.Count + " detailed addresses for " + coName );
																		      }
																	      }
																      }
															      }, 5 );

				//Logging.WriteLogLine("DEBUG DictCompanySummaryListsByAccountId.Count == CoNames.Count");
				CompanyAddressData cad = new()
										 {
											 DictCompanySummarysList = DictCompanySummaryListsByAccountId,
											 DictCompanyDetailsList  = DictCompanyDetailListsByAccountId
										 };
				SaveCompanyAddressData( cad );
			}
			else
				Logging.WriteLogLine( "NOT caching addresses for companies - too many" );
		}
	}

	private static void Worker_Completed( object sender, RunWorkerCompletedEventArgs e )
	{
		Logging.WriteLogLine( "Worker completed" );
	}
#endregion

#region Caching of Company Addresses
	public class CompanyAddressData
	{
		public Dictionary<string, CompanySummaryList> DictCompanySummarysList { get; set; } = new();
		public Dictionary<string, CompanyDetailList>  DictCompanyDetailsList  { get; set; } = new();
	}

	private void SaveCompanyAddressData( CompanyAddressData data )
	{
		if( !IsInDesignMode )
		{
			try
			{
				var serializedObject = JsonConvert.SerializeObject( data );

				//Logging.WriteLogLine( "DEBUG Saved company data:\n" + serializedObject );
				serializedObject = Encryption.Encrypt( serializedObject );
				var fileName = MakeCompanyAddressDataFileName();
				Logging.WriteLogLine( "Saving company address data to " + fileName );
				File.WriteAllText( fileName, serializedObject );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to save company data: " + E );
			}
		}
	}

	private void LoadCachedCompanyAddressData()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var fileName = MakeCompanyDataFileName();

				if( File.Exists( fileName ) )
				{
					Logging.WriteLogLine( "Loading company address data from " + fileName );
					var serializedObject = File.ReadAllText( fileName );
					serializedObject = Encryption.Decrypt( serializedObject );
					var cad = JsonConvert.DeserializeObject<CompanyAddressData>( serializedObject );

					if( cad != null )
					{
						Dispatcher.Invoke( () =>
										   {
											   if( cad.DictCompanySummarysList.Count > 0 )
												   DictCompanySummaryListsByAccountId = cad.DictCompanySummarysList;

											   if( cad.DictCompanyDetailsList.Count > 0 )
												   DictCompanyDetailListsByAccountId = cad.DictCompanyDetailsList;
										   } );
					}
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "ERROR: unable to load company address data: " + e );

				// Doing this to ensure that this tab will load if the file is corrupted
				Logging.WriteLogLine( "Deleting bad company address data file" );
				CompanyAddressData cad = new();
				SaveCompanyAddressData( cad );
			}
		}
	}

	private string MakeCompanyAddressDataFileName()
	{
		var AppData    = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		var id = Globals.Company.CarrierId;

		return $"{BaseFolder}\\{id}-companyaddressdata.json";
	}
    #endregion

    #region Caching of Company Data
    public class CompanyData
	{
		public List<CompanyAddress> CompanyAddresses { get; set; } = new();
		public List<string>         EnabledNames     { get; set; } = new();
	}

	//private readonly BackgroundWorker Worker = new();

	//private void Worker_SaveCompanyData( object sender, DoWorkEventArgs e )
	//{

	//}

	private static void Worker_SaveCompanyDataCompleted( object sender, RunWorkerCompletedEventArgs e )
	{
		Logging.WriteLogLine( "DEBUG worker completed" );
	}

	private void SaveCompanyData( CompanyData companyData )
	{
		if( !IsInDesignMode )
		{
			try
			{
				var serializedObject = JsonConvert.SerializeObject( companyData );

				//Logging.WriteLogLine( "DEBUG Saved company data:\n" + serializedObject );
				serializedObject = Encryption.Encrypt( serializedObject );
				var fileName = MakeCompanyDataFileName();
				Logging.WriteLogLine( "Saving company data to " + fileName );
				File.WriteAllText( fileName, serializedObject );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to save company data: " + E );
			}
		}
	}

	private void LoadCachedCompanyData()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var fileName = MakeCompanyDataFileName();

				if( File.Exists( fileName ) )
				{
					Logging.WriteLogLine( "Loading company data from " + fileName );
					var serializedObject = File.ReadAllText( fileName );
					serializedObject = Encryption.Decrypt( serializedObject );
					var cd = JsonConvert.DeserializeObject<CompanyData>( serializedObject );

					if( cd != null )
					{
						Dispatcher.Invoke( () =>
										   {
											   if( ( cd.CompanyAddresses != null ) && ( cd.CompanyAddresses.Count > 0 ) )
												   Companies = cd.CompanyAddresses;

											   if( ( cd.EnabledNames != null ) && ( cd.EnabledNames.Count > 0 ) )
												   CompanyNames = cd.EnabledNames;
										   } );
					}

					CompanyFullNames.Clear();

					foreach( var ca in Companies )
					{
						if( !CompanyFullNames.ContainsKey( ca.CompanyName ) )
							CompanyFullNames.Add( ca.CompanyName, ca );
					}
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "ERROR: unable to load company data: " + e );

				// Doing this to ensure that this tab will load if the file is corrupted
				Logging.WriteLogLine( "Deleting bad company data file" );
				CompanyData cd = new();
				SaveCompanyData( cd );
			}
		}
	}

	private string MakeCompanyDataFileName()
	{
		var AppData    = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		var id = Globals.Company.CarrierId;

		return $"{BaseFolder}\\{id}-companydata.json";
	}
#endregion
}