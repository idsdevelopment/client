﻿using System.Windows.Input;

namespace ViewModels.Trips.TripEntry;

/// <summary>
///     Interaction logic for FindCustomer.xaml
/// </summary>
public partial class FindCustomer : Window
{
	private FindCustomerModel Model { get; }

	//public string ShowDialog(List<CompanyAddress> companies, SortedDictionary<string, string> sortedCompanyNameId)
	public string DisplayDialog()
	{
		//Model.Companies = companies;
		//Model.SortedCompanyNameId = sortedCompanyNameId;

		_ = ShowDialog();

		return Model.SelectedCompanyId;
	}

	public FindCustomer( Window owner )
	{
		Owner = owner;
		InitializeComponent();

		if( DataContext is FindCustomerModel m )
			Model = m;

		TbFilter.Focus();
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}

	private void Button_Click_1( object sender, RoutedEventArgs e )
	{
		Model.SelectedCompanyId = string.Empty;
		Close();
	}

	private void TbFilter_SelectionChanged( object sender, RoutedEventArgs e )
	{
		LbCustomers.ItemsSource = null;

		Model.ApplyFilter( TbFilter.Text.Trim() );

		LbCustomers.ItemsSource = Model.FilteredNames;
	}

	private void TbFilter_PreviewKeyDown( object sender, KeyEventArgs e )
	{
		switch (e.Key)
		{
			case Key.Enter:
				if (Model.IsAssignButtonEnabled)
				{
					Close();
				}
				else
				{
					if (LbCustomers.Items.Count == 1)
					{
						LbCustomers.SelectedIndex = 0;
						Model.SelectedName = LbCustomers.SelectedItem.ToString();
						Model.WhenSelectedNameChanges();
						Model.IsAssignButtonEnabled = true;
						Close();
					}
				}
				break;

			//case Key.Up:
			case Key.Down:
				LbCustomers.Focus();
				break;

			default:
				break;
		}
	}

	private void LbCustomers_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		var selected = (string)LbCustomers.SelectedItem;

		if( selected.IsNotNullOrWhiteSpace() )
			Button_Click( null, null );
	}

    private void LbCustomers_PreviewKeyDown(object sender, KeyEventArgs e)
    {
		if (e.Key == Key.Enter && Model.IsAssignButtonEnabled)
		{
			Close();
		}
		else if (e.Key == Key.Up && LbCustomers.SelectedIndex == 0)
		{
			TbFilter.SelectAll();
			TbFilter.Focus();
		}
    }
}