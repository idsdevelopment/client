﻿#nullable enable

using System.Windows.Controls;

namespace ViewModels.Trips.EditAddress;

/// <summary>
///     Interaction logic for EditAddress.xaml
/// </summary>
public partial class EditAddress : Window
{
	private readonly EditAddressModel Model;

	public (bool cancelled, CompanyDetail? result) ShowEditAddress( CompanyDetail company, string contactName,  string phone,
																	string        suite,   string addressLine1, string city,
																	string        region,  string country,      string postalCode )
	{
		CompanyDetail? Result = null;

		Model.Company      = company;
		Model.ContactName  = contactName;
		Model.ContactPhone = phone;
		Model.Suite        = suite;
		Model.Street       = addressLine1;
		Model.City         = city;
		Model.Region       = region;
		Model.Country      = country;
		Model.Pcode        = postalCode;

		ShowDialog();

		var Cancelled = Model.Cancelled;

		if( !Cancelled )
			Result = Model.BuildResult();

		return ( Cancelled, Result );
	}

	public void BtnUpdate_Click( object sender, RoutedEventArgs e )
	{
		Close();
	}

	public void BtnCancel_Click( object? sender, RoutedEventArgs? e )
	{
		Model.Cancelled = true;
		Close();
	}

	public EditAddress( Window owner )
	{
		Owner = owner;
		InitializeComponent();
		Model      = (EditAddressModel)DataContext;
		Model.View = this;

		tbContactName.Focus();
		tbContactName.SelectAll();
	}

	private void CbAddressCountry_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
	}
}