﻿namespace ViewModels.Trips.EditAddress;

internal class EditAddressModel : ViewModelBase
{
	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public CompanyDetail Company
	{
		get { return Get( () => Company, new CompanyDetail() ); }
		set { Set( () => Company, value ); }
	}

	public string CompanyName
	{
		get { return Get( () => CompanyName, string.Empty ); }
		set { Set( () => CompanyName, value ); }
	}

	public string ContactName
	{
		get => Get( () => ContactName, string.Empty );
		set { Set( () => ContactName, value ); }
	}

	public string ContactPhone
	{
		get => Get( () => ContactPhone, string.Empty );
		set { Set( () => ContactPhone, value ); }
	}

	public string Suite
	{
		get { return Get( () => Suite, string.Empty ); }
		set { Set( () => Suite, value ); }
	}

	public string Street
	{
		get { return Get( () => Street, string.Empty ); }
		set { Set( () => Street, value ); }
	}

	public string City
	{
		get { return Get( () => City, string.Empty ); }
		set { Set( () => City, value ); }
	}

	public string Region
	{
		get { return Get( () => Region, string.Empty ); }
		set { Set( () => Region, value ); }
	}

	public List<string> Regions
	{
		get { return Get( () => Regions, new List<string>() ); }
		set { Set( () => Regions, value ); }
	}

	public string Country
	{
		get { return Get( () => Country, string.Empty ); }
		set { Set( () => Country, value ); }
	}

	public List<string> Countries => CountriesRegions.Countries;

	public string Pcode
	{
		get { return Get( () => Pcode, string.Empty ); }
		set { Set( () => Pcode, value ); }
	}

	//public bool Cancelled
	//{
	//    get { return Get(() => Cancelled, false); }
	//    set { Cancelled = value; }
	//}
	public bool Cancelled { get; set; } = false;

	public EditAddress View = null;

	[DependsUpon( nameof( Company ) )]
	public void WhenCompanyChanges()
	{
		if( Company != null )
		{
			CompanyName = Company.Company.CompanyName;
			//ContactName = Company.Company.ContactName;
			ContactName  = Company.Company.UserName;
			ContactPhone = Company.Company.Phone;
			Suite        = Company.Company.Suite;
			Street       = Company.Company.AddressLine1;
			City         = Company.Company.City;
			Region       = Company.Company.Region;
			Country      = Company.Company.Country;
			Pcode        = Company.Company.PostalCode;
		}
	}

	[DependsUpon( nameof( Country ) )]
	public void WhenCountryChanges()
	{
		Regions = ( Country?.ToLower() ?? "" ) switch
		          {
			          "australia"     => CountriesRegions.AustraliaRegions,
			          "canada"        => CountriesRegions.CanadaRegions,
			          "united states" => CountriesRegions.USRegions,
			          _               => null
		          };
	}


	public CompanyDetail BuildResult()
	{
		var result = Company;
		result.Address.ContactName  = ContactName;
		result.Address.Phone        = ContactPhone;
		result.Address.Suite        = result.Company.Suite        = Suite;
		result.Address.AddressLine1 = result.Company.AddressLine1 = Street;
		result.Address.City         = result.Company.City         = City;
		result.Address.Region       = result.Company.Region       = Region;
		result.Address.Country      = result.Company.Country      = Country;
		result.Address.PostalCode   = result.Company.PostalCode   = Pcode;

		result.Company.ContactName  = ContactName;
		result.Company.UserName     = ContactName;
		result.Company.Phone        = ContactPhone;
		result.Company.Suite        = result.Company.Suite        = Suite;
		result.Company.AddressLine1 = result.Company.AddressLine1 = Street;
		result.Company.City         = result.Company.City         = City;
		result.Company.Region       = result.Company.Region       = Region;
		result.Company.Country      = result.Company.Country      = Country;
		result.Company.PostalCode   = result.Company.PostalCode   = Pcode;

		return result;
	}
}