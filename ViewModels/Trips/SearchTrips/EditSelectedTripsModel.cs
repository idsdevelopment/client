﻿using static ViewModels.Trips.SearchTrips.EditSelectedTrips;

// ReSharper disable InconsistentNaming

namespace ViewModels.Trips.SearchTrips;

public class EditSelectedTripsModel : ViewModelBase
{
	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}


	public List<EditDisplayTrip> Trips
	{
		get { return Get( () => Trips, new List<EditDisplayTrip>() ); }
		set { Set( () => Trips, value ); }

		//set
		//{
		//    Trips = new ObservableCollection<DisplayTrip>(value);
		//}
	}

	public List<string> StatusStrings
	{
		get { return Get( () => StatusStrings, GetStatusStrings ); }
		set { Set( () => StatusStrings, value ); }
	}


	public string Description
	{
		get { return Get( () => Description, FindStringResource( "EditSelectedTripsDescription" ) ); }
	}


	//public static List<string> Drivers => GetDriversList();

	public ObservableCollection<string> Drivers
	{
		get { return Get( () => Drivers, new ObservableCollection<string>() ); }
		set { Set( () => Drivers, value ); }
	}


	public string SelectedDriver
	{
		get { return Get( () => SelectedDriver, "" ); }
		set { Set( () => SelectedDriver, value ); }
	}


	public bool IsDriverSelected
	{
		get { return Get( () => IsDriverSelected, false ); }
		set { Set( () => IsDriverSelected, value ); }
	}


	public DateTime SelectedCallTime
	{
		get { return Get( () => SelectedCallTime, DateTime.MinValue ); }
		set { Set( () => SelectedCallTime, value ); }
	}


	public bool IsCallTimeSelected
	{
		get { return Get( () => IsCallTimeSelected, false ); }
		set { Set( () => IsCallTimeSelected, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public string SelectedPOD
	{
		get { return Get( () => SelectedPOD, "" ); }
		set { Set( () => SelectedPOD, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public bool IsPODSelected
	{
		get { return Get( () => IsPODSelected, false ); }
		set { Set( () => IsPODSelected, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public string SelectedPOP
	{
		get { return Get( () => SelectedPOP, "" ); }
		set { Set( () => SelectedPOP, value ); }
	}


	// ReSharper disable once InconsistentNaming
	public bool IsPOPSelected
	{
		get { return Get( () => IsPOPSelected, false ); }
		set { Set( () => IsPOPSelected, value ); }
	}


	public DateTime SelectedTPu
	{
		get { return Get( () => SelectedTPu, DateTime.MinValue ); }
		set { Set( () => SelectedTPu, value ); }
	}


	public bool IsTPuSelected
	{
		get { return Get( () => IsTPuSelected, false ); }
		set { Set( () => IsTPuSelected, value ); }
	}


	public DateTime SelectedTDel
	{
		get { return Get( () => SelectedTDel, DateTime.MinValue ); }
		set { Set( () => SelectedTDel, value ); }
	}


	public bool IsTDelSelected
	{
		get { return Get( () => IsTDelSelected, false ); }
		set { Set( () => IsTDelSelected, value ); }
	}


	public string SelectedStatus
	{
		get { return Get( () => SelectedStatus, "" ); }
		set { Set( () => SelectedStatus, value ); }
	}


	public bool IsStatusSelected
	{
		get { return Get( () => IsStatusSelected, false ); }
		set { Set( () => IsStatusSelected, value ); }
	}

	public bool AnyTripsChanged
	{
		get { return Get( () => AnyTripsChanged, false ); }
		set { Set( () => AnyTripsChanged, value ); }
	}

	[DependsUpon( nameof( IsCallTimeSelected ) )]
	[DependsUpon( nameof( IsDriverSelected ) )]
	[DependsUpon( nameof( IsPODSelected ) )]
	[DependsUpon( nameof( IsPOPSelected ) )]
	[DependsUpon( nameof( IsStatusSelected ) )]
	[DependsUpon( nameof( IsTDelSelected ) )]
	[DependsUpon( nameof( IsTPuSelected ) )]
	public bool IsApplyButtonEnabled
	{
		//get { return Get(() => IsApplyButtonEnabled, false); }
		get => IsCallTimeSelected || IsDriverSelected || IsPODSelected || IsPOPSelected || IsStatusSelected || IsTDelSelected || IsTPuSelected;
		set { Set( () => IsApplyButtonEnabled, value ); }
	}

	private bool InTripsChange;

	protected override void OnInitialised()
	{
		GetDriversList();
	}

	[DependsUpon( nameof( Trips ) )]
	public void WhenTripsChange()
	{
		if( !InTripsChange )
		{
			InTripsChange = true;

			Trips = ( from T in Trips

			          //where ( T.Status1 != STATUS.DELETED ) && ( T.Status1 != STATUS.PAID )
			          where T.Status1 != STATUS.FINALISED
			          select T ).ToList();

			InTripsChange = false;
		}
	}

	public void SetApplyButtonState()
	{
		if( IsCallTimeSelected || IsDriverSelected || IsPODSelected || IsPOPSelected || IsStatusSelected || IsTDelSelected || IsTPuSelected )
			IsApplyButtonEnabled = true;
		else
			IsApplyButtonEnabled = false;
	}

	/// <summary>
	///     Updates the trips with the new values.
	///     Note: it doesn't update the db - that is handled by SearchTripsModel.
	/// </summary>
	public void UpdateTrips()
	{
		if( Trips.Count > 0 )
		{
			// Decide what needs to be changed
			// Putting this first, so that the times, pod, pop, and driver won't be overwritten
			if( IsStatusSelected )
			{
				AnyTripsChanged = true;
				Logging.WriteLogLine( "Updating trips with status: " + SelectedStatus );
				ChangeStatusOfTrips();
			}

			if( IsDriverSelected )
			{
				AnyTripsChanged = true;
				var Driver = SelectedDriver ?? string.Empty;

				Logging.WriteLogLine( "Updating trips with driver: " + Driver );

				foreach( var Trip in Trips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing driver " + Trip.Driver + " to " + Driver );
					Trip.Driver = Driver;

					if( Trip.Status1 < STATUS.DISPATCHED )
						Trip.Status1 = STATUS.DISPATCHED;
				}
			}

			if( IsCallTimeSelected )
			{
				AnyTripsChanged = true;
				var TCallTime = SelectedCallTime;
				Logging.WriteLogLine( "Updating trips with CallTime: " + TCallTime );

				foreach( var Trip in Trips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing CallTime " + Trip.CallTime + " to " + TCallTime );
					Trip.Ct = TCallTime;
				}
			}

			if( IsPOPSelected )
			{
				AnyTripsChanged = true;
				var Pop = SelectedPOP;
				Logging.WriteLogLine( "Updating trips with POP: " + Pop );

				foreach( var Trip in Trips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing POP " + Trip.POP + " to " + Pop );
					Trip.POP = Pop;
				}
			}

			if( IsPODSelected )
			{
				AnyTripsChanged = true;
				var Pod = SelectedPOD;
				Logging.WriteLogLine( "Updating trips with POD: " + Pod );

				foreach( var Trip in Trips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing POD " + Trip.POD + " to " + Pod );
					Trip.POD = Pod;
				}
			}

			if( IsTPuSelected )
			{
				AnyTripsChanged = true;
				var TPu = SelectedTPu;
				Logging.WriteLogLine( "Updating trips with T-PU: " + TPu );

				foreach( var Trip in Trips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing T-PU " + Trip.Pt + " to " + TPu );
					Trip.Pt = TPu;
				}
			}

			if( IsTDelSelected )
			{
				AnyTripsChanged = true;
				var TDel = SelectedTDel;
				Logging.WriteLogLine( "Updating trips with T-Del: " + TDel );

				foreach( var Trip in Trips )
				{
					Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing T-Del " + Trip.Dt + " to " + TDel );
					Trip.Dt = TDel;
				}
			}
		}
	}

	private static List<string> GetStatusStrings()
	{
		var strings = new List<string>
		              {
			              //string.Empty,

			              //STATUS.UNSET.AsString(),
			              STATUS.NEW.AsString(),
			              STATUS.ACTIVE.AsString(),
			              STATUS.DISPATCHED.AsString(),
			              STATUS.PICKED_UP.AsString(),
			              STATUS.DELIVERED.AsString(),
			              STATUS.VERIFIED.AsString(),

			              //STATUS.POSTED.AsString(),
			              //STATUS.INVOICED.AsString(),
			              //STATUS.SCHEDULED.AsString(),
			              //STATUS.PAID.AsString(),
			              STATUS.FINALISED.AsString(),
			              STATUS.DELETED.AsString()
		              };

		return strings;
	}


	private void GetDriversList()
	{
		Task.Run( async () =>
		          {
			          try
			          {
				          // From Terry - stop asking the server if not logged in
				          if( !IsInDesignMode )
				          {
					          Drivers = new ObservableCollection<string>( from D in await Azure.Client.RequestGetDrivers()
					                                                      orderby D.StaffId
					                                                      select D.StaffId );
				          }
			          }
			          catch( Exception e )
			          {
				          Logging.WriteLogLine( "Exception while getting driver list: " + e );
			          }
		          } );
	}

	/// <summary>
	///     Clears out some of the fields.
	/// </summary>
	private void ChangeStatusOfTrips()
	{
		var NewStatus = ConvertStatusStringToSTATUS( SelectedStatus );

		foreach( var Trip in Trips )
		{
			Logging.WriteLogLine( "TripId: " + Trip.TripId + " - Changing status " + Trip.Status + " to " + SelectedStatus );
			Trip.Status = NewStatus;

			switch( NewStatus )
			{
			case Boards.Common.DisplayTrip.STATUS.UNSET:
			case Boards.Common.DisplayTrip.STATUS.NEW:
			case Boards.Common.DisplayTrip.STATUS.ACTIVE:
				// Clear times, pop, pod, and driver
				Trip.ClaimTime = DateTimeOffset.MinValue;

				//trip.DeliveryTime = DateTimeOffset.MinValue;
				//trip.PickupTime = DateTimeOffset.MinValue;
				Trip.VerifiedTime  = DateTimeOffset.MinValue;
				Trip.Driver        = string.Empty;
				Trip.PickupNotes   = string.Empty;
				Trip.DeliveryNotes = string.Empty;
				Trip.POD           = string.Empty;
				Trip.POP           = string.Empty;

				break;

			case Boards.Common.DisplayTrip.STATUS.DISPATCHED:
				Trip.VerifiedTime  = DateTimeOffset.MinValue;
				Trip.PickupNotes   = string.Empty;
				Trip.DeliveryNotes = string.Empty;
				Trip.POD           = string.Empty;
				Trip.POP           = string.Empty;

				break;

			case Boards.Common.DisplayTrip.STATUS.PICKED_UP:
				Trip.DeliveryNotes = string.Empty;
				Trip.POD           = string.Empty;

				if( Trip is Trip Trip2 )
				{
					var Date = Trip2.PickupTime;

					//if( Date is null || ( Date == DateTimeOffset.MaxValue ) )
					if( Date is null || ( Date == DateTimeOffset.MaxValue ) || ( Date == DateTimeOffset.MinValue ) )
						Trip2.PickupTime = DateTimeOffset.Now;
				}
				break;

			case Boards.Common.DisplayTrip.STATUS.VERIFIED:
				if( Trip is Trip Trip1 )
				{
					var Now = DateTimeOffset.Now;

					DateTimeOffset DoDate( DateTimeOffset? date )
					{
						//if( date is null || ( date == DateTimeOffset.MaxValue ) )
						if( date is null || ( date == DateTimeOffset.MaxValue ) || ( date == DateTimeOffset.MinValue ) )
							return Now;

						return (DateTimeOffset)date;
					}

					Trip1.PickupTime   = DoDate( Trip1.PickupTime );
					Trip1.DeliveryTime = DoDate( Trip1.DeliveryTime );
					Trip1.VerifiedTime = DoDate( Trip1.VerifiedTime );
				}
				break;
			}
		}
	}

	private static Boards.Common.DisplayTrip.STATUS ConvertStatusStringToSTATUS( string status )
	{
		return status switch
		       {
			       "UNSET"      => Boards.Common.DisplayTrip.STATUS.UNSET,
			       "NEW"        => Boards.Common.DisplayTrip.STATUS.NEW,
			       "ACTIVE"     => Boards.Common.DisplayTrip.STATUS.ACTIVE,
			       "DISPATCHED" => Boards.Common.DisplayTrip.STATUS.DISPATCHED,
			       "PICKED UP"  => Boards.Common.DisplayTrip.STATUS.PICKED_UP,
			       "DELIVERED"  => Boards.Common.DisplayTrip.STATUS.DELIVERED,
			       "VERIFIED"   => Boards.Common.DisplayTrip.STATUS.VERIFIED,
			       "POSTED"     => Boards.Common.DisplayTrip.STATUS.POSTED,
			       "INVOICED"   => Boards.Common.DisplayTrip.STATUS.INVOICED,
			       "SCHEDULED"  => Boards.Common.DisplayTrip.STATUS.SCHEDULED,
			       //"PAID" => Boards.Common.DisplayTrip.STATUS.PAID,
			       "FINALISED" => Boards.Common.DisplayTrip.STATUS.PAID,
			       "DELETED"   => Boards.Common.DisplayTrip.STATUS.DELETED,
			       _           => Boards.Common.DisplayTrip.STATUS.UNSET
		       };
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="name">
	/// </param>
	/// <returns></returns>
	private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );
}