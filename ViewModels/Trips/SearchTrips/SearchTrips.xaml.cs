﻿#nullable enable

using System.Diagnostics;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using DocumentFormat.OpenXml.EMMA;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using ViewModels.Trips.TripEntry;
using static ViewModels.Trips.SearchTrips.EditSelectedTrips;

namespace ViewModels.Trips.SearchTrips;

/// <summary>
///     Interaction logic for SearchTrips.xaml
/// </summary>
public partial class SearchTrips : Page, IDisposable
{
	//private Boards.Common.DisplayTrip CurrentTrip;
	//private bool WasKeyPress;

	public SearchTrips()
	{
		InitializeComponent();
		Model = (SearchTripsModel)DataContext;
		Model.LoadSettings();
		ApplyActiveColumns();

		PopulateSettingsColumnList();

		if( !Model.IsUpdateAddressesRunning() )
			Model.StartUpdateAddresses();

		Model.SetView(this);
	}

	public void Dispose()
	{
        Mouse.OverrideCursor = null;
        try
        {
            if (Model != null)
            {
                Logging.WriteLogLine($"Disposing");
                Model.StopUpdateAddresses();
                Model.Dispose();
            }
        }
        catch { }
    }

	private readonly SearchTripsModel? Model;

    public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	/// <summary>
	///     Hides any columns that aren't in the list.
	/// </summary>
	public void ApplyActiveColumns()
	{
		if (Model != null)
		{
			List<string> hidden = new();
            foreach (string key in Model.DictionaryColumnVisibility.Keys)
            {
                if (Model.DictionaryColumnVisibility[key] != Visibility.Visible)
                {
                    //Logging.WriteLogLine("DEBUG Column " + key + " is " + Model.DictionaryColumnVisibility[key]);
					hidden.Add(key);
                }
            }
			if (hidden.Count > 0)
			{
				foreach (var col in DataGrid.Columns)
				{
					// Make sure it is visible to begin
					col.Visibility = Visibility.Visible;
					string header = (string)col.Header;
					foreach (string key in hidden)
					{
						string name = FindStringResource(key);
						if (header != null && header == name)
						{
							Logging.WriteLogLine("Hiding column: " + header);
							col.Visibility = Visibility.Hidden;
							break;
						}
					}
				}
			}
			
			//if (DataGrid.View is GridView gridView)
			//{
			//	GridViewColumnCollection cols = gridView.Columns;
			//	List<string> activeColumns = Model.GetActiveColumns();

			//	foreach (GridViewColumn col in cols)
			//	{
			//		//if (col.Header is Button header)
			//		if (col.Header is GridViewColumnHeader header)
			//		{

			//		}
			//	}
			//}

		}

				// TODO
				//if( this.TripListView.View is GridView gridView )
				//{
				//	GridViewColumnCollection cols = gridView.Columns;
				//	List<string> activeColumns = Model.GetActiveColumns();

				//	foreach( GridViewColumn col in cols )
				//	{
				//		//if (col.Header is Button header)
				//		if( col.Header is GridViewColumnHeader header )
				//		{
				//			string tmp = (string)header.Content;
				//			string column = Model.MapColumnToTripField( tmp );
				//			if( column != string.Empty )
				//			{
				//				if( !activeColumns.Contains( column ) )
				//				{
				//					col.Width = 0;
				//				}
				//				else
				//				{
				//					// Tells the program to treat the width as AUTO
				//					col.Width = double.NaN;
				//				}
				//			}
				//		}
				//	}
				//}

				//if( updateContextMenu )
				//{
				//	// Build the context menu
				//	if( this.TripListView.ContextMenu is ContextMenu menu )
				//	{
				//		// Application.Current.TryFindResource("SearchTripsSelectVisibleColumns")
				//		//int index = menu.Items.IndexOf(System.Windows.Application.Current.TryFindResource("SearchTripsSelectVisibleColumns"));
				//		int index = -1;
				//		for( int i = 0; i < menu.Items.Count; i++ )
				//		{
				//			if( menu.Items[ i ] is MenuItem mi )
				//			{
				//				if( mi.Header == System.Windows.Application.Current.TryFindResource( "SearchTripsSelectVisibleColumns" ) )
				//				{
				//					index = i;
				//					break;
				//				}
				//			}
				//		}

				//		if( index > -1 )
				//		{
				//			MenuItem parent = (MenuItem)menu.Items[ index ];
				//			parent.Items.Clear();
				//			// Attach all Columns with active ones checked
				//			//Dictionary<string, bool> columns = Model.Execute_SelectVisibleColumns();
				//			Dictionary<string, bool> columns = Model.dictionaryActiveColumns;
				//			if( columns != null && columns.Count > 0 )
				//			{
				//				foreach( string key in columns.Keys )
				//				{
				//					MenuItem newMi = new MenuItem
				//					                 {
				//						                 Header = key,
				//						                 IsCheckable = true,
				//						                 IsChecked = columns[ key ]
				//						                 //Command = Model.ToggleActiveColumn(key)
				//					                 };
				//					newMi.Click += new RoutedEventHandler( MenuItem_Click );
				//					parent.Items.Add( newMi );
				//				}
				//			}
				//		}
				//	}
				//}
			}


	/// <summary>
	///     Start Status
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void ComboBox_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is ComboBox {SelectedIndex: > -1} Cb )
		{
			var Label = (string)Cb.SelectedItem;

			// Special case for PICKED_UP
			if( Label.IndexOf( " ", StringComparison.Ordinal ) > -1 )
				Label = Label.Replace( " ", "_" );
			var Status = (STATUS)Enum.Parse( typeof( STATUS ), Label );
			Model.SelectedStartStatus = Status;
		}
	}

	/// <summary>
	///     End Status.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void ComboBox_SelectionChanged_1( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is ComboBox {SelectedIndex: > -1} Cb )
		{
			var Label = (string)Cb.SelectedItem;

			// Special case for PICKED_UP
			if( Label.IndexOf( " ", StringComparison.Ordinal ) > -1 )
				Label = Label.Replace( " ", "_" );
			var Status = (STATUS)Enum.Parse( typeof( STATUS ), Label );
			Model.SelectedEndStatus = Status;
		}
	}

	/// <summary>
	///     Filter box.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void TextBox_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( Model is not null && sender is TextBox Tb )
		{
			Model.Execute_Filter( Tb.Text.Trim() );
		}
	}

    private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (Model != null && e.Key == Key.Escape)
            Model.TripAdhocFilter = string.Empty;
    }

    private void TripListView_MouseDoubleClick( object? sender, MouseButtonEventArgs? e )
	{
		if( DataGrid.SelectedItem is DisplayTrip SelectedTrip )
		{
			Logging.WriteLogLine( "Selected trip: " + SelectedTrip.TripId );

			var LatestVersion = GetLatestVersion( SelectedTrip.TripId );

			if( LatestVersion?.Signatures != null )
			{
				Logging.WriteLogLine( "latestVersion.Signatures: " + LatestVersion.Signatures.Count );

				foreach( var Sig in LatestVersion.Signatures )
					Logging.WriteLogLine( "\tsignature.Status: " + Sig.Status );
			}

			// Checking for TripId in case the trip is in Storage
			if( ( LatestVersion != null ) && LatestVersion.TripId.IsNotNullOrWhiteSpace() && ( SelectedTrip.LastModified.ToLongTimeString() != LatestVersion.LastModified.ToLongTimeString() ) )
			{
				Logging.WriteLogLine( "Latest version - was : " + SelectedTrip.LastModified.ToLongTimeString() + ", now: " + LatestVersion.LastModified.ToLongTimeString() );
				SelectedTrip = LatestVersion;

				if( DataContext is SearchTripsModel TripsModel )
				{
					Dispatcher.Invoke( () =>
									   {
										   TripsModel.UpdateTripsWithLatestVersion( SelectedTrip );
									   } );
				}
			}

            // Need to create a new Trip Entry tab
            Logging.WriteLogLine("Opening new " + Globals.TRIP_ENTRY + " tab");
            Globals.RunNewProgram(Globals.TRIP_ENTRY, SelectedTrip);					
		}
	}

    private void TripListView_MouseDoubleClick_V1( object? sender, MouseButtonEventArgs? e )
	{
		if( DataGrid.SelectedItem is DisplayTrip SelectedTrip )
		{
			Logging.WriteLogLine( "Selected trip: " + SelectedTrip.TripId );

			var LatestVersion = GetLatestVersion( SelectedTrip.TripId );

			if( LatestVersion?.Signatures != null )
			{
				Logging.WriteLogLine( "latestVersion.Signatures: " + LatestVersion.Signatures.Count );

				foreach( var Sig in LatestVersion.Signatures )
					Logging.WriteLogLine( "\tsignature.Status: " + Sig.Status );
			}

			// TODO Checking for TripId in case the trip is in Storage
			if( ( LatestVersion != null ) && LatestVersion.TripId.IsNotNullOrWhiteSpace() && ( SelectedTrip.LastModified.ToLongTimeString() != LatestVersion.LastModified.ToLongTimeString() ) )
			{
				Logging.WriteLogLine( "Latest version - was : " + SelectedTrip.LastModified.ToLongTimeString() + ", now: " + LatestVersion.LastModified.ToLongTimeString() );
				SelectedTrip = LatestVersion;

				if( DataContext is SearchTripsModel TripsModel )
				{
					Dispatcher.Invoke( () =>
									   {
										   TripsModel.UpdateTripsWithLatestVersion( SelectedTrip );
									   } );
				}
			}

			var Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

			if( Tis.Count > 0 )
			{
				PageTabItem? Pti = null;
				// CD1-T24 Open in new tab
				//for( var i = 0; i < tis.Count; i++ )
				//{
				//	var ti = tis[ i ];
				//	if( ti.Content is TripEntry.TripEntry )
				//	{
				//		pti = ti;
				//		// Load the trip into the first TripEntry tab
				//		Logging.WriteLogLine( "Displaying trip in extant " + Globals.TRIP_ENTRY + " tab" );

				//		//( (TripEntry.TripEntryModel)pti.Content.DataContext ).Execute_ClearTripEntry();
				//		( (TripEntry.TripEntryModel)pti.Content.DataContext ).CurrentTrip = selectedTrip;
				//		await ((TripEntry.TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
				//		//((TripEntry.TripEntryModel)pti.Content.DataContext).PickupCountry = selectedTrip.PickupAddressCountry;
				//		//((TripEntry.TripEntryModel)pti.Content.DataContext).WhenPickupCountryChanges();
				//		//((TripEntry.TripEntryModel)pti.Content.DataContext).WhenSelectedAccountChanges();
				//		Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

				//		//( (SearchTrips)ti.Content ).ComboBox_SelectionChanged_2Async( null, null );

				//		break;
				//	}
				//}

				if( Pti == null )
				{
					// Need to create a new Trip Entry tab
					Logging.WriteLogLine( "Opening new " + Globals.TRIP_ENTRY + " tab" );
					Globals.RunNewProgram( Globals.TRIP_ENTRY, SelectedTrip );
					//Thread.Sleep(500);
					/*
					//Thread.Sleep(500);
					Dispatcher.Invoke(() =>
					{
						tis = Globals.DataContext.MainDataContext.ProgramTabItems;
						for (var i = 0; i < tis.Count; i++)
						{
							var ti = tis[i];
							if (ti.Content is TripEntry.TripEntry)
							{
								((TripEntry.TripEntryModel)ti.Content.DataContext).Execute_ClearTripEntry(false, selectedTrip.PickupAddressCountry);
								((TripEntry.TripEntryModel)ti.Content.DataContext).PickupCountry = selectedTrip.PickupAddressCountry;
								((TripEntry.TripEntryModel)ti.Content.DataContext).WhenPickupCountryChanges();
								((TripEntry.TripEntryModel)ti.Content.DataContext).DeliveryCountry = selectedTrip.DeliveryAddressCountry;
								((TripEntry.TripEntryModel)ti.Content.DataContext).WhenDeliveryCountryChanges();

								Thread.Sleep(500);

								((TripEntry.TripEntryModel)ti.Content.DataContext).CurrentTrip = selectedTrip;
								((TripEntry.TripEntryModel)ti.Content.DataContext).WhenCurrentTripChanges();


								Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;
								break;
							}
						}
					});
					*/
					Tis = Globals.DataContext.MainDataContext.ProgramTabItems;

					//for (var i = 0; i < tis.Count; i++)
					for( var I = Tis.Count - 1; I >= 0; I-- )
					{
						var Ti = Tis[ I ];

						if( Ti.Content is TripEntry.TripEntry )
						{
							( (TripEntryModel)Ti.Content.DataContext ).Execute_ClearTripEntry( false, SelectedTrip.PickupAddressCountry );
							( (TripEntryModel)Ti.Content.DataContext ).PickupCountry = SelectedTrip.PickupAddressCountry;
							( (TripEntryModel)Ti.Content.DataContext ).WhenPickupCountryChanges();
							( (TripEntryModel)Ti.Content.DataContext ).DeliveryCountry = SelectedTrip.DeliveryAddressCountry;
							( (TripEntryModel)Ti.Content.DataContext ).WhenDeliveryCountryChanges();

							Thread.Sleep( 500 );

							( (TripEntryModel)Ti.Content.DataContext ).CurrentTrip = SelectedTrip;
							//await ( (TripEntryModel)ti.Content.DataContext ).WhenCurrentTripChanges(false);
							//	await ( (TripEntryModel)ti.Content.DataContext ).WhenCurrentTripChanges();

							Globals.DataContext.MainDataContext.TabControl.SelectedIndex = I;
							break;
						}
					}
				}
			}
		}
	}


	private static DisplayTrip? GetLatestVersion( string tripId )
	{
		DisplayTrip? Dt = null;
		Logging.WriteLogLine( "Getting latest version of trip: " + tripId );

		Task.WaitAll( Task.Run( () =>
							    {
								    var GetTrip = new GetTrip
											      {
												      Signatures = true,
												      TripId     = tripId
											      };
								    var Trip = Azure.Client.RequestGetTrip( GetTrip ).Result;

								    if( ( Trip != null ) && Trip.TripId.IsNotNullOrWhiteSpace() )
									    Dt = new DisplayTrip( Trip );
							    } ) );

		return Dt;
	}

	/// <summary>
	///     Edit Selected Trips.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	//private async void MenuItem_Click_1( object sender, RoutedEventArgs e )
	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		if( DataGrid.SelectedItems.Count > 0 )
		{
			var Sb       = new StringBuilder();
			var Selected = new List<EditDisplayTrip>();

			foreach( DisplayTrip I in DataGrid.SelectedItems )
			{
				//var tmp = new EditDisplayTrip( I );
				//var dt  = new EditDisplayTrip( GetLatestVersion( tmp.TripId ) );
				//var Dt = new EditDisplayTrip( GetLatestVersion( I.TripId ) );
				// Eddy found a trip in PML (O673397602007) that returned a null for the latest version
				EditDisplayTrip Dt;
				var tmp = GetLatestVersion(I.TripId);
				if ( tmp != null )
				{
					Dt = new EditDisplayTrip( tmp );
				}
				else
				{
					Dt = new EditDisplayTrip(I);
				}

				//selected.Add( (DisplayTrip)tmp );
				//sb.Append( ( (DisplayTrip)tmp ).TripId ).Append( "," );
				Selected.Add( Dt );
				Sb.Append( Dt.TripId ).Append( "," );
			}

			Logging.WriteLogLine( "Edit Selected Trips: TripIds: " + Sb );
			var Changed = new EditSelectedTrips( Application.Current.MainWindow, Selected ).ShowEditSelectedTrips( Selected );

			if( Changed is {Count: > 0} )
			{
				if( Model is not null )
				{
					Dispatcher.Invoke( () =>
									   {
										   Model.UpdateChangedTripsAsync( Changed );
										   Logging.WriteLogLine( "Finished updating" );

										   //Thread.Sleep( 1000 );
										   //Model.Execute_SearchTrips();

										   // Update the displayed trips
										   if( ( Changed.Count > 0 ) && Model.Trips is {Count: > 0} )
										   {
											   DataGrid.Visibility = Visibility.Hidden;

											   foreach( var Trip in Changed )
											   {
												   try
												   {
													   var Match = ( from T in Model.Trips
																     where T.TripId != Trip.TripId
																     select T ).First();

													   if( Match != null )
													   {
														   Match.Driver = Trip.Driver;
														   Match.Ct     = Trip.Ct;
														   Match.POP    = Trip.POP;
														   Match.POD    = Trip.POD;
														   Match.Pt     = Trip.Pt;
														   Match.Dt     = Trip.Dt;
														   Match.Status = Trip.Status;
													   }
												   }
												   catch( Exception E )
												   {
													   Logging.WriteLogLine( "Exception thrown: " + E );
												   }
											   }
											   DataGrid.Visibility = Visibility.Visible;
											   DataGrid.InvalidateVisual();
										   }
									   } );
				}
				Logging.WriteLogLine( "Updated " + Changed.Count + " trips" );
			}
		}
	}

	private void ComboBox_SelectionChanged_3( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is ComboBox {SelectedIndex: > -1} Cb )
		{
			var Address = (string)Cb.SelectedItem;
			Model.SelectedCompanyPUAddressName = Address;
		}
	}

	private void ComboBox_SelectionChanged_4( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is ComboBox {SelectedIndex: > -1} Cb )
		{
			var Address = (string)Cb.SelectedItem;
			Model.SelectedCompanyDelAddressName = Address;
		}
	}

	private void CbCompanyName_Checked( object sender, RoutedEventArgs e )
	{
		if( Model is not null )
		{
			if( cbCompanyName.IsChecked == true )
			{
				comboCompanyName.IsEnabled = true;
				cbPUAddress.IsEnabled      = true;
				//this.comboPUAddress.IsEnabled = true;
				cbDelAddress.IsEnabled = true;
				//this.comboDelAddress.IsEnabled = true;

				Model.FilterByCompanyName = true;
			}
			else
			{
				comboCompanyName.IsEnabled = false;
				cbPUAddress.IsEnabled      = false;
				cbPUAddress.IsChecked      = false;
				comboPUAddress.IsEnabled   = false;
				cbDelAddress.IsEnabled     = false;
				cbDelAddress.IsChecked     = false;

				comboDelAddress.IsEnabled = false;
				Model.FilterByCompanyName = false;
			}
		}
	}

	private void CbPUAddress_Checked( object sender, RoutedEventArgs e )
	{
		if( Model is not null )
		{
			if( cbPUAddress.IsChecked == true )
			{
				comboPUAddress.IsEnabled = true;
				Model.FilterByPUAddress  = true;
			}
			else
			{
				comboPUAddress.IsEnabled = false;
				Model.FilterByPUAddress  = false;
			}
		}
	}

	private void CbDelAddress_Checked( object sender, RoutedEventArgs e )
	{
		if( Model is not null )
		{
			if( cbDelAddress.IsChecked == true )
			{
				comboDelAddress.IsEnabled     = true;
				Model.FilterByDeliveryAddress = true;
			}
			else
			{
				comboDelAddress.IsEnabled     = false;
				Model.FilterByDeliveryAddress = false;
			}
		}
	}

	private void DatePicker_SelectedDateChanged( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is DatePicker {SelectedDate: { }} Dp )
			Model.FromDate = (DateTimeOffset)Dp.SelectedDate;
	}

	private void DatePicker_SelectedDateChanged_1( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is DatePicker Dp )
		{
			if( Dp.SelectedDate != null )
				Model.ToDate = (DateTimeOffset)Dp.SelectedDate;
			else
				Model.ToDate = DateTimeOffset.Now;
		}
	}

	private void BtnHelp_Click( object sender, RoutedEventArgs e )
	{
		if( Model is not null )
		{
			Logging.WriteLogLine( "Opening " + Model.HelpUri );
			var Uri = new Uri( Model.HelpUri );
			Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
		}
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		dpFrom.SelectedDate = DateTime.Now;
		dpTo.SelectedDate   = DateTime.Now;

		comboCompanyName.SelectedIndex = -1;
		comboPUAddress.SelectedIndex   = -1;
		comboDelAddress.SelectedIndex  = -1;
	}

	private void CheckBox_Checked( object sender, RoutedEventArgs e )
	{
		tbClientReference.IsEnabled = false;
		tbTripId.IsEnabled          = true;
		tbTripId.Focus();

		cbPUAddress.IsEnabled  = false;
		cbDelAddress.IsEnabled = false;
	}

	private void CheckBox_Unchecked( object sender, RoutedEventArgs e )
	{
		tbTripId.IsEnabled = false;

		if( cbCompanyName.IsChecked == true )
		{
			cbPUAddress.IsEnabled  = true;
			cbDelAddress.IsEnabled = true;
		}
	}

	private void CheckBox_Checked_1( object sender, RoutedEventArgs e )
	{
		tbTripId.IsEnabled          = false;
		tbClientReference.IsEnabled = true;
		tbClientReference.Focus();

		cbPUAddress.IsEnabled  = false;
		cbDelAddress.IsEnabled = false;
	}

	private void CheckBox_Unchecked_1( object sender, RoutedEventArgs e )
	{
		tbClientReference.IsEnabled = false;

		if( cbCompanyName.IsChecked == true )
		{
			cbPUAddress.IsEnabled  = true;
			cbDelAddress.IsEnabled = true;
		}
	}

	private void BtnEdit_Click( object sender, RoutedEventArgs e )
	{
		//MenuItem_Click_1(null, null);            
		MiOpenInShipmentEntry_Click( null, null );
	}

	//
	// Copied from DispatchBoard.xaml.cs


	private void DataGrid_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is DataGrid Grid )
		{
			var Trips = Model.SelectedTrips;

			if( Trips is null )
				//Trips = new List<DisplayTrip>();
				Trips = new ObservableCollection<DisplayTrip>();
			else
				Trips.Clear();

			Model.SelectedTrips = null;
			//Trips.AddRange(Grid.SelectedItems.Cast<DisplayTrip>()); 

			foreach( DisplayTrip Dt in Grid.SelectedItems )
				Trips.Add( Dt );
			Model.SelectedTrips = Trips;
		}
	}

	//private void DataGrid_CleanUpVirtualizedItem(object sender, CleanUpVirtualizedItemEventArgs e)
	//{
	//	var row = e.UIElement as DataGridRow;
	//	if (row != null && row.Background == Brushes.LightGreen) //don't recycle light green rows...
	//	{
	//		e.Cancel = true;
	//	}
	//}

	private void ToggleButton_Checked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var DGrid = Tb.FindParent<DataGrid>();

			if( DGrid is not null )
			{
				var Ndx = DGrid.SelectedIndex;

				if( Ndx >= 0 )
				{
					var Itm = (DisplayTrip)DGrid.SelectedItem;
					Itm.DetailsVisible = true;

					var Row = DGrid.GetRow( Ndx );

					if( Row is not null )
						DGrid.ScrollIntoView( Row );
				}
			}
		}
	}

	private void ToggleButton_Unchecked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var Grid = Tb.FindParent<DataGrid>();

			if( Grid?.SelectedItem is DisplayTrip Itm )
				Itm.DetailsVisible = false;
		}
	}

	private void MiOpenInShipmentEntry_Click( object? sender, RoutedEventArgs? e )
	{
		// Same as double-click
		Logging.WriteLogLine( "loading into Shipment Entry tab" );
		TripListView_MouseDoubleClick( null, null );
	}

	/// <summary>
	///     Selected Company name.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	public async void ComboBox_SelectionChanged_2Async( object sender, SelectionChangedEventArgs e )
	{
		if( Model is not null && sender is ComboBox {SelectedIndex: > -1} Cb )
		{
			var CompanyName = (string)Cb.SelectedItem;
			Model.SelectedCompanyName = CompanyName;
			//Model.CompanyAddressNames.Clear();

			var Tmp = await Task.Run( () => Model.GetCompanyAddressDescriptions() );

			Model.CompanyAddressNames = Tmp;

			if( ( Tmp != null ) && ( Tmp.Count > 0 ) )
			{
				comboPUAddress.SelectedIndex  = 0;
				comboDelAddress.SelectedIndex = 0;
			}
		}
	}

    #region Settings

    private readonly List<CheckBox> CbColumns = new();

    private void Settings_Click(object sender, RoutedEventArgs e)
    {
        //Logging.WriteLogLine("Opening settings tab");
        tabItemSearch.Visibility = Visibility.Visible;
        tabItemSettings.Visibility = Visibility.Visible;
        tabItemSettings.IsSelected = true;
        tabItemSettings.Focus();
    }

    private void BtnCloseSettings_Click(object sender, RoutedEventArgs e)
    {
        //Logging.WriteLogLine("Closing settings tab");
        tabItemSearch.Visibility = Visibility.Collapsed;
        tabItemSettings.Visibility = Visibility.Collapsed;
        tabItemSearch.IsSelected = true;
        tabItemSearch.Focus();
    }

    /*
	 * StackPanel sp = new()
						{
							Name        = "SpDefaultPackageType",
							Orientation = Orientation.Horizontal
						};
	 */

    private void PopulateSettingsColumnList()
    {
        CbColumns.Clear();
        gridColumns.RowDefinitions.Clear();
        gridColumns.Children.Clear();

        var Columns = Model?.GetAllColumnsInOrder();

        if ((Columns != null) && (Columns.Count > 0) && Model != null)
        {
            Thickness margin = new(50, 0, 0, 3);

            gridColumns.RowDefinitions.Add(new RowDefinition());

            var LabelVisible = new Label
            {
                FontWeight = FontWeights.Bold,
                Content = FindStringResource("SearchTripsSettingsLabelVisible")
            };

            gridColumns.Children.Add(LabelVisible);
            Grid.SetColumn(LabelVisible, 0);
            Grid.SetRow(LabelVisible, 0);

            var Row = 1;
            var BgGray = Brushes.LightGray;
            var BgWhite = Brushes.White;
            var Bg = BgGray;

            foreach (var Column in Columns)
            {
                if (Column != "SearchTripsListTripId")
                {
                    gridColumns.RowDefinitions.Add(new RowDefinition());
					StackPanel sp = new()
					{
						Orientation = Orientation.Vertical,
						Background = Bg,
                    };

					gridColumns.Children.Add(sp);
					Grid.SetColumn(sp, 0);
					Grid.SetColumnSpan(sp, 3);
					Grid.SetRow(sp, Row);

                    var Name = FindStringResource(Column);

                    var CheckBox = new CheckBox
                    {
                        Name = Column,
                        Content = Name,
                        Margin = margin,
                    };

                    sp.Children.Add(CheckBox);

                    if (Model.DictionaryColumnVisibility.ContainsKey(Column) && (Model.DictionaryColumnVisibility[Column] == Visibility.Visible))
                        CheckBox.IsChecked = true;

                    CbColumns.Add(CheckBox);
                    ++Row;

                    Bg = ( Row % 2 ) == 0 ? BgWhite : BgGray;
                }
            }
        }
    }

    private void PopulateSettingsColumnList_V1()
    {
        CbColumns.Clear();
        gridColumns.RowDefinitions.Clear();
        gridColumns.Children.Clear();

        var Columns = Model?.GetAllColumnsInOrder();

        if ((Columns != null) && (Columns.Count > 0) && Model != null)
        {
			Thickness margin = new Thickness(50,0,0,2);

            gridColumns.RowDefinitions.Add(new RowDefinition());

            var LabelVisible = new Label
            {
                FontWeight = FontWeights.Bold,
                Content = FindStringResource("SearchTripsSettingsLabelVisible")
            };

            gridColumns.Children.Add(LabelVisible);
            Grid.SetColumn(LabelVisible, 0);
            Grid.SetRow(LabelVisible, 0);

            var Row = 1;

            foreach (var Column in Columns)
            {
                if (Column != "SearchTripsListTripId")
                {
                    gridColumns.RowDefinitions.Add(new RowDefinition());

                    var Name = FindStringResource(Column);
					
					Brush background = Brushes.Gray;

                    var CheckBox = new CheckBox
                    {
                        Name = Column,
                        Content = Name,
						Margin = margin,
                    };
					
					gridColumns.Children.Add(CheckBox);
                    Grid.SetColumn(CheckBox, 0);
                    Grid.SetRow(CheckBox, Row);

                    if (Model.DictionaryColumnVisibility.ContainsKey(Column) && (Model.DictionaryColumnVisibility[Column] == Visibility.Visible))
                        CheckBox.IsChecked = true;

                    CbColumns.Add(CheckBox);
                    ++Row;
                }
                //else
                //{
                //	Label label = new Label { Content = "   " };
                //	this.gridColumns.Children.Add(label);
                //	Grid.SetColumn(label, 0);
                //	Grid.SetRow(label, row);
                //	++row;
                //}
            }
        }
    }

    private void BtnSelectAll_Click(object sender, RoutedEventArgs e)
    {
        foreach (var Cb in CbColumns)
            Cb.IsChecked = true;
    }

    private void BtnSelectNone_Click(object sender, RoutedEventArgs e)
    {
        foreach (var Cb in CbColumns)
            Cb.IsChecked = false;
    }

    public void BtnSaveColumns_Click(object? sender, RoutedEventArgs? e)
    {
        //Logging.WriteLogLine("BtnSaveColumns clicked");
		if (Model != null)
		{
			var Columns = Model.GetAllColumnsInOrder();
			var NewColumns = new List<string>();

			//var I = 1; // TripId column is always visible
			int I = 0;

			foreach (var Cb in CbColumns)
			{
				var Tmp = Cb.IsChecked ?? false;

				if (Tmp && I < Columns.Count)
					NewColumns.Add(Columns[I]);
				++I;
			}

			Model.SaveSearchColumns(NewColumns);

			foreach (var Col in DataGrid.Columns)
			{
				//Logging.WriteLogLine("col.Header: " + col.Header);
				foreach (var Name in Columns)
				{
					var Header = FindStringResource(Name);

					if ((Header != null) && (Col.Header != null) && (Col.Header.ToString() == Header))
					{
						Col.Visibility = Model.DictionaryColumnVisibility[Name];
						break;
					}
				}
			}

			PopulateSettingsColumnList();

			//this.AddColumns();
			//ICollectionView cvTrips = CollectionViewSource.GetDefaultView(DataGrid.ItemsSource);
			//if (cvTrips != null && cvTrips.CanGroup == true)
			//{
			//	cvTrips.GroupDescriptions.Clear();
			//	cvTrips.GroupDescriptions.Add(new PropertyGroupDescription("Driver"));
			//}

			tabControl.SelectedIndex = 0;
		}
    }


    #endregion

    private void BtnSaveOtherSettings_Click(object sender, RoutedEventArgs e)
    {
		Model?.SaveOtherSettings();
    }

    private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
    {
        var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
        {
            RoutedEvent = UIElement.MouseWheelEvent,
            Source = e.Source
        };

        ScrollViewer scv = (ScrollViewer)sender;
		scv.RaiseEvent(eventArg);
		e.Handled = true;
    }
}