﻿#nullable enable

using System.CodeDom;
using System.Collections;
using System.IO;
using System.Windows.Data;
using System.Windows.Threading;
using ViewModels.BaseViewModels;
using ViewModels.Customers;
using ViewModels.Trips.Boards;
using static ViewModels.Trips.SearchTrips.EditSelectedTrips;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

// ReSharper disable InconsistentNaming

namespace ViewModels.Trips.SearchTrips;

public static class Extensions
{
	public static STATUS ToProtocolStatus( this Boards.Common.DisplayTrip.STATUS status )
	{
		return status switch
			   {
				   Boards.Common.DisplayTrip.STATUS.NEW        => STATUS.NEW,
				   Boards.Common.DisplayTrip.STATUS.ACTIVE     => STATUS.ACTIVE,
				   Boards.Common.DisplayTrip.STATUS.DISPATCHED => STATUS.DISPATCHED,
				   Boards.Common.DisplayTrip.STATUS.PICKED_UP  => STATUS.PICKED_UP,
				   Boards.Common.DisplayTrip.STATUS.DELIVERED  => STATUS.DELIVERED,
				   Boards.Common.DisplayTrip.STATUS.VERIFIED   => STATUS.VERIFIED,

				   Boards.Common.DisplayTrip.STATUS.PAID    => STATUS.FINALISED,
				   Boards.Common.DisplayTrip.STATUS.DELETED => STATUS.DELETED,

				   Boards.Common.DisplayTrip.STATUS.INVOICED => STATUS.INVOICED,
				   Boards.Common.DisplayTrip.STATUS.POSTED   => STATUS.POSTED,

				   Boards.Common.DisplayTrip.STATUS.LIMBO => STATUS.LIMBO,

				   _ => STATUS.UNKNOWN
			   };
	}
}

public class DisplayTrip : Boards.Common.DisplayTrip
{
	// These won't compile on my system
	//public new string CallTime => ( base.CallTime != null ? base.CallTime : DateTime.MinValue).ShowAsLocalTime( ShowAsLocalTime );
	public new string CallTime  => ( base.CallTime ?? DateTime.MinValue ).ShowAsLocalTime( ShowAsLocalTime );
	public new string ReadyTime => ( base.ReadyTime ?? DateTime.MinValue ).ShowAsLocalTime( ShowAsLocalTime );
	public new string DueTime   => ( base.DueTime ?? DateTime.MinValue ).ShowAsLocalTime( ShowAsLocalTime );


	//public new string PickupTime   => ( base.PickupTime != null ? base.PickupTime : DateTimeOffset.MinValue ).ShowAsLocalTime( ShowAsLocalTime );
	public new string PickupTime => ( base.PickupTime ?? DateTimeOffset.MinValue ).ShowAsLocalTime( ShowAsLocalTime );

	//public new string DeliveryTime => ( base.DeliveryTime != null ? base.DeliveryTime : DateTimeOffset.MinValue).ShowAsLocalTime( ShowAsLocalTime );
	public new string DeliveryTime => ( base.DeliveryTime ?? DateTimeOffset.MinValue ).ShowAsLocalTime( ShowAsLocalTime );

	public string VerifiedTimeDisplay
	{
		get
		{
			var dt = VerifiedTime;
			return dt.ShowAsLocalTime( ShowAsLocalTime );
		}
		//set { Set(() => VerifiedTime, value); }
	}

	public string Invoice { get; } = "";

	public DisplayTrip( Trip t ) : base( t, new ServiceLevel() )
	{
		// Kludge 
		POD                 = t.POD;
		POP                 = t.POP;
		BillingAddressPhone = t.BillingAddressPhone;
		BillingAddressNotes = t.BillingAddressNotes;
	}

	public bool ShowAsLocalTime = true;
}

public class SearchTripsList : ObservableCollection<DisplayTrip>
{
	public bool ShowAsLocalTime
	{
		get => _ShowAsLocalTime;
		set
		{
			_ShowAsLocalTime = value;

			foreach( var Trip in this )
				Trip.ShowAsLocalTime = value;

			CollectionViewSource.GetDefaultView( this ).Refresh();
		}
	}

	private bool _ShowAsLocalTime;

	public new void Add( DisplayTrip t )
	{
		t.ShowAsLocalTime = ShowAsLocalTime;
		base.Add( t );
	}
}

/// <summary>
///     From http://www.albahari.com/nutshell/predicatebuilder.aspx
/// </summary>

//public static class PredicateBuilder
//{
//    public static Expression<Func<T, bool>> True<T>() { return f => true; }
//    public static Expression<Func<T, bool>> False<T>() { return f => false; }

//    public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
//                                                        Expression<Func<T, bool>> expr2)
//    {
//        var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
//        return Expression.Lambda<Func<T, bool>>
//              (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
//    }

//    public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
//                                                         Expression<Func<T, bool>> expr2)
//    {
//        var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
//        return Expression.Lambda<Func<T, bool>>
//              (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
//    }
//}
public class SearchTripsModel : AMessagingViewModel
{
	// Used for the filter
	private const string SORT_CALLTIME        = "CallTime";
	private const string SORT_ACCOUNT         = "AccountId";
	private const string SORT_DRIVER          = "Driver";
	private const string SORT_DELIVERY_CITY   = "DeliveryCity";
	private const string SORT_DELIVERY_NAME   = "DeliveryName";
	private const string SORT_DELIVERY_STATE  = "DeliveryState";
	private const string SORT_DELIVERY_STREET = "DeliveryStreet";
	private const string SORT_DELIVERY_TIME   = "DeliveryTime";
	private const string SORT_PACKAGE_TYPE    = "PackageType";
	private const string SORT_PICKUP_CITY     = "PickupCity";
	private const string SORT_PICKUP_COMPANY  = "PickupCompany";
	private const string SORT_PICKUP_STATE    = "PickupState";
	private const string SORT_PICKUP_STREET   = "PickupStreet";
	private const string SORT_PICKUP_TIME     = "PickupTime";
	private const string SORT_PIECES          = "Pieces";
	private const string SORT_REFERENCE       = "Reference";
	private const string SORT_STATUS          = "Status";
	private const string SORT_TRIP_ID         = "TripId";
	private const string SORT_WEIGHT          = "Weight";

//	private const string SORT_READYTIME       = "ReadyTime";
	private const string SORT_DUETIME      = "DueTime";
	private const string SORT_VERIFIEDTIME = "VerifiedTime";

    public static Settings? SearchSettings { get; set; }
    public static OtherSettings? MiscSettings { get; set; }

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	[Setting]
	public bool AsLocalTime
	{
		get { return Get( () => AsLocalTime, true ); }
		set { Set( () => AsLocalTime, value ); }
	}

	public SearchTripsList Trips
	{
		get { return Get( () => Trips, new SearchTripsList() ); }
		set { Set( () => Trips, value ); }
	}


	public DisplayTrip? SelectedTrip
	{
		get { return Get( () => SelectedTrip, (DisplayTrip?)null ); }
		set { Set( () => SelectedTrip, value ); }
	}


	public int TotalTrips
	{
		get { return Get( () => TotalTrips, 0 ); }
		set { Set( () => TotalTrips, value ); }
	}

	public string TotalSearchTime
	{
		get { return Get( () => TotalSearchTime, "" ); }
		set { Set( () => TotalSearchTime, value ); }
	}

	public DateTimeOffset FromDate
	{
		get { return Get( () => FromDate, DateTimeOffset.Now ); }
		set { Set( () => FromDate, value ); }
	}

	public DateTimeOffset ToDate
	{
		get { return Get( () => ToDate, DateTimeOffset.Now ); }
		set { Set( () => ToDate, value ); }
	}


	public bool UseCallTime
	{
		get { return Get( () => UseCallTime, true ); }
		set { Set( () => UseCallTime, value ); }
	}


	public bool UsePUTime
	{
		get { return Get( () => UsePUTime, false ); }
		set { Set( () => UsePUTime, value ); }
	}


	public bool UseDelTime
	{
		get { return Get( () => UseDelTime, false ); }
		set { Set( () => UseDelTime, value ); }
	}

	public bool UseVerTime
	{
		get { return Get( () => UseVerTime, false ); }
		set { Set( () => UseVerTime, value ); }
	}

	public bool SearchStatusAll
	{
		get { return Get( () => SearchStatusAll, true ); }
		set { Set( () => SearchStatusAll, value ); }
	}

	public bool SearchStatusVerified
	{
		get { return Get( () => SearchStatusVerified, false ); }
		set { Set( () => SearchStatusVerified, value ); }
	}

	public bool SearchStatusPickedUp
	{
		get { return Get( () => SearchStatusPickedUp, false ); }
		set { Set( () => SearchStatusPickedUp, value ); }
	}

	public bool SearchStatusDeleted
	{
		get { return Get( () => SearchStatusDeleted, false ); }
		set { Set( () => SearchStatusDeleted, value ); }
	}

	public bool SearchStatusScheduled
	{
		get { return Get( () => SearchStatusScheduled, false ); }
		set { Set( () => SearchStatusScheduled, value ); }
	}

	public bool SearchByClientReference
	{
		get { return Get( () => SearchByClientReference, false ); }
		set { Set( () => SearchByClientReference, value ); }
	}

	public string ClientReference
	{
		get { return Get( () => ClientReference, "" ); }
		set { Set( () => ClientReference, value ); }
	}

	public bool SearchByTripId
	{
		get { return Get( () => SearchByTripId, false ); }
		set { Set( () => SearchByTripId, value ); }
	}

	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	public bool SearchByPackageType
	{
		get { return Get( () => SearchByPackageType, false ); }
		set { Set( () => SearchByPackageType, value ); }
	}


	public List<string> PackageTypes
	{
		get { return Get( () => PackageTypes, new List<string>() ); }
		set { Set( () => PackageTypes, value ); }
	}


	public List<string> SearchPackageTypes

	{
		get { return Get( () => SearchPackageTypes, new List<string>() ); }
		set { Set( () => SearchPackageTypes, value ); }
	}


	public string SelectedPackageType
	{
		get { return Get( () => SelectedPackageType, "" ); }
		set { Set( () => SelectedPackageType, value ); }
	}


	public bool SearchByServiceLevel
	{
		get { return Get( () => SearchByServiceLevel, false ); }
		set { Set( () => SearchByServiceLevel, value ); }
	}


	public string SelectedServiceLevel
	{
		get { return Get( () => SelectedServiceLevel, "" ); }
		set { Set( () => SelectedServiceLevel, value ); }
	}

	public string SelectedCallTakerId
	{
		get { return Get( () => SelectedCallTakerId, string.Empty ); }
		set { Set( () => SelectedCallTakerId, value ); }
	}


	public List<string> ServiceLevels
	{
		get { return Get( () => ServiceLevels, new List<string>() ); }
		set { Set( () => ServiceLevels, value ); }
	}


	public List<string> SearchServiceLevels
	{
		get { return Get( () => SearchServiceLevels, new List<string>() ); }
		set { Set( () => SearchServiceLevels, value ); }
	}


	public bool SearchButtonEnable
	{
		get { return Get( () => SearchButtonEnable, true ); }
		set { Set( () => SearchButtonEnable, value ); }
	}

	// Green == true
	public bool GreenRed
	{
		get { return Get( () => GreenRed, true ); }
		set { Set( () => GreenRed, value ); }
	}

	public bool DateEnable
	{
		get { return Get( () => DateEnable, true ); }
		set { Set( () => DateEnable, value ); }
	}


	public IList SelectedItems
	{
		get { return Get( () => SelectedItems, new List<object>() ); }
		set { Set( () => SelectedItems, value ); }
	}

	public ObservableCollection<DisplayTrip>? SelectedTrips
	{
		get { return Get( () => SelectedTrips, new ObservableCollection<DisplayTrip>() ); }
		set { Set( () => SelectedTrips, value ); }
	}

	public IList StartStatus
	{
		get { return Get( () => StartStatus, GetPublicStatusStrings() ); }
		set { Set( () => StartStatus, value ); }
	}


	public STATUS SelectedStartStatus
	{
		get { return Get( () => SelectedStartStatus, STATUS.UNSET ); }
		set { Set( () => SelectedStartStatus, value ); }

		//set { Set(() => SelectedStartStatus, value); }
		//set
		//{
		//    SelectedStartStatus = value;
		//    Console.WriteLine("SelectedStartStatus: set to " + SelectedStartStatus);
		//}
	}

	public string SelectedStartStatusString
	{
		get
		{
			return SelectedStartStatus.ToString();
		}
	}
	public IList EndStatus
	{
		get { return Get( () => EndStatus, GetPublicStatusStrings() ); }
		set { Set( () => EndStatus, value ); }
	}

	public STATUS SelectedEndStatus
	{
		get { return Get( () => SelectedEndStatus, STATUS.UNSET ); }
		set { Set( () => SelectedEndStatus, value ); }
	}

    public string SelectedEndStatusString
    {
        get
        {
            return SelectedEndStatus.ToString();
        }
    }

    public IList CompanyNames
	{
		get { return Get( () => CompanyNames, GetCompanyNames() ); }
		set { Set( () => CompanyNames, value ); }
	}


	public bool FilterByCompanyName
	{
		get { return Get( () => FilterByCompanyName, false ); }
		set { Set( () => FilterByCompanyName, value ); }

		//set
		//{
		//    //FilterByCompanyName = value;
		//    FilterByAddress = value;
		//}
	}

	public string SelectedCompanyName
	{
		get { return Get( () => SelectedCompanyName, "" ); }
		set { Set( () => SelectedCompanyName, value ); }
	}


	public bool EnableCsvCopy
	{
		get { return Get( () => EnableCsvCopy, false ); }
		set { Set( () => EnableCsvCopy, value ); }
	}

	public string TripAdhocFilter
	{
		get { return Get( () => TripAdhocFilter, "" ); }
		set { Set( () => TripAdhocFilter, value ); }
	}


	public bool FilterByAddressEnabled
	{
		get { return Get( () => FilterByAddressEnabled, false ); }
		set { Set( () => FilterByAddressEnabled, value ); }
	}


	public bool FilterByPUAddress
	{
		get { return Get( () => FilterByPUAddress, false ); }
		set { Set( () => FilterByPUAddress, value ); }
	}

	public bool FilterByDeliveryAddress
	{
		get { return Get( () => FilterByDeliveryAddress, false ); }
		set { Set( () => FilterByDeliveryAddress, value ); }
	}

	public IList CompanyAddressNames
	{
		//get { return Get(() => CompanyAddressNames, GetCompanyAddressDescriptions()); }
		get { return Get( () => CompanyAddressNames, new List<object>() ); }
		set { Set( () => CompanyAddressNames, value ); }
	}


	public string SelectedCompanyPUAddressName
	{
		get { return Get( () => SelectedCompanyPUAddressName, "" ); }
		set { Set( () => SelectedCompanyPUAddressName, value ); }
	}

	public string SelectedCompanyDelAddressName
	{
		get { return Get( () => SelectedCompanyDelAddressName, "" ); }
		set { Set( () => SelectedCompanyDelAddressName, value ); }
	}

	public string HelpUri
	{
		//get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638222337/search" ); }
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638124054/How+to+Search+Trip" ); }
	}

	public SearchTrips? View { get; set; } = null;

	public void SetView(SearchTrips? view )
	{
		View = view;
	}

    public GlobalAddressesModel GlobalAddressesModel
    {
        get => Get(() => GlobalAddressesModel, GlobalAddressesModel.GetInstance(this));
    }

    protected override void OnInitialised()
	{
		base.OnInitialised();
		Loaded = false;

		Task.Run( () =>
				  {
					  Task.WaitAll( Task.Run( GetServiceLevels ),
								    Task.Run( GetPackageTypes ),
								    Task.Run( () =>
										      {
											      GetCompanyNames();
										      } )
								  );

					  Dispatcher.Invoke( () =>
									     {
										     Loaded = true;

											 //view.Clear();
											 Execute_ClearSearchTrips();
										 } );
				  } );
	}

	private bool AccountAscending = true;

	private readonly List<string> ALL_COLUMNS = new()
												{
													"TripId", "AccountId", "DriverCode", "Reference", "CallTime", "PickupCompanyName", "PickupAddressCity", "PickupAddressAddressLine1",
													"DeliveryCompanyName", "DeliveryAddressCity", "DeliveryAddressAddressLine1", "ServiceLevel", "PackageType", "Pieces",
													"Weight", "ReadyTime", "DueTime", "Status1", "PickupTime", "DeliveryTime", "VerifiedTime", "DeliveryAddressRegion", "DeliveryAddressPostalCode",
													"PickupNotes", "PickupAddressNotes", "DeliveryNotes", "DeliveryAddressNotes", "BillingNotes"
												};

	private bool CallTimeAscending = true;

	private string CurrentSortName = SORT_CALLTIME;

	private bool DeliveryCityAscending = true;

	private bool DeliveryNameAscending = true;

	private bool DeliveryStateAscending = true;


	private bool DeliveryStreetAscending = true;

	private bool DeliveryTimeAscending = true;

	//private List<string> ActiveColumns = new List<string>();

	public Dictionary<string, bool> dictionaryActiveColumns = new();

	private static Dictionary<string, string> dictionaryColumns = new();

	private bool DriverAscending = true;

	private bool DueTimeAscending = true;


	private bool PackageTypeAscending = true;


	private bool PickupCityAscending = true;

	private bool PickupCompanyAscending = true;

	private bool PickupStateAscending = true;

	private bool PickupStreetAscending = true;

	private bool PickupTimeAscending = true;

	private bool PiecesAscending = true;

	private bool ReadyTimeAscending = true;

	private bool ReferenceAscending = true;

	private bool StatusAscending = true;

	private bool TripIdAscending = true;

	//public SearchTripsList UnFilteredTrips
	//{
	//    get { return Get(() => UnFilteredTrips, new SearchTripsList()); }
	//    set { Set(() => UnFilteredTrips, value); }
	//}
	private readonly List<DisplayTrip> UnFilteredTrips = new();

	private DispatcherTimer? updateAddressesTimer;

	private bool VerifiedTimeAscending = true;

	private bool WeightAscending = true;

	private static IList GetPublicStatusStrings()
	{
		var strings = new List<string>
					  {
						  STATUS.UNSET.AsString(),
						  STATUS.NEW.AsString(),
						  STATUS.ACTIVE.AsString(),
						  STATUS.DISPATCHED.AsString(),
						  STATUS.PICKED_UP.AsString(),
						  STATUS.DELIVERED.AsString(),
						  STATUS.VERIFIED.AsString(),
						  STATUS.POSTED.AsString(),
						  STATUS.INVOICED.AsString(),
						  STATUS.SCHEDULED.AsString(),
						  STATUS.FINALISED.AsString(),
						  STATUS.DELETED.AsString()
					  };

		//var values = Enum.GetValues(typeof(STATUS));
		//for (int i = 0; i < values.Length; i++)
		//{
		// //   strings.Add(TripExtensions.AsString(values[i]));
		//}

		return strings;
	}


	private IList GetCompanyNames()
	{
		var names = GlobalAddressesModel.CompanyNames;
		return names;
	}

	private static IList GetCompanyNames_V1()
	{
		//List<string> CompanyNames = new List<string>()
		//{
		//    "co 1",
		//    "co 2",
		//    "co 3",
		//    "co 4",
		//    "co 5"
		//};
		var CompanyNamesList = new List<string>();

		// CompanyNames = new List<string>();
		Task.Run( () =>
				  {
					  try
					  {
						  // NOTE If this is uncommented, no names appear
						  //Dispatcher.Invoke(() =>
						  //{
						  //    CompanyNames = new List<string>();
						  //});

						  //var Names = Client.RequestGetCustomerCodeList().Result;
						  // From Terry: Need to do this to avoid bad requests in the server log
						  CustomerCodeList? Names = null;

						  if( !IsInDesignMode )
							  Names = Azure.Client.RequestGetCustomerCodeList().Result;

						  if( Names is not null )
						  {
							  Dispatcher.Invoke( () =>
											     {
												     var Sd = new SortedDictionary<string, string>();

												     foreach( var Name in Names )

													     //Console.WriteLine("GetCompanyNames: " + Name);
													     Sd.Add( Name.ToLower(), Name );

												     foreach( var Key in Sd.Keys )
													     CompanyNamesList.Add( Sd[ Key ] );
											     } );
						  }
					  }
					  catch( Exception e )
					  {
						  //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
						  Logging.WriteLogLine( "Exception thrown: " + e, "GetCompanyNames", "SearchTripsModel" );
						  MessageBox.Show( "Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK );
					  }
				  } );

		return CompanyNamesList;
	}

	private void UpdateTrips( IEnumerable<DisplayTrip> trips )
	{
		Dispatcher.Invoke( () =>
						   {
							   Trips.Clear();

							   foreach( var Trip in trips )
								   Trips.Add( Trip );
						   } );
	}
			
	private void FilterTrips( string filter = "")
	{
		//Logging.WriteLogLine( "Filter: " + filter );

		if( !string.IsNullOrEmpty( filter ) )
		{
			filter = filter.ToLower();
			//List<DisplayTrip> filtered = (from T in UnFilteredTrips
			//                              where (T.CallTime.ToLower().Contains(filter)
			//                                  || T.DeliveryAddressCity.ToLower().Contains(filter)
			//                                  || T.DeliveryCompanyName.ToLower().Contains(filter)
			//                                  || T.DeliveryAddressRegion.ToLower().Contains(filter)
			//                                  || T.DeliveryAddressAddressLine1.ToLower().Contains(filter)
			//                                  || T.DeliveryTime.ToLower().Contains(filter)
			//                                  || T.PackageType.ToLower().Contains(filter)
			//                                  || T.PickupAddressCity.ToLower().Contains(filter)
			//                                  || T.PickupCompanyName.ToLower().Contains(filter)
			//                                  || T.PickupAddressRegion.ToLower().Contains(filter)
			//                                  || T.PickupAddressAddressLine1.ToLower().Contains(filter)
			//                                  || T.PickupTime.ToLower().Contains(filter)
			//                                  || T.Pieces.ToString().Contains(filter)
			//                                  || T.Reference.ToLower().Contains(filter)
			//                                  // NOTE the user will be searching for verified, not 6
			//                                  || T.Status1.ToString().ToLower().Contains(filter)
			//                                  || T.TripId.ToLower().Contains(filter)
			//                                  || T.Weight.ToString().Contains(filter)
			//                              )
			//                              orderby T.TripId
			//                              select T).ToList();

			var fieldsToSearch = GetActiveColumns();

			var filtered = new Dictionary<string, DisplayTrip>();

			foreach( var dt in UnFilteredTrips )
			{
				foreach( var field in fieldsToSearch )
				{
					//bool next = false;
					//Logging.WriteLogLine("Looking at field: " + field);

					var tmp = field switch
							  {
								  "TripId"     => dt.TripId.ToLower(),
								  "AccountId"  => dt.AccountId.ToLower(),
								  "DriverCode" => dt.Driver.ToLower(),
								  "Reference"  => dt.Reference.ToLower(),
								  "CallTime" =>

									  //tmp = dt.CallTime.ToLower();
									  dt.CallTime.ToLower(),
								  "PickupCompanyName"           => dt.PickupCompanyName.ToLower(),
								  "PickupAddressCity"           => dt.PickupAddressCity.ToLower(),
								  "PickupAddressAddressLine1"   => dt.PickupAddressAddressLine1.ToLower(),
								  "DeliveryCompanyName"         => dt.DeliveryCompanyName.ToLower(),
								  "DeliveryAddressCity"         => dt.DeliveryAddressCity.ToLower(),
								  "DeliveryAddressAddressLine1" => dt.DeliveryAddressAddressLine1.ToLower(),
								  "ServiceLevel"                => dt.ServiceLevel.ToLower(),
								  "PackageType"                 => dt.PackageType.ToLower(),
								  "Pieces"                      => dt.Pieces.ToString( CultureInfo.InvariantCulture ),
								  "Weight"                      => dt.Weight.ToString( CultureInfo.InvariantCulture ),
								  "ReadyTime"                   => dt.ReadyTime,
								  "DueTime"                     => dt.DueTime,
								  "Status1"                     => dt.Status1.ToString().ToLower(),
								  "PickupTime" =>

									  //tmp = dt.PickupTime;
									  dt.PickupTime,
								  "DeliveryTime" =>

									  //tmp = dt.DeliveryTime;
									  dt.DeliveryTime,
								  "DeliveryAddressRegion"     => dt.DeliveryAddressRegion.ToLower(),
								  "DeliveryAddressPostalCode" => dt.DeliveryAddressPostalCode.ToLower(),
								  "PickupNotes"               => dt.PickupNotes,
								  "PickupAddressNotes"        => dt.PickupAddressNotes,
								  "DeliveryNotes"             => dt.DeliveryNotes,
								  "DeliveryAddressNotes"      => dt.DeliveryAddressNotes,
								  "BillingNotes"              => dt.BillingNotes,
								  _                           => string.Empty
							  };

					if( tmp != string.Empty )
					{
						if( tmp.ToLower().Contains( filter ) )
						{
							var tripId = dt.TripId.ToLower();

							if( !filtered.ContainsKey( tripId ) )
								filtered.Add( tripId, dt );

							break;
						}
					}

					//this.ALL_COLUMNS;
				}
			}

			Trips.Clear();

			foreach( var trip in filtered.Values )
				Trips.Add( trip );
		}
		else
		{
			// Was a filter active?
			if( Trips.Count != UnFilteredTrips.Count )
			{
				//Console.WriteLine("SearchTripsModel.FilterTrips: Clearing filter");
				//Logging.WriteLogLine("Clearing filter", "FilterTrips", "SearchTripsModel");
				Logging.WriteLogLine( "Clearing filter" );

				// Filter is cleared
				Trips.Clear();

				foreach( var dt in UnFilteredTrips )
					Trips.Add( dt );
			}
		}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="cdl">
	/// </param>
	/// <param
	///     name="cbas">
	/// </param>
	/// <returns></returns>
	private static bool DoesCustomerCompaniesDetailedContainCompanySummaryListItem( CompanyDetailList cdl, CompanyByAccountSummary cbas )
	{
		var yes = false;

		foreach( var cd in cdl )
		{
			if( cd.Company.CompanyName == cbas.CompanyName )
			{
				yes = true;
				break;
			}
		}

		return yes;
	}

	private static IEnumerable<DisplayTrip> FixStatusAndBroadcast( List<DisplayTrip> changed )
	{
		foreach( var DisplayTrip in changed )
		{
			var Trip = (Trip)DisplayTrip;

			Trip.Status1                         = DisplayTrip.Status.ToProtocolStatus();
			DisplayTrip.BroadcastToDispatchBoard = true;
			DisplayTrip.BroadcastToDriver        = true;
			DisplayTrip.BroadcastToDriverBoard   = true;
		}
		return changed;
	}

	private List<string> GetGlobalAddresses()
	{
		var addresses = new List<string>();
		var global    = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;

		var result = Azure.Client.RequestGetCustomerCompaniesSummary( global ).Result;

		foreach( var address in result )
		{
			if( address.CompanyName != global )
				addresses.Add( address.CompanyName );
		}

		return addresses;
	}


	public void GetPackageTypes()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var Raw = Azure.Client.RequestGetPackageTypes().Result;

				if( Raw is not null )
				{
					var PTypes = ( from Pt in Raw
								   orderby Pt.SortOrder, Pt.Description
								   select Pt.Description ).ToList();

					Dispatcher.Invoke( () =>
									   {
										   PackageTypes = PTypes;

										   SearchPackageTypes = PTypes;
										   SearchPackageTypes.Insert( 0, "" ); // Make the first empty
									   } );
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown: " + e, "GetCompanyNames", "SearchTripsModel" );

				//MessageBox.Show("Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK);
			}
		}

		//return pts;
	}


	public void GetServiceLevels()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var sls = Azure.Client.RequestGetServiceLevelsDetailed().Result;

				if( sls != null )
				{
					var serviceLevels = ( from S in sls
										  orderby S.SortOrder, S.OldName
										  select S.OldName ).ToList();

					Dispatcher.Invoke( () =>
									   {
										   ServiceLevels = serviceLevels;

										   SearchServiceLevels = serviceLevels;
										   SearchServiceLevels.Insert( 0, "" ); // Make the first empty
									   } );
				}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown: " + e );

				//MessageBox.Show("Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK);
			}
		}
	}

	[DependsUpon( nameof( AsLocalTime ) )]
	public void WhenAsLocalTimeChanges()
	{
		Trips.ShowAsLocalTime = AsLocalTime;
	}

	[DependsUpon( nameof( Trips ) )]
	public void WhenTripsChange()
	{
		Trips.ShowAsLocalTime = AsLocalTime;
	}


	[DependsUpon( nameof( StartStatus ) )]
	public void WhenStartStatusChanges()
	{
		Console.WriteLine( "WhenStartStatusChanges" );
	}

	public STATUS GetStatusForString( string label ) => (STATUS)Enum.Parse( typeof( STATUS ), label );

	[DependsUpon( nameof( SelectedItems ), Delay = 100 )]
	public void WhenSelectedItemsChange()
	{
		EnableCsvCopy = SelectedItems.Count > 0;
	}

	[DependsUpon( nameof( SelectedTrips ), Delay = 100 )]
	public void WhenSelectedTripsChange()
	{
		EnableCsvCopy = SelectedTrips is not null && ( SelectedTrips.Count > 0 );
	}

	public void UpdateNewCompanyAddresses( string accountName )
	{
		if( accountName.IsNotNullOrWhiteSpace() && ( accountName.ToLower() == SelectedCompanyName.ToLower() ) )
		{
			Logging.WriteLogLine( "Reloading addresses for account: " + accountName );

			Task.Run( () =>
					  {
						  var tmp = GetCompanyAddressDescriptions();
						  CompanyAddressNames = tmp;
					  } );
		}

		//else
		//{
		//	Logging.WriteLogLine( "Account name: " + accountName + " isn't selected - not reloading addresses" );
		//}
	}

	public IList GetCompanyAddressDescriptions()
	{
		var cads = new List<string>();

		var CompanyName = SelectedCompanyName;

		if( CompanyName != string.Empty )
		{
			// i've turned this off and put the Task.Run into the caller - NOW it works
			//Task.Run(async () =>
			//{
			try
			{
				var result = Azure.Client.RequestGetCustomerCompaniesSummary( CompanyName ).Result;
				var global = GetGlobalAddresses();

				//var result = await Client.RequestGetCustomerCompaniesSummary(CompanyName);
				if( result is {Count: > 0} )
				{
					Logging.WriteLogLine( "Found " + result.Count + " companies for " + CompanyName );

					var Details = Azure.Client.RequestGetCustomerCompaniesDetailed( CompanyName ).Result;

					Dispatcher.Invoke( () =>
									   {
										   var sd = new SortedDictionary<string, string>();

										   // Needs check - throws exception
										   foreach( var cbas in result )
										   {
											   if( DoesCustomerCompaniesDetailedContainCompanySummaryListItem( Details, cbas ) )
											   {
												   if( !sd.ContainsKey( cbas.CompanyName.ToLower() ) )
													   sd.Add( cbas.CompanyName.ToLower(), cbas.CompanyName );
											   }
										   }

										   foreach( var name in global )
										   {
											   if( !sd.ContainsKey( name.ToLower() ) )
												   sd.Add( name.ToLower(), name );
										   }

										   foreach( var key in sd.Keys )
											   cads.Add( sd[ key ] );

										   //this.CompanyAddressDescriptions = cads;
										   //Utils.Logging.WriteLogLine("this.CompanyAddressNames.Count: " + this.CompanyAddressNames.Count);
									   } );
				}

				//CompanyByAccountAndCompanyNameList list = new CompanyByAccountAndCompanyNameList();
				//CompanyByAccountAndCompanyName tmp = new CompanyByAccountAndCompanyName
				//{
				//    CompanyName = CompanyName,
				//    CustomerCode = CompanyName
				//};
				//list.Add(tmp);

				//var result1 = Client.RequestGetCustomerCompanyAddressList(list).Result;
				//if (result1 != null)
				//{

				//}
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown fetching Company Addresses: " + e );
			}

			//});
		}

		return cads;
	}


	[DependsUpon( nameof( FromDate ) )]
	public void WhenFromDateChanges()
	{
		if( FromDate > ToDate )
			ToDate = FromDate;
	}

	[DependsUpon( nameof( ToDate ) )]
	public void WhenFromToChanges()
	{
		if( ToDate < FromDate )
			FromDate = ToDate;
	}


	[DependsUpon( nameof( SearchByClientReference ) )]
	[DependsUpon( nameof( SearchByTripId ) )]
	[DependsUpon( nameof( ClientReference ), Delay = 250 )]
	[DependsUpon( nameof( TripId ), Delay = 250 )]
	public void EnableSearchButton()
	{
		if( SearchByClientReference || SearchByTripId )
			SearchButtonEnable = !string.IsNullOrEmpty( ClientReference.Trim() ) || !string.IsNullOrEmpty( TripId.Trim() );
		else
			SearchButtonEnable = true;
	}

	[DependsUpon( nameof( SearchByClientReference ) )]
	public void WhenSearchByClientReferenceChanges()
	{
		if( SearchByClientReference )
			SearchByTripId = false;
	}


	[DependsUpon( nameof( SearchByTripId ) )]
	public void WhenSearchByTripIdChanges()
	{
		if( SearchByTripId )
			SearchByClientReference = false;
	}


	[DependsUpon( nameof( SearchByClientReference ) )]
	[DependsUpon( nameof( SearchByTripId ) )]
	public void NonDateSearch()
	{
		DateEnable = !SearchByClientReference && !SearchByTripId;
	}


	public void UpdateChangedTripsAsync( List<EditDisplayTrip> changed )
	{
		if( changed is {Count: > 0} )
		{
			var Changed = ( from C in changed
						    select (DisplayTrip)C ).ToList();

			try
			{
				Logging.WriteLogLine( "Updating database..." );

				Tasks.RunVoid( async () =>
							   {
								   foreach( var Trip in changed )
								   {
									   if( Trip.Driver.IsNotNullOrWhiteSpace() && ( Trip.Driver != Trip.OriginalDriver ) )
										   DriversBoardViewModel.AssignTripToNewDriver( Trip.Driver, Trip );
								   }

								   var tul = new TripUpdateList();
								   tul.Trips.AddRange( FixStatusAndBroadcast( Changed ) );

								   await Azure.Client.RequestAddUpdateTrips( tul );
							   } );

				//Execute_SearchTrips();
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown while updating trips: " + e );
			}

			Logging.WriteLogLine( "Updated database" );
		}
	}

	/// <summary>
	///     Replaces changed trips in Trips.
	///     Saves the changed trips to the db.
	/// </summary>
	/// <param
	///     name="changed">
	/// </param>
	public void UpdateChangedTripsAsync_V1( List<DisplayTrip> changed )

		//public Task UpdateChangedTripsAsync(ObservableCollection<DisplayTrip> changed)
		//public async Task UpdateChangedTripsAsync(ObservableCollection<DisplayTrip> changed)
	{
		// Update the UI
		//foreach( var trip in changed )
		//{
		//	var toDo = new Dictionary<int, DisplayTrip>();

		//	for( var i = 0; i < Trips.Count; i++ )
		//	{
		//		var dt = Trips[ i ];

		//		if( dt.TripId == trip.TripId )
		//		{
		//			Logging.WriteLogLine("trip.Status: " + trip.Status);
		//			toDo.Add( i, trip );

		//			break;
		//		}
		//	}

		//	foreach( int pos in toDo.Keys )
		//	{
		//		//Dispatcher.Invoke(() =>
		//		//{
		//		//	try
		//		//	{
		//		//		int at = pos;
		//		//		Logging.WriteLogLine("at: " + at);
		//		//		Trips.RemoveAt( at );
		//		//		Trips.Insert( at, trip );
		//		//		Logging.WriteLogLine("trip.Status: " + Trips[at].Status);
		//		//	}
		//		//	catch (Exception e)
		//		//	{
		//		//		Logging.WriteLogLine("Exception: " + e.ToString());
		//		//	}

		//		//});
		//		try
		//		{
		//			int at = pos;
		//			Logging.WriteLogLine("at: " + at);
		//			Trips.RemoveAt(at);
		//			Trips.Insert(at, trip);
		//			Logging.WriteLogLine("trip.Status: " + Trips[at].Status);
		//		}
		//		catch (Exception e)
		//		{
		//			Logging.WriteLogLine("Exception: " + e.ToString());
		//		}
		//	}
		//}

		// Save the trips

		try
		{
			Logging.WriteLogLine( "Updating database..." );

			Task.Run( async () =>
					  {
						  var tul = new TripUpdateList();
						  tul.Trips.AddRange( FixStatusAndBroadcast( changed ) );
						  await Azure.Client.RequestAddUpdateTrips( tul );
					  } );

			//Dispatcher.Invoke(() =>
			//{
			//	Task.Run(async () =>
			//	{
			//		var tul = new TripUpdateList();
			//		tul.Trips.AddRange(changed);
			//		await Azure.Client.RequestAddUpdateTrips(tul);
			//	});
			//	//_ = Azure.Client.RequestAddUpdateTrips(tul);
			//});
			//await Azure.Client.RequestAddUpdateTrips(tul);
		}
		catch( Exception e )
		{
			Logging.WriteLogLine( "Exception thrown while updating trips: " + e );
		}

		Logging.WriteLogLine( "Updated database" );
	}

	/// <summary>
	///     If ActiveColumns is empty, loads ALL_COLUMNS
	/// </summary>
	/// <returns></returns>
	public List<string> GetActiveColumns()
	{
		//if (this.ActiveColumns.Count == 0) {
		//    this.ActiveColumns = this.ALL_COLUMNS;

		//    // TESTING remove 1
		//    //this.ActiveColumns.RemoveAt(this.ActiveColumns.Count - 1);
		//}            
		//return this.ActiveColumns;
		var activeCols = new List<string>();

		if( dictionaryActiveColumns.Count == 0 )
		{
			foreach( var tmp in ALL_COLUMNS )
				dictionaryActiveColumns.Add( tmp, true );
		}

		foreach( var key in dictionaryActiveColumns.Keys )
		{
			if( dictionaryActiveColumns[ key ] )
				activeCols.Add( key );
		}

		return activeCols;
	}

	public void UpdateActiveColumns( string column )
	{
		if( !string.IsNullOrEmpty( column ) && dictionaryActiveColumns.ContainsKey( column ) )
		{
			Logging.WriteLogLine( "Toggling " + column, "UpdateActiveColumns", "SearchTripsModel" );
			dictionaryActiveColumns[ column ] = !dictionaryActiveColumns[ column ];
		}
	}

	/// <summary>
	///     Matches the column header to the field.
	///     TODO These will have to be updated when the missing fields
	///     are added to the table.
	/// </summary>
	/// <param
	///     name="column">
	/// </param>
	/// <returns></returns>
	public string MapColumnToTripField( string column )
	{
		var fieldName = string.Empty;

		if( dictionaryColumns.Count == 0 )
		{
			dictionaryColumns = new Dictionary<string, string>
								{
									{(string)Application.Current.TryFindResource( "SearchTripsListTripId" ), "TripId"},
									{(string)Application.Current.TryFindResource( "SearchTripsListAccount" ), "AccountId"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDriver" ), "DriverCode"},
									{(string)Application.Current.TryFindResource( "SearchTripsListClientReference" ), "Reference"},
									{(string)Application.Current.TryFindResource( "SearchTripsListCallTime" ), "CallTime"},
									{(string)Application.Current.TryFindResource( "SearchTripsListPickupName" ), "PickupCompanyName"},
									{(string)Application.Current.TryFindResource( "SearchTripsListPickupCity" ), "PickupAddressCity"},
									{(string)Application.Current.TryFindResource( "SearchTripsListPickupStreet" ), "PickupAddressAddressLine1"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDeliveryName" ), "DeliveryCompanyName"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDeliveryCity" ), "DeliveryAddressCity"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDeliveryStreet" ), "DeliveryAddressAddressLine1"},
									{(string)Application.Current.TryFindResource( "SearchTripsListServiceLevel" ), "ServiceLevel"},
									{(string)Application.Current.TryFindResource( "SearchTripsListPackageType" ), "PackageType"},
									{(string)Application.Current.TryFindResource( "SearchTripsListPieces" ), "Pieces"},
									{(string)Application.Current.TryFindResource( "SearchTripsListWeight" ), "Weight"},
									{(string)Application.Current.TryFindResource( "SearchTripsListReadyTime" ), "ReadyTime"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDueTime" ), "DueTime"},
									{(string)Application.Current.TryFindResource( "SearchTripsListStatus" ), "Status1"},
									{(string)Application.Current.TryFindResource( "SearchTripsListPickupTime" ), "PickupTime"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDeliveryTime" ), "DeliveryTime"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDeliveryState" ), "DeliveryAddressRegion"},
									{(string)Application.Current.TryFindResource( "SearchTripsListDeliveryZip" ), "DeliveryAddressPostalCode"}
								};
		}

		if( !string.IsNullOrEmpty( column ) )
			fieldName = dictionaryColumns[ column ];

		return fieldName;
	}

#region CSV / Clipboard
	public string CsvHeader
	{
		get { return Get( () => CsvHeader, header ); }
		set { Set( () => CsvHeader, value ); }
	}

	private readonly string header = "ShipmentId, CallTime, PickupCompanyName, PickupAddressStreet, PickupAddressCity, PickupAddressRegion, "
									 + "DeliveryCompanyName, DeliveryAddressStreet, DeliveryAddressCity, DeliveryAddressRegion, "
									 + "Pieces, Weight, PackageType, Reference, PickupTime, DeliveryTime, Status";

	private void CopyCsvToClipboard( string header )
	{
		//foreach( Trip T in SelectedItems )
		if( SelectedTrips is not null )
		{
			var Text = new StringBuilder( header );

			foreach( Trip T in SelectedTrips )
			{
				Text.Append( $"{T.TripId},{( T.CallTime ?? DateTimeOffset.MinValue ).ToTzDateTime()},{T.PickupCompanyName},{T.PickupAddressAddressLine1},{T.PickupAddressCity},{T.PickupAddressRegion},"
						     + $"{T.DeliveryCompanyName},{T.DeliveryAddressAddressLine1},{T.DeliveryAddressCity},{T.DeliveryAddressRegion},"
						     + $"{T.Pieces},{T.Weight},{T.PackageType},{T.Reference},{( T.PickupTime ?? DateTimeOffset.MinValue ).ToTzDateTime()},{( T.DeliveryTime ?? DateTimeOffset.MinValue ).ToTzDateTime()},{T.Status1.AsString()}\r\n"
						   );

				// These won't compile on my system
				/*
								Text.Append($"{T.TripId},{(T.CallTime != null ? T.CallTime : DateTimeOffset.MinValue).ToTzDateTime()},{T.PickupCompanyName},{T.PickupAddressAddressLine1},{T.PickupAddressCity},{T.PickupAddressRegion},"
										 + $"{T.DeliveryCompanyName},{T.DeliveryAddressAddressLine1},{T.DeliveryAddressCity},{T.DeliveryAddressRegion},"
										 + $"{T.Pieces},{T.Weight},{T.PackageType},{T.Reference},{(T.PickupTime != null ? T.PickupTime : DateTimeOffset.MinValue).ToTzDateTime()},{(T.DeliveryTime != null ? T.DeliveryTime : DateTimeOffset.MinValue).ToTzDateTime()},{T.Status1.AsString()}\r\n"
									   );
			*/
			}
			Clipboard.SetText( Text.ToString().TrimEnd() );
		}
	}

	public void Execute_CopyCsvToClipboard()
	{
		CopyCsvToClipboard( "" );
	}

	public void Execute_CopyCsvToClipboardWithHeader()
	{
		CopyCsvToClipboard( $"{CsvHeader}\r\n" );
	}

	public void Execute_CopyTripIdToClipboard()
	{
		//var Items = SelectedItems;
		var Items = SelectedTrips;

		if( Items is {Count: > 0} )
		{
			var Id = Items[ 0 ].TripId.Trim();
			Clipboard.SetText( Id );
		}
	}
#endregion

#region Execution
	public void Execute_SearchTrips(TripList trips)
	{
        void ProcessNotes()
        {
            foreach (var Trip in Trips)
            {
                if (DisplayNotesOnOneLine)
                {
                    // Had to do this in 2 passes because of Java line-endings
                    Trip.DisplayAccountNotes = Trip.BillingNotes.Replace("\r", " ");
                    Trip.DisplayAccountNotes = Trip.DisplayAccountNotes.Replace("\n", " ");
                    Trip.DisplayPickupAddressNotes = Trip.PickupAddressNotes.Replace("\r", " ");
                    Trip.DisplayPickupAddressNotes = Trip.DisplayPickupAddressNotes.Replace("\n", " ");
                    Trip.DisplayPickupNotes = Trip.PickupNotes.Replace("\r", " ");
                    Trip.DisplayPickupNotes = Trip.DisplayPickupNotes.Replace("\n", " ");
                    Trip.DisplayDeliveryAddressNotes = Trip.DeliveryAddressNotes.Replace("\r", " ");
                    Trip.DisplayDeliveryAddressNotes = Trip.DisplayDeliveryAddressNotes.Replace("\n", " ");
                    Trip.DisplayDeliveryNotes = Trip.DeliveryNotes.Replace("\r", " ");
                    Trip.DisplayDeliveryNotes = Trip.DisplayDeliveryNotes.Replace("\n", " ");
                }
                else
                {
                    Trip.DisplayAccountNotes = Trip.BillingNotes;
                    Trip.DisplayPickupAddressNotes = Trip.PickupAddressNotes;
                    Trip.DisplayPickupNotes = Trip.PickupNotes;
                    Trip.DisplayDeliveryAddressNotes = Trip.DeliveryAddressNotes;
                    Trip.DisplayDeliveryNotes = Trip.DeliveryNotes;
                }
            }
        }

        SearchButtonEnable = false;
		GreenRed           = false;
		TotalSearchTime    = "";
		var Start = DateTime.Now;
		//Trips.Clear();

		Task.Run( () =>
				  {
					  try
					  {
						  var Result = new SearchTripsList();

						  Dispatcher.Invoke( () =>
										     {
											     Trips = Result;
											     //Trips = new SearchTripsList();
											     TotalTrips = 0;
										     } );

						  //var Fd = FromDate;
						  //var Td = ToDate;
						  var Fd = FromDate.GetStartOfDay();
						  var Td = ToDate.GetEndOfDay();

						  var FromStatus = SelectedStartStatus;
						  var ToStatus   = SelectedEndStatus;

						  var PackageType  = SelectedPackageType;
						  var ServiceLevel = SelectedServiceLevel;

						  var SelectedCompany           = string.Empty;
						  var SelectedPUCompanyAddress  = string.Empty;
						  var SelectedDelCompanyAddress = string.Empty;

						  var message = "Searching for trips from " + Fd + " to " + Td
									    + ", FromStatus: " + FromStatus + ", ToStatus: " + ToStatus;

						  if( SearchByTripId )
							  message += ", TripId: " + TripId;

						  if( SearchByClientReference )
							  message += ", Reference: " + ClientReference;

						  if( FilterByCompanyName )
						  {
							  SelectedCompany =  SelectedCompanyName;
							  message         += ", SelectedCompany: " + SelectedCompany;

							  if( FilterByPUAddress && SelectedCompanyPUAddressName.IsNotNullOrEmpty() )
							  {
								  SelectedPUCompanyAddress =  SelectedCompanyPUAddressName;
								  message                  += ", SelectedCompanyPUAddressName: " + SelectedPUCompanyAddress;
							  }

							  if( FilterByDeliveryAddress && SelectedCompanyDelAddressName.IsNotNullOrEmpty() )
							  {
								  SelectedDelCompanyAddress =  SelectedCompanyDelAddressName;
								  message                   += ", SelectedCompanyDelAddressName: " + SelectedDelCompanyAddress;
							  }
						  }

						  Logging.WriteLogLine( message );

						  //	var DiffDays = ( Td - Fd ).TotalDays;
						  //	for( var I = 0; I < DiffDays; I++ )
						  //	{
						  //	Td = Fd.AddDays( 1 );

						  var Trps = Azure.Client.RequestSearchTrips( new Protocol.Data.SearchTrips
																      {
																	      UniqueUserId                  = UserName,
																	      FromDate                      = Fd,
																	      ToDate                        = Td,
																	      TripId                        = TripId,
																	      ByReference                   = SearchByClientReference,
																	      ByTripId                      = SearchByTripId,
																	      Reference                     = ClientReference,
																	      StartStatus                   = FromStatus,
																	      EndStatus                     = ToStatus,
																	      CompanyCode                   = SelectedCompany,
																	      SelectedCompanyPUAddressName  = SelectedPUCompanyAddress,
																	      SelectedCompanyDelAddressName = SelectedDelCompanyAddress,
																	      PackageType                   = PackageType,
																	      ServiceLevel                  = ServiceLevel,
																	      IncludeCharges                = true,
																	      ByPUTime                      = UsePUTime,
																	      ByDelTime                     = UseDelTime
																      } ).Result;

						  if( Trps is {Count: > 0} )
						  {
							  Dispatcher.Invoke( () =>
											     {
												     UnFilteredTrips.Clear();

												     //SearchTripsList Result = new();
												     foreach( var Trip in Trps )
												     {
														 //Logging.WriteLogLine("DEBUG TripId: " + Trip.TripId + " PickupTime: " + Trip.PickupTime + " VerifiedTime: " + Trip.VerifiedTime);
														 //Logging.WriteLogLine($"DEBUG Second Status (Invoiced): {Trip.Status2} Third Status: {Trip.Status3}");
													     // Kludge - There isn't support for these filters in the server call
													     if( UseVerTime )
													     {
														     // Filter out Verified times that are outside of range
														     //if (Trip.VerifiedTime < FromDate || Trip.VerifiedTime > ToDate)
														     if( ( Trip.VerifiedTime < Fd ) || ( Trip.VerifiedTime > Td ) )
														     {
															     //Logging.WriteLogLine("DEBUG Skipping trip " + Trip.TripId + ": VerifiedTime: " + Trip.VerifiedTime);
															     continue;
														     }
													     }

													     if( CheckCallTakerId( Trip ) )
													     {
														     var dt = new DisplayTrip( Trip );

														     Result.Add( dt );
														     UnFilteredTrips.Add( dt );
													     }
												     }
												     //Logging.WriteLogLine("About to assign to Trips");
												     Trips = Result;
													 ProcessNotes();

													 //Logging.WriteLogLine( "Done" );
													 TotalTrips = Result.Count;

												     var          Diff = DateTime.Now - Start;
												     const string FMT  = @"hh\:mm\:ss";

												     TotalSearchTime = $" ({Diff.ToString( FMT )})";

												     Logging.WriteLogLine( "Found " + TotalTrips + " trips in " + TotalSearchTime );
											     } );
						  }

						  //	Fd = Fd.AddDays( 1 );
						  //  }
					  }
					  catch( Exception e )
					  {
						  Logging.WriteLogLine( "ERROR searching for trips: " + e );
						  MessageBox.Show( "Error fetching trips\r\rTry a smaller date range", "Error", MessageBoxButton.OK );
					  }
					  finally
					  {
						  Dispatcher.Invoke( () =>
										     {
											     SearchButtonEnable = true;
											     GreenRed           = true;
										     } );
					  }
				  } );
	}

	private bool CheckCallTakerId( Trip trip )
	{
		var ok = true;

		if( SelectedCallTakerId.IsNotNullOrWhiteSpace() && !trip.CallTakerId.Trim().ToLower().Contains( SelectedCallTakerId.Trim().ToLower() ) )
			ok = false;

		return ok;
	}

	public void Execute_SearchTrips_Orig()
	{
		SearchButtonEnable = false;
		GreenRed           = false;
		TotalSearchTime    = "";
		var Start = DateTime.Now;

		Task.Run( () =>
				  {
					  try
					  {
						  var Result = new SearchTripsList();

						  Dispatcher.Invoke( () =>
										     {
											     Trips      = Result;
											     TotalTrips = 0;
										     } );

						  //var Fd = FromDate;
						  //var Td = ToDate;
						  var Fd = FromDate.GetStartOfDay();
						  var Td = ToDate.GetEndOfDay();

						  var FromStatus = SelectedStartStatus;
						  var ToStatus   = SelectedEndStatus;

						  var PackageType  = SelectedPackageType;
						  var ServiceLevel = SelectedServiceLevel;

						  var SelectedCompany           = string.Empty;
						  var SelectedPUCompanyAddress  = string.Empty;
						  var SelectedDelCompanyAddress = string.Empty;

						  var message = "Searching for trips from " + Fd + " to " + Td
									    + ", FromStatus: " + FromStatus + ", ToStatus: " + ToStatus;

						  if( SearchByTripId )
							  message += ", TripId: " + TripId;

						  if( SearchByClientReference )
							  message += ", Reference: " + ClientReference;

						  if( FilterByCompanyName )
						  {
							  SelectedCompany =  SelectedCompanyName;
							  message         += ", SelectedCompany: " + SelectedCompany;

							  if( FilterByPUAddress && SelectedCompanyPUAddressName.IsNotNullOrEmpty() )
							  {
								  SelectedPUCompanyAddress =  SelectedCompanyPUAddressName;
								  message                  += ", SelectedCompanyPUAddressName: " + SelectedPUCompanyAddress;
							  }

							  if( FilterByDeliveryAddress && SelectedCompanyDelAddressName.IsNotNullOrEmpty() )
							  {
								  SelectedDelCompanyAddress =  SelectedCompanyDelAddressName;
								  message                   += ", SelectedCompanyDelAddressName: " + SelectedDelCompanyAddress;
							  }
						  }

						  Logging.WriteLogLine( message );

						  //	var DiffDays = ( Td - Fd ).TotalDays;
						  //	for( var I = 0; I < DiffDays; I++ )
						  //	{
						  //	Td = Fd.AddDays( 1 );

						  var Trps = Azure.Client.RequestSearchTrips( new Protocol.Data.SearchTrips
																      {
																	      UniqueUserId                  = UserName,
																	      FromDate                      = Fd,
																	      ToDate                        = Td,
																	      TripId                        = TripId,
																	      ByReference                   = SearchByClientReference,
																	      ByTripId                      = SearchByTripId,
																	      Reference                     = ClientReference,
																	      StartStatus                   = FromStatus,
																	      EndStatus                     = ToStatus,
																	      CompanyCode                   = SelectedCompany,
																	      SelectedCompanyPUAddressName  = SelectedPUCompanyAddress,
																	      SelectedCompanyDelAddressName = SelectedDelCompanyAddress,
																	      PackageType                   = PackageType,
																	      ServiceLevel                  = ServiceLevel,
																	      IncludeCharges                = true,
																	      ByPUTime                      = UsePUTime,
																	      ByDelTime                     = UseDelTime
																      } ).Result;

						  if( Trps is {Count: > 0} )
						  {
							  Dispatcher.Invoke( () =>
											     {
												     UnFilteredTrips.Clear();

												     foreach( var Trip in Trps )
												     {
													     if( Trip.Signatures is {Count: > 0} )
													     {
														     Logging.WriteLogLine( "TripId: " + Trip.TripId + ", Trip.Signatures: " + Trip.Signatures.Count );

														     foreach( var sig in Trip.Signatures )
															     Logging.WriteLogLine( "\tsignature.Status: " + sig.Status );
													     }

													     var dt = new DisplayTrip( Trip );

													     //var dt = new DisplayTrip( Trip, new Boards.Common.ServiceLevel() );
													     Result.Add( dt );
													     UnFilteredTrips.Add( dt );
												     }

												     TotalTrips = Result.Count;

												     var          Diff = DateTime.Now - Start;
												     const string FMT  = @"hh\:mm\:ss";

												     TotalSearchTime = $" ({Diff.ToString( FMT )})";

												     Logging.WriteLogLine( "Found " + TotalTrips + " trips in " + TotalSearchTime );
											     } );
						  }

						  //	Fd = Fd.AddDays( 1 );
						  //  }
					  }
					  catch( Exception e )
					  {
						  Logging.WriteLogLine( "ERROR searching for trips: " + e );
						  MessageBox.Show( "Error fetching trips\r\rTry a smaller date range", "Error", MessageBoxButton.OK );
					  }
					  finally
					  {
						  Dispatcher.Invoke( () =>
										     {
											     SearchButtonEnable = true;
											     GreenRed           = true;
										     } );
					  }
				  } );
	}


	public void Execute_AuditTrip()
	{
		var Trip = SelectedTrip;

		if( Trip is not null )
			Globals.RunProgram( Globals.TRIP_AUDIT_TRAIL, Trip.TripId );
	}

	public void Execute_SortByTripId( string filter = "" )
	{
		CurrentSortName = SORT_TRIP_ID;
		TripIdAscending = !TripIdAscending;
		FilterTrips( filter );

		if( TripIdAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.TripId
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.TripId descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByAccount( string filter = "" )
	{
		CurrentSortName  = SORT_ACCOUNT;
		AccountAscending = !AccountAscending;
		FilterTrips( filter );

		if( AccountAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.AccountId
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.AccountId descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDriver( string filter = "" )
	{
		CurrentSortName = SORT_DRIVER;
		DriverAscending = !DriverAscending;
		FilterTrips( filter );

		if( DriverAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Driver
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Driver descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByCallTime( string filter = "" )
	{
		CurrentSortName   = SORT_CALLTIME;
		CallTimeAscending = !CallTimeAscending;
		FilterTrips( filter );

		if( CallTimeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.CallTime
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.CallTime descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPickupCompany( string filter = "" )
	{
		CurrentSortName        = SORT_PICKUP_COMPANY;
		PickupCompanyAscending = !PickupCompanyAscending;
		FilterTrips( filter );

		if( PickupCompanyAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupCompanyName
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupCompanyName descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPickupStreet( string filter = "" )
	{
		CurrentSortName       = SORT_PICKUP_STREET;
		PickupStreetAscending = !PickupStreetAscending;
		FilterTrips( filter );

		if( PickupStreetAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupAddressAddressLine1
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupAddressAddressLine1 descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPickupCity( string filter = "" )
	{
		CurrentSortName     = SORT_PICKUP_CITY;
		PickupCityAscending = !PickupCityAscending;
		FilterTrips( filter );

		if( PickupCityAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupAddressCity
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupAddressCity descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPickupState( string filter = "" )
	{
		CurrentSortName      = SORT_PICKUP_STATE;
		PickupStateAscending = !PickupStateAscending;
		FilterTrips( filter );

		if( PickupStateAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupAddressRegion
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupAddressRegion descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDeliveryName( string filter = "" )
	{
		CurrentSortName       = SORT_DELIVERY_NAME;
		DeliveryNameAscending = !DeliveryNameAscending;
		FilterTrips( filter );

		if( DeliveryNameAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryCompanyName
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryCompanyName descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDeliveryStreet( string filter = "" )
	{
		CurrentSortName         = SORT_DELIVERY_STREET;
		DeliveryStreetAscending = !DeliveryStreetAscending;
		FilterTrips( filter );

		if( DeliveryStreetAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryAddressAddressLine1
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryAddressAddressLine1 descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDeliveryCity( string filter = "" )
	{
		CurrentSortName       = SORT_DELIVERY_CITY;
		DeliveryCityAscending = !DeliveryCityAscending;
		FilterTrips( filter );

		if( DeliveryCityAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryAddressCity
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryAddressCity descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDeliveryState( string filter = "" )
	{
		CurrentSortName        = SORT_DELIVERY_STATE;
		DeliveryStateAscending = !DeliveryStateAscending;
		FilterTrips( filter );

		if( DeliveryStateAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryAddressRegion
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryAddressRegion descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPieces( string filter = "" )
	{
		CurrentSortName = SORT_PIECES;
		PiecesAscending = !PiecesAscending;
		FilterTrips( filter );

		if( PiecesAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Pieces
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Pieces descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByWeight( string filter = "" )
	{
		CurrentSortName = SORT_WEIGHT;
		WeightAscending = !WeightAscending;
		FilterTrips( filter );

		if( WeightAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Weight
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Weight descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPackageType( string filter = "" )
	{
		CurrentSortName      = SORT_PACKAGE_TYPE;
		PackageTypeAscending = !PackageTypeAscending;
		FilterTrips( filter );

		if( PackageTypeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PackageType
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PackageType descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByReference( string filter = "" )
	{
		CurrentSortName    = SORT_REFERENCE;
		ReferenceAscending = !ReferenceAscending;
		FilterTrips( filter );

		if( ReferenceAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Reference
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Reference descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByPickupTime( string filter = "" )
	{
		CurrentSortName     = SORT_PICKUP_TIME;
		PickupTimeAscending = !PickupTimeAscending;
		FilterTrips( filter );

		if( PickupTimeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupTime
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.PickupTime descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDeliveryTime( string filter = "" )
	{
		CurrentSortName       = SORT_DELIVERY_TIME;
		DeliveryTimeAscending = !DeliveryTimeAscending;
		FilterTrips( filter );

		if( DeliveryTimeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryTime
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DeliveryTime descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByStatus( string filter = "" )
	{
		CurrentSortName = SORT_STATUS;
		StatusAscending = !StatusAscending;
		FilterTrips( filter );

		if( StatusAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Status1
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.Status1 descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByReadyTime( string filter = "" )
	{
		CurrentSortName    = SORT_STATUS;
		ReadyTimeAscending = !ReadyTimeAscending;
		FilterTrips( filter );

		if( ReadyTimeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.ReadyTime
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.ReadyTime descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByDueTime( string filter = "" )
	{
		CurrentSortName  = SORT_DUETIME;
		DueTimeAscending = !DueTimeAscending;
		FilterTrips( filter );

		if( DueTimeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DueTime
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.DueTime descending
					       select T ).ToList() );
		}
	}

	public void Execute_SortByVerifiedTime( string filter = "" )
	{
		CurrentSortName       = SORT_VERIFIEDTIME;
		VerifiedTimeAscending = !VerifiedTimeAscending;
		FilterTrips( filter );

		if( VerifiedTimeAscending )
		{
			UpdateTrips( ( from T in Trips
					       orderby T.VerifiedTime
					       select T ).ToList() );
		}
		else
		{
			UpdateTrips( ( from T in Trips
					       orderby T.VerifiedTime descending
					       select T ).ToList() );
		}
	}

	/// <summary>
	///     Problem - need to reapply the sorting.
	/// </summary>
	/// <param
	///     name="filter">
	/// </param>
	public void Execute_Filter( string filter )
	{
		TripAdhocFilter = filter;

		if( filter != string.Empty )
			filter = filter.ToLower();

		switch( CurrentSortName )
		{
		// Keep the current sort direction by toggling the flag before calling the method
		case SORT_CALLTIME:
			CallTimeAscending = !CallTimeAscending;
			Execute_SortByCallTime( filter );

			break;

		case SORT_DELIVERY_CITY:
			DeliveryCityAscending = !DeliveryCityAscending;
			Execute_SortByDeliveryCity( filter );

			break;

		case SORT_DELIVERY_NAME:
			DeliveryNameAscending = !DeliveryNameAscending;
			Execute_SortByDeliveryName( filter );

			break;

		case SORT_DELIVERY_STATE:
			DeliveryStateAscending = !DeliveryStateAscending;
			Execute_SortByDeliveryState( filter );

			break;

		case SORT_DELIVERY_STREET:
			DeliveryStreetAscending = !DeliveryStreetAscending;
			Execute_SortByDeliveryStreet( filter );

			break;

		case SORT_DELIVERY_TIME:
			DeliveryTimeAscending = !DeliveryTimeAscending;
			Execute_SortByDeliveryTime( filter );

			break;

		case SORT_PACKAGE_TYPE:
			PackageTypeAscending = !PackageTypeAscending;
			Execute_SortByPackageType( filter );

			break;

		case SORT_PICKUP_CITY:
			PickupCityAscending = !PickupCityAscending;
			Execute_SortByPickupCity( filter );

			break;

		case SORT_PICKUP_COMPANY:
			PickupCompanyAscending = !PickupCompanyAscending;
			Execute_SortByPickupCompany( filter );

			break;

		case SORT_PICKUP_STATE:
			PickupStateAscending = !PickupStateAscending;
			Execute_SortByPickupState( filter );

			break;

		case SORT_PICKUP_STREET:
			PickupStreetAscending = !PickupStreetAscending;
			Execute_SortByPickupStreet( filter );

			break;

		case SORT_PICKUP_TIME:
			PickupTimeAscending = !PickupTimeAscending;
			Execute_SortByPickupTime( filter );

			break;

		case SORT_PIECES:
			PiecesAscending = !PiecesAscending;
			Execute_SortByPieces( filter );

			break;

		case SORT_REFERENCE:
			ReferenceAscending = !ReferenceAscending;
			Execute_SortByReference( filter );

			break;

		case SORT_STATUS:
			StatusAscending = !StatusAscending;
			Execute_SortByStatus( filter );

			break;

		case SORT_TRIP_ID:
			TripIdAscending = !TripIdAscending;
			Execute_SortByTripId( filter );

			break;

		case SORT_WEIGHT:
			WeightAscending = !WeightAscending;
			Execute_SortByWeight( filter );

			break;

		case SORT_VERIFIEDTIME:
			VerifiedTimeAscending = !VerifiedTimeAscending;
			Execute_SortByVerifiedTime( filter );

			break;
		}
	}

	public void Execute_ClearSearchTrips()
	{
		Logging.WriteLogLine( "Clearing Search Trips" );

		Trips.Clear();
		UnFilteredTrips.Clear();
		SearchByClientReference = false;
		ClientReference         = string.Empty;
		SearchByTripId          = false;
		TripId                  = string.Empty;

		SelectedPackageType  = string.Empty;
		SelectedServiceLevel = string.Empty;
		SelectedCallTakerId  = string.Empty;

		FilterByCompanyName     = false;
		FilterByPUAddress       = false;
		FilterByDeliveryAddress = false;
		FilterByAddressEnabled  = false;

		SelectedCompanyName           = string.Empty;
		SelectedCompanyPUAddressName  = string.Empty;
		SelectedCompanyDelAddressName = string.Empty;

		TotalSearchTime = string.Empty;
		TotalTrips      = 0;

		FromDate = DateTime.Now;
		ToDate   = DateTime.Now;

		TripAdhocFilter = string.Empty;

		SelectedStartStatus = STATUS.NEW;
		SelectedEndStatus = STATUS.VERIFIED;

		if (View != null)
		{
			View.CbStartStatus.SelectedIndex = ((int)SelectedStartStatus);
			View.CbEndStatus.SelectedIndex = ((int)SelectedEndStatus);
		}

		//FilterByAddress = false;
	}

	//public Dictionary<string, bool> Execute_SelectVisibleColumns()
	//{
	//    Dictionary<string, bool> mis = new Dictionary<string, bool>();
	//    Logging.WriteLogLine("Opening Select Visible Columns");

	//    foreach (string column in this.ALL_COLUMNS)
	//    {
	//        mis.Add(column, true);
	//        if (!this.ActiveColumns.Contains(column))
	//        {
	//            mis[column] = false;
	//        }
	//    }

	//    //foreach (string column in this.ALL_COLUMNS)
	//    //{
	//    //    System.Windows.Controls.MenuItem newMi = new System.Windows.Controls.MenuItem
	//    //    {
	//    //        Header = column,
	//    //        IsCheckable = true
	//    //    };
	//    //    newMi.IsChecked = this.ActiveColumns.Contains(column);
	//    //}


	//    return mis;
	//}

	public void Execute_EditSelectedTrips()
	{
	}

	public void UpdateTripsWithLatestVersion( DisplayTrip newVersion )
	{
		if( newVersion is not null )
		{
			var NewVersionTripId = newVersion.TripId;

			var C = UnFilteredTrips.Count;

			for( var i = 0; i < C; i++ )
			{
				if( UnFilteredTrips[ i ].TripId == NewVersionTripId )
				{
					UnFilteredTrips.RemoveAt( i );
					UnFilteredTrips.Insert( i, newVersion );

					break;
				}
			}

			// Update Display Trips
			C = Trips.Count;

			for( var I = 0; I < C; I++ )
			{
				if( Trips[ I ].TripId == NewVersionTripId )
				{
					Trips.RemoveAt( I );
					Trips.Insert( I, newVersion );
					break;
				}
			}
		}
	}
#endregion

#region Messaging
	public SearchTripsModel() : base( Messaging.SEARCH_TRIPS )
	{
	}

	protected override void OnUpdateTrip( Trip trip )
	{
		UpdateTripsWithLatestVersion( new DisplayTrip( trip ) );
	}

	protected override void OnRemoveTrip( string tripId )
	{
		foreach( var Trip in UnFilteredTrips )
		{
			if( Trip.TripId == tripId )
			{
				Trip.Status1 = STATUS.DELETED;
				UpdateTripsWithLatestVersion( Trip );
				break;
			}
		}
	}

	protected override void OnUpdateStatus( string tripId, STATUS status, STATUS1 status1, STATUS2 status2, bool receivedByDevice, bool readByDriver )
	{
		foreach( var Trip in UnFilteredTrips )
		{
			if( Trip.TripId == tripId )
			{
				Trip.Status1          = status;
				Trip.Status2          = status1;
				Trip.ReceivedByDevice = receivedByDevice;
				Trip.ReadByDriver     = readByDriver;
				UpdateTripsWithLatestVersion( Trip );
				break;
			}
		}
	}
#endregion

#region Address Timer
	public bool IsUpdateAddressesRunning() => updateAddressesTimer is not null && updateAddressesTimer.IsEnabled;

	/// <summary>
	///     Stops the update addresses timer.
	/// </summary>
	public void StopUpdateAddresses()
	{
		if( updateAddressesTimer != null )
		{
			Logging.WriteLogLine( "Stopping timer" );
			updateAddressesTimer.Stop();
		}
	}

	/// <summary>
	///     Starts the update trips timer.
	/// </summary>
	public void StartUpdateAddresses()
	{
		Logging.WriteLogLine( "Starting timer" );

		// var updateTripsDelegate = new TimerCallback(UpdateTripsChecker);
		// var updateTripsChecker = new UpdateTripsChecker();

		// Run every 15 seconds
		// timerUpdateTrips = new Timer(updateTripsChecker.UpdateTrips, null, 0, 15 * 1000);

		updateAddressesTimer          =  new DispatcherTimer();
		updateAddressesTimer.Tick     += UpdateAddresses_Tick;
		updateAddressesTimer.Interval =  new TimeSpan( 0, 0, 15 );
		updateAddressesTimer.Start();
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void UpdateAddresses_Tick( object sender, EventArgs e )
	{
		//Logging.WriteLogLine("UpdateAddresses fired");
		UpdateNewCompanyAddresses( SelectedCompanyName );
	}
	#endregion

	#region Settings

	private static readonly List<string> AllColumnsInOrder = new()
															 {
                                                                "SearchTripsListAccount",
                                                                "SearchTripsListDriver",
                                                                "SearchTripsListClientReference",
                                                                "SearchTripsListCallTime",
                                                                "SearchTripsListInvoice",
                                                                "SearchTripsListPickupName",
                                                                "SearchTripsListCurrentZone",
                                                                "SearchTripsListPickupCity",
                                                                "SearchTripsListPickupStreet",
                                                                "SearchTripsListDeliveryName",
                                                                "SearchTripsListDeliveryZone",
                                                                "SearchTripsListDeliveryCity",
                                                                "SearchTripsListDeliveryStreet",
                                                                "SearchTripsListPOD",
                                                                "SearchTripsListServiceLevel",
                                                                "SearchTripsListPackageType",
                                                                "SearchTripsListPieces",
                                                                "SearchTripsListWeight",
                                                                "SearchTripsListReadyTime",
                                                                "SearchTripsListDueTime",
                                                                "SearchTripsListStatus",
                                                                "SearchTripsListPickupTime",
                                                                "SearchTripsListDeliveryTime",
                                                                "SearchTripsListVerifiedTime",
                                                                "SearchTripsListDeliveryState",
                                                                "SearchTripsListDeliveryZip",
                                                                "SearchTripsListPickupNotes",
                                                                "SearchTripsListPickupAddressNotes",
                                                                "SearchTripsListDeliveryNotes",
                                                                "SearchTripsListDeliveryAddressNotes",
                                                                "SearchTripsListBillingNotes",
                                                                "SearchTripsListCallTakerId",
															};

    public List<string> GetAllColumnsInOrder() => AllColumnsInOrder;

    public readonly Dictionary<string, Visibility> DictionaryColumnVisibility = new()
                                                                                {
                                                                                    {"SearchTripsListAccount", Visibility.Hidden},
                                                                                    {"SearchTripsListDriver", Visibility.Hidden},
		                                                                            {"SearchTripsListClientReference", Visibility.Hidden},
		                                                                            {"SearchTripsListCallTime", Visibility.Hidden},   
		                                                                            {"SearchTripsListInvoice", Visibility.Hidden},    
		                                                                            {"SearchTripsListPickupName", Visibility.Hidden},          
		                                                                            {"SearchTripsListCurrentZone", Visibility.Hidden},
                                                                                    {"SearchTripsListPickupCity", Visibility.Hidden}, 
		                                                                            {"SearchTripsListPickupStreet", Visibility.Hidden},
		                                                                            {"SearchTripsListDeliveryName", Visibility.Hidden},      
		                                                                            {"SearchTripsListDeliveryZone", Visibility.Hidden},    
		                                                                            {"SearchTripsListDeliveryCity", Visibility.Hidden}, 
		                                                                            {"SearchTripsListDeliveryStreet", Visibility.Hidden},        
		                                                                            {"SearchTripsListPOD", Visibility.Hidden},                 
		                                                                            {"SearchTripsListServiceLevel", Visibility.Hidden},
                                                                                    {"SearchTripsListPackageType", Visibility.Hidden}, 
		                                                                            {"SearchTripsListPieces", Visibility.Hidden},
                                                                                    {"SearchTripsListWeight", Visibility.Hidden},
                                                                                    {"SearchTripsListReadyTime", Visibility.Hidden},    
		                                                                            {"SearchTripsListDueTime", Visibility.Hidden},      
		                                                                            {"SearchTripsListStatus", Visibility.Hidden},    
		                                                                            {"SearchTripsListPickupTime", Visibility.Hidden}, 
		                                                                            {"SearchTripsListDeliveryTime", Visibility.Hidden},        
		                                                                            {"SearchTripsListVerifiedTime", Visibility.Hidden},                   
		                                                                            {"SearchTripsListDeliveryState", Visibility.Hidden},
                                                                                    {"SearchTripsListDeliveryZip", Visibility.Hidden},
                                                                                    {"SearchTripsListPickupNotes", Visibility.Hidden},
                                                                                    {"SearchTripsListPickupAddressNotes", Visibility.Hidden},
                                                                                    {"SearchTripsListDeliveryNotes", Visibility.Hidden},
                                                                                    {"SearchTripsListDeliveryAddressNotes", Visibility.Hidden},
                                                                                    {"SearchTripsListBillingNotes", Visibility.Hidden},
                                                                                    {"SearchTripsListCallTakerId", Visibility.Hidden},
	                                                                            };

    public Visibility AccountVisibility
    {
        get { return Get(() => AccountVisibility, Visibility.Visible); }
        set { Set(() => AccountVisibility, value); }
    }

    public Visibility BillingNotesVisibility
    {
        get { return Get(() => BillingNotesVisibility, Visibility.Visible); }
        set { Set(() => BillingNotesVisibility, value); }
    }

    public Visibility CallTakerIdVisibility
    {
        get { return Get(() => CallTakerIdVisibility, Visibility.Visible); }
        set { Set(() => CallTakerIdVisibility, value); }
    }

    public Visibility CallTimeVisibility
    {
        get { return Get(() => CallTimeVisibility, Visibility.Visible); }
        set { Set(() => CallTimeVisibility, value); }
    }

    public Visibility CurrentZoneVisibility
    {
        get { return Get(() => CurrentZoneVisibility, Visibility.Visible); }
        set { Set(() => CurrentZoneVisibility, value); }
    }

    public Visibility DeliveryAddressAddressLine1Visibility
    {
        get { return Get(() => DeliveryAddressAddressLine1Visibility, Visibility.Visible); }
        set { Set(() => DeliveryAddressAddressLine1Visibility, value); }
    }

    public Visibility DeliveryAddressCityVisibility
    {
        get { return Get(() => DeliveryAddressCityVisibility, Visibility.Visible); }
        set { Set(() => DeliveryAddressCityVisibility, value); }
    }

    public Visibility DeliveryAddressNotesVisibility
    {
        get { return Get(() => DeliveryAddressNotesVisibility, Visibility.Visible); }
        set { Set(() => DeliveryAddressNotesVisibility, value); }
    }

    public Visibility DeliveryAddressPostalCodeVisibility
    {
        get { return Get(() => DeliveryAddressPostalCodeVisibility, Visibility.Visible); }
        set { Set(() => DeliveryAddressPostalCodeVisibility, value); }
    }

    public Visibility DeliveryAddressRegionVisibility
    {
        get { return Get(() => DeliveryAddressRegionVisibility, Visibility.Visible); }
        set { Set(() => DeliveryAddressRegionVisibility, value); }
    }

    public Visibility DeliveryCompanyNameVisibility
    {
        get { return Get(() => DeliveryCompanyNameVisibility, Visibility.Visible); }
        set { Set(() => DeliveryCompanyNameVisibility, value); }
    }

    public Visibility DeliveryNotesVisibility
    {
        get { return Get(() => DeliveryNotesVisibility, Visibility.Visible); }
        set { Set(() => DeliveryNotesVisibility, value); }
    }

    public Visibility DeliveryTimeVisibility
    {
        get { return Get(() => DeliveryTimeVisibility, Visibility.Visible); }
        set { Set(() => DeliveryTimeVisibility, value); }
    }

    public Visibility DeliveryZoneVisibility
    {
        get { return Get(() => DeliveryZoneVisibility, Visibility.Visible); }
        set { Set(() => DeliveryZoneVisibility, value); }
    }

    public Visibility DriverVisibility
    {
        get { return Get(() => DriverVisibility, Visibility.Visible); }
        set { Set(() => DriverVisibility, value); }
    }

    public Visibility DueTimeVisibility
    {
        get { return Get(() => DueTimeVisibility, Visibility.Visible); }
        set { Set(() => DueTimeVisibility, value); }
    }

    public Visibility InvoiceVisibility
    {
        get { return Get(() => InvoiceVisibility, Visibility.Visible); }
        set { Set(() => InvoiceVisibility, value); }
    }

    public Visibility PODVisibility
    {
        get { return Get(() => PODVisibility, Visibility.Visible); }
        set { Set(() => PODVisibility, value); }
    }

    public Visibility PackageTypeVisibility
    {
        get { return Get(() => PackageTypeVisibility, Visibility.Visible); }
        set { Set(() => PackageTypeVisibility, value); }
    }

    public Visibility PickupAddressAddressLine1Visibility
    {
        get { return Get(() => PickupAddressAddressLine1Visibility, Visibility.Visible); }
        set { Set(() => PickupAddressAddressLine1Visibility, value); }
    }

    public Visibility PickupAddressCityVisibility
    {
        get { return Get(() => PickupAddressCityVisibility, Visibility.Visible); }
        set { Set(() => PickupAddressCityVisibility, value); }
    }

    public Visibility PickupAddressNotesVisibility
    {
        get { return Get(() => PickupAddressNotesVisibility, Visibility.Visible); }
        set { Set(() => PickupAddressNotesVisibility, value); }
    }

    public Visibility PickupAddressPostalCodeVisibility
    {
        get { return Get(() => PickupAddressPostalCodeVisibility, Visibility.Visible); }
        set { Set(() => PickupAddressPostalCodeVisibility, value); }
    }

    public Visibility PickupAddressRegionVisibility
    {
        get { return Get(() => PickupAddressRegionVisibility, Visibility.Visible); }
        set { Set(() => PickupAddressRegionVisibility, value); }
    }

    public Visibility PickupCompanyNameVisibility
    {
        get { return Get(() => PickupCompanyNameVisibility, Visibility.Visible); }
        set { Set(() => PickupCompanyNameVisibility, value); }
    }

    public Visibility PickupNotesVisibility
    {
        get { return Get(() => PickupNotesVisibility, Visibility.Visible); }
        set { Set(() => PickupNotesVisibility, value); }
    }

    public Visibility PickupTimeVisibility
    {
        get { return Get(() => PickupTimeVisibility, Visibility.Visible); }
        set { Set(() => PickupTimeVisibility, value); }
    }

    public Visibility PickupZoneVisibility
    {
        get { return Get(() => PickupZoneVisibility, Visibility.Visible); }
        set { Set(() => PickupZoneVisibility, value); }
    }

    public Visibility PiecesVisibility
    {
        get { return Get(() => PiecesVisibility, Visibility.Visible); }
        set { Set(() => PiecesVisibility, value); }
    }

    public Visibility ReadyTimeVisibility
    {
        get { return Get(() => ReadyTimeVisibility, Visibility.Visible); }
        set { Set(() => ReadyTimeVisibility, value); }
    }

    public Visibility ReferenceVisibility
    {
        get { return Get(() => ReferenceVisibility, Visibility.Visible); }
        set { Set(() => ReferenceVisibility, value); }
    }

    public Visibility ServiceLevelVisibility
    {
        get { return Get(() => ServiceLevelVisibility, Visibility.Visible); }
        set { Set(() => ServiceLevelVisibility, value); }
    }

    public Visibility Status1Visibility
    {
        get { return Get(() => Status1Visibility, Visibility.Visible); }
        set { Set(() => Status1Visibility, value); }
    }

    public Visibility VerifiedTimeVisibility
    {
        get { return Get(() => VerifiedTimeVisibility, Visibility.Visible); }
        set { Set(() => VerifiedTimeVisibility, value); }
    }

    public Visibility WeightVisibility
    {
        get { return Get(() => WeightVisibility, Visibility.Visible); }
        set { Set(() => WeightVisibility, value); }
    }

    public void SaveSearchColumns( List<string> newColumns )
	{
		List<string> keys = new();

		foreach( var key in DictionaryColumnVisibility.Keys )
			keys.Add( key );

		foreach( var key in keys )
		{
			if( newColumns.Contains( key ) )
			{
				//Logging.WriteLogLine("Setting column :" + key + " to Visible");
				DictionaryColumnVisibility[ key ] = Visibility.Visible;
			}
			else
			{
				//Logging.WriteLogLine("Setting column :" + key + " to Hidden");
				DictionaryColumnVisibility[ key ] = Visibility.Hidden;
			}
		}

		foreach( var key in keys )
		{
			switch( key )
			{
				case "SearchTripsListTripId":
					break;
				case "SearchTripsListAccount":
					AccountVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListClientReference":
					ReferenceVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListCallTime":
					CallTimeVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListInvoice":
					InvoiceVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPickupName":
					PickupCompanyNameVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListCurrentZone":
					CurrentZoneVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPickupCity":
					PickupAddressCityVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPickupStreet":
					PickupAddressAddressLine1Visibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryName":
					DeliveryCompanyNameVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryZone":
					DeliveryZoneVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListDeliveryCity":
					DeliveryAddressCityVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryStreet":
					DeliveryAddressAddressLine1Visibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPOD":
					PODVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListServiceLevel":
					ServiceLevelVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListPackageType":
					PackageTypeVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPieces":
					PiecesVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListWeight":
					WeightVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListReadyTime":
					ReadyTimeVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListDueTime":
					DueTimeVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListStatus":
					Status1Visibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPickupTime":
					PickupTimeVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryTime":
					DeliveryTimeVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListVerifiedTime":
					VerifiedTimeVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryState":
					DeliveryAddressRegionVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryZip":
					DeliveryAddressPostalCodeVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListPickupNotes":
					PickupNotesVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListPickupAddressNotes":
					PickupAddressNotesVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryNotes":
					DeliveryNotesVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListDeliveryAddressNotes":
					DeliveryAddressNotesVisibility = DictionaryColumnVisibility[ key ];
                    break;
				case "SearchTripsListBillingNotes":
					BillingNotesVisibility = DictionaryColumnVisibility[ key ];
					break;
				case "SearchTripsListCallTakerId":
					CallTakerIdVisibility = DictionaryColumnVisibility[ key ];
                    break;
			}
		}

		ColumnSettings cs = new()
		                    {
			                    Columns   = newColumns
		                    };

		//if( SearchSettings == null )
		//	SearchSettings = new Settings();
		SearchSettings ??= new();

		//if( SearchSettings.SearchColumnSettings.ContainsKey( BoardName ) )
		//	SearchSettings.SearchColumnSettings[ BoardName ] = cs;
		//else
		//	SearchSettings.SearchColumnSettings.Add( BoardName, cs );
		SearchSettings.Columns = cs;
		SearchSettings.OtherSettings.ShowNotesOnOneLine = DisplayNotesOnOneLine;

		SaveSettings( SearchSettings );

		View?.ApplyActiveColumns();
	}

	public bool DisplayNotesOnOneLine
	{
		get => Get(() => DisplayNotesOnOneLine, false);
		set => Set(() => DisplayNotesOnOneLine, value);
	}

	public void SaveOtherSettings()
	{
		MiscSettings ??= new();
		MiscSettings.ShowNotesOnOneLine = DisplayNotesOnOneLine;
		SearchSettings ??= new();
		SearchSettings.OtherSettings = MiscSettings;

		View?.BtnSaveColumns_Click(null, null);
	}

    #region Settings Persistance

    public class ColumnSettings    
	{        
		public List<string> Columns { get; set; } = new();    
	}

    public class OtherSettings
    {
        public bool ShowNotesOnOneLine { get; set; }
    }

    public class Settings
	{
		public ColumnSettings Columns { get; set; } = new();
        public OtherSettings OtherSettings { get; set; } = new();
	}

    public new void LoadSettings()
    {
        if (!IsInDesignMode)
        {
            try
            {
                var FileName = MakeSettingsFileName();

                if (System.IO.File.Exists(FileName))
                {
                    Logging.WriteLogLine("Loading settings from " + FileName);
                    var SerializedObject = System.IO.File.ReadAllText(FileName);
                    //SerializedObject = Encryption.Decrypt(SerializedObject);
                    SearchSettings = JsonConvert.DeserializeObject<Settings>(SerializedObject);

                    if (SearchSettings is not null)
                    {
                        ColumnSettings? activeCols = SearchSettings.Columns;

						if (activeCols == null)
						{
							Logging.WriteLogLine("ColumnSettings is empty - loading all columns");
                            activeCols = new()
                            {
                                Columns = GetAllColumnsInOrder()
                            };
                        }

						foreach (var col in activeCols.Columns)
						{
							if (!DictionaryColumnVisibility.ContainsKey(col))
								DictionaryColumnVisibility.Add(col, Visibility.Visible);
							else
								DictionaryColumnVisibility[col] = Visibility.Visible;
						}

						foreach (string key in DictionaryColumnVisibility.Keys)
						{
							if (DictionaryColumnVisibility[key] != Visibility.Visible)
							{
								Logging.WriteLogLine("DEBUG Column " + key + " is " + DictionaryColumnVisibility[key]);
							}
						}

						MiscSettings = SearchSettings.OtherSettings;
						if (MiscSettings != null)
						{
							Logging.WriteLogLine($"Setting DisplayNotesOnOneLine to {MiscSettings.ShowNotesOnOneLine}");
							DisplayNotesOnOneLine = MiscSettings.ShowNotesOnOneLine;
						}
                    }
					else
					{
						SearchSettings = new();
					}
                }
                else
                {
                    var activeCols = GetAllColumnsInOrder();

                    foreach (var col in activeCols)
                    {
                        if (!DictionaryColumnVisibility.ContainsKey(col))
                            DictionaryColumnVisibility[col] = Visibility.Visible;
                        else
                            DictionaryColumnVisibility[col] = Visibility.Visible;
                    }
					SearchSettings = new();

                }
            }
            catch (Exception e)
            {
                Logging.WriteLogLine("ERROR: unable to load settings: " + e);

                // Doing this to ensure that the board will load if the file is corrupted
                Logging.WriteLogLine("Deleting bad settings file for Search");
                SearchSettings = new();
                SaveSettings(SearchSettings);
            }
        }
    }

    private void SaveSettings(Settings settings)
    {
        if (!IsInDesignMode)
        {
            try
            {
                var SerializedObject = JsonConvert.SerializeObject(settings);

                //Logging.WriteLogLine("Saved settings:\n" + SerializedObject);
                //SerializedObject = Encryption.Encrypt(SerializedObject);
                var fileName = MakeSettingsFileName();
                Logging.WriteLogLine("Saving settings to " + fileName);
                System.IO.File.WriteAllText(fileName, SerializedObject);
            }
            catch (Exception e)
            {
                Logging.WriteLogLine("ERROR: unable to save settings: " + e);
            }
        }
    }

	private string MakeSettingsFileName()
    {
        var AppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        var BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd('\\');

        if (!Directory.Exists(BaseFolder))
            Directory.CreateDirectory(BaseFolder);

        return $"{BaseFolder}\\Search.{SETTINGS_FILE_EXTENSION}";
    }
    #endregion

    #endregion
}