﻿using System.Windows.Controls;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.SearchTrips;

/// <summary>
///     Interaction logic for EditSelectedTrips.xaml
/// </summary>
public partial class EditSelectedTrips : Window
{
	public class EditDisplayTrip : DisplayTrip
	{
		public readonly string OriginalDriver;

		public EditDisplayTrip( Boards.Common.DisplayTrip trip ) : base( trip )
		{
			OriginalDriver = trip.Driver;
		}
	}

	public List<EditDisplayTrip> ShowEditSelectedTrips( List<EditDisplayTrip> trips )
	{
		if( DataContext is EditSelectedTripsModel Model )
		{
			Model.Trips = new List<EditDisplayTrip>( trips );
			ShowDialog();

			if( Model.AnyTripsChanged )
				return Model.Trips;
			return null;
		}
		return null;
	}

	public EditSelectedTrips( Window owner, IEnumerable<EditDisplayTrip> trips )
	{
		Owner = owner;
		InitializeComponent();

		if( DataContext is EditSelectedTripsModel Model )
			Model.Trips = new List<EditDisplayTrip>( trips );
	}

	/// <summary>
	///     Ok
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditSelectedTripsModel Model )
		{
			var proceed = true;

			// Check that a driver has been selected
			if( Model.IsStatusSelected )
			{
				if( ( Model.SelectedStatus == Boards.Common.DisplayTrip.STATUS.DISPATCHED.AsString() )
				    || ( Model.SelectedStatus == Boards.Common.DisplayTrip.STATUS.PICKED_UP.AsString() )
				    || ( Model.SelectedStatus == Boards.Common.DisplayTrip.STATUS.DELIVERED.AsString() )
				    || ( Model.SelectedStatus == Boards.Common.DisplayTrip.STATUS.VERIFIED.AsString() ) )
				{
					//var doAllTripsHaveDrivers = false;
					//var doNoTripsHaveDrivers = false;

					var noDrivers = new List<EditDisplayTrip>();

					// If a driver has been selected, leave it
					if( !Model.IsDriverSelected )
					{
						foreach( var trip in Model.Trips )
						{
							if( trip.Driver.Trim() == string.Empty )
								noDrivers.Add( trip );
						}
					}

					if( noDrivers.Count == Model.Trips.Count )
					{
						// Tell the user to select a driver
						var message = FindStringResource( "EditSelectedTripsErrorStatusChangedNoDriverSelected" );
						message = message.Replace( "@1", Model.SelectedStatus );
						MessageBox.Show( message, FindStringResource( "EditSelectedTripsErrorStatusTitle" ), MessageBoxButton.OK, MessageBoxImage.Error );
						proceed = false;
					}
					else if( noDrivers.Count > 0 )
					{
						// At least 1 is missing a driver
						var message = FindStringResource( "EditSelectedTripsErrorStatusChangedNoDriverNotUpdated" );
						message = message.Replace( "@1", Model.SelectedStatus );

						foreach( var trip in noDrivers )
						{
							Logging.WriteLogLine( "NOT updating trip because there is no driver: " + trip.TripId );
							message += "\n\tTripId: " + trip.TripId + " - Company: " + trip.BillingCompanyName;
							// Remove them from the selected trips
							Model.Trips.Remove( trip );
						}

						MessageBox.Show( message, FindStringResource( "EditSelectedTripsWarningStatusTitle" ), MessageBoxButton.OK, MessageBoxImage.Error );
					}
				}
			}

			if( proceed )
			{
				Model.UpdateTrips();

				Close();
			}
		}
	}

	/// <summary>
	///     Cancel
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_1( object sender, RoutedEventArgs e )
	{
		if( DataContext is EditSelectedTripsModel Model )
		{
			Model.Trips.Clear();

			Model.SetApplyButtonState();
		}
		Close();
	}

	/// <summary>
	///     Selects Driver.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_2( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model )
		{
			if( comboDrivers.IsEnabled )
			{
				b.Content              = FindStringResource( "EditSelectedTripsSelected" );
				comboDrivers.IsEnabled = false;
				Model.SelectedDriver   = (string)comboDrivers.SelectedItem;
				Model.IsDriverSelected = true;

				b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content              = FindStringResource( "EditSelectedTripsSelect" );
				comboDrivers.IsEnabled = true;
				Model.SelectedDriver   = null;
				Model.IsDriverSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}

	/// <summary>
	///     Selects POD.
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_3( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model )
		{
			if( tbPOD.IsEnabled )
			{
				b.Content           = FindStringResource( "EditSelectedTripsSelected" );
				tbPOD.IsEnabled     = false;
				Model.SelectedPOD   = tbPOD.Text.Trim();
				Model.IsPODSelected = true;

                b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content           = FindStringResource( "EditSelectedTripsSelect" );
				tbPOD.IsEnabled     = true;
				Model.SelectedPOD   = null;
				Model.IsPODSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}

	/// <summary>
	///     Selects T-PU
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_4( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model && dtpPU.Value != null )
		{
			if( dtpPU.IsEnabled )
			{
				b.Content           = FindStringResource( "EditSelectedTripsSelected" );
				dtpPU.IsEnabled     = false;
				Model.SelectedTPu   = (DateTime)dtpPU.Value;
				Model.IsTPuSelected = true;

                b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content           = FindStringResource( "EditSelectedTripsSelect" );
				dtpPU.IsEnabled     = true;
				Model.SelectedTPu   = DateTime.MinValue;
				Model.IsTPuSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}

	/// <summary>
	///     Selects T-DEL
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_5( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model && dtpDel.Value != null )
		{
			if( dtpDel.IsEnabled )
			{
				b.Content            = FindStringResource( "EditSelectedTripsSelected" );
				dtpDel.IsEnabled     = false;
				Model.SelectedTDel   = (DateTime)dtpDel.Value;
				Model.IsTDelSelected = true;

                b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content            = FindStringResource( "EditSelectedTripsSelect" );
				dtpDel.IsEnabled     = true;
				Model.SelectedTDel   = DateTime.MinValue;
				Model.IsTDelSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}

	/// <summary>
	///     Selects CallTime
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_6( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model && dtpCallTime.Value != null )
		{
			if( dtpCallTime.IsEnabled )
			{
				b.Content                = FindStringResource( "EditSelectedTripsSelected" );
				dtpCallTime.IsEnabled    = false;
				Model.SelectedCallTime   = (DateTime)dtpCallTime.Value;
				Model.IsCallTimeSelected = true;

                b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content                = FindStringResource( "EditSelectedTripsSelect" );
				dtpCallTime.IsEnabled    = true;
				Model.SelectedCallTime   = DateTime.MinValue;
				Model.IsCallTimeSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}

	/// <summary>
	///     POP
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_7( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model )
		{
			if( tbPOP.IsEnabled )
			{
				b.Content           = FindStringResource( "EditSelectedTripsSelected" );
				tbPOP.IsEnabled     = false;
				Model.SelectedPOP   = tbPOP.Text.Trim();
				Model.IsPOPSelected = true;

                b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content           = FindStringResource( "EditSelectedTripsSelect" );
				tbPOP.IsEnabled     = true;
				Model.SelectedPOP   = null;
				Model.IsPOPSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}

	/// <summary>
	/// </summary>
	/// <param
	///     name="name">
	/// </param>
	/// <returns></returns>
	private string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	/// <summary>
	///     Status
	/// </summary>
	/// <param
	///     name="sender">
	/// </param>
	/// <param
	///     name="e">
	/// </param>
	private void Button_Click_8( object sender, RoutedEventArgs e )
	{
		if( sender is Button b && DataContext is EditSelectedTripsModel Model )
		{
			if( cbStatus.IsEnabled )
			{
				b.Content              = FindStringResource( "EditSelectedTripsSelected" );
				cbStatus.IsEnabled     = false;
				Model.SelectedStatus   = (string)cbStatus.SelectedItem;
				Model.IsStatusSelected = true;

                b.Background = Brushes.LightGray;
			}
			else
			{
				b.Content              = FindStringResource( "EditSelectedTripsSelect" );
				cbStatus.IsEnabled     = true;
				cbStatus.SelectedIndex = 0;
				Model.SelectedStatus   = null;
				Model.IsStatusSelected = false;

                b.Background = Brushes.White;
			}
			Model.SetApplyButtonState();
		}
	}
}