﻿namespace ViewModels.Reporting;

public partial class DataSets
{
	public class TrackingDetails
	{
		public string TripId      => "Detailed Trip Information for Trip " + trip.TripId;
		public string DateOfEvent => date.ToString( "MM/dd/yyyy" );
		public string TimeOfEvent => date.ToString( "HH:mm" );
		public string Location    { get; }

		public string Description { get; }

		private readonly Trip           trip;
		private          DateTimeOffset date;

		// private Dictionary<DateTimeOffset, string> events;
		//private List<string> events = new List<string>();

		public TrackingDetails( Trip trip, DateTimeOffset date, string location, string description )
		{
			this.trip   = trip;
			this.date   = date;
			Location    = location;
			Description = description;
		}
	}

	/// <summary>
	///     Terry suggested this
	/// </summary>
	public class TrackingDetailsList : List<TrackingDetails>
	{
		public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

		public TrackingDetailsList( Trip trip )
		{
			if( trip != null )
				BuildEvents( trip );
		}


		/// <summary>
		///     Creates the lines for display.
		/// </summary>
		private void BuildEvents( Trip trip )
		{
			//StringBuilder line = new StringBuilder();
			if( trip.CallTime > DateTimeOffset.MinValue )
				Add( new TrackingDetails( trip, trip.CallTime ?? DateTimeOffset.MinValue, FormatAddress( trip.PickupCompanyName, trip.PickupAddressCity, trip.PickupAddressRegion ), FindStringResource( "DialoguesTrackingDialogShippingDetailsSubmitted" ) ) );

			if( trip.ReadyTime > DateTimeOffset.MinValue )
				Add( new TrackingDetails( trip, trip.ReadyTime ?? DateTimeOffset.MinValue, FormatAddress( trip.PickupCompanyName, trip.PickupAddressCity, trip.PickupAddressRegion ), FindStringResource( "DialoguesTrackingDialogTripReadyForPickup" ) ) );

			// TODO
			//if (trip.DriverAssignTime > DateTimeOffset.MinValue)
			//{
			//    line.Clear();
			//    line.Append(trip.DriverAssignTime.ToString("yyyy-MM-dd"));
			//    line.Append("\t").Append(trip.DriverAssignTime.ToString("HH-mm"));
			//    events.Add(line.ToString());
			//}
			if( ( trip.Status1 >= STATUS.PICKED_UP ) && ( trip.Status1 < STATUS.DELETED ) )
				Add( new TrackingDetails( trip, trip.PickupTime ?? DateTimeOffset.MinValue, FormatAddress( trip.PickupCompanyName, trip.PickupAddressCity, trip.PickupAddressRegion ), FindStringResource( "DialoguesTrackingDialogShipmentPickedUp" ) ) );

			if( ( trip.Status1 >= STATUS.DELIVERED ) && ( trip.Status1 < STATUS.DELETED ) )
				Add( new TrackingDetails( trip, trip.DeliveryTime ?? DateTimeOffset.MinValue, FormatAddress( trip.DeliveryCompanyName, trip.DeliveryAddressCity, trip.DeliveryAddressRegion ), FindStringResource( "DialoguesTrackingDialogShipmentDelivered" ) ) );

			if( ( trip.Status1 >= STATUS.VERIFIED ) && ( trip.Status1 < STATUS.DELETED ) )
				Add( new TrackingDetails( trip, trip.VerifiedTime, FormatAddress( trip.DeliveryCompanyName, trip.DeliveryAddressCity, trip.DeliveryAddressRegion ), FindStringResource( "DialoguesTrackingDialogShipmentVerified" ) ) );
		}

		private string FormatAddress( string companyName, string city, string region )
		{
			var sb = new StringBuilder( companyName ).Append( "\n" );

			if( !string.IsNullOrEmpty( city ) )
				sb.Append( city ).Append( " " );

			if( !string.IsNullOrEmpty( region ) )
				sb.Append( ", " ).Append( region ).Append( " " );

			return sb.ToString();
		}
	}
}