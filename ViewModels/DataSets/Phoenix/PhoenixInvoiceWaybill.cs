﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.ReportBrowser;

namespace ViewModels.Reporting;


public partial class DataSets
{
    public class PhoenixInvoiceWaybillDetails
    {
        private Invoicing.InvoicingModel.InvoiceReportSettings ReportSettings;
        private List<Trip> Trips;

        //public async Task<PhoenixInvoiceWaybillDetails> Load()
        //{
        //    //await SearchTrips();
        //    Logging.WriteLogLine("Found " + Trips.Count + " trips");
        //    return this;
        //}

        public PhoenixInvoiceWaybillDetails(Invoicing.InvoicingModel.InvoiceReportSettings irs) 
        { 
            ReportSettings = irs;
            Trips = irs.Trips;


        }

    }


    public class PhoenixInvoiceWaybillDetailsList : List<PhoenixInvoiceWaybillDetails>
    {
        public PhoenixInvoiceWaybillDetailsList(Invoicing.InvoicingModel.InvoiceReportSettings irs) 
        {
            if (irs != null && irs.Trips != null && irs.Trips.Count > 0)
            {
                PhoenixInvoiceWaybillDetails details = new(irs);
                Add(details);
            }
        }
    }
}
