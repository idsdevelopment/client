﻿namespace ViewModels.Reporting;

public partial class DataSets
{
	//private static Random random = new Random();

	public class WeighbillDetails
	{
		//private Random random = new Random();

		public string RandomItem      => page + "";
		public string PuAddress       => GetFormattedPickupAddress(); // "From: Pickup Address";
		public string Service         => trip.ServiceLevel;           // "Service Type";
		public string PuContact       => "Pickup Contact";
		public string DelContact      => "Delivery Contact";
		public string DelPhone        => "Delivery Phone";
		public string DelAddress      => GetFormattedDeliveryAddress(); // "To: Delivery Address";
		public string ShipmentDetails => GetShipmentDetails();
		public string Calltime        => trip.CallTime.ToString();    //"Date: Calltime";
		public string Weight          => trip.Weight + "";            // Weight: 1.0";
		public string Barcode         => GetBarcode();                // this.trip.tripId; // + "-" + this.page;
		public string Tripid          => "Waybill #: " + GetTripId(); // this.trip.tripId; // + "-" + this.page;
		public string DelZone         => trip.DeliveryZone;           //"Delivery Zone";
		public string DelNotes        => trip.DeliveryAddressNotes;   // "Delivery Notes";
		public string PieceCount      => "No. of Pieces: 1 of 1";
		public string ClientRef       => "Ref: " + trip.Reference;
		public string Description     => trip.PickupAddressNotes;
		public string CODLabel        => GetCODLabel();
		public string CODAmount       => GetCODAmount();

		public string ResellerName { get; }

		public string ResellerPhone { get; }

		private readonly Trip   trip;
		private readonly int    page;
		private readonly bool   isCOD;
		private readonly string codAmount;
		private readonly bool   isShowTripIdPlusPieceCount;
		public           string CarrierName = ""; // => "Intermountain Express Transport\n3059 West Parkway Blvd\nWest Valley City, UT. 84119"; // => this.trip.resellerId;

		public override string ToString() =>
			// return base.ToString();
			$"CARRIER_NAME: {CarrierName}\nPU_ADDRESS: {PuAddress}\nSERVICE: {Service}\nPU_CONTACT: {PuContact}\n";

		public WeighbillDetails( int _page )
		{
			page = _page;
		}

		public WeighbillDetails( int _page, Trip _trip, bool _isCOD, string _codAmount, bool _showTripIdPlusPieceCount, string _resellerName, string _resellerPhone )
		{
			page                       = _page;
			trip                       = _trip;
			isCOD                      = _isCOD;
			codAmount                  = _codAmount;
			isShowTripIdPlusPieceCount = _showTripIdPlusPieceCount;
			ResellerName               = _resellerName;
			ResellerPhone              = _resellerPhone;

			// this.CarrierName = "Intermountain Express Transport\n3059 West Parkway Blvd\nWest Valley City, UT. 84119";
		}

		private string GetFormattedPickupAddress()
		{
			var address = "";
			address += trip.PickupCompanyName + "\n";

			if( !string.IsNullOrEmpty( trip.PickupAddressSuite ) )
				address += trip.PickupAddressSuite + " ";

			address += trip.PickupAddressAddressLine1 + "\n";
			address += trip.PickupAddressCity + ", " + trip.PickupAddressRegion + ". " + trip.PickupAddressPostalCode;

			return address;
		}

		private string GetFormattedDeliveryAddress()
		{
			var address = "";
			address += trip.DeliveryCompanyName + "\n";

			if( !string.IsNullOrEmpty( trip.DeliveryAddressSuite ) )
				address += trip.DeliveryAddressSuite + " ";

			address += trip.DeliveryAddressAddressLine1 + "\n";
			address += trip.DeliveryAddressCity + ", " + trip.DeliveryAddressRegion + ". " + trip.DeliveryAddressPostalCode;

			return address;
		}

		private string GetShipmentDetails()
		{
			var details = "Date: " + trip.CallTime + "\n"
			              + "Ref: " + trip.Reference + "\n"
			              // + this.trip.pickupNotes + "\n"  // Removed 20170809 re: Eddy
			              + "Weight: " + trip.Weight;

			return details;
		}

		private string GetCODLabel()
		{
			var codLabel = string.Empty;

			if( isCOD )
				codLabel = "COD";

			return codLabel;
		}

		private string GetCODAmount()
		{
			var codAmount = string.Empty;

			if( isCOD )
			{
				float value;

				if( float.TryParse( this.codAmount, out value ) )
					codAmount = string.Format( "{0:C}", value );
				else
				{
					codAmount = "$" + this.codAmount;

					if( codAmount.IndexOf( "." ) < 0 )
						codAmount += ".00";
				}
			}

			return codAmount;
		}

		private string GetTripId()
		{
			var tripId = trip.TripId;

			if( isShowTripIdPlusPieceCount )
				tripId += "-" + page;

			return tripId;
		}

		private string GetBarcode()
		{
			var tripId = trip.TripId;

			if( isShowTripIdPlusPieceCount )
				tripId += "-" + page;

			// TODO Kludge
			//var server = Ids2Tracking.MainWindow.CURRENT_SERVER_NAME;
			var server = "jbtest2";
			tripId = "http://" + server + ".internetdispatcher.org:8080/barbecue/barcode?type=Code128&height=30&width=2&resolution=100&drawText=true&headless=false&data=" + tripId;

			// return "[" + tripId + "]";
			return tripId;
		}
	}

	// Terry suggested having this
	public class WeighbillDetailsList : List<WeighbillDetails>
	{
/*
			public WeighbillDetailsList( IEnumerable<Trip> tripList )
			{
				foreach( var Trip in tripList )
					Add( new WeighbillDetails( Trip ) );
			}

			public WeighbillDetailsList( IEnumerable<WeighbillDetails> weighbillDetailsList )
			{
				foreach( var Wbd in weighbillDetailsList )
					Add( Wbd );
			}
*/
/*            public WeighbillDetailsList()
			{
				Add(new WeighbillDetails(1));
				Add(new WeighbillDetails(2));
				Add(new WeighbillDetails(3));
				Add(new WeighbillDetails(4));
			}

			public WeighbillDetailsList(int pageCount)
			{
				if (pageCount > 0)
				{
					for(int page = 0; page < pageCount; page++)
					{
						Add(new WeighbillDetails(page));
					}
				}
			}
*/

		public WeighbillDetailsList( Trip _trip, bool isCOD, string codAmount, bool showTripIdPlusPieceCount, string resellerName, string resellerPhone )
		{
			if( _trip != null )
			{
				for( var page = 1; page <= _trip.Pieces; page++ )
					Add( new WeighbillDetails( page, _trip, isCOD, codAmount, showTripIdPlusPieceCount, resellerName, resellerPhone ) );
			}
		}
	}
}