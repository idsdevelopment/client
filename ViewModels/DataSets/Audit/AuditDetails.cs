﻿namespace ViewModels.Reporting;

public partial class DataSets
{
	public class AuditDetails
	{
		public string TripId      => "Audit Trail for item: " + trip.TripId;
		public string DateOfEvent => date.ToString( "yyyy/MM/dd hh:mm tt" );


		//public string DateOfEvent => {
		//    // var Time = _ShowAdLocalTime ? Timestamp : Timestamp.AddHours( -TimeZoneInfo.Local.GetUtcOffset( Timestamp ).Hours ).AddHours( TimeZoneOffset );
		//};

		public string User { get; }

		public string Action { get; }

		public string Description { get; }

		private readonly Trip           trip;
		private          DateTimeOffset date;

		public AuditDetails( Trip trip, DateTimeOffset date, string user, string action, string description )
		{
			this.trip   = trip;
			this.date   = date;
			User        = user;
			Action      = action;
			Description = description;
		}

		public AuditDetails( Trip trip, AuditLine line )
		{
			this.trip   = trip;
			date        = line.DateOfEvent;
			User        = line.User;
			Action      = line.Action;
			Description = line.Description;
		}
	}


	public class AuditDetailsList : List<AuditDetails>
	{
		public AuditDetailsList( Trip trip, List<AuditLine> lines )
		{
			if( lines != null )
			{
				foreach( var line in lines )
					Add( new AuditDetails( trip, line ) );
			}
		}
	}

	public class AuditLine
	{
		public DateTimeOffset DateOfEvent { get; }

		public string User { get; }

		public string Action { get; }

		public string Description { get; }

		public AuditLine( DateTimeOffset date, string user, string action, string description )
		{
			DateOfEvent = date;
			//var Time = _ShowAdLocalTime ? Timestamp 
			//                            : Timestamp.AddHours( -TimeZoneInfo.Local.GetUtcOffset( Timestamp ).Hours ).AddHours( TimeZoneOffset );
			//this.date.AddHours(-TimeZoneInfo.Local.GetUtcOffset(date).Hours);
			User        = user;
			Action      = action;
			Description = description;
		}
	}
}