﻿using ViewModels.ReportBrowser;

namespace ViewModels.Reporting;

public partial class DataSets
{
	public class LinfoxShipmentReportDetails
	{
		private readonly ReportSettings ReportSettings;

		//private List<Trip> trips = new List<Trip>();
		private TripList Trips = new();

		public async Task<LinfoxShipmentReportDetails> Load()
		{
			await SearchTrips();
			Logging.WriteLogLine( "Found " + Trips.Count + " trips" );
			return this;
		}

		public LinfoxShipmentReportDetails( ReportSettings rs )
		{
			ReportSettings = rs;
		}

		private async Task SearchTrips()
		{
			var St = new SearchTrips
			         {
				         FromDate    = ReportSettings.SelectedFrom,
				         ToDate      = ReportSettings.SelectedTo,
				         ByTripId    = false,
				         StartStatus = GetStatusForString( ReportSettings.SelectedFromStatus ),
				         EndStatus   = GetStatusForString( ReportSettings.SelectedToStatus ),
				         CompanyCode = ReportSettings.CompanyCode
			         };

			if( ReportSettings.SelectedPackageType != ReportSettings.All )
				St.PackageType = ReportSettings.SelectedPackageType;

			Trips = await Azure.Client.RequestSearchTrips( St );
		}

		private static STATUS GetStatusForString( string status )
		{
			var Result = status switch
			             {
				             "UNSET"      => STATUS.UNSET,
				             "NEW"        => STATUS.NEW,
				             "ACTIVE"     => STATUS.ACTIVE,
				             "DISPATCHED" => STATUS.DISPATCHED,
				             "PICKED_UP"  => STATUS.PICKED_UP,
				             "DELIVERED"  => STATUS.DELIVERED,
				             "VERIFIED"   => STATUS.VERIFIED,
				             "POSTED"     => STATUS.POSTED,
				             "INVOICED"   => STATUS.INVOICED,
				             "PAID"       => STATUS.FINALISED,
				             "DELETED"    => STATUS.DELETED,
				             _            => STATUS.UNSET
			             };

			return Result;
		}
	}

	/// <summary>
	///     Main entry.
	/// </summary>
	public class LinfoxShipmentReportDetailsList : List<LinfoxShipmentReportDetails>
	{
		private readonly ReportSettings ReportSettings;

		public async Task<LinfoxShipmentReportDetailsList> Load()
		{
			if( ReportSettings is not null )
			{
				Logging.WriteLogLine( "Building report: " + ReportSettings.Description );

				Add( await new LinfoxShipmentReportDetails( ReportSettings ).Load() );
			}
			return this;
		}

		public LinfoxShipmentReportDetailsList( ReportSettings reportSettings )
		{
			if( reportSettings is not null )
				ReportSettings = reportSettings;
			else
				Logging.WriteLogLine( "ERROR : reportSettings is null" );
		}
	}
}