﻿using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibraryV2.Utils;

namespace ViewModels._Customers.Priority.Routes;

/// <summary>
///     Interaction logic for Routes.xaml
/// </summary>
public partial class Routes : Page
{
	private PriorityRoutesViewModel Model;

	public Routes()
	{
		Initialized += ( _, _ ) =>
		               {
			               if( DataContext is PriorityRoutesViewModel M )
			               {
				               Model = M;

				               M.BeginEditRoute += _ =>
				                                   {
					                                   Grid.UpdateLayout();
					                                   Grid.Focus();
					                                   var Count = Grid.Items.Count;

					                                   if( Count > 0 )
					                                   {
						                                   var Cell = Grid.GetCell( Count - 1, 0 );

						                                   if( Cell is not null )
							                                   Cell.FindChild<TextBox>()?.Focus();
					                                   }
				                                   };
			               }
		               };
		InitializeComponent();
	}

	private void ComboBox_KeyDown( object sender, KeyEventArgs e )
	{
		if( !CustomerComboBox.IsDropDownOpen )
			CustomerComboBox.IsDropDownOpen = true;
	}
}