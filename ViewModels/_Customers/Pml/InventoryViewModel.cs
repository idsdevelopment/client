﻿using Protocol.Data._Customers.Pml;

namespace ViewModels._Customers.Pml;

public class InventoryViewModel : ViewModelBase
{
	public class DisplayInventory : PmlInventory, INotifyPropertyChanged
	{
		public enum STATE
		{
			NONE,
			NEW,
			MODIFIED,
			DELETED
		}

		public STATE State
		{
			get => _State;
			set
			{
				_State = value;
				OnPropertyChanged();
			}
		}

		public new string Barcode
		{
			get => base.Barcode;
			set
			{
				base.Barcode = value;
				OnPropertyChanged();
			}
		}


		public bool IsBlank => ( State == STATE.NEW ) && ( Barcode == "" ) && ( InventoryCode == "" ) && ( Description == "" ) && ( PackageQuantity <= 1 );

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public DisplayInventory()
		{
		}

		public DisplayInventory( PmlInventory inv ) : base( inv )
		{
		}

		private STATE _State = STATE.NONE;

		public override bool Equals( object obj )
		{
			if( obj is null )
				return false;

			if( !base.Equals( obj ) )
				return false;

			var I = (DisplayInventory)obj;
			return State == I.State;
		}

		public override int GetHashCode() => base.GetHashCode();

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public bool EnableNew
	{
		get { return Get( () => EnableNew, true ); }
		set { Set( () => EnableNew, value ); }
	}


	public ObservableCollection<DisplayInventory> Inventory
	{
		get { return Get( () => Inventory, new ObservableCollection<DisplayInventory>() ); }
		set { Set( () => Inventory, value ); }
	}


	public ICollection<DisplayInventory> SelectedInventory
	{
		get { return Get( () => SelectedInventory, new Collection<DisplayInventory>() ); }
		set { Set( () => SelectedInventory, value ); }
	}


	public int SelectedIndex
	{
		get { return Get( () => SelectedIndex, -1 ); }
		set { Set( () => SelectedIndex, value ); }
	}

	public string InventoryCode
	{
		get { return Get( () => InventoryCode, "" ); }
		set { Set( () => InventoryCode, value ); }
	}


	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}


	public bool IsDelete
	{
		get { return Get( () => IsDelete, true ); }
		set { Set( () => IsDelete, value ); }
	}


	public bool EnableRemoveDelete
	{
		get { return Get( () => EnableRemoveDelete, false ); }
		set { Set( () => EnableRemoveDelete, value ); }
	}


	public bool EnableSave
	{
		get { return Get( () => EnableSave, false ); }
		set { Set( () => EnableSave, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( !IsInDesignMode )
		{
			Task.Run( async () =>
					  {
						  var Inv = await Azure.Client.RequestPML_GetInventory();

						  if( Inv is not null )
						  {
							  var IList = new ObservableCollection<DisplayInventory>();

							  foreach( var I in Inv )
								  IList.Add( new DisplayInventory( I ) );

							  Dispatcher.Invoke( () =>
											     {
												     Inventory = IList;
											     } );
						  }
					  } );
		}
	}


	public Action<string> OnDuplicateInventory,
						  OnDuplicateBarcode;

	public EditInventoryEvent OnEditTrip;


	private readonly List<DisplayInventory> SaveCollection = new();

	private void DoEnableSave( bool enab = true )
	{
		var Inv = Inventory;

		var Changed = ( from I in Inv
					    where I.State != DisplayInventory.STATE.NONE
					    select I ).ToList();
		var Cnt = Changed.Count;

		if( Cnt > 0 )
		{
			if( enab )
				EnableSave = true;
			else
				EnableSave = ( Cnt > 1 ) && Changed[ Cnt - 1 ].IsBlank;
		}
		else
			EnableSave = false;
	}

	[DependsUpon( nameof( SelectedIndex ), Delay = 250 )]
	public void WhenSelectedIndexChanges()
	{
		var Ndx = SelectedIndex;

		if( Ndx >= 0 )
		{
			EnableRemoveDelete = true;
			IsDelete           = Inventory[ Ndx ].State != DisplayInventory.STATE.NEW;
			OnEditTrip?.Invoke( Ndx );
		}
		else
			EnableRemoveDelete = false;
	}

	[DependsUpon( nameof( InventoryCode ) )]
	public void WhenInventoryCodeChanges()
	{
		DoEnableSave( false );

		var InvCode = InventoryCode;

		if( !InvCode.IsNullOrWhiteSpace() )
		{
			var Ndx = SelectedIndex;

			if( Ndx >= 0 )
			{
				var IList = Inventory;
				var C     = IList.Count;

				if( Ndx < C )
				{
					for( var I = 0; I < C; I++ )
					{
						if( ( I != Ndx ) && ( IList[ I ].InventoryCode == InvCode ) )
						{
							OnDuplicateInventory?.Invoke( InvCode );
							return;
						}
					}

					var Item = IList[ Ndx ];

					if( Item.Barcode.IsNullOrWhiteSpace() )
						Item.Barcode = InvCode;
				}
			}

			DoEnableSave();
		}
	}

	[DependsUpon( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		DoEnableSave( false );

		var BCode = Barcode;

		if( !BCode.IsNullOrWhiteSpace() )
		{
			var Ndx = SelectedIndex;

			if( Ndx >= 0 )
			{
				var IList = Inventory;
				var C     = IList.Count;

				if( Ndx < C )
				{
					for( var I = 0; I < C; I++ )
					{
						if( ( I != Ndx ) && ( IList[ I ].Barcode == BCode ) )
						{
							DoEnableSave( false );
							OnDuplicateBarcode?.Invoke( BCode );
							return;
						}
					}
				}
			}

			DoEnableSave();
		}
	}

	public void Execute_AddInventory()
	{
		EnableNew = false;

		var Inv = Inventory;
		SaveCollection.Clear();
		SaveCollection.AddRange( Inv );

		var Item = new DisplayInventory
				   {
					   State = DisplayInventory.STATE.NEW
				   };

		Inv.Add( Item );

		InventoryCode = "";
		Barcode       = "";
		SelectedIndex = Inv.Count - 1; // Triggers Edit Trip
	}

	public void Execute_Clear()
	{
		DoEnableSave( false );

		Inventory.Clear();

		foreach( var PmlInventory in SaveCollection )
			Inventory.Add( PmlInventory );

		EnableNew = true;
	}

	public void Execute_Save()
	{
		if( !IsInDesignMode )
		{
			try
			{
				DoEnableSave( false );

				PmlInventoryList Items        = new(),
								 DeletedItems = new();

				foreach( var I in Inventory )
				{
					if( !I.IsBlank )
					{
						switch( I.State )
						{
						case DisplayInventory.STATE.NONE:
							break;

						case DisplayInventory.STATE.DELETED:
							DeletedItems.Add( I );
							break;

						default:
							Items.Add( I );
							break;
						}

						I.State = DisplayInventory.STATE.NONE;
					}
				}

				if( Items.Count > 0 )
				{
					Task.Run( async () =>
							  {
								  try
								  {
									  await Azure.Client.RequestPML_AddUpdateInventory( Items );
								  }
								  catch( Exception Exception )
								  {
									  Logging.WriteLogLine( Exception );
								  }
							  } );
				}

				if( DeletedItems.Count > 0 )
				{
					Task.Run( async () =>
							  {
								  try
								  {
									  await Azure.Client.RequestPML_DeleteInventory( DeletedItems );
								  }
								  catch( Exception Exception )
								  {
									  Logging.WriteLogLine( Exception );
								  }
							  } );
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( E );
			}
			finally
			{
				SaveCollection.Clear();
				EnableNew  = true;
				EnableSave = false;
				var Inv = Inventory;

				for( var I = Inv.Count; --I >= 0; )
				{
					if( Inv[ I ].IsBlank )
						Inv.RemoveAt( I );
				}
			}
		}
	}

	public void Execute_Delete()
	{
		var Ndx = SelectedIndex;

		if( Ndx >= 0 )
		{
			var Item = Inventory[ Ndx ];

			switch( Item.State )
			{
			case DisplayInventory.STATE.NEW:
				break;
			}
		}
	}

	public delegate void EditInventoryEvent( int row );
}