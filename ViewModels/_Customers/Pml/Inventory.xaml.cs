﻿using System.Timers;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibraryV2.Utils;
using Xceed.Wpf.Toolkit;

namespace ViewModels._Customers.Pml;

/// <summary>
///     Interaction logic for Inventory.xaml
/// </summary>
public partial class Inventory : Page
{
	private const int INVETORY_CELL_INDEX         = 0,
	                  BARCODE_CELL_INDEX          = 1,
	                  PACKAGE_QUANTITY_CELL_INDEX = 3;


	private InventoryViewModel Model;

	private readonly string InventoryAlreadyInFile,
	                        BarcodeAlreadyInFile;

	private Timer FocusTimer;

	public Inventory()
	{
		InventoryAlreadyInFile = Globals.FindStringResource( "PmlInventoryInFile" );
		BarcodeAlreadyInFile   = Globals.FindStringResource( "PmlBarcodeInFile" );

		Initialized += ( _, _ ) =>
		               {
			               if( DataContext is InventoryViewModel M )
			               {
				               Model = M;

				               M.OnEditTrip = row =>
				                              {
					                              DataGrid.CurrentItem = Model.Inventory[ row ];
					                              DataGrid.UpdateLayout();

					                              //       FocusInventoryCode( row );
					                              DataGrid.BeginEdit();
				                              };

				               M.OnDuplicateInventory = _ =>
				                                        {
					                                        Globals.Dialogues.Error.Show( InventoryAlreadyInFile );

					                                        DelayedFocusInventoryCode( DataGrid.SelectedIndex, _ =>
					                                                                                           {
						                                                                                           Model.InventoryCode = "";
						                                                                                           DataGrid.BeginEdit();
					                                                                                           } );
				                                        }
					               ;

				               M.OnDuplicateBarcode = _ =>
				                                      {
					                                      Globals.Dialogues.Error.Show( BarcodeAlreadyInFile );

					                                      DelayedFocusBarcode( DataGrid.SelectedIndex, _ =>
					                                                                                   {
						                                                                                   Model.Barcode = "";
						                                                                                   DataGrid.BeginEdit();
					                                                                                   } );
				                                      };
			               }
		               };

		InitializeComponent();
	}

	private void DataGrid_CellEditEnding( object sender, DataGridCellEditEndingEventArgs e )
	{
		if( e.Column is DataGridTextColumn && e.EditingElement is TextBox Tb )
		{
			var Code = Tb.Text.Trim();

			switch( e.Column.DisplayIndex )
			{
			case INVETORY_CELL_INDEX:
				Model.InventoryCode = Code;
				break;

			case BARCODE_CELL_INDEX:
				Model.Barcode = Code;
				break;
			}
		}
	}

	private DataGridCell FocusInventoryCode( int row )
	{
		var Cell = DataGrid.GetCell( row, 0 );
		Cell?.Focus();

		return Cell;
	}

	private DataGridCell FocusBarcode( int row )
	{
		var Cell = DataGrid.GetCell( row, 1 );
		Cell?.Focus();

		return Cell;
	}


	private void DelayedFocusInventoryCode( int row, Action<int> callback = null )
	{
		FocusTimer = new Timer( 10 )
		             {
			             AutoReset = false
		             };

		FocusTimer.Elapsed += ( _, _ ) =>
		                      {
			                      Dispatcher.Invoke( () =>
			                                         {
				                                         DataGrid.Focus();
				                                         FocusInventoryCode( row );
				                                         callback?.Invoke( row );
			                                         } );
			                      FocusTimer.Dispose();
			                      FocusTimer = null;
		                      };
		FocusTimer.Start();
	}


	private void DelayedFocusBarcode( int row, Action<int> callback = null )
	{
		FocusTimer = new Timer( 10 )
		             {
			             AutoReset = false
		             };

		FocusTimer.Elapsed += ( _, _ ) =>
		                      {
			                      Dispatcher.Invoke( () =>
			                                         {
				                                         DataGrid.Focus();
				                                         FocusBarcode( row );
				                                         callback?.Invoke( row );
			                                         } );
			                      FocusTimer.Dispose();
			                      FocusTimer = null;
		                      };
		FocusTimer.Start();
	}

	private void DataGrid_BeginningEdit( object sender, DataGridBeginningEditEventArgs e )
	{
		var Ndx = Model.SelectedIndex;

		var CellNdx = e.Column.DisplayIndex;
		var Cell    = DataGrid.GetCell( e.Row, CellNdx );

		if( Cell is not null )
		{
			Cell.Focus();

			if( CellNdx == PACKAGE_QUANTITY_CELL_INDEX )
			{
				FocusTimer = new Timer( 100 )
				             {
					             AutoReset = false
				             };

				FocusTimer.Elapsed += ( _, _ ) =>
				                      {
					                      Dispatcher.Invoke( () =>
					                                         {
						                                         ( (ContentPresenter)Cell.Content ).FindChild<DecimalUpDown>().Focus();
					                                         } );
					                      FocusTimer.Dispose();
					                      FocusTimer = null;
				                      };
				FocusTimer.Start();
			}
		}
	}

	private void DataGrid_PreviewKeyDown( object sender, KeyEventArgs e )
	{
		if( e.Key == Key.Tab )
		{
			var Ndx = DataGrid.SelectedIndex;

			if( ( Ndx++ == ( Model.Inventory.Count - 1 ) ) && ( DataGrid.CurrentCell.Column.DisplayIndex == PACKAGE_QUANTITY_CELL_INDEX ) )
			{
				DataGrid.CommitEdit();
				var Item = new InventoryViewModel.DisplayInventory();
				Model.Inventory.Add( Item );
				Model.InventoryCode    = "";
				Model.Barcode          = "";
				DataGrid.CurrentItem   = Item;
				DataGrid.SelectedIndex = Ndx;
				FocusInventoryCode( Ndx );
				DataGrid.BeginEdit();
			}
		}
	}

	private void DataGridRow_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
	{
		DataGrid.CancelEdit();
		Model.InventoryCode = "";
		Model.Barcode       = "";
		Model.SelectedIndex = ( (DataGridRow)sender ).GetIndex();
	}
}