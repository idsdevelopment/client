﻿#nullable enable

using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;
using ViewModels.ReportBrowser;
using File = System.IO.File;

namespace ViewModels.Reports.WebReports;

public class WebReportsModel : ViewModelBase
{
	protected override async void OnInitialised()
	{
		base.OnInitialised();

		await Task.WhenAll(
						   GetTrucks(),
						   GetCompanyNames()
						  );
	}

	public Action<string>? OnPageAvailable;

#region Phoenix
	public string GlobalAddressBookId = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;

	private void ShowReport( ReportReply? rep )
	{
		if( rep is not null )
		{
			var FileName = $"{Path.GetTempFileName()}.html";
			File.WriteAllText( FileName, rep.HtmlReport );
			OnPageAvailable?.Invoke( FileName );
			Process.Start( FileName );

			Tasks.RunVoid( 10_000, () =>
								   {
									   File.Delete( FileName );
								   } );
		}
	}

    #region Call Taker
    public ICommand CallTakerReport => Commands[ nameof( CallTakerReport ) ];

	public async void Execute_CallTakerReport()
	{
		//var Now = DateTimeOffset.Now;

		//var Day = (int)Now.DayOfWeek;          // Sunday == 0
		//Now = Now.AddDays( ( -Day + 1 ) - 7 ); // Monday Of Previous Week

		var Result = await Azure.Client.RequestReport( new Report
													   {
														   Name         = "CallTaker",
														   DateTimeArg1 = DateFrom,
														   DateTimeArg2 = DateTo
		} );
		ShowReport( Result );
	}

	/// <summary>
	/// This is a redirect to generate the Driver Manifest 
	/// from the Report Browser instead of from the test report page
	/// </summary>
	/// <param name="rs"></param>
	/// <returns></returns>
	public void ReportBrowserExecuteCallTakerReport(ReportSettings rs)
	{
		DateFrom = rs.SelectedFrom;
		DateTo = rs.SelectedTo;
		Execute_CallTakerReport();
	}

	#endregion

	#region Waybill
	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	public override void OnArgumentChange( object? arg )
	{
		base.OnArgumentChange( arg );

		if( arg is string Id )
			TripId = Id.Trim();
	}

	public ICommand WaybillReport => Commands[ nameof( WaybillReport ) ];

	public async void Execute_WaybillReport()
	{
		var Now = DateTimeOffset.Now; // for now just generate waybills for shipments for the last 24 hours
		// I want a search bar to enter in the shipment and/or auto generate many for a company or a time range etc

		var Result = await Azure.Client.RequestReport( new Report
													   {
														   Name         = "WaybillReport",
														   DateTimeArg1 = Now.AddDays( -1 ),
														   DateTimeArg2 = Now,
														   StringArg1   = TripId
													   } );
		ShowReport( Result );
	}

	/// <summary>
	/// This is a redirect to generate the Waybill 
	/// from the Shipment Entry Page
	/// </summary>
	public void ShipmentEntry_Waybill(string shipmentid)
	{
		TripId = shipmentid;
		Execute_WaybillReport();
	}

	/// <summary>
	/// This is a redirect to generate the Waybill
	/// from the Report Browser in addition to from the test reports and from the shipment entry pages
	/// </summary>
	/// <param name="rs"></param>
	/// <returns></returns>
	public void ReportBrowserExecuteWaybillReport(ReportSettings rs)
	{
		ShipmentEntry_Waybill(rs.String1);
	}

	#endregion

	#region Driver Manifest
	public DateTimeOffset DateFrom
	{
		get { return Get( () => DateFrom, DateTimeOffset.Now ); }
		set { Set( () => DateFrom, value ); }
	}

	public DateTimeOffset DateTo
	{
		get { return Get( () => DateTo, DateTimeOffset.Now ); }
		set { Set( () => DateTo, value ); }
	}

	public IList Trucks
	{
		get { return Get( () => Trucks, new List<string>() ); }
		set { Set( () => Trucks, value ); }
	}

	public string SelectedTruck
	{
		get { return Get( () => SelectedTruck, "" ); }
		set { Set( () => SelectedTruck, value ); }
	}

	private async Task GetTrucks()
	{
		var List  = new List<string> {"All"};
		var Names = await Azure.Client.RequestGetDrivers();

		if( Names is not null )
		{
			var CoNames = ( from N in Names
						    let S = N.StaffId
						    orderby S
						    select S ).ToList();

			List.AddRange( CoNames );
		}
		Trucks        = List;
		SelectedTruck = Trucks[ 0 ].ToString();
	}

	public ICommand DriverManifestReport => Commands[ nameof( DriverManifestReport ) ];

	public async void Execute_DriverManifestReport()
	{
		if( DateTo < DateFrom )
			( DateFrom, DateTo ) = ( DateTo, DateFrom );

		var Result = await Azure.Client.RequestReport( new Report
													   {
														   Name         = "DriverManifest",
														   IsLandscape  = true,
														   DateTimeArg1 = DateFrom.StartOfDay(),
														   DateTimeArg2 = DateTo.EndOfDay(),
														   StringArg1   = SelectedTruck
													   } );

		ShowReport( Result );
	}

	/// <summary>
	///     This is a redirect to generate the Driver Manifest
	///     from the Report Browser instead of from the test report page
	/// </summary>
	/// <param
	///     name="rs">
	/// </param>
	/// <returns></returns>
	public void ReportBrowserExecuteDriverManifest( ReportSettings rs )
	{
		DateFrom      = rs.SelectedFrom;
		DateTo        = rs.SelectedTo;
		SelectedTruck = rs.SelectedDriver;
		Execute_DriverManifestReport();
	}

	public ICommand DriverArriveTimeReport => Commands[ nameof( DriverArriveTimeReport ) ];

	public async void Execute_DriverArriveTimeReport()
	{
		if( DateTo < DateFrom )
			( DateFrom, DateTo ) = ( DateTo, DateFrom );

		var Result = await Azure.Client.RequestReport( new Report
													   {
														   Name         = "DriverArriveTimeReport",
														   IsLandscape  = true,
														   DateTimeArg1 = DateFrom.StartOfDay(),
														   DateTimeArg2 = DateTo.EndOfDay(),
														   StringArg1   = SelectedTruck
													   } );

		ShowReport( Result );
	}

	/// <summary>
	///     This is a redirect to generate the Driver Manifest
	///     from the Report Browser instead of from the test report page
	/// </summary>
	/// <param
	///     name="rs">
	/// </param>
	/// <returns></returns>
	public void ReportBrowserExecuteDriverArriveTimeReport( ReportSettings rs )
	{
		DateFrom      = rs.SelectedFrom;
		DateTo        = rs.SelectedTo;
		SelectedTruck = rs.SelectedDriver;
		Execute_DriverArriveTimeReport();
	}
#endregion

	#region General Report
	public ICommand GeneralTripReport => Commands[ nameof( GeneralTripReport ) ];

	// Following Drop down set up in Search trips
	public IList StartStatus
	{
		get { return Get( () => StartStatus, GetPublicStatusStrings() ); }
		set { Set( () => StartStatus, value ); }
	}

	public STATUS SelectedStartStatus
	{
		get { return Get( () => SelectedStartStatus, STATUS.UNSET ); }
		set { Set( () => SelectedStartStatus, value ); }
	}

	public IList EndStatus
	{
		get { return Get( () => EndStatus, GetPublicStatusStrings() ); }
		set { Set( () => EndStatus, value ); }
	}

	public STATUS SelectedEndStatus
	{
		get { return Get( () => SelectedEndStatus, STATUS.UNSET ); }
		set { Set( () => SelectedEndStatus, value ); }
	}

	private static IList GetPublicStatusStrings()
	{
		var Strings = new List<string>
					  {
						  STATUS.UNSET.AsString(),
						  STATUS.NEW.AsString(),
						  STATUS.ACTIVE.AsString(),
						  STATUS.DISPATCHED.AsString(),
						  STATUS.PICKED_UP.AsString(),
						  STATUS.DELIVERED.AsString(),
						  STATUS.VERIFIED.AsString(),
						  STATUS.POSTED.AsString(),
						  STATUS.INVOICED.AsString(),
						  STATUS.SCHEDULED.AsString(),
						  STATUS.FINALISED.AsString(),
						  STATUS.DELETED.AsString()
					  };
		return Strings;
	}

	//public IList CompanyNames
	//{
	//	get { return Get(() => CompanyNames, GetAccountStrings()); }
	//	set { Set(() => CompanyNames, value); }
	//}

	//var Names = await Azure.Client.RequestGetCustomerCodeList();
	//               if(Names is not null )
	//               {
	//	               var CoNames = (from N in Names
	//                                              let Ln = N.ToLower()
	//                                              orderby Ln
	//                                              select Ln).ToList();
	//var CompanyAddresses = new List<CompanyAddress>();
	//await new Tasks.Task<string>().RunAsync(CoNames, async coName =>
	//                                                                                 {
	//    try
	//    {
	//        var Company = await Azure.Client.RequestGetResellerCustomerCompany(coName);
	//        lock (CompanyAddresses)
	//            CompanyAddresses.Add(Company);
	//    }
	//    catch (Exception Exception)
	//    {
	//        Logging.WriteLogLine("Exception thrown: " + Exception);
	//    }
	//}, 5 );
	//	               Dispatcher.Invoke(() =>
	//	                                  {
	//		                                  CompanyNames = CoNames;
	//		                                  Companies    = CompanyAddresses;
	//	                                  } );
	//               }

	//public List<CompanyAddress> Companies
	//{
	//	get { return Get(() => Companies, new List<CompanyAddress>()); }
	//	set { Set(() => Companies, value); }
	//}

	//public SortedDictionary<string, CompanyAddress> CompanyFullNames
	//{
	//	get { return Get(() => CompanyFullNames, new SortedDictionary<string, CompanyAddress>(StringComparer.OrdinalIgnoreCase)); }
	//	set { Set(() => CompanyFullNames, value); }
	//}

	//public string SelectedAccount
	//{
	//	get { return Get(() => SelectedAccount, ""); }
	//	set { Set(() => SelectedAccount, value); }
	//}

	//// Get company list 
	//private static IList GetAccountStrings() {
	//	return new List<string>();
	//	//return new List<CompanyAddress>();
	//}

	public async void Execute_GeneralTripReport()
	{
		if( DateTo < DateFrom )
			( DateFrom, DateTo ) = ( DateTo, DateFrom );

		var Result = await Azure.Client.RequestReport( new Report
													   {
														   Name         = "GeneralTripReport",
														   IsLandscape  = true,
														   DateTimeArg1 = DateFrom.StartOfDay(),
														   DateTimeArg2 = DateTo.EndOfDay(),
														   DecimalArg1  = (int)SelectedStartStatus,
														   DecimalArg2  = (int)SelectedEndStatus
													   } );
		ShowReport( Result );
	}
#endregion

#region PhoenixInvoiceWaybill


    private Dictionary<string, CustomerCodeCompanyName> DictCustomerNamesIds = new();
	

	public ObservableCollection<string> CompanyNames
	{
		get => Get( () => CompanyNames, new ObservableCollection<string>() );
		set => Set( () => CompanyNames, value );
	}

	public string CompanyName
	{
		get => Get( () => CompanyName, "" );
		set => Set( () => CompanyName, value );
	}

	//[DependsUpon(nameof( CompanyName))]
	public void WhenCompanyNameChanges()
	{
		Logging.WriteLogLine( "Selected CompanyName: " + CompanyName );
	}



    public async Task GetCompanyNames()
    {
        if (!IsInDesignMode)
        {
            var Companies = (from Company in await Azure.Client.RequestGetCustomerCodeCompanyNameList()
                             let CompanyName = Company.CompanyName.Trim().ToLower()
                             orderby CompanyName
                             group Company by CompanyName
                                  into G
                             select G.First()).ToList();
			DictCustomerNamesIds = Companies.ToDictionary(a => a.CompanyName, a => a);

			// Don't include the Global Address Book in the list
			var Names = new ObservableCollection<string>(from Company in Companies
                                                         where Company.CompanyName != GlobalAddressBookId
                                                         select Company.CompanyName);
            CompanyNames = Names;
            Logging.WriteLogLine("Loaded " + Names.Count + " companies");
        }
    }

	public string InvoiceId
	{
		get => Get( () => InvoiceId, "" );
		set => Set( () => InvoiceId, value );
	}

	//[DependsUpon(nameof(InvoiceId))]
	public void WhenInvoiceIdChanges()
	{
		Logging.WriteLogLine( $"Invoice Id: {InvoiceId}" );
	}

	public async void Execute_PhoenixInvoiceWaybill()
	{
        if (DateTo < DateFrom)
            (DateFrom, DateTo) = (DateTo, DateFrom);
		var Id = -1;

		if ( InvoiceId.IsNotNullOrWhiteSpace() )
			int.TryParse( InvoiceId, out Id );

		var Result = await Azure.Client.RequestReport( new Report
													   {
														   Name         = "PhoenixInvoiceWaybill",
														   IsLandscape  = false,
														   DateTimeArg1 = DateFrom.StartOfDay(),
														   DateTimeArg2 = DateTo.EndOfDay(),
														   StringArg1   = DictCustomerNamesIds[ CompanyName ].CustomerCode,
														   DecimalArg1  = Id
													   } );

		ShowReport(Result);
    }

#endregion
#endregion
}