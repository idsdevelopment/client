﻿#nullable enable

using System.Windows.Input;
using static Protocol.Data.Report;

namespace ViewModels.Reports.WebReports.Component;

public class WebReportsComponentViewModel : ViewModelBase
{
#region Save Settings
	[Setting]
	public string PdfFolder
	{
		get { return Get( () => PdfFolder, @"\" ); }
		set { Set( () => PdfFolder, value ); }
	}
#endregion

	public string ReportLabel
	{
		get { return Get( () => ReportLabel, "" ); }
		set { Set( () => ReportLabel, value ); }
	}

	public string RunButtonLabel
	{
		get { return Get( () => RunButtonLabel, "Execute" ); }
		set { Set( () => RunButtonLabel, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		WhenPrintChanges();
		WhenSavePdfChanges();
	}

#region Enable Run
	public bool EnableRunDependency
	{
		get { return Get( () => EnableRunDependency, true ); }
		set { Set( () => EnableRunDependency, value ); }
	}

	[DependsUpon( nameof( EnableRunDependency ) )]
	[DependsUpon( nameof( ReportName ) )]
	public void WhenEnableRunDependencyChanges()
	{
		EnableRun = EnableRunDependency && ReportName.IsNotNullOrWhiteSpace();
	}

	public bool EnableRun
	{
		get { return Get( () => EnableRun, true ); }
		set { Set( () => EnableRun, value ); }
	}
#endregion

#region Execute Report
	public string ReportName
	{
		get { return Get( () => ReportName, "" ); }
		set { Set( () => ReportName, value ); }
	}

	public ICommand OnRunReport => Commands[ nameof( OnRunReport ) ];

	public async Task Execute_OnRunReport()
	{
		var Name = ReportName;

		if( Name.IsNotNullOrWhiteSpace() )
		{
			EnableRun = false;

			try
			{
				var Report = await Azure.Client.RequestReport( new Report
															   {
																   Name = Name,

																   Send = SEND.RETURN_PDF,

																   DateTimeArg1 = DateTimeArg1,
																   DateTimeArg2 = DateTimeArg2,
																   DateTimeArg3 = DateTimeArg3,
																   DateTimeArg4 = DateTimeArg4,

																   DecimalArg1 = DecimalArg1,
																   DecimalArg2 = DecimalArg2,
																   DecimalArg3 = DecimalArg3,
																   DecimalArg4 = DecimalArg4,

																   StringArg1 = StringArg1,
																   StringArg2 = StringArg2,
																   StringArg3 = StringArg3,
																   StringArg4 = StringArg4,

																   EmailAddress = ""
															   } );

				if( Report is not null )
				{
				}
			}
			finally
			{
				WhenEnableRunDependencyChanges();
			}
		}
	}

	public Func<string>? OnGetPdfPathAndName;
#endregion

#region Paper
	public PAPER_SIZE PaperSize
	{
		get { return Get( () => PaperSize, PAPER_SIZE.A4 ); }
		set { Set( () => PaperSize, value ); }
	}

	public bool IsLandscape
	{
		get { return Get( () => IsLandscape, true ); }
		set { Set( () => IsLandscape, value ); }
	}
#endregion

#region String Args
	public string StringArg1
	{
		get { return Get( () => StringArg1, "" ); }
		set { Set( () => StringArg1, value ); }
	}

	public string StringArg2
	{
		get { return Get( () => StringArg2, "" ); }
		set { Set( () => StringArg2, value ); }
	}

	public string StringArg3
	{
		get { return Get( () => StringArg3, "" ); }
		set { Set( () => StringArg3, value ); }
	}

	public string StringArg4
	{
		get { return Get( () => StringArg4, "" ); }
		set { Set( () => StringArg4, value ); }
	}
#endregion

#region Decimal Args
	public decimal DecimalArg1
	{
		get { return Get( () => DecimalArg1, 0 ); }
		set { Set( () => DecimalArg1, value ); }
	}

	public decimal DecimalArg2
	{
		get { return Get( () => DecimalArg2, 0 ); }
		set { Set( () => DecimalArg2, value ); }
	}

	public decimal DecimalArg3
	{
		get { return Get( () => DecimalArg3, 0 ); }
		set { Set( () => DecimalArg3, value ); }
	}

	public decimal DecimalArg4
	{
		get { return Get( () => DecimalArg4, 0 ); }
		set { Set( () => DecimalArg4, value ); }
	}
#endregion

#region Date Time Args
	public DateTimeOffset DateTimeArg1
	{
		get { return Get( () => DateTimeArg1, DateTimeOffset.MinValue ); }
		set { Set( () => DateTimeArg1, value ); }
	}

	public DateTimeOffset DateTimeArg2
	{
		get { return Get( () => DateTimeArg2, DateTimeOffset.MinValue ); }
		set { Set( () => DateTimeArg2, value ); }
	}

	public DateTimeOffset DateTimeArg3
	{
		get { return Get( () => DateTimeArg3, DateTimeOffset.MinValue ); }
		set { Set( () => DateTimeArg3, value ); }
	}

	public DateTimeOffset DateTimeArg4
	{
		get { return Get( () => DateTimeArg4, DateTimeOffset.MinValue ); }
		set { Set( () => DateTimeArg4, value ); }
	}
#endregion

#region Print Options
	[Setting]
	public bool Print
	{
		get { return Get( () => Print, true ); }
		set { Set( () => Print, value ); }
	}

	[DependsUpon( nameof( Print ) )]
	public void WhenPrintChanges()
	{
		EnablePrint = Print;
	}

	public bool EnablePrint
	{
		get { return Get( () => EnablePrint, true ); }
		set { Set( () => EnablePrint, value ); }
	}

#region Printer
	[Setting]
	public string PrinterName
	{
		get { return Get( () => PrinterName, "" ); }
		set { Set( () => PrinterName, value ); }
	}

	[Setting]
	public string PrinterUncPath
	{
		get { return Get( () => PrinterUncPath, "" ); }
		set { Set( () => PrinterUncPath, value ); }
	}
#endregion

	[Setting]
	public bool SavePdf
	{
		get { return Get( () => SavePdf, false ); }
		set { Set( () => SavePdf, value ); }
	}

	[DependsUpon( nameof( SavePdf ) )]
	public void WhenSavePdfChanges()
	{
		EnableSavePdf = SavePdf;
	}

	public bool EnableSavePdf
	{
		get { return Get( () => EnableSavePdf, false ); }
		set { Set( () => EnableSavePdf, value ); }
	}
#endregion
}