﻿#nullable enable

using System.Windows.Forms;
using IdsControlLibrary;
using UserControl = System.Windows.Controls.UserControl;

namespace ViewModels.Reports.WebReports.Component;

/// <summary>
///     Interaction logic for UserControl1.xaml
/// </summary>
public partial class WebReport : UserControl
{
#region Events
	public Func<string>? OnGetPdfPathAndName
	{
		get => Model.OnGetPdfPathAndName;
		set => Model.OnGetPdfPathAndName = value;
	}
#endregion

	public WebReport()
	{
		InitializeComponent();
		Model              = (WebReportsComponentViewModel)Root.DataContext;
		RestoringPdfFolder = true;
		PdfSaveFolder      = Model.PdfFolder;
		RestoringPdfFolder = false;
	}

	private readonly WebReportsComponentViewModel Model;

	private void SelectPrinterButton_OnSelectionChanged( object sender, PrinterInfo newPrinter, string name )
	{
		Model.PrinterName    = name;
		Model.PrinterUncPath = newPrinter.UncPath;
	}

	private void Control_Unloaded( object sender, RoutedEventArgs e )
	{
		Model.SaveSettings();
	}

#region Report Name
	public string ReportName
	{
		get => (string)GetValue( ReportNameProperty );
		set => SetValue( ReportNameProperty, value );
	}

	// Using a DependencyProperty as the backing store for ReportName.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ReportNameProperty =
		DependencyProperty.Register( nameof( ReportName ),
								     typeof( string ),
								     typeof( WebReport ),
								     new FrameworkPropertyMetadata( "",
															        ReportNamePropertyChangedCallback ) );

	private static void ReportNamePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string Name )
			R.Model.ReportName = Name;
	}
#endregion

#region Run Button Label
	public string RunButtonLabel
	{
		get => (string)GetValue( RunButtonLabelProperty );
		set => SetValue( RunButtonLabelProperty, value );
	}

	// Using a DependencyProperty as the backing store for RunButtonLabel.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty RunButtonLabelProperty = DependencyProperty.Register( nameof( RunButtonLabel ),
																								    typeof( string ),
																								    typeof( WebReport ),
																								    new FrameworkPropertyMetadata( "Execute",
																															       RunButtonLabelPropertyChangedCallback
																															     ) );

	private static void RunButtonLabelPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string L )
			R.Model.RunButtonLabel = L;
	}

	public bool RunButtonEnable
	{
		get => (bool)GetValue( RunButtonEnableProperty );
		set => SetValue( RunButtonEnableProperty, value );
	}

	// Using a DependencyProperty as the backing store for RunButtonEnable.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty RunButtonEnableProperty = DependencyProperty.Register( nameof( RunButtonEnable ),
																								     typeof( bool ),
																								     typeof( WebReport ),
																								     new FrameworkPropertyMetadata( false,
																															        RunButtonEnablePropertyChangedCallback ) );

	private static void RunButtonEnablePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is bool Enable )
			R.Model.EnableRunDependency = Enable;
	}
#endregion

#region Report Label
	// Using a DependencyProperty as the backing store for ReportLabel.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ReportLabelProperty = DependencyProperty.Register( nameof( ReportLabel ),
																							     typeof( string ),
																							     typeof( WebReport ),
																							     new FrameworkPropertyMetadata( "",
																														        ReportLabelChangedCallback ) );

	private static void ReportLabelChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string L )
			R.Model.ReportLabel = L;
	}

	public string ReportLabel
	{
		get => (string)GetValue( ReportLabelProperty );
		set => SetValue( ReportLabelProperty, value );
	}
#endregion

#region StringArg1
	public string StringArg1
	{
		get => (string)GetValue( StringArg1Property );
		set => SetValue( StringArg1Property, value );
	}

	// Using a DependencyProperty as the backing store for StringArg1.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StringArg1Property = DependencyProperty.Register( nameof( StringArg1 ),
																							    typeof( string ),
																							    typeof( WebReport ),
																							    new FrameworkPropertyMetadata( "",
																														       StringArg1ChangedCallback ) );

	private static void StringArg1ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string Arg )
			R.Model.StringArg1 = Arg;
	}
#endregion

#region StringArg2
	public string StringArg2
	{
		get => (string)GetValue( StringArg2Property );
		set => SetValue( StringArg2Property, value );
	}

	// Using a DependencyProperty as the backing store for StringArg2.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StringArg2Property = DependencyProperty.Register( nameof( StringArg2 ),
																							    typeof( string ),
																							    typeof( WebReport ),
																							    new FrameworkPropertyMetadata( "",
																														       StringArg2ChangedCallback ) );

	private static void StringArg2ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string Arg )
			R.Model.StringArg2 = Arg;
	}
#endregion

#region StringArg3
	public string StringArg3
	{
		get => (string)GetValue( StringArg3Property );
		set => SetValue( StringArg3Property, value );
	}

	// Using a DependencyProperty as the backing store for StringArg3.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StringArg3Property = DependencyProperty.Register( nameof( StringArg3 ),
																							    typeof( string ),
																							    typeof( WebReport ),
																							    new FrameworkPropertyMetadata( "",
																														       StringArg3ChangedCallback ) );

	private static void StringArg3ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string Arg )
			R.Model.StringArg3 = Arg;
	}
#endregion

#region StringArg4
	public string StringArg4
	{
		get => (string)GetValue( StringArg4Property );
		set => SetValue( StringArg4Property, value );
	}

	// Using a DependencyProperty as the backing store for StringArg4.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StringArg4Property = DependencyProperty.Register( nameof( StringArg4 ),
																							    typeof( string ),
																							    typeof( WebReport ),
																							    new FrameworkPropertyMetadata( "",
																														       StringArg4ChangedCallback ) );

	private static void StringArg4ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is string Arg )
			R.Model.StringArg4 = Arg;
	}
#endregion

#region DecimalArg1
	public decimal DecimalArg1
	{
		get => (decimal)GetValue( DecimalArg1Property );
		set => SetValue( DecimalArg1Property, value );
	}

	// Using a DependencyProperty as the backing store for DecimalArg1.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DecimalArg1Property = DependencyProperty.Register( nameof( DecimalArg1 ),
																							     typeof( decimal ),
																							     typeof( WebReport ),
																							     new FrameworkPropertyMetadata( 0M,
																														        DecimalArg1ChangedCallback ) );

	private static void DecimalArg1ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is decimal Arg )
			R.Model.DecimalArg1 = Arg;
	}
#endregion

#region DecimalArg2
	public decimal DecimalArg2
	{
		get => (decimal)GetValue( DecimalArg2Property );
		set => SetValue( DecimalArg2Property, value );
	}

	// Using a DependencyProperty as the backing store for DecimalArg2.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DecimalArg2Property = DependencyProperty.Register( nameof( DecimalArg2 ),
																							     typeof( decimal ),
																							     typeof( WebReport ),
																							     new FrameworkPropertyMetadata( 0M,
																														        DecimalArg2ChangedCallback ) );

	private static void DecimalArg2ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is decimal Arg )
			R.Model.DecimalArg2 = Arg;
	}
#endregion

#region DecimalArg3
	public decimal DecimalArg3
	{
		get => (decimal)GetValue( DecimalArg3Property );
		set => SetValue( DecimalArg3Property, value );
	}

	// Using a DependencyProperty as the backing store for DecimalArg3.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DecimalArg3Property = DependencyProperty.Register( nameof( DecimalArg3 ),
																							     typeof( decimal ),
																							     typeof( WebReport ),
																							     new FrameworkPropertyMetadata( 0M,
																														        DecimalArg3ChangedCallback ) );

	private static void DecimalArg3ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is decimal Arg )
			R.Model.DecimalArg3 = Arg;
	}
#endregion

#region DecimalArg4
	public decimal DecimalArg4
	{
		get => (decimal)GetValue( DecimalArg4Property );
		set => SetValue( DecimalArg4Property, value );
	}

	// Using a DependencyProperty as the backing store for DecimalArg4.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DecimalArg4Property = DependencyProperty.Register( nameof( DecimalArg4 ),
																							     typeof( decimal ),
																							     typeof( WebReport ),
																							     new FrameworkPropertyMetadata( 0M,
																														        DecimalArg4ChangedCallback ) );

	private static void DecimalArg4ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is decimal Arg )
			R.Model.DecimalArg4 = Arg;
	}
#endregion

#region DateTimeArg1
	public DateTimeOffset DateTimeArg1
	{
		get => (DateTimeOffset)GetValue( DateTimeArg1Property );
		set => SetValue( DateTimeArg1Property, value );
	}

	// Using a DependencyProperty as the backing store for DateTimeArg1.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DateTimeArg1Property = DependencyProperty.Register( nameof( DateTimeArg1 ),
																								  typeof( DateTimeOffset ),
																								  typeof( WebReport ),
																								  new FrameworkPropertyMetadata( DateTimeOffset.MinValue,
																															     DateTimeArg1ChangedCallback ) );

	private static void DateTimeArg1ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is DateTimeOffset Arg )
			R.Model.DateTimeArg1 = Arg;
	}
#endregion

#region DateTimeArg2
	public DateTimeOffset DateTimeArg2
	{
		get => (DateTimeOffset)GetValue( DateTimeArg2Property );
		set => SetValue( DateTimeArg2Property, value );
	}

	// Using a DependencyProperty as the backing store for DateTimeArg2.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DateTimeArg2Property = DependencyProperty.Register( nameof( DateTimeArg2 ),
																								  typeof( DateTimeOffset ),
																								  typeof( WebReport ),
																								  new FrameworkPropertyMetadata( DateTimeOffset.MinValue,
																															     DateTimeArg2ChangedCallback ) );

	private static void DateTimeArg2ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is DateTimeOffset Arg )
			R.Model.DateTimeArg2 = Arg;
	}
#endregion

#region DateTimeArg3
	public DateTimeOffset DateTimeArg3
	{
		get => (DateTimeOffset)GetValue( DateTimeArg3Property );
		set => SetValue( DateTimeArg3Property, value );
	}

	// Using a DependencyProperty as the backing store for DateTimeArg3.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DateTimeArg3Property = DependencyProperty.Register( nameof( DateTimeArg3 ),
																								  typeof( DateTimeOffset ),
																								  typeof( WebReport ),
																								  new FrameworkPropertyMetadata( DateTimeOffset.MinValue,
																															     DateTimeArg3ChangedCallback ) );

	private static void DateTimeArg3ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is DateTimeOffset Arg )
			R.Model.DateTimeArg3 = Arg;
	}
#endregion

#region DateTimeArg4
	public DateTimeOffset DateTimeArg4
	{
		get => (DateTimeOffset)GetValue( DateTimeArg4Property );
		set => SetValue( DateTimeArg4Property, value );
	}

	// Using a DependencyProperty as the backing store for DateTimeArg4.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DateTimeArg4Property = DependencyProperty.Register( nameof( DateTimeArg4 ),
																								  typeof( DateTimeOffset ),
																								  typeof( WebReport ),
																								  new FrameworkPropertyMetadata( DateTimeOffset.MinValue,
																															     DateTimeArg4ChangedCallback ) );

	private static void DateTimeArg4ChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is DateTimeOffset Arg )
			R.Model.DateTimeArg4 = Arg;
	}
#endregion

#region Content
	public new UIElement Content
	{
		get => (UIElement)GetValue( ContentProperty );
		set => SetValue( ContentProperty, value );
	}

	// Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty ContentProperty = DependencyProperty.Register( nameof( Content ),
																							     typeof( UIElement ),
																							     typeof( WebReport ),
																							     new FrameworkPropertyMetadata( null,
																														        ContentPropertyChangedCallback
																														      ) );

	private static void ContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport R && e.NewValue is UIElement Ui )
		{
			var B = R.ContentBorder;
			B.Child = Ui;
			B.UpdateLayout();
		}
	}
#endregion

#region Save Pdf
	private readonly bool RestoringPdfFolder;

	public string PdfSaveFolder
	{
		get => (string)GetValue( PdfSaveFolderProperty );
		set => SetValue( PdfSaveFolderProperty, value );
	}

	// Using a DependencyProperty as the backing store for PdfSaveFolder.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty PdfSaveFolderProperty = DependencyProperty.Register(
																								  nameof( PdfSaveFolder ),
																								  typeof( string ),
																								  typeof( WebReport ),
																								  new FrameworkPropertyMetadata( @"\", PdfSaveFolderChangedCallback ) );

	private static void PdfSaveFolderChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is WebReport {RestoringPdfFolder: false} W && e.NewValue is string Folder )
			W.Model.PdfFolder = Folder;
	}

	private void SelectPdfFolder_Click( object sender, RoutedEventArgs e )
	{
		var Dlg = new FolderBrowserDialog
				  {
					  RootFolder          = Environment.SpecialFolder.MyComputer,
					  Description         = "Select Pdf Folder",
					  ShowNewFolderButton = true,
					  SelectedPath        = PdfSaveFolder
				  };
		var Result = Dlg.ShowDialog();

		if( Result == DialogResult.OK )
			PdfSaveFolder = Dlg.SelectedPath;
	}
#endregion
}