﻿using System.Windows.Controls;

namespace ViewModels.Reports.WebReports;

/// <summary>
///     Interaction logic for WebReports.xaml
/// </summary>
public partial class WebReports : Page
{
    private readonly WebReportsModel Model;

    public WebReports()
    {
        InitializeComponent();
        Model = (WebReportsModel)Root.DataContext;

        WebView2.EnsureCoreWebView2Async();

        Model.OnPageAvailable = htmlFileName =>
                                {
                                    try
                                    {
                                        WebView2.Source = new Uri($"file://{htmlFileName}");
                                    }
                                    catch (Exception Exception)
                                    {
                                        Console.WriteLine(Exception);
                                    }
                                };
    }

    private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
    {

    }


    private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
        if (Root.DataContext is WebReportsModel && sender is DatePicker dp)
        {
            if (dp.SelectedDate != null)
            {
                Model.DateFrom = (DateTimeOffset)dp.SelectedDate;
            }
        }
    }

    private void DatePicker_SelectedDateChanged_1(object sender, SelectionChangedEventArgs e)
    {
        if (Root.DataContext is WebReportsModel && sender is DatePicker dp)
        {
            if (dp.SelectedDate != null)
            {
                Model.DateTo = (DateTimeOffset)dp.SelectedDate;
            }
        }
    }

    // STATUS drop down

    private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (sender is ComboBox cb && Root.DataContext is WebReportsModel Model)
        {
            if (cb.SelectedIndex > -1)
            {
                var label = (string)cb.SelectedItem;

                // Special case for PICKED_UP
                if (label.IndexOf(" ") > -1)
                    label = label.Replace(" ", "_");
                var status = (STATUS)Enum.Parse(typeof(STATUS), label);
                Model.SelectedStartStatus = status;
            }
        }
    }

    private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
    {
        if (sender is ComboBox cb && Root.DataContext is WebReportsModel Model)
        {
            if (cb.SelectedIndex > -1)
            {
                var label = (string)cb.SelectedItem;

                // Special case for PICKED_UP
                if (label.IndexOf(" ") > -1)
                    label = label.Replace(" ", "_");
                var status = (STATUS)Enum.Parse(typeof(STATUS), label);
                Model.SelectedEndStatus = status;
            }
        }
    }

    private void ComboBox_SelectionChanged_truck(object sender, SelectionChangedEventArgs e)
    {
        if (sender is ComboBox cb && Root.DataContext is WebReportsModel Model)
        {
            Model.SelectedTruck = (string)cb.SelectedItem;
        }
    }

    private void ComboBox_SelectionChanged_2(object sender, SelectionChangedEventArgs e)
    {
        if (sender is ComboBox cb && Root.DataContext is WebReportsModel model)
        {
            model.CompanyName = (string)cb.SelectedItem;
        }
    }


    //private void ComboBox_SelectionChanged_Account(object sender, SelectionChangedEventArgs e)
    //{
    //    if (sender is ComboBox cb && Root.DataContext is WebReportsModel Model)
    //    {
    //        var label = (string)cb.SelectedItem;
    //        Model.SelectedAccount = label;
    //    }
    //}
}
