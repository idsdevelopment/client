﻿#nullable enable

using ViewModels.Trips.Boards.Common;
using ViewModels.Trips.ImportShipments;
using File = System.IO.File;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

namespace ViewModels.RouteShipments;

internal class RouteShipmentsModel : ViewModelBase
{
	public static readonly string PROGRAM = "Route Shipments (" + Globals.CurrentVersion.APP_VERSION + ")";

	public bool Loaded
	{
		get { return Get( () => Loaded, false ); }
		set { Set( () => Loaded, value ); }
	}

	public RouteShipments? View;

	public void SetView( RouteShipments rs )
	{
		View = rs;
	}

	protected override async void OnInitialised()
	{
		base.OnInitialised();

		Loaded = false;
		LoadSettings();

		DisplayTrip.SelectedBackground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedBackgroundColourBrush" );
		DisplayTrip.SelectedForeground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedForegroundColourBrush" );

		try
		{
			Trips.Clear();

			if( !IsInDesignMode )
			{
				var STemp = await Azure.Client.RequestGetServiceLevelsDetailed();

				Drivers = new ObservableCollection<Driver>( from D in await Azure.Client.RequestGetDrivers()
				                                            select new Driver
				                                                   {
					                                                   StaffId     = D.StaffId,
					                                                   DisplayName = $"{D.FirstName} {D.LastName}".Trim().Capitalise()
				                                                   } );

				foreach( var ServiceLevel in STemp )
					ServiceLevels.Add( ServiceLevel.NewName, new ServiceLevel( ServiceLevel ) );

				DataGridVisible = true;
				Loaded          = true;
			}
			else
				Loaded = true;
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( E );
		}
	}

	public override void OnArgumentChange( object? selectedTrips )
	{
		if( selectedTrips is List<DisplayTrip> Trps )
		{
			var Temp = new List<DisplayTrip>();
			Temp.AddRange( Trps );
			Trips = LoadTrips( Temp );
			DictTripIdsAndDrivers.Clear();

			foreach( var dt in Trips )
			{
				Logging.WriteLogLine( "DEBUG TripId: " + dt.TripId + " Driver: " + dt.Driver );

				if( !DictTripIdsAndDrivers.ContainsKey( dt.TripId ) )
					DictTripIdsAndDrivers.Add( dt.TripId, dt.Driver );
			}
		}
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

#region Data
	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/1701216257/IDS+Route" ); }
	}

	public bool EnableAssignDriver
	{
		get { return Get( () => EnableAssignDriver, false ); }
		set { Set( () => EnableAssignDriver, value ); }
	}


	public bool EnableDelete
	{
		get { return Get( () => EnableDelete, false ); }
		set { Set( () => EnableDelete, value ); }
	}


	public bool EnableRoute
	{
		get { return Get( () => EnableRoute, false ); }
		set { Set( () => EnableRoute, value ); }
	}

	public static List<string> AllColumnsInOrder = new()
	                                               {
		                                               "ColumnDriver",
		                                               "ColumnTripId",
		                                               "ColumnAcct",
		                                               "ColumnCompanyName",
		                                               "ColumnAcctNotes",
		                                               "ColumnCallDate",
		                                               "ColumnCZ",
		                                               "ColumnPickup",
		                                               "ColumnPickupSuite",

		                                               "ColumnPickupStreet",
		                                               "ColumnPickupCity",
		                                               "ColumnPickupProvState",
		                                               "ColumnPickupCountry",
		                                               "ColumnPickupPostalZip",
		                                               "ColumnPickupAddressNotes",
		                                               "ColumnPickupNotes",
		                                               "ColumnPZ",
		                                               "ColumnDelivery",
		                                               "ColumnDeliverySuite",

		                                               "ColumnDeliveryStreet",
		                                               "ColumnDeliveryCity",
		                                               "ColumnDeliveryProvState",
		                                               "ColumnDeliveryCountry",
		                                               "ColumnDeliveryPostalZip",
		                                               "ColumnDeliveryAddressNotes",
		                                               "ColumnDeliveryNotes",
		                                               "ColumnDZ",
		                                               "ColumnServ",
		                                               "ColumnPkg",

		                                               "ColumnReady",
		                                               "ColumnDue",
		                                               "ColumnPcs",
		                                               "ColumnWt"
	                                               };

	// ReSharper disable once UnusedMember.Local
	private static readonly Dictionary<string, string> DictionaryColumns = new()
	                                                                       {
		                                                                       {"ColumnDriver", "driver"},
		                                                                       {"ColumnTripId", "tripId"},
		                                                                       {"ColumnAcct", "accountId"},
		                                                                       {"ColumnCompanyName", "BillingCompanyName"},
		                                                                       {"ColumnAcctNotes", "BillingNotes"},
		                                                                       {"ColumnCallDate", "CallTime"},
		                                                                       {"ColumnCZ", "CurrentZone"},

		                                                                       {"ColumnPickup", "PickupCompanyName"},
		                                                                       {"ColumnPickupSuite", "PickupAddressSuite"},
		                                                                       {"ColumnPickupStreet", "PickupAddressAddressLine1"},
		                                                                       {"ColumnPickupCity", "PickupAddressCity"},
		                                                                       {"ColumnPickupProvState", "PickupAddressRegion"},
		                                                                       {"ColumnPickupCountry", "PickupAddressCountry"},
		                                                                       {"ColumnPickupPostalZip", "PickupAddressPostalCode"},
		                                                                       {"ColumnPickupAddressNotes", "PickupAddressNotes"},
		                                                                       {"ColumnPickupNotes", "PickupNotes"},
		                                                                       {"ColumnPZ", "PickupZone"},

		                                                                       {"ColumnDelivery", "DeliveryCompanyName"},
		                                                                       {"ColumnDeliverySuite", "DeliveryAddressSuite"},
		                                                                       {"ColumnDeliveryStreet", "DeliveryAddressAddressLine1"},
		                                                                       {"ColumnDeliveryCity", "DeliveryAddressCity"},
		                                                                       {"ColumnDeliveryProvState", "DeliveryAddressRegion"},
		                                                                       {"ColumnDeliveryCountry", "DeliveryAddressCountry"},
		                                                                       {"ColumnDeliveryPostalZip", "DeliveryAddressPostalCode"},
		                                                                       {"ColumnDeliveryAddressNotes", "DeliveryAddressNotes"},
		                                                                       {"ColumnDeliveryNotes", "DeliveryNotes"},
		                                                                       {"ColumnDZ", "DeliveryZone"},

		                                                                       {"ColumnServ", "ServiceLevel"},
		                                                                       {"ColumnPkg", "PackageType"},
		                                                                       {"ColumnReady", "ReadyTime"},
		                                                                       {"ColumnDue", "DueTime"},
		                                                                       {"ColumnPcs", "Pieces"},
		                                                                       {"ColumnWt", "Weight"}
	                                                                       };

	public readonly Dictionary<string, Visibility> DictionaryColumnVisibility = new()
	                                                                            {
		                                                                            {"ColumnDriver", Visibility.Visible},
		                                                                            {"ColumnTripId", Visibility.Hidden},
		                                                                            {"ColumnAcct", Visibility.Hidden},
		                                                                            {"ColumnCompanyName", Visibility.Hidden}, // New
		                                                                            {"ColumnAcctNotes", Visibility.Hidden},   // New
		                                                                            {"ColumnCallDate", Visibility.Hidden},    // New
		                                                                            {"ColumnCZ", Visibility.Hidden},          // New
		                                                                            {"ColumnPickup", Visibility.Hidden},
		                                                                            {"ColumnPickupSuite", Visibility.Hidden}, // New

		                                                                            {"ColumnPickupStreet", Visibility.Hidden},
		                                                                            {"ColumnPickupCity", Visibility.Hidden},
		                                                                            {"ColumnPickupProvState", Visibility.Hidden},    // New
		                                                                            {"ColumnPickupCountry", Visibility.Hidden},      // New
		                                                                            {"ColumnPickupPostalZip", Visibility.Hidden},    // New
		                                                                            {"ColumnPickupAddressNotes", Visibility.Hidden}, // New
		                                                                            {"ColumnPickupNotes", Visibility.Hidden},        // New
		                                                                            {"ColumnPZ", Visibility.Hidden},                 // New
		                                                                            {"ColumnDelivery", Visibility.Hidden},
		                                                                            {"ColumnDeliverySuite", Visibility.Hidden}, // New

		                                                                            {"ColumnDeliveryStreet", Visibility.Hidden},
		                                                                            {"ColumnDeliveryCity", Visibility.Hidden},
		                                                                            {"ColumnDeliveryProvState", Visibility.Hidden},    // New
		                                                                            {"ColumnDeliveryCountry", Visibility.Hidden},      // New
		                                                                            {"ColumnDeliveryPostalZip", Visibility.Hidden},    // New
		                                                                            {"ColumnDeliveryAddressNotes", Visibility.Hidden}, // New
		                                                                            {"ColumnDeliveryNotes", Visibility.Hidden},        // New
		                                                                            {"ColumnDZ", Visibility.Hidden},                   // New
		                                                                            {"ColumnServ", Visibility.Hidden},
		                                                                            {"ColumnPkg", Visibility.Hidden},

		                                                                            {"ColumnReady", Visibility.Hidden},
		                                                                            {"ColumnDue", Visibility.Hidden},
		                                                                            {"ColumnPcs", Visibility.Hidden},
		                                                                            {"ColumnWt", Visibility.Hidden}
	                                                                            };

	//	private readonly List<Company> CurrentCompanies = new();

	public CompanyDetailList? CurrentDetailList;


	public string CurrentAccountId
	{
		get { return Get( () => CurrentAccountId, "" ); }
		set { Set( () => CurrentAccountId, value ); }
	}
/*
		//private readonly Dictionary<string, List<CompanyAddress>> DictAddresses = new();
		private readonly Dictionary<string, List<Company>> DictCompanies = new();

		private readonly MemoryCache<string, CompanyDetailList> CompanyAddressCache = new();
*/

	//private async Task LoadCompanies(DisplayTrip trip)
	// ReSharper disable once UnusedParameter.Local
	private void LoadCompanies( TripUpdate _ )
	{
		CurrentDetailList = null;
		Loaded            = true;
/*
			try
			{
				Loaded = false;

				var AccountId = trip.AccountId;

				if( !IsInDesignMode && ( AccountId != CurrentAccountId ) )
				{
					CurrentAccountId = AccountId;

					Logging.WriteLogLine( "Loading addresses for " + trip.AccountId + "..." );

					var (Ok, CompanyDetailList) = CompanyAddressCache.TryGetValue( CurrentAccountId );

					if( !Ok )
					{
						var Temp = await Azure.Client.RequestGetCustomerCompaniesDetailed( AccountId );
						CurrentDetailList = Temp;
						CompanyAddressCache.AddSliding( CurrentAccountId, Temp, TimeSpan.FromMinutes( 5 ) );
					}
					else
						CurrentDetailList = CompanyDetailList;

					var SdExtended = ( from D in CurrentDetailList
					                   let CompanyName = D.Company.CompanyName.Trim().ToLower()
					                   where CompanyName.IsNotNullOrWhiteSpace()
					                   orderby CompanyName
					                   group new
					                         {
						                         CompanyName,
						                         D.Company
					                         } by CompanyName
					                   into CoNames // Remove Duplicates
					                   select CoNames.First() ).ToDictionary( d => d.CompanyName, d => d.Company );

					Dispatcher.Invoke( () =>
					                   {
						                   foreach( var E in SdExtended )
						                   {
							                   var Value = E.Value;

							                   Logging.WriteLogLine( "DEBUG ca: " + Value.CompanyName + ", LocationBarcode: " + Value.LocationBarcode );
							                   CurrentCompanies.Add( Value );
						                   }

						                   if( !DictCompanies.ContainsKey( CurrentAccountId ) )
							                   DictCompanies.Add( CurrentAccountId, new List<Company>() );
						                   DictCompanies[ CurrentAccountId ] = CurrentCompanies;

						                   Loaded = true;
					                   } );
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "Exception thrown fetching addresses:\n" + E );
			}
*/
	}
#endregion

#region Map
	public string BingMapApiKey
	{
		get { return Get( () => BingMapApiKey, "" ); }
		set { Set( () => BingMapApiKey, value ); }
	}

	public async Task GetBingMapApiKey()
	{
		//Logging.WriteLogLine("DEBUG Getting map api keys");

		if( !IsInDesignMode )
		{
			var Keys = await Azure.Client.RequestGetMapsApiKeys();

			if( Keys is not null )
			{
				var Key = Keys.BingMapsKey;

				//Logging.WriteLogLine("DEBUG BingMapsKey: " + key);
				BingMapApiKey = Key;
			}
		}
	}

	public readonly string Html = MapHelper.Html;

	public string GetMapHtml()
	{
		var StrHtml = Html.Replace( "[BING_MAPS_KEY]", BingMapApiKey );

		return StrHtml;
	}


	public bool IsShortestRoute
	{
		get { return Get( () => IsShortestRoute, false ); }
		set { Set( () => IsShortestRoute, value ); }
	}


	public bool IsAvoidTolls
	{
		get { return Get( () => IsAvoidTolls, false ); }
		set { Set( () => IsAvoidTolls, value ); }
	}


	public bool IsAvoidHighways
	{
		get { return Get( () => IsAvoidHighways, false ); }
		set { Set( () => IsAvoidHighways, value ); }
	}

	public bool IsAvoidFerries
	{
		get { return Get( () => IsAvoidFerries, false ); }
		set { Set( () => IsAvoidFerries, value ); }
	}


	public bool IsByDeliveryAddress
	{
		get { return Get( () => IsByDeliveryAddress, false ); }
		set { Set( () => IsByDeliveryAddress, value ); }
	}
#endregion

#region DataGrid
	public bool DataGridVisible
	{
		get { return Get( () => DataGridVisible, IsInDesignMode ); }
		set { Set( () => DataGridVisible, value ); }
	}

	public ObservableCollection<Driver> Drivers
	{
		get { return Get( () => Drivers, new ObservableCollection<Driver>() ); }
		set { Set( () => Drivers, value ); }
	}

	private readonly Dictionary<string, ServiceLevel> ServiceLevels = new();

	private ServiceLevel GetServiceLevel( string s )
	{
		if( !ServiceLevels.TryGetValue( s, out var Sl ) )
			Sl = new ServiceLevel();

		return Sl;
	}

	public Dictionary<string, DisplayTrip> MappedTrips { get; } = new();

	public ObservableCollection<DisplayTrip> Trips
	{
		get { return Get( () => Trips, new ObservableCollection<DisplayTrip>() ); }
		set { Set( () => Trips, value ); }
	}

	public Dictionary<string, string> DictTripIdsAndDrivers { get; } = new();


	// [DependsUpon(nameof(Trips))]
	public void WhenTripsChanges()
	{
		if( Trips.Count == 0 )
		{
			if( View is not null )
				Dispatcher.Invoke( () =>
				                   {
					                   MapHelper.RemoveAllTrips( View.webView );
				                   } );
		}
		else
		{
			foreach( var Trip in Trips )
			{
				if( !MappedTrips.ContainsKey( Trip.TripId ) )
				{
					var Unused = FindStringResource( "DriversMapLabelTotalWeightPrefix" ) + Trip.Weight + "<br/>"
					             + FindStringResource( "DriversMapLabelTotalPiecesPrefix" ) + Trip.Pieces + "<br/>"
					             + FindStringResource( "DriversMapLabelServiceLevelPrefix" ) + Trip.ServiceLevel + "<br/>";
				}
			}
		}
	}

	public DisplayTrip? SelectedTrip
	{
		get { return Get( () => SelectedTrip, new DisplayTrip() ); }
		set { Set( () => SelectedTrip, value ); }
	}

	private DisplayTrip? PreviousSelectedTrip;

	[DependsUpon( nameof( SelectedTrip ) )]
	public void WhenSelectedTripChanges()
	{
		if( PreviousSelectedTrip is not null )
			PreviousSelectedTrip.Selected = false;

		PreviousSelectedTrip = SelectedTrip;

		if( SelectedTrip is not null )
		{
			SelectedTrip.Selected = true;
			var Drvr = SelectedTrip.Driver;

			var Ok = Drvr.IsNotNullOrWhiteSpace();

			if( Ok )
				Ok = Drivers.Any( driver => driver.StaffId == Drvr );

			var Trip = SelectedTrip;
			Trip.EnableAccept = Ok;
		}

		EnableAssignDriver = Trips.Any( displayTrip => displayTrip.EnableAccept );
	}

	public List<DisplayTrip>? SelectedTrips
	{
		get { return Get( () => SelectedTrips, new List<DisplayTrip>() ); }
		set { Set( () => SelectedTrips, value ); }
	}

	[DependsUpon( nameof( SelectedTrips ) )]
	public void WhenSelectedTripsChanges()
	{
		if( SelectedTrips is not null )
			EnableDelete = SelectedTrips.Count > 0;
	}
#endregion

#region Actions
	public async void Execute_AssignDriver()
	{
		var Tps = Trips;

		var UpdateTrips = ( from T in Tps
		                    where T.EnableAccept
		                    group T by new
		                               {
			                               T.Driver,
			                               T.Status2,
			                               T.Status3
		                               }
		                    into G
		                    select G ).ToDictionary( g => g.Key, g => g.ToList() );

		foreach( var T in UpdateTrips )
		{
			var K = T.Key;

			await UpdateStatusAndRemove( T.Value, STATUS.DISPATCHED, K.Status2, K.Status3, K.Driver );

			var trips = UpdateTrips[ K ];

			foreach( var dt in trips )
			{
				if( !DictTripIdsAndDrivers.ContainsKey( dt.TripId ) )
					DictTripIdsAndDrivers.Add( dt.TripId, string.Empty );
				DictTripIdsAndDrivers[ dt.TripId ] = dt.Driver;
			}
		}

		//TripCount = Tps.Count;
	}

	public void Execute_ClearSelection()
	{
		if( SelectedTrips is not null )
		{
			//foreach( var Trip in SelectedTrips )
			//	Trip.Driver = string.Empty;
			SelectedTrip = null;
			SelectedTrips.Clear();

			RaisePropertyChanged( nameof( SelectedTrip ) );
			RaisePropertyChanged( nameof( SelectedTrips ) );
		}
	}

	public async Task<(bool ok, List<string> messages)> RouteShipments()
	{
		var Result = ( Ok: true, Messages: new List<string>() );
		var S      = SelectedTrips;

		if( View is not null && S is {Count: > 0} && CheckTripsForCommonPickupAddress() )
		{
			var St = new List<DisplayTrip>( S );

			var TravelMode = IsShortestRoute ? RouteBase.TRAVEL_MODE.SHORTEST : RouteBase.TRAVEL_MODE.FASTEST;

			if( IsAvoidFerries )
				TravelMode |= RouteBase.TRAVEL_MODE.AVOID_FERRIES;

			if( IsAvoidHighways )
				TravelMode |= RouteBase.TRAVEL_MODE.AVOID_MOTORWAYS;

			if( IsAvoidTolls )
				TravelMode |= RouteBase.TRAVEL_MODE.AVOID_TOLL_ROADS;

			var Addresses = new RouteOptimisationCompanyAddresses
			                {
				                Optimise   = true,
				                TravelMode = TravelMode
			                };

			Company AddPickup( TripUpdate t )
			{
				var Company = new Company
				              {
					              CompanyName  = t.PickupCompanyName,
					              Suite        = t.PickupAddressSuite,
					              AddressLine1 = t.PickupAddressAddressLine1,
					              AddressLine2 = t.PickupAddressAddressLine2,
					              City         = t.PickupAddressCity,
					              Region       = t.PickupAddressRegion,
					              Country      = t.PickupAddressCountry,
					              CountryCode  = t.PickupAddressCountryCode,
					              Vicinity     = t.PickupAddressVicinity,
					              Latitude     = (decimal)t.PickupLatitude,
					              Longitude    = (decimal)t.PickupLongitude
				              };
				Addresses.Companies.Add( Company );
				return Company;
			}

			Company AddDelivery( TripUpdate t )
			{
				var Company = new Company
				              {
					              CompanyName  = t.DeliveryCompanyName,
					              Suite        = t.DeliveryAddressSuite,
					              AddressLine1 = t.DeliveryAddressAddressLine1,
					              AddressLine2 = t.DeliveryAddressAddressLine2,
					              City         = t.DeliveryAddressCity,
					              Region       = t.DeliveryAddressRegion,
					              Country      = t.DeliveryAddressCountry,
					              CountryCode  = t.DeliveryAddressCountryCode,
					              Vicinity     = t.DeliveryAddressVicinity,
					              Latitude     = (decimal)t.DeliveryLatitude,
					              Longitude    = (decimal)t.DeliveryLongitude
				              };
				Addresses.Companies.Add( Company );
				return Company;
			}

			var First  = true;
			var Driver = "";

			var CoTrips = new Dictionary<string, List<DisplayTrip>>();

			static string CoKey( CompanyBase co )
			{
				return $"{co.CompanyName.Trim()}{co.AddressLine1.Trim()}".ToUpper();
			}

			void AddCompanyTrips( CompanyBase co, DisplayTrip trip )
			{
				var Key = CoKey( co );

				if( !CoTrips.TryGetValue( Key, out var TList ) )
					CoTrips[ Key ] = TList = new List<DisplayTrip>();

				TList.Add( trip );
			}

			var PickupTrips = new List<DisplayTrip>( St );

			foreach( var T in St )
			{
				if( T.Driver.IsNullOrWhiteSpace() )
				{
					Result.Ok = false;
					Result.Messages.Add( FindStringResource( "RouteShipmentsErrorsMissingDrivers" ) );
					continue;
				}

				if( First )
				{
					First  = false;
					Driver = T.Driver;
					var Co = AddPickup( T );
					CoTrips[ CoKey( Co ) ] = PickupTrips;
				}
				else if( Driver != T.Driver )
				{
					Result.Ok = false;
					Result.Messages.Add( FindStringResource( "RouteShipmentsErrorsMultipleDrivers" ) );
					continue;
				}
				AddCompanyTrips( AddDelivery( T ), T );
			}

			if( Result.Ok )
			{
				var Route = await Azure.Client.RequestDistancesWithAddresses( Addresses );

				if( Route is not null )
				{
					MapHelper.RemoveAllDrivers( View.webView );
					MapHelper.RemoveAllTrips( View.webView );
					MapHelper.RemoveAllLines( View.webView );

					MapHelper.RemoveEverything( View.webView );

					var DriversMapLabelTotalWeightPrefix  = FindStringResource( "DriversMapLabelTotalWeightPrefix" );
					var DriversMapLabelTotalPiecesPrefix  = FindStringResource( "DriversMapLabelTotalPiecesPrefix" );
					var DriversMapLabelServiceLevelPrefix = FindStringResource( "DriversMapLabelServiceLevelPrefix" );

					GpsPoint? FocusPoint = null;

					void DoCompany( bool isPickup, CompanyBase co )
					{
						var Key = CoKey( co );

						if( CoTrips!.TryGetValue( Key, out var DisplayTrips ) )
						{
							var TripIds = string.Join( ",", from T in DisplayTrips
							                                select T.TripId );

							GpsPoint GpsPoint = new()
							                    {
								                    Latitude  = (double)co.Latitude,
								                    Longitude = (double)co.Longitude
							                    };

							if( isPickup )
							{
								FocusPoint = GpsPoint;
								MapHelper.MapPUTrip( TripIds, co.CompanyName, GpsPoint, View!.webView );
							}
							else
								MapHelper.MapDelTrip( TripIds, co.CompanyName, GpsPoint, View!.webView );

							var FirstLine = true;
							var Content   = new StringBuilder();

							foreach( var DisplayTrip in DisplayTrips )
							{
								if( !FirstLine )
									Content.Append( "<br/>" );
								else
									FirstLine = false;

								Content.Append( $"{DisplayTrip.TripId}<br/>"
								                + $"{DriversMapLabelTotalWeightPrefix}{DisplayTrip.Weight}&nbsp&nbsp{DriversMapLabelTotalPiecesPrefix}{DisplayTrip.Pieces}&nbsp&nbsp{DriversMapLabelServiceLevelPrefix}{DisplayTrip.ServiceLevel}<br/>"
								              );
							}

							if( isPickup )
								MapHelper.MapPUTripInfobox( "", Content.ToString(), GpsPoint, View.webView );
							else
								MapHelper.MapDelTripInfobox( "", Content.ToString(), GpsPoint, View.webView );
						}
					}

					First = true;

					foreach( var Leg in Route )
					{
						if( First )
							DoCompany( true, Leg.FromCompany );

						DoCompany( false, Leg.ToCompany );

						// Now map
						MapHelper.MapRoutes( Leg.Route, View.webView );

						First = false;
					}

					if( FocusPoint is not null )
						MapHelper.CentreMapOnPoint( FocusPoint, View.webView );
				}

				Result.Messages.Add( FindStringResource( "RouteShipmentsSuccess" )
				                     .Replace( "@1", $"{St.Count}" )
				                     .Replace( "@2", Driver ) );
			}
		}
		else
		{
			Result.Ok = false;

			if( View == null )
				Result.Messages.Add( "View is NULL" );

			if( IsByDeliveryAddress )
			{
				// Not all trips have the same pickup address
				Result.Messages.Add( FindStringResource( "RouteShipmentsErrorsPickupAddressesNotTheSame" ) );
			}
		}

		return Result;
	}


	//private (bool ok, List<string> messages) CheckTripsForCommonPickupAddress()
	private bool CheckTripsForCommonPickupAddress()
	{
		var          ok       = true;
		List<string> messages = new();

		bool CompareLocationBarcodes()
		{
			var ok = true;

			Dictionary<string, string> lbs = new();

			foreach( var dt in SelectedTrips )
			{
				// Barcodes can't be empty
				if( dt.Location.IsNullOrWhiteSpace() )
				{
					ok = false;
					break;
				}

				if( !lbs.ContainsKey( dt.Location ) )
					lbs.Add( dt.Location.TrimToLower(), string.Empty );
			}

			if( ok && ( lbs.Count > 1 ) )
			{
				// Location barcodes don't match
				ok = false;
			}

			return ok;
		}

		bool CompareGpsCoords()
		{
			var ok = true;

			Dictionary<decimal, decimal> lats  = new();
			Dictionary<decimal, decimal> longs = new();

			// ?
			foreach( var dt in SelectedTrips )
			{
				if( dt.PickupAddressLatitude > 0 )
				{
					if( !lats.ContainsKey( dt.PickupAddressLatitude ) )
						lats.Add( dt.PickupAddressLatitude, 0 );
				}
				else
				{
					ok = false;
					break;
				}

				if( dt.PickupAddressLongitude > 0 )
				{
					if( !lats.ContainsKey( dt.PickupAddressLongitude ) )
						lats.Add( dt.PickupAddressLongitude, 0 );
				}
				else
				{
					ok = false;
					break;
				}
			}

			if( ok && ( ( lats.Count > 1 ) || ( longs.Count > 1 ) ) )
				ok = false;

			return ok;
		}

		string Compress( string raw )
		{
			var compressed = string.Empty;
			compressed = raw.TrimToLower();
			compressed = compressed.Replace( " ", "" );

			return compressed;
		}

		if( IsByDeliveryAddress )
		{
			if( SelectedTrips != null )
			{
				ok = CompareLocationBarcodes();

				if( !ok )
				{
					//messages.Add("")
					ok = CompareGpsCoords(); // TODO - need to look at Address objects as well

					if( !ok )
					{
						//List<string> companyNames = new();
						//List<string> suites = new();
						List<string> streets   = new();
						List<string> cities    = new();
						List<string> regions   = new();
						List<string> countries = new();
						List<string> pcodes    = new();

						foreach( var dt in SelectedTrips )
						{
							var tmp = Compress( dt.PickupAddressPostalCode );

							if( !pcodes.Contains( tmp ) )
								pcodes.Add( tmp );
							tmp = Compress( dt.PickupAddressCountry );

							if( !countries.Contains( tmp ) )
								countries.Add( tmp );
							tmp = Compress( dt.PickupAddressRegion );

							if( !regions.Contains( tmp ) )
								regions.Add( tmp );
							tmp = Compress( dt.PickupAddressCity );

							if( !cities.Contains( tmp ) )
								cities.Add( tmp );
							tmp = Compress( dt.PickupAddressAddressLine1 );

							if( !streets.Contains( tmp ) )
								streets.Add( tmp );

							//tmp = Compress(dt.PickupAddressSuite);
							//if (!suites.Contains(tmp))
							//{
							//	suites.Add(tmp);
							//}
							//tmp = Compress(dt.PickupCompanyName);
							//if (!companyNames.Contains(tmp))
							//{
							//	companyNames.Add(tmp);
							//}
						}

						//if (pcodes.Count > 1 || countries.Count > 1 || regions.Count > 1 || cities.Count > 1 || streets.Count > 1 || suites.Count > 1 || companyNames.Count > 1)
						if( ( pcodes.Count > 1 ) || ( countries.Count > 1 ) || ( regions.Count > 1 ) || ( cities.Count > 1 ) || ( streets.Count > 1 ) )
							ok = false;
						else
							ok = true;
					}
				}
			}
		}

		//return (ok, messages);
		return ok;
	}

	public async void Execute_DeleteShipments()
	{
		if( SelectedTrips != null )
		{
			foreach( var dt in SelectedTrips )
			{
				if( DictTripIdsAndDrivers.ContainsKey( dt.TripId ) )
					DictTripIdsAndDrivers.Remove( dt.TripId );
			}

			await UpdateStatusAndRemove( SelectedTrips, STATUS.DELETED );
			SelectedTrip  = new DisplayTrip();
			SelectedTrips = null;
			RaisePropertyChanged( nameof( SelectedTrip ) );
			RaisePropertyChanged( nameof( SelectedTrips ) );
		}
	}

	public void Execute_ImportShipments()
	{
		var List = new ImportShipments( Application.Current.MainWindow ).GetTrips();

		if( List != null && List.Count > 0 )
		{
			var Dts = new List<DisplayTrip>( from T in List
			                                 orderby T.Status3 descending, T.CallTime
			                                 select new DisplayTrip( T, GetServiceLevel( T.ServiceLevel ) ) );
			Dispatcher.Invoke(() =>
			{
				Trips = LoadTrips( Dts );
			});
		}
	}

	private async Task UpdateStatusAndRemove( IEnumerable<DisplayTrip> trips, STATUS status, STATUS1 status1 = STATUS1.UNSET, STATUS2 status2 = STATUS2.UNSET, string driver = "" )
	{
		foreach( var Trip in trips )
		{
			switch( status )
			{
			case STATUS.DELETED:
				//await UpdateStatus(PROGRAM, status, status1, status2, TripIds, driver, TripUpdateStatus.BROADCAST.DISPATCH_BOARD);
				await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( Trip ) );
				break;

			case STATUS.DISPATCHED:
				//await UpdateStatus(PROGRAM, status, status1, status2, TripIds, driver, TripUpdateStatus.BROADCAST.DRIVERS_BOARD | TripUpdateStatus.BROADCAST.DRIVER | TripUpdateStatus.BROADCAST.DISPATCH_BOARD);
				await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( Trip ) );
				break;
			}

			RaisePropertyChanged( nameof( Trips ) );
		}
	}

	private static Trip UpdateBroadcast( Trip t )
	{
		switch( t.Status1 )
		{
		case STATUS.ACTIVE:
			t.BroadcastToDispatchBoard = true;
			break;

		case STATUS.DISPATCHED:
		case STATUS.PICKED_UP:
			t.BroadcastToDispatchBoard = true; // Shipment entry can also dispatch
			t.BroadcastToDriverBoard   = true;
			t.BroadcastToDriver        = true;
			break;
		}

		return t;
	}

	public ObservableCollection<DisplayTrip> LoadTrips( List<DisplayTrip> trips )
	{
		var Trps = new ObservableCollection<DisplayTrip>();

		if( trips.Count > 0 )
		{
			foreach( var Trip in trips )
			{
				Trps.Add( Trip );
				LoadCompanies( Trip );
			}
		}
		return Trps;
	}
#endregion

#region Location methods
/*
		private (string latitude, string longitude) GeolocateAddress( string addressLine1, string city, string region, string country, string postalCode )
		{
			var AddressQuery = BuildAddressQuery( country, region, city, postalCode, addressLine1 );

			//Logging.WriteLogLine("DEBUG addressQuery: " + addressQuery);
			var GeocodeRequest  = "http://dev.virtualearth.net/REST/v1/Locations?" + AddressQuery + "&o=xml&key=" + BingMapApiKey;
			var GeocodeResponse = GetXmlResponse( GeocodeRequest );

			var (Latitude, Longitude) = GetPointD( GeocodeResponse );

			//Logging.WriteLogLine("DEBUG latitude: " + latitude + ", longitude: " + longitude);

			return ( Latitude, Longitude );
		}
*/
/*
		private string BuildAddressQuery( string country, string region, string city, string postalCode, string addressLine )
		{
			// "countryRegion=AU&adminDistrict=Vic&locality=Kyabram";
			var query = "countryRegion=@1&adminDistrict=@2&locality=@3&postalCode=@4&addressLine=@5";

			return query.Replace( "@1", country.IsNotNullOrWhiteSpace() ? country : "" )
			            .Replace( "@2", region.IsNotNullOrWhiteSpace() ? region : "" )
			            .Replace( "@3", city.IsNotNullOrWhiteSpace() ? city : "" )
			            .Replace( "@4", postalCode.IsNotNullOrWhiteSpace() ? postalCode : "" )
			            .Replace( "@5", addressLine.IsNotNullOrWhiteSpace() ? addressLine : "" );
		}
*/

/*		
		/// <summary>
		///     Submit a REST Services or Spatial Data Services request and return the response
		/// </summary>
		/// <param name="requestUrl"></param>
		/// <returns></returns>
		private XmlDocument GetXmlResponse( string requestUrl )
		{
			var XmlDoc = new XmlDocument();

			//System.Diagnostics.Trace.WriteLine("Request URL (XML): " + requestUrl);

			if( WebRequest.Create( requestUrl ) is HttpWebRequest Request )
			{
				using var Response = Request.GetResponse() as HttpWebResponse;

				if( Response is not null )
				{
					if( Response.StatusCode != HttpStatusCode.OK )
						throw new Exception( $"Server error (HTTP {Response.StatusCode}: {Response.StatusDescription})." );

					var Stream = Response.GetResponseStream();

					if( Stream is not null )
						XmlDoc.Load( Stream );
				}
			}
			return XmlDoc;
		}
*/

/*
		/// <summary>
		///     Grabs the values directly from the document.
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <returns></returns>
		private (string latitude, string longitude) GetPointD( XmlDocument xmlDoc )
		{
			var Latitude  = "0.0";
			var Longitude = "0.0";

			//Create namespace manager
			var Nsmgr = new XmlNamespaceManager( xmlDoc.NameTable );
			Nsmgr.AddNamespace( "rest", "http://schemas.microsoft.com/search/local/ws/rest/v1" );

			//Get all locations in the response and then extract the coordinates for the top location
			var LocationElements = xmlDoc.SelectNodes( "//rest:Point", Nsmgr );

			if( LocationElements is not null && ( LocationElements.Count > 0 ) )
			{
				Latitude  = LocationElements[ 0 ].SelectSingleNode( ".//rest:Latitude", Nsmgr )?.InnerText ?? "0.0";
				Longitude = LocationElements[ 0 ].SelectSingleNode( ".//rest:Longitude", Nsmgr )?.InnerText ?? "0.0";
			}
			else
				Logging.WriteLogLine( "Error: no location found" );

			return ( Latitude, Longitude );
		}
*/
#endregion

#region Settings
	public static Settings? RouteShipmentsSettings { get; set; }


	//
	// Visibility properties for columns
	//

	public Visibility AccountVisibility
	{
		get { return Get( () => AccountVisibility, Visibility.Visible ); }
		set { Set( () => AccountVisibility, value ); }
	}


	public Visibility ConditionVisibility
	{
		get { return Get( () => ConditionVisibility, Visibility.Visible ); }
		set { Set( () => ConditionVisibility, value ); }
	}


	public Visibility CompanyNameVisibility
	{
		get { return Get( () => CompanyNameVisibility, Visibility.Visible ); }
		set { Set( () => CompanyNameVisibility, value ); }
	}

	public Visibility AccountNotesVisibility
	{
		get { return Get( () => AccountNotesVisibility, Visibility.Visible ); }
		set { Set( () => AccountNotesVisibility, value ); }
	}

	public Visibility CallDateVisibility
	{
		get { return Get( () => CallDateVisibility, Visibility.Visible ); }
		set { Set( () => CallDateVisibility, value ); }
	}

	public Visibility CurrentZoneVisibility
	{
		get { return Get( () => CurrentZoneVisibility, Visibility.Visible ); }
		set { Set( () => CurrentZoneVisibility, value ); }
	}

	public Visibility PickupCompanyVisibility
	{
		get { return Get( () => PickupCompanyVisibility, Visibility.Visible ); }
		set { Set( () => PickupCompanyVisibility, value ); }
	}

	public Visibility PickupSuiteVisibility
	{
		get { return Get( () => PickupSuiteVisibility, Visibility.Visible ); }
		set { Set( () => PickupSuiteVisibility, value ); }
	}

	public Visibility PickupStreetVisibility
	{
		get { return Get( () => PickupStreetVisibility, Visibility.Visible ); }
		set { Set( () => PickupStreetVisibility, value ); }
	}

	public Visibility PickupCityVisibility
	{
		get { return Get( () => PickupCityVisibility, Visibility.Visible ); }
		set { Set( () => PickupCityVisibility, value ); }
	}

	public Visibility PickupProvStateVisibility
	{
		get { return Get( () => PickupProvStateVisibility, Visibility.Visible ); }
		set { Set( () => PickupProvStateVisibility, value ); }
	}

	public Visibility PickupCountryVisibility
	{
		get { return Get( () => PickupCountryVisibility, Visibility.Visible ); }
		set { Set( () => PickupCountryVisibility, value ); }
	}

	public Visibility PickupPostalZipVisibility
	{
		get { return Get( () => PickupPostalZipVisibility, Visibility.Visible ); }
		set { Set( () => PickupPostalZipVisibility, value ); }
	}

	public Visibility PickupAddressNotesVisibility
	{
		get { return Get( () => PickupAddressNotesVisibility, Visibility.Visible ); }
		set { Set( () => PickupAddressNotesVisibility, value ); }
	}

	public Visibility PickupNotesVisibility
	{
		get { return Get( () => PickupNotesVisibility, Visibility.Visible ); }
		set { Set( () => PickupNotesVisibility, value ); }
	}

	public Visibility PickupZoneVisibility
	{
		get { return Get( () => PickupZoneVisibility, Visibility.Visible ); }
		set { Set( () => PickupZoneVisibility, value ); }
	}

	public Visibility DeliveryCompanyVisibility
	{
		get { return Get( () => DeliveryCompanyVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryCompanyVisibility, value ); }
	}

	public Visibility DeliverySuiteVisibility
	{
		get { return Get( () => DeliverySuiteVisibility, Visibility.Visible ); }
		set { Set( () => DeliverySuiteVisibility, value ); }
	}

	public Visibility DeliveryStreetVisibility
	{
		get { return Get( () => DeliveryStreetVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryStreetVisibility, value ); }
	}

	public Visibility DeliveryCityVisibility
	{
		get { return Get( () => DeliveryCityVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryCityVisibility, value ); }
	}

	public Visibility DeliveryProvStateVisibility
	{
		get { return Get( () => DeliveryProvStateVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryProvStateVisibility, value ); }
	}

	public Visibility DeliveryCountryVisibility
	{
		get { return Get( () => DeliveryCountryVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryCountryVisibility, value ); }
	}

	public Visibility DeliveryPostalZipVisibility
	{
		get { return Get( () => DeliveryPostalZipVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryPostalZipVisibility, value ); }
	}

	public Visibility DeliveryAddressNotesVisibility
	{
		get { return Get( () => DeliveryAddressNotesVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryAddressNotesVisibility, value ); }
	}

	public Visibility DeliveryNotesVisibility
	{
		get { return Get( () => DeliveryNotesVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryNotesVisibility, value ); }
	}

	public Visibility DeliveryZoneVisibility
	{
		get { return Get( () => DeliveryZoneVisibility, Visibility.Visible ); }
		set { Set( () => DeliveryZoneVisibility, value ); }
	}

	public Visibility ServiceLevelVisibility
	{
		get { return Get( () => ServiceLevelVisibility, Visibility.Visible ); }
		set { Set( () => ServiceLevelVisibility, value ); }
	}

	public Visibility PackageTypeVisibility
	{
		get { return Get( () => PackageTypeVisibility, Visibility.Visible ); }
		set { Set( () => PackageTypeVisibility, value ); }
	}

	public Visibility ReadyTimeVisibility
	{
		get { return Get( () => ReadyTimeVisibility, Visibility.Visible ); }
		set { Set( () => ReadyTimeVisibility, value ); }
	}

	public Visibility DueTimeVisibility
	{
		get { return Get( () => DueTimeVisibility, Visibility.Visible ); }
		set { Set( () => DueTimeVisibility, value ); }
	}

	public Visibility PiecesVisibility
	{
		get { return Get( () => PiecesVisibility, Visibility.Visible ); }
		set { Set( () => PiecesVisibility, value ); }
	}

	public Visibility WeightVisibility
	{
		get { return Get( () => WeightVisibility, Visibility.Visible ); }
		set { Set( () => WeightVisibility, value ); }
	}

	// End of Visibility

	public List<string> GetAllColumnsInOrder() => AllColumnsInOrder;

	public void SaveColumns( List<string> newColumns )
	{
		List<string> Keys = new();

		foreach( var Key in DictionaryColumnVisibility.Keys )
			Keys.Add( Key );

		foreach( var Key in Keys )
		{
			if( newColumns.Contains( Key ) )
			{
				Logging.WriteLogLine( "Setting column: " + Key + " to Visible" );
				DictionaryColumnVisibility[ Key ] = Visibility.Visible;
			}
			else
			{
				Logging.WriteLogLine( "Setting column: " + Key + " to Hidden" );
				DictionaryColumnVisibility[ Key ] = Visibility.Hidden;
			}
		}

		foreach( var Key in Keys )
		{
			switch( Key )
			{
			case "ColumnDriver":
			case "ColumnTripId":
				break;

			case "ColumnAcct":
				AccountVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnCompanyName":
				CompanyNameVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnAcctNotes":
				AccountNotesVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnCZ":
				CurrentZoneVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickup":
				PickupCompanyVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupSuite":
				PickupSuiteVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupStreet":
				PickupStreetVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupCity":
				PickupCityVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupProvState":
				PickupProvStateVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupCountry":
				PickupCountryVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupPostalZip":
				PickupPostalZipVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupAddressNotes":
				PickupAddressNotesVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPickupNotes":
				PickupNotesVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPZ":
				PickupZoneVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDelivery":
				DeliveryCompanyVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliverySuite":
				DeliverySuiteVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryStreet":
				DeliveryStreetVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryCity":
				DeliveryCityVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryProvState":
				DeliveryProvStateVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryCountry":
				DeliveryCountryVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryPostalZip":
				DeliveryPostalZipVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryAddressNotes":
				DeliveryAddressNotesVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDeliveryNotes":
				DeliveryNotesVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDZ":
				DeliveryZoneVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnServ":
				ServiceLevelVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPkg":
				PackageTypeVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnReady":
				ReadyTimeVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnDue":
				DueTimeVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnPcs":
				PiecesVisibility = DictionaryColumnVisibility[ Key ];
				break;

			case "ColumnWt":
				WeightVisibility = DictionaryColumnVisibility[ Key ];
				break;
			}
		}

		ColumnSettings Cs = new()
		                    {
			                    Columns = newColumns
		                    };

		RouteShipmentsSettings ??= new Settings();

		RouteShipmentsSettings.ColumnSettings = Cs;

		SaveSettings( RouteShipmentsSettings );
	}
#endregion

#region Settings Persistence
	public class ColumnSettings
	{
		public List<string> Columns { get; set; } = new();
	}


	public class Settings
	{
		public ColumnSettings ColumnSettings { get; set; } = new();
	}

	public new void LoadSettings()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var FileName = MakeFileName();

				if( File.Exists( FileName ) )
				{
					Logging.WriteLogLine( "Loading settings from " + FileName );
					var SerializedObject = File.ReadAllText( FileName );
					SerializedObject       = Encryption.Decrypt( SerializedObject );
					RouteShipmentsSettings = JsonConvert.DeserializeObject<Settings>( SerializedObject );

					if( RouteShipmentsSettings is not null )
					{
						var ActiveCols = RouteShipmentsSettings.ColumnSettings.Columns;

						foreach( var Col in ActiveCols )
							DictionaryColumnVisibility[ Col ] = Visibility.Visible;
					}
				}
				else
				{
					var ActiveCols = GetAllColumnsInOrder();

					foreach( var Col in ActiveCols )
						DictionaryColumnVisibility[ Col ] = Visibility.Visible;
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to load settings: " + E );

				// Doing this to ensure that the board will load if the file is corrupted
				Logging.WriteLogLine( "Deleting bad settings file for DispatchBoard" );
				RouteShipmentsSettings = new Settings();
				SaveSettings( RouteShipmentsSettings );
			}
		}
	}

	private void SaveSettings( Settings settings )
	{
		if( !IsInDesignMode )
		{
			try
			{
				var SerializedObject = JsonConvert.SerializeObject( settings );

				//Logging.WriteLogLine("Saved settings:\n" + SerializedObject);
				SerializedObject = Encryption.Encrypt( SerializedObject );
				var FileName = MakeFileName();
				Logging.WriteLogLine( "Saving settings to " + FileName );
				File.WriteAllText( FileName, SerializedObject );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR: unable to save settings: " + E );
			}
		}
	}
#endregion
}