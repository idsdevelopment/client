﻿// using IdsControlLibrary.Utils;

using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using Microsoft.Web.WebView2.Core;
using ViewModels.Trips.Boards.Common;
using ViewModels.Trips.TripEntry;

namespace ViewModels.RouteShipments;

/// <summary>
///     Interaction logic for RouteShipments.xaml
/// </summary>
public partial class RouteShipments : Page
{
	private RouteShipmentsModel Model;
	private DisplayTrip         CurrentTrip;
	private bool                WasKeyPress;

	protected override async void OnInitialized( EventArgs e )
	{
		base.OnInitialized( e );
		webView.Visibility = Visibility.Hidden;

		if( DataContext is RouteShipmentsModel M )
			Model = M;

		//Logging.WriteLogLine( "DEBUG Getting map keys" );
		await Model.GetBingMapApiKey();

		Model.SetView( this );

		await LoadMap();

		// Run this when new icons are used, to get their base64 strings
		//LoadIcons();

		webView.Visibility = Visibility.Visible;
		webView.InvalidateVisual();
	}

	public async Task LoadMap()
	{
		await webView.EnsureCoreWebView2Async();

		if ( DataContext is RouteShipmentsModel Model )
		{
			var html = Model.GetMapHtml();
			webView.NavigateToString( html );

			webView.CoreWebView2.Settings.IsWebMessageEnabled = true;
		}
	}

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public void SetWaitCursor()
	{
		Mouse.OverrideCursor = Cursors.Wait;
	}

	public void ClearWaitCursor()
	{
		Mouse.OverrideCursor = null;
	}

	public RouteShipments()
	{
		Initialized += ( _, _ ) =>
		               {
			               void DoSelectionChanged()
			               {
				               WasKeyPress = false;

				               if( DriverCombo.Visibility == Visibility.Visible )
				               {
					               if( DriverCombo.SelectedItem is Driver D )
					               {
						               CurrentTrip.Driver = D.StaffId.Trim();
						               DataGrid.CancelEdit();
						               Model.SelectedTrip = CurrentTrip;
						               Model.WhenSelectedTripChanges();
					               }
				               }
			               }

			               DriverCombo.PreviewKeyDown += ( _, eventArgs ) =>
			                                             {
				                                             var Key = eventArgs.Key;

				                                             switch( Key )
				                                             {
				                                             case Key.Return:
				                                             case Key.Tab:
					                                             {
						                                             WasKeyPress = false;
						                                             var Txt = DriverCombo.Text;

						                                             if( DriverCombo.ItemsSource is ObservableCollection<Driver> Drivers )
						                                             {
							                                             foreach( var Driver in Drivers )
							                                             {
								                                             if( Driver.StaffId == Txt )
								                                             {
									                                             DriverCombo.SelectedItem = Driver;
									                                             DoSelectionChanged();

									                                             break;
								                                             }
							                                             }
						                                             }
					                                             }

					                                             break;

				                                             default:
					                                             WasKeyPress = true;

					                                             break;
				                                             }
			                                             };

			               DriverCombo.SelectionChanged += ( _, _ ) =>
			                                               {
				                                               if( !WasKeyPress )
					                                               DoSelectionChanged();
				                                               else
					                                               WasKeyPress = false;
			                                               };

			               DriverCombo.LostFocus += ( _, _ ) =>
			                                        {
				                                        DoSelectionChanged();
			                                        };

			               DriverCombo.DropDownClosed += ( _, _ ) =>
			                                             {
				                                             DoSelectionChanged();
			                                             };

			               //DriversPopup.OnSelectionChanged += ( o, s ) =>
			               //                                   {
			               //                                    try
			               //                                    {
			               //                                     var Driver = s.Trim();

			               //                                     foreach( var Trip in Model.SelectedTrips )
			               //                                     {
			               //                                      Trip.Driver = Driver;
			               //                                      Trip.EnableAccept = true;
			               //                                     }

			               //                                     DriversPopup.IsOpen = false;
			               //                                     DataGrid.CancelEdit();
			               //                                     Model.SelectedTrip = CurrentTrip;
			               //                                    }
			               //                                    catch( Exception Exception )
			               //                                    {
			               //                                     Console.WriteLine( Exception );
			               //                                    }
			               //                                   };
		               };
		InitializeComponent();

		var columns = Model.GetAllColumnsInOrder();

		foreach( var col in DataGrid.Columns )
		{
			foreach( var name in columns )
			{
				var header = FindStringResource( name );

				if( col.Header.ToString() == header )
				{
					col.Visibility = Model.DictionaryColumnVisibility[ name ];
					break;
				}
			}
		}
		InitializeComponent();
		PopulateSettingsColumnList();
	}

#region DataGrid
	private void MiAllocateDriver_Click( object sender, RoutedEventArgs e )
	{
		var SelectedTrips = Model.SelectedTrips;

		if( SelectedTrips is not null )
		{
			var Count = SelectedTrips.Count;

			if( Count > 0 )
			{
				var Ndx  = 0;
				var Trip = SelectedTrips[ 0 ];

				foreach( var ModelTrip in Model.Trips )
				{
					if( Trip == ModelTrip )
					{
						var Cell = DataGrid.GetCell( Ndx, 0 );

						if( Cell != null )
						{
							Cell.Focus();
							DataGrid.BeginEdit();
							break;
						}
					}
					++Ndx;
				}
			}
		}
	}

	private void ToggleButton_Checked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var DGrid = Tb.FindParent<DataGrid>();

			if( DGrid is not null )
			{
				var Ndx = DGrid.SelectedIndex;

				if( Ndx >= 0 )
				{
					var Itm = (DisplayTrip)DGrid.SelectedItem;
					Itm.DetailsVisible = true;

					var Row = DGrid.GetRow( Ndx );

					if( Row is not null )
						DGrid.ScrollIntoView( Row );
				}
			}
		}
	}

	private void ToggleButton_Unchecked( object sender, RoutedEventArgs e )
	{
		// Binding sometimes doesn't work
		if( sender is ToggleButton Tb )
		{
			var Grid = Tb.FindParent<DataGrid>();

			if( Grid?.SelectedItem is DisplayTrip Itm )
				Itm.DetailsVisible = false;
		}
	}

	private void DataGrid_BeginningEdit( object sender, DataGridBeginningEditEventArgs e )
	{
		// Make sure that there are no previous drivers
		var selectedTrips = Model.SelectedTrips;

		if( ( selectedTrips != null ) && ( selectedTrips.Count > 0 ) )
		{
			foreach( var trip in selectedTrips )
				trip.Driver = string.Empty;
		}
	}

	private void DataGrid_CellEditEnding( object sender, DataGridCellEditEndingEventArgs e )
	{
		// Apply the driver to all selected trips
		var selectedTrips = Model.SelectedTrips;

		if( ( selectedTrips != null ) && ( selectedTrips.Count > 0 ) )
		{
			var driver = string.Empty;

			foreach( var trip in selectedTrips )
			{
				Logging.WriteLogLine( "TripId: " + trip.TripId + " - driver: " + trip.Driver );

				if( trip.Driver.IsNotNullOrWhiteSpace() )
				{
					driver = trip.Driver;
					Logging.WriteLogLine( "Setting trip's driver to " + driver );
					break;
				}
			}

			foreach( var trip in selectedTrips )
			{
				trip.Driver       = driver;
				trip.EnableAccept = true;
			}
		}
	}


	private void DataGrid_PreparingCellForEdit( object sender, DataGridPreparingCellForEditEventArgs e )
	{
		if( e.Column.DisplayIndex == 0 )
		{
			var StackPanel = e.EditingElement.FindChild<StackPanel>();

			if( StackPanel is not null )
			{
				DriverCombo.RemoveParent();
				StackPanel.Children.Add( DriverCombo );
				DriverCombo.DataContext = Model;
				DriverCombo.Visibility  = Visibility.Visible;
				DriverCombo.UpdateLayout();
				var TexBox = DriverCombo.FindChild<TextBox>( "PART_EditableTextBox" );

				if( CurrentTrip is not null )
					TexBox.Text = CurrentTrip.Driver;
				TexBox?.Focus();
			}
		}
	}

	private void DataGrid_SelectedCellsChanged( object sender, SelectedCellsChangedEventArgs e )
	{
		if( ( e.AddedCells.Count > 0 ) && e.AddedCells[ 0 ].Item is DisplayTrip Trip )
			CurrentTrip = Trip;
	}

	private void DataGrid_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( sender is DataGrid Grid )
		{
			var Trips = Model.SelectedTrips;

			if( Trips is null )
				Trips = new List<DisplayTrip>();
			else
				Trips.Clear();

			Model.SelectedTrips = null;
			Trips.AddRange( Grid.SelectedItems.Cast<DisplayTrip>() );
			Model.SelectedTrips = Trips;
		}
	}

	private async void DataGrid_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		var selectedTrip = (DisplayTrip)DataGrid.SelectedItem;

		if( selectedTrip != null )
		{
			Logging.WriteLogLine( "Selected trip: " + selectedTrip.TripId );

			if( selectedTrip.Driver.IsNullOrWhiteSpace() )
			{
				selectedTrip.Driver = Model.DictTripIdsAndDrivers[ selectedTrip.TripId ];
				//DisplayTrip dt = (from T in Model.Trips where T.TripId == selectedTrip.TripId select T).FirstOrDefault();
				//if (dt != null)
				//               {
				//	selectedTrip.Driver = dt.Driver;
				//               }
			}

			var tis = Globals.DataContext.MainDataContext.ProgramTabItems;

			if( ( tis != null ) && ( tis.Count > 0 ) )
			{
				PageTabItem pti = null;

				for( var i = 0; i < tis.Count; i++ )
				{
					var ti = tis[ i ];

					if( ti.Content is TripEntry )
					{
						pti = ti;

						// Load the trip into the first TripEntry tab
						Logging.WriteLogLine( "Displaying trip in extant " + Globals.TRIP_ENTRY + " tab" );

						( (TripEntryModel)pti.Content.DataContext ).CurrentTrip = selectedTrip;
						await ( (TripEntryModel)pti.Content.DataContext ).WhenCurrentTripChanges();
						Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

						break;
					}
				}

				if( pti == null )
				{
					// Need to create a new Trip Entry tab
					Logging.WriteLogLine( "Opening new " + Globals.TRIP_ENTRY + " tab" );
					Globals.RunProgram( Globals.TRIP_ENTRY, selectedTrip );
				}
			}
		}
	}
#endregion

#region Settings
	private readonly List<CheckBox> cbColumns = new();

	private void PopulateSettingsColumnList()
	{
		cbColumns.Clear();
		gridColumns.RowDefinitions.Clear();
		gridColumns.Children.Clear();

		if( DataContext is RouteShipmentsModel Model )
		{
			var columns = Model.GetAllColumnsInOrder();

			if( ( columns != null ) && ( columns.Count > 0 ) )
			{
				gridColumns.RowDefinitions.Add( new RowDefinition() );

				var labelVisible = new Label
				                   {
					                   FontWeight = FontWeights.Bold,
					                   Content    = FindStringResource( "LabelVisible" )
				                   };

				gridColumns.Children.Add( labelVisible );
				Grid.SetColumn( labelVisible, 0 );
				Grid.SetRow( labelVisible, 0 );

				var row = 1;

				foreach( var column in columns )
				{
					gridColumns.RowDefinitions.Add( new RowDefinition() );

					var name = FindStringResource( column );

					// This column must be visible
					if( column != "ColumnDriver" )
					{
						var checkBox = new CheckBox
						               {
							               Name    = column,
							               Content = name
						               };
						gridColumns.Children.Add( checkBox );
						Grid.SetColumn( checkBox, 0 );
						Grid.SetRow( checkBox, row );

						if( Model.DictionaryColumnVisibility.ContainsKey( column ) && ( Model.DictionaryColumnVisibility[ column ] == Visibility.Visible ) )
							checkBox.IsChecked = true;

						//if (currentCols.Contains(column))
						//{
						//	checkBox.IsChecked = true;
						//}

						cbColumns.Add( checkBox );

						if( column == "ColumnTripId" )
							checkBox.Visibility = Visibility.Hidden;
						++row;
					}
				}
			}
		}
	}


	private void BtnCloseSettings_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Closing settings tab" );

		tabItemRouteShipments.Visibility = Visibility.Collapsed;
		tabItemSettings.Visibility       = Visibility.Collapsed;
		tabItemRouteShipments.IsSelected = true;
		tabItemRouteShipments.Focus();
	}

	private void BtnSelectAll_Click( object sender, RoutedEventArgs e )
	{
		foreach( var cb in cbColumns )
			cb.IsChecked = true;
	}

	private void BtnSelectNone_Click( object sender, RoutedEventArgs e )
	{
		foreach( var cb in cbColumns )
			cb.IsChecked = false;
	}

	private void BtnSaveColumns_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is RouteShipmentsModel Model )
		{
			Logging.WriteLogLine( "BtnSaveColumns clicked" );
			var columns    = Model.GetAllColumnsInOrder();
			var newColumns = new List<string>();

			var i = 1; // Drivers column is always visible

			foreach( var cb in cbColumns )
			{
				var tmp = cb.IsChecked ?? false;

				if( tmp )
					newColumns.Add( columns[ i ] );
				++i;
			}

			Model.SaveColumns( newColumns );

			foreach( var col in DataGrid.Columns )
			{
				Logging.WriteLogLine( "DEBUG col.Header: " + col.Header );

				foreach( var name in columns )
				{
					if( ( name != "ColumnDriver" ) && ( name != "ColumnTripId" ) )
					{
						var header = FindStringResource( name );
						Logging.WriteLogLine( "DEBUG header: " + header );

						if( col.Header.ToString() == header )
						{
							col.Visibility = Model.DictionaryColumnVisibility[ name ];
							break;
						}
					}
				}
			}

			PopulateSettingsColumnList();

			tabControl.SelectedIndex = 0;
		}
	}
#endregion

#region Buttons, etc
	private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}

	private void MenuItem_ShowToolbar( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Collapsed;
		toolbar.Visibility  = Visibility.Visible;
	}

	private void MenuItem_Click_1( object sender, RoutedEventArgs e )
	{
		menuMain.Visibility = Visibility.Visible;
		toolbar.Visibility  = Visibility.Collapsed;
	}


	private void BtnHelp_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is RouteShipmentsModel Model )
		{
			Logging.WriteLogLine( "Opening " + Model.HelpUri );
			var uri = new Uri( Model.HelpUri );
			Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
		}
	}

	private void MenuItem_ViewSettings( object sender, RoutedEventArgs e )
	{
		SettingsButton_Click( null, null );
	}

	private void SettingsButton_Click( object sender, RoutedEventArgs e )
	{
		Logging.WriteLogLine( "Settings tab visible" );
		tabItemRouteShipments.Visibility = Visibility.Visible;
		tabItemSettings.Visibility       = Visibility.Visible;
		tabItemSettings.Focus();
	}

	private void WebView_NavigationCompleted( object sender, CoreWebView2NavigationCompletedEventArgs e )
	{
	}

	private void WebView_NavigationStarting( object sender, CoreWebView2NavigationStartingEventArgs e )
	{
	}

	private void WebView_WebMessageReceived( object sender, CoreWebView2WebMessageReceivedEventArgs e )
	{
	}

	private async void BtnRoute_Click( object sender, RoutedEventArgs e )
	{
		if( DataContext is RouteShipmentsModel ShipmentsModel )
		{
			var (Ok, Messages) = await ShipmentsModel.RouteShipments();

			if( !Ok )
			{
				StringBuilder Sb = new();

				foreach( var Line in Messages )
					Sb.Append( "\r\n\t" ).Append( Line );

				Logging.WriteLogLine( "Errors found: " + Sb );

				var Caption = FindStringResource( "RouteShipmentsErrorsTitle" );
				var Message = Caption + ":" + Sb;

				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Error );
			}
			else
			{
				Logging.WriteLogLine( Messages[ 0 ] );
				var Caption = FindStringResource( "RouteShipmentsSuccessTitle" );
				MessageBox.Show( Messages[ 0 ], Caption, MessageBoxButton.OK, MessageBoxImage.Information );
			}
		}
	}

	private void MiRoute_Click( object sender, RoutedEventArgs e )
	{
		BtnRoute_Click( null, null );
	}

	private void CbByDeliveryAddress_Checked( object sender, RoutedEventArgs e )
	{
	}

	private void CbByDeliveryAddress_Unchecked( object sender, RoutedEventArgs e )
	{
	}
#endregion
}