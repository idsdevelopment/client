﻿#nullable enable

using System.Diagnostics;

namespace ViewModels;

public static partial class Globals
{
	public static class Uri
	{
		public static void Execute( string uri )
		{
			var Uri = new System.Uri( uri.Trim() );
			Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
		}
	}
}