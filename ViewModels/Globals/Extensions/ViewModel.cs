﻿using System.Windows.Controls;

namespace ViewModels;

public static partial class Extensions
{
	public static void CloseModel( this ViewModelBase model )
	{
		Globals.DataContext.MainDataContext?.CloseProgram( model );
	}

	public static void ClosePage( this Page page )
	{
		Globals.DataContext.MainDataContext?.CloseProgram( page );
	}
}