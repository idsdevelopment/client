﻿using System.Windows.Controls;

namespace ViewModels;

public static partial class Extensions
{
	public static void Close( this Page page )
	{
		Globals.DataContext.MainDataContext?.CloseProgram( page );
	}
}