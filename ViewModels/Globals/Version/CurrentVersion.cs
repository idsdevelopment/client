﻿// ReSharper disable InconsistentNaming

namespace ViewModels;

public static partial class Globals
{
	public static class CurrentVersion
	{
		public static string VERSION => $"1.0.{CURRENT_VERSION}";

		public static string APP_VERSION => "Client Vsn: " + VERSION;
		public static string IDS_VERSION => "IDS Shipments (" + VERSION + ")";

		public static uint CURRENT_VERSION { get; set; }
	}
}