﻿#nullable enable

using ViewModels._Customers.Priority.Routes;
using ViewModels._Diagnostics.StressTest;
using ViewModels._Ids;
using ViewModels.Customers.Accounts;
using ViewModels.Customers.AddressBook;
using ViewModels.Reports.WebReports;
using ViewModels.Roles;
using ViewModels.Staff.ShiftRates;
using ViewModels.Staff.ShiftRates.ShiftRates;
using ViewModels.Trips.Boards;
using ViewModels.Trips.PricingPosting;
using ViewModels.Trips.TripEntry;
using ViewModels.Uplift.Products;
using ViewModels.Uplift.Shipments;
using BackupRestore = ViewModels._Ids.Backup.BackupRestore;
using Inventory = ViewModels._Customers.Pml.Inventory;
using SearchTrips = ViewModels.Trips.SearchTrips.SearchTrips;
using Time = ViewModels._Diagnostics.ServerTime.Time;
using Trip = ViewModels.AuditTrails.Trip;

namespace ViewModels;

public static partial class Globals
{
	// These names are used in the client log file
	public const string LOGIN                 = "Login",
	                    CARRIER_CREATE_WIZARD = "IDS/Wizard",
	                    MAINTAIN_SETTINGS     = "Maintain Settings",
	                    MAINTAIN_STAFF        = "Maintain Staff",
	                    SHIFT_RATES           = "Shift Rates",
	                    SEARCH_TRIPS          = "Search Shipments",
	                    DISPATCH_BOARD        = "Dispatch Board",
	                    DRIVERS_BOARD         = "Drivers Board",
	                    STAFF_AUDIT_TRAIL     = "Staff Audit Trail",
	                    TRIP_AUDIT_TRAIL      = "Trip Audit Trail",
	                    PRIORITY_ROUTES       = "Priority Maintain Routes",
	                    TRIP_ENTRY            = "Shipment Entry",
	                    MAINTAIN_ROLES        = "Maintain Roles",
	                    RATE_WIZARD           = "Rate Wizard",
	                    ZONE_WIZARD           = "Zone Wizard",
	                    CHARGES_WIZARD        = "Charges Wizard",
	                    CUSTOMER_IMPORT       = "Customer Import",
	                    MAP_BROWSER           = "Map Browser",
	                    ADDRESS_BOOK          = "Address Book",
	                    ACCOUNTS              = "Accounts",
	                    PML_INVENTORY         = "Pml Inventory",
	                    UPLIFT_PRODUCTS       = "Uplift Products",
	                    UPLIFT_SHIPMENTS      = "Uplift Shipments",
	                    REPORT_BROWSER        = "Reports",
	                    ROUTE_SHIPMENTS       = "RouteShipments",
	                    WEB_REPORTS           = "Web Reports",
                        INVOICING			  = "Invoicing",
                        PRICING_POSTING		  = "Pricing/Posting",


                        // Ids
                        BACKUP_RESTORE = "Backup/Restore",

	                    // Diagnostics
	                    SERVER_TIME = "Server Time",
	                    STRESS_TEST = "Stress Test";

	public class Program
	{
		public bool AllowClose = true;

		public bool CanView,
		            CanEdit,
		            CanDelete,
		            CanCreate;

		public Type?                                      PageType;
		public (string? Name, Func<bool>? CanExecute )[]? RelatedPrograms = Array.Empty<(string? Name, Func<bool>? CanExecute)>();
	}

	public static Dictionary<string, Program> Programs = new()
	                                                     {
		                                                     {
			                                                     LOGIN,
			                                                     new Program
			                                                     {
				                                                     PageType   = typeof( StartPage.StartPage ),
				                                                     AllowClose = false
			                                                     }
		                                                     },
		                                                     {
			                                                     CARRIER_CREATE_WIZARD,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( IdsCreateWizard )
			                                                     }
		                                                     },

		                                                     {
			                                                     MAINTAIN_SETTINGS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Settings.Settings )
			                                                     }
		                                                     },

	                                                     #region Staff
		                                                     {
			                                                     MAINTAIN_STAFF,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( MaintainStaff )
			                                                     }
		                                                     },
		                                                     {
			                                                     SHIFT_RATES,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( ShiftRates )
			                                                     }
		                                                     },
	                                                     #endregion

		                                                     {
			                                                     SEARCH_TRIPS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( SearchTrips ),
				                                                     RelatedPrograms = new (string? Name, Func<bool>? CanExecute )[]
				                                                                       {
					                                                                       ( TRIP_ENTRY, AutoOpenShipmentTab )
				                                                                       }
			                                                     }
		                                                     },
		                                                     {
			                                                     DISPATCH_BOARD,

			                                                     new Program
			                                                     {
				                                                     PageType = typeof( DispatchBoard ),
				                                                     RelatedPrograms = new (string? Name, Func<bool>? CanExecute )[]
				                                                                       {
					                                                                       ( TRIP_ENTRY, AutoOpenShipmentTab )
				                                                                       }
			                                                     }
		                                                     },
		                                                     {
			                                                     DRIVERS_BOARD,

			                                                     new Program
			                                                     {
				                                                     PageType = typeof( DriversBoard ),
				                                                     RelatedPrograms = new (string? Name, Func<bool>? CanExecute )[]
				                                                                       {
					                                                                       ( TRIP_ENTRY, AutoOpenShipmentTab )
				                                                                       }
			                                                     }
		                                                     },
		                                                     {
			                                                     STAFF_AUDIT_TRAIL,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( AuditTrails.Staff )
			                                                     }
		                                                     },
		                                                     {
			                                                     TRIP_AUDIT_TRAIL,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Trip )
			                                                     }
		                                                     },
		                                                     {
			                                                     TRIP_ENTRY,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( TripEntry )
			                                                     }
		                                                     },
		                                                     {
			                                                     PRIORITY_ROUTES,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Routes )
			                                                     }
		                                                     },
		                                                     {
			                                                     MAINTAIN_ROLES,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( MaintainRoles )
			                                                     }
		                                                     },
		                                                     {
			                                                     RATE_WIZARD,
			                                                     new Program
			                                                     {
				                                                     // TODO only allow 1 instance
				                                                     PageType = typeof( RateWizard.RateWizard )
			                                                     }
		                                                     },
		                                                     {
			                                                     ZONE_WIZARD,
			                                                     new Program
			                                                     {
				                                                     // TODO only allow 1 instance
				                                                     PageType = typeof( ZoneWizard.ZoneWizard )
			                                                     }
		                                                     },
		                                                     {
			                                                     CHARGES_WIZARD,
			                                                     new Program
			                                                     {
				                                                     // TODO only allow 1 instance
				                                                     PageType = typeof( ChargesWizard.ChargesWizard )
			                                                     }
		                                                     },
		                                                     {
			                                                     MAP_BROWSER,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( MapBrowser.MapBrowser ),
				                                                     RelatedPrograms = new (string? Name, Func<bool>? CanExecute )[]
				                                                                       {
					                                                                       ( TRIP_ENTRY, AutoOpenShipmentTab )
				                                                                       }
			                                                     }
		                                                     },
		                                                     {
			                                                     ADDRESS_BOOK,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( AddressBook )
			                                                     }
		                                                     },
		                                                     {
			                                                     ACCOUNTS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Accounts )
			                                                     }
		                                                     },
		                                                     {
			                                                     PML_INVENTORY,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Inventory )
			                                                     }
		                                                     },
		                                                     {
			                                                     UPLIFT_PRODUCTS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Products )
			                                                     }
		                                                     },
		                                                     {
			                                                     UPLIFT_SHIPMENTS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Shipments )
			                                                     }
		                                                     },
		                                                     {
			                                                     REPORT_BROWSER,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( ReportBrowser.ReportBrowser )
			                                                     }
		                                                     },
															 {
																 INVOICING,
																 new Program
																 {
																	 PageType= typeof( Invoicing.Invoicing )
																 }
															 },
															 {
																 PRICING_POSTING,
																 new Program
																 {
																	PageType = typeof(PricingPosting)
 																 }
															 },
		                                                     {
			                                                     ROUTE_SHIPMENTS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( RouteShipments.RouteShipments )
			                                                     }
		                                                     },
		                                                     {
			                                                     WEB_REPORTS,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( WebReports ),
			                                                     }
		                                                     },
		                                                     // Ids
		                                                     {
			                                                     BACKUP_RESTORE,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( BackupRestore )
			                                                     }
		                                                     },

		                                                     // Diagnostics
		                                                     {
			                                                     SERVER_TIME,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Time )
			                                                     }
		                                                     },
		                                                     {
			                                                     STRESS_TEST,
			                                                     new Program
			                                                     {
				                                                     PageType = typeof( Test )
			                                                     }
		                                                     }
	                                                     };

	public static bool AutoOpenShipmentTab() => Menu.Preferences.AutoOpenMenuTab;

	public static void RunProgram( string programName, object? arg = null )
	{
		DataContext.MainDataContext.RunProgram( programName, arg );
	}

	public static void RunNewProgram( string programName, object? arg = null )
	{
		DataContext.MainDataContext.RunNewProgram( programName, arg );
	}
}