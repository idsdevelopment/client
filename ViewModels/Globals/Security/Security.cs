﻿namespace ViewModels;

public static partial class Globals
{
	public static class Security
	{
		public static bool IsIds,
		                   IsNewCarrier;

		public static string CarrierId
		{
			get => Company.CompanyName;
			set => Company.CompanyName = value;
		}
	}
}