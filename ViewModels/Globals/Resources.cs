﻿namespace ViewModels;

public static partial class Globals
{
	internal static class Dictionary
	{
		internal static string AsString( string key )
		{
			try
			{
				return FindStringResource != null ? FindStringResource( key ) : key;
			}
			catch
			{
				return key;
			}
		}

		internal static Color AsColor( string key )
		{
			try
			{
				return FindColorResource?.Invoke( key ) ?? Colors.Red;
			}
			catch
			{
				return Colors.Red;
			}
		}

		internal static void SetColor( string key, Color value )
		{
			try
			{
				// ReSharper disable once PossibleNullReferenceException
				SetColorResource?.Invoke( key, value );
			}
			catch
			{
			}
		}


		internal static SolidColorBrush AsSolidColorBrush( string key )
		{
			try
			{
				return FindSolidColorBrush?.Invoke( key ) ?? new SolidColorBrush( Colors.Red );
			}
			catch
			{
				return new SolidColorBrush( Colors.Red );
			}
		}

		internal static void SetSolidBrush( string key, SolidColorBrush value )
		{
			try
			{
				SetSolidBrushResource?.Invoke( key, value );
			}
			catch
			{
			}
		}
	}

	public static Func<string, string>            FindStringResource;
	public static Func<string, Color>             FindColorResource;
	public static Func<string, SolidColorBrush>   FindSolidColorBrush;
	public static Action<string, Color>           SetColorResource;
	public static Action<string, SolidColorBrush> SetSolidBrushResource;
}