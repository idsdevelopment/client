﻿using System.Threading;
using ViewModels.Dialogues;

namespace ViewModels;

public static partial class Globals
{
	public partial class Dialogues
	{
		public class Confirm
		{
			public static bool Show( string text )
			{
				var Busy   = true;
				var Result = false;

				Application.Current.Dispatcher.Invoke( () =>
				                                       {
					                                       var Dlg = new ViewModels.Dialogues.Confirm( text )
					                                                 {
						                                                 Owner = Windows.CurrentWindow
					                                                 };

					                                       if( Dlg.DataContext is ConfirmModel Model )
						                                       Model.Text = Dictionary.AsString( text );
					                                       Result = Dlg.ShowDialog();
					                                       Busy   = false;
				                                       } );

				while( Busy )
					Thread.Sleep( 100 );

				return Result;
			}
		}
	}
}