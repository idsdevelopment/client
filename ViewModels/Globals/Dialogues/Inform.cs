﻿using System.Threading;
using ViewModels.Dialogues;

namespace ViewModels;

public static partial class Globals
{
	public partial class Dialogues
	{
		public class Inform
		{
			public static void Show( string text )
			{
				var Busy = true;

				Application.Current.Dispatcher.Invoke( () =>
				                                       {
					                                       var Dlg = new ViewModels.Dialogues.Inform
					                                                 {
						                                                 Owner = Windows.CurrentWindow
					                                                 };

					                                       if( Dlg.DataContext is InformModel Model )
						                                       Model.Text = Dictionary.AsString( text );
					                                       Dlg.ShowDialog();
					                                       Busy = false;
				                                       } );

				while( Busy )
					Thread.Sleep( 100 );
			}
		}
	}
}