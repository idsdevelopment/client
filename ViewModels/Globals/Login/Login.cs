﻿#nullable enable

namespace ViewModels;

public static partial class Globals
{
	public static class Login
	{
		// ReSharper disable once InconsistentNaming
		public static Action? _SignOut;

		public static void SignOut()
		{
			_SignOut?.Invoke();
		}
	}
}