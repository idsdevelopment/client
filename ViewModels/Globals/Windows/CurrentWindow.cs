﻿namespace ViewModels;

public static partial class Globals
{
	public class Windows
	{
		public static Action MainWindowColourChange;

		public static Window CurrentWindow => ( from W in Application.Current.Windows.OfType<Window>()
		                                        where W.IsActive
		                                        select W ).FirstOrDefault() ?? Application.Current.MainWindow;

		public static Window MainWindow => Application.Current.MainWindow;
	}
}