﻿using System.Windows.Data;

namespace ViewModels.Convertors;

public class BoolToGreenRedConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is bool Green && Green ? Brushes.Green : Brushes.OrangeRed;


	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}