﻿using System.Windows.Data;

namespace ViewModels.Convertors;

public class BoolToPoint5OpacityConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is bool One && One ? 1 : 0.5;


	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}