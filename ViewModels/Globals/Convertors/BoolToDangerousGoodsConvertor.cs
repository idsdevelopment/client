﻿using System.Windows.Data;

namespace ViewModels.Convertors;

public class BoolToDangerousGoodsConvertor : IValueConverter
{
	private static SolidColorBrush NormalColor,
	                               DangerousColor;

	private static bool HaveColours;

	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( !HaveColours )
		{
			HaveColours = true;

			if( ViewModelBase.IsInDesignMode )
			{
				NormalColor    = new SolidColorBrush( Colors.SteelBlue );
				DangerousColor = new SolidColorBrush( Colors.Maroon );
			}
			else
			{
				NormalColor    = Globals.Dictionary.AsSolidColorBrush( "IdsBackgroundBrush" );
				DangerousColor = Globals.Dictionary.AsSolidColorBrush( "DangerousGoodsColourBrush" );
			}
		}

		if( value is bool Dangerous )
			return Dangerous ? DangerousColor : NormalColor;
		return new SolidColorBrush( Colors.Red );
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}