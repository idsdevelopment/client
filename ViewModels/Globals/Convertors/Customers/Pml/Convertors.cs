﻿using System.Windows.Data;
using ViewModels._Customers.Pml;

namespace ViewModels.Convertors.Customers.Pml;

public class InventoryStateConvertor : IValueConverter
{
	private static SolidColorBrush NewBrush,
	                               ModifiedBrush,
	                               DeletedBrush;

	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( NewBrush is null )
		{
			NewBrush      = Globals.FindSolidColorBrush( "NewBrush" );
			ModifiedBrush = Globals.FindSolidColorBrush( "ModifiedBrush" );
			DeletedBrush  = Globals.FindSolidColorBrush( "DeletedBrush" );
		}

		if( value is InventoryViewModel.DisplayInventory.STATE State )
		{
			switch( State )
			{
			case InventoryViewModel.DisplayInventory.STATE.DELETED:
				return DeletedBrush;

			case InventoryViewModel.DisplayInventory.STATE.MODIFIED:
				return ModifiedBrush;

			case InventoryViewModel.DisplayInventory.STATE.NEW:
				return NewBrush;
			}
		}

		return Brushes.Transparent;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class DeleteRemoveConvertor : IValueConverter
{
	private static string Remove,
	                      Delete;

	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( Remove is null )
		{
			if( ViewModelBase.IsInDesignMode )
				return "R/D";

			Remove = Globals.FindStringResource( "Remove" ) ?? "Remove";
			Delete = Globals.FindStringResource( "Delete" ) ?? "Delete";
		}

		return value is bool Del && Del ? Delete : Remove;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}