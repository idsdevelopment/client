﻿#nullable enable

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Brushes = System.Drawing.Brushes;
using Color = System.Drawing.Color;
using Pen = System.Drawing.Pen;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = System.Drawing.Point;

namespace ViewModels.Convertors;

public class SignatureListToImageSourceConvertor : IValueConverter
{
	private const int PADDING          = 3;
	private const int VERTICAL_SPACING = 20;

	private const int TEXT_HEIGHT  = 30;
	private const int TEXT_PADDING = 5;

	public object? Convert( object value, Type? targetType, object? parameter, CultureInfo? culture )
	{
		if( value is List<Signature> {Count: > 0} Signatures )
		{
			var MaxHeight = 0.0;
			var MaxWidth  = 0.0;

			foreach( var Signature in Signatures )
			{
				MaxHeight = Math.Max( Signature.Height, MaxHeight );
				MaxWidth  = Math.Max( Signature.Width, MaxWidth );
			}

			var UnitHeight = (int)MaxHeight + TEXT_HEIGHT + TEXT_PADDING + VERTICAL_SPACING;
			var UnitWidth  = (int)MaxWidth;

			var Count  = Signatures.Count;
			var Height = ( UnitHeight * Count ) + ( 2 * PADDING );
			var BMap   = new Bitmap( UnitWidth + ( 2 * PADDING ), Height, PixelFormat.Format32bppArgb );

			using( var G = Graphics.FromImage( BMap ) )
			{
				G.Clear( Color.FromArgb( 0, Color.Red ) ); // Transparent red
				var Font = new Font( "Segoe UI", 10 );
				var Pen  = new Pen( Color.Black, 2 );

				var J = 0;

				// Display most recent signature first
				for( var I = Signatures.Count; --I >= 0; ++J )
				{
					var Signature = Signatures[ I ];

					var Top = ( UnitHeight * J ) + PADDING;

					G.Clip = new Region( new Rectangle( PADDING, Top, UnitWidth, UnitHeight ) );

					var SigText = Signature.Status1 != STATUS1.UNSET ? $"{Signature.Status1.AsString()}, " : "";
					SigText = $"{SigText}{Signature.Status.AsString()}";

					var Date = $"{SigText}  {Signature.Date:g}";
					G.DrawString( Date, Font, Brushes.Black, 0, Top );

					var Points = Signature.Points.Split( ',' );

					var PointList = ( from P in Points
					                  select P.Split( ':' )
					                  into TurtlePoint
					                  where TurtlePoint.Length == 3
					                  let X = int.Parse( TurtlePoint[ 1 ] )
					                  let Y = int.Parse( TurtlePoint[ 2 ] )
					                  select ( MouseDown: TurtlePoint[ 0 ] == "1", X, Y ) ).ToList();

					// Move signatures up
					var MinTop = int.MaxValue;

					foreach( var (_, _, Y) in PointList )
						MinTop = Math.Min( MinTop, Y );

					Top += ( TEXT_HEIGHT + TEXT_PADDING ) - MinTop;

					var XScale = MaxWidth / Signature.Width;
					var YScale = MaxHeight / Signature.Height;
					var Scale  = Math.Min( XScale, YScale );

					var PenPos = new Point( PADDING, Top );

					foreach( var (MouseDown, X, Y) in PointList )
					{
						var P = new Point
						        {
							        X = (int)( X * Scale ) + PADDING,
							        Y = (int)( Y * Scale ) + Top
						        };

						if( MouseDown )
							G.DrawLine( Pen, PenPos, P );

						PenPos = P;
					}
				}
			}

			using var Memory = new MemoryStream();

			BMap.Save( Memory, ImageFormat.Png );
			Memory.Position = 0;
			var BitmapImage = new BitmapImage();
			BitmapImage.BeginInit();
			BitmapImage.StreamSource = Memory;
			BitmapImage.CacheOption  = BitmapCacheOption.OnLoad;
			BitmapImage.EndInit();

			return BitmapImage;
		}

		return null;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}