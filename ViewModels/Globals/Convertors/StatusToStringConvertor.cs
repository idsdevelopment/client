﻿#nullable enable

using System.Windows.Data;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Convertors;

public class StatusToStringConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		return value switch
			   {
				   int IntStatus              => ( (DisplayTrip.STATUS)IntStatus ).AsString(),
				   STATUS Status              => Status.AsString(),
				   DisplayTrip.STATUS DStatus => DStatus.AsString(),
				   _                          => STATUS.UNSET.AsString()
			   };
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class IntStatusToStringConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		STATUS Stat;

		switch( value )
		{
		case int Status:
			Stat = (STATUS)Status;
			break;

		case uint Status:
			Stat = (STATUS)Status;
			break;

		case DisplayTrip.STATUS Status:
			Stat = (STATUS)Status;
			break;

		case STATUS Status:
			Stat = Status;
			break;

		default:
			return "UNKNOWN";
		}

		return Stat.AsString();
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class StatusToSolidBrushConvertor : IValueConverter
{
	public static SolidColorBrush Delivered     => _Delivered ??= Globals.FindSolidColorBrush( "TripDeliveredRowBrush" );
	public static SolidColorBrush PickedUp      => _PickedUp ??= Globals.FindSolidColorBrush( "TripPickedUpRowBrush" );
	public static SolidColorBrush Dispatched    => _Dispatched ??= Globals.FindSolidColorBrush( "TripDispatchedRowBrush" );
	public static SolidColorBrush Undeliverable => _Undeliverable ??= Globals.FindSolidColorBrush( "TripUndeliverableRowBrush" );
	public static SolidColorBrush Default       => _Default ??= new SolidColorBrush( SystemColors.WindowColor );

	private static SolidColorBrush? _Delivered,
									_PickedUp,
									_Dispatched,
									_Default,
									_Undeliverable;


	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		STATUS Status;

		SolidColorBrush BrushConvert()
		{
			return Status switch
				   {
					   STATUS.DISPATCHED => Dispatched,
					   //STATUS.DELIVERED => Delivered,
					   STATUS.DELIVERED => PickedUp,
					   STATUS.PICKED_UP => PickedUp,
					   _                => Default
				   };
		}

		switch( value )
		{
		case STATUS Status1:
			{
				Status = Status1;

				return BrushConvert();
			}

		case DisplayTrip.STATUS DStatus:
			switch( DStatus )
			{
			case DisplayTrip.STATUS.UNDELIVERABLE:
			case DisplayTrip.STATUS.UNPICKUPABLE:
			case DisplayTrip.STATUS.UNDELIVERABLE_NEW:
				return Undeliverable;

			default:
				Status = (STATUS)DStatus;

				return BrushConvert();
			}

		default:
			return new SolidColorBrush( Colors.Red );
		}
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}