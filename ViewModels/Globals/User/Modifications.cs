﻿namespace ViewModels;

public static partial class Globals
{
	public static class Company
	{
		public static string CompanyName
		{
			get => _CompanyName;
			set => _CompanyName = value.Trim();
		}

		public static string CarrierId
		{
			get => _CompanyName;
			set => _CompanyName = value.Trim();
		}

		private static string _CompanyName;
	}

	public partial class Modifications
	{
		private static bool IsCompany( string companyName, string compareName ) => string.Compare( companyName, compareName, StringComparison.OrdinalIgnoreCase ) == 0;

		private static bool IsCompany( string companyName ) => IsCompany( companyName, Company.CompanyName );
	}

	public class Modules
	{
		public static bool HasInventoryModule => Menu.Preferences.HasInventory;
	}
}