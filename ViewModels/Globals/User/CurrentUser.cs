﻿namespace ViewModels;

public static partial class Globals
{
	public class CurrentUser
	{
		public static string UserName
		{
			get => DataContext.MainDataContext.UserName;
			set => DataContext.MainDataContext.UserName = value.Capitalise();
		}
	}
}