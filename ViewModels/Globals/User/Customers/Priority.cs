﻿#nullable enable

namespace ViewModels;

public static partial class Globals
{
	public partial class Modifications
	{
		public static bool IsPriority => Menu.Preferences.IsPriority;
	}
}