﻿namespace ViewModels;

public static partial class Globals
{
	public partial class Modifications
	{
		public static bool IsPml => Menu.Preferences.IsPml;
	}
}