﻿#nullable enable

using Role = ViewModels.Roles.Role;

namespace ViewModels.MainWindow;

public partial class MainWindowModel : ViewModelBase
{
	public async Task EnableMenuOptions( string userName )
	{
		HideMenuOptions();
		var HasInventory = Globals.Modules.HasInventoryModule;

		RoutesVisible = Globals.Modifications.IsPriority;

		var IsPml = Globals.Modifications.IsPml;

		bool                Ids;
		Protocol.Data.Roles Roles;

		if( IsInDesignMode )
		{
			Roles        = new Protocol.Data.Roles();
			Ids          = true;
			HasInventory = true;
		}
		else
		{
			Roles = await Azure.Client.RequestStaffMemberRoles( userName );
			Ids   = Globals.Security.IsIds;
		}

		Dispatcher.Invoke( () =>
		                   {
			                   if( Ids )
			                   {
				                   InventoryVisible = HasInventory;

				                   PreferencesVisible = true;
				                   RolesVisible       = true;

				                   ShipmentsVisible        = true;
				                   ShipmentVisible         = true;
				                   AdvancedShipmentVisible = true;

				                   SearchVisible = true;

				                   ReportsVisible = true;

				                   AuditVisible      = true;
				                   AuditStaffVisible = true;
				                   AuditTripVisible  = true;

			                   #region Staff
				                   StaffVisible         = true;
				                   MaintainStaffVisible = true;
				                   ShiftRatesVisible    = true;
			                   #endregion

				                   RoutesVisible = true;

				                   BoardsVisible        = true;
				                   DispatchBoardVisible = true;
				                   DriversBoardVisible  = true;
				                   RateWizardVisible    = true;

				                   LogsVisible        = true;
				                   ViewLogsVisible    = true;
				                   ExploreLogsVisible = true;

				                   CustomersVisible         = true;
				                   CustomerAccountsVisible  = true;
				                   CustomerAddressesVisible = true;

				                   PmlVisible          = true;
				                   PmlProductsVisible  = true;
				                   PmlShipmentsVisible = true;
			                   }
			                   else
			                   {
				                   var AdminRole = new Role
				                                   {
					                                   Inventory = HasInventory,

					                                   Routes      = true,
					                                   Roles       = true,
					                                   CanCreate   = true,
					                                   Preferences = true,

					                                   Shipment         = true,
					                                   Shipments        = true,
					                                   AdvancesShipment = true,

					                                   Search = true,

					                                   Reports = true,

					                                   Audit      = true,
					                                   AuditStaff = true,
					                                   AuditTrip  = true,

				                                   #region Staff
					                                   Staff         = true,
					                                   MaintainStaff = true,
					                                   ShiftRates    = true,
				                                   #endregion

					                                   Boards        = true,
					                                   DriversBoard  = true,
					                                   DispatchBoard = true,
					                                   RateWizard    = true,

					                                   Logs       = true,
					                                   ViewLog    = true,
					                                   ExploreLog = true,

					                                   Customers         = true,
					                                   CustomerAccounts  = true,
					                                   CustomerAddresses = true,

					                                   CanEdit   = true,
					                                   CanDelete = true,
					                                   CanView   = true,

					                                   Pml          = IsPml,
					                                   PmlProducts  = IsPml,
					                                   PmlShipments = IsPml
				                                   };

				                   PmlVisible = Globals.Modifications.IsPml;

				                   foreach( var Role in Roles )
				                   {
					                   try
					                   {
						                   Role R;

						                   if( Role.IsAdministrator )
							                   R = AdminRole;
						                   else
							                   R = JsonConvert.DeserializeObject<Role>( Role.JsonObject ) ?? new Role();

						                   InventoryVisible |= R.Inventory & HasInventory;

						                   PreferencesVisible |= R.Preferences | Ids;
						                   RolesVisible       |= R.Roles;

						                   ShipmentsVisible        |= R.Shipments;
						                   ShipmentVisible         |= R.Shipment;
						                   AdvancedShipmentVisible |= R.AdvancesShipment;

						                   SearchVisible |= R.Search;

						                   ReportsVisible |= R.Reports;

						                   AuditVisible      |= R.Audit;
						                   AuditStaffVisible |= R.AuditStaff;
						                   AuditTripVisible  |= R.AuditTrip;

					                   #region Staff
						                   StaffVisible         |= R.Staff;
						                   MaintainStaffVisible |= R.MaintainStaff;
						                   ShiftRatesVisible    |= R.ShiftRates;
					                   #endregion

						                   RoutesVisible |= R.Routes;

						                   BoardsVisible        |= R.Boards;
						                   DispatchBoardVisible |= R.DispatchBoard;
						                   DriversBoardVisible  |= R.DriversBoard;
						                   RateWizardVisible    |= R.RateWizard;

						                   LogsVisible        |= R.Logs;
						                   ViewLogsVisible    |= R.ViewLog;
						                   ExploreLogsVisible |= R.ExploreLog;

						                   CustomersVisible         |= R.Customers;
						                   CustomerAccountsVisible  |= R.CustomerAccounts;
						                   CustomerAddressesVisible |= R.CustomerAddresses;

						                   if( IsPml )
						                   {
							                   PmlVisible          = R.Pml;
							                   PmlProductsVisible  = R.PmlProducts;
							                   PmlShipmentsVisible = R.PmlShipments;
						                   }
						                   else
						                   {
							                   PmlVisible          = false;
							                   PmlProductsVisible  = false;
							                   PmlShipmentsVisible = false;
						                   }

						                   if( Role.IsAdministrator )
							                   break;
					                   }
					                   catch
					                   {
					                   }
				                   }

				                   RoutesVisible = false;
			                   }
		                   } );
	}

	private void HideMenuOptions()
	{
		PreferencesVisible = false;
		RolesVisible       = false;

		ShipmentsVisible        = false;
		ShipmentVisible         = false;
		AdvancedShipmentVisible = false;

		SearchVisible = false;

		ReportsVisible = false;

		AuditVisible      = false;
		AuditStaffVisible = false;
		AuditTripVisible  = false;

	#region Staff
		StaffVisible         = false;
		MaintainStaffVisible = false;
		ShiftRatesVisible    = false;
	#endregion

		RoutesVisible = false;

		BoardsVisible        = false;
		DispatchBoardVisible = false;
		DriversBoardVisible  = false;
		RateWizardVisible    = false;

		LogsVisible        = false;
		ViewLogsVisible    = false;
		ExploreLogsVisible = false;

		CustomersVisible         = false;
		CustomerAccountsVisible  = false;
		CustomerAddressesVisible = false;

		PmlVisible = false;
	}
}