﻿#nullable enable

using System.Diagnostics;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsUpdater;
using ViewModels.Customers.Accounts;
using ViewModels.StartPage;
using ViewModels.Trips.ImportShipments;

// ReSharper disable InconsistentNaming

namespace ViewModels.MainWindow;

public partial class MainWindowModel : ViewModelBase
{
	public enum LANGUAGE
	{
		DEFAULT,
		AUSTRALIAN,
		CANADIAN,
		US
	}

	public bool IsIds
	{
		get { return Get( () => IsIds, IsInDesignMode ); }
		set { Set( () => IsIds, value ); }
	}

	public string VersionNumber
	{
		get { return Get( () => VersionNumber, Globals.CurrentVersion.IDS_VERSION ); }
		set { Set( () => VersionNumber, value ); }
	}

	public string HelpUri
	{
		get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/overview" ); }
	}

	public LANGUAGE Language
	{
		get { return Get( () => Language, LANGUAGE.DEFAULT ); }
		set { Set( () => Language, value ); }
	}


	[Setting]
	public bool LanguageDefault
	{
		get { return Get( () => LanguageDefault, true ); }
		set { Set( () => LanguageDefault, value ); }
	}

	[Setting]
	public bool LanguageAustralian
	{
		get { return Get( () => LanguageAustralian, false ); }
		set { Set( () => LanguageAustralian, value ); }
	}


	[Setting]
	public bool LanguageCanadian
	{
		get { return Get( () => LanguageCanadian, false ); }
		set { Set( () => LanguageCanadian, value ); }
	}


	[Setting]
	public bool LanguageUs
	{
		get { return Get( () => LanguageUs, false ); }
		set { Set( () => LanguageUs, value ); }
	}

	public PageTabControl TabControl
	{
		get { return Get( () => TabControl, new PageTabControl() ); }
		set { Set( () => TabControl, value ); }
	}

	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}

	public string Account
	{
		get { return Get( () => Account, "" ); }
		set { Set( () => Account, value ); }
	}

	public string GLOBAL_ADDRESS_BOOK_NAME { get; } = "CORE_GLOBAL_ADDRESS_BOOK";

	public string NONE_DISPATCHING_ROUTE   { get; } = "NONE_DISPATCHING_ROUTE";

	public string ACCOUNT_CHARGE_PREFIX { get; } = "account_pref_charge_";
	
	public string SURCHARGE_PREFIX { get; } = "surcharge_";

	public bool UpdateAvailable
	{
		get { return Get( () => UpdateAvailable, IsInDesignMode ); }
		set { Set( () => UpdateAvailable, value ); }
	}


	public ObservableCollection<PageTabItem> ProgramTabItems
	{
		get { return Get( () => ProgramTabItems, new ObservableCollection<PageTabItem>() ); }
		set { Set( () => ProgramTabItems, value ); }
	}

	public ICommand Install => Commands[ nameof( Install ) ];

	private readonly ObservableCollection<PageTabItem> ProgramTabs = new();

	private bool InLanguageChange;

	private bool InLanguageChangeEvent;

	public OnChangeLanguageEvent? OnLanguageChange;

	public OnLoginEvent? OnLogin;

	public OnSignOutEvent? OnSignOut,
						   OnExit;

	public Updater? Updater;

	[DependsUpon( nameof( Language ) )]
	public void WhenLanguageTypeChange()
	{
		switch( Language )
		{
		case LANGUAGE.DEFAULT:
			LanguageDefault = true;

			break;

		case LANGUAGE.AUSTRALIAN:
			LanguageAustralian = true;

			break;

		case LANGUAGE.CANADIAN:
			LanguageCanadian = true;

			break;

		case LANGUAGE.US:
			LanguageUs = true;

			break;
		}
	}

	[DependsUpon( nameof( LanguageDefault ) )]
	public void WhenLanguageDefaultChanges()
	{
		if( !InLanguageChange )
		{
			InLanguageChange = true;

			try
			{
				LanguageAustralian = false;
				LanguageCanadian   = false;
				LanguageUs         = false;
			}
			finally
			{
				InLanguageChange = false;
			}
		}
	}

	[DependsUpon( nameof( LanguageAustralian ) )]
	public void WhenLanguageAustralianChanges()
	{
		if( !InLanguageChange )
		{
			InLanguageChange = true;

			try
			{
				LanguageDefault  = false;
				LanguageCanadian = false;
				LanguageUs       = false;
			}
			finally
			{
				InLanguageChange = false;
			}
		}
	}

	[DependsUpon( nameof( LanguageCanadian ) )]
	public void WhenLanguageCanadianChanges()
	{
		if( !InLanguageChange )
		{
			InLanguageChange = true;

			try
			{
				LanguageDefault    = false;
				LanguageAustralian = false;
				LanguageUs         = false;
			}
			finally
			{
				InLanguageChange = false;
			}
		}
	}


	[DependsUpon( nameof( LanguageUs ) )]
	public void WhenLanguageUsChanges()
	{
		if( !InLanguageChange )
		{
			InLanguageChange = true;

			try
			{
				LanguageDefault    = false;
				LanguageAustralian = false;
				LanguageCanadian   = false;
			}
			finally
			{
				InLanguageChange = false;
			}
		}
	}

	[DependsUpon( nameof( LanguageDefault ) )]
	[DependsUpon( nameof( LanguageAustralian ) )]
	[DependsUpon( nameof( LanguageCanadian ) )]
	[DependsUpon( nameof( LanguageUs ) )]
	public void WhenLanguageChanges()
	{
		if( !InLanguageChange && !InLanguageChangeEvent && ( OnLanguageChange != null ) )
		{
			InLanguageChangeEvent = true;

			try
			{
				LANGUAGE L;

				if( LanguageAustralian )
					L = LANGUAGE.AUSTRALIAN;
				else if( LanguageCanadian )
					L = LANGUAGE.CANADIAN;
				else if( LanguageUs )
					L = LANGUAGE.US;
				else
					L = LANGUAGE.DEFAULT;

				Language = L;

				OnLanguageChange( L );
			}
			finally
			{
				InLanguageChangeEvent = false;
			}
		}
	}

	public void RunProgram( string pgmName, object? arg )
	{
		var (Tab, Running) = IsProgramRunning( pgmName );

		var Page = !Running ? RunProgram( pgmName, arg is not null ).Page : Tab?.Content;

		if( Page is not null )
		{
			Page.Tag = arg;

			if( Page.DataContext.IsNotNull() && Page.DataContext is ViewModelBase Model )
			{
				if( Tab is not null )
					Tab.IsSelected = true;

				Model.OnArgumentChange( arg );
			}
		}
	}

	public void RunNewProgram( string pgmName, object? arg )
	{
		//(PageTabItem Tab, bool Running) = IsProgramRunning(pgmName);

		//var Page = !Running ? RunProgram(pgmName, !(arg is null)).Page : Tab?.Content;
		var Page = RunProgram( pgmName, arg is not null ).Page;

		if( Page is not null )
		{
			Page.Tag = arg;

			if( Page.DataContext.IsNotNull() && Page.DataContext is ViewModelBase Model )
			{
				for( var i = ProgramTabs.Count - 1; i >= 0; i-- )
				{
					if( ProgramTabs[ i ].Tag is string Prog && ( Prog == pgmName ) )
					{
						ProgramTabs[ i ].IsSelected = true;
						break;
					}
				}
				//if (Tab is not null)
				//    Tab.IsSelected = true;

				Model.OnArgumentChange( arg );
			}
		}
	}

	public void CloseAllPrograms()
	{
		Dispatcher.Invoke( () =>
						   {
							   for( var I = ProgramTabs.Count; --I >= 0; )
							   {
								   var Item = ProgramTabs[ I ];
								   ProgramTabs.Remove( Item );
								   Item.Dispose();
							   }
						   } );
	}

	public void CloseProgram( Page page )
	{
		Dispatcher.Invoke( () =>
						   {
							   foreach( var ProgramTab in ProgramTabs )
							   {
								   if( Equals( ProgramTab.Content, page ) )
								   {
									   ProgramTabs.Remove( ProgramTab );
									   ProgramTab.Dispose();

									   // 20210119 From Gord - start with a Shipment Entry page
									   if( ProgramTab.Content.Title == "StartPage" )
										   Globals.RunProgram( Globals.TRIP_ENTRY );
									   break;
								   }
							   }
						   } );
	}

	public void CloseProgram( ViewModelBase model )
	{
		Dispatcher.Invoke( () =>
						   {
							   foreach( var ProgramTab in ProgramTabs )
							   {
								   if( ProgramTab.Content.DataContext == model )
								   {
									   ProgramTabs.Remove( ProgramTab );
									   ProgramTab.Dispose();
									   break;
								   }
							   }
						   } );
	}

	[DependsUpon( "TabControl" )]
	public void WhenTabControlChanges()
	{
		TabControl.ItemsSource = ProgramTabs;
		RunProgram( Globals.LOGIN );
	}

	public void Execute_FileExit()
	{
		OnExit?.Invoke();
	}

	public void Execute_SignOut()
	{
		OnSignOut?.Invoke();
	}

	public void Execute_SearchTrips()
	{
		RunProgram( Globals.SEARCH_TRIPS );
	}

	//public void Execute_DispatchBoard()
	public (PageTabItem? Tab, Page?) Execute_DispatchBoard()
	{
		var (Tab, Page) = RunProgram( Globals.DISPATCH_BOARD );
		return ( Tab, Page );
	}

	public void Execute_DriversBoard()
	{
		RunProgram( Globals.DRIVERS_BOARD );
	}

	public void Execute_MaintainSettings()
	{
		RunProgram( Globals.MAINTAIN_SETTINGS );
	}

	public void Execute_MaintainRoles()
	{
		RunProgram( Globals.MAINTAIN_ROLES );
	}

	public void Execute_StaffAudit()
	{
		RunProgram( Globals.STAFF_AUDIT_TRAIL );
	}

	public void Execute_TripAudit()
	{
		RunProgram( Globals.TRIP_AUDIT_TRAIL );
	}

	public void Execute_TripEntry()
	{
		RunProgram( Globals.TRIP_ENTRY );
	}

	public void Execute_MaintainRoutes()
	{
		RunProgram( Globals.PRIORITY_ROUTES );
	}

	public void Execute_RateWizard()
	{
		RunProgram( Globals.RATE_WIZARD );
	}

	public void Execute_ZoneWizard()
	{
		RunProgram( Globals.ZONE_WIZARD );
	}

	public void Execute_ChargesWizard()
	{
		RunProgram( Globals.CHARGES_WIZARD );
	}

	public void Execute_CustomerImport()
	{
		// var changed = new EditSelectedTrips(Application.Current.MainWindow, selected).ShowEditSelectedTrips(selected);
		new CustomerImport( Application.Current.MainWindow );
	}

	public void Execute_MapBrowser()
	{
		RunProgram( Globals.MAP_BROWSER );
	}

	public void Execute_Accounts()
	{
		RunProgram( Globals.ACCOUNTS );
	}

	public void Execute_AddressBook()
	{
		RunProgram( Globals.ADDRESS_BOOK );
	}

	public void Execute_RouteShipments()
	{
		RunProgram( Globals.ROUTE_SHIPMENTS );
	}

	public void Execute_ShowHelp()
	{
		Logging.WriteLogLine( "Opening " + HelpUri );
		var Uri = new Uri( HelpUri );
		Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
	}

	public void Execute_ViewCurrentLog()
	{
		var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
		var FileName     = AppDataLocal + "\\Ids\\" + DateTime.Now.ToString( "yyyy-MM-dd" ) + "-Ids2ClientLog.Txt";
		Logging.WriteLogLine( "Opening current log: " + FileName );
		Process.Start( FileName );
	}

	public void Execute_ExploreLogDirectory()
	{
		var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
		var Directory    = AppDataLocal + "\\Ids\\";

		try
		{
			Process.Start( "explorer", Directory );
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "ERROR : Opening " + Directory + " : " + E );
		}
	}

	public bool Execute_DeleteLocalSettings()
	{
		var Success = true;

		var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var Directory    = AppDataLocal + "\\Ids2Client\\";

		var Di = new DirectoryInfo( Directory );

		//var files = di.GetFiles("*.Settings").Where(p => p.Extension == "*Settings").ToArray();
		var Files = Di.GetFiles( "*.Settings" );

		if( Files.Length > 0 )
		{
			foreach( var File in Files )
			{
				Logging.WriteLogLine( "Deleting " + File.FullName );

				try
				{
					System.IO.File.Delete( File.FullName );
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown when deleting " + File.FullName + " : " + E );
					Success = false;
				}
			}
		}

		return Success;
	}

	public bool Execute_DeleteLocalCachedData()
	{
		var Success = true;

		var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var Directory    = AppDataLocal + "\\Ids2Client\\";

		var Di = new DirectoryInfo( Directory );

		//var files = di.GetFiles("*.Settings").Where(p => p.Extension == "*Settings").ToArray();
		var account = Globals.DataContext.MainDataContext.Account;

		if( account.Contains( "/" ) )
			account = account.Substring( account.IndexOf( "/", StringComparison.Ordinal ) + 1 );
		var Files = Di.GetFiles( account + "*.json" );

		if( Files.Length > 0 )
		{
			foreach( var File in Files )
			{
				Logging.WriteLogLine( "Deleting " + File.FullName );

				try
				{
					System.IO.File.Delete( File.FullName );
				}
				catch( Exception E )
				{
					Logging.WriteLogLine( "Exception thrown when deleting " + File.FullName + " : " + E );
					Success = false;
				}
			}
		}

		return Success;
	}

	public void Execute_ExploreLocalSettingsDirectory()
	{
		var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var Directory    = AppDataLocal + "\\Ids2Client\\";

		try
		{
			Process.Start( "explorer", Directory );
		}
		catch( Exception E )
		{
			Logging.WriteLogLine( "ERROR : Opening " + Directory + " : " + E );
		}
	}


	public void Execute_PmlInventory()
	{
		RunProgram( Globals.PML_INVENTORY );
	}

	public void Execute_UpliftProducts()
	{
		RunProgram( Globals.UPLIFT_PRODUCTS );
	}

	public void Execute_UpliftShipments()
	{
		RunProgram( Globals.UPLIFT_SHIPMENTS );
	}

	public void Execute_ImportShipments()
	{
		Logging.WriteLogLine( "DEBUG Import Files" );

		new ImportShipments( Application.Current.MainWindow, true );
	}

	public void Execute_ReportBrowser()
	{
		RunProgram( Globals.REPORT_BROWSER );
	}

    public void Execute_PricingPosting()
    {
        RunProgram(Globals.PRICING_POSTING);
    }

    public void Execute_Invoicing()
    {
        RunProgram(Globals.INVOICING);
    }

    public void Execute_StressTest()
	{
		RunProgram( Globals.STRESS_TEST );
	}

	public void Execute_Install()
	{
		CloseAllPrograms();
		OnSignOut?.Invoke();
		Updater?.RunInstall();
		Application.Current.Shutdown();
	}

	public MainWindowModel()
	{
		UserName        = Globals.Dictionary.AsString( "NotSignedIn" );
		ProgramTabItems = ProgramTabs;

		if( !IsInDesignMode )
			HideMenuOptions();
	}

	public delegate void OnChangeLanguageEvent( LANGUAGE language );

	public delegate void OnLoginEvent( string companyName, string userName );

	public delegate void OnSignOutEvent();

	private ( PageTabItem? Tab, bool Running) IsProgramRunning( string pgmName )
	{
		foreach( var Tab in ProgramTabs )
		{
			if( Tab.Tag is string Prog && ( Prog == pgmName ) )
				return ( Tab, true );
		}

		return ( null, false );
	}


	private (PageTabItem? Tab, Page? Page) RunProgram( string pgmName, bool isPrimary = true )
	{
		try
		{
			if( Globals.Programs.TryGetValue( pgmName, out var Pgm ) )
			{
				Logging.WriteLogLine( $"\r\n----------------------------------------------------------------\r\nStarted program: {pgmName}" );

				var Page = (Page)Activator.CreateInstance( Pgm.PageType! );

				Page.Loaded += ( _, _ ) =>
							   {
								   switch( Page.DataContext )
								   {
								   case StartPageModel Context:

									   async void ContextOnLogin( string companyName, string userName )
									   {
										   if( Globals.Security.IsIds )
										   {
											   if( Globals.Security.IsNewCarrier )
											   {
												   Dispatcher.Invoke( () =>
																      {
																	      RunProgram( Globals.CARRIER_CREATE_WIZARD );
																      } );

												   return;
											   }
										   }

										   await EnableMenuOptions( userName );
										   OnLogin?.Invoke( companyName, userName );
									   }

									   Context.OnLogin = ContextOnLogin;

									   break;

//						               case ViewModelBase Model:
//							               Model.SetSecurity( Pgm.CanEdit, Pgm.CanDelete, Pgm.CanCreate );

//							               break;
								   }
							   };

				var Header = Globals.FindStringResource( Page.Title );

				var PrimaryTab = new PageTabItem
								 {
									 Tag        = pgmName,
									 Header     = Header,
									 AllowClose = Pgm.AllowClose,
									 Content    = Page
								 };
				ProgramTabs.Add( PrimaryTab );

				if( Pgm.RelatedPrograms is not null )
				{
					// Run related programs
					foreach( var (Name, _) in Pgm.RelatedPrograms )
					{
						if( Name is not null && !IsProgramRunning( Name ).Running )
						{
							//if (CanExecute is null || CanExecute())
							//    RunProgram( Name, false );
							RunProgram( Name, false );
						}
					}
				}

				if( isPrimary && TabControl.IsNotNull() )
					TabControl.SelectedItem = PrimaryTab;

				return ( PrimaryTab, Page );
			}
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
		}

		return ( null, null );
	}

#region Slots
	public string Slot
	{
		get { return Get( () => Slot, "" ); }
		set { Set( () => Slot, value ); }
	}

	public bool IsProductionSlot
	{
		get { return Get( () => IsProductionSlot, false ); }
		set { Set( () => IsProductionSlot, value ); }
	}


	public bool IsTestingSlot
	{
		get { return Get( () => IsTestingSlot, true ); }
		set { Set( () => IsTestingSlot, value ); }
	}

	[DependsUpon( nameof( Slot ) )]
	public void WhenSlotChanges()
	{
		IsTestingSlot = !IsProductionSlot;
	}
#endregion

#region Menu Options
	[Setting]
	public bool ShowIcons
	{
		get { return Get( () => ShowIcons, false ); }
		set { Set( () => ShowIcons, value ); }
	}

#region Inventory
	public bool InventoryVisible
	{
		get { return Get( () => InventoryVisible, IsInDesignMode ); }
		set { Set( () => InventoryVisible, value ); }
	}
#endregion

	public bool RolesVisible
	{
		get { return Get( () => RolesVisible, true ); }
		set { Set( () => RolesVisible, value ); }
	}


	public bool PreferencesVisible
	{
		get { return Get( () => PreferencesVisible, true ); }
		set { Set( () => PreferencesVisible, value ); }
	}


	public bool ReportsVisible
	{
		get { return Get( () => ReportsVisible, true ); }
		set { Set( () => ReportsVisible, value ); }
	}


	public bool AuditVisible
	{
		get { return Get( () => AuditVisible, true ); }
		set { Set( () => AuditVisible, value ); }
	}


	public bool AuditStaffVisible
	{
		get { return Get( () => AuditStaffVisible, true ); }
		set { Set( () => AuditStaffVisible, value ); }
	}


	public bool AuditTripVisible
	{
		get { return Get( () => AuditTripVisible, true ); }
		set { Set( () => AuditTripVisible, value ); }
	}

#region Staff
	public bool StaffVisible
	{
		get { return Get( () => StaffVisible, true ); }
		set { Set( () => StaffVisible, value ); }
	}


	public bool MaintainStaffVisible
	{
		get { return Get( () => MaintainStaffVisible, true ); }
		set { Set( () => MaintainStaffVisible, value ); }
	}


	public bool ShiftRatesVisible
	{
		get { return Get( () => ShiftRatesVisible, true ); }
		set { Set( () => ShiftRatesVisible, value ); }
	}

#region Actions
	public ICommand MaintainStaff => Commands[ nameof( MaintainStaff ) ];

	public void Execute_MaintainStaff()
	{
		RunProgram( Globals.MAINTAIN_STAFF );
	}

	public ICommand ShiftRates => Commands[ nameof( ShiftRates ) ];

	public void Execute_ShiftRates()
	{
		RunProgram( Globals.SHIFT_RATES );
	}
#endregion
#endregion


#region Shipments
	public bool ShipmentsVisible
	{
		get { return Get( () => ShipmentsVisible, true ); }
		set { Set( () => ShipmentsVisible, value ); }
	}


	public bool ShipmentVisible
	{
		get { return Get( () => ShipmentVisible, true ); }
		set { Set( () => ShipmentVisible, value ); }
	}


	public bool AdvancedShipmentVisible
	{
		get { return Get( () => AdvancedShipmentVisible, true ); }
		set { Set( () => AdvancedShipmentVisible, value ); }
	}
#endregion


	public bool SearchVisible
	{
		get { return Get( () => SearchVisible, true ); }
		set { Set( () => SearchVisible, value ); }
	}


	public bool RoutesVisible
	{
		get { return Get( () => RoutesVisible, true ); }
		set { Set( () => RoutesVisible, value ); }
	}

	public bool BoardsVisible
	{
		get { return Get( () => BoardsVisible, true ); }
		set { Set( () => BoardsVisible, value ); }
	}

	public bool DispatchBoardVisible
	{
		get { return Get( () => DispatchBoardVisible, true ); }
		set { Set( () => DispatchBoardVisible, value ); }
	}

	public bool DriversBoardVisible
	{
		get { return Get( () => DriversBoardVisible, true ); }
		set { Set( () => DriversBoardVisible, value ); }
	}


	public bool RateWizardVisible
	{
		get { return Get( () => RateWizardVisible, true ); }
		set { Set( () => RateWizardVisible, value ); }
	}


	public bool LogsVisible
	{
		get { return Get( () => LogsVisible, true ); }
		set { Set( () => LogsVisible, value ); }
	}


	public bool ViewLogsVisible
	{
		get { return Get( () => ViewLogsVisible, true ); }
		set { Set( () => ViewLogsVisible, value ); }
	}

	public bool ExploreLogsVisible
	{
		get { return Get( () => ExploreLogsVisible, true ); }
		set { Set( () => ExploreLogsVisible, value ); }
	}


	public bool CustomersVisible
	{
		get { return Get( () => CustomersVisible, true ); }
		set { Set( () => CustomersVisible, value ); }
	}


	public bool CustomerAccountsVisible
	{
		get { return Get( () => CustomerAccountsVisible, true ); }
		set { Set( () => CustomerAccountsVisible, value ); }
	}


	public bool CustomerAddressesVisible
	{
		get { return Get( () => CustomerAddressesVisible, true ); }
		set { Set( () => CustomerAddressesVisible, value ); }
	}
#endregion

#region Pml
	public bool PmlVisible
	{
		get { return Get( () => PmlVisible, IsInDesignMode ); }
		set { Set( () => PmlVisible, value ); }
	}


	public bool PmlProductsVisible
	{
		get { return Get( () => PmlProductsVisible, true ); }
		set { Set( () => PmlProductsVisible, value ); }
	}


	public bool PmlShipmentsVisible
	{
		get { return Get( () => PmlShipmentsVisible, true ); }
		set { Set( () => PmlShipmentsVisible, value ); }
	}
#endregion

#region Ids / Diagnostics
	public void Execute_ServerTime()
	{
		RunProgram( Globals.SERVER_TIME );
	}

	public void Execute_BackupRestore()
	{
		RunProgram( Globals.BACKUP_RESTORE );
	}

	public void Execute_TestReports()
	{
		RunProgram( Globals.WEB_REPORTS );
	}

	public ICommand NewTripEntry => Commands[ nameof( NewTripEntry ) ];

	public void Execute_NewTripEntry()
	{
//		RunProgram( Globals.TRIP_ENTRY_V2 );
	}
#endregion

#region Show Menu
	public bool ShowMenu
	{
		get { return Get( () => ShowMenu, IsInDesignMode ); }
		set { Set( () => ShowMenu, value ); }
	}


	[DependsUpon( nameof( ShowMenu ) )]
	public void WhenShowMenuChanges()
	{
		IsIds = Globals.Security.IsIds;
	}
#endregion
}