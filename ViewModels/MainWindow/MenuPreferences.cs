﻿#nullable enable

namespace ViewModels;

public static partial class Globals
{
	public static partial class Menu
	{
		public static MenuPreference Preferences
		{
			get { return _Preferences ??= Azure.Client.RequestMenuPreferences().Result; }
		}

		private static MenuPreference? _Preferences;

		public static void ResetMenuPreferences()
		{
			Task.Run( async () =>
			          {
				          _Preferences = await Azure.Client.RequestMenuPreferences();
			          } );
		}
	}
}