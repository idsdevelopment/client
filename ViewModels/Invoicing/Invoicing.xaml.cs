﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewModels.Dialogues;
using ViewModels.ReportBrowser;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Invoicing;

/// <summary>
/// Interaction logic for Invoicing.xaml
/// </summary>
public partial class Invoicing : Page
{
    private InvoicingModel? Model { get; set; } = null;

    public Invoicing()
    {
        InitializeComponent();

        if (DataContext is InvoicingModel m)
        {
            Model = m;
            Model.SetView(this);

            //Model.FromDate = Model.ToDate = DateTime.Now;
            //DpFrom_SelectedDatesChanged(null, null);
            DpFrom.SelectedDate = DateTime.Now;
            DpTo.SelectedDate = DateTime.Now.ToEndOfDay();
            DpBulkFrom.SelectedDate = DateTime.Now;
            DpBulkTo.SelectedDate = DateTime.Now.ToEndOfDay();
            CbOnlyWithPosted.IsChecked = true;
            CbOnlyWithPosted_Checked();
        }

        TbFilter.Focus();
    }

    private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

    public void SetWaitCursor()
    {
        Mouse.OverrideCursor = Cursors.Wait;
    }

    public void ClearWaitCursor()
    {
        Mouse.OverrideCursor = null;
    }

    #region Account Invoicing
    private void BtnPreview_Click(object sender, RoutedEventArgs e)
    {
        Model ??= (InvoicingModel)DataContext;
        SetWaitCursor();
        // adname (accountId)
        string accountId = Model.GetAccountIdFromFormattedCompanyName(Model.CompanyName);
        if (accountId.IsNotNullOrWhiteSpace())
        {
            //List<Trip> trips = (from T in Model.PostedTrips where T.AccountId == accountId select T).ToList();
            List<string> ids = (from T in Model.PostedTrips where T.AccountId == accountId select T.TripId).ToList();
            if (ids?.Count > 0)
            {
                List<Trip> trips = Model.GetTripsWithSignatures(ids);

                (bool success, long invoiceId) = Model.CreateInvoice(accountId, trips, true);

                //if (success)
                //{
                //    Logging.WriteLogLine($"Loading invoice {invoiceId}");
                //    //Dispatcher.Invoke(() =>
                //    //{
                //    //});
                //    InvoicingModel.InvoiceReportSettings rs = BuildReportSettings(accountId, trips, invoiceId);
                //    ReportDialog rd = new(Application.Current.MainWindow, rs);
                //    rd.ShowDialog();
                //}
            }
            else
            {
                Logging.WriteLogLine($"No trips for account {accountId}");
                string title = FindStringResource("InvoicingErrorNoTripsTitle");
                string message = FindStringResource("InvoicingErrorNoTripsMessage");
                message = message.Replace("@1", accountId) + "\n" + FindStringResource("InvoicingErrorNoTripsMessage1");
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        ClearWaitCursor();
    }

    private void BtnRun_Click(object sender, RoutedEventArgs e)
    {
        Model ??= (InvoicingModel)DataContext;
        SetWaitCursor();
        
        string accountId = Model.GetAccountIdFromFormattedCompanyName(Model.CompanyName);                
        if (accountId.IsNotNullOrWhiteSpace())
        {
            //List<Trip> trips = (from T in Model.PostedTrips where T.AccountId == accountId select T).ToList();
            List<string> ids = (from T in Model.PostedTrips where T.AccountId == accountId select T.TripId).ToList();
            if (ids?.Count > 0)
            {
                string title = FindStringResource("InvoicingAccountDialogueTitle");
                string message = FindStringResource("InvoicingAccountDialogueMessage");
                message = message.Replace("@1", accountId);
                message = message.Replace("@2", ids.Count.ToString());
                message += "\n" + FindStringResource("InvoicingAccountDialogueMessage1");
                MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (mbr == MessageBoxResult.Yes)
                {
                    List<Trip> trips = Model.GetTripsWithSignatures(ids);

                    (bool success, long invoiceId) = Model.CreateInvoice(accountId, trips, true);
                }
                else
                {
                    Logging.WriteLogLine($"Creating invoice for {accountId} cancelled");
                }
            }
            else
            {
                Logging.WriteLogLine($"No trips for account {accountId}");
                string title = FindStringResource("InvoicingErrorNoTripsTitle");
                string message = FindStringResource("InvoicingErrorNoTripsMessage");
                message = message.Replace("@1", accountId) + "\n" + FindStringResource("InvoicingErrorNoTripsMessage1");
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        ClearWaitCursor();
    }

    private InvoicingModel.InvoiceReportSettings BuildReportSettings(string accountId, List<Trip> trips, long invoiceId)
    {
        Model ??= (InvoicingModel)DataContext;

        string id = Globals.Company.CarrierId;
        (PrimaryCompany? pc, CompanyAddress? ca) = GetReseller(id);

        InvoicingModel.InvoiceReportSettings rs = new()
        {
            Trips = trips,
            //InvoicingCompany = ,
            Description = FindStringResource("ReportDialogPhoenixInvoiceTitle"),
            CompanyCode = accountId,
            SelectedFrom = Model.FromDate.StartOfDay(),
            SelectedTo = Model.ToDate.EndOfDay(),
            SelectedFromStatus = STATUS.INVOICED.AsString(),
            SelectedToStatus = STATUS.INVOICED.AsString()
        };
        
        return rs;
    }

    private (PrimaryCompany? pc, CompanyAddress? ca) GetReseller(string companyId)
    {
        PrimaryCompany? company = null;
        CompanyAddress? resellerCompany = null;

        Task.WaitAll(
            Task.Run( async () =>
            {
                company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(companyId);
                resellerCompany = await Azure.Client.RequestGetResellerCustomerCompany(companyId);
            })
        );

        return (company, resellerCompany );
    }

    private void CbOnlyWithPosted_Checked(object? sender = null, RoutedEventArgs? e = null)
    {
        Model ??= (InvoicingModel)DataContext;
        TbFilter.Focus();
        Model.OnlyShowAccountWithPosted = true;
        Model.WhenOnlyShowAccountWithPostedChanges();
        //Model?.WhenOnlyShowAccountWithPostedChanges();
    }

    private void CbOnlyWithPosted_Unchecked(object sender, RoutedEventArgs e)
    {
        Model ??= (InvoicingModel)DataContext;
        TbFilter.Focus();
        Model.OnlyShowAccountWithPosted = false;
        //Model?.WhenOnlyShowAccountWithPostedChanges();
    }

    private void TbFilter_PreviewKeyDown(object sender, KeyEventArgs e)
    {
        //if (Model != null && e.Key == Key.Escape && sender is TextBox tb)
        //{
        //    tb.Text = string.Empty;
        //    Model.AdhocFilter = string.Empty;
        //    //Model.Execute_Filter("");
        //    Model.Execute_Filter();
        //}
        if (Model != null && sender is TextBox tb)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    tb.Text = string.Empty;
                    Model.AdhocFilter = string.Empty;
                    //Model.Execute_Filter("");
                    Model.Execute_Filter();
                    break;

                //case Key.Up:
                case Key.Down:
                    LbAccounts.Focus();
                    break;

                default:
                    break;
            }
        }
    }

    private void TbFilter_TextChanged(object sender, TextChangedEventArgs e)
    {
        if (Model != null && sender is TextBox tb)
        {
            Model.AdhocFilter = tb.Text.Trim();
            Model.Execute_Filter();

        }
    }

    private void DpFrom_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
    {
        if (Model != null && DpFrom.SelectedDate != null)
        {
            Model.FromDate = new DateTimeOffset(DpFrom.SelectedDate.Value).StartOfDay(); 
            LblSummary.Content = Model.UpdateSummary();
            Task.Run( async () =>
            {
                await Model.GetPostedTrips();
            });
        }
    }

    private void DpTo_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
    {
        if (Model != null && DpTo.SelectedDate != null)
        {
            Model.ToDate = new DateTimeOffset(DpTo.SelectedDate.Value).EndOfDay();
            LblSummary.Content = Model.UpdateSummary();
            Task.Run(async () =>
            {
                await Model.GetPostedTrips();
            });
        }
    }

    private void LbAccounts_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (Model != null && LbAccounts.SelectedItem != null)
        {
            Model.CompanyName = LbAccounts.SelectedItem.ToString();
            LblSummary.Content = Model.UpdateSummary();
        }
    }

    private void LbAccounts_PreviewKeyDown(object sender, KeyEventArgs e)
    {
        Model ??= (InvoicingModel)DataContext;
        if (e.Key == Key.Up && LbAccounts.SelectedIndex == 0) 
        { 
            TbFilter.SelectAll();
            TbFilter.Focus();
        }
    }

    private void LvInvoices_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {

    }

    private void BtnViewInvoice_Click(object sender, RoutedEventArgs e)
    {

    }

    private void LvInvoices_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        Model ??= (InvoicingModel)DataContext;
        Model.SelectedInvoice = (InvoicingModel.TestInvoice)LvInvoices.SelectedItem;
        Model.IsInvoiceSelected = LvInvoices.SelectedIndex > -1;
    }
#endregion

    #region Bulk Invoicing & Statements
    private void DpBulkFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
        if (Model != null && DpBulkFrom.SelectedDate != null)
        {
            Model.BulkFromDate = new DateTimeOffset(DpBulkFrom.SelectedDate.Value).StartOfDay();
            Task.Run(async () =>
            {
                await Model.GetPostedTripsForBulkInvoicing();
            });
        }
    }

    private void DpBulkTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
        if (Model != null && DpBulkTo.SelectedDate != null)
        {
            Model.BulkToDate = new DateTimeOffset(DpBulkTo.SelectedDate.Value).EndOfDay();
            Task.Run(async () =>
            {
                await Model.GetPostedTripsForBulkInvoicing();
            });
        }
    }
    private void BtnBulkCreate_Click(object sender, RoutedEventArgs e)
    {

    }

    private void LvBulkTrips_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (LvBulkTrips.SelectedItem is Trip trip)
        {
            var dt = new DisplayTrip(trip, new Trips.Boards.Common.ServiceLevel());
            Logging.WriteLogLine($"Opening new {Globals.TRIP_ENTRY} tab to display trip: {dt.TripId}");
            Globals.RunNewProgram(Globals.TRIP_ENTRY, dt);        
        }
    }

    #endregion
}
