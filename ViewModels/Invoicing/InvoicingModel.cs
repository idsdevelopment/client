﻿#nullable enable

using DocumentFormat.OpenXml.Office2010.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.Customers;
using ViewModels.ReportBrowser;

namespace ViewModels.Invoicing;

public class InvoicingModel : ViewModelBase
{
    protected override async void OnInitialised()
    {
        base.OnInitialised();

        Loaded = false;
        await Task.WhenAll(
                            GetPreferences()
                          );
        await Task.WhenAll(
                            GetCompanyNames(),
                            GetPostedTrips()
                          );
    }

    #region Data
    private Invoicing? View = null;

    private readonly string PROGRAM_NAME = "InvoicingModel";

    public void SetView(Invoicing view)
    {
        View = view;
    }

    public GlobalAddressesModel GlobalAddressesModel
    {
        get => Get(() => GlobalAddressesModel, GlobalAddressesModel.GetInstance(this));
    }

    private static string FindStringResource(string name) => (string)Application.Current.TryFindResource(name);

    public bool Loaded { get; set; } = false;

    public string GlobalAddressBookId = Globals.DataContext.MainDataContext.GLOBAL_ADDRESS_BOOK_NAME;

    public Preference? PreferenceNextInvoiceNumber;

    #endregion

    #region Actions
    public async Task GetPreferences()
    {
        if (!IsInDesignMode)
        {
            Logging.WriteLogLine("Getting Preferences");
            Preferences Prefs = await Azure.Client.RequestPreferences();

            Logging.WriteLogLine("Found " + Prefs?.Count + " preferences;");


            if (Prefs?.Count > 0)
            {
                Preference? pref = (from P in Prefs
                                    where P.Description.Contains("Invoices: By Delivered Date")
                                    select P).FirstOrDefault();
                SearchByDeliveredDate = (pref != null) && pref.Enabled;
                Logging.WriteLogLine($"Preference {pref?.Description} is {SearchByDeliveredDate}");

                SearchBy = SearchByDeliveredDate ? FindStringResource("InvoicingLabelSearchingByDelivered") : FindStringResource("InvoicingLabelSearchingByCallTime");

                pref = (from P in Prefs
                        where P.Description.Contains("Invoice Billing Type: Additional groups (Monthly 2 etc)")
                        select P).FirstOrDefault();
                InvoiceBillingTypeAdditionalGroups = (pref != null) && pref.Enabled;
                Logging.WriteLogLine($"Preference '{pref?.Description}' is {InvoiceBillingTypeAdditionalGroups}");

                BulkBillingPeriods = InvoiceBillingTypeAdditionalGroups ? GetBillingPeriodsWithAdditional() : GetBillingPeriods();
            }
        }
    }
    #endregion

    #region Data Account Invoicing

    private Dictionary<string, CustomerCodeCompanyName> DictCustomerNamesIds = new();

    private Dictionary<string, List<Trip>> DictAccountIdTrips = new();

    private List<string> AllCompanyNames { get; set; } = new();

    public ObservableCollection<string> CompanyNames
    {
        get => Get(() => CompanyNames, new ObservableCollection<string>());
        set => Set(() => CompanyNames, value);
    }

    public List<string> PostedCompanyNames
    {
        get => Get(() => PostedCompanyNames, new List<string>());
        set => Set(() => PostedCompanyNames, value);
    }

    public string CompanyName
    {
        get => Get(() => CompanyName, "");
        set => Set(() => CompanyName, value);
    }

    [DependsUpon(nameof(CompanyName))]
    public void WhenCompanyNameChanges()
    {
        if (CompanyName.IsNotNullOrWhiteSpace())
        {
            GetInvoicesForAccount(GetAccountIdFromFormattedCompanyName(CompanyName));
            Logging.WriteLogLine($"Found {Invoices.Count} invoices for {CompanyName}");
        }
        else
        {
            Invoices.Clear();
        }

    }


    private List<string> FormatCompanyNames(List<string> companyNames)
    {
        List<string> names = new();

        if (companyNames != null)
        {
            foreach (var companyName in companyNames)
            {
                string name = FormatCompanyName(companyName);
                names.Add(name);
            }
        }

        return names;
    }

    private string FormatCompanyName(string companyName)
    {
        string formatted = companyName;

        if (companyName.IsNotNullOrWhiteSpace())
        {
            string id = (from C in DictCustomerNamesIds.Keys where C == companyName select DictCustomerNamesIds[C].CustomerCode).FirstOrDefault();
            formatted = $"{companyName} ({id})";
        }

        return formatted;
    }

    public string GetAccountIdFromFormattedCompanyName(string formatted)
    {
        string accountId = string.Empty;
        if (formatted.IsNotNullOrWhiteSpace())
        {
            string[] pieces = formatted.Split('(');
            //                          0   1      2
            // Problem - some names are xxx(bobs) (id)
            if (pieces.Length >= 2)
            {
                pieces = pieces[pieces.Length - 1].Split(')');
                accountId = pieces[0].Trim();
            }
        }

        return accountId;
    }


    public TripList PostedTrips { get; set; } = new();


    public bool OnlyShowAccountWithPosted
    {
        get => Get(() => OnlyShowAccountWithPosted, false);
        set => Set(() => OnlyShowAccountWithPosted, value);
    }

    [DependsUpon(nameof(OnlyShowAccountWithPosted))]
    public void WhenOnlyShowAccountWithPostedChanges()
    {
        if (OnlyShowAccountWithPosted)
        {
            // Have to reload as the user might have gone away and posted some trips
            _ = GetPostedTrips();
            if (PostedTrips.Count > 0)
            {
                CompanyNames = new(PostedCompanyNames);
            }
            else
            {
                CompanyNames.Clear();
            }
        }
        else
        {
            CompanyNames = new ObservableCollection<string>(AllCompanyNames);
        }
    }

    public string AdhocFilter
    {
        get => Get(() => AdhocFilter, "");
        set => Set(() => AdhocFilter, value);
    }

    public DateTimeOffset FromDate
    {
        get { return Get(() => FromDate, DateTimeOffset.Now); }
        set { Set(() => FromDate, value); }
    }

    public string DisplayFromDate
    {
        get => Get(() => DisplayFromDate, "");
        set => Set(() => DisplayFromDate, value);
    }

    [DependsUpon(nameof(FromDate))]
    public void WhenFromDateChanges()
    {
        DisplayFromDate = $"{$"{FromDate.DateTime.ToLongDateString()}"} {FromDate.DateTime.ToLongTimeString()}";
        //DisplayFromDate = $"{FromDate.DateTime.ToLongDateString()} {FromDate.DateTime.ToLongTimeString()}";
    }

    public DateTimeOffset ToDate
    {
        get { return Get(() => ToDate, DateTimeOffset.Now); }
        set { Set(() => ToDate, value); }
    }

    public string DisplayToDate
    {
        get => Get(() => DisplayToDate, "");
        set => Set(() => DisplayToDate, value);
    }

    [DependsUpon(nameof(ToDate))]
    public void WhenToDateChanges()
    {
        DisplayToDate = $"{$"{ToDate.DateTime.ToLongDateString()}"} {ToDate.DateTime.ToLongTimeString()}";
        //DisplayFromDate = $"{ToDate.DateTime.ToLongDateString()} {ToDate.DateTime.ToLongTimeString()}";
    }

    public Visibility NoPostedTripsVisibility
    {
        get => Get(() => NoPostedTripsVisibility, Visibility.Hidden);
        set => Set(() => NoPostedTripsVisibility, value);
    }

    private Visibility SetVisibility()
    {
        Visibility visibility = Visibility.Hidden;
        if (PostedTrips.Count == 0)
        {
            visibility = Visibility.Visible;
        }
        NoPostedTripsVisibility = visibility;

        Logging.WriteLogLine("Setting message visibility to " + NoPostedTripsVisibility.ToString());
        return visibility;
    }

    [DependsUpon(nameof(CompanyName))]
    [DependsUpon(nameof(FromDate))]
    public void WhenCompanyNameOrDatesChange()
    {
        IsRunEnabled = CompanyName.IsNotNullOrWhiteSpace() && FromDate != null && ToDate != null;
    }

    public bool IsRunEnabled
    {
        get => Get(() => IsRunEnabled, false);
        set => Set(() => IsRunEnabled, value);
    }

    public string InvoicingSummary
    {
        get => Get(() => InvoicingSummary, UpdateSummary());
        set => Set(() => InvoicingSummary, value);
    }

    [DependsUpon(nameof(CompanyName))]
    [DependsUpon(nameof(FromDate))]
    [DependsUpon(nameof(ToDate))]
    [DependsUpon(nameof(IsRunEnabled))]
    public string UpdateSummary()
    {
        string accountId = GetAccountIdFromFormattedCompanyName(CompanyName);
        List<Trip> trips = (from T in PostedTrips where T.AccountId == accountId select T).ToList();
        int count = trips?.Count ?? 0;
        string message = FindStringResource("InvoicingSummary");
        message = message.Replace("@1", count.ToString());
        message = message.Replace("@2", accountId);
        message = message.Replace("@3", DisplayFromDate);
        message = message.Replace("@4", DisplayToDate);

        return message;
    }

    public bool SearchByDeliveredDate
    {
        get => Get(() => SearchByDeliveredDate, false);
        set => Set(() => SearchByDeliveredDate, value);
    }

    public string SearchBy
    {
        get => Get(() => SearchBy, FindStringResource("InvoicingLabelSearchingByCallTime"));
        set => Set(() => SearchBy, value);
    }

    // TODO Connect to Terry's objects
    public TestInvoiceList Invoices
    {
        //get => Get(() => Invoices, new InvoiceList());
        get => Get(() => Invoices, new TestInvoiceList());
        set => Set(() => Invoices, value);
    }

    public TestInvoice? SelectedInvoice { get; set; } = null;

    [DependsUpon(nameof(SelectedInvoice))]
    public void WhenSelectedInvoiceChanges()
    {
        IsInvoiceSelected = SelectedInvoice != null;
    }
    public bool IsInvoiceSelected
    {
        get => Get(() => IsInvoiceSelected, false);
        set => Set(() => IsInvoiceSelected, value);
    }
    #endregion

    #region Actions Account Invoicing
    public async Task GetCompanyNames()
    {
        if (!IsInDesignMode)
        {
            var Companies = (from Company in await Azure.Client.RequestGetCustomerCodeCompanyNameList()
                             let CompanyName = Company.CompanyName.Trim().ToLower()
                             orderby CompanyName
                             group Company by CompanyName
                                  into G
                             select G.First()).ToList();
            DictCustomerNamesIds = Companies.ToDictionary(a => a.CompanyName, a => a);

            // Don't include the Global Address Book in the list
            //var Names = new ObservableCollection<string>(from Company in Companies
            //                                             where Company.CompanyName != GlobalAddressBookId
            //                                             select Company.CompanyName);
            List<string> tmp = new(from Company in Companies
                                   where Company.CompanyName != GlobalAddressBookId
                                   select Company.CompanyName);
            CompanyNames = new(FormatCompanyNames(tmp));
            //AllCompanyNames.AddRange(Names);
            AllCompanyNames = new(FormatCompanyNames(tmp));

            Loaded = true;
            Logging.WriteLogLine("Loaded " + AllCompanyNames.Count + " companies");

            WhenOnlyShowAccountWithPostedChanges();
        }
    }

    public async Task GetPostedTrips()
    {
        if (!IsInDesignMode && View != null)
        {
            Logging.WriteLogLine("Getting Posted shipments by " + (SearchByDeliveredDate ? " Delivered Date" : " CallTime"));
            Dispatcher.Invoke(() =>
            {
                View.SetWaitCursor();
            });

            PostedTrips = await Azure.Client.RequestSearchTrips(new SearchTrips
            {
                UniqueUserId = Globals.DataContext.MainDataContext.UserName,
                StartStatus = STATUS.POSTED,
                EndStatus = STATUS.POSTED,
                FromDate = FromDate,
                ToDate = ToDate,
                ByDelTime = SearchByDeliveredDate,
                IncludeCharges = true,
            });

            Logging.WriteLogLine($"Found {PostedTrips.Count} posted shipments");

            GetPostedCompanyNames();

            Loaded = true;

            SetVisibility();
            Dispatcher.Invoke(() =>
            {
                View.ClearWaitCursor();
            });
        }
    }

    public List<Trip> GetTripsWithSignatures(List<string> ids)
    {
        List<Trip> sigs = new();

        Task.WaitAll(
            Task.Run(async () =>
            {
                int tripsThatHaveSignatures = 0;
                foreach (string id in ids)
                {
                    var Latest = await Azure.Client.RequestGetTrip(new GetTrip
                    {
                        Signatures = true,
                        TripId = id
                    });
                    sigs.Add(Latest);

                    if (Latest.Signatures.Count > 0)
                    {
                        ++tripsThatHaveSignatures;
                    }
                }

                Logging.WriteLogLine($"There are {tripsThatHaveSignatures} trips that have signatures.");

            })

        );

        return sigs;
    }

    private void GetPostedCompanyNames()
    {
        if (PostedTrips.Count > 0)
        {
            //PostedCompanyNames.Clear();

            // List of AccountIds
            var list = (from T in PostedTrips orderby T.AccountId group T by T.AccountId into G select G).ToList();
            List<string> tmp = list.Select(x => x.Key).ToList();
            List<string> names = new();
            StringBuilder badAccountIdsAndTrips = new();
            foreach (var id in tmp)
            {
                if (id.IsNotNullOrWhiteSpace())
                {
                    string name = (from C in DictCustomerNamesIds.Values where C.CustomerCode == id select C.CompanyName).FirstOrDefault();
                    if (name != null)
                    {
                        names.Add(name);
                    }
                    else
                    {
                        List<Trip> bad = (from T in PostedTrips where T.AccountId == id select T).ToList();
                        StringBuilder sb = new();
                        if (bad != null)
                        {
                            foreach(var trip in bad)
                            {
                                sb.Append(trip.TripId).Append(",");
                            }
                        }
                        Logging.WriteLogLine($"Can't find a company for accountId: {id} - Trips:\n{sb}\n");
                        badAccountIdsAndTrips.Append($"Can't find a company for accountId: {id} - Trips:\n{sb}\n");
                    }

                    // TODO DEBUG
                    List<Trip> trips = (from T in PostedTrips where T.AccountId == id select T).ToList();
                    Logging.WriteLogLine($"Account {id}: Found {trips?.Count} trips");
                }
            }

            if (badAccountIdsAndTrips.Length > 0)
            {
                Dispatcher.Invoke(() =>
                {
                    string title = FindStringResource("InvoicingAccountNoCompanyForAccountIdTitle");
                    string message = FindStringResource("InvoicingAccountNoCompanyForAccountIdMessage") + "\n" + FindStringResource("InvoicingAccountNoCompanyForAccountIdMessage1");
                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
                    NotepadHelper.ShowMessage(badAccountIdsAndTrips.ToString(), title);
                });
            }

            PostedCompanyNames = new(FormatCompanyNames(names));

            Logging.WriteLogLine($"Found {PostedCompanyNames.Count} Companies with Posted trips");

            if (OnlyShowAccountWithPosted)
            {
                Dispatcher.Invoke(() =>
                {
                    CompanyNames = new(PostedCompanyNames);
                });
            }
        }
        else
        {
            //PostedCompanyNames.Clear();
            PostedCompanyNames = new();
            if (OnlyShowAccountWithPosted)
            {
                CompanyNames = new(PostedCompanyNames);
            }
        }
    }

    public void Execute_Filter()
    {
        if (AdhocFilter.IsNotNullOrWhiteSpace())
        {
            CompanyNames.Clear();
            string filter = AdhocFilter.Trim().ToLower();
            if (!OnlyShowAccountWithPosted)
            {
                //List<string> matches = (from C in AllCompanyNames where FormatCompanyName(C.ToLower()).Contains(filter) select C).ToList();
                List<string> matches = (from C in AllCompanyNames where C.IsNotNullOrWhiteSpace() && C.ToLower().Contains(filter) select C).ToList();
                CompanyNames = new(matches);
            }
            else
            {
                // TODO Getting nulls in PostedCompanyNames
                //List<string> matches = (from C in PostedCompanyNames where FormatCompanyName(C.ToLower()).Contains(filter) select C).ToList();
                List<string> matches = (from C in PostedCompanyNames where C.IsNotNullOrWhiteSpace() && C.ToLower().Contains(filter) select C).ToList();
                CompanyNames = new(matches);
            }
        }
        else
        {
            CompanyNames = OnlyShowAccountWithPosted ? new ObservableCollection<string>(PostedCompanyNames) : new ObservableCollection<string>(AllCompanyNames);
        }


    }

    public (bool success, long invoiceId) CreateInvoice(string accountId, List<Trip> trips, bool isPreview = false)
    {
        bool success = true;
        long invoiceId = 0;
        if (!IsInDesignMode)
        {
            if (!isPreview)
            {
                Logging.WriteLogLine($"Creating invoice for AccountId: {accountId} FromDate: {FromDate.StartOfDay()} ToDate: {ToDate.EndOfDay()}");
            }
            else
            {
                Logging.WriteLogLine($"Creating PREVIEW invoice for AccountId: {accountId} FromDate: {FromDate.StartOfDay()} ToDate: {ToDate.EndOfDay()}");
            }

            //List<TripCharge> tripCharges = new();
            //List<string> tripIds = new();
            //foreach (Trip trip in trips)
            //{
            //    tripIds.Add(trip.TripId);
            //    tripCharges.AddRange(trip.TripCharges);
            //    if (trip.BillingCompanyName.IsNullOrWhiteSpace() )
            //    {
            //        string name = (from C in DictCustomerNamesIds.Values where C.CustomerCode == trip.AccountId select C.CompanyName).FirstOrDefault();
            //        trip.BillingCompanyName = name;
            //    }
            //}

            (trips, List<string> tripIds, List<TripCharge> tripCharges, decimal totalCharges, decimal totalAmount, decimal totalTaxAmount) = AccumulateValuesForInvoice(trips);

            CompanyAddress ca = (from C in GlobalAddressesModel.Companies where C.CompanyNumber == trips[0].AccountId select C).FirstOrDefault();
            if (ca != null)
            {
                Invoice invoice = new()
                {
                    BillingCompanyName = trips[0].BillingCompanyName,
                    BillingPeriod = ca.BillingPeriod,
                    CombinedTripCharges = tripCharges,
                    EmailInvoice = ca.EmailInvoice,
                    InvoiceDateTime = DateTimeOffset.Now,
                    InvoiceFrom = FromDate,
                    InvoiceTo = ToDate,
                    IsCreditNode = false, // TODO
                    ProgramName = PROGRAM_NAME,
                    TripIds = tripIds,
                    TotalCharges = totalCharges,
                    TotalTaxA = totalTaxAmount,
                    TotalValue = totalAmount,
                    //TotalDiscount = totalDiscount,
                };
                DumpInvoiceToLog(invoice);
                Task.Run(async () =>
                {
                    invoiceId = await Azure.Client.RequestAddInvoice(invoice);
                    Logging.WriteLogLine($"Saved invoice {invoiceId} From {invoice.InvoiceFrom} To {invoice.InvoiceTo}");
                    if (invoiceId < 0)
                    {
                        success = false;
                    }
                    // Remove the account and trips from the list
                    if (View != null)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            View.LbAccounts.ItemsSource = null;
                        });
                        await GetPostedTrips();
                        Dispatcher.Invoke(() =>
                        {   
                            InvoicingSummary = string.Empty;
                            View.LblSummary.Content = string.Empty;
                            View.LbAccounts.ItemsSource = CompanyNames;
                        });
                    }
                });
            }
        }

        return (success, invoiceId);
    }

    private (List<Trip> trips, List<string> tripIds, List<TripCharge> tripCharges, decimal totalCharges, decimal totalAmount, decimal totalTaxAmount) AccumulateValuesForInvoice(List<Trip> trips)
    {
        List<TripCharge> tripCharges = new();
        List<string> tripIds = new();
        decimal totalCharges = 0;
        decimal totalAmount = 0;
        decimal totalFixedAmount = 0;
        decimal totalTaxAmount = 0;
        
        foreach (Trip trip in trips)
        {
            tripIds.Add(trip.TripId);
            tripCharges.AddRange(trip.TripCharges);
            foreach (TripCharge tc in trip.TripCharges)
            {
                if (tc.Value > 0)
                {
                    totalCharges += tc.Value;
                }
            }

            if (trip.BillingCompanyName.IsNullOrWhiteSpace())
            {
                string name = (from C in DictCustomerNamesIds.Values where C.CustomerCode == trip.AccountId select C.CompanyName).FirstOrDefault();
                trip.BillingCompanyName = name;
            }

            totalAmount += trip.TotalAmount;
            totalFixedAmount += trip.TotalFixedAmount;
            totalTaxAmount += trip.TotalTaxAmount;
        }
        return (trips, tripIds, tripCharges, totalCharges, totalAmount, totalTaxAmount);
    }

    private void DumpInvoiceToLog(Invoice invoice)
    {
        if (invoice != null)
        {
            StringBuilder ids = new();
            int i = 0;
            foreach (string id in invoice.TripIds)
            {
                if (++i % 20 == 0)
                {
                    ids.Append("\n\t");
                }
                ids.Append(id).Append(",");
            }
            StringBuilder tcs = new();
            foreach (TripCharge tc in invoice.CombinedTripCharges)
            {
                tcs.Append("\tChargeId: ").Append(tc.ChargeId).Append(" Quantity: ").Append(tc.Quantity).Append(" Text: ").Append(tc.Text).Append(" Value: ").Append(tc.Value).Append("\n");
            }
            StringBuilder sb = new();
            if (invoice.InvoiceNumber > 0)
            {
                sb.AppendLine($"InvoiceId: {invoice.InvoiceNumber}");
            }
            sb.AppendLine($"BillingCompany: {invoice.BillingCompanyName}");
            sb.AppendLine($"BillingPeriod: {invoice.BillingPeriod}");
            sb.AppendLine($"EmailAddress: {invoice.EmailAddress}");
            sb.AppendLine($"EmailInvoice: {invoice.EmailInvoice}");
            sb.AppendLine($"EmailSubject: {invoice.EmailSubject}");
            sb.AppendLine($"InvoiceDateTime: {invoice.InvoiceDateTime}");
            sb.AppendLine($"InvoiceFrom: {invoice.InvoiceFrom}");
            sb.AppendLine($"InvoiceTo: {invoice.InvoiceTo}");
            sb.AppendLine($"IsCreditNote: {invoice.IsCreditNode}");
            sb.AppendLine($"NonTaxableValue: {invoice.NonTaxableValue}");
            sb.AppendLine($"Residual: {invoice.Residual}");
            sb.AppendLine($"TaxableValue: {invoice.TaxableValue}");
            sb.AppendLine($"TotalCharges: {invoice.TotalCharges}");
            sb.AppendLine($"TotalDiscount: {invoice.TotalDiscount}");
            sb.AppendLine($"TotalTaxA: {invoice.TotalTaxA}");
            sb.AppendLine($"TotalTaxB: {invoice.TotalTaxB}");
            sb.AppendLine($"TotalValue: {invoice.TotalValue}");
            sb.AppendLine($"ProgramName: {invoice.ProgramName}");
            sb.AppendLine($"TripIds: {ids}");
            sb.AppendLine($"CombinedTripCharges:\n{tcs}");

            Logging.WriteLogLine($"Invoice:\n{sb}\n");
        }
    }

    public async void GetInvoicesForAccount(string accountId)
    {
        if (!IsInDesignMode)
        {
            /*
            // TODO Connect to Terry's invoicing
            // For the moment, using fake data
            Logging.WriteLogLine($"Fetching invoices for {accountId}");
            InvoiceSearch search = new()
            {
                FromDate = DateTime.Now.AddYears(-1),
                ToDate = DateTime.Now,
                Search = InvoiceSearch.SEARCH.BY_DATE,
            };
            var invoices = await Azure.Client.RequestSearchInvoices(search);

            Invoices = new(false);
            Logging.WriteLogLine($"Found {invoices?.Count} invoices");
            */
        }
    }
    #endregion

    #region Bulk Invoicing Data
    public List<string> BulkReportTypes
    {
        get { return Get(() => BulkReportTypes, GetReportTypes()); }
        set { Set(() => BulkReportTypes, value); }
    }

    public string SelectedReportType
    {
        get => Get(() => SelectedReportType, "");
        set => Set(() => SelectedReportType, value);
    }

    public List<string> GetReportTypes() => new()
    {
        FindStringResource("InvoicingBulkReportTypeInvoice"),
        FindStringResource("InvoicingBulkReportTypeStatement"),
        FindStringResource("InvoicingBulkReportTypeCollectionLetter")
    };

    public List<string> BulkBillingMethods
    {
        get { return Get(() => BulkBillingMethods, GetBillingMethods); }

        //set { Set( () => SelectedAccountBillingMethod, value ); }
    }

    public List<string> GetBillingMethods() => new()
                                               {
                                                   FindStringResource( "AccountsBillingMethodPrint" ),
                                                   FindStringResource( "AccountsBillingMethodEmail" )
                                               };

    public string SelectedBulkBillingMethod
    {
        get => Get(() => SelectedBulkBillingMethod, "");
        set => Set(() => SelectedBulkBillingMethod, value);
    }

    public List<string> BulkBillingPeriods
    {
        get { return Get(() => BulkBillingPeriods, GetBillingPeriods); }
        set => Set(() => BulkBillingPeriods, value);
    }

    public string SelectedBillingPeriod
    {
        get => Get(() => SelectedBillingPeriod, "");
        set => Set(() => SelectedBillingPeriod, value);
    }

    public bool InvoiceBillingTypeAdditionalGroups
    {
        get => Get(() => InvoiceBillingTypeAdditionalGroups, false);
        set => Set(() => InvoiceBillingTypeAdditionalGroups, value);
    }

    public List<string> GetBillingPeriods() => new()
                                               {
                                                   FindStringResource( "AccountsBillingPeriodDaily" ),
                                                   FindStringResource( "AccountsBillingPeriodWeekly" ),
                                                   FindStringResource( "AccountsBillingPeriodBiWeekly" ),
                                                   FindStringResource( "AccountsBillingPeriodMonthly" ),
                                                   FindStringResource( "AccountsBillingPeriodSingle" ),
                                                   //FindStringResource( "AccountsBillingPeriodIndividual" )
                                               };

    public List<string> GetBillingPeriodsWithAdditional() => new()
                                               {
                                                   FindStringResource( "AccountsBillingPeriodDaily" ),
                                                   FindStringResource( "AccountsBillingPeriodWeekly" ),
                                                   FindStringResource( "AccountsBillingPeriodBiWeekly" ),
                                                   FindStringResource( "AccountsBillingPeriodMonthly" ),
                                                   FindStringResource( "AccountsBillingPeriodSingle" ),

                                                   FindStringResource( "AccountsBillingPeriodDaily2" ),
                                                   FindStringResource( "AccountsBillingPeriodDaily3" ),
                                                   FindStringResource( "AccountsBillingPeriodWeekly2" ),
                                                   FindStringResource( "AccountsBillingPeriodWeekly3" ),
                                                   FindStringResource( "AccountsBillingPeriodBiWeekly2" ),
                                                   FindStringResource( "AccountsBillingPeriodBiWeekly3" ),
                                                   FindStringResource( "AccountsBillingPeriodMonthly2" ),
                                                   FindStringResource( "AccountsBillingPeriodMonthly3" ),
                                                   FindStringResource( "AccountsBillingPeriodSingle2" ),
                                                   FindStringResource( "AccountsBillingPeriodSingle3" ),
                                                   //FindStringResource( "AccountsBillingPeriodIndividual" )
                                               };

    public DateTimeOffset BulkFromDate
    {
        get { return Get(() => BulkFromDate, DateTimeOffset.Now); }
        set { Set(() => BulkFromDate, value); }
    }

    public string DisplayBulkFromDate
    {
        get => Get(() => DisplayBulkFromDate, "");
        set => Set(() => DisplayBulkFromDate, value);
    }

    [DependsUpon(nameof(BulkFromDate))]
    public void WhenBulkFromDateChanges()
    {
        DisplayBulkFromDate = $"{BulkFromDate.DateTime.ToLongDateString()} {BulkFromDate.DateTime.ToLongTimeString()}";
    }

    public DateTimeOffset BulkToDate
    {
        get { return Get(() => BulkToDate, DateTimeOffset.Now); }
        set { Set(() => BulkToDate, value); }
    }

    public string DisplayBulkToDate
    {
        get => Get(() => DisplayBulkToDate, "");
        set => Set(() => DisplayBulkToDate, value);
    }

    [DependsUpon(nameof(BulkToDate))]
    public void WhenBulkToDateChanges()
    {
        DisplayBulkToDate = $"{BulkToDate.DateTime.ToLongDateString()} {BulkToDate.DateTime.ToLongTimeString()}";
    }

    public List<string> AccountsToBeBulkInvoiced
    {
        get => Get(() => AccountsToBeBulkInvoiced, new List<string>());
        set => Set(() => AccountsToBeBulkInvoiced, value);
    }

    public string BulkSelectedAccount
    {
        get => Get(() => BulkSelectedAccount, string.Empty);
        set => Set(() => BulkSelectedAccount, value);
    }

    public TripList BulkPostedTrips { get; set; } = new();

    public bool IsBtnBulkCreateEnabled
    {
        get => Get(() => IsBtnBulkCreateEnabled, false);
        set => Set(() => IsBtnBulkCreateEnabled, value);
    }

    public Dictionary<string, List<Trip>> DictBulkAccountsTrips
    {
        get => Get(() => DictBulkAccountsTrips, new Dictionary<string, List<Trip>>());
        set => Set(() => DictBulkAccountsTrips, value);
    }

    public List<Trip> TripsForSelectedBulkAccount
    {
        get => Get(() =>  TripsForSelectedBulkAccount, new List<Trip>());
        set => Set(() => TripsForSelectedBulkAccount, value);
    }

    [DependsUpon(nameof(BulkSelectedAccount))]
    public void WhenBulkSelectedAccountChanges()
    {
        if (BulkSelectedAccount.IsNotNullOrEmpty() && DictBulkAccountsTrips.ContainsKey(BulkSelectedAccount))
        {
            TripsForSelectedBulkAccount = DictBulkAccountsTrips[BulkSelectedAccount];
        }
    }

    #endregion

    #region Bulk Invoicing Actions

    public async Task GetPostedTripsForBulkInvoicing()
    {
        if (!IsInDesignMode && View != null)
        {
            Logging.WriteLogLine("Getting Posted shipments by " + (SearchByDeliveredDate ? " Delivered Date" : " CallTime"));
            Dispatcher.Invoke(() =>
            {
                View.SetWaitCursor();
                AccountsToBeBulkInvoiced.Clear();
                TripsForSelectedBulkAccount.Clear();
            });

            BulkPostedTrips = await Azure.Client.RequestSearchTrips(new SearchTrips
            {
                UniqueUserId = Globals.DataContext.MainDataContext.UserName,
                StartStatus = STATUS.POSTED,
                EndStatus = STATUS.POSTED,
                FromDate = BulkFromDate,
                ToDate = BulkToDate,
                ByDelTime = SearchByDeliveredDate,
                IncludeCharges = true,
            });

            Logging.WriteLogLine($"Found {BulkPostedTrips.Count} posted shipments");

            //GetPostedCompanyNames();
            // Now check for the selected billing period
            if (BulkPostedTrips.Count > 0)
            {
                short selectedPeriod = 0;
                for ( ; selectedPeriod < BulkBillingPeriods.Count; selectedPeriod++ )
                {
                    if (SelectedBillingPeriod == BulkBillingPeriods[selectedPeriod])
                    {
                        Logging.WriteLogLine($"SelectedPeriod: {SelectedBillingPeriod}");
                        break;
                    }
                }
                //var trips = (from T in BulkPostedTrips
                //                 let Id = T.AccountId.Trim().ToLower()
                //                 orderby Id
                //                 group T by Id
                //                      into G
                //                 select G.First()).ToList();

                AccountsToBeBulkInvoiced.Clear();
                DictBulkAccountsTrips.Clear();
                
                List<CompanyAddress> cas = GetCompaniesForBillingPeriod(selectedPeriod);
                foreach ( var trip in BulkPostedTrips )
                {
                    var match = (from C in cas where C.CompanyNumber == trip.AccountId select C).FirstOrDefault();
                    if ( match != null)
                    {
                        if ( !DictBulkAccountsTrips.ContainsKey(match.CompanyNumber))
                        {
                            DictBulkAccountsTrips.Add(match.CompanyNumber, new List<Trip>());
                            AccountsToBeBulkInvoiced.Add(match.CompanyNumber);
                        }
                        DictBulkAccountsTrips[trip.AccountId].Add(trip);
                    }
                }
                AccountsToBeBulkInvoiced = AccountsToBeBulkInvoiced.OrderBy(q => q.ToLower()).ToList();
                Logging.WriteLogLine($"Found {AccountsToBeBulkInvoiced.Count} accounts with {BulkPostedTrips.Count} trips in total");
                if (View != null)
                {
                    Dispatcher.Invoke(() =>
                    {
                        View.LbBulkAccounts.ItemsSource = null;
                        View.LbBulkAccounts.ItemsSource = AccountsToBeBulkInvoiced;
                    });
                }
                // TODO Add summary line #accounts & #trips - also, set button to enabled

                IsBtnBulkCreateEnabled = DictBulkAccountsTrips.Count > 0 ? true : false;
                
            }

            Loaded = true;

            //SetVisibility();
            Dispatcher.Invoke(() =>
            {
                View?.ClearWaitCursor();
            });
        }
    }

    private List<CompanyAddress> GetCompaniesForBillingPeriod(short billingPeriod)
    {
        List<CompanyAddress> matches = (from C in GlobalAddressesModel.Companies where C.BillingPeriod == billingPeriod select C).ToList();
        //AccountsToBeBulkInvoiced = (from C in matches select C.CompanyNumber).ToList();
        return matches;
    }

    #endregion

    public class InvoiceReportSettings : ReportSettings
    {
        public List<Trip> Trips = new();
        public Company InvoicingCompany = new();

        
    }

    public class TestInvoiceList : ObservableCollection<TestInvoice>
    {
        public TestInvoiceList(bool populate = false)
        {
            if (populate)
            {
                CreateSampleData();
            }
        }
        public void CreateSampleData(int number = 10)
        {
            DateTime dt = DateTime.Now.AddDays(-9);
            for (int i = 0; i < number; i++) {
                TestInvoice ti = new TestInvoice();
                ti.Invoice = i.ToString();
                ti.Issued = dt.ToString();
                dt = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
                ti.From = dt.ToString();
                dt = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
                ti.To = dt.ToString();
                ti.Amount = ti.Outstanding = 100;
                ti.Discount = 0;
                ti.Disputed = FindStringResource("InvoicingDisputedMessage");
                Add(ti);
                dt = dt.AddDays(1);
            }
        }
    }

    public class TestInvoice
    {
        public string? Invoice { get; set; }
        public string? Issued { get; set; }
        public string? From { get; set; }
        public string? To { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Outstanding { get; set; }
        public decimal? Discount { get; set; }
        public string? Disputed { get; set; }
    }

}
