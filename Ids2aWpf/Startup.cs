﻿#nullable enable

using System.Threading;
using AzureRemoteService;
using Microsoft.Shell;

namespace Ids2aWpf;

public class Startup
{
	private static Logging? Log;

	[STAThread]
	public static void Main()
	{
		if( SingleInstance<App>.InitializeAsFirstInstance( "IDS2A_CLIENT" ) )
		{
			var Application = new App();
			Application.InitializeComponent();

			if( Application.Dispatcher is not null )
			{
				Application.Dispatcher.UnhandledException += ( _, args ) =>
				                                             {
					                                             args.Handled = false;
				                                             };
			}

			Log = new Logging
			      {
				      OnUnhandledException = exception =>
				                             {
					                             Console.WriteLine( exception.Message ); // Put to Log
					                             Console.WriteLine( exception.StackTrace );
					                             var Ie = exception.InnerException;

					                             if( Ie is not null )
					                             {
						                             if( Ie is AggregateException Ae )
						                             {
							                             foreach( var E in Ae.Flatten().InnerExceptions )
								                             Console.WriteLine( E.Message );
						                             }
						                             else
							                             Console.WriteLine( Ie.Message );
					                             }
					                             #pragma warning disable 4014
					                             Azure.Client.RequestClientCrashLog( new CrashReport
					                                                                 {
						                                                                 CrashData = Log?.Log ?? "",
						                                                                 TimeZone  = (sbyte)DateTimeOffset.Now.Offset.Hours,
						                                                                 Account   = Globals.Login.Account,
						                                                                 User      = Globals.Login.UserName
					                                                                 } );
					                             #pragma warning restore 4014
					                             Thread.Sleep( 5000 );
				                             }
			      };

			try
			{
				Application.Run();
			}
			catch( Exception Exception )
			{
				Log.OnUnhandledException?.Invoke( Exception );
			}
			finally
			{
				try
				{
					Logging.WriteLogLine( "Shutting down\r\n___________________________________\r\n" );

					// Make it sleep for .5 seconds to ensure that the log is closed
					Thread.Sleep( 500 );
					Log.Dispose();
				}
				finally
				{
					// Allow single instance code to perform cleanup operations
					SingleInstance<App>.Cleanup();
				}
			}
		}
	}
}