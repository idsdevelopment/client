﻿namespace Ids2aWpf;

internal static partial class Globals
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class CurrentVersion
	{
		public const uint UPDATE_VERSION = 301;
	}
}