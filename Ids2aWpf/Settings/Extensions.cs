﻿using System.Windows.Data;
using Settings = Ids2aWpf.Properties.Settings;

namespace Ids2aWpf.Setting;

public class SettingBindingExtension : Binding
{
	public SettingBindingExtension()
	{
		Initialize();
	}

	public SettingBindingExtension( string path ) : base( path )
	{
		Initialize();
	}

	private void Initialize()
	{
		Source = Settings.Default;
		Mode   = BindingMode.TwoWay;
	}
}