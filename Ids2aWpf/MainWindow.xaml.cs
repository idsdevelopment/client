﻿#nullable enable

using System.Threading;
using System.Windows;
using AzureRemoteService;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using IdsUpdater;
using ViewModels.MainWindow;
using Settings = Ids2aWpf.Properties.Settings;

namespace Ids2aWpf;

/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
	private const string SLOT      = "/SLOT=",
	                     ALPHA_TRW = "ALPHA_TRW",
	                     ALPHAC    = "ALPHAC",
	                     BETA      = "BETA",
	                     ALPHAR    = "ALPHAR";

	public static MainWindow? Instance { get; private set; }

	private bool IgnoreLanguageChange;
	private bool IsClosing;

	public static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

	public MainWindow()
	{
		Instance = this;

		ViewModels.Globals.FindStringResource    = Globals.Dictionary.AsString;
		ViewModels.Globals.FindColorResource     = Globals.Dictionary.AsColor;
		ViewModels.Globals.FindSolidColorBrush   = Globals.Dictionary.AsSolidColorBrush;
		ViewModels.Globals.SetColorResource      = Globals.Dictionary.SetColor;
		ViewModels.Globals.SetSolidBrushResource = Globals.Dictionary.SetSolidBrush;

		ViewModels.Globals.CurrentVersion.CURRENT_VERSION = Globals.CurrentVersion.UPDATE_VERSION;

		Globals.Language = (MainWindowModel.LANGUAGE)Settings.Default.CurrentLanguage;

		Initialized += ( _, _ ) =>
		               {
			               if( DataContext is MainWindowModel Model )
			               {
				               foreach( var Arg in Environment.GetCommandLineArgs() )
				               {
					               var A = Arg.Trim().ToUpper();

					               if( A.StartsWith( SLOT ) )
					               {
						               var Slot = A.Substring( SLOT.Length );

						               var IsProductionSlot = ( Azure.Slot = Slot switch
						                                                     {
							                                                     ALPHA_TRW => Azure.SLOT.ALPHA_TRW,
							                                                     ALPHAC    => Azure.SLOT.ALPHAC,
							                                                     BETA      => Azure.SLOT.BETA,
							                                                     ALPHAR    => Azure.SLOT.ALPHAR,
							                                                     _         => Azure.SLOT.PRODUCTION
						                                                     } ) == Azure.SLOT.PRODUCTION;

						               Model.IsProductionSlot = IsProductionSlot;
						               Model.Slot             = IsProductionSlot ? "" : Slot;

						               break;
					               }
				               }
			               }
		               };

		InitializeComponent();
	}

	private void DoSignOut()
	{
		if( DataContext is MainWindowModel Model )
		{
			Model.CloseAllPrograms();
			ViewModels.Globals.DataContext.ResetDataContext();

			var Mw   = new MainWindow();
			var Save = Application.Current.MainWindow;
			Application.Current.MainWindow = Mw;
			Mw.Show();

			Save?.Close();
		}
	}

	private void MiDeleteLocalSettings_Click( object sender, RoutedEventArgs e )
	{
		var Caption = FindStringResource( "SettingsDeleteLocalSettingsTitle" );
		var Message = FindStringResource( "SettingsDeleteLocalSettingsMessage" );
		var Choice  = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

		if( ( Choice == MessageBoxResult.Yes ) && DataContext is MainWindowModel Model )
		{
			var Success = Model.Execute_DeleteLocalSettings();

			if( Success )
			{
				Message = FindStringResource( "SettingsDeleteLocalSettingsRestartMessage" );
				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
			}
		}
	}

	private void MiDeleteLocalCachedData_Click( object sender, RoutedEventArgs e )
	{
		var Caption = FindStringResource( "SettingsDeleteLocalCachedDataTitle" );
		var Message = FindStringResource( "SettingsDeleteLocalCachedDataMessage" );
		var Choice  = MessageBox.Show( Message, Caption, MessageBoxButton.YesNo, MessageBoxImage.Question );

		if( ( Choice == MessageBoxResult.Yes ) && DataContext is MainWindowModel Model )
		{
			var Success = Model.Execute_DeleteLocalCachedData();

			if( Success )
			{
				Message = FindStringResource( "SettingsDeleteLocalCachedDataRestartMessage" );
				MessageBox.Show( Message, Caption, MessageBoxButton.OK, MessageBoxImage.Information );
			}
		}
	}

	private void Window_Loaded( object sender, RoutedEventArgs e )
	{
		if( DataContext is MainWindowModel Model )
		{
			IgnoreLanguageChange = true;
			Model.Language       = Globals.Language;
			IgnoreLanguageChange = false;

			Model.TabControl = this.FindChild<PageTabControl>();

			Model.OnLogin = ( companyName, userName ) =>
			                {
				                Globals.Login.Account  = companyName;
				                Globals.Login.UserName = userName;
				                Globals.Login.SignedIn = true;

				                Globals.Terminologies.Terminology = companyName;

				                Model.Account = companyName.Capitalise();

				                Model.Updater = new Updater( ViewModels.Globals.CurrentVersion.CURRENT_VERSION, "CurrentVersion.txt", $"Client/{Model.Slot.Capitalise()}", "IdsClientSetup.exe",
				                                             ( _, _ ) =>
				                                             {
					                                             Dispatcher.InvokeAsync( () =>
					                                                                     {
						                                                                     Model.UpdateAvailable = true;
					                                                                     } );
				                                             }, companyName );
			                };

			Model.OnSignOut = DoSignOut;

			Model.OnExit = () =>
			               {
				               DoSignOut();
				               Application.Current.Shutdown();
			               };

			Model.OnLanguageChange = language =>
			                         {
				                         if( !IgnoreLanguageChange )
				                         {
					                         if( Globals.Login.SignedIn )
					                         {
						                         if( !Globals.Dialogs.Confirm( "Restart" ) )
							                         return;
					                         }

					                         Globals.Language                 = language;
					                         Settings.Default.CurrentLanguage = (byte)language;
					                         Settings.Default.Save();

					                         DoSignOut();
				                         }
			                         };
		}

		Globals.Azure.Ping.StartPing();

		var LastPing = true;

		Globals.Azure.Ping.OnPing += online =>
		                             {
			                             if( LastPing != online )
			                             {
				                             // ReSharper disable once PossibleNullReferenceException
				                             Dispatcher.Invoke( () =>
				                                                {
					                                                LastPing         = online;
					                                                Window.IsEnabled = online;
				                                                } );
			                             }
		                             };

		ViewModels.Globals.Login._SignOut = DoSignOut;
	}

	private async void Window_Closing( object sender, CancelEventArgs e )
	{
		if( DataContext is MainWindowModel Model )
		{
			var ModelLanguage = Model.Language;
			Globals.Language                 = ModelLanguage;
			Settings.Default.CurrentLanguage = (byte)ModelLanguage;
		}

		Settings.Default.Save();

		if( Globals.Login.SignedIn )
		{
			Globals.Login.SignedIn = false;
			IsClosing              = true;

			await Azure.Client.RequestSignOut( new SignOut
			                                   {
				                                   Time = Globals.Login.AmountOfTimeLoggedIn
			                                   } );
			IsClosing = false;
		}
	}

	private void Window_Closed( object sender, EventArgs e )
	{
		for( var I = 50; IsClosing && ( --I > 0 ); ) // Give up to 5 secs to close
			Thread.Sleep( 100 );
	}
}