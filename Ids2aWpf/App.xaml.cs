﻿using System.Windows;
using Microsoft.Shell;

namespace Ids2aWpf;

/// <summary>
///     Interaction logic for App.xaml
/// </summary>
public partial class App : Application, ISingleInstanceApp
{
	public bool SignalExternalCommandLineArgs( IList<string> args ) => true;
}