﻿using System.Timers;
using System.Windows.Controls;
using IdsControlLibrary.Utils;

namespace IdsControlLibrary
{
	/// <summary>
	///     Interaction logic for HamburgerWindow.xaml
	/// </summary>
	internal partial class HamburgerWindow
	{
		public new Visibility Visibility
		{
			get => base.Visibility;
			set
			{
				if( !InVisibilityChange )
				{
					InVisibilityChange = true;

					try
					{
						var Vis = value == Visibility.Visible;

						if( Vis )
						{
							if( !AutoClosed )
								SetSize();
							else
								return;
						}

						if( Vis )
						{
							Activate();
							this.SlideFromLeft().FadeIn( 0.5, 0.3 );
							base.Visibility = Visibility.Visible;
						}
						else
						{
							this.FadeOut( 0.2, 0.1, () =>
							                        {
								                        base.Visibility = Visibility.Hidden;
							                        } );
						}
					}
					finally
					{
						var T = new Timer( 600 );

						T.Elapsed += ( _, _ ) =>
						             {
							             InVisibilityChange = false;
							             T.Dispose();
						             };
						T.Start();
					}
				}
			}
		}

		private readonly Window              MainWindow;
		private readonly IdsContentPresenter Presenter;

		private bool InVisibilityChange;

		internal bool AutoClosed;

		internal void DoHamburgerClick()
		{
			Visibility = Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
		}

		public HamburgerWindow( Window mainWindow, IdsContentPresenter presenter )
		{
			MainWindow = mainWindow;
			Presenter  = presenter;

			InitializeComponent();

			if( mainWindow != null )
			{
				mainWindow.LayoutUpdated += ( _, _ ) =>
				                            {
					                            SetSize();
				                            };

				mainWindow.LocationChanged += ( _, _ ) =>
				                              {
					                              SetSize();
				                              };
				base.Visibility = Visibility.Hidden;
			}
			else
				SetSize();
		}

		private void SetSize()
		{
			if( MainWindow != null )
			{
				Top    = MainWindow.Top + 100;
				Left   = MainWindow.Left + 3;
				Height = MainWindow.ActualHeight - 129;
			}
		}

		private void Window_Deactivated( object sender, EventArgs e )
		{
			if( !InVisibilityChange )
			{
				this.FadeOut( 0.2, 0.1, () =>
				                        {
					                        base.Visibility = Visibility.Hidden;
					                        AutoClosed      = true;
					                        var T = new Timer( 300 );

					                        T.Elapsed += ( _, _ ) =>
					                                     {
						                                     AutoClosed = false;
						                                     T.Dispose();
					                                     };
					                        T.Start();
				                        } );
			}
		}

		private void ListBox_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			e.Handled = true;
			var Ndx = ListBox.SelectedIndex;

			if( Ndx >= 0 )
			{
				if( ListBox.Items[ Ndx ] is HamburgerItem Item )
				{
					var Prog = Item.OnClick( Presenter );

					if( Prog != null )
						Presenter.LoadProgram( Prog );
				}
				ListBox.SelectedIndex = -1;
				Window_Deactivated( this, null );
			}
		}
	}
}