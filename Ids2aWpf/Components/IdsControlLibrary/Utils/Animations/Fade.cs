﻿using System.Windows.Media.Animation;

namespace IdsControlLibrary.Utils
{
	public static partial class Animations
	{
		private const double DEFAULT_FADE_DURATION = 0.5,
		                     DEFAULT_POWER         = 10.0;

		public static UIElement FadeIn( this UIElement element, double duration, double power, Action completeCallback = null ) => ShowHFadeInOutide( element, true, duration, power, completeCallback );

		public static FrameworkElement FadeIn( this FrameworkElement element, double duration, double power, Action completeCallback = null )
		{
			ShowHFadeInOutide( element, true, duration, power, completeCallback );
			return element;
		}

		public static UIElement FadeIn( this UIElement element, double duration, Action completeCallback = null ) => FadeIn( element, duration, DEFAULT_POWER, completeCallback );

		public static FrameworkElement FadeIn( this FrameworkElement element, double duration, Action completeCallback = null ) => FadeIn( element, duration, DEFAULT_POWER, completeCallback );

		public static UIElement FadeIn( this UIElement element, Action completeCallback ) => FadeIn( element, DEFAULT_FADE_DURATION, completeCallback );

		public static FrameworkElement FadeIn( this FrameworkElement element, Action completeCallback ) => FadeIn( element, DEFAULT_FADE_DURATION, completeCallback );

		public static UIElement FadeIn( this UIElement element ) => FadeIn( element, null );

		public static FrameworkElement FadeIn( this FrameworkElement element ) => FadeIn( element, null );

		public static UIElement FadeOut( this UIElement element, double duration, double power, Action completeCallback = null ) => ShowHFadeInOutide( element, false, duration, power, completeCallback );

		public static FrameworkElement FadeOut( this FrameworkElement element, double duration, double power, Action completeCallback = null )
		{
			ShowHFadeInOutide( element, false, duration, power, completeCallback );
			return element;
		}

		public static UIElement FadeOut( this UIElement element, double duration, Action completeCallback ) => FadeOut( element, duration, DEFAULT_POWER, completeCallback );

		public static FrameworkElement FadeOut( this FrameworkElement element, double duration, Action completeCallback ) => FadeOut( element, duration, DEFAULT_POWER, completeCallback );

		public static UIElement FadeOut( this UIElement element, Action completeCallback ) => FadeOut( element, DEFAULT_FADE_DURATION, completeCallback );

		public static FrameworkElement FadeOut( this FrameworkElement element, Action completeCallback ) => FadeOut( element, DEFAULT_FADE_DURATION, completeCallback );

		public static UIElement FadeOut( this UIElement element ) => FadeOut( element, null );

		public static FrameworkElement FadeOut( this FrameworkElement element ) => FadeOut( element, null );

		private static UIElement ShowHFadeInOutide( UIElement element, bool show, double duration, double power, Action completeCallback )
		{
			Application.Current.Dispatcher.InvokeAsync( () =>
			                                            {
				                                            var Duration   = TimeSpan.FromSeconds( duration );
				                                            var Storyboard = new Storyboard();

				                                            element.Opacity = show ? 0 : 100;

				                                            var Opacity = show
					                                                          ? new DoubleAnimation {From = 0, To = 1, Duration = Duration, EasingFunction = new PowerEase {EasingMode = EasingMode.EaseIn, Power  = power}}
					                                                          : new DoubleAnimation {From = 1, To = 0, Duration = Duration, EasingFunction = new PowerEase {EasingMode = EasingMode.EaseOut, Power = power}};

				                                            Storyboard.SetTargetProperty( Opacity, new PropertyPath( "Opacity" ) );
				                                            Storyboard.SetTarget( Opacity, element );
				                                            Storyboard.Children.Add( Opacity );

				                                            if( completeCallback != null )
				                                            {
					                                            Storyboard.Completed += ( _, _ ) =>
					                                                                    {
						                                                                    completeCallback();
					                                                                    };
				                                            }

				                                            Storyboard.Begin();
			                                            } );
			return element;
		}
	}
}