﻿using System.Windows.Controls;

namespace IdsControlLibrary.Utils
{
	public static class Controls
	{
		public static TextBox TextBox( this ComboBox combo ) => (TextBox)combo.Template.FindName( "PART_EditableTextBox", combo );

		public static SolidColorBrush ComboBoxReadonlyColor( bool readOnly ) => readOnly ? Colours.GetReadonlyBrush() : SystemColors.WindowBrush;

		public static ComboBox SetReadOnlyColour( this ComboBox combo, bool isReadOnly )
		{
			if( combo.TextBox().Parent is Border B )
				B.Background = ComboBoxReadonlyColor( isReadOnly );

			return combo;
		}

		public static void ComboxSearch( ComboBox lookupCombo, ref TextBox textBox, Func<int, ( string FullText, string[] SearchText )> getItemSearchText, Action<(bool HasPartialMatch, bool HasCompleteMatch)> onNoMatches )
		{
			if( textBox == null )
				textBox = lookupCombo.TextBox();

			var SavePosition = textBox.CaretIndex;

			var Text = lookupCombo.Text.Trim();

			lookupCombo.IsDropDownOpen = true;

			var Items = lookupCombo.Items;
			var Count = Items.Count;

			var HaveVisible      = false;
			var HasCompleteMatch = false;

			for( var I = 0; I < Count; I++ )
			{
				var Item = (ComboBoxItem)lookupCombo.ItemContainerGenerator.ContainerFromIndex( I );

				if( Item != null )
				{
					var SearchItem = getItemSearchText( I );

					HasCompleteMatch |= SearchItem.FullText.Trim() == Text;

					var Vis = Text.ToUpper().SubSet( SearchItem.SearchText );
					HaveVisible     |= Vis;
					Item.Visibility =  Vis ? Visibility.Visible : Visibility.Collapsed;
				}
			}
			var LenOk = Text.Length > 0;

			onNoMatches( ( HasPartialMatch: HaveVisible && LenOk, HasCompleteMatch: HasCompleteMatch && LenOk ) );

			textBox.SelectionLength = 0;
			textBox.CaretIndex      = SavePosition;
		}
	}
}