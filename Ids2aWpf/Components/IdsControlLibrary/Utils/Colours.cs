﻿namespace IdsControlLibrary.Utils
{
	public static class Colours
	{
		private static SolidColorBrush _ReadOnly;

		public static ( Brush Even, Brush Odd) GetComboBoxBrushes()
		{
			Brush Odd = null, Even = null;

			var Current = Application.Current;

			Current.Dispatcher.Invoke( () =>
			                           {
				                           Odd  = (Brush)Current.Resources[ "ComboBoxOddRowColourBrush" ];
				                           Even = (Brush)Current.Resources[ "ComboBoxEvenRowColourBrush" ];
			                           } );

			return ( Even, Odd );
		}

		public static SolidColorBrush GetReadonlyBrush()
		{
			if( _ReadOnly == null )
			{
				var Current = Application.Current;

				Current.Dispatcher.Invoke( () =>
				                           {
					                           _ReadOnly = (SolidColorBrush)Current.Resources[ "ReadOnlyBrush" ];
				                           } );
			}
			return _ReadOnly;
		}
	}
}