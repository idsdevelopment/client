﻿namespace IdsControlLibrary.Utils
{
	public static class TextSets
	{
		public static bool SubSet( this string txt, string[] superSet )
		{
			var SubSet = txt.Split( new[] {' '}, StringSplitOptions.RemoveEmptyEntries );

			return ( SubSet.Length == 0 ) || SubSet.Any( sub => superSet.Any( super => super.Contains( sub ) ) );
		}

		public static bool SubSet( this string txt, string superSet )
		{
			return txt.SubSet( superSet.Split( new[] {' '}, StringSplitOptions.RemoveEmptyEntries ) );
		}
	}
}