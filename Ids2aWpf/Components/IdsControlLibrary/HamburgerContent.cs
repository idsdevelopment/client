﻿using System.Windows.Controls;

namespace IdsControlLibrary
{
	/// <summary>
	///     Interaction logic for HamburgerContent.xaml
	/// </summary>
	public class HamburgerContent : Page
	{
		public HamburgerContent()
		{
			Items = new List<HamburgerItem>();
		}

		public List<HamburgerItem> Items
		{
			get => (List<HamburgerItem>)GetValue( ItemsProperty );
			set => SetValue( ItemsProperty, value );
		}

		// Using a DependencyProperty as the backing store for Items.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ItemsProperty =
			DependencyProperty.Register( "Items", typeof( List<HamburgerItem> ), typeof( HamburgerContent ), new PropertyMetadata( new List<HamburgerItem>() ) );
	}
}