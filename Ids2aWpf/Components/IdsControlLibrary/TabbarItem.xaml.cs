﻿using System.Windows.Controls;

namespace IdsControlLibrary
{
	/// <summary>
	///     Interaction logic for TabbarItem.xaml
	/// </summary>
	public partial class TabbarItem
	{
		public IdsContentPresenter Presenter { get; internal set; }

		public void Show( Action onShown = null )
		{
			Presenter.Show( this, onShown );
		}

		public TabbarItem()
		{
			InitializeComponent();
		}

		private void UserControl_Loaded( object sender, RoutedEventArgs e )
		{
			ToolBarContent?.UpdateLayout();
		}

		public string Header
		{
			get => (string)GetValue( HeaderProperty );
			set => SetValue( HeaderProperty, value );
		}

		// Using a DependencyProperty as the backing store for Header.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty HeaderProperty =
			DependencyProperty.Register( "Header", typeof( string ), typeof( TabbarItem ), new PropertyMetadata( "" ) );

		public Page Page
		{
			get => (Page)GetValue( PageProperty );
			set => SetValue( PageProperty, value );
		}

		// Using a DependencyProperty as the backing store for Page.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PageProperty =
			DependencyProperty.Register( "Page", typeof( Page ), typeof( TabbarItem ), new PropertyMetadata() );

		public ContentControl ToolBarContent
		{
			get => (ContentControl)GetValue( ToolBarContentProperty );
			set => SetValue( ToolBarContentProperty, value );
		}

		// Using a DependencyProperty as the backing store for ToolBarContent.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ToolBarContentProperty =
			DependencyProperty.Register( "ToolBarContent", typeof( ContentControl ), typeof( TabbarItem ) );
	}
}