﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using IdsControlLibrary.Utils;
using Microsoft.Reporting.WinForms;
using Brushes = System.Drawing.Brushes;

namespace IdsControlLibrary.ReportViewer
{
	// Based on The Code Project: https://www.codeproject.com/Questions/86383/Print-rdlc-report-without-preview
	// Also this Doc: https://msdn.microsoft.com/en-us/library/ms252172(v=vs.100).aspx

	public static class PrintRdlcReport
	{
		internal class DirectPrint : Disposable
		{
			private readonly PageSettings  PageSettings;
			private readonly LocalReport   Report;
			private readonly string        PrinterName;
			private          int           CurrentPageIndex;
			private          IList<Stream> PrintStreams;
			private          bool          Printed;
			private          PaperSize     PaperSize;
			private          Margins       Margins;

			internal bool Execute()
			{
				PaperSize = PageSettings.PaperSize;
				Margins   = PageSettings.Margins;

				// The device info string defines the page range to print as well as the size of the page.
				// A start and end page of 0 means generate all pages.
				var DeviceInfo = "<DeviceInfo>" +
				                 "<OutputFormat>EMF</OutputFormat>" +
				                 $"<PageWidth>{ToInches( PaperSize.Width )}</PageWidth>" +
				                 $"<PageHeight>{ToInches( PaperSize.Height )}</PageHeight>" +
				                 $"<MarginTop>{ToInches( Margins.Top )}</MarginTop>" +
				                 $"<MarginLeft>{ToInches( Margins.Left )}</MarginLeft>" +
				                 $"<MarginRight>{ToInches( Margins.Right )}</MarginRight>" +
				                 $"<MarginBottom>{ToInches( Margins.Bottom )}</MarginBottom>" +
				                 "</DeviceInfo>";

				PrintStreams = new List<Stream>();

				Report.Render( "Image", DeviceInfo, ( _, _, _, _, _ ) =>
				                                    {
					                                    Stream Stream = new MemoryStream();
					                                    PrintStreams.Add( Stream );
					                                    return Stream;
				                                    }, out _ );

				foreach( var Stream in PrintStreams )
					Stream.Position = 0;

				return Print();
			}

			protected override void OnDispose( bool systemDisposing )
			{
				if( PrintStreams != null )
				{
					foreach( var Stream in PrintStreams )
						Stream.Close();
					PrintStreams = null;
				}
			}

			internal DirectPrint( LocalReport report, string printerName )
			{
				Report      = report;
				PrinterName = printerName;

				PageSettings = new PageSettings();
				var ReportPageSettings = report.GetDefaultPageSettings();

				PageSettings.PaperSize = ReportPageSettings.PaperSize;
				PageSettings.Margins   = ReportPageSettings.Margins;
			}

			private static string ToInches( int hundrethsOfInch )
			{
				var Inches = hundrethsOfInch / 100.0;
				return $"{Inches}in";
			}

			private bool Print()
			{
				Printed = false;

				if( ( PrintStreams == null ) || ( PrintStreams.Count == 0 ) )
					throw new Exception( "Error: no stream to print." );

				var PrintDoc = new PrintDocument {PrinterSettings = {PrinterName = PrinterName}};

				if( !PrintDoc.PrinterSettings.IsValid )
					throw new Exception( $"Error: cannot find printer: {PrinterName}" );

				CurrentPageIndex = 0;

				PrintDoc.PrintPage += ( _, pageEventArgs ) =>
				                      {
					                      Printed = true;

					                      var PageToPrint = PrintStreams[ CurrentPageIndex ];
					                      PageToPrint.Position = 0;

					                      // Load each page into a Metafile to draw it.
										  using var PageMetaFile = new Metafile( PageToPrint );

										  var AdjustedRect = new Rectangle(
																		   pageEventArgs.PageBounds.Left - (int)pageEventArgs.PageSettings.HardMarginX,
																		   pageEventArgs.PageBounds.Top - (int)pageEventArgs.PageSettings.HardMarginY,
																		   pageEventArgs.PageBounds.Width,
																		   pageEventArgs.PageBounds.Height );

										  // Draw a white background for the report
										  pageEventArgs.Graphics.FillRectangle( Brushes.White, AdjustedRect );

										  // Draw the report content
										  pageEventArgs.Graphics.DrawImage( PageMetaFile, AdjustedRect );

										  // Prepare for next page.  Make sure we haven't hit the end.
										  CurrentPageIndex++;
										  pageEventArgs.HasMorePages = CurrentPageIndex < PrintStreams.Count;
									  };
				PrintDoc.Print();
				return Printed;
			}
		}

		public static bool PrintToPrinter( this LocalReport report, string printerName )
		{
			try
			{
				using var Dp = new DirectPrint( report, printerName );

				return Dp.Execute();
			}
			catch( Exception E )
			{
				Console.WriteLine( E );
			}
			return false;
		}
	}
}