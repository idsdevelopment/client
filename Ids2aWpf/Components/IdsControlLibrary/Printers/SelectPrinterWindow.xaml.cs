﻿using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibrary.Utils;

namespace IdsControlLibrary.SelectPrinterWindow
{
	public class SelectedPrinter : PrinterInfo, INotifyPropertyChanged
	{
		public bool Selected
		{
			get => _Selected;
			set
			{
				_Selected = value;
				OnPropertyChanged( nameof( Selected ) );
			}
		}

		public  SelectedPrinter Self => this;
		private bool            _Selected;

		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		internal SelectedPrinter()
		{
		}

		internal SelectedPrinter( PrinterInfo p, string currentPrinter ) : base( p )
		{
			Selected = p.Name == currentPrinter;
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	internal class PrinterList : List<SelectedPrinter>
	{
	}

	/// <summary>
	///     Interaction logic for SelectPrinterWindow.xaml
	/// </summary>
	public partial class SelectPrinterWindow
	{
		public                  ObservableCollection<SelectedPrinter> AvailablePrinters { get; private set; }
		private static readonly SelectedPrinter                       NoPrinter = new SelectedPrinter {Name = SelectPrinter.NO_PRINTER};

		private bool OkEnabled
		{
			set => OkButton.IsEnabled = value;
		}

		private SelectedPrinter CurrentPrinter = NoPrinter;

		private readonly SelectPrinter MyOwner;
		private          string        CurrentPrinterName;

		public (PrinterInfo PrinterInfo, bool Ok ) ShowDialog( string currentPrinterName )
		{
			CurrentPrinterName = currentPrinterName.Trim();

			AvailablePrinters = new ObservableCollection<SelectedPrinter>( from P in MyOwner.AvailablePrinters
			                                                               select new SelectedPrinter( P, CurrentPrinterName ) );

			foreach( var Printer in AvailablePrinters )
			{
				if( Printer.Name == currentPrinterName )
				{
					Printer.Selected = true;
					CurrentPrinter   = Printer;
					break;
				}
			}

			PrinterListBox.ItemsSource = AvailablePrinters;

			var Okn = ShowDialog();
			var Ok  = ( Okn != null ) && (bool)Okn;

			if( Ok )
				CurrentPrinterName = CurrentPrinter.Name;

			return ( PrinterInfo: Ok ? CurrentPrinter : NoPrinter, Ok );
		}

		public SelectPrinterWindow( SelectPrinter myOwner )
		{
			MyOwner = myOwner;
			InitializeComponent();
		}

		private void RadioButton_Click( object sender, RoutedEventArgs e )
		{
			if( sender is RadioButton RButton )
			{
				if( e != null )
					e.Handled = true;

				var RChecked = RButton.IsChecked;
				var Checked  = ( RChecked != null ) && (bool)RChecked;

				var Self = (SelectedPrinter)RButton.Tag;
				PrinterListBox.SelectedItem = Self;

				if( Checked )
				{
					CurrentPrinter = Self;

					foreach( var Printer in AvailablePrinters )
					{
						if( Printer != Self )
							Printer.Selected = false;
					}
					OkEnabled = true;
				}
				else
					OkEnabled = false;
			}
		}

		private void DockPanel_MouseDown( object sender, MouseButtonEventArgs e )
		{
			if( sender is DockPanel Panel )
			{
				e.Handled = true;
				var Rb = Panel.FindChild<RadioButton>();

				if( Rb != null )
				{
					Rb.IsChecked = !Rb.IsChecked;
					RadioButton_Click( Rb, null );
				}
			}
		}

		private void CloseButton_Click( object sender, RoutedEventArgs e )
		{
			DialogResult = false;
			Close();
		}

		private void OkButton_Click( object sender, RoutedEventArgs e )
		{
			DialogResult = true;
			Close();
		}

		private void CancelButton_Click( object sender, RoutedEventArgs e )
		{
			DialogResult = false;
			Close();
		}

		private void DockPanel_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
		{
			DragMove();
		}

		public new string Title
		{
			get => (string)GetValue( TitleProperty );
			set => SetValue( TitleProperty, value );
		}

		// Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
		public new static readonly DependencyProperty TitleProperty =
			DependencyProperty.Register( "Title", typeof( string ), typeof( SelectPrinterWindow ), new PropertyMetadata( "Select Printer" ) );

		public Brush TitleBackground
		{
			get => (Brush)GetValue( TitleBackgroundProperty );
			set => SetValue( TitleBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TitleBackgroundProperty =
			DependencyProperty.Register( "TitleBackground", typeof( Brush ), typeof( SelectPrinterWindow ), new PropertyMetadata( Brushes.Transparent ) );

		public Brush TitleForeground
		{
			get => (Brush)GetValue( TitleForegroundProperty );
			set => SetValue( TitleForegroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TitleForegroundProperty =
			DependencyProperty.Register( "TitleForeground", typeof( Brush ), typeof( SelectPrinterWindow ), new PropertyMetadata( Brushes.Black ) );
	}
}