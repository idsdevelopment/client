﻿#nullable enable

namespace IdsControlLibrary;

public class PrinterInfo
{
	public string Name      { get; internal set; } = "";
	public string UncPath   { get; internal set; } = "";
	public bool   Available { get; internal set; }
	public bool   OnLine    { get; internal set; }
	public bool   InError   { get; internal set; }
	public bool   Ok        => Available && OnLine && !InError;

	public PrinterInfo()
	{
	}

	public PrinterInfo( PrinterInfo p )
	{
		Name      = p.Name;
		UncPath   = p.UncPath;
		Available = p.Available;
		OnLine    = p.OnLine;
		InError   = p.InError;
	}
}

/// <summary>
///     Interaction logic for SelectPrinter.xaml
/// </summary>
public partial class SelectPrinter : INotifyPropertyChanged
{
	internal const string NO_PRINTER              = "No printer selected",
						  SETTINGS_FILE           = "ApplicationPrinters.json",
						  PRINTER_SETTINGS_FOLDER = "PrinterSettings",
						  DEFAULT_CONTEXT         = "Application";

	public static readonly DependencyProperty SettingsFolderProperty =
		DependencyProperty.Register( nameof( SettingsFolder ), typeof( string ), typeof( SelectPrinter ), new FrameworkPropertyMetadata( PRINTER_SETTINGS_FOLDER, FrameworkPropertyMetadataOptions.None, OnSettingsFolderChanged ) );

	public static readonly DependencyProperty ApplicationContextProperty =
		DependencyProperty.Register( nameof( ApplicationContext ), typeof( string ), typeof( SelectPrinter ), new FrameworkPropertyMetadata( DEFAULT_CONTEXT, FrameworkPropertyMetadataOptions.None, OnApplicationContextChanged ) );

	// Using a DependencyProperty as the backing store for CancelCaption.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty CancelCaptionProperty =
		DependencyProperty.Register( nameof( CancelCaption ), typeof( string ), typeof( SelectPrinter ), new PropertyMetadata( "Cancel" ) );

	// Using a DependencyProperty as the backing store for OkCaption.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty OkCaptionProperty =
		DependencyProperty.Register( nameof( OkCaption ), typeof( string ), typeof( SelectPrinter ), new PropertyMetadata( "Ok" ) );

	// Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleProperty =
		DependencyProperty.Register( nameof( Title ), typeof( string ), typeof( SelectPrinter ), new PropertyMetadata( "" ) );

	// Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleBackgroundProperty =
		DependencyProperty.Register( nameof( TitleBackground ),
								     typeof( Brush ),
								     typeof( SelectPrinter ),
								     new FrameworkPropertyMetadata( Brushes.Transparent,
															        FrameworkPropertyMetadataOptions.AffectsRender,
															        ( o, args ) =>
															        {
																        ( (SelectPrinter)o ).TitleBackground = (Brush)args.NewValue;
															        } ) );

	// Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleForegroundProperty =
		DependencyProperty.Register( nameof( TitleForeground ),
								     typeof( Brush ),
								     typeof( SelectPrinter ),
								     new FrameworkPropertyMetadata( Brushes.Black,
															        FrameworkPropertyMetadataOptions.AffectsRender,
															        ( o, args ) =>
															        {
																        ( (SelectPrinter)o ).TitleForeground = (Brush)args.NewValue;
															        } ) );

	public string CurrentPrinterName { get; private set; } = NO_PRINTER;

	public List<PrinterInfo> AvailablePrinters
	{
		get
		{
			string UncFromDesc( string desc )
			{
				var P = desc.IndexOf( ',' );

				if( P >= 0 )
					desc = desc.Substring( 0, P );

				return desc;
			}

			return ( from PrintQueue Printer in new LocalPrintServer().GetPrintQueues( new[]
																				       {
																					       EnumeratedPrintQueueTypes.Local,
																					       EnumeratedPrintQueueTypes.Connections
																				       } ).ToList()
				     select new PrinterInfo
					        {
						        Name      = Printer.Name.Trim(),
						        UncPath   = UncFromDesc( Printer.Description ),
						        InError   = Printer.IsInError,
						        OnLine    = !Printer.IsOffline,
						        Available = !Printer.IsNotAvailable
					        } ).ToList();
		}
	}

	public string ApplicationContext
	{
		get => (string)GetValue( ApplicationContextProperty );
		set
		{
			value = value.Trim().ToUpper();
			SetValue( ApplicationContextProperty, value );
			HaveContext = true;
			LoadDictionary();
		}
	}

	public string CancelCaption
	{
		get => (string)GetValue( CancelCaptionProperty );
		set
		{
			SetValue( CancelCaptionProperty, value );
			PrinterWindow.CancelButton.Content = value;
		}
	}

	public string OkCaption
	{
		get => (string)GetValue( OkCaptionProperty );
		set
		{
			SetValue( OkCaptionProperty, value );
			PrinterWindow.OkButton.Content = value;
		}
	}

	public string SettingsFolder
	{
		get => (string)GetValue( SettingsFolderProperty );
		set
		{
			SetValue( SettingsFolderProperty, value );
			HaveFolder = true;
			LoadDictionary();
		}
	}

	// Using a DependencyProperty as the backing store for ApplicationContext.  This enables animation, styling, binding, etc...

	// Using a DependencyProperty as the backing store for SettingsFolder.  This enables animation, styling, binding, etc...

	public string Title
	{
		get => (string)GetValue( TitleProperty );
		set
		{
			SetValue( TitleProperty, value );
			PrinterWindow.Title = value;
		}
	}

	public Brush TitleBackground
	{
		get => (Brush)GetValue( TitleBackgroundProperty );
		set
		{
			SetValue( TitleBackgroundProperty, value );
			PrinterWindow.TitleBackground = value;
		}
	}

	public Brush TitleForeground
	{
		get => (Brush)GetValue( TitleForegroundProperty );
		set
		{
			SetValue( TitleForegroundProperty, value );
			PrinterWindow.TitleForeground = value;
		}
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public SelectPrinter()
	{
		InitializeComponent();

		if( !DesignerProperties.GetIsInDesignMode( this ) )
			Visibility = Visibility.Collapsed;
	}

	private Dictionary<string, string>? ApplicationCtxs;

	private bool HaveFolder, HaveContext;

	private SelectPrinterWindow.SelectPrinterWindow PrinterWindow = null!;

	private static string GetFileName( string settingsFolder )
	{
		var Pth = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), settingsFolder.Trim() );

		if( !Directory.Exists( Pth ) )
			Directory.CreateDirectory( Pth );
		Pth = Path.Combine( Pth, SETTINGS_FILE );
		return Pth;
	}

	private void LoadDictionary()
	{
		if( HaveFolder && HaveContext && ApplicationCtxs is null )
		{
			try
			{
				using var Stream = new FileStream( GetFileName( SettingsFolder ), FileMode.Open, FileAccess.Read, FileShare.Read );

				using var Reader = new StreamReader( Stream, Encoding.UTF8 );

				var Ctxs = Reader.ReadToEnd();

				ApplicationCtxs = ( string.IsNullOrEmpty( Ctxs ) ? new Dictionary<string, string>()
									    : JsonConvert.DeserializeObject<Dictionary<string, string>>( Ctxs ) ) ?? new Dictionary<string, string>();

				if( ApplicationCtxs.TryGetValue( ApplicationContext, out var Printer ) )
					CurrentPrinterName = Printer;
			}
			catch( Exception E )
			{
				Console.WriteLine( E );
				ApplicationCtxs = new Dictionary<string, string>();
			}
		}
	}

	private static void OnSettingsFolderChanged( DependencyObject obj, DependencyPropertyChangedEventArgs e )
	{
		( (SelectPrinter)obj ).SettingsFolder = (string)e.NewValue;
	}

	private static void OnApplicationContextChanged( DependencyObject obj, DependencyPropertyChangedEventArgs e )
	{
		( (SelectPrinter)obj ).ApplicationContext = (string)e.NewValue;
	}

	private void UserControl_Initialized( object sender, EventArgs e )
	{
		PrinterWindow = new SelectPrinterWindow.SelectPrinterWindow( this );
	}

	public (PrinterInfo PrinterInfo, bool Ok) Execute()
	{
		try
		{
			var Result = PrinterWindow.ShowDialog( CurrentPrinterName );

			if( Result.Ok )
			{
				CurrentPrinterName = Result.PrinterInfo.Name;

				var ACtx = ApplicationContext;

				if( string.IsNullOrEmpty( ACtx ) )
				{
					Console.WriteLine( "No ApplicationContext" );
					Result.Ok = false;
				}
				else
				{
					ApplicationCtxs ??= new Dictionary<string, string>();

					if( ApplicationCtxs.ContainsKey( ApplicationContext ) )
						ApplicationCtxs[ ApplicationContext ] = CurrentPrinterName;
					else
						ApplicationCtxs.Add( ApplicationContext, CurrentPrinterName );

					var Ctxs = JsonConvert.SerializeObject( ApplicationCtxs );

					using var Stream = new FileStream( GetFileName( SettingsFolder ), FileMode.Create, FileAccess.Write, FileShare.None );

					using var Writer = new StreamWriter( Stream, Encoding.UTF8 );

					Writer.Write( Ctxs );
				}
			}
			return Result;
		}
		finally
		{
			PrinterWindow = new SelectPrinterWindow.SelectPrinterWindow( this )
							{
								Title           = Title,
								TitleBackground = TitleBackground,
								TitleForeground = TitleForeground,
								OkButton        = {Content = OkCaption},
								CancelButton    = {Content = CancelCaption}
							};
		}
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}