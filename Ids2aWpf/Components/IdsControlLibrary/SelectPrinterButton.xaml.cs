﻿#nullable enable

using System.Windows.Input;

namespace IdsControlLibrary;

/// <summary>
///     Interaction logic for SelectPrinterButton.xaml
/// </summary>
public partial class SelectPrinterButton
{
	public string CurrentPrinterName => SelectPrinter.CurrentPrinterName;

	public string OkCaption
	{
		get => SelectPrinter.OkCaption;
		set => SelectPrinter.OkCaption = value;
	}

	public string CancelCaption
	{
		get => SelectPrinter.CancelCaption;
		set => SelectPrinter.CancelCaption = value;
	}

#region Title
	public string Title
	{
		get => SelectPrinter.Title;
		set => SelectPrinter.Title = value;
	}
#endregion

	public event SelectionChangedEvent? SelectionChanged;

	~SelectPrinterButton()
	{
		SelectionChanged -= OnChangedSelection;
	}

	public SelectPrinterButton()
	{
		InitializeComponent();

		SelectionChanged += OnChangedSelection;
	}

	private static void OnChangedSelection( object sender, PrinterInfo printer, string name )
	{
		if( printer.Ok && sender is SelectPrinterButton {OnSelectionChanged: { } OnChanged} S )
		{
			if( OnChanged.CanExecute( S ) )
				OnChanged.Execute( S );
		}
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		var RetVal = SelectPrinter.Execute();

		if( RetVal.Ok )
			SelectionChanged?.Invoke( this, RetVal.PrinterInfo, RetVal.PrinterInfo.Name );
	}

	public delegate void SelectionChangedEvent( object sender, PrinterInfo newPrinter, string name );

#region OnSelectionChanged
	[Category( "Options" )]
	public ICommand? OnSelectionChanged
	{
		get => (ICommand)GetValue( OnSelectionChangedProperty );
		set => SetValue( OnSelectionChangedProperty, value );
	}

	// Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty OnSelectionChangedProperty =
		DependencyProperty.Register( nameof( OnSelectionChanged ), 
									 typeof( ICommand ), 
									 typeof( SelectPrinterButton ), 
									 new FrameworkPropertyMetadata( null ) );
#endregion

#region Title Foreground
	[Category( "Options" )]
	public Brush TitleForeground
	{
		get => (Brush)GetValue( TitleForegroundProperty );
		set
		{
			SetValue( TitleForegroundProperty, value );
			SelectPrinter.TitleForeground = value;
		}
	}

	// Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleForegroundProperty =
		DependencyProperty.Register( nameof( TitleForeground ),
								     typeof( Brush ),
								     typeof( SelectPrinterButton ),
								     new FrameworkPropertyMetadata( Brushes.Black,
															        FrameworkPropertyMetadataOptions.AffectsRender,
															        ( o, args ) =>
															        {
																        ( (SelectPrinterButton)o ).TitleForeground = (Brush)args.NewValue;
															        } ) );
#endregion

#region Application Context
	[Category( "Options" )]
	public string ApplicationContext
	{
		get => (string)GetValue( ApplicationContextProperty );
		set
		{
			SetValue( ApplicationContextProperty, value );
			SelectPrinter.ApplicationContext = value;
		}
	}

	public static readonly DependencyProperty ApplicationContextProperty = DependencyProperty.Register( nameof( ApplicationContext ),
																									    typeof( string ),
																									    typeof( SelectPrinterButton ),
																									    new FrameworkPropertyMetadata( SelectPrinter.DEFAULT_CONTEXT,
																																       FrameworkPropertyMetadataOptions.None,
																																       OnApplicationContextChanged ) );

	private static void OnApplicationContextChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is SelectPrinterButton B && e.NewValue is string C )
			B.ApplicationContext = C;
	}
#endregion

#region Caption
	[Category( "Options" )]
	public string Caption
	{
		get => (string)GetValue( CaptionProperty );
		set => SetValue( CaptionProperty, value );
	}

	public static readonly DependencyProperty CaptionProperty =
		DependencyProperty.Register( nameof( Caption ),
								     typeof( string ),
								     typeof( SelectPrinterButton ),
								     new PropertyMetadata( "Select Printer" ) );
#endregion

#region Settings Folder
	[Category( "Options" )]
	public string SettingsFolder
	{
		get => (string)GetValue( SettingsFolderProperty );
		set
		{
			SetValue( SettingsFolderProperty, value );
			SelectPrinter.SettingsFolder = value;
		}
	}

	public static readonly DependencyProperty SettingsFolderProperty = DependencyProperty.Register( nameof( SettingsFolder ),
																								    typeof( string ),
																								    typeof( SelectPrinterButton ),
																								    new FrameworkPropertyMetadata( SelectPrinter.PRINTER_SETTINGS_FOLDER,
																															       FrameworkPropertyMetadataOptions.None,
																															       OnSettingsFolderChanged ) );

	private static void OnSettingsFolderChanged( DependencyObject obj, DependencyPropertyChangedEventArgs e )
	{
		( (SelectPrinterButton)obj ).SettingsFolder = (string)e.NewValue;
	}
#endregion

	// Using a DependencyProperty as the backing store for SettingsFolder.  This enables animation, styling, binding, etc...

#region TitleBackground
	[Category( "Options" )]
	public Brush TitleBackground
	{
		get => (Brush)GetValue( TitleBackgroundProperty );
		set
		{
			SetValue( TitleBackgroundProperty, value );
			SelectPrinter.TitleBackground = value;
		}
	}

	// Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleBackgroundProperty =
		DependencyProperty.Register( nameof( TitleBackground ),
								     typeof( Brush ),
								     typeof( SelectPrinterButton ),
								     new FrameworkPropertyMetadata( Brushes.Transparent,
															        FrameworkPropertyMetadataOptions.AffectsRender,
															        ( o, args ) =>
															        {
																        ( (SelectPrinterButton)o ).TitleBackground = (Brush)args.NewValue;
															        } ) );
#endregion
}