﻿using System.Windows.Controls;
using System.Windows.Input;

namespace IdsControlLibrary
{
	/// <summary>
	///     Interaction logic for HamburgerItem.xaml
	/// </summary>
	public partial class HamburgerItem : UserControl
	{
		private Brush SaveBackground;

		public event HamburgerItemClick Click;

		public virtual Page OnClick( IdsContentPresenter presenter ) => Click?.Invoke( presenter, this );

		public HamburgerItem()
		{
			InitializeComponent();

			if( DesignerProperties.GetIsInDesignMode( this ) )
				Background = Brushes.DarkGray;
		}

		public delegate Page HamburgerItemClick( IdsContentPresenter presenter, HamburgerItem sender );

		private void DockPanel_MouseEnter( object sender, MouseEventArgs e )
		{
			if( sender is DockPanel Sender )
				Sender.Background = MouseOverBrush;
		}

		private void DockPanel_MouseLeave( object sender, MouseEventArgs e )
		{
			if( sender is DockPanel Sender )
				Sender.Background = SaveBackground;
		}

		private void UserControl_Loaded( object sender, RoutedEventArgs e )
		{
			SaveBackground = Background;
		}

		public ImageSource ImageSource
		{
			get => (ImageSource)GetValue( ImageSourceProperty );
			set => SetValue( ImageSourceProperty, value );
		}

		// Using a DependencyProperty as the backing store for ImageSource.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ImageSourceProperty =
			DependencyProperty.Register( "ImageSource", typeof( ImageSource ), typeof( HamburgerItem ) );

		public Brush MouseOverBrush
		{
			get => (Brush)GetValue( MouseOverBrushProperty );
			set => SetValue( MouseOverBrushProperty, value );
		}

		// Using a DependencyProperty as the backing store for MouseOverBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MouseOverBrushProperty =
			DependencyProperty.Register( "MouseOverBrush", typeof( Brush ), typeof( HamburgerItem ), new PropertyMetadata( Brushes.Transparent ) );

		public string Text
		{
			get => (string)GetValue( TextProperty );
			set => SetValue( TextProperty, value );
		}

		// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register( "Text", typeof( string ), typeof( HamburgerItem ), new PropertyMetadata( "" ) );
	}
}