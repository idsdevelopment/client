﻿using System.Windows.Input;

namespace IdsControlLibrary
{
	/// <summary>
	///     Interaction logic for ToolbarItem.xaml
	/// </summary>
	public partial class ToolbarItem
	{
		private Brush SaveBrush;

		public event ToolbarItemClickEvent Click;

		public ToolbarItem()
		{
			InitializeComponent();
		}

		public delegate void ToolbarItemClickEvent( ToolbarItem sender );

		private void UserControl_MouseDown( object sender, MouseButtonEventArgs e )
		{
			Click?.Invoke( this );
		}

		private void UserControl_MouseLeave( object sender, MouseEventArgs e )
		{
			Background = SaveBrush;
		}

		private void UserControl_MouseEnter( object sender, MouseEventArgs e )
		{
			SaveBrush          = Background;
			Background         = Brushes.SlateGray.Clone();
			Background.Opacity = 0.1;
		}
	}
}