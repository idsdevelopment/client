﻿#nullable enable

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using AzureRemoteService;
using CommandViewModel.Annotations;
using Protocol.Data;
using Utils;
using XamlViewModel.Wpf;

namespace IdsRemoteServiceControlLibraryV2.Formulas;

public class VariableEntry : INotifyPropertyChanged
{
	public string Name { get; set; }

	public string Value
	{
		get => _Value;
		set
		{
			_Value = value;
			OnPropertyChanged();
		}
	}

	private string _Value = null!;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public VariableEntry( string name )
	{
		Name  = name;
		Value = "";
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class FormulaViewModel : ViewModelBase
{
	public string TestResult
	{
		get { return Get( () => TestResult, "" ); }
		set { Set( () => TestResult, value ); }
	}

	public ObservableCollection<VariableEntry> Variables
	{
		get { return Get( () => Variables, new ObservableCollection<VariableEntry>() ); }
		set { Set( () => Variables, value ); }
	}

#region Testing
	public ICommand Test => Commands[ nameof( Test ) ];

	public async void Execute_Test()
	{
		if( !IsInDesignMode )
		{
			var Result = await Azure.Client.RequestEvaluateFormula( new EvaluateFormula
			                                                        {
				                                                        DataType = Type,
				                                                        Variables = ( from V in Variables
				                                                                      select V ).ToDictionary( entry => entry.Name, entry => entry.Value ),
				                                                        Expression = Expression
			                                                        } );

			TestResult = Result.Value.ToString( CultureInfo.InvariantCulture );

			var Errs = new StringBuilder();

			foreach( var E in Result.ErrorList )
				Errs.Append( $"{E.Trim()}\r\n" );

			Errors = Errs.ToString().Trim();
		}
	}

	public string Errors
	{
		get { return Get( () => Errors, "" ); }
		set { Set( () => Errors, value ); }
	}
#endregion


#region Formula
	public Action<string>? OnFormulaChange;

	public string Expression
	{
		get { return Get( () => Expression, "" ); }
		set { Set( () => Expression, value ); }
	}

	[DependsUpon250( nameof( Expression ) )]
	public void WhenExpressionChanges()
	{
		Dispatcher.Invoke( () =>
		                   {
			                   OnFormulaChange?.Invoke( Expression );
		                   } );
	}
#endregion

#region DATA_TYPE
	private string Type = "";

	public Formulas.DATA_TYPE DataType
	{
		get { return Get( () => DataType, Formulas.DATA_TYPE.UNSET ); }
		set { Set( () => DataType, value ); }
	}

	[DependsUpon( nameof( DataType ) )]
	public async void WhenDataTypeChanges()
	{
		Type = DataType switch
		       {
			       Formulas.DATA_TYPE.SHIFT => "SHIFT",
			       Formulas.DATA_TYPE.TRIP  => "TRIP",
			       _                        => ""
		       };

		if( !IsInDesignMode && Type.IsNotNullOrWhiteSpace() )
		{
			Variables = new ObservableCollection<VariableEntry>( from V in await Azure.Client.RequestGetFormulaVariables( Type )
			                                                     select new VariableEntry( V ) );
		}
	}
#endregion
}