﻿#nullable enable

using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace IdsRemoteServiceControlLibraryV2.Formulas;

/// <summary>
///     Interaction logic for Formulas.xaml
/// </summary>
public partial class Formulas : UserControl
{
	private bool InFormulaChange;

	private readonly FormulaViewModel Model;

	public Formulas()
	{
		InitializeComponent();
		Model = (FormulaViewModel)Root.DataContext;

		Model.OnFormulaChange = expression =>
		                        {
			                        InFormulaChange = true;

			                        try
			                        {
				                        Formula = expression;
			                        }
			                        finally
			                        {
				                        InFormulaChange = false;
			                        }
		                        };
	}

#region DATA_TYPE
	public enum DATA_TYPE
	{
		UNSET,
		SHIFT,
		TRIP
	}

	private static void DataTypePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is Formulas F && e.NewValue is DATA_TYPE DataType )
			F.Model.DataType = DataType;
	}

	[Category( "Options" )]
	public DATA_TYPE DataType
	{
		get => (DATA_TYPE)GetValue( DataTypeProperty );
		set => SetValue( DataTypeProperty, value );
	}

	public static readonly DependencyProperty DataTypeProperty =
		DependencyProperty.Register( nameof( DataType ), typeof( DATA_TYPE ), typeof( Formulas ), new FrameworkPropertyMetadata( DATA_TYPE.UNSET, DataTypePropertyChangedCallback ) );
#endregion

#region Formula
	private static void FormulaPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		// ReSharper disable once MergeSequentialPatterns
		if( d is Formulas F && !F.InFormulaChange && e.NewValue is string Expression )
			F.Model.Expression = Expression;
	}

	[Category( "Options" )]
	public string Formula
	{
		get => (string)GetValue( FormulaProperty );
		set => SetValue( FormulaProperty, value );
	}

	public static readonly DependencyProperty FormulaProperty =
		DependencyProperty.Register( nameof( Formula ), typeof( string ), typeof( Formulas ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, FormulaPropertyChangedCallback ) );
#endregion
}