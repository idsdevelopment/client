﻿using System.Windows.Controls.Primitives;
using IdsControlLibraryV2.Utils;
using Utils;
using WeakEvent;

namespace IdsRemoteServiceControlLibraryV2.Drivers;

public class DriversPopup : Popup
{
	~DriversPopup()
	{
		Dispatcher?.Invoke( () =>
		                    {
			                    var DriverLists = Lv.FindChildren<ListView>( "DriverList" );

			                    foreach( var DriverList in DriverLists )
				                    DriverList.SelectionChanged -= DriverListOnSelectionChanged;
		                    } );
	}

	public event EventHandler<string> OnSelectionChanged
	{
		add => _OnSelectionChanged.Subscribe( value );
		remove => _OnSelectionChanged.Unsubscribe( value );
	}

	public DriversPopup()
	{
		if( Border is null )
		{
			Border = @"<Border.Padding>0,0,0,2</Border.Padding>".XamlToBorder();

			Lv = @"<ListView.Resources>
							<Style TargetType=""GridViewColumnHeader"">
								<Setter Property=""Visibility"" Value=""Collapsed"" />
    						</Style >
    					</ListView.Resources>
                        <ListView.ItemContainerStyle> 
							<Style TargetType=""ListViewItem""> 
								<Setter Property=""HorizontalContentAlignment"" Value=""Stretch""/>
								<Setter Property=""VerticalContentAlignment"" Value=""Stretch""/>
							</Style>
						</ListView.ItemContainerStyle>".XamlToListView();

			ScrollViewer.SetHorizontalScrollBarVisibility( Lv, ScrollBarVisibility.Disabled );

			Border.Child = Lv;

			var Ip = "<WrapPanel  Orientation=\"Horizontal\" />".XamlToItemsPanelTemplate();
			Lv.ItemsPanel = Ip;

			var Gv = @"<GridView.Columns>
					           <GridViewColumn>
						           <GridViewColumn.CellTemplate>
										<DataTemplate>
											<DockPanel>
                                                <TextBlock Text=""{Binding Category}"" FontWeight=""Bold""  DockPanel.Dock=""Top""/>
                                                <ListView ItemsSource=""{Binding Drivers}"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Stretch"" DockPanel.Dock=""Top"" x:Name=""DriverList"" Margin=""0,0,0,5"">
                                                    <ListView.Resources>
														<system:Double x:Key=""{x:Static SystemParameters.VerticalScrollBarWidthKey}"">10</system:Double>
														<system:Double x:Key=""{x:Static SystemParameters.HorizontalScrollBarHeightKey}"">10</system:Double >
													</ListView.Resources >
													<ListView.ItemContainerStyle> 
														<Style TargetType=""ListViewItem""> 
															<Setter Property=""HorizontalContentAlignment"" Value=""Stretch""/>
                                                        </Style>
													</ListView.ItemContainerStyle>
                                                    <ListView.ItemTemplate>
														<DataTemplate>
															<TextBlock Text=""{Binding Display}"" />
														</DataTemplate>
													</ListView.ItemTemplate>
												</ListView>
											</DockPanel>
										</DataTemplate>
						           </GridViewColumn.CellTemplate>
					           </GridViewColumn>
				           </GridView.Columns>".XamlToGridView();

			Lv.View = Gv;

			Model          = new DriversPopupViewModel();
			Lv.DataContext = Model;
			Lv.ItemsSource = Model.Drivers;
			Placement      = PlacementMode.Right;

			Lv.Loaded += ( sender, args ) =>
			             {
				             var DriverLists = Lv.FindChildren<ListView>( "DriverList" );

				             foreach( var DriverList in DriverLists )
				             {
					             DriverList.SelectionChanged -= DriverListOnSelectionChanged;
					             DriverList.SelectedItem     =  null;
					             DriverList.SelectionChanged += DriverListOnSelectionChanged;
				             }
			             };
		}
	}

	private readonly WeakEventSource<string> _OnSelectionChanged = new();
	private static   Border                  Border;

	public static readonly DependencyProperty DriverSelectedProperty =
		DependencyProperty.Register( nameof( DriverSelected ), typeof( bool ), typeof( DriversPopup ), new FrameworkPropertyMetadata( false ) );

	private static          Popup                 LastPopup;
	private static readonly object                LockObject = new();
	private static          ListView              Lv;
	private static          DriversPopupViewModel Model;

	public new static readonly DependencyProperty IsOpenProperty =
		DependencyProperty.Register( nameof( IsOpen ), typeof( bool ), typeof( DriversPopup ), new FrameworkPropertyMetadata( false, IsOpenPropertyChangedCallback ) );


	// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable


	public static readonly DependencyProperty SelectedProperty =
		DependencyProperty.Register( nameof( Selected ), typeof( string ), typeof( DriversPopup ),
		                             new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, SelectedPropertyChangedCallback, SelectedCoerceValueCallback ) );

	public static readonly DependencyProperty SelectTextProperty =
		DependencyProperty.Register( nameof( SelectText ), typeof( string ), typeof( DriversPopup ), new FrameworkPropertyMetadata( "Select Driver" ) );

	private void DriverListOnSelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( e.AddedItems[ 0 ] is DriversPopupViewModel.DriverEntry Entry )
		{
			Model.SelectedDriverEntry = Entry;
			var Driver = Entry.DriveId;
			Selected = Driver;
			IsOpen   = false;
			_OnSelectionChanged.Raise( this, Driver );
		}
	}


	private static void IsOpenPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is DriversPopup Dp && e.NewValue is bool Open )
		{
			lock( LockObject )
			{
				var Pop = (Popup)Dp;

				if( Open )
				{
					if( LastPopup is not null )
					{
						LastPopup.IsOpen = false;
						LastPopup.Child  = null;
					}

					LastPopup         = Pop;
					Dp.DriverSelected = false;
					Pop.IsOpen        = true;
					Pop.Child         = Border;
				}
				else
				{
					Pop.IsOpen = false;
					Pop.Child  = null;
				}
			}
		}
	}

	private static object SelectedCoerceValueCallback( DependencyObject d, object basevalue )
	{
		if( d is DriversPopup Dp && basevalue is string Drv )
			return Drv.IsNotNullOrWhiteSpace() ? Drv : Dp.SelectText;

		return "??????";
	}

	private static void SelectedPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
	}

	[Category( "Common" )]
	public bool DriverSelected
	{
		get => (bool)GetValue( DriverSelectedProperty );
		set => SetValue( DriverSelectedProperty, value );
	}


	[Category( "Common" )]
	public new bool IsOpen
	{
		get => (bool)GetValue( IsOpenProperty );
		set => SetValue( IsOpenProperty, value );
	}


	[Category( "Common" )]
	public string Selected
	{
		get => (string)GetValue( SelectedProperty );
		set => SetValue( SelectedProperty, value );
	}


	[Category( "Common" )]
	public string SelectText
	{
		get => (string)GetValue( SelectTextProperty );
		set => SetValue( SelectTextProperty, value );
	}
}