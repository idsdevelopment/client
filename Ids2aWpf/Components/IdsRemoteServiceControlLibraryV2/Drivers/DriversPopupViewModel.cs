﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using XamlViewModel.Wpf;

namespace IdsRemoteServiceControlLibraryV2.Drivers;

public class DriversPopupViewModel : ViewModelBase
{
	private const int REFRESH_INTERVAL_IN_MINUTES = 15;

	public class DriverCategory
	{
		public string Category { get; internal set; }

		// ReSharper disable once MemberHidesStaticFromOuterClass
		public ObservableCollection<DriverEntry> Drivers { get; set; } = new();
	}

	public class DriverEntry
	{
		public string DriveId { get; set; }          = "";
		public string Display { get; internal set; } = "";
	}

	public static List<StaffLookupSummary> DriverList
	{
		get
		{
			if( IsInDesignMode )
				return new List<StaffLookupSummary>();

			lock( LockObject )
			{
				if( LastResponse is null || ( LastResponse.Count == 0 ) )
				{
					LastResponse = ( from S in Azure.Client.RequestGetDrivers().Result
					                 orderby S.StaffId
					                 select S ).ToList();
				}

				return LastResponse;
			}
		}

		private set
		{
			lock( LockObject )
				LastResponse = value;
		}
	}

	public ObservableCollection<DriverCategory> Drivers
	{
		get { return Get( () => Drivers, Categories ); }
		set { Set( () => Drivers, value ); }
	}


	public DriverEntry SelectedDriverEntry
	{
		get { return Get( () => SelectedDriverEntry, new DriverEntry() ); }
		set { Set( () => SelectedDriverEntry, value ); }
	}


	public string SelectedDriver
	{
		get { return Get( () => SelectedDriver, "" ); }
		set { Set( () => SelectedDriver, value ); }
	}

	private static readonly object                               LockObject   = new();
	private static readonly Timer                                RefreshTimer = new( 1000 * 60 * REFRESH_INTERVAL_IN_MINUTES );
	private static          ObservableCollection<DriverCategory> Categories   = new();
	private static          uint                                 InstanceCount;
	private static          bool                                 First        = true;
	private static          List<StaffLookupSummary>             LastResponse = new();

	private static readonly List<DriversPopupViewModel> ActiveModels = new();

	public Action OnDriverListChanged;

	[DependsUpon( nameof( SelectedDriverEntry ) )]
	public void WhenSelectedDriverEntryChanges()
	{
		SelectedDriver = SelectedDriverEntry.DriveId;
	}

	~DriversPopupViewModel()
	{
		lock( LockObject )
		{
			ActiveModels.Remove( this );

			if( ( InstanceCount > 0 ) && ( --InstanceCount == 0 ) )
			{
				RefreshTimer.Stop();
				Categories = new ObservableCollection<DriverCategory>();
			}
		}
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		lock( LockObject )
		{
			InstanceCount++;
			RefreshTimer.Elapsed += RefreshTimerOnElapsed;

			ActiveModels.Add( this );

			if( InstanceCount == 1 )
			{
				if( First )
				{
					First                =  false;
					RefreshTimer.Elapsed += RefreshTimerOnElapsed;
				}

				RefreshTimer.Start();
				RefreshTimerOnElapsed( null, null );
			}
		}
	}


	private static async void RefreshTimerOnElapsed( object sender, ElapsedEventArgs e )
	{
		if( !IsInDesignMode )
		{
			var Response = ( from S in await Azure.Client.RequestGetDrivers()
			                 orderby S.StaffId
			                 select S ).ToList();

			if( Response.Count == LastResponse.Count )
			{
				var Found = true;
				var Ndx   = 0;

				foreach( var Summary in Response )
				{
					if( LastResponse[ Ndx++ ].StaffId != Summary.StaffId )
					{
						Found = false;
						break;
					}
				}

				if( Found ) // Lists equal
					return;
			}

			LastResponse = Response;

			var Drvs = new List<DriverCategory>();

			void Add( string driverId, string display )
			{
				if( driverId.IsNotNullOrWhiteSpace() && display.IsNotNullOrWhiteSpace() )
				{
					display = display.Capitalise();

					if( display.IsNotNullOrWhiteSpace() )
					{
						var Letter = display[ 0 ].ToString();

						var Found = false;

						var Driver = new DriverEntry
						             {
							             DriveId = driverId,
							             Display = display
						             };

						foreach( var DriverCategory in Drvs )
						{
							if( DriverCategory.Category == Letter )
							{
								Found = true;
								DriverCategory.Drivers.Add( Driver );
								break;
							}
						}

						if( !Found )
						{
							var Cat = new DriverCategory
							          {
								          Category = Letter
							          };
							Cat.Drivers.Add( Driver );
							Drvs.Add( Cat );
						}
					}
				}
			}

			foreach( var Driver in Response )
			{
				Add( Driver.StaffId, Driver.StaffId );
				Add( Driver.StaffId, $"{Driver.FirstName} {Driver.LastName}".Trim() );

				if( Driver.LastName.IsNotNullOrWhiteSpace() )
					Add( Driver.StaffId, $"{Driver.LastName} ({Driver.FirstName})".Trim() );
			}

			Drvs = ( from D in Drvs
			         orderby D.Category
			         select D ).ToList();

			foreach( var DriverCategory in Drvs )
			{
				DriverCategory.Drivers = new ObservableCollection<DriverEntry>( from D in DriverCategory.Drivers
				                                                                orderby D.Display
				                                                                select D );
			}

			Dispatcher.Invoke( () =>
			                   {
				                   lock( LockObject )
				                   {
					                   Categories.Clear();

					                   foreach( var Category in Drvs )
						                   Categories.Add( Category );
				                   }
			                   } );

			lock( LockObject )
			{
				foreach( var Model in ActiveModels )
					Model.OnDriverListChanged?.Invoke();
			}
		}
	}
}