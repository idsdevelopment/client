﻿using Notes = IdsControlLibraryV2.Notes.Notes;

namespace IdsRemoteServiceControlLibraryV2.Staff;

/// <summary>
///     Interaction logic for StaffNotes.xaml
/// </summary>
public partial class StaffNotes : UserControl
{
	private StaffNotesViewModel Model;

	public event Notes.OnBeforeDeleteNoteEvent OnBeforeDeleteNote;

	public StaffNotes()
	{
		Initialized += ( sender, args ) =>
		               {
			               if( Notes.DataContext is StaffNotesViewModel M )
			               {
				               Model = M;

				               M.OnRefresh = () =>
				                             {
					                             Notes.Refresh();
				                             };

				               Notes.OnBeforeDeleteNote += name => OnBeforeDeleteNote?.Invoke( name ) ?? true;

				               Notes.OnDeleteNote += name =>
				                                     {
					                                     Model?.ExecuteDeleteNote( name );
				                                     };
			               }
		               };

		InitializeComponent();
	}

	private void Notes_OnNewNote( string noteName )
	{
		Model?.ExecuteNewNote( noteName );
	}

	private void Notes_OnRenameNote( string oldNoteName, string newNoteName )
	{
		Model?.ExecuteRenameNote( oldNoteName, newNoteName );
	}


	private static void StaffIdPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is StaffNotes N && e.NewValue is string Id )
			N.Model.StaffId = Id;
	}

	public void Refresh()
	{
		Model?.OnRefresh?.Invoke();
	}


	[Category( "Options" )]
	public string ColumnHeader
	{
		get => (string)GetValue( ColumnHeaderProperty );
		set => SetValue( ColumnHeaderProperty, value );
	}

	// Using a DependencyProperty as the backing store for ColumnHeader.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ColumnHeaderProperty =
		DependencyProperty.Register( nameof( ColumnHeader ), typeof( string ), typeof( StaffNotes ), new FrameworkPropertyMetadata( "" ) );


	[Category( "Common" )]
	public new bool IsEnabled
	{
		get => (bool)GetValue( IsEnabledProperty );
		set => SetValue( IsEnabledProperty, value );
	}

	// Using a DependencyProperty as the backing store for IsEnabled.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty IsEnabledProperty =
		DependencyProperty.Register( nameof( IsEnabled ), typeof( bool ), typeof( StaffNotes ), new FrameworkPropertyMetadata( true ) );


	[Category( "Common" )]
	public string SelectedNoteName => (string)GetValue( SelectedNoteNameProperty );


	// Using a DependencyProperty as the backing store for SelectedNoteName.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedNoteNameProperty =
		DependencyProperty.Register( nameof( SelectedNoteName ), typeof( string ), typeof( StaffNotes ), new FrameworkPropertyMetadata( "" ) );


	[Category( "Options" )]
	public string StaffId
	{
		get => (string)GetValue( StaffIdProperty );
		set => SetValue( StaffIdProperty, value );
	}

	// Using a DependencyProperty as the backing store for StaffId.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StaffIdProperty =
		DependencyProperty.Register( nameof( StaffId ), typeof( string ), typeof( StaffNotes ), new FrameworkPropertyMetadata( "", StaffIdPropertyChangedCallback ) );
}