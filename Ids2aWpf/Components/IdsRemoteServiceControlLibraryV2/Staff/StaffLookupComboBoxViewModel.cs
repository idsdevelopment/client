﻿using AzureRemoteService;
using IdsControlLibraryV2.Combos;
using Utils;

namespace IdsRemoteServiceControlLibraryV2.Staff;

public class LookupItem : LookupComboBoxModelBase<StaffLookupSummary, string>.LookupItem
{
	public LookupItem( StaffLookupSummary item ) : base( item )
	{
	}
}

public class StaffLookupComboBoxViewModel : LookupComboBoxModelBase<StaffLookupSummary, string>
{
	public string StaffId
	{
		get { return Get( () => StaffId, "" ); }
		set { Set( () => StaffId, value ); }
	}

	public StaffLookupComboBox StaffLookupComboBox;

	private StaffLookup NewLookup( PreFetch.PREFETCH fetch, uint count ) => new()
																			{
																				Key           = StaffId,
																				PreFetchCount = (int)count,
																				Fetch         = fetch
																			};

	private StaffLookup NewLookup( PreFetch.PREFETCH fetch, string key, int count )
	{
		var Temp = NewLookup( fetch, (uint)count );
		Temp.Key = key;
		return Temp;
	}


	[DependsUpon( nameof( StaffId ) )]
	public void WhenStaffIdChanges()
	{
		Items = new ObservableCollection<LookupItem>();

		if( SearchText == "" )
			WhenSearchTextChanges();
		else
			SearchText = "";

		StaffLookupComboBox.StaffId = StaffId;
	}

	public override async Task<IList<StaffLookupSummary>> First( uint count ) => !IsInDesignMode ? await Azure.Client.RequestStaffLookup( NewLookup( PreFetch.PREFETCH.FIRST, count ) )
																					 : new List<StaffLookupSummary>();

	public override Task<IList<StaffLookupSummary>> First( string key, uint count ) => throw new NotImplementedException();

	public override async Task<IList<StaffLookupSummary>> Last( uint count ) => !IsInDesignMode ? await Azure.Client.RequestStaffLookup( NewLookup( PreFetch.PREFETCH.LAST, count ) )
																					: new List<StaffLookupSummary>();

	public override Task<IList<StaffLookupSummary>> Last( string key, uint count ) => throw new NotImplementedException();

	public override async Task<IList<StaffLookupSummary>> FindRange( string key, int count ) => !IsInDesignMode ? await Azure.Client.RequestStaffLookup( NewLookup( PreFetch.PREFETCH.FIND_RANGE, key, count ) )
																									: new List<StaffLookupSummary>();

	public override async Task<IList<StaffLookupSummary>> FindMatch( string key, int count ) => !IsInDesignMode ? await Azure.Client.RequestStaffLookup( NewLookup( PreFetch.PREFETCH.FIND_MATCH, key, count ) )
																									: new List<StaffLookupSummary>();

	public override int CompareSearchKey( string searchKey, string k2 ) => string.Compare( searchKey, k2, StringComparison.CurrentCultureIgnoreCase );

	public override int Compare( string k1, string k2 ) => string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );

	public override bool StartsWith( string searchValue, string k2 ) => k2.StartsWith( searchValue, StringComparison.CurrentCultureIgnoreCase );

	public override bool Filter( string filter, StaffLookupSummary value ) => throw new NotImplementedException();

	public override string Key( StaffLookupSummary item ) => item.StaffId;

	public override string Key( string searchText ) => searchText;

	public override bool IsEmpty( string key ) => key.IsNullOrEmpty();

	public override string AsString( string key ) => key;
}