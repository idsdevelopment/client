﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AzureRemoteService;
using Utils;
using XamlViewModel.Wpf;

namespace IdsRemoteServiceControlLibraryV2.Staff;

public class StaffNotesViewModel : ViewModelBase
{
	private const string PROGRAM_NAME = "StaffNotes";

	public IDictionary<string, string> Notes
	{
		get { return Get( () => Notes, new Dictionary<string, string>() ); }
		set { Set( () => Notes, value ); }
	}

	public string StaffId
	{
		get { return Get( () => StaffId, "" ); }
		set { Set( () => StaffId, value ); }
	}

	public Action OnRefresh;

	[DependsUpon350( nameof( StaffId ) )]
	public async Task WhenStaffIdChanges()
	{
		var StaffNotes = await Azure.Client.RequestGetStaffNotes( StaffId );

		if( StaffNotes.IsNotNull() )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   var Dict = new Dictionary<string, string>();

				                   foreach( var Note in StaffNotes )
					                   Dict.Add( Note.NoteId, Note.Text );

				                   Notes = Dict;
			                   } );
		}
	}

	public async Task ExecuteNewNote( string noteName )
	{
		await Azure.Client.RequestAddUpdateStaffNote( PROGRAM_NAME, StaffId, noteName, "" );
	}

	public async Task ExecuteRenameNote( string oldNoteName, string newNoteName )
	{
		await Azure.Client.RequestRenameStaffNote( PROGRAM_NAME, StaffId, oldNoteName, newNoteName );
	}

	public async Task ExecuteDeleteNote( string noteName )
	{
		await Azure.Client.RequestDeleteStaffNote( PROGRAM_NAME, StaffId, noteName );
	}
}