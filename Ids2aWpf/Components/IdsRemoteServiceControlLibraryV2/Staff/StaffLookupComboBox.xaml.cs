﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using IdsControlLibraryV2.Combos;
using Protocol.Data;
using Utils;

namespace IdsRemoteServiceControlLibraryV2.Staff;

public class StaffComboBoolToEvenOddSelectedBackgroundConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( parameter is StaffLookupComboBox Combo && value is LookupItemBase Item )
		{
			if( Item.Selected )
				return Combo.SelectedItemBackground;
			return Item.IsEvenRow ? Combo.EvenRowBackground : Combo.OddRowBackground;
		}
		return Brushes.Red;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

/// <summary>
///     Interaction logic for StaffLookupComboBox.xaml
/// </summary>
public partial class StaffLookupComboBox : UserControl
{
	public  bool                         DesignMode => DesignerProperties.GetIsInDesignMode( this );
	private StaffLookupComboBoxViewModel Context;

	public StaffLookupComboBox()
	{
		InitializeComponent();

		StaffCombo.ItemSelected += item =>
		                           {
			                           SelectedItem = item.GetItem() as StaffLookupSummary;
		                           };

		Loaded += ( sender, args ) =>
		          {
			          if( StaffCombo.DataContext is StaffLookupComboBoxViewModel Ctx )
			          {
				          Context                     = Ctx;
				          Context.StaffLookupComboBox = this;
				          Ctx.WhenSearchTextChanges();
			          }
		          };
	}

	private static void SelectedItemPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is StaffLookupComboBox Cl && Cl.Context.IsNotNull() && !Cl.DesignMode && e.NewValue is StaffLookupSummary Summary )
			Cl.Context.SelectItem = Summary;
	}


	private static void StaffIdPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is StaffLookupComboBox Lc && e.NewValue is string StaffId )
		{
			var Ctx = Lc.Context;

			if( Ctx.IsNotNull() )
				Ctx.StaffId = StaffId;
		}
	}

	[Category( "Brush" )]
	public Brush EvenRowBackground
	{
		get => (Brush)GetValue( EvenRowBackgroundProperty );
		set => SetValue( EvenRowBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for OddRowBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty EvenRowBackgroundProperty =
		DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.AliceBlue ) );


	[Category( "Brush" )]
	public Brush OddRowBackground
	{
		get => (Brush)GetValue( OddRowBackgroundProperty );
		set => SetValue( OddRowBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for OddRowBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty OddRowBackgroundProperty =
		DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightGoldenrodYellow ) );

	[Category( "Common" )]
	public StaffLookupSummary SelectedItem
	{
		get => (StaffLookupSummary)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}

	// Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedItemProperty =
		DependencyProperty.Register( nameof( SelectedItem ), typeof( StaffLookupSummary ), typeof( StaffLookupComboBox ),
		                             new FrameworkPropertyMetadata( default( RouteSchedule ),
		                                                            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
		                                                            SelectedItemPropertyChangedCallback ) );


	[Category( "Brush" )]
	public Brush SelectedItemBackground
	{
		get => (Brush)GetValue( SelectedItemBackgroundProperty );
		set => SetValue( SelectedItemBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for SelectedItemBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedItemBackgroundProperty =
		DependencyProperty.Register( nameof( SelectedItemBackground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightBlue ) );


	[Category( "Common" )]
	public string StaffId
	{
		get => (string)GetValue( StaffIdProperty );
		set => SetValue( StaffIdProperty, value );
	}

	// Using a DependencyProperty as the backing store for StaffId.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StaffIdProperty =
		DependencyProperty.Register( nameof( StaffId ), typeof( string ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, StaffIdPropertyChangedCallback ) );
}