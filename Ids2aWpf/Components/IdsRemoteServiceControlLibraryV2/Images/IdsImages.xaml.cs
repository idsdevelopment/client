﻿namespace IdsRemoteServiceControlLibraryV2.Images;

/// <summary>
///     Interaction logic for IdsImages.xaml
/// </summary>
public partial class IdsImages : UserControl
{
	private readonly ImagesViewModel Model;

	public IdsImages()
	{
		InitializeComponent();
		Model = (ImagesViewModel)Root.DataContext;
	}

#region Image Type
	private static void ImageTypePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is GetImages.IMAGE_TYPE ImageType )
			Images.Model.ImageType = ImageType;
	}

	[Category( "Options" )]
	public GetImages.IMAGE_TYPE ImageType
	{
		get => (GetImages.IMAGE_TYPE)GetValue( ImageTypeProperty );
		set => SetValue( ImageTypeProperty, value );
	}

	public static readonly DependencyProperty ImageTypeProperty = DependencyProperty.Register( nameof( ImageType ),
	                                                                                           typeof( GetImages.IMAGE_TYPE ),
	                                                                                           typeof( IdsImages ),
	                                                                                           new FrameworkPropertyMetadata( GetImages.IMAGE_TYPE.TRIP,
	                                                                                                                          ImageTypePropertyChangedCallback ) );
#endregion


#region ShowDetails
	private static void ShowDetailsPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is bool ShowDetails )
			Images.Model.ShowDetails = ShowDetails;
	}

	public bool ShowDetails
	{
		get => (bool)GetValue( ShowDetailsProperty );
		set => SetValue( ShowDetailsProperty, value );
	}

	public static readonly DependencyProperty ShowDetailsProperty = DependencyProperty.Register( nameof( ShowDetails ),
	                                                                                             typeof( bool ),
	                                                                                             typeof( IdsImages ),
	                                                                                             new FrameworkPropertyMetadata( false,
	                                                                                                                            ShowDetailsPropertyChangedCallback ) );
#endregion


#region Height
	private static void HeightPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is int Height )
		{
			( (UserControl)Images ).Height = Height;
			Images.Model.Height            = Height;
		}
	}

	public new int Height
	{
		get => (int)GetValue( HeightProperty );
		set => SetValue( HeightProperty, value );
	}

	public new static readonly DependencyProperty HeightProperty = DependencyProperty.Register( nameof( Height ),
	                                                                                            typeof( int ),
	                                                                                            typeof( IdsImages ),
	                                                                                            new FrameworkPropertyMetadata( GetImages.DEFAULT_HEIGHT,
	                                                                                                                           HeightPropertyChangedCallback ) );
#endregion


#region Image Height
	private static void ImageHeightPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is int Height )
			Images.Model.ImageHeight = Height;
	}

	[Category( "Options" )]
	public int ImageHeight
	{
		get => (int)GetValue( ImageHeightProperty );
		set => SetValue( ImageHeightProperty, value );
	}

	public static readonly DependencyProperty ImageHeightProperty = DependencyProperty.Register( nameof( ImageHeight ),
	                                                                                             typeof( int ),
	                                                                                             typeof( IdsImages ),
	                                                                                             new FrameworkPropertyMetadata( GetImages.DEFAULT_HEIGHT,
	                                                                                                                            ImageHeightPropertyChangedCallback ) );
#endregion


#region Width
	private static void WidthPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is int Width )
		{
			( (UserControl)Images ).Width = Width;
			Images.Model.Width            = Width;
		}
	}

	public new int Width
	{
		get => (int)GetValue( WidthProperty );
		set => SetValue( WidthProperty, value );
	}

	public new static readonly DependencyProperty WidthProperty = DependencyProperty.Register( nameof( Width ),
	                                                                                           typeof( int ),
	                                                                                           typeof( IdsImages ),
	                                                                                           new FrameworkPropertyMetadata( GetImages.DEFAULT_WIDTH,
	                                                                                                                          WidthPropertyChangedCallback ) );
#endregion


#region Image Width
	private static void ImageWidthPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is int Width )
			Images.Model.ImageWidth = Width;
	}

	[Category( "Options" )]
	public int ImageWidth
	{
		get => (int)GetValue( ImageWidthProperty );
		set => SetValue( ImageWidthProperty, value );
	}

	public static readonly DependencyProperty ImageWidthProperty = DependencyProperty.Register( nameof( ImageWidth ),
	                                                                                            typeof( int ),
	                                                                                            typeof( IdsImages ),
	                                                                                            new FrameworkPropertyMetadata( GetImages.DEFAULT_WIDTH,
	                                                                                                                           ImageWidthPropertyChangedCallback ) );
#endregion

#region Image Name
	private static void ImageNamePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is IdsImages Images && e.NewValue is string ImageName )
			Images.Model.Name = ImageName;
	}

	[Category( "Options" )]
	public string ImageName
	{
		get => (string)GetValue( ImageNameProperty );
		set => SetValue( ImageNameProperty, value );
	}

	public static readonly DependencyProperty ImageNameProperty = DependencyProperty.Register( nameof( ImageName ),
	                                                                                           typeof( string ),
	                                                                                           typeof( IdsImages ),
	                                                                                           new FrameworkPropertyMetadata( "",
	                                                                                                                          ImageNamePropertyChangedCallback ) );
#endregion
}