﻿#nullable enable

using System.IO;
using AzureRemoteService;

namespace IdsRemoteServiceControlLibraryV2.Images;

internal sealed class ImagesViewModel : ViewModelBase
{
	public class ImageAndDetails
	{
		public BitmapImage Image { get; }
		public string      Name  { get; }

		public string Description { get; }
		public string DateTime    { get; }

		public ImageAndDetails( BitmapImage image, string name, string description, DateTimeOffset dateTime )
		{
			Image       = image;
			Name        = name;
			Description = description;
			DateTime    = $"{dateTime:F}";
		}
	}

	public ObservableCollection<ImageAndDetails> Images
	{
		get { return Get( () => Images, EmptySources ); }
		set { Set( () => Images, value ); }
	}


	public GetImages.IMAGE_TYPE ImageType
	{
		get { return Get( () => ImageType, GetImages.IMAGE_TYPE.TRIP ); }
		set { Set( () => ImageType, value ); }
	}


	public int ImageHeight
	{
		get { return Get( () => ImageHeight, GetImages.DEFAULT_HEIGHT ); }
		set { Set( () => ImageHeight, value ); }
	}


	public int ImageWidth
	{
		get { return Get( () => ImageWidth, GetImages.DEFAULT_WIDTH ); }
		set { Set( () => ImageWidth, value ); }
	}


	public string Name
	{
		get { return Get( () => Name, "" ); }
		set { Set( () => Name, value ); }
	}


	public bool ShowDetails
	{
		get { return Get( () => ShowDetails, false ); }
		set { Set( () => ShowDetails, value ); }
	}

	private static readonly ObservableCollection<ImageAndDetails> EmptySources = new();


	public int Height = GetImages.DEFAULT_HEIGHT,
	           Width  = GetImages.DEFAULT_WIDTH;

	[DependsUpon150( nameof( ImageType ) )]
	[DependsUpon150( nameof( Name ) )]
	[DependsUpon150( nameof( ImageHeight ) )]
	[DependsUpon150( nameof( ImageWidth ) )]
	public async void WhenOptionChanges()
	{
		var Ih = ImageHeight;

		if( Ih <= 0 )
		{
			Ih          = GetImages.DEFAULT_HEIGHT;
			ImageHeight = Ih;
		}

		var Iw = ImageWidth;

		if( Iw <= 0 )
		{
			Iw         = GetImages.DEFAULT_WIDTH;
			ImageWidth = Iw;
		}

		var DownloadedImages = await Azure.Client.RequestDownloadImageByType( new GetImages
		                                                                      {
			                                                                      ImageType     = ImageType,
			                                                                      ImageNameOrId = Name,
			                                                                      Resolution = new GetImages.ImageResolution
			                                                                                   {
				                                                                                   Height = Ih,
				                                                                                   Width  = Iw
			                                                                                   }
		                                                                      } );

		Dispatcher.Invoke( () =>
		                   {
			                   var NewImages = new ObservableCollection<ImageAndDetails>();

			                   if( DownloadedImages is not null )
			                   {
				                   foreach( var Image in DownloadedImages )
				                   {
					                   var BMap = new BitmapImage();
					                   BMap.BeginInit();
					                   BMap.CacheOption  = BitmapCacheOption.OnLoad;
					                   BMap.StreamSource = new MemoryStream( Image.Bytes );
					                   BMap.EndInit();

					                   NewImages.Add( new ImageAndDetails( BMap,
					                                                       Path.GetFileNameWithoutExtension( Image.Name ),
					                                                       Image.Status.AsString(),
					                                                       Image.DateTime ) );
				                   }
			                   }

			                   Images = NewImages;
		                   } );
	}
}