﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using IdsControlLibraryV2.Combos;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Routes;

public class RouteComboBoolToEvenOddSelectedBackgroundConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( parameter is RouteLookupComboBox Combo && value is LookupItemBase Item )
		{
			if( Item.Selected )
				return Combo.SelectedItemBackground;
			return Item.IsEvenRow ? Combo.EvenRowBackground : Combo.OddRowBackground;
		}

		return Brushes.Red;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

/// <summary>
///     Interaction logic for RouteLookupComboBox.xaml
/// </summary>
public partial class RouteLookupComboBox : UserControl
{
	public  bool                     DesignMode => DesignerProperties.GetIsInDesignMode( this );
	private RouteLookupComboBoxModel Context;

	public RouteLookupComboBox()
	{
		InitializeComponent();

		Loaded += ( sender, args ) =>
		          {
			          if( RouteCombo.DataContext is RouteLookupComboBoxModel Ctx )
			          {
				          Context = Ctx;
				          Ctx.WhenSearchTextChanges();
			          }
		          };
	}

	private static void CustomerCodePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is RouteLookupComboBox Lc && e.NewValue is string CustCode )
		{
			var Ctx = Lc.Context;

			if( !( Ctx is null ) )
			{
				Ctx.CustomerCode = CustCode;
				Lc.RouteName     = "";
			}
		}
	}

	private static void SelectedItemPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is RouteLookupComboBox Cl && !Cl.DesignMode && e.NewValue is RouteSchedule Summary )
			Cl.Context.SelectItem = Summary;
	}

	private void RouteCombo_ItemSelected( LookupItemBase item )
	{
		if( item.GetItem() is RouteSchedule Item )
		{
			SelectedItem = Item;
			RouteName    = Item.RouteName;
		}
	}


	[Category( "Common" )]
	public string CustomerCode
	{
		get => (string)GetValue( CustomerCodeProperty );
		set => SetValue( CustomerCodeProperty, value );
	}

	// Using a DependencyProperty as the backing store for CustomerCode.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty CustomerCodeProperty =
		DependencyProperty.Register( nameof( CustomerCode ), typeof( string ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( "", CustomerCodePropertyChangedCallback ) );


	[Category( "Brush" )]
	public Brush EvenRowBackground
	{
		get => (Brush)GetValue( EvenRowBackgroundProperty );
		set => SetValue( EvenRowBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for OddRowBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty EvenRowBackgroundProperty =
		DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.AliceBlue ) );


	[Category( "Brush" )]
	public Brush OddRowBackground
	{
		get => (Brush)GetValue( OddRowBackgroundProperty );
		set => SetValue( OddRowBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for OddRowBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty OddRowBackgroundProperty =
		DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightGoldenrodYellow ) );

	[Category( "Common" )]
	public string RouteName
	{
		get => (string)GetValue( RouteNameProperty );
		set => SetValue( RouteNameProperty, value );
	}

	// Using a DependencyProperty as the backing store for RouteName.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty RouteNameProperty =
		DependencyProperty.Register( nameof( RouteName ), typeof( string ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );


	[Category( "Common" )]
	public RouteSchedule SelectedItem
	{
		get => (RouteSchedule)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}

	// Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedItemProperty =
		DependencyProperty.Register( nameof( SelectedItem ), typeof( RouteSchedule ), typeof( RouteLookupComboBox ),
		                             new FrameworkPropertyMetadata( default( RouteSchedule ),
		                                                            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, SelectedItemPropertyChangedCallback ) );


	[Category( "Brush" )]
	public Brush SelectedItemBackground
	{
		get => (Brush)GetValue( SelectedItemBackgroundProperty );
		set => SetValue( SelectedItemBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for SelectedItemBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedItemBackgroundProperty =
		DependencyProperty.Register( nameof( SelectedItemBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightBlue ) );
}