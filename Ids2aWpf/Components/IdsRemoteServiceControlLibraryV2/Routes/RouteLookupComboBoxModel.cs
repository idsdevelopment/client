﻿#nullable enable

using System.Globalization;
using System.Windows.Data;
using AzureRemoteService;
using IdsControlLibraryV2.Combos;

namespace IdsRemoteServiceControlLibraryV2.Routes;

public class ObjectToRouteScheduleConvertor : IValueConverter
{
	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is RouteSchedule Summary ? Summary : null;

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class LookupItem : LookupComboBoxModelBase<RouteSchedule, string>.LookupItem
{
	public LookupItem( RouteSchedule item ) : base( item )
	{
	}
}

public class RouteLookupComboBoxModel : LookupComboBoxModelBase<RouteSchedule, string>
{
	public string CustomerCode
	{
		get { return Get( () => CustomerCode, "" ); }
		set { Set( () => CustomerCode, value ); }
	}

	private RouteLookup NewRouteLookup( PreFetch.PREFETCH fetch ) => new()
																	 {
																		 CustomerCode  = CustomerCode,
																		 PreFetchCount = (int)PreFetchCount,
																		 Fetch         = fetch
																	 };

	private RouteLookup NewRouteLookup( PreFetch.PREFETCH fetch, string key )
	{
		var Temp = NewRouteLookup( fetch );
		Temp.Key = key;
		return Temp;
	}

	[DependsUpon( nameof( CustomerCode ) )]
	public void WhenCustomerCodeChanges()
	{
		Items = new ObservableCollection<LookupItem>();

		if( SearchText == "" )
			WhenSearchTextChanges();
		else
			SearchText = "";
	}

	public override async Task<IList<RouteSchedule>> First( uint       count )           => await Azure.Client.RequestRouteLookup( NewRouteLookup( PreFetch.PREFETCH.FIRST ) );
	public override       Task<IList<RouteSchedule>> First( string     key, uint count ) => throw new NotImplementedException();
	public override async Task<IList<RouteSchedule>> Last( uint        count )           => await Azure.Client.RequestRouteLookup( NewRouteLookup( PreFetch.PREFETCH.LAST ) );
	public override       Task<IList<RouteSchedule>> Last( string      key, uint count ) => throw new NotImplementedException();
	public override async Task<IList<RouteSchedule>> FindRange( string key, int  count ) => await Azure.Client.RequestRouteLookup( NewRouteLookup( PreFetch.PREFETCH.FIND_RANGE, key ) );
	public override async Task<IList<RouteSchedule>> FindMatch( string key, int  count ) => await Azure.Client.RequestRouteLookup( NewRouteLookup( PreFetch.PREFETCH.FIND_MATCH, key ) );

	public override int    CompareSearchKey( string searchKey,   string        k2 )    => string.Compare( searchKey, k2, StringComparison.CurrentCultureIgnoreCase );
	public override int    Compare( string          k1,          string        k2 )    => string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
	public override bool   StartsWith( string       searchValue, string        k2 )    => k2.StartsWith( searchValue, StringComparison.CurrentCultureIgnoreCase );
	public override bool   Filter( string           filter,      RouteSchedule value ) => throw new NotImplementedException();
	public override string Key( RouteSchedule       item )       => item.RouteName;
	public override string Key( string              searchText ) => searchText;
	public override bool   IsEmpty( string          key )        => string.IsNullOrEmpty( key );
	public override string AsString( string         key )        => key;
}