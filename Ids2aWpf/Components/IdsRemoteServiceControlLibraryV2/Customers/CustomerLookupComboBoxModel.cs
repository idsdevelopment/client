﻿#nullable enable

using System.Globalization;
using System.Windows.Data;
using AzureRemoteService;
using IdsControlLibraryV2.Combos;

namespace IdsRemoteServiceControlLibraryV2.Customers;

public class ObjectToCustomerLookupSummaryConvertor : IValueConverter
{
	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is CustomerLookupSummary Summary ? Summary : null;

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class LookupItem : LookupComboBoxModelBase<CustomerLookupSummary, string>.LookupItem
{
	public LookupItem( CustomerLookupSummary item ) : base( item )
	{
	}
}

public class CustomerLookupComboBoxModel : LookupComboBoxModelBase<CustomerLookupSummary, string>
{
	public string CustomerCode
	{
		get { return Get( () => CustomerCode, "" ); }
		set { Set( () => CustomerCode, value ); }
	}

	[Setting]
	public bool SearchingByAccount
	{
		get { return Get( () => SearchingByAccount, true ); }
		set { Set( () => SearchingByAccount, value ); }
	}

	public string SearchByCompanyNameText
	{
		get { return Get( () => SearchByCompanyNameText, "C" ); }
		set { Set( () => SearchByCompanyNameText, value ); }
	}

	public string SearchByAccountText
	{
		get { return Get( () => SearchByAccountText, "A" ); }
		set { Set( () => SearchByAccountText, value ); }
	}

	public string SearchButtonText
	{
		get { return Get( () => SearchButtonText, "" ); }
		set { Set( () => SearchButtonText, value ); }
	}

	public Action? OnCustomerCodeChanges;

	private CustomerLookup NewLookup( PreFetch.PREFETCH fetch, uint count ) => new()
																			   {
																				   Fetch           = fetch,
																				   PreFetchCount   = (int)count,
																				   ByAccountNumber = SearchingByAccount
																			   };

	private CustomerLookup NewLookup( PreFetch.PREFETCH fetch, string key, int count )
	{
		var Temp = NewLookup( fetch, (uint)count );
		Temp.Key = key;
		return Temp;
	}

	[DependsUpon( nameof( CustomerCode ), Delay = 200 )]
	public void WhenCustomerCodeChanges()
	{
		OnCustomerCodeChanges?.Invoke();
	}

	[DependsUpon( nameof( SearchingByAccount ) )]
	public void WhenSearchingByAccountChanges()
	{
		SearchButtonText = SearchingByAccount ? SearchByAccountText : SearchByCompanyNameText;
		SearchText       = "";
	}

	public override async Task<IList<CustomerLookupSummary>> First( uint   count )           => await Azure.Client.RequestCustomerLookup( NewLookup( PreFetch.PREFETCH.FIRST, count ) );
	public override       Task<IList<CustomerLookupSummary>> First( string key, uint count ) => throw new NotImplementedException();
	public override async Task<IList<CustomerLookupSummary>> Last( uint    count )           => await Azure.Client.RequestCustomerLookup( NewLookup( PreFetch.PREFETCH.LAST, count ) );
	public override       Task<IList<CustomerLookupSummary>> Last( string  key, uint count ) => throw new NotImplementedException();

	public override async Task<IList<CustomerLookupSummary>> FindRange( string key, int count ) => await Azure.Client.RequestCustomerLookup( NewLookup( PreFetch.PREFETCH.FIND_RANGE, key, count ) );

	public override async Task<IList<CustomerLookupSummary>> FindMatch( string key, int count ) => await Azure.Client.RequestCustomerLookup( NewLookup( PreFetch.PREFETCH.FIND_MATCH, key, count ) );

	public override string AsString( string           key )                                      => key;
	public override int    CompareSearchKey( string   k1,          string                k2 )    => string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
	public override int    Compare( string            k1,          string                k2 )    => string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
	public override bool   StartsWith( string         searchValue, string                k2 )    => k2.StartsWith( searchValue, StringComparison.CurrentCultureIgnoreCase );
	public override bool   Filter( string             filter,      CustomerLookupSummary value ) => throw new NotImplementedException();
	public override string Key( CustomerLookupSummary item )       => SearchingByAccount ? item.CustomerCode : item.CompanyName;
	public override string Key( string                searchText ) => searchText;
	public override bool   IsEmpty( string            key )        => string.IsNullOrEmpty( key );
}