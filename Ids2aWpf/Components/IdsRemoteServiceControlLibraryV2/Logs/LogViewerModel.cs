﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Protocol.Data;
using Utils;
using XamlViewModel.Wpf;

namespace IdsRemoteServiceControlLibraryV2.Logs;

public class DisplayEntry : LogEntry, INotifyPropertyChanged
{
	public bool ShowAdLocalTime
	{
		get => _ShowAdLocalTime;
		set
		{
			_ShowAdLocalTime = value;
			OnPropertyChanged( nameof( DisplayTimestamp ) );
		}
	}

	public string DisplayTimestamp
	{
		get
		{
			var Time = _ShowAdLocalTime ? Timestamp : Timestamp.AddHours( -TimeZoneInfo.Local.GetUtcOffset( Timestamp ).Hours ).AddHours( TimeZoneOffset );

			return $"{Time:g}";
		}
	}


	private bool _ShowAdLocalTime = true;

	private readonly LOG Log;


	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public DisplayEntry( LOG l, LogEntry e ) : base( e )
	{
		Log = l;
	}

	public event PropertyChangedEventHandler PropertyChanged;

#region User
	private string _User;

	public string User
	{
		get
		{
			const string SEP = "--";

			if( _User.IsNullOrWhiteSpace() )
			{
				switch( Log )
				{
				case LOG.TRIP:
					var Pos = RowKey.IndexOf( SEP, StringComparison.Ordinal );

					if( Pos >= 0 )
						_User = RowKey.Substring( Pos + SEP.Length ).Trim();

					break;

				default:
					_User = PartitionKey;

					break;
				}
			}

			return _User;
		}
	}
#endregion
}

public class LogViewerModel : ViewModelBase
{
	public ObservableCollection<DisplayEntry> Items
	{
		get { return Get( () => Items, new ObservableCollection<DisplayEntry>() ); }
		set { Set( () => Items, value ); }
	}

	[Setting]
	public bool ShowAsLocalTime
	{
		get { return Get( () => ShowAsLocalTime, true ); }
		set { Set( () => ShowAsLocalTime, value ); }
	}

	public LOG Log
	{
		get { return Get( () => Log, LOG.LOGIN ); }
		set { Set( () => Log, value ); }
	}


	public DateTime FromDate
	{
		get { return Get( () => FromDate, DateTime.Now ); }
		set { Set( () => FromDate, value ); }
	}


	public DateTime ToDate
	{
		get { return Get( () => ToDate, DateTime.Now ); }
		set { Set( () => ToDate, value ); }
	}


	public string Key
	{
		get { return Get( () => Key, "" ); }
		set { Set( () => Key, value ); }
	}


	public bool SearchButtonEnabled
	{
		get { return Get( () => SearchButtonEnabled, true ); }
		set { Set( () => SearchButtonEnabled, value ); }
	}


	public bool NothingFound
	{
		get { return Get( () => NothingFound, false ); }
		set { Set( () => NothingFound, value ); }
	}


	public Func<string, string>       FormatData      = data => data;
	public Func<LogLookup, LogLookup> RequestOverride = lookup => lookup;

	[DependsUpon( nameof( ShowAsLocalTime ) )]
	public void WhenShowAsLocalTimeChanges()
	{
		var Local = ShowAsLocalTime;

		foreach( var Item in Items )
			Item.ShowAdLocalTime = Local;
	}


	public void Execute_SortByUser()
	{
	}

	public void Execute_SortByTimeStamp()
	{
	}

	public void Execute_SortByOperation()
	{
	}

	public void Execute_SortByDescription()
	{
	}

	public void Execute_Search()
	{
		if( !IsInDesignMode )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   SearchButtonEnabled = false;
			                   } );

			var Lookup = new LogLookup
			             {
				             Log      = Log,
				             FromDate = FromDate,
				             ToDate   = ToDate,
				             Key      = Key
			             };

			Task.Run( async () =>
			          {
				          try
				          {
					          var Lg       = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );
					          var LogItems = new ObservableCollection<DisplayEntry>();

					          foreach( var Entry in Lg )
					          {
						          Entry.Data = FormatData( Entry.Data );
						          var L = Log;
						          LogItems.Add( new DisplayEntry( L, Entry ) );
					          }

					          Dispatcher.Invoke( () =>
					                             {
						                             NothingFound = LogItems.Count == 0;

						                             Items               = LogItems;
						                             SearchButtonEnabled = true;
					                             } );
				          }
				          catch( Exception Exception )
				          {
					          Console.WriteLine( Exception );
				          }
			          } );
		}
	}
}