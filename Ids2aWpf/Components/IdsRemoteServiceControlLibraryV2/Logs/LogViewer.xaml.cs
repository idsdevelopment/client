﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Logs;

/// <summary>
///     Interaction logic for LogViewer.xaml
/// </summary>
public partial class LogViewer : UserControl
{
	private LogViewerModel Model;

	public void GetLog()
	{
		Model?.Execute_Search();
	}

	public LogViewer()
	{
		Initialized += ( sender, args ) =>
		               {
			               if( Root.DataContext is LogViewerModel M )
				               Model = M;
		               };

		InitializeComponent();
	}

	private static void ContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LogViewer Lv )
		{
			Lv.ContentPresenter.Content = e.NewValue;

			if( e.NewValue is FrameworkElement F )
				F.DataContext = Lv.DataContext;
		}
	}

	private static void EnableSearchPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LogViewer Lv && e.NewValue is bool Enable )
			Lv.SearchButton.IsEnabled = Enable;
	}


	// Using a DependencyProperty as the backing store for Log.  This enables animation, styling, binding, etc...
	private static void LogChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LogViewer Lv && e.NewValue is LOG L )
			Lv.Model.Log = L;
	}


	private static void KeyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LogViewer Lv && e.NewValue is string U )
			Lv.Model.Key = U;
	}

	[Category( "Common" )]
	public new object Content
	{
		get => GetValue( ContentProperty );
		set => SetValue( ContentProperty, value );
	}

	// Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty ContentProperty =
		DependencyProperty.Register( nameof( Content ), typeof( object ), typeof( LogViewer ), new FrameworkPropertyMetadata( null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ContentPropertyChangedCallback ) );


	[Category( "Options" )]
	public bool EnableSearch
	{
		get => (bool)GetValue( EnableSearchProperty );
		set => SetValue( EnableSearchProperty, value );
	}

	// Using a DependencyProperty as the backing store for EnableSearch.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty EnableSearchProperty =
		DependencyProperty.Register( nameof( EnableSearch ), typeof( bool ), typeof( LogViewer ), new FrameworkPropertyMetadata( true, EnableSearchPropertyChangedCallback ) );


	[Category( "Options" )]
	public string Key
	{
		get => (string)GetValue( KeyProperty );
		set => SetValue( KeyProperty, value );
	}


	public static readonly DependencyProperty KeyProperty =
		DependencyProperty.Register( nameof( Key ), typeof( string ), typeof( LogViewer ), new FrameworkPropertyMetadata( "", KeyChangedCallback ) );


	[Category( "Options" )]
	public string KeyHeaderText
	{
		get => (string)GetValue( KeyHeaderTextProperty );
		set => SetValue( KeyHeaderTextProperty, value );
	}

	// Using a DependencyProperty as the backing store for KeyDisplayText.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty KeyHeaderTextProperty =
		DependencyProperty.Register( nameof( KeyHeaderText ), typeof( string ), typeof( LogViewer ), new FrameworkPropertyMetadata( "Key" ) );


	[Category( "Options" )]
	public LOG Log
	{
		get => (LOG)GetValue( LogProperty );
		set => SetValue( LogProperty, value );
	}

	public static readonly DependencyProperty LogProperty =
		DependencyProperty.Register( nameof( Log ), typeof( LOG ), typeof( LogViewer ), new FrameworkPropertyMetadata( LOG.LOGIN, LogChangedCallback ) );


	public string NothingFoundText
	{
		get => (string)GetValue( NothingFoundTextProperty );
		set => SetValue( NothingFoundTextProperty, value );
	}

	// Using a DependencyProperty as the backing store for NothingFoundText.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty NothingFoundTextProperty =
		DependencyProperty.Register( nameof( NothingFoundText ), typeof( string ), typeof( LogViewer ), new FrameworkPropertyMetadata( "Nothing found." ) );


	[Category( "Options" )]
	public bool ShowDateSearch
	{
		get => (bool)GetValue( ShowDateSearchProperty );
		set => SetValue( ShowDateSearchProperty, value );
	}

	// Using a DependencyProperty as the backing store for ShowDateSearch.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ShowDateSearchProperty =
		DependencyProperty.Register( nameof( ShowDateSearch ), typeof( bool ), typeof( LogViewer ), new FrameworkPropertyMetadata( true ) );
}