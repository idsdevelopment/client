﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IdsRemoteServiceControlLibraryV2.Roles;
using Utils;

namespace IdsRemoteServiceControlLibraryV2;

public class Role
{
	public string Name = "";
	public bool   Selected;
}

public partial class RoleListBox : UserControl
{
	private RoleListBoxModel Model;

	public event OnSelectionChangedEvent SelectionChanged;

	public void Refresh()
	{
		if( Model.IsNotNull() )
			Model.Refresh();
	}

	public RoleListBox()
	{
		InitializeComponent();
	}

	public delegate void OnSelectionChangedEvent( object sender, IList<Protocol.Data.Role> selectedItems, bool @checked );


	private void CheckBox_Checked( object sender, RoutedEventArgs e )
	{
		SelectionChanged?.Invoke( this, SelectedRoles, true );
	}

	private void CheckBox_Unchecked( object sender, RoutedEventArgs e )
	{
		SelectionChanged?.Invoke( this, SelectedRoles, false );
	}

	private void UserControl_Loaded( object sender, RoutedEventArgs e )
	{
		if( RolesListBox.DataContext is RoleListBoxModel M )
		{
			M.RoleListBox = this;
			Model         = M;
		}
	}

	private static void ChangedRolePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is RoleListBox Rlb && Rlb.Model.IsNotNull() )
		{
			Rlb.SelectedRoles = ( from R in Rlb.Model.Items
			                      where R.Checked
			                      select (Protocol.Data.Role)R ).ToList();
		}
	}

	private static void PropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is RoleListBox Rlb && Rlb.Model.IsNotNull() && e.NewValue is string StaffId )
			Rlb.Model.StaffId = StaffId;
	}


	[Category( "Options" )]
	public Role ChangedRole
	{
		get => (Role)GetValue( ChangedRoleProperty );
		set => SetValue( ChangedRoleProperty, value );
	}

	// Using a DependencyProperty as the backing store for ChangedRole.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ChangedRoleProperty =
		DependencyProperty.Register( nameof( ChangedRole ), typeof( Role ), typeof( RoleListBox ), new FrameworkPropertyMetadata( new Role(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ChangedRolePropertyChangedCallback ) );

	[Category( "Options" )]
	public IList<Protocol.Data.Role> SelectedRoles
	{
		get => (IList<Protocol.Data.Role>)GetValue( SelectedRolesProperty );
		set => SetValue( SelectedRolesProperty, value );
	}

	// Using a DependencyProperty as the backing store for SelectedRoles.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedRolesProperty =
		DependencyProperty.Register( nameof( SelectedRoles ), typeof( IList<Protocol.Data.Role> ), typeof( RoleListBox ), new FrameworkPropertyMetadata( new List<Protocol.Data.Role>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );


	[Category( "Options" )]
	public string StaffId
	{
		get => (string)GetValue( StaffIdProperty );
		set => SetValue( StaffIdProperty, value );
	}

	// Using a DependencyProperty as the backing store for StaffId.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty StaffIdProperty =
		DependencyProperty.Register( nameof( StaffId ), typeof( string ), typeof( RoleListBox ), new FrameworkPropertyMetadata( "", PropertyChangedCallback ) );
}