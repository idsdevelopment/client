﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Utils;
using XamlViewModel.Wpf;

namespace IdsRemoteServiceControlLibraryV2.Roles;

public class RoleEntry : Protocol.Data.Role, INotifyPropertyChanged
{
	public bool Checked
	{
		get => _Checked;
		set
		{
			_Checked = value;
			Owner.DoRoleChanged( this );
			OnPropertyChanged();
		}
	}

	private readonly RoleListBoxModel Owner;
	private          bool             _Checked;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	internal RoleEntry( RoleListBoxModel owner )
	{
		Owner = owner;
	}

	public event PropertyChangedEventHandler PropertyChanged;
}

public class RoleListBoxModel : ViewModelBase
{
	public ObservableCollection<RoleEntry> Items
	{
		get
		{
			lock( LockObject )
				return Get( () => Items, new ObservableCollection<RoleEntry>() );
		}
		private set
		{
			lock( LockObject )
				Set( () => Items, value );
		}
	}


	public string StaffId
	{
		get { return Get( () => StaffId, "" ); }
		set { Set( () => StaffId, value ); }
	}

	private readonly object LockObject = new();

	internal bool IgnoreEvents;

	public OnRoleChangedEvent OnRoleChanged;

	internal RoleListBox RoleListBox;

	internal void DoRoleChanged( RoleEntry role )
	{
		if( !IgnoreEvents && RoleListBox.IsNotNull() )
		{
			RoleListBox.ChangedRole = new Role
			                          {
				                          Name     = role.Name,
				                          Selected = role.Checked
			                          };

			OnRoleChanged?.Invoke( role.Name, role.Checked );
		}
	}

	[DependsUpon250( nameof( StaffId ) )]
	public void WhenStaffIdChanges()
	{
		if( !IsInDesignMode )
		{
			Task.Run( () =>
			          {
				          GetRoles();
			          } );
		}
	}

	public new void Refresh()
	{
		if( !IsInDesignMode )
		{
			IgnoreEvents = true;

			Task.Run( () =>
			          {
				          GetRoles( () =>
				                    {
					                    IgnoreEvents = false;
				                    } );
			          } );
		}
	}

	public RoleListBoxModel()
	{
		if( !IsInDesignMode )
		{
			Task.Run( async () =>
			          {
				          IgnoreEvents = true;

				          try
				          {
					          var Roles = await Azure.Client.RequestRoles();

					          if( Roles != null )
					          {
						          var Rls = new ObservableCollection<RoleEntry>();

						          foreach( var Role in from R in Roles
						                               orderby R.Name
						                               select R )
						          {
							          Rls.Add( new RoleEntry( this )
							                   {
								                   Mandatory = Role.Mandatory,
								                   Name      = Role.Name
							                   } );
						          }

						          Items = Rls;
					          }
				          }
				          finally
				          {
					          IgnoreEvents = false;
				          }
			          } );
		}
	}

	public delegate void OnRoleChangedEvent( string roleName, bool selected );


	private async void GetRoles( Action callback = null )
	{
		//var Roles = await Azure.Client.RequestStaffMemberRoles( StaffId );
		var Roles = new Protocol.Data.Roles();

		if( StaffId.IsNotNullOrWhiteSpace() )
			Roles = await Azure.Client.RequestStaffMemberRoles( StaffId );

		Dispatcher.Invoke( () =>
		                   {
			                   var Itms = Items;

			                   foreach( var RoleEntry in Itms )
			                   {
				                   var Checked = false;

				                   foreach( var Role in Roles )
				                   {
					                   if( Role.Name == RoleEntry.Name )
					                   {
						                   Checked = true;
						                   break;
					                   }
				                   }

				                   RoleEntry.Checked = Checked;
			                   }

			                   callback?.Invoke();
		                   } );
	}
}