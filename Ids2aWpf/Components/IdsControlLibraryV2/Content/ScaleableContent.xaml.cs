﻿using System.ComponentModel;
using System.Windows.Media;

namespace IdsControlLibraryV2.Content;

/// <summary>
///     Interaction logic for ScaleableContent.xaml
/// </summary>
public partial class ScaleableContent : UserControl
{
	public ScaleableContent()
	{
		InitializeComponent();
	}

	private void DoScale()
	{
		if( ContentControl != null )
		{
			var S = DisplayScaleSlider.Value;
			ContentControl.LayoutTransform = new ScaleTransform( S, S );
		}
	}

	private void DisplayScaleSlider_ValueChanged( object sender, RoutedPropertyChangedEventArgs<double> e )
	{
		DoScale();
	}

	private static void ScalePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ScaleableContent Sc && e.NewValue is double Scale )
			Sc.DisplayScaleSlider.Value = Scale;
	}

	private static void ContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ScaleableContent Sc )
			Sc.ContentControl.Content = e.NewValue;
	}

	private void UserControl_Initialized( object sender, EventArgs e )
	{
		DoScale();
	}

	[Category( "Common" )]
	public new object Content
	{
		get => GetValue( ContentProperty );
		set => SetValue( ContentProperty, value );
	}

	public static readonly DependencyProperty ScaleProperty =
		DependencyProperty.Register( nameof( Scale ), typeof( double ), typeof( ScaleableContent ), new FrameworkPropertyMetadata( 1.0, FrameworkPropertyMetadataOptions.AffectsArrange, ScalePropertyChangedCallback ) );

	public static readonly DependencyProperty SliderBackgroundProperty =
		DependencyProperty.Register( nameof( SliderBackground ), typeof( Brush ), typeof( ScaleableContent ), new FrameworkPropertyMetadata( Brushes.LightBlue ) );

	public new static readonly DependencyProperty ContentProperty =
		DependencyProperty.Register( nameof( Content ), typeof( object ), typeof( ScaleableContent ), new FrameworkPropertyMetadata( null, FrameworkPropertyMetadataOptions.AffectsRender, ContentPropertyChangedCallback ) );

	[Category( "Appearance" )]
	public double Scale
	{
		get => (double)GetValue( ScaleProperty );
		set => SetValue( ScaleProperty, value );
	}

	// Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...


	[Category( "Brush" )]
	public Brush SliderBackground
	{
		get => (Brush)GetValue( SliderBackgroundProperty );
		set => SetValue( SliderBackgroundProperty, value );
	}


	// Using a DependencyProperty as the backing store for SliderBackground.  This enables animation, styling, binding, etc...
}