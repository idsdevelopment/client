﻿using System.ComponentModel;
using System.Timers;
using System.Windows.Input;
using System.Windows.Media;

// ReSharper disable LocalVariableHidesMember

namespace IdsControlLibraryV2.Window;

/// <summary>
///     Interaction logic for UniversalFormsStyleWindow.xaml
/// </summary>
public partial class UniversalFormsStyleWindow
{
	private Brush                 SaveBrush;
	private System.Windows.Window ParentWindow;
	private Timer                 MouseDragTimer, DoubleClickTimer;

	private enum RESIZE_HANDLE
	{
		NONE,
		LEFT,
		TOP_LEFT,
		TOP,
		TOP_RIGHT,
		RIGHT,
		BOTTOM_RIGHT,
		BOTTOM,
		BOTTOM_LEFT
	}

	// Window Coordinates
	private Point CurrentPosition;
	private bool  AllowResize, InMouseMove;

	private RESIZE_HANDLE ResizeHandle = RESIZE_HANDLE.NONE;

	private PresentationSource PresentationSource;

	public event Action Maximise;
	public event Action Minimise;

	public event PositionChangedEvent PositionChanged;

	public void CancelDragMove()
	{
		if( MouseDragTimer != null )
		{
			var Mdt = MouseDragTimer;
			MouseDragTimer = null;
			Mdt.Stop();
			Mdt.Dispose();
		}
	}

	public void SendMouseDown( MouseEventArgs e )
	{
		MouseButton Mb;

		if( e.LeftButton == MouseButtonState.Pressed )
			Mb = MouseButton.Left;
		else if( e.RightButton == MouseButtonState.Pressed )
			Mb = MouseButton.Right;
		else
			Mb = 0;

		TitlePanel_MouseDown( this, new MouseButtonEventArgs( e.MouseDevice, e.Timestamp, Mb ) );
	}

	protected virtual void OnPositionChanged( Point windowposition, Point mouseposition )
	{
		PositionChanged?.Invoke( windowposition, mouseposition );
	}

	public UniversalFormsStyleWindow()
	{
		InitializeComponent();
	}

	public delegate void PositionChangedEvent( Point windowPosition, Point mousePosition );

	private static void ShowMinimisePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleWindow Window && e.NewValue is bool Enable )
			Window.MinimisePanel.Visibility = Enable ? Visibility.Visible : Visibility.Collapsed;
	}

	private static void ShowMaximisePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleWindow Window && e.NewValue is bool Enable )
			Window.MaximisePanel.Visibility = Enable ? Visibility.Visible : Visibility.Collapsed;
	}

	private static void DisableClosePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleWindow Window && e.NewValue is bool Disable )
			Window.CloseLabel.Opacity = Disable ? 0.2 : 1;
	}

	private static void ResizeBorderPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleWindow Window && e.NewValue is Brush Brush )
			Window.UiResizeBorder.BorderBrush = Brush;
	}

	private static void IsToolBarVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleWindow Window && e.NewValue is bool Visible )
			Window.ToolBar.Visibility = Visible ? Visibility.Visible : Visibility.Collapsed;
	}

	private static void ContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleWindow Window )
			Window.ContentControl.Content = e.NewValue;
	}

	private void CloseLabel_MouseEnter( object sender, MouseEventArgs e )
	{
		e.Handled             = true;
		CloseLabel.Background = DisableClose ? CloseDisableHover : SystemCloseHover;
	}

	private void CloseLabel_MouseLeave( object sender, MouseEventArgs e )
	{
		e.Handled             = true;
		CloseLabel.Background = Brushes.Transparent;
	}

	private void MaximisePanel_MouseEnter( object sender, MouseEventArgs e )
	{
		SaveBrush                = MaximisePanel.Background;
		MaximisePanel.Background = new SolidColorBrush( Colors.White ) {Opacity = 0.4};
	}

	private void MaximisePanel_MouseLeave( object sender, MouseEventArgs e )
	{
		MaximisePanel.Background = SaveBrush;
	}

	private void MinimisePanel_MouseEnter( object sender, MouseEventArgs e )
	{
		SaveBrush                = MinimisePanel.Background;
		MinimisePanel.Background = new SolidColorBrush( Colors.White ) {Opacity = 0.4};
	}

	private void MinimisePanel_MouseLeave( object sender, MouseEventArgs e )
	{
		MinimisePanel.Background = SaveBrush;
	}

	private void MinimiseLabel_MouseDown( object sender, MouseButtonEventArgs e )
	{
		ParentWindow.WindowState = WindowState.Minimized;
		Minimise?.Invoke();
	}

	private void UserControl_Loaded( object sender, RoutedEventArgs e )
	{
		UiResizeBorder.BorderBrush = ResizeBorder;

		ParentWindow = System.Windows.Window.GetWindow( this );

		if( ParentWindow != null )
		{
			ParentWindow.WindowStyle = WindowStyle.None;
			ParentWindow.ResizeMode  = ResizeMode.CanResizeWithGrip;
			ParentWindow.Background  = Brushes.Transparent;

			ParentWindow.LocationChanged += ( o, args ) =>
			                                {
				                                var MousePos = ParentWindow.PointToScreen( Mouse.GetPosition( ParentWindow ) );
				                                OnPositionChanged( new Point( ParentWindow.Left, ParentWindow.Top ), MousePos );
			                                };
		}
	}

	private void CloseLabel_MouseDown( object sender, MouseButtonEventArgs e )
	{
		if( !DisableClose )
		{
			if( CanCloseApplication )
				Application.Current.Shutdown();
			else
				ParentWindow.Close();
		}
	}

	private void MaximiseLabel_MouseDown( object sender, MouseButtonEventArgs e )
	{
		ParentWindow.WindowState = ParentWindow.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
		Maximise?.Invoke();
	}

	private void ResizeBorder_MouseEnter( object sender, MouseEventArgs e )
	{
		const int HIT_AREA = 5;

		CurrentPosition = PointToScreen( e.GetPosition( this ) );
		var Pos = e.GetPosition( UiResizeBorder );

		var X = Pos.X;
		var Y = Pos.Y;

		var Height = UiResizeBorder.ActualHeight - ( HIT_AREA * 2 );
		var Width  = UiResizeBorder.ActualWidth - ( HIT_AREA * 2 );

		if( X < HIT_AREA ) // Left
		{
			if( Y < HIT_AREA ) // Top Left
			{
				UiResizeBorder.Cursor = Cursors.SizeNWSE;
				ResizeHandle          = RESIZE_HANDLE.TOP_LEFT;
			}
			else if( Y > Height ) // Bottom Left
			{
				UiResizeBorder.Cursor = Cursors.SizeNESW;
				ResizeHandle          = RESIZE_HANDLE.BOTTOM_LEFT;
			}
			else
			{
				UiResizeBorder.Cursor = Cursors.SizeWE; // Left
				ResizeHandle          = RESIZE_HANDLE.LEFT;
			}
		}
		else if( X > Width ) // Right
		{
			if( Y < HIT_AREA ) // Top Right
			{
				UiResizeBorder.Cursor = Cursors.SizeNESW;
				ResizeHandle          = RESIZE_HANDLE.TOP_RIGHT;
			}
			else if( Y > Height )
			{
				UiResizeBorder.Cursor = Cursors.SizeNWSE; // Bottom Right
				ResizeHandle          = RESIZE_HANDLE.BOTTOM_RIGHT;
			}
			else
			{
				UiResizeBorder.Cursor = Cursors.SizeWE; // Right
				ResizeHandle          = RESIZE_HANDLE.RIGHT;
			}
		}
		else if( Y < HIT_AREA )
		{
			UiResizeBorder.Cursor = Cursors.SizeNS; // Top
			ResizeHandle          = RESIZE_HANDLE.TOP;
		}
		else if( Y > Height )
		{
			UiResizeBorder.Cursor = Cursors.SizeNS; // Bottom
			ResizeHandle          = RESIZE_HANDLE.BOTTOM;
		}
		else if( ResizeHandle != RESIZE_HANDLE.NONE )
		{
			UiResizeBorder.Cursor = Cursors.Arrow;
			ResizeHandle          = RESIZE_HANDLE.NONE;
			Mouse.Capture( null );
			AllowResize = false;
		}
	}

	private void ResizeBorder_MouseLeave( object sender, MouseEventArgs e )
	{
		if( !AllowResize && ( ResizeHandle != RESIZE_HANDLE.NONE ) )
		{
			UiResizeBorder.Cursor = Cursors.Arrow;
			ResizeHandle          = RESIZE_HANDLE.NONE;
			Mouse.Capture( null );
			AllowResize = false;
		}
	}

	private Point FixPosition( Point p )
	{
		if( PresentationSource == null )
			PresentationSource = PresentationSource.FromVisual( this );

		if( PresentationSource?.CompositionTarget != null )
			p = PresentationSource.CompositionTarget.TransformFromDevice.Transform( p );
		return p;
	}

	private void ResizeBorder_MouseMove( object sender, MouseEventArgs e )
	{
		if( !InMouseMove )
		{
			InMouseMove = true;

			try
			{
				if( AllowResize && ( ResizeHandle != RESIZE_HANDLE.NONE ) )
				{
					if( sender is Border Border && Equals( Border, UiResizeBorder ) )
					{
						var PrevPos = CurrentPosition;
						CurrentPosition = FixPosition( PointToScreen( e.GetPosition( ParentWindow ) ) );

						switch( ResizeHandle )
						{
						case RESIZE_HANDLE.LEFT:
						case RESIZE_HANDLE.TOP_LEFT:
							try
							{
								var Diff = CurrentPosition.X - PrevPos.X;
								ParentWindow.Left  =  CurrentPosition.X;
								ParentWindow.Width -= Diff;

								if( ResizeHandle == RESIZE_HANDLE.TOP_LEFT )
									goto case RESIZE_HANDLE.TOP;
							}
							catch
							{
							}

							break;

						case RESIZE_HANDLE.TOP:
							try
							{
								var Diff = CurrentPosition.Y - PrevPos.Y;
								ParentWindow.Top    =  CurrentPosition.Y;
								ParentWindow.Height -= Diff;
							}
							catch
							{
							}

							break;

						case RESIZE_HANDLE.RIGHT:
						case RESIZE_HANDLE.TOP_RIGHT:
							try
							{
								var Diff = CurrentPosition.X - PrevPos.X;
								ParentWindow.Width += Diff;

								if( ResizeHandle == RESIZE_HANDLE.TOP_RIGHT )
									goto case RESIZE_HANDLE.TOP;
							}
							catch
							{
							}

							break;

						case RESIZE_HANDLE.BOTTOM:
						case RESIZE_HANDLE.BOTTOM_RIGHT:
						case RESIZE_HANDLE.BOTTOM_LEFT:
							try
							{
								var Diff = CurrentPosition.Y - PrevPos.Y;
								ParentWindow.Height += Diff;

								if( ResizeHandle == RESIZE_HANDLE.BOTTOM_RIGHT )
									goto case RESIZE_HANDLE.RIGHT;

								if( ResizeHandle == RESIZE_HANDLE.BOTTOM_LEFT )
									goto case RESIZE_HANDLE.LEFT;
							}
							catch
							{
							}

							break;
						}
					}
				}
				else
					ResizeBorder_MouseEnter( sender, e );
			}
			finally
			{
				InMouseMove = false;
			}
		}
	}

	private void ResizeBorder_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
	{
		if( ResizeHandle != RESIZE_HANDLE.NONE )
		{
			CurrentPosition = FixPosition( PointToScreen( e.GetPosition( ParentWindow ) ) );

			AllowResize = true;
			Mouse.Capture( UiResizeBorder );
		}
	}

	private void ResizeBorder_MouseLeftButtonUp( object sender, MouseButtonEventArgs e )
	{
		if( ( e.LeftButton == MouseButtonState.Released ) && ( ResizeHandle != RESIZE_HANDLE.NONE ) )
		{
			AllowResize = false;
			Mouse.Capture( null );
			ResizeBorder_MouseLeave( sender, null );
		}
	}

	private void TitlePanel_MouseUp( object sender, MouseButtonEventArgs e )
	{
		CancelDragMove();
	}

	private void TitlePanel_MouseDown( object sender, MouseButtonEventArgs e )
	{
		try
		{
			if( e.ChangedButton == MouseButton.Left )
			{
				if( DoubleClickTimer == null )
				{
					DoubleClickTimer = new Timer( 300 );

					DoubleClickTimer.Elapsed += ( o, args ) =>
					                            {
						                            var Tmr = DoubleClickTimer;
						                            DoubleClickTimer = null;
						                            Tmr.Stop();
						                            Tmr.Dispose();
					                            };
					DoubleClickTimer.Start();
				}
				else
				{
					ParentWindow.WindowState = ParentWindow.WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
					return;
				}

				if( MouseDragTimer == null )
				{
					MouseDragTimer = new Timer( 50 );

					MouseDragTimer.Elapsed += ( o, args ) =>
					                          {
						                          CancelDragMove();

						                          try
						                          {
							                          Dispatcher.Invoke( () =>
							                                             {
								                                             try
								                                             {
									                                             ParentWindow.DragMove();
								                                             }
								                                             catch
								                                             {
								                                             }
							                                             } );
						                          }
						                          catch // Task canceled exception when closing
						                          {
						                          }
					                          };
					MouseDragTimer.Start();
				}
			}
		}
		catch
		{
		}
	}

	[Category( "Options" )]
	public bool CanCloseApplication
	{
		get => (bool)GetValue( CanCloseApplicationProperty );
		set => SetValue( CanCloseApplicationProperty, value );
	}

	// Using a DependencyProperty as the backing store for CanCloseApplication.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty CanCloseApplicationProperty =
		DependencyProperty.Register( nameof( CanCloseApplication ), typeof( bool ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( true ) );

	[Category( "Brush" )]
	public Brush CloseDisableHover
	{
		get => (Brush)GetValue( CloseDisableHoverProperty );
		set => SetValue( CloseDisableHoverProperty, value );
	}

	// Using a DependencyProperty as the backing store for CloseDisableHover.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty CloseDisableHoverProperty =
		DependencyProperty.Register( nameof( CloseDisableHover ), typeof( Brush ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( Brushes.DimGray ) );

	[Category( "Common" )]
	public new object Content
	{
		get => GetValue( ContentProperty );
		set => SetValue( ContentProperty, value );
	}

	public static readonly DependencyProperty SystemCloseHoverProperty =
		DependencyProperty.Register( nameof( SystemCloseHover ), typeof( Brush ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( Brushes.Red, FrameworkPropertyMetadataOptions.AffectsRender ) );

	public static readonly DependencyProperty ShowMinimiseProperty =
		DependencyProperty.Register( nameof( ShowMinimise ), typeof( bool ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( true, FrameworkPropertyMetadataOptions.Inherits, ShowMinimisePropertyChangedCallback ) );

	public static readonly DependencyProperty ShowMaximiseProperty =
		DependencyProperty.Register( nameof( ShowMaximise ), typeof( bool ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( true, FrameworkPropertyMetadataOptions.Inherits, ShowMaximisePropertyChangedCallback ) );

	public static readonly DependencyProperty DisableCloseProperty =
		DependencyProperty.Register( nameof( DisableClose ), typeof( bool ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.AffectsRender, DisableClosePropertyChangedCallback ) );

	public static readonly DependencyProperty SystemIconsBackgroundProperty =
		DependencyProperty.Register( nameof( SystemIconsBackground ), typeof( Brush ), typeof( UniversalFormsStyleWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LightBlue ) );

	public static readonly DependencyProperty SystemIconsForegroundProperty =
		DependencyProperty.Register( nameof( SystemIconsForeground ), typeof( Brush ), typeof( UniversalFormsStyleWindow ),
		                             new FrameworkPropertyMetadata( Brushes.White ) );

	public static readonly DependencyProperty TitleBarBackgroundProperty =
		DependencyProperty.Register( nameof( TitleBarBackground ), typeof( Brush ), typeof( UniversalFormsStyleWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LightBlue ) );

	public static readonly DependencyProperty TitleBarContentProperty =
		DependencyProperty.Register( nameof( TitleBarContent ), typeof( object ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( null ) );

	public static readonly DependencyProperty ResizeBorderProperty =
		DependencyProperty.Register( nameof( ResizeBorder ), typeof( Brush ), typeof( UniversalFormsStyleWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LawnGreen, FrameworkPropertyMetadataOptions.Inherits, ResizeBorderPropertyChangedCallback ) );

	public static readonly DependencyProperty IconImageSourceProperty =
		DependencyProperty.Register( nameof( IconImageSource ), typeof( ImageSource ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( null ) );

	public static readonly DependencyProperty IsToolBarVisibleProperty =
		DependencyProperty.Register( nameof( IsToolBarVisible ), typeof( bool ), typeof( UniversalFormsStyleWindow ),
		                             new FrameworkPropertyMetadata( true, FrameworkPropertyMetadataOptions.AffectsArrange, IsToolBarVisiblePropertyChangedCallback ) );

	public new static readonly DependencyProperty ContentProperty =
		DependencyProperty.Register( nameof( Content ), typeof( object ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( null, FrameworkPropertyMetadataOptions.AffectsArrange, ContentPropertyChangedCallback ) );

	[Category( "Options" )]
	public bool DisableClose
	{
		get => (bool)GetValue( DisableCloseProperty );
		set => SetValue( DisableCloseProperty, value );
	}


	// Using a DependencyProperty as the backing store for DisableClose.  This enables animation, styling, binding, etc...


	[Category( "Appearance" )]
	public ImageSource IconImageSource
	{
		get => (ImageSource)GetValue( IconImageSourceProperty );
		set => SetValue( IconImageSourceProperty, value );
	}


	// Using a DependencyProperty as the backing store for IconImageSource.  This enables animation, styling, binding, etc...


	[Category( "Appearance" )]
	public bool IsToolBarVisible
	{
		get => (bool)GetValue( IsToolBarVisibleProperty );
		set => SetValue( IsToolBarVisibleProperty, value );
	}


	// Using a DependencyProperty as the backing store for IsToolBarVisible.  This enables animation, styling, binding, etc...


	[Category( "Brush" )]
	public Brush ResizeBorder
	{
		get => (Brush)GetValue( ResizeBorderProperty );
		set => SetValue( ResizeBorderProperty, value );
	}


	// Using a DependencyProperty as the backing store for ResizeBorder.  This enables animation, styling, binding, etc...


	[Category( "Options" )]
	public bool ShowMaximise
	{
		get => (bool)GetValue( ShowMaximiseProperty );
		set => SetValue( ShowMaximiseProperty, value );
	}


	// Using a DependencyProperty as the backing store for ShowMinimise.  This enables animation, styling, binding, etc...


	[Category( "Options" )]
	public bool ShowMinimise
	{
		get => (bool)GetValue( ShowMinimiseProperty );
		set => SetValue( ShowMinimiseProperty, value );
	}


	// Using a DependencyProperty as the backing store for ShowMinimise.  This enables animation, styling, binding, etc...


	[Category( "Brush" )]
	public Brush SystemCloseHover
	{
		get => (Brush)GetValue( SystemCloseHoverProperty );
		set => SetValue( SystemCloseHoverProperty, value );
	}

	// Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for SystemCloseHover.  This enables animation, styling, binding, etc...


	[Category( "Brush" )]
	public Brush SystemIconsBackground
	{
		get => (Brush)GetValue( SystemIconsBackgroundProperty );
		set => SetValue( SystemIconsBackgroundProperty, value );
	}


	// Using a DependencyProperty as the backing store for SystemIconsBackground.  This enables animation, styling, binding, etc...


	[Category( "Brush" )]
	public Brush SystemIconsForeground
	{
		get => (Brush)GetValue( SystemIconsForegroundProperty );
		set => SetValue( SystemIconsForegroundProperty, value );
	}


	// Using a DependencyProperty as the backing store for SystemIconsForeground.  This enables animation, styling, binding, etc...


	[Category( "Brush" )]
	public Brush TitleBarBackground
	{
		get => (Brush)GetValue( TitleBarBackgroundProperty );
		set => SetValue( TitleBarBackgroundProperty, value );
	}


	// Using a DependencyProperty as the backing store for TitleBarBackground.  This enables animation, styling, binding, etc...


	[Category( "Common" )]
	public object TitleBarContent
	{
		get => GetValue( TitleBarContentProperty );
		set => SetValue( TitleBarContentProperty, value );
	}


	// Using a DependencyProperty as the backing store for TitleBarContent.  This enables animation, styling, binding, etc...


	[Category( "Common" )]
	public object ToolBarContent
	{
		get => GetValue( ToolBarContentProperty );
		set => SetValue( ToolBarContentProperty, value );
	}

	// Using a DependencyProperty as the backing store for ToolBarContent.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ToolBarContentProperty =
		DependencyProperty.Register( nameof( ToolBarContent ), typeof( object ), typeof( UniversalFormsStyleWindow ), new FrameworkPropertyMetadata( null ) );
}