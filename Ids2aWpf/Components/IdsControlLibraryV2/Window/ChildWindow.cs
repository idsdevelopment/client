﻿using System.ComponentModel;
using System.Windows.Media;

namespace IdsControlLibraryV2.Window;

public class ChildWindow : System.Windows.Window
{
	public Point ScreenPosition => new() {X = _ScreenPosition.X, Y = _ScreenPosition.Y};

	public double ScreenTop  => ScreenPosition.Y;
	public double ScreenLeft => ScreenPosition.X;

	private PresentationSource PresentationSource => _PresentationSource ?? ( _PresentationSource = PresentationSource.FromVisual( this ) );

	private PresentationSource _PresentationSource;

	private Point _ScreenPosition = new( -1, -1 );

	private bool AlreadyLoaded;

	private         bool InAdjustWindow;
	public readonly bool IsDesignMode;

	public new UIElement Owner;

	public ChildWindow()
	{
		var Current      = Application.Current;
		var ParentWindow = Current.MainWindow;

		if( !( ParentWindow is null ) )
		{
			Loaded += ( sender, args ) =>
			          {
				          if( !AlreadyLoaded )
				          {
					          AlreadyLoaded         =  true;
					          Current.SessionEnding += CurrentOnSessionEnding;

					          ParentWindow.LocationChanged += ParentWindowOnLocationChanged;
					          ParentWindow.LayoutUpdated   += ParentWindowOnLayoutUpdated;
					          ParentWindow.Closing         += ParentWindowOnClosed;
				          }
			          };

			Closed += ( sender, args ) =>
			          {
				          Current.SessionEnding -= CurrentOnSessionEnding;

				          ParentWindow.LocationChanged -= ParentWindowOnLocationChanged;
				          ParentWindow.LayoutUpdated   -= ParentWindowOnLayoutUpdated;
				          ParentWindow.Closing         -= ParentWindowOnClosed;
			          };
		}

		IsDesignMode = DesignerProperties.GetIsInDesignMode( this );

		WindowStyle        = WindowStyle.None;
		AllowsTransparency = true;
		ShowInTaskbar      = false;
		ResizeMode         = ResizeMode.NoResize;
		Background         = Brushes.Transparent;
	}

	private void CurrentOnSessionEnding( object sender, SessionEndingCancelEventArgs e )
	{
		Close();
	}

	private void ParentWindowOnClosed( object sender, EventArgs e )
	{
		Close();
	}

	private void AdjustWindow()
	{
		if( !InAdjustWindow && IsVisible )
		{
			try
			{
				InAdjustWindow = true;

				if( PresentationSource?.CompositionTarget != null )
				{
					var Pos       = ( Owner ?? this ).PointToScreen( new Point( 0, 0 ) );
					var ScreenPos = PresentationSource.CompositionTarget.TransformFromDevice.Transform( Pos );
					_ScreenPosition = ScreenPos;

					Left = ScreenPos.X + OffsetLeft;
					Top  = ScreenPos.Y + OffsetTop;
				}
			}
			catch // Probably not connected to Presentation Source
			{
			}
			finally
			{
				InAdjustWindow = false;
			}
		}
	}

	private void ParentWindowOnLocationChanged( object sender, EventArgs e )
	{
		AdjustWindow();
	}

	private void ParentWindowOnLayoutUpdated( object sender, EventArgs e )
	{
		AdjustWindow();
	}


	private static void OffsetPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ChildWindow Window )
			Window.AdjustWindow();
	}

	[Category( "Layout" )]
	public double OffsetLeft
	{
		get => (double)GetValue( OffsetLeftProperty );
		set => SetValue( OffsetLeftProperty, value );
	}

	public static readonly DependencyProperty OffsetTopProperty =
		DependencyProperty.Register( nameof( OffsetTop ), typeof( double ), typeof( ChildWindow ), new FrameworkPropertyMetadata( 0.0, OffsetPropertyChangedCallback ) );

	public static readonly DependencyProperty OffsetLeftProperty =
		DependencyProperty.Register( nameof( OffsetLeft ), typeof( double ), typeof( ChildWindow ), new FrameworkPropertyMetadata( 0.0, OffsetPropertyChangedCallback ) );


	[Category( "Layout" )]
	public double OffsetTop
	{
		get => (double)GetValue( OffsetTopProperty );
		set => SetValue( OffsetTopProperty, value );
	}
}