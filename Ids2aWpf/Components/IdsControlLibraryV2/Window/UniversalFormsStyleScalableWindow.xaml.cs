﻿using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Content;

namespace IdsControlLibraryV2.Window;

/// <summary>
///     Interaction logic for UniversalFormsStyleScalableWindow.xaml
/// </summary>
public partial class UniversalFormsStyleScalableWindow : UserControl
{
	[Category( "Options" )]
	public bool CanCloseApplication
	{
		get => UWindow.CanCloseApplication;
		set => UWindow.CanCloseApplication = value;
	}

	private readonly ScaleableContent ScaleableContent;

	public void SendMouseDown( MouseEventArgs e )
	{
		UWindow.SendMouseDown( e );
	}


	public event UniversalFormsStyleWindow.PositionChangedEvent PositionChanged
	{
		add => UWindow.PositionChanged += value;
		remove => UWindow.PositionChanged -= value;
	}

	public UniversalFormsStyleScalableWindow()
	{
		InitializeComponent();

		if( base.Content is UniversalFormsStyleWindow UniversalFormsStyleWindow )
			ScaleableContent = UniversalFormsStyleWindow.Content as ScaleableContent;
	}


	// Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...


	private static void ContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleScalableWindow Sw )
			Sw.ScaleableContent.Content = e.NewValue;
	}


	// Using a DependencyProperty as the backing store for CloseDisableHover.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...


	private static void ScalePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleScalableWindow Sw && e.NewValue is double Scale )
			Sw.ScaleableContent.Scale = Scale;
	}


	// Using a DependencyProperty as the backing store for SliderBackground.  This enables animation, styling, binding, etc...


	private static void SliderBackgroundPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is UniversalFormsStyleScalableWindow Sw && e.NewValue is Brush Brush )
			Sw.ScaleableContent.SliderBackground = Brush;
	}

	[Category( "Brush" )]
	public Brush CloseDisableHover
	{
		get => (Brush)GetValue( CloseDisableHoverProperty );
		set => SetValue( CloseDisableHoverProperty, value );
	}

	public static readonly DependencyProperty ScaleProperty =
		DependencyProperty.Register( nameof( Scale ), typeof( double ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( 1.0, ScalePropertyChangedCallback ) );

	public static readonly DependencyProperty SliderBackgroundProperty =
		DependencyProperty.Register( nameof( SliderBackground ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LightBlue, SliderBackgroundPropertyChangedCallback ) );

	public new static readonly DependencyProperty ContentProperty =
		DependencyProperty.Register( nameof( Content ), typeof( object ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( null, ContentPropertyChangedCallback ) );

	public static readonly DependencyProperty CloseDisableHoverProperty =
		DependencyProperty.Register( nameof( CloseDisableHover ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( Brushes.DimGray ) );

	[Category( "Common" )]
	public new object Content
	{
		get => GetValue( ContentProperty );
		set => SetValue( ContentProperty, value );
	}


	[Category( "Options" )]
	public bool DisableClose
	{
		get => (bool)GetValue( DisableCloseProperty );
		set => SetValue( DisableCloseProperty, value );
	}

	// Using a DependencyProperty as the backing store for DisableClose.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DisableCloseProperty =
		DependencyProperty.Register( nameof( DisableClose ), typeof( bool ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( false ) );


	[Category( "Appearance" )]
	public ImageSource IconImageSource
	{
		get => (ImageSource)GetValue( IconImageSourceProperty );
		set => SetValue( IconImageSourceProperty, value );
	}

	// Using a DependencyProperty as the backing store for IconImageSource.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty IconImageSourceProperty =
		DependencyProperty.Register( nameof( IconImageSource ), typeof( ImageSource ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( null ) );

	[Category( "Appearance" )]
	public bool IsToolBarVisible
	{
		get => (bool)GetValue( IsToolBarVisibleProperty );
		set => SetValue( IsToolBarVisibleProperty, value );
	}

	// Using a DependencyProperty as the backing store for IsToolBarVisible.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty IsToolBarVisibleProperty =
		DependencyProperty.Register( nameof( IsToolBarVisible ), typeof( bool ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( true ) );


	[Category( "Brush" )]
	public Brush ResizeBorder
	{
		get => (Brush)GetValue( ResizeBorderProperty );
		set => SetValue( ResizeBorderProperty, value );
	}

	// Using a DependencyProperty as the backing store for ResizeBorder.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ResizeBorderProperty =
		DependencyProperty.Register( nameof( ResizeBorder ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LawnGreen, FrameworkPropertyMetadataOptions.Inherits ) );

	[Category( "Appearance" )]
	public double Scale
	{
		get => (double)GetValue( ScaleProperty );
		set => SetValue( ScaleProperty, value );
	}


	[Category( "Brush" )]
	public Brush SliderBackground
	{
		get => (Brush)GetValue( SliderBackgroundProperty );
		set => SetValue( SliderBackgroundProperty, value );
	}


	[Category( "Brush" )]
	public Brush SystemCloseHover
	{
		get => (Brush)GetValue( SystemCloseHoverProperty );
		set => SetValue( SystemCloseHoverProperty, value );
	}

	// Using a DependencyProperty as the backing store for SystemCloseHover.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SystemCloseHoverProperty =
		DependencyProperty.Register( nameof( SystemCloseHover ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( Brushes.Red ) );

	[Category( "Brush" )]
	public Brush SystemIconsBackground
	{
		get => (Brush)GetValue( SystemIconsBackgroundProperty );
		set => SetValue( SystemIconsBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for SystemIconsBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SystemIconsBackgroundProperty =
		DependencyProperty.Register( nameof( SystemIconsBackground ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LightBlue ) );

	[Category( "Brush" )]
	public Brush SystemIconsForeground
	{
		get => (Brush)GetValue( SystemIconsForegroundProperty );
		set => SetValue( SystemIconsForegroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for SystemIconsForeground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SystemIconsForegroundProperty =
		DependencyProperty.Register( nameof( SystemIconsForeground ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ),
		                             new FrameworkPropertyMetadata( Brushes.White ) );

	[Category( "Brush" )]
	public Brush TitleBarBackground
	{
		get => (Brush)GetValue( TitleBarBackgroundProperty );
		set => SetValue( TitleBarBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for TitleBarBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleBarBackgroundProperty =
		DependencyProperty.Register( nameof( TitleBarBackground ), typeof( Brush ), typeof( UniversalFormsStyleScalableWindow ),
		                             new FrameworkPropertyMetadata( Brushes.LightBlue ) );

	[Category( "Common" )]
	public object TitleBarContent
	{
		get => GetValue( TitleBarContentProperty );
		set => SetValue( TitleBarContentProperty, value );
	}

	// Using a DependencyProperty as the backing store for TitleBarContent.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TitleBarContentProperty =
		DependencyProperty.Register( nameof( TitleBarContent ), typeof( object ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( null ) );

	[Category( "Common" )]
	public object ToolBarContent
	{
		get => GetValue( ToolBarContentProperty );
		set => SetValue( ToolBarContentProperty, value );
	}

	// Using a DependencyProperty as the backing store for ToolBarContent.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ToolBarContentProperty =
		DependencyProperty.Register( nameof( ToolBarContent ), typeof( object ), typeof( UniversalFormsStyleScalableWindow ), new FrameworkPropertyMetadata( null ) );
}