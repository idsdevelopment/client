﻿using System.ComponentModel;
using System.Windows.Media;

namespace IdsControlLibraryV2.Window;

/// <summary>
///     Interaction logic for ChildWindowSave.xaml
/// </summary>
public partial class ChildWindowSave : UserControl
{
	public Point ScreenPosition => new() {X = _ScreenPosition.X, Y = _ScreenPosition.Y};

	public double Top  => Window?.Top ?? -1;
	public double Left => Window?.Left ?? -1;

	public double ActualTop  => Window != null ? Window.Top + OffsetTop : -1;
	public double ActualLeft => Window != null ? Window.Left + OffsetLeft : -1;

	public double ScreenTop  => ScreenPosition.Y;
	public double ScreenLeft => ScreenPosition.X;

	private PresentationSource    PresentationSource => _PresentationSource ?? ( _PresentationSource = PresentationSource.FromVisual( Window ) );
	private System.Windows.Window ParentWindow, Window;
	private PresentationSource    _PresentationSource;

	private Point _ScreenPosition = new( -1, -1 );

	private         bool InAdjustWindow;
	public readonly bool IsDesignMode;

	public static explicit operator System.Windows.Window( ChildWindowSave cw ) => cw.Window;

	public bool Activate() => Window?.Activate() ?? false;

	public ChildWindowSave()
	{
		IsDesignMode = DesignerProperties.GetIsInDesignMode( this );
		InitializeComponent();
		base.Height = Height;
		base.Width  = Width;

		Background = Brushes.Transparent;
	}

	private void UserControl_Loaded( object sender, RoutedEventArgs e )
	{
		if( !IsDesignMode && ( Window == null ) )
		{
			Window = new System.Windows.Window
			         {
				         WindowStyle        = WindowStyle.None,
				         AllowsTransparency = true,
				         Background         = Background,
				         Foreground         = Foreground,
				         Visibility         = Visibility,
				         Opacity            = Opacity,
				         ShowInTaskbar      = false,
				         ResizeMode         = ResizeMode.NoResize,
				         SizeToContent      = SizeToContent,
				         Content            = Content,
				         DataContext        = this
			         };

			ParentWindow = System.Windows.Window.GetWindow( this );

			Window.Owner = ParentWindow;

			if( ParentWindow != null )
			{
				ParentWindow.LocationChanged += ParentWindowOnLocationChanged;
				ParentWindow.LayoutUpdated   += ParentWindowOnLayoutUpdated;
			}
		}
	}

	private void AdjustWindow()
	{
		if( !InAdjustWindow )
		{
			try
			{
				InAdjustWindow = true;

				if( PresentationSource?.CompositionTarget != null )
				{
					var Pos       = PointToScreen( new Point( 0, 0 ) );
					var ScreenPos = PresentationSource.CompositionTarget.TransformFromDevice.Transform( Pos );
					_ScreenPosition = ScreenPos;

					Window.Left = ScreenPos.X + OffsetLeft;
					Window.Top  = ScreenPos.Y + OffsetTop;

					var Sc = SizeToContent;

					switch( Sc )
					{
					case SizeToContent.Manual:
						Window.Height = Height;
						Window.Width  = Width;
						break;

					case SizeToContent.Height:
						Window.Width = Width;
						break;

					case SizeToContent.Width:
						Window.Height = Height;
						break;
					}
					Window.SizeToContent = Sc; // Changes on resize
				}
			}
			catch // Probably not connected to Presentation Source
			{
			}
			finally
			{
				InAdjustWindow = false;
			}
		}
	}

	private static void OffsetPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ChildWindowSave Window )
			Window.AdjustWindow();
	}

	private static void SizeToContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ChildWindowSave Window && e.NewValue is SizeToContent Size )
			Window.SizeToContent = Size;
	}

	private void DoBaseVisibility( Visibility vis )
	{
		if( Window != null )
			Window.Visibility = vis;
		base.Visibility = vis;
	}

	private static object CoerceValueCallback( DependencyObject d, object basevalue )
	{
		if( d is ChildWindowSave && basevalue is double Opacity )
		{
			if( Opacity < 0 )
				Opacity = 0;
			else if( Opacity > 1 )
				Opacity = 1;
			return Opacity;
		}
		return basevalue;
	}

	private void DoBaseOpacity( double opacity )
	{
		if( Window != null )
			Window.Opacity = opacity;
		base.Opacity = opacity;
	}

	private static void OpacityPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ChildWindowSave Window && e.NewValue is double Opacity )
			Window.DoBaseOpacity( Opacity );
	}

	private static void VisibilityPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is ChildWindowSave Window && e.NewValue is Visibility Visibility )
			Window.DoBaseVisibility( Visibility );
	}

	private void ParentWindowOnLocationChanged( object sender, EventArgs e )
	{
		AdjustWindow();
	}

	private void UserControl_SizeChanged( object sender, SizeChangedEventArgs e )
	{
		AdjustWindow();
	}

	private void ParentWindowOnLayoutUpdated( object sender, EventArgs e )
	{
		AdjustWindow();
	}

	[Category( "Layout" )]
	public new double Height
	{
		get => (double)GetValue( HeightProperty );
		set => SetValue( HeightProperty, value );
	}

	// Using a DependencyProperty as the backing store for Height.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty HeightProperty =
		DependencyProperty.Register( nameof( Height ), typeof( double ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( double.NaN, OffsetPropertyChangedCallback ) );

	[Category( "Layout" )]
	public double OffsetLeft
	{
		get => (double)GetValue( OffsetLeftProperty );
		set => SetValue( OffsetLeftProperty, value );
	}

	public new static readonly DependencyProperty WidthProperty =
		DependencyProperty.Register( nameof( Width ), typeof( double ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( double.NaN, OffsetPropertyChangedCallback ) );

	public static readonly DependencyProperty OffsetTopProperty =
		DependencyProperty.Register( nameof( OffsetTop ), typeof( double ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( 0.0, OffsetPropertyChangedCallback ) );

	public static readonly DependencyProperty OffsetLeftProperty =
		DependencyProperty.Register( nameof( OffsetLeft ), typeof( double ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( 0.0, OffsetPropertyChangedCallback ) );

	[Category( "Layout" )]
	public double OffsetTop
	{
		get => (double)GetValue( OffsetTopProperty );
		set => SetValue( OffsetTopProperty, value );
	}


	// Using a DependencyProperty as the backing store for OffsetTop.  This enables animation, styling, binding, etc...


	[Category( "Appearance" )]
	public new double Opacity
	{
		get => (double)GetValue( OpacityProperty );
		set => SetValue( OpacityProperty, value );
	}

	public static readonly DependencyProperty SizeToContentProperty =
		DependencyProperty.Register( nameof( SizeToContent ), typeof( SizeToContent ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( SizeToContent.Manual, SizeToContentPropertyChangedCallback ) );

	public new static readonly DependencyProperty OpacityProperty =
		DependencyProperty.Register( nameof( Opacity ), typeof( double ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( 1.0, OpacityPropertyChangedCallback, CoerceValueCallback ) );

	[Category( "Options" )]
	public SizeToContent SizeToContent
	{
		get => (SizeToContent)GetValue( SizeToContentProperty );
		set => SetValue( SizeToContentProperty, value );
	}

	// Using a DependencyProperty as the backing store for Opacity.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for SizeToContent.  This enables animation, styling, binding, etc...


	[Category( "Appearance" )]
	public new Visibility Visibility
	{
		get => (Visibility)GetValue( VisibilityProperty );
		set => SetValue( VisibilityProperty, value );
	}

	// Using a DependencyProperty as the backing store for Visibility.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty VisibilityProperty =
		DependencyProperty.Register( nameof( Visibility ), typeof( Visibility ), typeof( ChildWindowSave ), new FrameworkPropertyMetadata( Visibility.Visible, VisibilityPropertyChangedCallback ) );

	[Category( "Layout" )]
	public new double Width
	{
		get => (double)GetValue( WidthProperty );
		set => SetValue( WidthProperty, value );
	}

	// Using a DependencyProperty as the backing store for OffsetLeft.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for Width.  This enables animation, styling, binding, etc...
}