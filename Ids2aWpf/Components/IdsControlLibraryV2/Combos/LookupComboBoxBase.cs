﻿#nullable enable

using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using IdsControlLibraryV2.Input;
using IdsControlLibraryV2.Utils;
using IdsControlLibraryV2.Window;
using Timer = System.Timers.Timer;

namespace IdsControlLibraryV2.Combos;

public static class ItemsControlExtensions
{
	private static bool TryScrollToCenterOfView( this ItemsControl itemsControl, object item )
	{
		// Find the container
		if( itemsControl.ItemContainerGenerator.ContainerFromItem( item ) is not UIElement Container )
			return false;

		// Find the ScrollContentPresenter
		ScrollContentPresenter? Presenter = null;

		for( Visual? Vis = Container; ( Vis != null ) && ( Vis != itemsControl ); Vis = VisualTreeHelper.GetParent( Vis ) as Visual )
		{
			if( ( Presenter = Vis as ScrollContentPresenter ) is not null )
				break;
		}

		if( Presenter == null )
			return false;

		// Find the IScrollInfo
		var ScrollInfo = !Presenter.CanContentScroll ? Presenter :
							 Presenter.Content as IScrollInfo ??
							 FirstVisualChild( Presenter.Content as ItemsPresenter ) as IScrollInfo ??
							 Presenter;

		// Compute the center point of the container relative to the scrollInfo
		var Size   = Container.RenderSize;
		var Center = Container.TransformToAncestor( (Visual)ScrollInfo ).Transform( new Point( Size.Width / 2, Size.Height / 2 ) );
		Center.Y += ScrollInfo.VerticalOffset;
		Center.X += ScrollInfo.HorizontalOffset;

		// Adjust for logical scrolling
		if( ScrollInfo is StackPanel or VirtualizingStackPanel )
		{
			var LogicalCenter = itemsControl.ItemContainerGenerator.IndexFromContainer( Container ) + 0.5;
			var Orientation   = ScrollInfo is StackPanel Panel ? Panel.Orientation : ( (VirtualizingStackPanel)ScrollInfo ).Orientation;

			if( Orientation == Orientation.Horizontal )
				Center.X = LogicalCenter;
			else
				Center.Y = LogicalCenter;
		}

		// Scroll the center of the container to the center of the viewport
		if( ScrollInfo.CanVerticallyScroll )
			ScrollInfo.SetVerticalOffset( CenteringOffset( Center.Y, ScrollInfo.ViewportHeight, ScrollInfo.ExtentHeight ) );

		if( ScrollInfo.CanHorizontallyScroll )
			ScrollInfo.SetHorizontalOffset( 0 );

		//ScrollInfo.SetHorizontalOffset(CenteringOffset(Center.X, ScrollInfo.ViewportWidth, ScrollInfo.ExtentWidth));
		return true;
	}

	private static double CenteringOffset( double center, double viewport, double extent ) => Math.Min( extent - viewport, Math.Max( 0, center - ( viewport / 2 ) ) );

	private static DependencyObject? FirstVisualChild( DependencyObject? visual )
	{
		if( visual is null )
			return null;

		return VisualTreeHelper.GetChildrenCount( visual ) == 0
				   ? null
				   : VisualTreeHelper.GetChild( visual, 0 );
	}

	public static T? GetDescendantByType<T>( this Visual? element ) where T : class
	{
		if( element == null )
			return default;

		if( element.GetType() == typeof( T ) )
			return element as T;

		T? FoundElement = null;

		if( element is FrameworkElement Element )
			Element.ApplyTemplate();

		for( var I = 0; I < VisualTreeHelper.GetChildrenCount( element ); I++ )
		{
			var Visual = VisualTreeHelper.GetChild( element, I ) as Visual;
			FoundElement = Visual.GetDescendantByType<T>();

			if( FoundElement is not null )
				break;
		}
		return FoundElement;
	}

	public static void ScrollToCenterOfView( this ItemsControl itemsControl, object item )
	{
		// Scroll immediately if possible
		if( !itemsControl.TryScrollToCenterOfView( item ) )
		{
			// Otherwise wait until everything is loaded, then scroll
			if( itemsControl is ListBox ItemsControl )
				ItemsControl.ScrollIntoView( item );

			itemsControl.Dispatcher.BeginInvoke( DispatcherPriority.Loaded, new Action( () =>
																				        {
																					        itemsControl.TryScrollToCenterOfView( item );
																				        } ) );
		}
	}
}

public class BoolToComboSelectedBackgroundConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is bool Selected && parameter is LookupComboBoxBase Combo )
			return Selected ? Combo.SelectedItemBackground : Combo.Background;

		return Brushes.Red;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class LookupComboBoxBase : Border
{
	private const int    DEFAULT_SEARCH_INTERVAL = 150;
	private const double DEFAULT_TEXT_WIDTH      = 100;

	private bool ListViewHasFocus => CurrentListView is not null && ( CurrentListView.IsFocused || CurrentListView.IsKeyboardFocusWithin );

	private ListView? CurrentListView
	{
		get => _CurrentListView;
		set
		{
			_CurrentListView = value;

			if( ViewModel is not null )
				ViewModel.ListView = value;
		}
	}

	~LookupComboBoxBase()
	{
		ListVieWindow.Visibility        =  Visibility.Collapsed;
		ListVieWindow.LostKeyboardFocus -= OnLostFocus;
		ListVieWindow.PreviewMouseWheel -= ListViewWindowOnPreviewMouseWheel;

		KeyUp     -= OnKeyUp;
		LostFocus -= OnLostFocus;
		GotFocus  -= OnGotFocus;
		Loaded    -= OnLoaded;
		SearchTimer.Dispose();
	}

	public LookupComboBoxBase()
	{
		var W = ListVieWindow = new ChildWindow
								{
									Top           = 0,
									Left          = 0,
									SizeToContent = SizeToContent.WidthAndHeight,
									Background    = Brushes.Red,
									Padding       = new Thickness(),
									Owner         = this
								};

		W.LostKeyboardFocus += OnLostFocus;
		W.PreviewMouseWheel += ListViewWindowOnPreviewMouseWheel;

		LostFocus += OnLostFocus;
		GotFocus  += OnGotFocus;
		Loaded    += OnLoaded;

		BorderThickness = new Thickness( 1, 1, 1, 1 );
		Padding         = new Thickness();

		HorizontalAlignment = HorizontalAlignment.Left;

		SearchTimer.Interval  = DEFAULT_SEARCH_INTERVAL;
		SearchTimer.AutoReset = false;

		SearchTimer.Elapsed += ( _, _ ) =>
							   {
								   if( !StoppingSearchTimer )
								   {
									   Dispatcher.Invoke( () =>
													      {
														      var Txt = Input?.Text ?? "";

														      if( ViewModel is not null )
															      ViewModel.SearchText = Txt;

														      if( !DontOpenDropdown )
														      {
															      if( AutoDropDown && !IsDropDownVisible )
																      IsDropDownVisible = true;
														      }
														      else
															      DontOpenDropdown = false;
													      } );
								   }
							   };

		var Inp = Input = new TextBox
						  {
							  MinWidth        = DEFAULT_TEXT_WIDTH,
							  BorderThickness = new Thickness()
						  };

		Inp.GotFocus += ( sender, _ ) =>
						{
							if( sender is TextBox Tb )
							{
								Timer = new System.Threading.Timer( _ =>
																    {
																	    Dispatcher.Invoke( () =>
																					       {
																						       Tb.SelectionStart  = 0;
																						       Tb.SelectionLength = Tb.Text.Length;
																					       } );
																	    // ReSharper disable once AccessToModifiedClosure
																	    Timer?.Dispose();
																	    Timer = null;
																    }, null, 10, Timeout.Infinite );
							}
						};

		Inp.KeyUp += ( _, _ ) =>
					 {
						 StartSearchTimer();
					 };

		KeyUp += OnKeyUp;

		DropDownButton = new Button
						 {
							 Content             = "˅",
							 Background          = Brushes.Transparent,
							 Margin              = new Thickness(),
							 Padding             = new Thickness( 3, 0, 3, 0 ),
							 BorderThickness     = new Thickness( 1, 0, 1, 0 ),
							 HorizontalAlignment = HorizontalAlignment.Right,
							 Focusable           = false
						 };

		DropDownButton.Click += ( _, args ) =>
								{
									args.Handled = true;
									var Vis = !IsDropDownVisible;

									if( Vis && ViewModel is not null )
										ViewModel.SearchText = Inp.Text;

									IsDropDownVisible = Vis;
								};

		SecondaryButton = new Button
						  {
							  Content             = "",
							  Background          = Brushes.Transparent,
							  Margin              = new Thickness(),
							  Padding             = new Thickness( 2, 0, 2, 0 ),
							  BorderThickness     = new Thickness(),
							  HorizontalAlignment = HorizontalAlignment.Right,
							  Focusable           = false,
							  Visibility          = Visibility.Collapsed
						  };

		SecondaryButton.Click += ( _, args ) =>
								 {
									 args.Handled = true;
									 SecondaryButtonClick?.Invoke();
								 };

		var Dp = new DockPanel();

		var C = Dp.Children;
		DockPanel.SetDock( SecondaryButton, Dock.Right );
		DockPanel.SetDock( DropDownButton, Dock.Right );
		DockPanel.SetDock( Inp, Dock.Left );

		C.Add( SecondaryButton );
		C.Add( DropDownButton );
		C.Add( Inp );

		DockPanel.SetDock( Dp, Dock.Top );

		var Dp1 = new DockPanel();
		Dp1.Children.Add( Dp );

		// ReSharper disable once VirtualMemberCallInConstructor
		Child       = Dp1;
		BorderBrush = Input.BorderBrush;
	}

	private          ListView?   _CurrentListView;
	private          bool        DontOpenDropdown;
	private readonly TextBox     Input;
	private          bool        JustSetText;
	private readonly ChildWindow ListVieWindow;

	private readonly Button SecondaryButton, DropDownButton;

	private ALookupComboBoxModelBase? ViewModel;
	private VirtualizingStackPanel?   VPanel;

	private void OnLoaded( object o, RoutedEventArgs routedEventArgs )
	{
		SetUpWindow();

		if( DataContext is ALookupComboBoxModelBase Model )
		{
			ViewModel = Model;

			Model.WhenSelectedItemChanges = item =>
											{
												ListView.ScrollToCenterOfView( item );
											};

			ViewModel.CollectionChanged = () =>
										  {
											  Dispatcher.Invoke( () =>
															     {
																     if( CurrentListView is not null )
																	     CurrentListView.ItemsSource = ViewModel.GetItems();

																     if( VPanel is not null )
																     {
																	     VPanel = ListView.FindChild<VirtualizingStackPanel>();

																	     if( VPanel is not null )
																	     {
																		     VPanel.FindParent<ScrollViewer>().ScrollChanged += ( _, eventArgs ) =>
																														        {
																															        var VChange = eventArgs.VerticalChange;

																															        if( VChange != 0 )
																																        ViewModel.ScrollPositionChanged( eventArgs.ViewportHeight, eventArgs.VerticalChange, eventArgs.VerticalOffset );
																														        };
																	     }
																     }
															     } );
										  };

			ViewModel.ListView = CurrentListView;

			if( CurrentListView is not null )
			{
				CurrentListView.SelectionChanged += ( _, eventArgs ) =>
													{
														eventArgs.Handled = true;
														var Items = eventArgs.AddedItems;

														if( ( Items.Count > 0 ) && Items[ 0 ] is LookupItemBase Li )
														{
															SelectedItem = Li.GetItem();
															JustSetText  = true;

															try
															{
																Text = ViewModel.ItemKeyAsString( Li );
																ItemSelected?.Invoke( Li );
															}
															finally
															{
																JustSetText = false;
															}
														}
													};
			}
			ViewModel?.WhenSearchTextChanges();
		}
	}

	private void OnGotFocus( object o, RoutedEventArgs routedEventArgs )
	{
		if( ViewModel is not null )
			ViewModel.SearchText = Input.Text;
	}

	private void SetUpWindow()
	{
		ListVieWindow.Visibility = Visibility.Collapsed;
		ListVieWindow.OffsetTop  = ActualHeight - 1; // Border thickness
		ListVieWindow.OffsetLeft = -1;
		ListVieWindow.MinWidth   = ActualWidth;
		ListVieWindow.MaxWidth   = ActualWidth;
		ListVieWindow.UpdateLayout();
	}

	private void OnLostFocus( object sender, RoutedEventArgs e )
	{
		if( Equals( sender, this ) )
		{
			IsDropDownVisible = false;
			return;
		}

		switch( IsDropDownVisible )
		{
		case true when sender is ChildWindow Win:
			Win.Topmost = true;
			Win.Topmost = false;
			break;
		}

		switch( e )
		{
		case KeyboardFocusChangedEventArgs {NewFocus: UIElement NewElement} when !NewElement.IsChildOf( this ):
			IsDropDownVisible = false;
			return;
		}

		if( !IsKeyboardFocusWithin )
			IsDropDownVisible = false;
	}

#region Keyboard
#region Scroll Wheel
	private int TotalDelta;

	private void ListViewWindowOnPreviewMouseWheel( object sender, MouseWheelEventArgs e )
	{
		if( sender is ChildWindow W )
		{
			TotalDelta += e.Delta;

			if( ( Math.Abs( TotalDelta ) >= W.Height ) && ViewModel is { } M )
			{
				if( TotalDelta < 0 ) // Scrolling Down
					M.AppendMore();
			}
		}
	}
#endregion

	private void DoSelection( LookupItemBase? item )
	{
		Dispatcher.Invoke( () =>
						   {
							   if( ViewModel != null )
							   {
								   ViewModel.SetSelectItem( item );

								   if( !ListViewHasFocus )
									   ListView.Focus();
							   }
						   } );
	}

	private async void OnKeyUp( object? sender, KeyEventArgs? args )

	{
		var Key = args?.Key ?? System.Windows.Input.Key.Home;

		void DoSelected( LookupItemBase? item )
		{
			Dispatcher.Invoke( () =>
							   {
								   DoSelection( item );
								   SendKeys.Send( Key );
							   } );
		}

		switch( Key )
		{
		case Key.Home:
			StopSearchTimer();

			ViewModel?.First( async () =>
							  {
								  DoSelected( await ViewModel.GetFirstItem() );
							  } );
			break;

		case Key.End:
			StopSearchTimer();

			ViewModel?.Last( async () =>
						     {
							     DoSelected( await ViewModel.GetLastItem() );
						     } );
			break;

		case Key.PageDown:
		case Key.PageUp:
		case Key.Down:
		case Key.Up:
			break;

		case Key.Enter:
		case Key.Tab:
			if( IsDropDownVisible )
				IsDropDownVisible = false;
			var Txt = Input.Text;

			if( ViewModel is not null )
			{
				var Item = await ViewModel.GetItem( Txt );

				if( Item is not null )
				{
					JustSetText = true;

					try
					{
						Text = Txt;
						ViewModel.SetSelectItem( Item );
						ItemSelected?.Invoke( Item );
					}
					finally
					{
						JustSetText = false;
					}
				}
			}

			return;

		default:
			StartSearchTimer();

			if( !IsDropDownVisible )
				IsDropDownVisible = true;

			return;
		}

		if( args is not null )
			args.Handled = true;
	}
#endregion

#region Timer
	private readonly Timer SearchTimer = new();

	private bool StoppingSearchTimer;

	private void StartSearchTimer()
	{
		StoppingSearchTimer = true;
		SearchTimer.Stop();
		SearchTimer.Start();
		StoppingSearchTimer = false;
	}

	private void StopSearchTimer()
	{
		StoppingSearchTimer = true;
		SearchTimer.Stop();
		StoppingSearchTimer = false;
	}

	private System.Threading.Timer? Timer;
#endregion

#region Events
	public delegate void ItemSelectedEvent( LookupItemBase item );

	public event ItemSelectedEvent? ItemSelected;

	public delegate void SecondaryButtonClickEvent();

	public event SecondaryButtonClickEvent? SecondaryButtonClick;
#endregion

#region IsDropDownVisible
	private static void IsDropDownVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is bool Visible )
		{
			var Win = Lc.ListVieWindow;

			if( Visible )
			{
				// Stop flashing in wrong position
				Win.Height = 0;
				Win.Width  = 0;
				Win.UpdateLayout();

				Lc.SetUpWindow();
				Win.Show();
				Win.Topmost = true;
				Win.Topmost = false;

				var Item = Lc.ViewModel?.GetSelectItem();

				if( Item is not null )
					Lc.ListView.ScrollIntoView( Item );
				Lc.Input.Focus();
			}
			else
				Win.Visibility = Visibility.Collapsed;
		}
	}

	// Using a DependencyProperty as the backing store for IsDropDownVisible.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty IsDropDownVisibleProperty =
		DependencyProperty.Register( nameof( IsDropDownVisible ),
								     typeof( bool ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( false,
															        FrameworkPropertyMetadataOptions.Inherits,
															        IsDropDownVisiblePropertyChangedCallback ) );

	[Category( "Appearance" )]
	public bool IsDropDownVisible
	{
		get => (bool)GetValue( IsDropDownVisibleProperty );
		set => SetValue( IsDropDownVisibleProperty, value );
	}
#endregion

#region ItemsSource
	private static void ItemsSourcePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is IList<LookupItemBase> Items && Lc.CurrentListView is not null )
			Lc.CurrentListView.ItemsSource = Items;
	}

	// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ItemsSourceProperty =
		DependencyProperty.Register( nameof( ItemsSource ),
								     typeof( IList<LookupItemBase> ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( null, ItemsSourcePropertyChangedCallback ) );

	[Category( "Common" )]
	public IList<LookupItemBase> ItemsSource
	{
		get => (IList<LookupItemBase>)GetValue( ItemsSourceProperty );
		set => SetValue( ItemsSourceProperty, value );
	}
#endregion

#region ListView
	private static void ListViewPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is ListView Lv )
		{
			var H = Lc.DropDownHeight;
			Lv.Height                =  H;
			Lv.MaxHeight             =  H;
			Lv.Visibility            =  Visibility.Visible;
			Lv.IsTextSearchEnabled   =  false;
			Lv.LostFocus             += Lc.OnLostFocus;
			Lc.ListVieWindow.Content =  Lv;
			Lc.CurrentListView       =  Lv;
			Lc.IsDropDownVisible     =  false;
		}
	}

	// Using a DependencyProperty as the backing store for ListView.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ListViewProperty =
		DependencyProperty.Register( nameof( ListView ),
								     typeof( ListView ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( null,
															        ListViewPropertyChangedCallback ) );

	[Category( "Common" )]
	public ListView ListView
	{
		get => (ListView)GetValue( ListViewProperty );
		set => SetValue( ListViewProperty, value );
	}
#endregion

#region SearchDelayInMs
	private static void SearchDelayInMsPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is int Delay )
		{
			var Timer = Lc.SearchTimer;
			Timer.Stop();
			Timer.Interval = Delay;
		}
	}

	// Using a DependencyProperty as the backing store for SearchDelayInMs.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SearchDelayInMsProperty =
		DependencyProperty.Register( nameof( SearchDelayInMs ),
								     typeof( int ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( DEFAULT_SEARCH_INTERVAL,
															        SearchDelayInMsPropertyChangedCallback ) );

	[Category( "Options" )]
	public int SearchDelayInMs
	{
		get => (int)GetValue( SearchDelayInMsProperty );
		set => SetValue( SearchDelayInMsProperty, value );
	}
#endregion

#region SecondaryButtonContent
	private static void SecondaryButtonContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc )
			Lc.SecondaryButton.Content = e.NewValue;
	}

	// Using a DependencyProperty as the backing store for SecondaryButtonContent.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SecondaryButtonContentProperty =
		DependencyProperty.Register( nameof( SecondaryButtonContent ),
								     typeof( object ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( default,
															        SecondaryButtonContentPropertyChangedCallback ) );

	[Category( "Appearance" )]
	public object SecondaryButtonContent
	{
		get => GetValue( SecondaryButtonContentProperty );
		set => SetValue( SecondaryButtonContentProperty, value );
	}
#endregion

#region SecondaryButtonVisible
	private static void SecondaryButtonVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is bool Visible )
			Lc.SecondaryButton.Visibility = Visible ? Visibility.Visible : Visibility.Collapsed;
	}

	// Using a DependencyProperty as the backing store for SecondaryButtonVisible.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SecondaryButtonVisibleProperty =
		DependencyProperty.Register( nameof( SecondaryButtonVisible ),
								     typeof( bool ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( false,
															        SecondaryButtonVisiblePropertyChangedCallback ) );

	[Category( "Appearance" )]
	public bool SecondaryButtonVisible
	{
		get => (bool)GetValue( SecondaryButtonVisibleProperty );
		set => SetValue( SecondaryButtonVisibleProperty, value );
	}
#endregion

#region SelectedItem
	// Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedItemProperty =
		DependencyProperty.Register( nameof( SelectedItem ),
								     typeof( object ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( default,
															        FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );

	[Category( "Common" )]
	public object? SelectedItem
	{
		get => GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}
#endregion

#region SelectedItemBackground
	// Using a DependencyProperty as the backing store for SelectedItemBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SelectedItemBackgroundProperty =
		DependencyProperty.Register( nameof( SelectedItemBackground ),
								     typeof( Brush ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( Brushes.LightBlue ) );

	[Category( "Brush" )]
	public Brush SelectedItemBackground
	{
		get => (Brush)GetValue( SelectedItemBackgroundProperty );
		set => SetValue( SelectedItemBackgroundProperty, value );
	}
#endregion

#region Text
	private static void TextPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is string Text )
		{
			Lc.Input.Text = Text;

			if( !Lc.JustSetText )
			{
				Lc.DontOpenDropdown = true;
				var Timer = Lc.SearchTimer;
				Timer.Stop();
				Timer.Start();
			}
		}
	}

	// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TextProperty =
		DependencyProperty.Register( nameof( Text ),
								     typeof( string ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( "",
															        FrameworkPropertyMetadataOptions.Inherits
															        | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
															        TextPropertyChangedCallback ) );

	[Category( "Common" )]
	public string Text
	{
		get => (string)GetValue( TextProperty );
		set => SetValue( TextProperty, value );
	}
#endregion

#region TextWidth
	private static void TextWidthPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is double Width )
			Lc.Input.MinWidth = Width;
	}

	// Using a DependencyProperty as the backing store for TextWidth.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TextWidthProperty =
		DependencyProperty.Register( nameof( TextWidth ),
								     typeof( double ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( DEFAULT_TEXT_WIDTH,
															        FrameworkPropertyMetadataOptions.Inherits,
															        TextWidthPropertyChangedCallback ) );

	[Category( "Appearance" )]
	public double TextWidth
	{
		get => (double)GetValue( TextWidthProperty );
		set => SetValue( TextWidthProperty, value );
	}
#endregion

#region AutoDropDown
	// Using a DependencyProperty as the backing store for AutoDropDown.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty AutoDropDownProperty =
		DependencyProperty.Register( nameof( AutoDropDown ),
								     typeof( bool ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( true ) );

	[Category( "Appearance" )]
	public bool AutoDropDown
	{
		get => (bool)GetValue( AutoDropDownProperty );
		set => SetValue( AutoDropDownProperty, value );
	}
#endregion

#region DropDownHeight
	// Using a DependencyProperty as the backing store for DropDownHeight.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DropDownHeightProperty =
		DependencyProperty.Register( nameof( DropDownHeight ),
								     typeof( double ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( 200.0 ) );

	[Category( "Appearance" )]
	public double DropDownHeight
	{
		get => (double)GetValue( DropDownHeightProperty );
		set => SetValue( DropDownHeightProperty, value );
	}
#endregion

#region Foreground
	private static void ForegroundPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is LookupComboBoxBase Lc && e.NewValue is Brush Brush )
		{
			Lc.Input.Foreground           = Brush;
			Lc.DropDownButton.Foreground  = Brush;
			Lc.SecondaryButton.Foreground = Brush;
		}
	}

	// Using a DependencyProperty as the backing store for Foreground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ForegroundProperty =
		DependencyProperty.Register( nameof( Foreground ),
								     typeof( Brush ),
								     typeof( LookupComboBoxBase ),
								     new FrameworkPropertyMetadata( Brushes.Black,
															        ForegroundPropertyChangedCallback ) );

	[Category( "Brush" )]
	public Brush Foreground
	{
		get => (Brush)GetValue( ForegroundProperty );
		set => SetValue( ForegroundProperty, value );
	}
#endregion
}