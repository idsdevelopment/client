﻿#nullable enable

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using IdsControlLibraryV2.Annotations;
using IdsControlLibraryV2.Virtualisation;

// ReSharper disable UnusedMember.Local

namespace IdsControlLibraryV2.Combos;

public abstract class ALookupComboBoxModelBase : ViewModelBase
{
	private const uint PREFETCH_COUNT = 100;

	public uint PreFetchCount
	{
		get { return Get( () => PreFetchCount, PREFETCH_COUNT ); }
		set { Set( () => PreFetchCount, value ); }
	}

	public string SearchText
	{
		get { return Get( () => SearchText, "" ); }
		set
		{
			Set( () => SearchText, value );
			WhenSearchTextChanges(); // Doesn't want to fire through DependsUpon
		}
	}

	internal Action? CollectionChanged;

	public ListView?               ListView;
	public Action<LookupItemBase>? WhenSelectedItemChanges;

	public abstract void AppendMore();

	public abstract void    WhenSearchTextChanges();
	public abstract void    SetSelectItem( LookupItemBase? item );
	public abstract object? GetSelectItem();

	public abstract Task<LookupItemBase?> GetFirstItem();
	public abstract Task<LookupItemBase?> GetLastItem();
	public abstract Task<LookupItemBase?> GetItem( string key );

	public abstract string ItemKeyAsString( LookupItemBase item );

	public abstract void First( Action? completeCallback = null );
	public abstract void Last( Action?  completeCallback = null );

	public abstract void                         ScrollPositionChanged( double viewportHeight, double scrollChange, double verticalOffset );
	public abstract IEnumerable<LookupItemBase?> GetItems();
}

public abstract class LookupItemBase
{
	public abstract bool Selected { get; set; }

	public uint Index { get; set; }

	public          bool    IsEvenRow => ( Index & 1 ) == 0;
	public          bool    IsOddRow  => ( Index & 1 ) != 0;
	public abstract object? GetItem();
}

public abstract class LookupComboBoxModelBase<T, Tk> : ALookupComboBoxModelBase, IItemsProvider<T, Tk> where T : new()
{
	public class LookupItem : LookupItemBase, INotifyPropertyChanged
	{
		public override bool Selected
		{
			get => _Selected;
			set
			{
				_Selected = value;
				OnPropertyChanged();
			}
		}

		public T Item { get; }

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public LookupItem( T item )
		{
			Item = item;
		}

		private bool _Selected;

		public override object? GetItem() => Item;

		public static implicit operator T( LookupItem i ) => i.Item;

		public event PropertyChangedEventHandler? PropertyChanged;
	}

	public ObservableCollection<LookupItem> DisplayItems
	{
		get { return Get( () => DisplayItems, new ObservableCollection<LookupItem>() ); }
		set { Set( () => DisplayItems, value ); }
	}

	public ObservableCollection<LookupItem> Items
	{
		get { return Get( () => Items, new ObservableCollection<LookupItem>() ); }
		set
		{
			Set( () => Items, value );
			CollectionChanged?.Invoke();
		}
	}

	public T SelectItem
	{
		get { return Get( () => SelectItem, new T() ); }
		set
		{
			lock( LockObject )
			{
				Set( () => SelectItem, value );

				var K = Key( value );

				foreach( var Item in Items )
				{
					if( Compare( Key( Item ), K ) == 0 )
					{
						Dispatcher.InvokeAsync( () =>
											    {
												    foreach( var LookupItem in Items )
													    LookupItem.Selected = false;

												    Item.Selected = true;
												    WhenSelectedItemChanges?.Invoke( Item );
											    } );
						return;
					}
				}
			}
		}
	}

	private enum SEARCH_BY
	{
		NONE,
		FIRST,
		RANGE,
		LAST
	}

	private bool InScrollChange;

	private readonly object LockObject = new();

	private bool ProcessingRequest;

	private void UpdateItems( IList<T> items, bool addToListOnly )
	{
		Dispatcher.Invoke( () =>
						   {
							   lock( LockObject )
							   {
								   var Itms = Items;

								   if( !addToListOnly )
								   {
									   var Remove = ( from CurrentItem in Itms
												      let I = ( from NewItem in items
														        where Compare( Key( NewItem ), Key( CurrentItem ) ) != 0
														        select NewItem ).FirstOrDefault()
												      where I == null
												      select CurrentItem ).ToList();

									   foreach( var Item in Remove )
										   Itms.Remove( Item );
								   }

								   var Add = ( from NewItem in items
										       let I = ( from CurrentItem in Itms
												         where Compare( Key( NewItem ), Key( CurrentItem ) ) == 0
												         select CurrentItem ).FirstOrDefault()
										       where I == null
										       select NewItem ).ToList();

								   var Combined = Itms.ToList();

								   Combined.AddRange( from A in Add
												      select new LookupItem( A ) );

								   uint Index = 0;

								   var Ordered = from Item in Combined
											     orderby AsString( Key( Item.Item ) )
											     select new LookupItem( Item ) {Index = Index++};

								   Items = new ObservableCollection<LookupItem>( Ordered );
							   }
						   } );
	}

	public override IEnumerable<LookupItemBase?> GetItems() => Items;

#region Append
	private Task? AppendTask;

	public override void AppendMore()
	{
		AppendTask ??= Task.Run( async () =>
							     {
								     try
								     {
									     var I       = Items;
									     var ItemKey = Key( I[ I.Count - 1 ] );
									     UpdateItems( await FindRange( ItemKey, (int)PreFetchCount ), true );
								     }
								     finally
								     {
									     AppendTask = null;
								     }
							     } );
	}
#endregion

	public override void ScrollPositionChanged( double viewportHeight, double scrollChange, double verticalOffset )
	{
		if( !InScrollChange )
		{
			var ViewportHeight = (int)viewportHeight;
			var ScrollChange   = (int)scrollChange;
			var VerticalOffset = (int)verticalOffset;

			var ItemsCount = Items.Count;

			if( ItemsCount <= 0 )
				return;

			if( VerticalOffset > ItemsCount )
				VerticalOffset = ItemsCount - 1;

			InScrollChange = true;
			var ItemKey = Key( Items[ VerticalOffset ] );

			var Backwards = ScrollChange < 0;

			var FetchCount = Backwards ? -PreFetchCount : PreFetchCount;

			// Top or end of list
			if( ( ( VerticalOffset <= ViewportHeight ) && Backwards ) || ( ( VerticalOffset >= ( ItemsCount - VerticalOffset ) ) && !Backwards ) )
			{
				Task.Run( async () =>
						  {
							  try
							  {
								  UpdateItems( await FindRange( ItemKey, (int)FetchCount ), true );
							  }
							  finally
							  {
								  InScrollChange = false;
							  }
						  } );
			}
		}
	}

	public override void First( Action? completeCallback = null )
	{
		Task.Run( async () =>
				  {
					  UpdateItems( await First( PreFetchCount ), false );
					  completeCallback?.Invoke();
				  } );
	}

	public override void Last( Action? completeCallback = null )
	{
		Task.Run( async () =>
				  {
					  UpdateItems( await Last( PreFetchCount ), false );
					  completeCallback?.Invoke();
				  } );
	}

	public override async Task<LookupItemBase?> GetFirstItem()
	{
		return await Task.Run( async () =>
						       {
							       while( ProcessingRequest )
								       await Task.Delay( 1 );

							       return Items.Count > 0 ? Items[ 0 ] : null;
						       } );
	}

	public override async Task<LookupItemBase?> GetLastItem()
	{
		return await Task.Run( async () =>
						       {
							       while( ProcessingRequest )
								       await Task.Delay( 1 );

							       var C = Items.Count;

							       return C > 0 ? Items[ C - 1 ] : null;
						       } );
	}

	public override async Task<LookupItemBase?> GetItem( string key )
	{
		return await Task.Run( async () =>
						       {
							       while( ProcessingRequest )
								       await Task.Delay( 1 );

							       var SearchKey = Key( key );

							       foreach( var Item in Items )
							       {
								       if( Compare( SearchKey, Key( Item ) ) == 0 )
									       return Item;
							       }

							       var Result = await FindMatch( SearchKey, 1 );

							       return Result.Count > 0 ? new LookupItem( Result[ 0 ] ) : null;
						       } );
	}

	public override string ItemKeyAsString( LookupItemBase item ) => AsString( Key( (LookupItem)item ) );

	public override void SetSelectItem( LookupItemBase? item )
	{
		if( item is LookupItem Item )
			SelectItem = Item;
	}

	// Keep in sync with Azure (May have been modified)
	[DependsUpon( nameof( SelectItem ) )]
	public void WhenSelectedItemChanged()
	{
		Task.Run( async () =>
				  {
					  var Ndx    = 0;
					  var RNdx   = -1;
					  var SelKey = Key( SelectItem );
					  var SKey   = AsString( SelKey );

					  foreach( var Item in Items )
					  {
						  if( Item is not null )
						  {
							  var Key = AsString( this.Key( Item ) );

							  if( SKey == Key )
							  {
								  RNdx = Ndx;
								  break;
							  }
						  }

						  Ndx++;
					  }

					  var MatchItem = await FindMatch( SelKey, 1 );

					  if( ( RNdx != -1 ) && ( RNdx < Items.Count ) )
					  {
						  Dispatcher.Invoke( () =>
										     {
											     Items.RemoveAt( RNdx );
										     } );
					  }

					  UpdateItems( MatchItem, true );
				  } );
	}

	public override object? GetSelectItem() => SelectItem;

	public override async void WhenSearchTextChanges()
	{
		var Search = SearchText;

		try
		{
			SEARCH_BY DoSelected()
			{
				var Result = SEARCH_BY.RANGE;

				lock( LockObject )
				{
					if( string.IsNullOrEmpty( Search ) )
						return SEARCH_BY.FIRST;

					var Selected = new List<LookupItem>();

					foreach( var Item in Items )
					{
						Item.Selected = false;

						if( StartsWith( Search, Key( Item ) ) )
						{
							Selected.Add( Item );
							Result = SEARCH_BY.NONE;
						}
					}

					if( Selected.Count > 0 )
					{
						var Item = Selected[ 0 ];
						Item.Selected = true;
						SelectItem    = Item;
					}
				}

				return Result;
			}

			ProcessingRequest = true;

			switch( DoSelected() )
			{
			case SEARCH_BY.FIRST:
				await Task.Run( async () =>
							    {
								    UpdateItems( await First( PreFetchCount ), false );
							    } );
				break;

			case SEARCH_BY.RANGE:
				await Task.Run( async () =>
							    {
								    var Range = await FindRange( Key( SearchText ), (int)PreFetchCount );
								    UpdateItems( Range, false );
								    SetSelectItem( await GetItem( Search ) );
							    } );
				break;
			}

			ProcessingRequest = false;
		}
		catch( Exception E )
		{
			Console.WriteLine( nameof( WhenSearchTextChanges ) );
			Console.WriteLine( E );
		}
	}

	public abstract Task<IList<T>> First( uint              count );
	public abstract Task<IList<T>> First( Tk                key, uint count );
	public abstract Task<IList<T>> Last( uint               count );
	public abstract Task<IList<T>> Last( Tk                 key,         uint count );
	public abstract Task<IList<T>> FindRange( Tk            key,         int  count );
	public abstract Task<IList<T>> FindMatch( Tk            key,         int  count );
	public abstract int            Compare( Tk              k1,          Tk   k2 );
	public abstract bool           StartsWith( string       searchValue, Tk   k2 );
	public abstract bool           Filter( Tk               filter,      T    value );
	public abstract Tk             Key( T                   item );
	public abstract Tk             Key( string              searchText );
	public abstract bool           IsEmpty( Tk              key );
	public abstract string         AsString( Tk             key );
	public abstract int            CompareSearchKey( string searchKey, Tk k2 );
}