﻿using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using IdsControlLibraryV2.Annotations;
using IdsControlLibraryV2.Utils.Colours;

namespace IdsControlLibraryV2.Convertors;

public class UIntToArgbBrushConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is uint Argb ? Argb.ArgbToSolidColorBrush() : new SolidColorBrush( Colors.Red );

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class IntToArgbBrushConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is int Argb ? Argb.ArgbToSolidColorBrush() : new SolidColorBrush( Colors.Red );

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToInvisibleConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is bool Vis )
			return !Vis ? Visibility.Visible : Visibility.Collapsed;
		return Visibility.Collapsed;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class ObjectToStringConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is string S )
			return S;
		return "";
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => value;
}

public class BoolToVisibilityConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is bool Vis )
			return Vis ? Visibility.Visible : Visibility.Collapsed;
		return Visibility.Collapsed;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToHiddenVisibilityConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is bool Vis )
			return Vis ? Visibility.Visible : Visibility.Hidden;
		return Visibility.Hidden;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToUnHiddenVisibilityConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is bool Vis )
			return !Vis ? Visibility.Visible : Visibility.Hidden;
		return Visibility.Hidden;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class TimeSpanToDateTimeConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		long Ticks;

		if( value is TimeSpan T )
			Ticks = T.Ticks;
		else
			Ticks = 0;

		return new DateTime( Ticks );
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
	{
		long Ticks;

		if( value is DateTime D )
			Ticks = D.Ticks;
		else
			Ticks = 0;

		return new TimeSpan( Ticks );
	}
}

public class GridLengthConverter : System.Windows.GridLengthConverter, IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => ConvertFrom( value );

	public object ConvertBack( [NotNull] object value, Type targetType, object parameter, CultureInfo culture ) => value is GridLength Length ? Length.Value : (object)0.0;
}

public class UtcToLocalTimeConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		switch( value )
		{
		case DateTime Time:
			return Time.ToLocalTime();

		case DateTimeOffset Dto:
			return Dto.ToLocalTime();

		default:
			return DateTime.MinValue;
		}
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class UtcToLocalTimeStringConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		DateTimeOffset T;

		switch( value )
		{
		case DateTime Time:
			T = Time.ToLocalTime();
			break;

		case DateTimeOffset Dto:
			T = Dto.ToLocalTime();
			break;

		default:
			T = DateTime.MinValue;
			break;
		}

		return $"{T:g}";
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}