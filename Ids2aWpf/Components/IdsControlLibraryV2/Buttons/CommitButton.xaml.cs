﻿using System.Reflection;
using System.Windows.Input;
using IdsControlLibraryV2.Utils;

namespace IdsControlLibraryV2;

/// <summary>
///     Interaction logic for CommitButton.xaml
/// </summary>
public partial class CommitButton
{
	private bool InKeyDown;

	public CommitButton()
	{
		InitializeComponent();
	}

	private void Button_Loaded( object sender, RoutedEventArgs e )
	{
		var P = this.FindParent<Page>();

		if( P != null )
		{
			P.PreviewKeyDown += ( o, args ) =>
			                    {
				                    if( !InKeyDown )
				                    {
					                    try
					                    {
						                    InKeyDown = true; // Double key bounce, sometimes

						                    if( ( args.Key == Key.Enter ) && ( ( Keyboard.Modifiers & ModifierKeys.Control ) == ModifierKeys.Control ) )
						                    {
							                    e.Handled = true;
							                    IsEnabled = false;

							                    try
							                    {
								                    typeof( Button ).GetMethod( "set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic )?.Invoke( this, new object[] {true} );

								                    Dispatcher.InvokeAsync( () =>
								                                            {
									                                            RaiseEvent( new RoutedEventArgs( ClickEvent ) );
								                                            } );
							                    }
							                    finally
							                    {
								                    typeof( Button ).GetMethod( "set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic )?.Invoke( this, new object[] {false} );
								                    IsEnabled = true;
							                    }
						                    }
					                    }
					                    finally
					                    {
						                    InKeyDown = false;
					                    }
				                    }
			                    };
		}
	}
}