﻿namespace IdsControlLibraryV2;

/// <summary>
///     Interaction logic for RotateLabel.xaml
/// </summary>
public partial class RotateLabel
{
	public RotateLabel()
	{
		InitializeComponent();
	}

	public double Angle
	{
		get => (double)GetValue( AngleProperty );
		set => SetValue( AngleProperty, value );
	}

	// Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty AngleProperty =
		DependencyProperty.Register( nameof( Angle ), typeof( double ), typeof( RotateLabel ), new FrameworkPropertyMetadata( -90.0 ) );

	public string Text
	{
		get => (string)GetValue( TextProperty );
		set => SetValue( TextProperty, value );
	}

	// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty TextProperty =
		DependencyProperty.Register( nameof( Text ), typeof( string ), typeof( RotateLabel ), new FrameworkPropertyMetadata( "" ) );
}