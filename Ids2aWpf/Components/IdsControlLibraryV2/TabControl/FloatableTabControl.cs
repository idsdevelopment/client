﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Window;

// ReSharper disable PossibleMultipleEnumeration

namespace IdsControlLibraryV2.TabControl;

public class FloatableTabItem : TabItem
{
	private const int MOUSE_MOVE_THRESHOLD = 5;

	protected UniversalFormsStyleScalableWindow UWindow { get; private set; }

	internal Guid Id { get; }

	internal bool Floating => FloatingWindow != null;

	private PresentationSource PresentationSource => _PresentationSource ??= PresentationSource.FromVisual( this );

	private PresentationSource _PresentationSource;

	private System.Windows.Window FloatingWindow;

	private Point? LastMousePosition;

	private readonly Label TabHeaderLabel = new()
	                                        {
		                                        Margin  = new Thickness( 0 ),
		                                        Padding = new Thickness( 0 ),
		                                        Name    = nameof( TabHeaderLabel )
	                                        };

	public Action BeginFloat,
	              EndFloat;

	internal bool DockOnMouseUp,
	              IgnoreClose,
	              InitialDrag;

	internal FloatableTabControl Owner;

	protected void SizeToContent()
	{
		if( FloatingWindow != null )
			FloatingWindow.SizeToContent = System.Windows.SizeToContent.WidthAndHeight;
	}

	protected void RemoveSizeToContent()
	{
		if( FloatingWindow != null )
			FloatingWindow.SizeToContent = System.Windows.SizeToContent.Manual;
	}

	internal void RemoveTab()
	{
		Owner.RemoveTab( this );

		if( FloatingWindow != null )
		{
			FloatingWindow.Close();
			FloatingWindow = null;
		}
	}

	protected virtual void SetHeader( object header )
	{
		TabHeaderLabel.Content = header;
	}

	protected virtual object GetHeader() => TabHeaderLabel.Content;

	public FloatableTabItem()
	{
		Id = Guid.NewGuid();

		TabHeaderLabel.MouseMove += ( sender, args ) =>
		                            {
			                            if( args.LeftButton == MouseButtonState.Pressed )
			                            {
				                            if( ( FloatingWindow == null ) && MouseMoveOK( args.GetPosition( this ) ) )
				                            {
					                            InitialDrag = true;
					                            Label TitleContent;

					                            FloatingWindow = new System.Windows.Window
					                                             {
						                                             Visibility         = Visibility.Visible,
						                                             AllowsTransparency = true,
						                                             Height             = Height,
						                                             Width              = Width,
						                                             WindowStyle        = WindowStyle.None,
						                                             Topmost            = true,
						                                             Content = UWindow = new UniversalFormsStyleScalableWindow
						                                                                 {
							                                                                 TitleBarContent       = TitleContent = new Label {Content = GetHeader() as string ?? ""},
							                                                                 Content               = Content,
							                                                                 CanCloseApplication   = false,
							                                                                 DisableClose          = !AllowClose,
							                                                                 TitleBarBackground    = Owner.FloatingHeaderBackground,
							                                                                 SystemIconsBackground = Owner.FloatingHeaderBackground
						                                                                 }
					                                             };
					                            FloatingWindow.Activate();
					                            Content = null;

					                            FloatingWindow.Closed += ( o, eventArgs ) =>
					                                                     {
						                                                     if( !IgnoreClose )
							                                                     RemoveTab();
					                                                     };

					                            void MButtonUp( object o, MouseButtonEventArgs eventArgs )
					                            {
						                            InitialDrag       = false;
						                            eventArgs.Handled = true;

						                            if( FloatingWindow != null )
						                            {
							                            FloatingWindow.Topmost = false;

							                            if( DockOnMouseUp )
							                            {
								                            IgnoreClose   = true;
								                            DockOnMouseUp = false;
								                            Content       = UWindow.Content;
								                            FloatingWindow.Close();
								                            FloatingWindow = null;
								                            Owner.Docked( this );
								                            IgnoreClose = false;
							                            }
							                            else
								                            Visibility = Owner.CollapseTabsOnFloat ? Visibility.Collapsed : Visibility.Hidden;
						                            }

						                            EndFloat?.Invoke();
					                            }

					                            FloatingWindow.MouseLeftButtonUp += MButtonUp;
					                            TitleContent.MouseLeftButtonUp   += MButtonUp;

					                            UWindow.PositionChanged += ( position, mousePosition ) =>
					                                                       {
						                                                       if( !InitialDrag && ( Owner != null ) && ( FloatingWindow != null ) )
							                                                       Owner.CheckForDocking( this, position, mousePosition );
					                                                       };
					                            BeginFloat?.Invoke();
				                            }

				                            if( ( PresentationSource?.CompositionTarget != null ) && ( Owner != null ) && ( FloatingWindow != null ) )
				                            {
					                            var Pos       = PointToScreen( new Point( 0, 0 ) );
					                            var ScreenPos = PresentationSource.CompositionTarget.TransformFromDevice.Transform( Pos );

					                            FloatingWindow.Top    = ScreenPos.Y;
					                            FloatingWindow.Left   = ScreenPos.X;
					                            FloatingWindow.Height = Owner.ActualHeight;
					                            FloatingWindow.Width  = Owner.ActualWidth;

					                            Visibility = Visibility.Hidden;
					                            UWindow.SendMouseDown( args );
				                            }
			                            }
			                            else
			                            {
				                            LastMousePosition = null;
				                            InitialDrag       = false;
			                            }
		                            };
		base.Header = TabHeaderLabel;
	}

	// ReSharper disable once InconsistentNaming
	private bool MouseMoveOK( Point p )
	{
		if( !LastMousePosition.HasValue )
		{
			LastMousePosition = p;
			return false;
		}

		var Lp = LastMousePosition.Value;
		return ( Math.Abs( Lp.X - p.X ) > MOUSE_MOVE_THRESHOLD ) || ( Math.Abs( Lp.Y - p.Y ) > MOUSE_MOVE_THRESHOLD );
	}


	// Using a DependencyProperty as the backing store for AllowClose.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for Header.  This enables animation, styling, binding, etc...


	private static void HeaderPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is FloatableTabItem Ft )
			Ft.SetHeader( e.NewValue );
	}


	[Category( "Options" )]
	public bool AllowClose
	{
		get => (bool)GetValue( AllowCloseProperty );
		set => SetValue( AllowCloseProperty, value );
	}

	public new static readonly DependencyProperty HeaderProperty =
		DependencyProperty.Register( nameof( Header ), typeof( object ), typeof( FloatableTabItem ), new FrameworkPropertyMetadata( null, HeaderPropertyChangedCallback ) );

	public static readonly DependencyProperty AllowCloseProperty =
		DependencyProperty.Register( nameof( AllowClose ), typeof( bool ), typeof( FloatableTabItem ), new FrameworkPropertyMetadata( false ) );

	[Category( "Common" )]
	public new object Header
	{
		get => GetHeader();
		set => SetValue( HeaderProperty, value );
	}
}

public class FloatableTabControl : System.Windows.Controls.TabControl
{
	private PresentationSource PresentationSource => _PresentationSource ??= PresentationSource.FromVisual( this );
	private PresentationSource _PresentationSource;

	private bool First = true;

	private TabPanel HeaderPanel;

	private Brush SaveBackGround;


	protected virtual void OnRemoveTab( FloatableTabItem item )
	{
	}

	internal void RemoveTab( FloatableTabItem item )
	{
		Dispatcher.Invoke( () =>
		                   {
			                   if( base.ItemsSource is ICollection<FloatableTabItem> Itms )
			                   {
				                   var Id = item.Id;

				                   foreach( var Item in Itms )
				                   {
					                   if( Item.Id == Id )
					                   {
						                   Itms.Remove( Item );
						                   break;
					                   }
				                   }

				                   foreach( var Item in ItemsSource )
				                   {
					                   if( Item.Id == Id )
					                   {
						                   Itms.Remove( Item );
						                   break;
					                   }
				                   }

				                   var Save = ItemsSource;
				                   ItemsSource = null;
				                   UpdateLayout();
				                   ItemsSource = Save;
				                   OnRemoveTab( item );
			                   }
		                   } );
	}

	internal void Docked( FloatableTabItem tabItem )
	{
		if( SaveBackGround != null )
		{
			Background     = SaveBackGround;
			SaveBackGround = null;
		}
	}

	internal void CheckForDocking( FloatableTabItem tabItem, Point windowPosition, Point mousePosition )
	{
		if( PresentationSource?.CompositionTarget != null )
		{
			var ScreenPosTopLeft     = PointToScreen( new Point( 0, 0 ) );
			var ScreenPosBottomRight = PointToScreen( new Point( ActualWidth, ActualHeight ) );

			if( ( mousePosition.X >= ScreenPosTopLeft.X ) && ( mousePosition.X <= ScreenPosBottomRight.X ) && ( mousePosition.Y >= ScreenPosTopLeft.Y ) && ( mousePosition.Y <= ScreenPosBottomRight.Y ) )
			{
				if( SaveBackGround == null )
				{
					SaveBackGround        = Background;
					tabItem.Visibility    = Visibility.Visible;
					SelectedItem          = tabItem;
					tabItem.DockOnMouseUp = true;
				}

				Background = Docking;
			}
			else if( SaveBackGround != null )
			{
				Background     = SaveBackGround;
				SaveBackGround = null;

				if( tabItem.Floating )
				{
					tabItem.Visibility    = Visibility.Hidden;
					tabItem.DockOnMouseUp = false;
				}
			}
		}
	}

	public FloatableTabControl()
	{
		Loaded += ( sender, args ) =>
		          {
			          if( First )
			          {
				          First = false;

				          if( Items.Count > 0 )
					          ItemsSource = Items;
			          }
			          else
			          {
				          HeaderPanel = Template.FindName( "HeaderPanel", this ) as TabPanel;

				          if( HeaderPanel != null )
				          {
					          HeaderPanel.Background = HeaderBackground;
					          HeaderPanel.Margin     = new Thickness( 0 );
				          }
			          }
		          };
	}

	private void DoBaseItemsSource( ICollection<FloatableTabItem> items )
	{
		foreach( var Item in items )
			Item.Owner = this;

		base.ItemsSource = items;

		if( items.Count > 0 )
			SelectedIndex = 0;
	}


	private static void HeaderBackgroundPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is FloatableTabControl Ft && e.NewValue is Brush Brush )
		{
			if( Ft.HeaderPanel != null )
				Ft.HeaderPanel.Background = Brush;
		}
	}


	// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...


	private static void ItemsSourcePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is FloatableTabControl Tc && e.NewValue is ICollection<FloatableTabItem> Items )
			Tc.DoBaseItemsSource( Items );
	}


	[Category( "Appearance" )]
	public bool CollapseTabsOnFloat
	{
		get => (bool)GetValue( CollapseTabsOnFloatProperty );
		set => SetValue( CollapseTabsOnFloatProperty, value );
	}

	// Using a DependencyProperty as the backing store for CollapseTabsOnFloat.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty CollapseTabsOnFloatProperty =
		DependencyProperty.Register( nameof( CollapseTabsOnFloat ), typeof( bool ), typeof( FloatableTabControl ), new FrameworkPropertyMetadata( true ) );


	[Category( "Brush" )]
	public Brush Docking
	{
		get => (Brush)GetValue( DockingProperty );
		set => SetValue( DockingProperty, value );
	}

	// Using a DependencyProperty as the backing store for Docking.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty DockingProperty =
		DependencyProperty.Register( nameof( Docking ), typeof( Brush ), typeof( FloatableTabControl ), new FrameworkPropertyMetadata( new SolidColorBrush( new Color
		                                                                                                                                                    {
			                                                                                                                                                    A = 0xff,
			                                                                                                                                                    R = 244,
			                                                                                                                                                    G = 228,
			                                                                                                                                                    B = 229
		                                                                                                                                                    } ) ) );


	[Category( "Brush" )]
	public Brush FloatingHeaderBackground
	{
		get => (Brush)GetValue( FloatingHeaderBackgroundProperty );
		set => SetValue( FloatingHeaderBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for FloatingHeaderBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty FloatingHeaderBackgroundProperty =
		DependencyProperty.Register( nameof( FloatingHeaderBackground ), typeof( Brush ), typeof( FloatableTabControl ), new FrameworkPropertyMetadata( Brushes.White, FrameworkPropertyMetadataOptions.Inherits ) );


	[Category( "Brush" )]
	public Brush HeaderBackground
	{
		get => (Brush)GetValue( HeaderBackgroundProperty );
		set => SetValue( HeaderBackgroundProperty, value );
	}

	// Using a DependencyProperty as the backing store for HeaderBackground.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty HeaderBackgroundProperty =
		DependencyProperty.Register( nameof( HeaderBackground ), typeof( Brush ), typeof( FloatableTabControl ), new FrameworkPropertyMetadata( Brushes.White, FrameworkPropertyMetadataOptions.Inherits, HeaderBackgroundPropertyChangedCallback ) );


	[Category( "Common" )]
	public new ObservableCollection<FloatableTabItem> Items
	{
		get => (ObservableCollection<FloatableTabItem>)GetValue( ItemsProperty );
		set => SetValue( ItemsProperty, value );
	}


	public static readonly DependencyProperty ItemsProperty =
		DependencyProperty.Register( nameof( Items ), typeof( ObservableCollection<FloatableTabItem> ), typeof( FloatableTabControl ), new FrameworkPropertyMetadata( new ObservableCollection<FloatableTabItem>() ) );


	// Using a DependencyProperty as the backing store for Items.  This enables animation, styling, binding, etc...


	[Category( "Common" )]
	public new ICollection<FloatableTabItem> ItemsSource
	{
		get => (ICollection<FloatableTabItem>)GetValue( ItemsSourceProperty );
		set => SetValue( ItemsSourceProperty, value );
	}

	public new static readonly DependencyProperty ItemsSourceProperty =
		DependencyProperty.Register( nameof( ItemsSource ), typeof( ICollection<FloatableTabItem> ), typeof( FloatableTabControl ), new FrameworkPropertyMetadata( null, ItemsSourcePropertyChangedCallback ) );
}