﻿#nullable enable

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Utils;
using Newtonsoft.Json;
using Utils;
using File = System.IO.File;

namespace IdsControlLibraryV2.Ribbon;

public class Ribbon : DockPanel
{
	private const string TOOLBARS                = "ToolBars",
						 SETTINGS_FILE_EXTENSION = "Settings";

	public class ToolBarSetting
	{
		public int Band      { get; set; }
		public int BandIndex { get; set; }
	}

	public class Settings
	{
		public List<ToolBarSetting> BarSettings   { get; set; } = new();
		public bool                 IsMenuVisible { get; set; }
	}

	public Collection<ToolBar> ToolBars => ToolBarTray.ToolBars;

	public List<MenuItem> CollapsedMenuItems { get; set; } = new();

	private static string? ProductName
	{
		get
		{
			if( string.IsNullOrWhiteSpace( _ProductName ) )
				_ProductName = ( Assembly.GetEntryAssembly()!.GetCustomAttributes( typeof( AssemblyProductAttribute ) ).SingleOrDefault() as AssemblyProductAttribute )?.Product;

			return _ProductName;
		}
	}

	protected string MakeFileName()
	{
		var AppData    = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		var BaseFolder = $"{AppData}\\{ProductName}\\{TOOLBARS}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		return $"{BaseFolder}\\{LayoutSaveProfile}.{SETTINGS_FILE_EXTENSION}";
	}

	public Ribbon()
	{
		var Prop = DesignerProperties.IsInDesignModeProperty;
		IsInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty( Prop, typeof( FrameworkElement ) ).Metadata.DefaultValue;

		HorizontalAlignment = HorizontalAlignment.Stretch;
		VerticalAlignment   = VerticalAlignment.Stretch;

		BaseToolBar = new ToolBar
					  {
						  Background = Brushes.White
					  };

		BaseToolBar.Items.Add( ToolBarTray = new ToolBarTray
										     {
											     Background = Brushes.Transparent
										     } );

		ToolBarTray.LayoutUpdated += ( _, _ ) =>
									 {
										 if( !DidUnload )
										 {
											 if( Mouse.LeftButton == MouseButtonState.Pressed )
												 ToolBarLayoutUpdated = true;
											 else if( ToolBarLayoutUpdated )
											 {
												 ToolBarLayoutUpdated = false;
												 SaveSettings();
											 }
										 }
									 };

		Menu = new Menu
			   {
				   Visibility          = IsInDesignMode ? Visibility.Visible : Visibility.Collapsed,
				   HorizontalAlignment = HorizontalAlignment.Stretch,
				   Height              = 20,
				   Background          = Brushes.Transparent
			   };

		Menu.MouseDoubleClick += ( _, _ ) =>
								 {
									 MenuVisible = false;
								 };

		SetDock( Menu, Dock.Top );
		SetDock( BaseToolBar, Dock.Top );

		Children.Add( Menu );
		Children.Add( BaseToolBar );

		Unloaded += ( _, _ ) =>
					{
						if( !DidUnload )
						{
							DidUnload = true;
							SaveSettings();
						}
					};

		static void DoHide( Control? toolbar )
		{
			if( toolbar is not null && ( ToolBar.GetOverflowMode( toolbar ) != OverflowMode.Always ) )
			{
				if( toolbar.Template.FindName( "OverflowGrid", toolbar ) is FrameworkElement OverflowGrid )
					OverflowGrid.Visibility = Visibility.Collapsed;

				if( toolbar.Template.FindName( "MainPanelBorder", toolbar ) is FrameworkElement MainPanelBorder )
					MainPanelBorder.Margin = new Thickness();
			}
		}

		Initialized += ( sender, _ ) =>
					   {
						   DoHide( sender as ToolBar );
					   };

		Loaded += ( _, args ) =>
				  {
					  if( !MenuLoaded )
					  {
						  MenuLoaded = true;

						  DoMenuVisibility( this, false );

						  ToolBarTray.ToolTip = ShowMenuToolTip;
						  Menu.ToolTip        = ShowToolBarToolTip;

						  foreach( var ToolBar in ToolBarTray.ToolBars )
						  {
							  ToolBar.Loaded += ( o, _ ) =>
											    {
												    DoHide( o as ToolBar );
											    };

							  DoHide( ToolBar );

							  var Header = ToolBar.Header;

							  if( Header is not null )
							  {
								  ToolBar.Header = null;

								  var Items = new List<FrameworkElement>();

								  foreach( var Item in ToolBar.Items )
								  {
									  if( Item is FrameworkElement E )
										  Items.Add( E );
								  }

								  ToolBar.Items.Clear();

								  var Dp = new DockPanel
										   {
											   LastChildFill     = true,
											   VerticalAlignment = VerticalAlignment.Stretch,
										   };

								  foreach( var Item in Items )
								  {
									  Dp.Children.Add( Item );
									  SetDock( Item, Dock.Top );

									  var Tb = new TextBlock
											   {
												   Margin = new Thickness( 0, Spacing, 0, 0 ),

												   HorizontalAlignment = HorizontalAlignment.Stretch,
												   TextAlignment       = TextAlignment.Center,
												   Text                = Header.ToString(),
												   VerticalAlignment   = VerticalAlignment.Bottom
											   };

									  Dp.Children.Add( Tb );
									  SetDock( Tb, Dock.Bottom );
								  }

								  ToolBar.Items.Add( Dp );
							  }
						  }

						  var Mi = Menu.Items;

						  foreach( var Item in CollapsedMenuItems )
							  Mi.Add( Item );

						  var MenuText = ShowToolBarText.Split( new[] {"//"}, StringSplitOptions.RemoveEmptyEntries );

						  if( MenuText.Length >= 2 )
						  {
							  var SubItem = new MenuItem
										    {
											    Header = MenuText[ 1 ]
										    };

							  SubItem.Click += ( _, _ ) =>
											   {
												   MenuVisible = false;
											   };

							  var MainItem = new MenuItem
										     {
											     Header = MenuText[ 0 ]
										     };
							  MainItem.Items.Add( SubItem );
							  Mi.Add( MainItem );
						  }

						  var P = Page = this.FindParent<Page>();

						  if( P != null )
						  {
							  P.PreviewKeyDown += ( _, kargs ) =>
												  {
													  if( kargs.Key == Key.F1 )
													  {
														  args.Handled   = true;
														  Page.IsEnabled = false;

														  try
														  {
															  var Url = HelpUri;

															  if( !string.IsNullOrEmpty( Url ) )
															  {
																  try
																  {
																	  Process.Start( Url );
																  }
																  catch( Exception E )
																  {
																	  Console.WriteLine( E );
																  }
															  }
														  }
														  finally
														  {
															  Page.IsEnabled = true;
														  }
													  }
												  };
						  }

						  LoadSettings();
					  }
				  };

		BaseToolBar.MouseDoubleClick += ( _, _ ) =>
										{
											MenuVisible = true;
										};

		ShowMenuToolTip = new TextBlock
						  {
							  Text = "Double click to hide toolbar"
						  };

		ShowToolBarToolTip = new TextBlock
							 {
								 Text = "Double click to show toolbar"
							 };
	}

	private static string? _ProductName;

	// Using a DependencyProperty as the backing store for HelpUri.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty BackgroundProperty =
		DependencyProperty.Register( nameof( Background ), typeof( Brush ), typeof( Ribbon ), new FrameworkPropertyMetadata( Brushes.Transparent, BackgroundPropertyChangedCallback ) );

	private readonly ToolBar BaseToolBar;

	public static readonly DependencyProperty HelpUriProperty =
		DependencyProperty.Register( nameof( HelpUri ), typeof( string ), typeof( Ribbon ), new FrameworkPropertyMetadata( "https://idsservices.atlassian.net/wiki/display/IKB" ) );

	private readonly bool IsInDesignMode;

	// Using a DependencyProperty as the backing store for LayoutSaveProfile.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty LayoutSaveProfileProperty =
		DependencyProperty.Register( nameof( LayoutSaveProfile ), typeof( string ), typeof( Ribbon ), new FrameworkPropertyMetadata( "" ) );

	private readonly Menu Menu;
	private          bool MenuLoaded;

	public static readonly DependencyProperty MenuVisibleProperty =
		DependencyProperty.Register( nameof( MenuVisible ), typeof( bool ), typeof( Ribbon ), new FrameworkPropertyMetadata( false, MenuVisiblePropertyChangedCallback ) );

	private Page? Page;

	// Using a DependencyProperty as the backing store for ShowMenuToolTip.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ShowMenuToolTipProperty =
		DependencyProperty.Register( nameof( ShowMenuToolTip ), typeof( object ), typeof( Ribbon ), new FrameworkPropertyMetadata( null, ShowMenuToolTipPropertyChangedCallback ) );

	// Using a DependencyProperty as the backing store for ShowToolBarText.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ShowToolBarTextProperty =
		DependencyProperty.Register( nameof( ShowToolBarText ), typeof( string ), typeof( Ribbon ), new FrameworkPropertyMetadata( "View//Show Toolbar" ) );

	// Using a DependencyProperty as the backing store for ShowMenuToolTip.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ShowToolBarToolTipProperty =
		DependencyProperty.Register( nameof( ShowToolBarToolTip ), typeof( object ), typeof( Ribbon ), new FrameworkPropertyMetadata( null, ShowToolBarToolPropertyChangedCallback ) );

	private bool ToolBarLayoutUpdated,
				 DidUnload;

	private readonly ToolBarTray ToolBarTray;

	private static void DoMenuVisibility( Ribbon r, bool vis )
	{
		if( vis )
		{
			r.BaseToolBar.Visibility = Visibility.Collapsed;
			r.Menu.Visibility        = Visibility.Visible;
		}
		else
		{
			r.BaseToolBar.Visibility = Visibility.Visible;
			r.Menu.Visibility        = Visibility.Collapsed;
		}
	}

	private static void MenuVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is Ribbon R && e.NewValue is bool Vis )
			DoMenuVisibility( R, Vis );
	}

	private void SaveSettings()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var Settings = new Settings
							   {
								   IsMenuVisible = MenuVisible
							   };

				foreach( var ToolBar in ToolBarTray.ToolBars )
				{
					Settings.BarSettings.Add( new ToolBarSetting
											  {
												  Band      = ToolBar.Band,
												  BandIndex = ToolBar.BandIndex
											  } );
				}

				var SerializedObject = JsonConvert.SerializeObject( Settings );
				SerializedObject = Encryption.Encrypt( SerializedObject );
				File.WriteAllText( MakeFileName(), SerializedObject );
			}
			catch
			{
			}
		}
	}

	private void LoadSettings()
	{
		if( !IsInDesignMode )
		{
			try
			{
				var FileName = MakeFileName();

				if( File.Exists( FileName ) )
				{
					var SerializedObject = File.ReadAllText( FileName );
					SerializedObject = Encryption.Decrypt( SerializedObject );
					var Settings = JsonConvert.DeserializeObject<Settings>( SerializedObject );

					if( Settings is not null )
					{
						MenuVisible = Settings.IsMenuVisible;
						var Bars  = ToolBarTray.ToolBars;
						var Count = Math.Min( Bars.Count, Settings.BarSettings.Count );

						for( var I = 0; I < Count; I++ )
						{
							var Setting = Settings.BarSettings[ I ];
							var TBar    = Bars[ I ];
							TBar.Band      = Setting.Band;
							TBar.BandIndex = Setting.BandIndex;
						}
					}
				}
			}
			catch
			{
			}
		}
	}

	private static void ShowMenuToolTipPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is Ribbon R )
			R.BaseToolBar.ToolTip = e.NewValue;
	}

	private static void ShowToolBarToolPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is Ribbon R )
			R.Menu.ToolTip = e.NewValue;
	}

	[Category( "Options" )]
	public string HelpUri
	{
		get => (string)GetValue( HelpUriProperty );
		set => SetValue( HelpUriProperty, value );
	}

	public string LayoutSaveProfile
	{
		get
		{
			var Temp = (string)GetValue( LayoutSaveProfileProperty );

			if( string.IsNullOrWhiteSpace( Temp ) )
				Temp = Page?.Title ?? "Ribbon";

			return Temp;
		}
		set => SetValue( LayoutSaveProfileProperty, value );
	}

	[Category( "Options" )]
	public bool MenuVisible
	{
		get => (bool)GetValue( MenuVisibleProperty );
		set => SetValue( MenuVisibleProperty, value );
	}

	public object ShowMenuToolTip
	{
		get => GetValue( ShowMenuToolTipProperty );
		set => SetValue( ShowMenuToolTipProperty, value );
	}

	[Category( "Options" )]
	public string ShowToolBarText
	{
		get => (string)GetValue( ShowToolBarTextProperty );
		set => SetValue( ShowToolBarTextProperty, value );
	}

	public object ShowToolBarToolTip
	{
		get => GetValue( ShowToolBarToolTipProperty );
		set => SetValue( ShowToolBarToolTipProperty, value );
	}

#region Footer Category Spacing
	public static readonly DependencyProperty SpacingProperty =
		DependencyProperty.Register( nameof( Spacing ), 
									 typeof( int ), 
									 typeof( Ribbon ), 
									 new FrameworkPropertyMetadata( 5 ) );

	[Category( "Options" )]
	public int Spacing
	{
		get => (int)GetValue( SpacingProperty );
		set => SetValue( SpacingProperty, value );
	}

	// Using a DependencyProperty as the backing store for MenuVisible.  This enables animation, styling, binding, etc...
#endregion

	// Using a DependencyProperty as the backing store for MenuVisible.  This enables animation, styling, binding, etc...

#region Background
	[Category( "Brush" )]
	public new Brush Background
	{
		get => (Brush)GetValue( BackgroundProperty );
		set => SetValue( BackgroundProperty, value );
	}

	private static void BackgroundPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is Ribbon R && e.NewValue is SolidColorBrush B )
		{
			R.BaseToolBar.Background = B;

			if( R is DockPanel D )
				D.Background = B;
		}
	}
#endregion
}