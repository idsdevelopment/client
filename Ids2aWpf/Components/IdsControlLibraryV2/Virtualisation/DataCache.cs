﻿namespace IdsControlLibraryV2.Virtualisation;

public class DataCache<Tk, Tv> : Dictionary<Tk, Tv>
{
	private readonly Dictionary<Tk, int> AgeDict = new();

	private readonly List<Tk>    AgeList = new();
	private readonly HashSet<Tv> ByValue = new();
	private readonly int         MaxEntries;

	private bool SuppressEvent;

	public event DeleteFromCacheEvent OnDeleteFromCache;

	public new void Clear()
	{
		base.Clear();
		AgeList.Clear();
		AgeDict.Clear();
	}

	public new void Remove( Tk key )
	{
		if( AgeDict.TryGetValue( key, out var Index ) )
		{
			AgeDict.Remove( key );
			AgeList.RemoveAt( Index );
		}

		if( TryGetValue( key, out var Value ) )
		{
			ByValue.Remove( Value );
			base.Remove( key );

			if( !SuppressEvent )
				OnDeleteFromCache?.Invoke( key, Value );
		}
	}

	public new void Add( Tk key, Tv value )
	{
		var C = AgeList.Count;

		if( ( C > 0 ) && ( C >= MaxEntries ) )
			Remove( AgeList[ 0 ] );

		if( TryGetValue( key, out var Value ) )
		{
			base[ key ] = Value;

			if( AgeDict.TryGetValue( key, out var Index ) )
			{
				AgeDict.Remove( key );
				AgeList.RemoveAt( Index );
			}
		}
		else
			base.Add( key, value );

		var Ndx = AgeList.Count;
		AgeList.Add( key );
		AgeDict.Add( key, Ndx );
		ByValue.Add( value );
	}

	public DataCache( int maxEntries )
	{
		MaxEntries = maxEntries - 1;
	}

	public new Tv this[ Tk index ]
	{
		get => base[ index ];

		set
		{
			SuppressEvent = true;
			Remove( index );
			Add( index, value );
			SuppressEvent = false;
		}
	}

	public delegate void DeleteFromCacheEvent( Tk key, Tv value );
}