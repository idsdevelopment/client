﻿using System.Windows.Media;
using System.Windows.Media.Animation;

namespace IdsControlLibraryV2.Animations;

public static partial class Animations
{
	private const double DEFAULT_SHOW_ANIMATION_TIME = 0.4;

	private enum DIRECTION
	{
		TOP,
		BOTTOM,
		LEFT,
		RIGHT
	}

	public static FrameworkElement ShowFromLeft( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.LEFT, true, duration, completeCallback );

	public static FrameworkElement ShowFromLeft( this FrameworkElement element, Action completeCallback ) => ShowFromLeft( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement ShowFromLeft( this FrameworkElement element ) => ShowFromLeft( element, null );

	public static FrameworkElement HideToLeft( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.LEFT, false, duration, completeCallback );

	public static FrameworkElement HideToLeft( this FrameworkElement element, Action completeCallback ) => HideToLeft( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement HideToLeft( this FrameworkElement element ) => HideToLeft( element, null );

	public static FrameworkElement ShowFromTop( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.TOP, true, duration, completeCallback );

	public static FrameworkElement ShowFromTop( this FrameworkElement element, Action completeCallback ) => ShowFromTop( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement ShowFromTop( this FrameworkElement element ) => ShowFromTop( element, null );

	public static FrameworkElement HideToTop( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.TOP, false, duration, completeCallback );

	public static FrameworkElement HideToTop( this FrameworkElement element, Action completeCallback ) => HideToTop( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement HideToTop( this FrameworkElement element ) => HideToTop( element, null );

	public static FrameworkElement ShowFromRight( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.RIGHT, true, duration, completeCallback );

	public static FrameworkElement ShowFromRight( this FrameworkElement element, Action completeCallback ) => ShowFromRight( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement ShowFromRight( this FrameworkElement element ) => ShowFromRight( element, null );

	public static FrameworkElement HideToRight( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.RIGHT, false, duration, completeCallback );

	public static FrameworkElement HideToRight( this FrameworkElement element, Action completeCallback ) => HideToRight( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement HideToRight( this FrameworkElement element ) => HideToRight( element, null );

	public static FrameworkElement ShowFromBottom( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.BOTTOM, true, duration, completeCallback );

	public static FrameworkElement ShowFromBottom( this FrameworkElement element, Action completeCallback ) => ShowFromBottom( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement ShowFromBottom( this FrameworkElement element ) => ShowFromRight( element, null );

	public static FrameworkElement HideToBottom( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowHide( element, DIRECTION.BOTTOM, false, duration, completeCallback );

	public static FrameworkElement HideToBottom( this FrameworkElement element, Action completeCallback ) => HideToBottom( element, DEFAULT_SHOW_ANIMATION_TIME, completeCallback );

	public static FrameworkElement HideToBottom( this FrameworkElement element ) => HideToBottom( element, null );

	private static FrameworkElement ShowHide( FrameworkElement element, DIRECTION direction, bool show, double duration, Action completeCallback )
	{
		Application.Current.Dispatcher.InvokeAsync( () =>
		                                            {
			                                            var Duration   = TimeSpan.FromSeconds( duration );
			                                            var ODelay     = TimeSpan.FromSeconds( duration * 0.5 );
			                                            var Storyboard = new Storyboard {FillBehavior = FillBehavior.Stop};

			                                            element.Opacity = 0;

			                                            var Opacity = show
				                                                          ? new DoubleAnimation {From = 0, To = 1, Duration = Duration, BeginTime = ODelay}
				                                                          : new DoubleAnimation {From = 1, To = 0, Duration = Duration};

			                                            Storyboard.SetTargetProperty( Opacity, new PropertyPath( "Opacity" ) );
			                                            Storyboard.SetTarget( Opacity, element );
			                                            Storyboard.Children.Add( Opacity );

			                                            var Animation = !show
				                                                            ? new DoubleAnimation {From = 1, To = 0, AutoReverse = false, Duration = Duration}
				                                                            : new DoubleAnimation {From = 0, To = 1, AutoReverse = false, Duration = Duration, AccelerationRatio = 0.8, DecelerationRatio = 0.2};

			                                            PropertyPath Path;

			                                            switch( direction )
			                                            {
			                                            case DIRECTION.TOP:
			                                            case DIRECTION.BOTTOM:
				                                            Path = new PropertyPath( "RenderTransform.ScaleY" );
				                                            break;

			                                            default:
				                                            Path = new PropertyPath( "RenderTransform.ScaleX" );
				                                            break;
			                                            }

			                                            ScaleTransform Scale;

			                                            switch( direction )
			                                            {
			                                            case DIRECTION.BOTTOM:
				                                            Scale = new ScaleTransform( 1, 1, 0, element.ActualHeight );
				                                            break;

			                                            case DIRECTION.TOP:
			                                            case DIRECTION.LEFT:
				                                            Scale = new ScaleTransform( 1, 1, 0, 0 );
				                                            break;

			                                            default:
				                                            Scale = new ScaleTransform( 1, 1, element.ActualWidth, 0 );
				                                            break;
			                                            }

			                                            element.RenderTransform = Scale;

			                                            Storyboard.SetTargetProperty( Animation, Path );
			                                            Storyboard.SetTarget( Animation, element );
			                                            Storyboard.Children.Add( Animation );

			                                            if( completeCallback != null )
			                                            {
				                                            Storyboard.Completed += ( sender, args ) =>
				                                                                    {
					                                                                    completeCallback();
				                                                                    };
			                                            }

			                                            Storyboard.Begin();
		                                            } );
		return element;
	}
}