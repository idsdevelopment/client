﻿using System.Windows.Media.Animation;

namespace IdsControlLibraryV2.Animations;

public static partial class Animations
{
	private const double DEFAULT_SLIDE_ANIMATION_TIME = 0.2;

	public static FrameworkElement SlideFromLeft( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.LEFT, true, duration, completeCallback );

	public static FrameworkElement SlideFromTop( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.TOP, true, duration, completeCallback );

	public static FrameworkElement SlideFromRight( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.RIGHT, true, duration, completeCallback );

	public static FrameworkElement SlideFromBottom( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.BOTTOM, true, duration, completeCallback );

	public static FrameworkElement SlideToLeft( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.LEFT, false, duration, completeCallback );

	public static FrameworkElement SlideToTop( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.TOP, false, duration, completeCallback );

	public static FrameworkElement SlideToRight( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.RIGHT, false, duration, completeCallback );

	public static FrameworkElement SlideToBottom( this FrameworkElement element, double duration, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.BOTTOM, false, duration, completeCallback );

	public static FrameworkElement SlideFromLeft( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.LEFT, true, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideFromTop( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.TOP, true, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideFromRight( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.RIGHT, true, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideFromBottom( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.BOTTOM, true, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideToLeft( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.LEFT, false, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideToTop( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.TOP, false, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideToRight( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.RIGHT, false, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	public static FrameworkElement SlideToBottom( this FrameworkElement element, Action completeCallback = null ) => ShowSlideInOut( element, DIRECTION.BOTTOM, false, DEFAULT_SLIDE_ANIMATION_TIME, completeCallback );

	private static FrameworkElement ShowSlideInOut( FrameworkElement element, DIRECTION direction, bool show, double duration, Action completeCallback )
	{
		Application.Current.Dispatcher.InvokeAsync( () =>
		                                            {
			                                            var Duration   = TimeSpan.FromSeconds( duration );
			                                            var Storyboard = new Storyboard {FillBehavior = FillBehavior.Stop};

			                                            double AValue;
			                                            string AProperty;

			                                            switch( direction )
			                                            {
			                                            case DIRECTION.BOTTOM:
			                                            case DIRECTION.TOP:
				                                            AValue    = element.ActualHeight;
				                                            AProperty = "Height";
				                                            break;

			                                            default:
				                                            AValue    = element.ActualWidth;
				                                            AProperty = "Width";
				                                            break;
			                                            }

			                                            var W = show
				                                                    ? new DoubleAnimation {From = 0, To      = AValue, Duration = Duration}
				                                                    : new DoubleAnimation {From = AValue, To = 0, Duration      = Duration};

			                                            Storyboard.SetTargetProperty( W, new PropertyPath( AProperty ) );
			                                            Storyboard.SetTarget( W, element );
			                                            Storyboard.Children.Add( W );

			                                            if( completeCallback != null )
			                                            {
				                                            Storyboard.Completed += ( sender, args ) =>
				                                                                    {
					                                                                    completeCallback();
				                                                                    };
			                                            }

			                                            Storyboard.Begin();
		                                            } );
		return element;
	}
}