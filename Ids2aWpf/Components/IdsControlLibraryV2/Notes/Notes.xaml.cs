﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Timers;
using System.Windows.Input;
using IdsControlLibraryV2.Utils;
using Utils;

namespace IdsControlLibraryV2.Notes;

/// <summary>
///     Interaction logic for Notes.xaml
/// </summary>
public partial class Notes
{
	private readonly Timer FocusTimer = new( 100 ) {AutoReset = false};

	private bool AddHasFocus;


	private string OriginalNoteName;

	public event OnNoteEvent OnNewNote, OnDeleteNote;

	public event OnBeforeDeleteNoteEvent
		OnBeforeDeleteNote;

	public event OnRenameNoteEvent OnRenameNote;

	public void Refresh()
	{
		var Temp = ItemsSourceDictionary;
		ItemsSourceDictionary = null;
		ItemsSourceDictionary = Temp;
	}

	public Notes()
	{
		InitializeComponent();
	}

	public delegate bool OnBeforeDeleteNoteEvent( string noteName );

	public delegate void OnNoteEvent( string noteName );

	public delegate void OnRenameNoteEvent( string oldNoteName, string newNoteName );

	private string NewText( string txt )
	{
		txt = txt.Trim();
		var Key  = txt;
		var I    = 1;
		var Dict = ItemsSourceDictionary;

		while( Dict.ContainsKey( Key ) )
			Key = $"{txt}_{I++}";
		return Key;
	}

	private void AddButton_Click( object sender, RoutedEventArgs e )
	{
		var Note = NewNoteName.Trim();

		if( Note.IsNotNullOrWhiteSpace() )
		{
			AddHasFocus = true;
			e.Handled   = true;
			NoteList.Focus();
			var NoteName = NewText( Note );
			var Temp     = ItemsSourceDictionary;
			Temp.Add( NoteName, "" );
			ItemsSourceDictionary = null;
			ItemsSourceDictionary = Temp;
			NoteTextBox.Clear();
			NoteList.SelectedIndex = ItemsSourceDictionary.Count - 1;

			FocusTimer.Elapsed += ( o, args ) =>
			                      {
				                      Dispatcher.Invoke( () =>
				                                         {
					                                         NoteList_SelectionChanged( NoteList, null );
					                                         OnNewNote?.Invoke( NoteName );
				                                         } );
			                      };
			FocusTimer.Start();
		}
	}

	private void DockPanel_GotFocus( object sender, RoutedEventArgs e )
	{
		if( sender is DockPanel Panel )
		{
			var TBox = Panel.FindChild<TextBox>();
			TBox.SelectionStart  = 0;
			TBox.SelectionLength = TBox.Text.Length;
		}
	}

	private void NoteList_SelectionChanged( object sender, SelectionChangedEventArgs e )
	{
		if( e.IsNotNull() )
			e.Handled = true;

		var Item = NoteList.ItemContainerGenerator.ContainerFromIndex( NoteList.SelectedIndex ) as ListBoxItem;
		Item?.FindChild<TextBox>()?.Focus();
	}

	private void NoteName_LostFocus( object sender, RoutedEventArgs e )
	{
		if( !AddHasFocus )
		{
			if( sender is TextBox Tb )
			{
				var Txt = Tb.Text;

				if( OriginalNoteName.IsNotNullOrWhiteSpace() )
				{
					if( OriginalNoteName != Txt )
						OnRenameNote?.Invoke( OriginalNoteName, Txt );
				}
				else
					Tb.Text = NewText( Txt );
				OriginalNoteName = "";
			}
		}
	}

	private void NoteName_KeyDown( object sender, KeyEventArgs e )
	{
		AddHasFocus = false;
	}

	private void Button_Click( object sender, RoutedEventArgs e )
	{
		if( sender is Button B )
		{
			var Dp = B.FindParent<DockPanel>();

			if( Dp.IsNotNull() )
			{
				var Tb = Dp.FindChild<TextBox>();

				if( Tb.IsNotNull() )
				{
					var Text = Tb.Text;

					if( OnBeforeDeleteNote?.Invoke( Text ) ?? true )
					{
						var Temp = ItemsSourceDictionary;
						Temp.Remove( Text );
						ItemsSourceDictionary = null;
						ItemsSourceDictionary = Temp;
						OnDeleteNote?.Invoke( Text );
					}
				}
			}
		}
	}

	private void NoteName_GotFocus( object sender, RoutedEventArgs e )
	{
		if( sender is TextBox Tb )
			OriginalNoteName = Tb.Text;
	}


	private static void ItemsDictionaryPropertyChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is Notes N )
		{
			var Is = new ObservableCollection<string>();
			var Lb = N.NoteList;

			if( e.NewValue is Dictionary<string, string> Dict )
			{
				foreach( var NoteName in from D in Dict
				                         orderby D.Key
				                         select D.Key )
					Is.Add( NoteName );
			}
			Lb.ItemsSource = Is;
			N.NoteTextBox.Clear();
		}
	}

	[Category( "Options" )]
	public string ColumnHeader
	{
		get => (string)GetValue( ColumnHeaderProperty );
		set => SetValue( ColumnHeaderProperty, value );
	}

	public static readonly DependencyProperty ColumnHeaderProperty =
		DependencyProperty.Register( nameof( ColumnHeader ), typeof( string ), typeof( Notes ), new FrameworkPropertyMetadata( "" ) );


	[Category( "Common" )]
	public Dictionary<string, string> ItemsSourceDictionary
	{
		get => (Dictionary<string, string>)GetValue( ItemsSourceDictionaryProperty );
		set => SetValue( ItemsSourceDictionaryProperty, value );
	}

	public static readonly DependencyProperty ItemsSourceDictionaryProperty =
		DependencyProperty.Register( nameof( ItemsSourceDictionary ), typeof( Dictionary<string, string> ), typeof( Notes ), new FrameworkPropertyMetadata( new Dictionary<string, string>(), ItemsDictionaryPropertyChanged ) );


	[Category( "Options" )]
	public string NewNoteName
	{
		get => (string)GetValue( NewNoteNameProperty );
		set => SetValue( NewNoteNameProperty, value );
	}

	public static readonly DependencyProperty NewNoteNameProperty =
		DependencyProperty.Register( nameof( NewNoteName ), typeof( string ), typeof( Notes ), new FrameworkPropertyMetadata( "New Note" ) );


	[Category( "Common" )]
	public int SelectedIndex
	{
		get => (int)GetValue( SelectedIndexProperty );
		set => SetValue( SelectedIndexProperty, value );
	}

	public static readonly DependencyProperty SelectedIndexProperty =
		DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( Notes ), new FrameworkPropertyMetadata( -1 ) );

	[Category( "Common" )]
	public string SelectedNoteName
	{
		get => (string)GetValue( SelectedNoteNameProperty );
		set => SetValue( SelectedNoteNameProperty, value );
	}

	public static readonly DependencyProperty SelectedNoteNameProperty =
		DependencyProperty.Register( nameof( SelectedNoteName ), typeof( string ), typeof( Notes ), new FrameworkPropertyMetadata( "" ) );
}