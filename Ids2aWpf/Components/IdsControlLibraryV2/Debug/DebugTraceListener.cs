﻿using System.Diagnostics;

namespace IdsControlLibraryV2.Debug;

public static class DebugTrace
{
	internal class Listener : TraceListener
	{
		public override void Write( string message )
		{
		}

		public override void WriteLine( string message )
		{
			Debugger.Break();
		}
	}

	public static void Listen()
	{
		PresentationTraceSources.Refresh();
		PresentationTraceSources.DataBindingSource.Listeners.Add( new ConsoleTraceListener() );
		PresentationTraceSources.DataBindingSource.Listeners.Add( new Listener() );
		PresentationTraceSources.DataBindingSource.Switch.Level = SourceLevels.Warning | SourceLevels.Error;
	}
}