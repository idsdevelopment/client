﻿using System.Windows.Controls.Primitives;

namespace IdsControlLibraryV2.Utils;

/// <summary>
///     Extension methods for DataGrid
///     These methods are thanks to
///     http://blogs.msdn.com/b/vinsibal/archive/2008/11/05/wpf-datagrid-new-item-template-sample.aspx
/// </summary>
public static class DataGridExtensions
{
	public static DataGridCell GetCell( this DataGrid grid, DataGridRow row, int column )
	{
		var Presenter = row?.FindChild<DataGridCellsPresenter>();

		if( !( Presenter is null ) )
		{
			// try to get the cell but it may possibly be virtualized
			var Cell = (DataGridCell)Presenter.ItemContainerGenerator.ContainerFromIndex( column );

			if( !( Cell is null ) )
			{
				// now try to bring into view and retrieve the cell
				var Column = grid.Columns[ column ];

				grid.ScrollIntoView( row, Column );

				Cell = (DataGridCell)Presenter.ItemContainerGenerator.ContainerFromIndex( column );
			}

			return Cell;
		}

		return null;
	}


	/// <summary>
	///     Returns a DataGridCell for the given row and column
	/// </summary>
	/// <param
	///     name="grid">
	///     The DataGrid
	/// </param>
	/// <param
	///     name="row">
	///     The zero-based row index
	/// </param>
	/// <param
	///     name="column">
	///     The zero-based column index
	/// </param>
	/// <returns>The requested DataGridCell, or null if the indices are out of range</returns>
	public static DataGridCell GetCell( this DataGrid grid, int row, int column ) => GetCell( grid, grid.GetRow( row ), column );

	/// <summary>
	///     Gets the DataGridRow based on the given index
	/// </summary>
	/// <param
	///     name="dataGrid">
	/// </param>
	/// <param
	///     name="idx">
	///     The zero-based index of the container to get
	/// </param>
	public static DataGridRow GetRow( this DataGrid dataGrid, int idx )
	{
		var Row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex( idx );

		if( !( Row is null ) )
		{
			// may be virtualized, bring into view and try again
			dataGrid.ScrollIntoView( dataGrid.Items[ idx ] );
			dataGrid.UpdateLayout();

			Row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex( idx );
		}

		return Row;
	}
}