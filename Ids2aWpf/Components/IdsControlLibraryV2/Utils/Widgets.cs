﻿using System.Windows.Media;

namespace Utils;

public static class Widgets
{
	/// <summary>
	///     From
	///     https://stackoverflow.com/questions/874380/wpf-how-do-i-loop-through-the-all-controls-in-a-window
	/// </summary>
	/// <param
	///     name="parent">
	/// </param>
	/// <param
	///     name="recurse">
	/// </param>
	/// <returns></returns>
	public static IEnumerable<Visual> GetChildren( this Visual parent, bool recurse = true )
	{
		if( parent != null )
		{
			var Count = VisualTreeHelper.GetChildrenCount( parent );

			for( var i = 0; i < Count; i++ )
			{
				// Retrieve child visual at specified index value.
				if( VisualTreeHelper.GetChild( parent, i ) is Visual Child )
				{
					yield return Child;

					if( recurse )
					{
						foreach( var GrandChild in Child.GetChildren() )
							yield return GrandChild;
					}
				}
			}
		}
	}

	///// <summary>
	///// 
	///// </summary>
	///// <param name="ctrl"></param>
	///// <param name="message"></param>
	///// <returns></returns>
	//public static string SetErrorState(Control ctrl, string message)
	//{
	//    string error = string.Empty;
	//    if (IsControlEmpty(ctrl))
	//    {
	//        ctrl.Background = Brushes.Yellow;
	//        error = message;
	//    }
	//    else
	//    {
	//        ctrl.Background = Brushes.White;
	//    }

	//    return error;
	//}

	///// <summary>
	///// 
	///// </summary>
	///// <param name="ctrl"></param>
	///// <returns></returns>
	//public static bool IsControlEmpty(Control ctrl)
	//{
	//    bool isEmpty = true;
	//    if (ctrl is TextBox tb)
	//    {
	//        isEmpty = IsTextBoxEmpty(tb);
	//    }
	//    else if (ctrl is ComboBox cb)
	//    {
	//        isEmpty = IsComboBoxSelected(cb);
	//    }
	//    else if (ctrl is DateTimePicker dtp)
	//    {
	//        isEmpty = IsDateTimePickerEmpty(dtp);
	//    }

	//    return isEmpty;
	//}

	///// <summary>
	///// 
	///// </summary>
	///// <param name="tb"></param>
	///// <returns></returns>
	//public static bool IsTextBoxEmpty(TextBox tb)
	//{
	//    bool isEmpty = true;

	//    if (!string.IsNullOrEmpty(tb.Text))
	//    {
	//        isEmpty = false;
	//    }

	//    return isEmpty;
	//}

	///// <summary>
	///// 
	///// </summary>
	///// <param name="cb"></param>
	///// <returns></returns>
	//public static bool IsComboBoxSelected(ComboBox cb)
	//{
	//    bool isEmpty = true;

	//    if (cb.SelectedIndex > -1)
	//    {
	//        isEmpty = false;
	//    }

	//    return isEmpty;
	//}

	//public static bool IsDateTimePickerEmpty(Xceed.Wpf.Toolkit.DateTimePicker dtp)
	//{
	//    bool isEmpty = true;


	//    return isEmpty;
	//}
}