﻿using System.Windows.Markup;

namespace IdsControlLibraryV2.Utils;

public static class Xaml
{
	public static object XamlToObject( this string xaml ) => XamlReader.Parse( xaml );

	public static ItemsPanelTemplate XamlToItemsPanelTemplate( this string xaml ) => XamlToObject( ToXaml( "ItemsPanelTemplate", xaml ) ) as ItemsPanelTemplate;

	public static GridView XamlToGridView( this string xaml ) => XamlToObject( ToXaml( "GridView", xaml ) ) as GridView;

	public static ListView XamlToListView( this string xaml ) => XamlToObject( ToXaml( "ListView", xaml ) ) as ListView;

	public static Border XamlToBorder( this string xaml ) => XamlToObject( ToXaml( "Border", xaml ) ) as Border;

	public static ToolBar XamlToToolBar( this string xaml ) => XamlToObject( ToXaml( "ToolBar", xaml ) ) as ToolBar;

	public static ToolBarTray XamlToToolBarTray( this string xaml ) => XamlToObject( ToXaml( "ToolBarTray", xaml ) ) as ToolBarTray;

	private static string ToXaml( string objectType, string xaml ) => $@"<{objectType} xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
									xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'
									xmlns:system='clr-namespace:System;assembly=mscorlib'>
							{xaml}
					</{objectType}>";
}