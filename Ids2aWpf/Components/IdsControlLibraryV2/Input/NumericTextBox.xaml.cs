﻿using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Windows.Input;

namespace IdsControlLibraryV2;

/// <summary>
///     Interaction logic for NumericTextBox.xaml
/// </summary>
public partial class NumericTextBox
{
	private bool AllowNegative;
	private int  CaretPos;

	private bool IgnoreFocus;

	private bool IgnoreValueChanged;

	private string Mask;

	private string SaveText;

	public event TextValueChangedEvent ValueChanged;

	public NumericTextBox()
	{
		InitializeComponent();
		LayoutRoot.DataContext = this;

		//Force overtype
		var TextEditorProperty = typeof( TextBox ).GetProperty( "TextEditor", BindingFlags.NonPublic | BindingFlags.Instance );

		if( TextEditorProperty != null )
		{
			var TextEditor = TextEditorProperty.GetValue( Input, null );

			// set _OvertypeMode on the TextEditor
			var OvertypeModeProperty = TextEditor.GetType().GetProperty( "_OvertypeMode", BindingFlags.NonPublic | BindingFlags.Instance );

			if( OvertypeModeProperty != null )
				OvertypeModeProperty.SetValue( TextEditor, true, null );
		}
	}

	public delegate void TextValueChangedEvent( NumericTextBox sender, decimal value );

	private static string FixPoint( string str )
	{
		if( str.Contains( "." ) ) // So that 1. is Ok
			str += '0';
		return str;
	}

	private bool IsTextAllowed( string text, out decimal value )
	{
		var L = text.Length;

		if( ( L == 0 ) || ( AllowNegative && ( L == 1 ) && ( text[ 0 ] == '-' ) ) )
		{
			value = 0;
			return true;
		}
		var Ok       = false;
		var DecValue = MinimumValue;

		var Dec = Decimals;

		if( Dec > 0 )
		{
			if( decimal.TryParse( FixPoint( text ), out DecValue ) )
			{
				if( AllowNegative || ( DecValue > 0 ) )
				{
					text = DecValue.ToString( Mask, CultureInfo.InvariantCulture );

					var Pos = text.IndexOf( '.' );

					if( Pos >= 0 )
					{
						var Txt = text.Substring( Pos + 1 );
						Ok = Txt.Length <= Dec;
					}
					else
						Ok = true;
				}
				else
					Ok = true;
			}
		}
		else if( int.TryParse( text, out var IntValue ) )
		{
			DecValue = IntValue;
			Ok       = AllowNegative || ( IntValue >= 0 );
		}

		value = DecValue;

		return Ok && ( DecValue >= MinimumValue ) && ( DecValue <= MaximumValue );
	}

	private void DoSpinner( decimal val )
	{
		if( SpinnerVisible )
		{
			Dispatcher.InvokeAsync( () =>
			                        {
				                        Spinner.DisableDown = val <= MinimumValue;
				                        Spinner.DisableUp   = val >= MaximumValue;
			                        } );
		}
	}

	private void TextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		var Key = e.Text[ 0 ];

		if( Key is >= '0' and <= '9' or '.' or '-' or '+' )
		{
			if( sender is TextBox TxtBox )
			{
				CaretPos = TxtBox.CaretIndex;
				SaveText = TxtBox.Text;
				return;
			}
		}
		e.Handled = true;
	}

	private void Input_TextChanged( object sender, TextChangedEventArgs e )
	{
		if( !IgnoreValueChanged && sender is TextBox TxtBox )
		{
			var Text = TxtBox.Text;
			var Ok   = IsTextAllowed( Text, out _ );
			e.Handled = !Ok;

			if( Ok )
			{
				if( decimal.TryParse( Text, out var V ) )
					ValueChanged?.Invoke( this, V );
			}
			else
			{
				TxtBox.Text       = SaveText;
				TxtBox.CaretIndex = CaretPos;
			}
		}
	}

	private void BuildMask()
	{
		var M = new string( '0', MinimumDigits );
		var D = Decimals;

		if( D > 0 )
			M += $".{new string( '0', D )}";
		Mask = M;
	}

	private void Input_GotFocus( object sender, RoutedEventArgs e )
	{
		if( !IgnoreFocus )
		{
			Dispatcher.InvokeAsync( () =>
			                        {
				                        Input.SelectionStart  = 0;
				                        Input.SelectionLength = Input.Text.Length;
			                        } );
		}
		else
			IgnoreFocus = false;
	}

	private void Spinner_SpinUpClick( object sender, EventArgs e )
	{
		IgnoreFocus =  true;
		Value       += Increment;
	}

	private void Spinner_SpinDownClick( object sender, EventArgs e )
	{
		IgnoreFocus =  true;
		Value       -= Increment;
	}

	private void UserControl_GotFocus( object sender, RoutedEventArgs e )
	{
		Input.Focus();
	}

	private void UserControl_MouseWheel( object sender, MouseWheelEventArgs e )
	{
		if( e.Delta > 0 )
			Spinner_SpinUpClick( sender, e );
		else
			Spinner_SpinDownClick( sender, e );
	}

	private void UserControl_Loaded( object sender, RoutedEventArgs e )
	{
		BuildMask();
		var V = Value;
		DoSpinner( V );
		Input.Text = V.ToString( Mask, CultureInfo.InvariantCulture );
	}

	private void UserControl_LostFocus( object sender, RoutedEventArgs e )
	{
		Input.Text = FixPoint( Input.Text );
		Value      = Value; // Cause Changed event
	}

	private static void BorderThicknessPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
	{
		if( d is NumericTextBox N && e.NewValue is Thickness T )
			N.Input.BorderThickness = T;
	}

	private static void DecimalsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
		if( dependencyObject is NumericTextBox InBox )
			InBox.BuildMask();
	}

	private static object DecimalsCoerceValueCallback( DependencyObject dependencyObject, object baseValue )
	{
		if( baseValue is int Value )
		{
			if( Value < 0 )
				Value = 0;

			if( Value > 10 )
				Value = 10;

			baseValue = Value;
		}
		return baseValue;
	}


	// Using a DependencyProperty as the backing store for MaximumValue.  This enables animation, styling, binding, etc...


	private static void MaximumValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
	}

	private static object MaximumValueCoerceValueCallback( DependencyObject dependencyObject, object baseValue )
	{
		if( dependencyObject is NumericTextBox InBox && baseValue is decimal Value )
		{
			var Min = InBox.MinimumValue;

			if( Value < Min )
				Value = Min;

			baseValue = Value;

			InBox.AllowNegative = Value < 0;
		}
		return baseValue;
	}


	// Using a DependencyProperty as the backing store for Decimals.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for MinimumDigits.  This enables animation, styling, binding, etc...


	private static void MinimumDigitsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
		if( dependencyObject is NumericTextBox InBox )
			InBox.BuildMask();
	}

	private static object MinimumDigitsCoerceValueCallback( DependencyObject dependencyObject, object baseValue )
	{
		if( baseValue is int Value )
		{
			if( Value < 1 )
				Value = 1;

			if( Value > 10 )
				Value = 10;

			baseValue = Value;
		}
		return baseValue;
	}


	// Using a DependencyProperty as the backing store for MinimumValue.  This enables animation, styling, binding, etc...


	private static void MinimumValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
	}

	private static object MinimumValueCoerceValueCallback( DependencyObject dependencyObject, object baseValue )
	{
		if( dependencyObject is NumericTextBox InBox && baseValue is decimal Value )
		{
			var MaxValue = InBox.MaximumValue;

			if( Value > MaxValue )
				Value = MaxValue;
			baseValue = Value;
		}
		return baseValue;
	}

	private static void ValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
		if( dependencyObject is NumericTextBox InBox && dependencyPropertyChangedEventArgs.NewValue is decimal V )
		{
			if( !InBox.IgnoreValueChanged )
			{
				var Txt = V.ToString( InBox.Mask, CultureInfo.InvariantCulture );
				InBox.Input.Text = Txt;
				InBox.DoSpinner( V );

				try
				{
					InBox.ValueChanged?.Invoke( InBox, V );
				}
				catch
				{
				}
			}
		}
	}

	private static object ValueCoerceValueCallback( DependencyObject dependencyObject, object baseValue )
	{
		if( dependencyObject is NumericTextBox InBox && baseValue is decimal Value )
			baseValue = Math.Max( Math.Min( Math.Round( Value, InBox.Decimals, MidpointRounding.AwayFromZero ), InBox.MaximumValue ), InBox.MinimumValue );
		return baseValue;
	}


	public new Thickness BorderThickness
	{
		get => (Thickness)GetValue( BorderThicknessProperty );
		set => SetValue( BorderThicknessProperty, value );
	}

	// Using a DependencyProperty as the backing store for BorderThickness.  This enables animation, styling, binding, etc...
	public new static readonly DependencyProperty BorderThicknessProperty =
		DependencyProperty.Register( nameof( BorderThickness ), typeof( Thickness ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( new Thickness( 1 ), BorderThicknessPropertyChangedCallback ) );

	[Category( "Options" )]
	public int Decimals
	{
		get => (int)GetValue( DecimalsProperty );
		set => SetValue( DecimalsProperty, value );
	}

	public static readonly DependencyProperty MinimumDigitsProperty =
		DependencyProperty.Register( nameof( MinimumDigits ), typeof( int ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( 1, MinimumDigitsPropertyChangedCallback, MinimumDigitsCoerceValueCallback ) );

	public static readonly DependencyProperty DecimalsProperty =
		DependencyProperty.Register( nameof( Decimals ), typeof( int ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( 0, DecimalsPropertyChangedCallback, DecimalsCoerceValueCallback ) );

	[Category( "Options" )]
	public decimal Increment
	{
		get => (decimal)GetValue( IncrementProperty );
		set => SetValue( IncrementProperty, Math.Abs( value ) );
	}

	public static readonly DependencyProperty ValueProperty =
		DependencyProperty.Register( nameof( Value ), typeof( decimal ), typeof( NumericTextBox ),
		                             new FrameworkPropertyMetadata( (decimal)0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ValuePropertyChangedCallback, ValueCoerceValueCallback ) );

	public static readonly DependencyProperty MinimumValueProperty =
		DependencyProperty.Register( nameof( MinimumValue ), typeof( decimal ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( decimal.MinValue, MinimumValuePropertyChangedCallback, MinimumValueCoerceValueCallback ) );

	public static readonly DependencyProperty MaximumValueProperty =
		DependencyProperty.Register( nameof( MaximumValue ), typeof( decimal ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( decimal.MaxValue, MaximumValuePropertyChangedCallback, MaximumValueCoerceValueCallback ) );

	public static readonly DependencyProperty IncrementProperty =
		DependencyProperty.Register( nameof( Increment ), typeof( decimal ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( (decimal)1 ) );

	[Category( "Options" )]
	public bool IsCurrency
	{
		get => (bool)GetValue( IsCurrencyProperty );
		set => SetValue( IsCurrencyProperty, value );
	}

// Using a DependencyProperty as the backing store for IsCurrency.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty IsCurrencyProperty =
		DependencyProperty.Register( nameof( IsCurrency ), typeof( bool ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( false ) );


	[Category( "Options" )]
	public decimal MaximumValue
	{
		get => (decimal)GetValue( MaximumValueProperty );
		set => SetValue( MaximumValueProperty, value );
	}


	[Category( "Options" )]
	public int MinimumDigits
	{
		get => (int)GetValue( MinimumDigitsProperty );
		set => SetValue( MinimumDigitsProperty, value );
	}


	[Category( "Options" )]
	public decimal MinimumValue
	{
		get => (decimal)GetValue( MinimumValueProperty );
		set
		{
			var MaxValue = MaximumValue;

			if( value > MaxValue )
				Value = MaxValue;
			SetValue( MinimumValueProperty, value );
		}
	}


	[Category( "Options" )]
	public bool SpinnerVisible
	{
		get => (bool)GetValue( SpinnerVisibleProperty );
		set => SetValue( SpinnerVisibleProperty, value );
	}

	// Using a DependencyProperty as the backing store for SpinnerVisible.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty SpinnerVisibleProperty = DependencyProperty.Register( nameof( SpinnerVisible ), typeof( bool ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( true ) );

	[Category( "Options" )]
	public decimal Value
	{
		get
		{
			if( !decimal.TryParse( Input.Text.Trim(), out var Vlue ) )
				Vlue = 0;
			IgnoreValueChanged = true;

			try
			{
				var Txt = Vlue.ToString( Mask, CultureInfo.InvariantCulture );
				Input.Text = Txt;
			}
			catch
			{
			}
			finally
			{
				IgnoreValueChanged = false;
			}
			return Vlue;
		}
		set => SetValue( ValueProperty, value );
	}
}