﻿using System.Windows.Input;

namespace IdsControlLibraryV2.Input;

public static class SendKeys
{
	/// <summary>
	///     Sends the specified key to the currently focused control.
	/// </summary>
	/// <param
	///     name="key">
	///     The key.
	/// </param>
	public static void Send( Key key )
	{
		if( Keyboard.PrimaryDevice != null )
		{
			if( Keyboard.PrimaryDevice.ActiveSource != null )
			{
				var KeyEventArgs = new KeyEventArgs( Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, key )
				                   {
					                   RoutedEvent = Keyboard.KeyDownEvent
				                   };
				InputManager.Current.ProcessInput( KeyEventArgs );

				// Note: Based on your requirements you may also need to fire events for:
				// RoutedEvent = Keyboard.PreviewKeyDownEvent
				// RoutedEvent = Keyboard.KeyUpEvent
				// RoutedEvent = Keyboard.PreviewKeyUpEvent
			}
		}
	}
}