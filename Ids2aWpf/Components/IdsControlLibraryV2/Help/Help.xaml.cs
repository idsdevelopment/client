﻿#nullable enable

using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using IdsControlLibraryV2.Utils;

namespace IdsControlLibraryV2;

/// <summary>
///     Interaction logic for Help.xaml
/// </summary>
public partial class Help
{
	private System.Windows.Window? Window;

	~Help()
	{
		if( Window is not null )
			Window.PreviewKeyDown -= WindowOnPreviewKeyDown;
	}

	public Help()
	{
		InitializeComponent();

		if( !DesignerProperties.GetIsInDesignMode( this ) )
			HelpLabel.Visibility = Visibility.Collapsed;
	}

	private void HelpLabel_Loaded( object sender, RoutedEventArgs e )
	{
		if( IsEnabled )
		{
			Window = this.FindParent<System.Windows.Window>();

			if( Window is not null )
				Window.PreviewKeyDown += WindowOnPreviewKeyDown;
		}
	}

	private void WindowOnPreviewKeyDown( object? sender, KeyEventArgs? e )
	{
		if( e is null || e.Key == Key.F1 )
		{
			if( e is not null )
				e.Handled = true;

			IsEnabled = false;

			try
			{
				var Url = Uri;

				if( !string.IsNullOrEmpty( Url ) )
					Process.Start( Url );
			}
			finally
			{
				IsEnabled = true;
			}
		}
	}

	public string Uri
	{
		get => (string)GetValue( UriProperty );
		set => SetValue( UriProperty, value.Trim() );
	}

	// Using a DependencyProperty as the backing store for Uri.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty UriProperty = DependencyProperty.Register( nameof( Uri ), typeof( string ), typeof( Help ), new PropertyMetadata( "" ) );

	public void Show()
	{
		WindowOnPreviewKeyDown( null, null );
	}
}