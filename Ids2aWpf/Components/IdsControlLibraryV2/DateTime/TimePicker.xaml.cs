﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Annotations;

namespace IdsControlLibraryV2;

public partial class TimePicker : INotifyPropertyChanged
{
	private const string A  = "A",
	                     AM = "AM",
	                     PM = "PM";

	private bool LoadingValue
	{
		get => _LoadingValue || !Initialised;
		set => _LoadingValue = value;
	}

	private enum SPIN_STATE
	{
		HOURS,
		MINS,
		SECS,
		AM_PM
	}

	private SPIN_STATE State = SPIN_STATE.HOURS;

	private bool _LoadingValue;
	private bool Initialised;

	public event ValueChangedEvent? ValueChanged;

	public TimePicker()
	{
		InitializeComponent();
	}

	public delegate void ValueChangedEvent( TimePicker sender, DateTime newValue );

	private static void ShowSecondsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
		if( dependencyObject is TimePicker Picker && dependencyPropertyChangedEventArgs.NewValue is bool Show )
		{
			if( !Show )
			{
				Picker.Seconds.Visibility        = Visibility.Collapsed;
				Picker.SecondsDivider.Visibility = Visibility.Collapsed;
			}
			else
			{
				Picker.Seconds.Visibility        = Visibility.Visible;
				Picker.SecondsDivider.Visibility = Visibility.Visible;
			}
		}
	}

	private static void AmPmPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
		if( dependencyObject is TimePicker Picker )
			Picker.Hours.MaximumValue = (bool)dependencyPropertyChangedEventArgs.NewValue ? 11 : 23;
	}

	private void ResetBackgrounds()
	{
		Hours.Background     = Background;
		Minutes.Background   = Background;
		Seconds.Background   = Background;
		AmPmInput.Background = Background;
	}

	private void Hours_GotFocus( object sender, RoutedEventArgs e )
	{
		ResetBackgrounds();
		State            = SPIN_STATE.HOURS;
		Hours.Background = SelectedBackground;
	}

	private void Minutes_GotFocus( object sender, RoutedEventArgs e )
	{
		ResetBackgrounds();
		State              = SPIN_STATE.MINS;
		Minutes.Background = SelectedBackground;
	}

	private void Seconds_GotFocus( object sender, RoutedEventArgs e )
	{
		ResetBackgrounds();
		State              = SPIN_STATE.SECS;
		Seconds.Background = SelectedBackground;
	}

	private void AmPmInput_GotFocus( object sender, RoutedEventArgs e )
	{
		ResetBackgrounds();
		AmPmInput.SelectAll();
		State                = SPIN_STATE.AM_PM;
		AmPmInput.Background = SelectedBackground;
	}

	private void SwapAmPm()
	{
		if( AmPm )
		{
			var Input = AmPmInput;
			Input.Text = Input.Text is A or AM ? PM : AM;
			LostFocus( this, null );
		}
	}

	private void Spinner_SpinUpClick( object sender, EventArgs e )
	{
		switch( State )
		{
		case SPIN_STATE.HOURS:
			Value = Value.AddHours( 1 );
			break;

		case SPIN_STATE.MINS:
			Value = Value.AddMinutes( 1 );
			break;

		case SPIN_STATE.SECS:
			if( ShowSeconds )
				Value = Value.AddSeconds( 1 );
			break;

		case SPIN_STATE.AM_PM:
			SwapAmPm();
			break;
		}
	}

	private void Spinner_SpinDownClick( object sender, EventArgs e )
	{
		var V = Value.AddDays( 1 ); // Stop -ve underflow

		switch( State )
		{
		case SPIN_STATE.HOURS:
			Value = V.AddHours( -1 );
			break;

		case SPIN_STATE.MINS:
			Value = V.AddMinutes( -1 );
			break;

		case SPIN_STATE.SECS:
			if( ShowSeconds )
				Value = V.AddSeconds( -1 );
			break;

		case SPIN_STATE.AM_PM:
			SwapAmPm();
			break;
		}
	}

	private void AmPmInput_PreviewKeyDown( object sender, KeyEventArgs e )
	{
		if( e.Key != Key.Tab )
		{
			e.Handled = true;
			Value     = Value.AddHours( 12 );
		}
	}

	private void AmPmInput_PreviewKeyUp( object sender, KeyEventArgs e )
	{
		e.Handled = true;
	}

	private void UserControl_Loaded( object sender, RoutedEventArgs e )
	{
		Initialised = true;
	}

	private void AmPmInput_MouseWheel( object sender, MouseWheelEventArgs e )
	{
		e.Handled = true;
		Value     = Value.AddHours( 12 );
	}

	private void AmPmInput_LostFocus( object sender, RoutedEventArgs e )
	{
		ResetBackgrounds();
	}

	[Category( "Options" )]
	public bool AmPm
	{
		get => (bool)GetValue( AmPmProperty );
		set => SetValue( AmPmProperty, value );
	}

	public static readonly DependencyProperty SelectedBackgroundProperty =
		DependencyProperty.Register( nameof( SelectedBackground ), typeof( Brush ), typeof( TimePicker ), new FrameworkPropertyMetadata( Brushes.PowderBlue ) );

	public static readonly DependencyProperty ShowSecondsProperty =
		DependencyProperty.Register( nameof( ShowSeconds ), typeof( bool ), typeof( TimePicker ), new FrameworkPropertyMetadata( true, ShowSecondsPropertyChangedCallback ) );

	public static readonly DependencyProperty AmPmProperty =
		DependencyProperty.Register( nameof( AmPm ), typeof( bool ), typeof( TimePicker ), new FrameworkPropertyMetadata( true, AmPmPropertyChangedCallback ) );

	[Category( "Brush" )]
	public Brush SelectedBackground
	{
		get => (Brush)GetValue( SelectedBackgroundProperty );
		set => SetValue( SelectedBackgroundProperty, value );
	}


	// Using a DependencyProperty as the backing store for AmPm.  This enables animation, styling, binding, etc...


	// Using a DependencyProperty as the backing store for SelectedBackground.  This enables animation, styling, binding, etc...


	[Category( "Options" )]
	public bool ShowSeconds
	{
		get => (bool)GetValue( ShowSecondsProperty );
		set => SetValue( ShowSecondsProperty, value );
	}


	// Using a DependencyProperty as the backing store for ShowSeconds.  This enables animation, styling, binding, etc...


#region Value
	private static readonly TimeSpan Hours12 = TimeSpan.FromHours( 12 );
	private                 bool     InChangeValue;

	private DateTime GetValue()
	{
		var Hour = (int)Hours.Value;

		var Span = new TimeSpan( Hour, (int)Minutes.Value, (int)Seconds.Value );

		if( AmPm )
		{
			if( AmPmInput.Text.Trim() == PM )
			{
				if( Hour != 12 )
					Span = Span.Add( Hours12 );
			}
			else // AM
			{
				if( Hour == 12 )
					Span = Span.Subtract( Hours12 );
			}
		}

		return new DateTime( Span.Ticks );
	}

	[Category( "Options" )]
	public DateTime Value
	{
		get => GetValue();

		set
		{
			if( !LoadingValue )
			{
				LoadingValue = true;

				try
				{
					SetValue( ValueProperty, value );
					ValueChanged?.Invoke( this, Value );
				}
				finally
				{
					LoadingValue = false;
				}
			}
		}
	}

	// Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
	public static readonly DependencyProperty ValueProperty = DependencyProperty.Register( nameof( Value ), typeof( DateTime ), typeof( TimePicker ),
	                                                                                       new FrameworkPropertyMetadata( DateTime.MaxValue,
	                                                                                                                      FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
	                                                                                                                      ValuePropertyChangedCallback
	                                                                                                                    ) );

	private new void LostFocus( object sender, RoutedEventArgs? _ )
	{
		Value = GetValue();
		OnPropertyChanged( nameof( Value ) );
		ValueChanged?.Invoke( this, Value );
	}

	private static void ValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
	{
		// ReSharper disable once MergeIntoPattern
		if( dependencyObject is TimePicker Picker && !Picker.InChangeValue )
		{
			Picker.InChangeValue = true;

			try
			{
				var Value = (DateTime)dependencyPropertyChangedEventArgs.NewValue;
				var Hour  = Value.Hour;

				string PickerText;

				if( Picker.AmPm )
				{
					if( Value.TimeOfDay > Hours12 )
					{
						if( Hour >= 13 )
							Hour -= 12;

						PickerText = PM;
					}
					else
					{
						if( Hour == 0 )
							Hour = 12;

						PickerText = AM;
					}
				}
				else
					PickerText = "";

				Picker.AmPmInput.Text = PickerText;

				Picker.Hours.Value   = Hour;
				Picker.Minutes.Value = Value.Minute;
				Picker.Seconds.Value = Value.Second;
			}
			finally
			{
				Picker.InChangeValue = false;
			}
		}
	}
#endregion

#region Property Changed
	public event PropertyChangedEventHandler? PropertyChanged;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}
#endregion
}