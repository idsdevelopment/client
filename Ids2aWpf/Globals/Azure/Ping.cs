﻿using System.Timers;
using System.Windows;
using System.Windows.Controls;
using IdsControlLibraryV2.Utils;
using IdsControlLibraryV2.Window;

namespace Ids2aWpf;

internal static partial class Globals
{
	public static class Azure
	{
		public static class Ping
		{
			public const int PING_INTERVAL_IN_SECONDS     = 2,
			                 OFFLINE_FLASH_INTERVAL_IN_MS = 500;


			internal class SaveWindowData
			{
				internal object Tag;
				internal object Title;

				internal Brush TitleBrush,
				               IconsBrush;

				internal bool                      WasErrorColour = true;
				internal UniversalFormsStyleWindow Window;
			}

			private static readonly Timer PingTimer = new( PING_INTERVAL_IN_SECONDS * 1000 );
			private static          Timer FlashTimer;

			private static bool PrevOnline = true;

			private static SolidColorBrush OffLineBrush,
			                               OfflineForegroundBrush;

			private static string OffLineText;

			public static event PingEvent OnPing;

			public static void StartPing()
			{
				OffLineBrush           = (SolidColorBrush)Application.Current.FindResource( "OfflineBrush" );
				OfflineForegroundBrush = (SolidColorBrush)Application.Current.FindResource( "OfflineForegroundBrush" );

				OffLineText = (string)Application.Current.FindResource( "Offline" );

				OnPing += online =>
				          {
					          if( online != PrevOnline )
					          {
						          PrevOnline = online;

						          // ReSharper disable once PossibleNullReferenceException
						          Application.Current.Dispatcher.Invoke( () =>
						                                                 {
							                                                 var Windows = Application.Current.Windows.OfType<Window>();

							                                                 if( !online )
							                                                 {
								                                                 // ReSharper disable once PossibleMultipleEnumeration
								                                                 foreach( var W in Windows )
								                                                 {
									                                                 var Uw = W.FindChild<UniversalFormsStyleWindow>();

									                                                 if( Uw != null )
									                                                 {
										                                                 W.Tag = new SaveWindowData
										                                                         {
											                                                         Tag        = W.Tag,
											                                                         Title      = Uw.TitleBarContent,
											                                                         TitleBrush = Uw.TitleBarBackground,
											                                                         IconsBrush = Uw.SystemIconsBackground,
											                                                         Window     = Uw
										                                                         };

										                                                 Uw.TitleBarContent = new Label
										                                                                      {
											                                                                      Content    = OffLineText,
											                                                                      Foreground = OfflineForegroundBrush,
											                                                                      Background = Brushes.Transparent,
											                                                                      FontWeight = FontWeights.Bold
										                                                                      };
									                                                 }
								                                                 }

								                                                 FlashTimer = new Timer( OFFLINE_FLASH_INTERVAL_IN_MS );

								                                                 FlashTimer.Elapsed += ( sender, args ) =>
								                                                                       {
									                                                                       if( Application.Current != null )
									                                                                       {
										                                                                       Application.Current.Dispatcher.Invoke( () =>
										                                                                                                              {
											                                                                                                              // ReSharper disable once PossibleMultipleEnumeration
											                                                                                                              foreach( var Window in Windows )
											                                                                                                              {
												                                                                                                              if( Window.Tag is SaveWindowData Sd )
												                                                                                                              {
													                                                                                                              Sd.WasErrorColour = !Sd.WasErrorColour;

													                                                                                                              if( Sd.WasErrorColour )
													                                                                                                              {
														                                                                                                              Sd.Window.TitleBarBackground    = Sd.TitleBrush;
														                                                                                                              Sd.Window.SystemIconsBackground = Sd.IconsBrush;
													                                                                                                              }
													                                                                                                              else
													                                                                                                              {
														                                                                                                              Sd.Window.TitleBarBackground    = OffLineBrush;
														                                                                                                              Sd.Window.SystemIconsBackground = OffLineBrush;
													                                                                                                              }
												                                                                                                              }
											                                                                                                              }
										                                                                                                              } );
									                                                                       }
								                                                                       };

								                                                 FlashTimer.Start();
							                                                 }

							                                                 else
							                                                 {
								                                                 if( FlashTimer != null )
								                                                 {
									                                                 FlashTimer.Stop();
									                                                 FlashTimer.Dispose();
									                                                 FlashTimer = null;
								                                                 }

								                                                 foreach( var Window in Windows )
								                                                 {
									                                                 if( Window.Tag is SaveWindowData Sd )
									                                                 {
										                                                 Window.Tag                      = Sd.Tag;
										                                                 Sd.Window.TitleBarBackground    = Sd.TitleBrush;
										                                                 Sd.Window.TitleBarContent       = Sd.Title;
										                                                 Sd.Window.SystemIconsBackground = Sd.IconsBrush;
									                                                 }
								                                                 }
							                                                 }
						                                                 } );
					          }
				          };

				PingTimer.Elapsed += async ( sender, args ) =>
				                     {
					                     bool OnLine;

					                     try
					                     {
						                     var RetVal = await AzureRemoteService.Azure.Client.RequestPing();
						                     OnLine = RetVal == "OK";
					                     }
					                     catch( Exception Exception )
					                     {
						                     Logging.WriteLogLine( $"Ping:\r\n{Exception}" );
						                     OnLine = false;
					                     }

					                     OnPing?.Invoke( OnLine );
				                     };

				PingTimer.Start();
			}

			public delegate void PingEvent( bool online );
		}
	}
}