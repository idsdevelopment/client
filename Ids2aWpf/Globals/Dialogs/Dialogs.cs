﻿using System.Windows;
using ViewModels.Dialogues;

namespace Ids2aWpf;

internal static partial class Globals
{
	public static class Dialogs
	{
		public static bool Confirm( string text, bool isKey = true )
		{
			if( isKey )
				text = Dictionary.AsString( text );

			return new Confirm( Application.Current.MainWindow, text ).ShowDialog();
		}
	}
}