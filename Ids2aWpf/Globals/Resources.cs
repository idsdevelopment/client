﻿using System.Windows;

namespace Ids2aWpf;

internal static partial class Globals
{
	internal static class Dictionary
	{
		private static void SetResource( string key, object value )
		{
			try
			{
				Application.Current.Resources[ key ] = value;
			}
			catch( Exception E )
			{
				Console.WriteLine( E );
			}
		}

		internal static string AsString( string key )
		{
			try
			{
				return (string)Application.Current.FindResource( key );
			}
			catch
			{
				return key;
			}
		}

		internal static Color AsColor( string key )
		{
			try
			{
				// ReSharper disable once PossibleNullReferenceException
				return (Color)Application.Current.FindResource( key );
			}
			catch
			{
				return Colors.Red;
			}
		}

		internal static void SetColor( string key, Color color )
		{
			MainWindow.Instance?.Dispatcher?.Invoke( () =>
												     {
													     SetResource( key, new Color
																           {
																	           A = color.A,
																	           R = color.R,
																	           G = color.G,
																	           B = color.B
																           } );
												     } );
		}

		internal static void SetSolidBrush( string key, SolidColorBrush value )
		{
			var Color = value.Color;

			MainWindow.Instance?.Dispatcher?.Invoke( () =>
												     {
													     SetResource( key, new SolidColorBrush( new Color
																					            {
																						            A = Color.A,
																						            R = Color.R,
																						            G = Color.G,
																						            B = Color.B
																					            } ) );
												     } );
		}

		internal static SolidColorBrush AsSolidColorBrush( string key )
		{
			try
			{
				// ReSharper disable once PossibleNullReferenceException
				return (SolidColorBrush)Application.Current.FindResource( key );
			}
			catch
			{
				return new SolidColorBrush( Colors.Red );
			}
		}
	}
}