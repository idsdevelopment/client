﻿using ViewModels.MainWindow;

namespace Ids2aWpf;

internal static partial class Globals
{
	private const string LANGUAGE_OVERRIDE = "~~~LANGUAGE_OVERRIDE~~~";

	internal const string LANGUAGE_BASE = "Resources/Dictionaries/Languages";

	internal class DictPath
	{
		internal string BasePath;

		// ReSharper disable once MemberHidesStaticFromOuterClass
		internal List<string> Dictionaries;
	}

	public static MainWindowModel.LANGUAGE Language
	{
		get => _Language;
		set
		{
			_Language = value;

			if( LanguageDictionary.TryGetValue( value, out var Path ) )
			{
				var Base = $"{LANGUAGE_BASE}/{Path.BasePath}/";

				Dictionaries.RemoveResourceDictionaryContainingKey( LANGUAGE_OVERRIDE );

				if( value != MainWindowModel.LANGUAGE.DEFAULT )
				{
					foreach( var Dict in Path.Dictionaries )
						Dictionaries.MergedResourceDictionary( $"{Base}{Dict}" ).Add( LANGUAGE_OVERRIDE, 0 );
				}
			}
		}
	}

	private static MainWindowModel.LANGUAGE _Language;


	private static readonly Dictionary<MainWindowModel.LANGUAGE, DictPath> LanguageDictionary = new()
	                                                                                            {
		                                                                                            {
			                                                                                            MainWindowModel.LANGUAGE.AUSTRALIAN, new DictPath
			                                                                                                                                 {
				                                                                                                                                 BasePath = "Australian",
				                                                                                                                                 Dictionaries = new List<string>
				                                                                                                                                                {
					                                                                                                                                                "_General.xaml"
				                                                                                                                                                }
			                                                                                                                                 }
		                                                                                            },

		                                                                                            {
			                                                                                            MainWindowModel.LANGUAGE.CANADIAN, new DictPath
			                                                                                                                               {
				                                                                                                                               BasePath = "Canadian",
				                                                                                                                               Dictionaries = new List<string>
				                                                                                                                                              {
					                                                                                                                                              "_General.xaml"
				                                                                                                                                              }
			                                                                                                                               }
		                                                                                            },
		                                                                                            {
			                                                                                            MainWindowModel.LANGUAGE.US, new DictPath
			                                                                                                                         {
				                                                                                                                         BasePath = "Us",
				                                                                                                                         Dictionaries = new List<string>
				                                                                                                                                        {
					                                                                                                                                        "_General.xaml"
				                                                                                                                                        }
			                                                                                                                         }
		                                                                                            }
	                                                                                            };
}