﻿namespace Ids2aWpf;

internal static partial class Globals
{
	public static class Login
	{
		public static bool _SignedIn;

		public static string Account, UserName;

		public static bool SignedIn
		{
			get => _SignedIn;
			set
			{
				_SignedIn = value;

				if( value )
					SignedInTime = DateTime.Now;
			}
		}


		public static  TimeSpan AmountOfTimeLoggedIn => DateTime.Now - SignedInTime;
		private static DateTime SignedInTime = DateTime.Now;
	}
}