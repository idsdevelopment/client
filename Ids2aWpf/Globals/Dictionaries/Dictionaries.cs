﻿using System.Windows;

namespace Ids2aWpf;

internal static partial class Globals
{
	public static class Dictionaries
	{
		public static ResourceDictionary MergedResourceDictionary( Collection<ResourceDictionary> resourceDictionaries, string dictionaryXaml )
		{
			var Dictionary = new ResourceDictionary
			                 {
				                 Source = new Uri( dictionaryXaml, UriKind.Relative )
			                 };

			resourceDictionaries.Add( Dictionary );
			return Dictionary;
		}

		public static Collection<ResourceDictionary> CloneMergedResourceDictionary( Collection<ResourceDictionary> mergedResourceDictionary )
		{
			var Result = new Collection<ResourceDictionary>();

			foreach( var ResourceDictionary in mergedResourceDictionary )
				Result.Add( ResourceDictionary );

			return Result;
		}

		public static Collection<ResourceDictionary> RemoveResourceDictionaryContainingKey( Collection<ResourceDictionary> mergedResourceDictionary, string key )
		{
			var C = mergedResourceDictionary.Count;

			for( var I = C; --I >= 0; )
			{
				var Dict = mergedResourceDictionary[ I ];

				if( Dict.Contains( key ) )
					mergedResourceDictionary.Remove( Dict );
			}
			return mergedResourceDictionary;
		}

		public static Collection<ResourceDictionary> RemoveResourceDictionaryContainingKey( string key ) => RemoveResourceDictionaryContainingKey( Application.Current.Resources.MergedDictionaries, key );

		public static ResourceDictionary MergedResourceDictionary( string dictionaryXaml ) => MergedResourceDictionary( Application.Current.Resources.MergedDictionaries, dictionaryXaml );
	}
}