﻿namespace Ids2aWpf;

internal static partial class Globals
{
	public static partial class Terminologies
	{
		public const string PML = "PML";

		public static DictPath PmlTerminology => new()
		                                         {
			                                         BasePath     = "Pml",
			                                         Dictionaries = new List<string>()
		                                         };
	}
}