﻿using ViewModels.MainWindow;

namespace Ids2aWpf;

internal static partial class Globals
{
	public static partial class Terminologies
	{
		internal const string TERMINOLOGY_BASE     = "Resources/Dictionaries/Terminologies";
		private const  string TERMINOLOGY_OVERRIDE = "~~~TERMINOLOGY_OVERRIDE~~~";

		public static string Terminology
		{
			get => _Terminology;
			set
			{
				value        = value.ToUpper();
				_Terminology = value;

				if( !value.IsNullOrWhiteSpace() )
				{
					if( TerminologyDictionary.TryGetValue( Language, out var LDict ) )
					{
						if( LDict.TryGetValue( value, out var TDict ) )
						{
							var Base = $"{TERMINOLOGY_BASE}/{TDict.BasePath}/";
							Dictionaries.RemoveResourceDictionaryContainingKey( TERMINOLOGY_OVERRIDE );

							foreach( var Dict in TDict.Dictionaries )
								Dictionaries.MergedResourceDictionary( $"{Base}{Dict}" ).Add( TERMINOLOGY_OVERRIDE, 0 );
						}
					}
				}
			}
		}

		private static readonly Dictionary<MainWindowModel.LANGUAGE, Dictionary<string, DictPath>> TerminologyDictionary = new()
		                                                                                                                   {
			                                                                                                                   {
				                                                                                                                   MainWindowModel.LANGUAGE.AUSTRALIAN, new Dictionary<string, DictPath>
				                                                                                                                                                        {
					                                                                                                                                                        {PML, PmlTerminology}
				                                                                                                                                                        }
			                                                                                                                   }
		                                                                                                                   };

		private static string _Terminology;
	}
}