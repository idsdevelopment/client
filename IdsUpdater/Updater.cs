﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Timers;
using Utils;
using File = System.IO.File;

namespace IdsUpdater;

public class Updater
{
	private const string BASE_URI          = "https://idsroute.blob.core.windows.net/downloads",
	                     VERSION_FILE_NAME = "CurrentVersion.txt",
	                     DEFAULT_RESELLER  = "Default",
	                     UPDATES_FOLDER    = @"Ids\Software Updates\";

	private const int DEFAULT_POLLING_INTERVAL_IN_MINUTES = 60;

	private readonly uint CurrentVersionNumber;

	private Uri VersionUri,
	            InstallExeUri;

	private          string InstallFileName;
	private readonly string InstallerExeName;

	private Timer CheckForUpdateTimer;

	private bool Installing;

	~Updater()
	{
		KillTimer();

		if( !Installing && !string.IsNullOrEmpty( InstallFileName ) )
			File.Delete( InstallFileName );
	}

	public void RunInstall()
	{
		Installing = true;
		Process.Start( InstallFileName )?.WaitForInputIdle( 10000 );
	}

	public Updater( uint currentVersionNumber, string versionTextFileName, string baseFolder, string installerExeName, Action<Updater, uint> onUpdateAvailable, string resellerAccountId = DEFAULT_RESELLER,
	                int pollingIntervalInMinutes = DEFAULT_POLLING_INTERVAL_IN_MINUTES )
	{
		pollingIntervalInMinutes *= 60 * 1000; // To Ms

		var Pos = resellerAccountId.IndexOf( '/' ); // Ids special login

		if( Pos >= 0 )
			resellerAccountId = resellerAccountId.Substring( Pos + 1 );

		resellerAccountId = resellerAccountId.Capitalise();

		CurrentVersionNumber = currentVersionNumber;
		InstallerExeName     = installerExeName.Trim();
		baseFolder           = baseFolder.TrimEnd( '/' );

		void BuildBaseUri( string reseller )
		{
			var BaseUri = $"{BASE_URI}/{baseFolder}/{reseller}/";
			VersionUri    = new Uri( $"{BaseUri}{versionTextFileName}" );
			InstallExeUri = new Uri( $"{BaseUri}{installerExeName}" );
		}

		void CheckUpdate()
		{
			BuildBaseUri( resellerAccountId );

			if( !CheckForUpdate( onUpdateAvailable ) )
			{
				if( resellerAccountId != DEFAULT_RESELLER )
				{
					BuildBaseUri( DEFAULT_RESELLER );
					CheckForUpdate( onUpdateAvailable );
				}
			}
		}

		void Run()
		{
			Tasks.RunVoid( () =>
			               {
				               CheckUpdate();

				               if( pollingIntervalInMinutes > 0 )
					               Tasks.RunVoid( pollingIntervalInMinutes, Run );
			               } );
		}

		Run();
	}

	public Updater( uint currentVersionNumber, string baseFolder, string installerExeName, Action<Updater, uint> onUpdateAvailable, string resellerAccountId = DEFAULT_RESELLER,
	                int pollingIntervalInMinutes = DEFAULT_POLLING_INTERVAL_IN_MINUTES )
		: this( currentVersionNumber, VERSION_FILE_NAME, baseFolder, installerExeName, onUpdateAvailable, resellerAccountId, pollingIntervalInMinutes )
	{
	}

	private bool CheckForUpdate( Action<Updater, uint> onUpdateAvailable )
	{
		var RetVal = false;

		try
		{
			byte[] Bytes;

			using( var Client = new HttpClient {Timeout = new TimeSpan( 0, 0, 0, 10 )} )
				Bytes = Client.GetByteArrayAsync( VersionUri ).Result;

			RetVal = true;

			var Version = Encoding.UTF8.GetString( Bytes, 3, Bytes.Length - 3 ).Trim();

			if( uint.TryParse( Version, out var ServerVersion ) )
			{
				if( ServerVersion > CurrentVersionNumber )
				{
					byte[] InstallBytes;

					using( var Client = new HttpClient {Timeout = new TimeSpan( 0, 0, 15, 0 )} )
						InstallBytes = Client.GetByteArrayAsync( InstallExeUri ).Result;

					var AppData = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ), UPDATES_FOLDER );
					Directory.CreateDirectory( AppData );
					InstallFileName = Path.Combine( AppData, InstallerExeName );
					File.Delete( InstallFileName );
					File.WriteAllBytes( InstallFileName, InstallBytes );
					onUpdateAvailable( this, ServerVersion );
				}
			}
		}
		catch( Exception E )
		{
			Console.WriteLine( E );
		}
		return RetVal;
	}

	private void KillTimer()
	{
		if( CheckForUpdateTimer != null )
		{
			lock( CheckForUpdateTimer )
			{
				var T = CheckForUpdateTimer;
				CheckForUpdateTimer = null;
				T.Stop();
				T.Dispose();
			}
		}
	}
}