# Changelog for Ids2Wpf

Based on https://keepachangelog.com/en/1.0.0/

Types of changes within each release:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## Note: Git Check in
#### A single line 
___
means that the code has been pushed to **ChristopherClient**.  
#### 2 lines 
___
___
means that it has been pushed to **PreReleaseClient**.


## [Unreleased]
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

___
___
## 2023-03-27
### Changed
Shipment Entry - Surcharges are enabled by a preference 'Trip Entry: Enable Surcharges'.  This only affects pricing in Shipment Entry - the Rate Matrix always shows them.

Shipment Entry - Charges are recalculated as the value in their Text Box changes.

### Added
Shipment Entry - A QUOTES package type in a shipment replaces the shipment's TripMatrix rates with the content of the QUOTES charge.  This behaviour is controlled by a preference 'Trip Entry: Package Type QUOTES overrides RateMatrix'.

Shipment Entry - Surcharges appear and disappear depending on package type and zone matching.
___
___
## 2023-03-23
### Fixed 
CD1-T45 - Dispatch Board crashed when header double-clicked.

Terry had checked in a version that didn't include Pricing/Posting and Invoicing, and their associated dictionaries.

### Changed 
Rate Matrix - Surcharges - Tidying up and adding the framework for pricing in Shipment Entry.

___
___
## 2023-03-21
### Fixed 
Search - Didn't display the full search results.

### Added
Rate Matrix - Surcharges - They are saved and restored from the server.  Partly connected to trip pricing.

___
## 2023-03-17
### Added
Rate Matrix - Surcharges - Filling in the surcharge tab. Not saving yet.
___
___
## 2023-03-16
### Added
Invoicing - If a company can't be found for a trip's accountId, the user is warned and notepad is loaded with the accountIds and the trips that have those accountIds.  There are quite a few in Phoenixtest.

Invoicing - Invoice totals are now included in the invoice.

___
___
## 2023-03-15
### Fixed
CD1-T81 - Found another path that didn't populate the Customer field when opening a trip from search.

Invoicing - Modified CreateInvoice according to Terry's explanation.

## 2023-03-14
### Added
Invoicing - Hooked CreateInvoice to Terry's AddInvoice.  Doesn't seem to work.

### Fixed
CD1-T81 - Shipment Entry - Looping when searching for a non-existent Customer.

## 2023-03-13
### Fixed
CD1-T81 - Trips opened from Search didn't display the Customer name.

___
___
## 2023-03-09
### Fixed
CD1-T81 - Dispatch - Made DispatchBoard IDisposable and added a Dispose method to shut down the UpdateConditions thread.  Also applied this to Search, DriversBoard, and MapBrowser.

___
___
## 2023-03-08
### Added
Invoicing - Bulk - Double-clicking a trip in the list opens it in a new Shipment Entry tab.
___
___
## 2023-03-08
### Added
Invoicing - Bulk - Added list of accounts that fall within the selected dates.  Clicking on an account shows the trips that will be invoiced.

### Fixed
Shipment Entry - CallTakerId wasn't being set in trips.

## 2023-03-07
### Added
Search - Multi-line notes can be wrapped if setting is checked.

Accounts - Billing Period, Billing Method, Invoice Email, and Credit Limit are connected to the db.

## 2023-03-06
### Added
Search Trips - There is now a setting to display notes on one line, just as in Dispatch.

### Fixed
Search Trips - Scrolling the results to the right meant that the toolbar vanished.

Search Trips - Mouse wheel didn't work in results.

### Changed
Pricing/Posting Trip value totals are now displayed.

Search Trips gets its company data from GlobalAddressesModel.

### Added
Have moved the GlobalAddressBook and other company/address pieces out of ShipmentEntryModel and into GlobalAddressesModel.  This is a singleton class.  It checks for global address updates every 120 minutes, which is saved as a preference.

___
___
## 2023-03-03
### Added
Shipment Entry - Pricing totals are saved and restored.  Currently they aren't being restored because of a server bug.

### Fixed
CD1-T85 - Rate Matrix - Eddy had a crash when trying to remove a lane caused by a 'key not present in the dictionary' exception.  

Shipment Entry - Fixed price rates weren't being calculated although they worked in the test matrix.

Shipment Entry - Charges weren't restored when a trip was loaded from Search, etc.  

___
___
## 2023-02-28
### Fixed
Pricing/Posting - Filter wasn't firing.

Pricing/Posting - Trips data grid was expanding too much when loading lots of trips.  Also added a wait cursor to the load process.

___
___
## 2023-02-27
### Fixed
Missed Daily2 &amp; Daily3 Billing Periods in the Accounts and Invoicing.

Invoicing - Accounţ Filter was broken.

Accounts - Regular Charge Overrides were deleted when default charges were saved.
___
___
## 2023-02-27
### Added
Shipment Entry - Default charges with AlwaysApplied set are checked and populated.  Pricing isn't connected yet.

## 2023-02-24
### Added
Accounts - Default Charges - They are saved and deleted to the ChargeDetails table.  

## 2023-02-22
### Fixed
CD1-T85 - Rate Wizard - Fixed amount lanes were broken.

## 2023-02-21
### Added
Invoicing - Filling in UI for Bulk Invoicing.  The Invoicing Period is populated after looking up reseller preference.

Pricing/Posting - A dialogue is displayed if there are no verified shipments.

### Changed
Accounts - Adding additional billing periods for bulk invoicing, based upon reseller preference.

___
## 2023-02-17
### Changed
Invoicing screen - Modified layout and behaviour.  Working on creation of local invoice for preview.

___
___
## 2023-02-14
### Changed
Search - Edit Selected - Added highlighting to emphasise the 'Selected' items. 

___
## 2023-02-14
### Changed
Invoicing screen - Modified layout.

___
## 2023-02-13
### Added
Invoicing screen - UI is working, but no back end report generation.

___
## 2023-02-10
### Added
These are under the Reports menu.  
Pricing/Posting screen - working.  

___
___
## 2023-02-08
### Fixed
CD1-T218 - Zone Wizard - Import files are checked for duplicate routes and postal codes.

CD1-T221 - Search - Problem opening trips in new Shipment Entry tabs.

CD1-T220 - Shipment Entry - The 'Find Customer' toolbar button didn't load the selected account.  Also improved the behaviour of the 'Find Customer' dialogue.

CD1-T219 - Shipment Entry - Changing the service level changed package types that were in the list of types in the service level.

___

## 2023-02-08
### Fixed
CD1-T217 - Search - Changed tab title.  
CD1-T217 - Search - Changed button colours.

___
___
## 2023-02-06
### Fixed
CD1-T27 - Search - Wrapped the display in a scroll viewer so that the toolbar doesn't vanish.

CD1-T27 - Search - Eddy found a trip (O673397602007) in PML which returned a null when fetching the latest version.

### Added
Shipment Entry - Moved GetRateForTripPackage out of TripEntryModel and into PriceTripHelper.  It hasn't been connected to TripEntry as Terry is still working on it.
- Package Overrides are working.  
- Account Discounts are working.  


___
___
## 2023-02-02
### Added
Accounts - Pricing - The pricing UI has been connected to the server and the redirects and discounts are saved and deleted.  
Working on connecting them to the shipment pricing in Shipment Entry.

___
___
## 2023-01-27
### Changed
Charges Wizard - Edit formula - Tidied up the UI.

### Fixed
Route Shipments - Shipments weren't being loaded from import file.

Route Shipments - Initializing the map crashed the client.  Had to remove  
`CreationProperties="{StaticResource EvergreenWebView2CreationProperties}"`  
from the xaml.

Zones Wizard - Editing a mapping left the old one in place.  The server isn't updating properly so the old one is deleted first.

___
___
## 2023-01-26
### Added
Zones Wizard - The zone/pcode mappings can be exported to a csv file.  It works for both PML and generic routes.  The format is, of course, that which can be imported.  
Tidied up the interface and added a count to show the number of mappings.

___
## 2023-01-25
### Added
Search - There is now a Settings button on the toolbar that opens a Settings tab where the visible columns can be selected.

### Changed
Search - Pressing **Escape** in the filter box clears the filter.

___
___
## 2023-01-24
### Fixed
Charges Wizard - Broken. Null pointer exception on creation caused by the removal of a null check.

Dispatch - The adhoc filter didn't include the account.

Shipment Entry - Broken. Addresses weren't loaded when selecting from the Customer drop down.

Rate Wizard - Service Levels - Misinterpreted Eddy's report about not being able to delete associated package types.  Now they are removed.

Search - Broken after Terry's changes.  The Start and End Status values weren't being set.

### Changed
Search - Clear didn't reset the Start and End status fields.

___
___
## 2023-01-23
### Fixed
CD1-T37 - Rate Wizard - Zones - An exception was thrown when adding a new Zone.

### Changed
Rate Wizard - Service Levels - Put listbox that contains the service level's package types into a scroll viewer to stop the page from expanding downwards too much.

### Added
Rate Wizard - Service Levels - Added a dialogue to prompt the user to select a package type in the list before clicking 'delete'.

___
___
## 2023-01-23
### Fixed
Rate Matrix - Lane Pricing - Cumulative is working.

___
## 2023-01-20
### Fixed
Rate Matrix - Lane Pricing - i think that i have cracked the cumulative pricing problem.

Rate Matrix - Lane Pricing - After looking at the formulae being used, i have removed the second condition from EditRateFormulaWindow.

___
___
## 2023-01-18
### Fixed
CD1-T216 - Shipment Entry - Keep previously selected package type selected if it is in the package types associated with the service level.

CD1-T210 - Shipment Entry - Mouse click in Customer drop-down didn't select the customer.

___
___
## 2023-01-17
### Fixed
CD1-T210 - Shipment Entry - Hitting -Tab- after selecting an account didn't fire the code to load the selected account.

CD1-T214 - Accounts - AccountName field is readonly if AccountId is.  AccountName is cleared on save.

CD1-T209 - Search - Searching for verified time wasn't using the start and end of the days.

___
___
## 2023-01-13
### Fixed
Shipment Entry - A package override charge was being applied to the wrong package.

### Changed
Rate Matrix - Lane Pricing - Even if there are no references to the lanes for a package/service pair, the lanes are preserved and restored when loaded.

___
___
## 2023-01-12
### Fixed
CD1-T210 - Shipment Entry - Account wasn't properly selected if the dropdown wasn't open.

Accounts - Tab order was broken for the country and prov/state fields.

CD1-T214 - Accounts - Name vanished when an account was loaded and it couldn't be edited.

### Changed
CD1-T212 - Shipment Entry - Eddy has decided to not pre-populate the country and region when an account is selected.

CD1-T209 - Search - The original design from Gord only specified the delivered time for searching.  Have added another radio button for verified.  The server doesn't support this, so the client filters the returned list before displaying.

Dispatch Board - Added confirmation dialogue to context menu's Clear Board.

___
___
## 2023-01-10
### Changed
Dispatch Board - Eddy says that Phoenix are reporting trips being moved unintentionally to different boards. i've added a prompt to confirm the action.  -Enter- will confirm and -Escape- will cancel the action.  
I have also added buttons to the toolbar that will perform the same actions, without the prompt.

___
___
## 2023-01-06
### Fixed
CD1-T211 - Bugs in Search and Shipment Entry.

Search - Trips that had been edited weren't updated on the screen until the search was run again.  Found while prowling phoenix logs.

## 2023-01-05
### Fixed
CD1-T211 - Shipment Entry - Anthony, Emma, Blake logs.  GenerateIdFromAddressName is somehow being called without an address name which causes an exception.  Worked around.

___
___
## 2023-01-05
### Fixed
CD1-T85 - Zones - Strange duplication of zones occur when saving.

### Added
Rate Wizard - Empty zones can no longer be created.  Fields are coloured yellow when missing values.

___
___
### Changed
CD1-T212 - Accounts and Shipment Entry - Region and Country are cleared.

___
___
## 2023-01-03
### Changed
CD1-T209 - Search Page - Changed T-VER to display dates formatted the same way as the other dates.


___
___
## 2022-12-06
### Changed
CD1-T210 - Shipment account selection - Reworked its behaviour.

Shipment Entry - Tweaked the code that sets the pickup and delivery Prov/State fields to try and ensure that the correct country and region are chosen.

___
___
## 2022-12-06
### Fixed
Charge Wizard - The wrong charge was selected when editing, deleting, or moving.

Account Details - Account Notes wasn't cleared when Clear was clicked.

Account Details - Caught several null pointer exceptions on Save.

___
___
## 2022-12-06
### Fixed
CD1-T81 - Shipment Entry - Pickup and Delivery address latitudes and longitudes weren't being saved in the trip.

___
___
## 2022-12-05
### Fixed
Shipment Entry - A service level wasn't always selected if there was no default.  Now it is set to the first.

Shipment Entry - The correct RateMatrixItem wasn't being loaded if the zones were in the wrong order.  This was caused by the Rate Matrix not being symmetrically populated.

Shipment Entry - GetRateForTripPackage didn't understand the new multiple lane pricing mechanism so the delivery charge wasn't calculated if there was more than 1 lane for the package type/service level pair.
___
___
## 2022-12-02
### Fixed
Rate Matrix - Lanes - Testing with Intermountain's rates revealed some bugs in lanes with both fixed price and formula rates.

___
___
## 2022-11-25
### Fixed
CD1-T207 - Dispatch - Assignment to board broke again.  Also fixed another instance of the white/gray alternation failing.


___
___
## 2022-11-24
### Fixed
CD1-T202 - Dispatch - Trip selection highlighting problem.

CD1-T202 - Dispatch - Alternating white/gray backgrounds were sometimes mixed up until the next refresh.

Shipment Entry - Trip is recalculated when saved and cloned.

___
___

## 2022-11-23
### Fixed
CD1-T201 - There were some bugs that occurred when the user chose 'Save'.  I've added a 'Cancel' option as well.  The user is prompted to save every time a clone button is clicked.

___
___

## 2022-11-22
### Changed
CD1-T202 - Drivers - The OK flag is not set when driver allocation is cancelled.

CD1-T203 - Drivers - Driver and Name column is left-aligned and expands appropriately.

CD1-T203 - Drivers - There is now a context menu item to open the selected trip in a shipment entry tab.  As before, double-clicking the trip also works.
___
___

## 2022-11-21
### Fixed
CD1-T198 - Drivers weren't assigned if a trip came in.  It's working better but there is still a problem with selection.

Shipment Entry - Pickup and delivery regions are sometimes not set although there are valid abbreviations in the trip.

Shipment Entry - Occasional crash found by Eddy caused by duplicate keys in the address dictionary.
___
___

## 2022-11-17
### Fixed
CD1-T199 - Package types weren't reloaded when Service level changes.

CD1-T204 - Trips moved to another board are displayed without having to close and open the board.

CD1-T198 - Trips stay selected when new trips come in.

CD1-T194 - The <enter> key press was being passed from the Allocate Driver dialogue through to the data grid which caused the selected row to move down.

___
___

## 2022-11-16
### Fixed
CD1-T197 - Trips don't open if the double-click occurs on the driver column.

CD1-T198 - Trips aren't updated if the select driver dialogue is open.  When it closes, they are.

CD1-T194 - Driver is displayed after left-clicking twice to allocate the driver to a single trip.

### Changed
Dispatch and Drivers boards - Left-clicking the TripId field in the right-hand trip display copies the TripId to the clipboard.  Wanted this for testing. 

Dispatch Board - Assign Driver - Changed the strings in the dialogue from Assign to Allocate to match the context menu.  Note: this only occurs when called from Dispatch.  The Drivers board still shows Assign.

Dispatch Board - Assign Driver - If there is only one item in the list, the Allocate/Assign button is enabled.

CD1-T26 - After saving, the selected inventory items are left, as well as the selected address or group.
___
___
## 2022-11-14
### Added
CD1-T26 - Escape clears the inventory item, company name, and location barcode filters.

CD1-T26 - Add All button to add all inventory items.  It respects the inventory filter.

CD1-T26 - Remove All button to clear selected inventory items.

### Changed
CD1-T26 - Reduced the maximum height of the address list box so that the selected address and group boxes are higher.

__
__
## 2022-11-10
### Fixed
CD1-T196 - Couldn't sort on Board in Dispatch.

### Added
Address Book Import - Can handle basic province or state abbreviations in the csv file, especially for Australia (PML).  VIC -> Victoria.  

Address Book Import - Added a catch to pick up the file being open in Excel.

___
___

## 2022-11-09
### Fixed
CD1-T192 - Board 3 button now behaves properly.  
CD1-T192 - Changed descriptive message for 'New' board.

___
___

## 2022-11-08
### Fixed
CD1-T191 - Inconsistent behaviour when using the Customer combo box.

## 2022-11-07
### Fixed
CD1-T190 - Account phone number sometimes vanishes when trip is loaded.

Dispatch Board - If multiple trips selected, then use right click and Allocate.  Double click on the wanted driver in the drop down. Allocates the selected driver to all highlighted trips except the first trip.

Dispatch Board - If multiple trips selected, then use right click and Allocate. Enter the first 4 digits of the driver ID and use the enter key - only assigns the selected driver to the first trip.
### Changed
Dispatch Board - Double-click opens trip in new shipment tab.

___
___
## 2022-11-04
### Changed
CD1-T180 - Cleared more fields when cloning.

Drivers Board - Assign driver dialog is only loaded when the drivers column is clicked.

Rate Matrix - Lane pricing - Working save.

### Fixed
CD1-T188 - 'Clear Selection' removed driver and Ok column for all trips, not just the selected ones.

CD1-T186 - Selecting an account in when the screen has been cleared led to another package line being created.

___
___
## 2022-10-28
### Fixed
#### PML Report
- To date defaults to today.
- After the report has been saved, the user is asked if it should be opened.  If yes, the file is opened using the registered application.
- Eddy reports that the report is timing out loading 1 June to end of October.  Have broken the loading down into month-long blocks.


___
___
## 2022-10-27
### Fixed
CD1-T186 - Trips opened from dispatch, drivers, and search had extra package rows inserted frequently.

___
___
## 2022-10-25
### Fixed
Dispatch Board - Removed the combo-box driver selection and replaced it with the Assign Driver dialog from the drivers board.  For multiple trips, there is a new context menu 'Select Driver for Multiple Trips'.

CD1-T187 - Increased width of Pickup and Delivery company combo boxes.

CD1-T186 - Extra package rows were appearing occasionally when cloning.

Shipment Entry - Couldn't remove a service level default.

CD1-T185 - Clone pickup dropped the package type, pieces, and weight.

CD1-T184 - Trips assigned to another board didn't appear until the board was closed and opened.

## 2022-10-24
### Changed
Drivers Board/Assign Driver - Improved selection handling by adding double-click and enter to select the new driver.

___
___

## 2022-10-21
### Changed
Dispatch Board - The driver selection dropdown stays open for as long as you want.  The grid doesn't get updated until it closes.

### Fixed
Dispatch Board - The trips didn't return to the grid when the filter was cleared.

Shipment Entry - The package selection and totals rows weren't created when a new tab was created.

___
___

## 2022-09-29
### Fixed
Shipment Entry - Strange crash in Eddy's log caused by an unnecessary call to update other Shipment Entry tabs.
___
___

## 2022-09-27
### Changed
Zone Wizard - This now behaves differently depending upon the state of the Preference 'Pml' because Pml (and Pmltest) uses a different mechanism to handle the mapping.  Shipment Entry has been modified to understand the differences.

___
___

## 2022-09-22
### Fixed
Rate Wizard/Price Matrix/Lane pricing - There was a subtle bug in the indexing of the cells and their contents.

## 2022-09-21
### Fixed
Rate Wizard/Price Matrix/Lane pricing - Changing an individual lane selection in the grid, rather than using the Modify Values button, didn't set the modified flag.


___
___

## 2022-09-20
### Changed
Empty Zones are excluded in RateWizard's Rate matrix.

####Rate Wizard/Price Matrix/Lane pricing  
Found a problem with the EditRateFormula window when editing an extant formula that prevented the correct item for the condition to be selected.

The EditRateFormula didn't handle mileage correctly when evaluating a formula for testing.

Cumulative pricing is working again.

Reworked the loading of the rate matrix when the wizard is starting.  

___
___

## 2022-09-15
### Added
Shipment Entry - Added dialog to display images attached to the shipment.  Phoenixtest/ZSC1524506435 has Terry's test images attached.

___
___

## 2022-09-15
### Fixed
Rate Wizard/Price Matrix/Lane pricing - The modified flag was being set incorrectly when switching package/service pairs.

___
### Added
Shipment Entry - Produce Pictures button and menu item to load associated pictures.  NOTE - does nothing at the moment.

___
## 2022-09-14
### Fixed
Shipment Entry - Had to change the pcode-to-zone lookup to use the new mapping mechanism.

___

## 2022-09-13
### Added
Shipment Entry - Driver Notes are displayed beside Delivery Notes.  Read-only.

Rate Wizard/Price Matrix/Lane pricing - There is a 'Cumulative' checkbox next to the Lane pricing radio button that is enabled when lane pricing is selected.  It causes a variable 'Cumulative = 0' if not checked or 'Cumulative = 1' if checked to be saved with the lane's formula and controls the method of evaluation of the formula.  

### Changed
Zone Wizard - Now using Terry's new mechanism for these mappings which means that there will no longer be zones like Zone1+0.  NOTE: Although the associated route (driver) is saved, it isn't returned from the server.

___
___

## 2022-09-07
### Changed
Shipment Entry - Pickup and Delivery addresses aren't updated until a selection has been made in the comboboxes.  This stops the UI from being constantly updated when scrolling through the companies.

### Fixed
CD1-T180 - Clone pickup didn't clear the delivery fields.

Shipment Entry - Hotkeys for Clone Return and Pickup no longer work if the preferences aren't set.

___
___
## 2022-09-02
### Fixed
Rate Wizard/Price Matrix/Lane pricing - i found that the problem lay with editing the formula in a lane.  The method for detecting changes didn't see the difference and so, didn't update the rare matrix items.

### Changed
Accounts/Pricing - Put a border around each section.

___
___

## 2022-09-01
### Fixed
Rate Wizard/Price Matrix/Lane pricing - Problems with saving and loading lanes.

### Added
Accounts/Pricing - Built UI for Package Redirects and Account Discounts.

___
___

## 2022-08-31
### Fixed
Rate Wizard/Price Matrix/Lane pricing - Save button was sometimes turned on when changes hadn't been made.

___
___

## 2022-08-30
### Fixed
Shipment Entry - signatures weren't displayed.


___
___

## 2022-08-29
### Changed
Rate Wizard/Price Matrix/Lane pricing - Sped up the populating of the test matrix.

### Fixed
Shipment Entry - Null object when checking to see if address name exists.

Drivers Board - Total Trips number wasn't always updating.

Rate Wizard/Price Matrix/Lane pricing - Problem when loading for the first time, caused by the Zones not being loaded.

Rate Wizard/Price Matrix/Lane pricing - Fixed occasional exceptions when loading.
___
___
## 2022-08-25
### Fixed
Shipment Entry - Package totals row wasn't displayed on loading a new tab.  This meant that the bottom-left totals didn't reflect the rates from the package rows.

___
___
## 2022-08-23
### Fixed
#### Rate Matrix/Lanes
Save button wasn't always enabled properly.  
Lane Formulas weren't being saved on modification.

___
___

## 2022-08-22
### Fixed
Shipment Entry - Crashes when saving a trip opened from the search screen. The global address book wasn't loaded when a trip was opened from the search screen.


___
___

## 2022-08-19
### Changed
Customer Import - Updates accounts that already exist with the information from the import file as well as adding new ones.

Zone Wizard - Route and Zone combo boxes drop down when focused and enabled.  Also decreased the width of the fields to make them more obvious.

Rate Wizard/Price Matrix/Lane pricing - Changed the way that the linked formulas in a lane are calculated.  It now calculates each section for the amount of the selected object (pieces, weight, etc.) in the condition.  
For example:

Test Weight = 75

-- Weight s/b 25   
-- Remainder s/b 50
Total = 0  
If Weight <= 25   
Total = 0 + Weight * 10.5  
Endif  
Result = 262.50  

-- Weight s/b 24  
-- Remainder s/b 26  
Total = 0  
If Weight <= 49   
Total = 0 + Weight * 8.5  
Endif  
Result = 204.00  

-- Weight s/b 26  
-- Remainder s/b 0  
Total = 0  
If Weight <= 100   
Total = 0 + Weight * 7.5  
Endif  
Result = 195.00  
Final Total: 262.50+204.00+195.00=661.50
___
___

## 2022-08-15
### Fixed
Rate Wizard - Zones - Didn't have scroll bars.

CD1-T177 - This turned out to be a visual glitch.  If you selected another account and then came back to the original, the addresses weren't there.  They were attached to the global account id.  This glitch has been fixed.

___
___

## 2022-08-12
### Fixed
CD1-T178 - After making the changes in CD1-T177, i am no longer getting any crashes.

CD1-T177 - The addition of a trailing "'" to the global addresses broke the loading of pickup addresses if the preference 'Make Account's Shipping Address default Pick Up Address' is checked.
___
___

## 2022-08-11
### Fixed
Address Book - Importing addresses was reporting false errors when loading the file.

### Changed
Global Addresses - Addresses imported through the Address Book's Import Addresses tool into the Global address book have a "'" appended to the name.  It can't be at the start, otherwise sorting won't work.  
New addresses created in Shipment Entry when the preference 'Use Global Address Book' is checked have a "'" appended to the name.

Account Details - Moved the Pricing's package redirects and Employee into separate tabs, replacing the toolbar buttons.  The Pricing tab isn't working yet.

___
___

## 2022-07-29
### Fixed
CD1-T176 - Address Notes weren't imported properly.

## 2022-07-28
### Added
CD1-T176 - ReferenceMandatory, BillingZone, and ShippingZone have been added to the import file.

### Changed
Shipment Entry - Fixed layout of second, etc. package lines.

### Fixed
CD1-T148 - Loading trip from Search doesn't crash (at least for me).

Accounts - Reference mandatory wasn't being saved.

CD1-T173 - Logic test for merging the global address book with the current was reversed.

___
___

## 2022-07-22
### Changed
CD1-T175 - When Use Global Address Book is on, the pickup and delivery companies only show the global address book.

### Fixed
CD1-T81 - Trips opened from search in PML now load without a long delay.

## 2022-07-21
### Added
Preferences: 'Use Global Address Book'.

## 2022-07-20
### Fixed
Shipment Entry - Clone Pickup & Delivery logic was reversed.

Address Book - This wasn't showing the global addresses because Terry had removed the global company and addresses.  In addition, RequestGetResellerCustomerCompany returns an empty CompanyAddress rather than null if the company doesn't exist, so the global account wasn't created.

### Changed
Address Book - no longer shows the 3 global addresses when selected.

___
___
## 2022-07-07
### Fixed
Drivers Map - Managed to get the map to centre when adding a driver but not when it is refreshed by the update tick.

___
___
## 2022-07-05
### Fixed
CD1-T65 - Blake caused a weird bug with the map going into a frenzied loop updating.  Have turned off the automatic centring of the map when checking a driver on the list.  The refresh tick is also set to 2 minutes instead of 1.

Drivers Map - Using the 'Show on map' button on the Selected tab didn't restore the driver's trips.

Drivers Map - Using the 'Hide on map' button on the Selected tab didn't hide the driver's trips.
___
___
## 2022-07-04
### Fixed
Drivers Map - A lot of fiddling trying to get the live map to behave properly.  NOTE: The History map tab is turned off, until it stops affecting the Live map.

CD1-T166 - Current location data are being reliably provided.

CD1-T157 - Focus is set to the last checked driver, but not when the map's update tick fires.

___
___
## 2022-06-23
### Fixed
Address Book - If shift was pressed while typing in the company name or location barcode filter fields, the string would be reversed.

CD1-T163 - Accounts tab froze when saving an account.

## 2022-06-22
### Fixed
CD1-T164 - Editing an address in a new trip didn't reflect the latest version if it had been edited in the address book.  Now the latest version is loaded from the server.  i've also taken the Close button off the top-right corner to ensure that there is no confusing the action to be taken.

___
___
## 2022-06-21
### Changed
CD1-T158, CD1-160 - The showing and hiding of driver's lines and info boxes worked inconsistently or not at all.
___
## 2022-06-17
### Changed
CD1-T148 - Put range checks and debug logging into BuildNewPackageTypeRow to try and catch Rowan's crash.

CD1-T161 - Set map to blank page at first, hid the map, and invalidated the map after making it visible again.


## 2022-06-07
### Changed
CD1-T157

+ Added last ping time to driver's info.

+ Drivers are listed by staffId rather than name.

Rate Wizard - Added message when trying to remove lane 1.

___
___
## 2022-06-06
### Changed
Drivers Map

+ Added exception blocks to MapHelper when it is updating the map object.  That seems to be where Blake's crash is happening.

+ Changed the driver's list to use a TextBlock to hold the driver's name.  The checkbox treats an '_' as the beginning of an accelerator key, thus hiding it until the 'Alt' key is pressed.

+ Added a 'Redraw Map' button.

CD1-T144 - Zone is cleared if no match is found from postal code/zip in a new trip.

___
## 2022-06-03
### Fixed
Rate Wizard - The Save and Save All buttons weren't being enabled when changing the lane pieces.
___ 
___
## 2022-06-01
### Added
Shipment Tab - The Preferences

+ Trip Entry: hide default - "Delivery"
+ Trip Entry: hide default - "Pickup"
+ Trip Entry: Show Clone "Pickup"
+ Trip Entry: Show Clone "Return"

have been connected to their UI elements.


### Fixed
CD1-T154 - Changes to Text fields in Preferences weren't enabling the Save button or being reliably saved.
___
## 2022-05-27
### Fixed
Drivers Map - The 'Hide Driver on Map' button on the History tab didn't work.

Drivers Map - History crashed.

CD1-T140 - Driver's routes and infoboxes are controlled by checkboxes.

### Added
CD1-T140 - Checkbox to control the driver's info box.

## 2022-05-19
### Fixed
___
Shipment Entry - 'Is Quote' checkbox was checked on starting.
___
Shipment Entry - On opening, the tab was missing the package row until New or Clear was clicked.
___
## 2022-05-18
### Fixed
CD1-T150 - The correct TripId is now displayed.

### Added
CD1-T142 - There is now a preference that controls whether the weight field allows non-integer weights.  If not checked, the weight changes by 1, otherwise by 0.1.

___
## 2022-05-12
### Added
Drivers Map - There is a checkbox labeled 'Show Shipment Totals' that controls whether or not the pop-up showing the shipments pieces, weight, and service level.

### Fixed
CD1-T136 - Searching with CallTakerId specified works if Enter is pushed rather than the Search button clicked.

Shipment Tab - Addresses were not loading sometimes, or the incorrect address was displayed.

Shipment Tab - When editing an extant trip, the pickup and delivery address combo boxes didn't have the global addresses.
___
## 2022-05-11
### Added
CD1-T137 - Predates have been added to Shipment tab.  Depend upon Predates preference being enabled and a driver in the Predates Driver field.  On starting, the Shipment tab checks for the predates driver (if enabled) and creates a driver with that name if not found.
___
### Changed
Drivers Map - Shrank the icons.
### Added
___
When the client starts, it records the version number and slot in the log.
___
## 2022-05-10
### Added
Search Tab - A search can be restricted to a CallTakerId, which is also a column in the grid.	
___
## 2022-05-09
### Fixed
Shipment Screen - Clone Trip for Return failed.
___
## 2022-05-06
### Added
CD1-T24 - Postal Code is editable even if the address is locked.  Tab from the pickup or delivery company combo box goes to the associated postal code field.

CD1-T147 - Missing hot keys have been added.  Calculator has been remapped to ALT-Z.
 
## 2022-05-05
### Fixed
CD1-T24 - Edit Address wasn't updating the contact name or contact phone on the screen, although the correct data were saved.
___
## 2022-05-04
### Added
CD1-T24 - Address fields are locked if the company name selected is already in the addresses.

CD1-T24 - Addresses have an 'Edit Address' button that opens a dialog and allows the address to be modified.  These buttons can be turned off with a preference setting.
## 2022-05-03
### Added

CD1-T24 - Alt-P works for address swap. 
___
## 2022-05-02
### Fixed

CD1-T24 - Crashes caused by Terry's zone fix not checking for nulls.
___
CD1-T24 - Address Notes were occasionally editable.

___
## 2022-04-28
### Added
CD1-T24 - Adding 'Edit Address' function for pickup and delivery addresses.  The changes aren't being saved yet.
### Fixed
CD1-T24 - Swap addresses didn't work properly if only 1 address filled in.

### Changed
___
CD1-T133 - Zone Wizard - Hiding the NONE_DISPATCHING_ROUTE in the list.
___
## 2022-04-27
### Changed
CD1-T133 - Zone Wizard - Route is no longer required.  Route and Zone are dropdowns.  The routes are created with a user NONE_DISPATCHING_ROUTE that doesn't appear in the staff screen or any other lists of drivers.

CD1-T24 - Increased delay before the selected account and selected company are changed.

CD1-T24 - Increased width of Weight and Pieces.
___
## 2022-04-25
### Fixed
CD1-T24 - Problem with invalid company names.  Caused the tab to loop and freeze.

Shipment Entry - Wouldn't load a trip from dispatch or elsewhere.

CD1-T44 - Opening a trip from the dispatch board would remove the selected driver.
___
## 2022-04-22
### Changed
Shipment Entry - CORE_GLOBAL_ADDRESS_BOOK no longer appears in the account or customer lists.
### Fixed
Shipment Entry - Had to change how CORE_GLOBAL_ADDRESS_BOOK is 
checked for, as it no longer exists in the cached company data and was creating extra copies.
### Added
File menu -> Settings now has an item to delete the locally cached data.

## 2022-04-20
### Added
___
CD1-T24 - Finished tying in the Customer combobox.  Worked on making focus start with the account combobox.

## 2022-04-13
### Added
___
CD1-T24 - Customer field is now a combobox with the company names.
CD1-T24 - Added 'Make Account’s Shipping Address default Pick Up Address' preference.  It overrides the saved default for the PickupAddress.

CD1-T24 - Added prefences to lock the 'CallDate', 'CallTime', and 'Dispatch To Driver' widgets.

### Fixed
Shipment Entry - CallTakerId, POP, and POD are displayed.

CD1-T44 - Calltaker is displayed.

CD1-T46 - Calltaker is displayed.
___
## 2022-04-07
### Fixed
CD1-T24 - Signatures were being duplicated on screen.

Shipment Entry - If there are no service levels or package types the defaults system would cause a crash.

CD1-T24 - Swap addresses works again.

## 2022-04-05
### Added
CD1-T24 - Added jump to Reference field.  Also fixed the jump to the package type and weight.

### Changed
CD1-T24 - Tab order jumps from account to caller name.

CD1-T24 - Location barcode fields are locked.

## 2022-04-04
### Added
___
CD1-T44 - There is a new dedicated board that only shows trips that haven't been assigned to a board.

Shipment Entry - If a new Shipment Entry tab is opened, it tries to load the global addresses from an extant tab.  Needed to speed up the loading of a tab when viewing a trip in the dispatch and other tabs.

## 2022-04-04
### Fixed
___
CD1-T24 - Removed most of the delay caused by merging the global address book with the addresses for the selected account.

### Added
CD1-T44 - Added Board to the columns.

MapBrowser - The default location for a driver is set to the most common city in the dispatched trips.

## 2022-03-28
### Fixed
___
CD1-T45 - Trip count now matches the filter.

CD1-T44 - Problem with editing the trip's pickup and delivery notes.
___
## 2022-03-23
### Changed
Global Address Book.  No longer using the resellerId for the global address book.  Now it is CORE_GLOBAL_ADDRESS_BOOK.  The account is hidden.
___
## 2022-03-22
### Added
Global Address Book.  
- Global addresses are added to the pickup and delivery addresses in Shipment Entry.  
- New addresses are added to the global book.  
- In Address Book, the Global radio button works to see the global address book.  
- Address imports work with the global book.  
- The Search tab includes the global addresses in the filter's pickup and delivery addresses.

___
## 2022-03-14
### Added
ShipmentEntry - Toolbar button and menu item to remove checked defaults.

ShipmentEntry - ReadyTime and DueByTime defaults are working.

___
## 2022-03-11
### Added
ShipmentEntry - Defaults for addresses, package type, weight, pieces, service level, ready time, and due by time.  All the values are being saved and restored, but ready date/time and due by date/time are being overwritten somewhere.

## 2022-03-07
### Fixed
___
Drivers Board - Trip count was slow to update after bouncing trips.

CD1-T44 - Trips bounced from the drivers board now show up.

___
## 2022-02-25
### Changed
Dispatch Board - On a new install when there are no settings saved, boards 1, 2, and 3 will default to only showing trips for the specific board.

## 2022-02-24
### Fixed
Re-did the fixes for the Accounts.  Overwritten by Terry.

Staff - There was a problem loading the staff if the last staff member was a WEB_ADMIN.
___
## 2022-02-23
### Fixed
Shipment Entry - POP wasn't cleared.

CD1-T44 - Dispatch now ignores OnlyShowShipmentsForThisBoard if it isn't one of the special boards - 1, 2, or 3.

### Changed
Eddy - Put 'FINALISED' back as a status for editing shipments in Search.

## 2022-02-22
### Fixed
CD1-T25 - Changed tab name to match item in main menu.

CD1-T25 - Error backgrounds are cleared when a different account is loaded.

CD1-T25 - Account admin users weren't being loaded.
___
## 2022-02-11
### Fixed
CD1-T75 - If the Role Web Admin doesn't exist when Accounts are loaded, it is added.

CD1-T130 - Imported shipments don't have any TripPackages.  Creates a new one and populates with PackageType, Pieces, and Weight.

CD1-T130 - Imported shipments with crap addresses threw an exception.

CD1-T44 - Shipments assigned to boards 1, 2, or 3 display on the correct board.
___
## 2022-02-07
### Fixed
CD1-T45 - Sort is preserved when new trips appear.

## 2022-02-03
### Added
Dispatch Board - Label now shows what column is sorted and what direction.

## 2022-02-03
### Changed
CD1-T44 - Sorting is now handled by the Model, not the DataGrid.  This has fixed a problem with the background colouring not being correct until the UpdateConditions tick happened.

## 2022-01-31
### Fixed
___
CD1-T75 - The staff members that were created as account admins now have a role of Web Admin and are disabled to prevent them from appearing in the staff list and from logging into the client.

## 2022-01-28
### Fixed
CD1-T75 - After adding a new user, the encrypted password was displayed until the users were reloaed from the server.

### Changed
CD1-T24 - Opening a trip in Search, Dispatch, and Drivers opens a new TripEntry tab. 

## 2022-01-26
### Fixed
___
CD1-T24 - Swapping addresses preserves entered data.

## 2022-01-25
### Changed
___
CD1-T24 - TDEL -> TVER.

### Fixed
Search and Audit Trip tabs load.

## 2022-01-19
### Added
___
Rate Matrix - Added a warning when locking a modified rate matrix that the changes will be lost.

### Fixed
Rate Matrix - The primary 'Unsaved Changes' label wasn't displayed after clearing all lanes.

## 2022-01-18
### Fixed
Rate Matrix - An empty lane pricing matrix crashed when saving.

Maintain Staff - Staff with a role (e.g. Administrator) were removed from the appropriate filter when edited and saved.

Maintain Staff - The adhoc filter wasn't applied when changing the radio buttons (select role or status).
### Changed
Maintain Staff - The staff members are no longer reloaded after saving or deleting an entry.

### Added
Maintain Staff - The staff members are cached locally and then updated.

## 2022-01-13
### Fixed
CD1-T37 - Hadn't handled accounts that had been disabled or deleted. 
## 2022-01-12
### Added
___
CD1-T37 - Caching Trip Entry's companies.  This is what was blocking the loading of service levels.  The companies are saved locally and then a thread runs to update the collection.  Going to do the same for staff.


## 2022-01-07
### Fixed
___
CD1-T44 - Line colouring is largely working.

Dispatch - The adhoc filter wasn't resetting when cleared.

Dispatch - Phoenix's notes weren't displaying on one line when setting on.  Caused by the line-breaks inserted by Java which are different to those inserted by this client.

## 2022-01-06
### Fixed
___
CD1-T44 - Sorting seems to be working properly. Line colouring still has a problem - service level coloured lines are close but the alternating backgrounds aren't stretching across.

## 2021-12-22
### Fixed
___
CD1-T44 - Working on the sorting problem.  Not finished.
## 2021-12-21
### Added
CD1-T44 - Open Boards 1,2, & 3 to main menu.
### Fixed
CD1-T44 - Notes are displayed on one line if setting is on.
 
## 2021-12-20
### Added
CD1-T44 - Dedicated buttons for Boards 1, 2, & 3.  They either open a new board for that number or switch to it.

### Changed
Dispatch Board - Copied driver-fetching method from DriversViewBoardModel that seems to find more drivers than RequestGetDrivers().

CD1-T44 - Double-click no longer opens the trip in Shipment Entry; right-click menu item.

## 2021-12-16
### Fixed
___
Shipment Entry - Addresses were missing for new Account.

## 2021-12-14
### Changed
CD1-T44 - Modified toolbar.  Buttons are no longer disabled.  Vertical separators are invisible.

## 2021-12-13
### Changed
CD1-T44 - Added Call Taker to top of right-hand section.
___
CD1-T44 - If a service level has a colour, it is applied to all columns.
## 2021-12-10
### Changed
___
CD1-T44 - Trip pickup and delivery notes are editable.
___
CD1-T44 - Trip details on right side aren't cleared when the selected trip loses focus.

___
## 2021-12-09
### Added
CD1-T24 - The 'Make Reference Field on Shipment Mandatory' is saved in Customers/Accounts and obeyed in Shipment Entry.

___
## 2021-12-07
### Fixed
Shipment Entry - validation was broken.

___
## 2021-12-03
### Added
CD1-T24 - A preference has been added to lock the address notes fields if set.

## 2021-12-02
### Added
CD1-T24 - Swap Addresses is working.

CD1-T24 - Find Customer is working.  Opens a filterable list of names.

## 2021-12-01
### Fixed
CD1-T24 - Shipment Entry audit shows local times for event time stamp.

## 2021-11-30
### Added
CD1-T24 - Jumping to sections (billing, etc.) works.  Added Delivery Notes to the jump list.

CD1-T24 - Added hotkeys to match IDS1.

## 2021-11-29
### Added
CD1-T24 - Toolbar button and menu item to open Windows calculator.

### Fixed
CD1-T24 - Trip search by TripId is working.  Also, clicking the 'Shipment' button hides the search field and button.

## 2021-11-26
### Changed
CD1-T24 - Shipments menu item changed to Shipment Entry.

___
## 2021-11-19
### Fixed
CD1-T45 - 'Collapse All' is working.  The problem was that there were trips with drivers that didn't exist in the staff list.

CD1-T45 - Occasionally double-clicking a driver in the list would not go to a driver after filtering the drivers.  This was caused by drivers being displayed that didn't have trips.

## 2021-11-17
### Changed
CD1-T45 - Row colours match IDS1.
### Added
CD1-T45 - Assign driver button and menu item.

## 2021-11-16
### Added
CD1-T45 - Assign driver context menu.  It loads a dialog that allows the driver to be filtered from the list.  The trips are saved when the dialog is closed and the drivers board is updated.

### Changed
CD1-T45 - DataGrid's borders have been removed except for that between each row.

Changed Show/Hide drivers from a ToggleButton to a CheckBox.
___
## 2021-11-12
### Changed
CD1-T103 - BillingNotes in the LinFox shipment report are trimmed.

### Added
CD1-T45 - Trip Pickup and Delivery notes are editable.  Double-click also works.

## 2021-11-09
### Fixed
CD1-T45 - Trip that is displayed on left isn't cleared until another valid trip is selected in the DataGrid.

CallTime wasn't loading when selected on start up.

## 2021-11-08
### Added
___
CD1-T45 - Added CallTime to DataGrid.

### Changed
CD1-T45 - Changed 'Delete Trip' to 'Delete Shipment'.  Took alternating colours out again.

CD1-T45 - Changed tab heading to Drivers Board - did the same for Dispatch.
___
CD1-T45 - DataGrid now has alternating colours (white and light gray) unless overridden by the driver's state colouring.
___
CD1-T45 - Toolbar and menu has been changed to match other tabs.

## 2021-11-04
### Changed
___
CD1-T45 - Changed responsiveness of the driver's filter.
Drivers Board - Removed duplicate drivers from drivers list.

___
### Added
CD-T114 - Customer accounts now are imported, skipping those that already exist.

## 2021-11-03
### Fixed
CD1-T45 - Jumping to a driver's trips wasn't working.

## 2021-10-28
### Changed
CD1-T31 - Sped up the loading of the addresses and groups for an account and the saving and deleting of addresses.

### Added
CD-T114 - Starting to build the Customer Account process.

## 2021-10-26
### Added
CD1-T51 - Routing of trips with a single pickup location and multiple delivery locations is working.

## 2021-10-22
### Fixed
CD1-T51 - Trips that were double-clicked to view lost their driver.
### Changed
CD1-T51 - Tidied up the Toolbar and the Mapping options section.

## 2021-10-18
### Added
Shipment Entry - Trip pricing with overrides seems to be working.

## 2021-10-18
### Fixed
Shipment Entry - Totals weren't being cleared when the Clear button was clicked.

Charge Wizard - Overrides - Service Level overrides didn't load correctly.

## 2021-10-18
### Fixed
Charge Wizard - Overrides - Overrides didn't delete.
### Added
Shipment Entry - Charges and overrides are working, but only for shipments with 1 package type.

## 2021-10-12
### Fixed
Rate Matrix - Price Matrix - After saving the Price Matrix, it still showed that there were unsaved changes.
### Changed
Rate Matrix - Price Matrix - Service Levels are sorted by their SortOrder.
### Added
Shipment Entry - Reference charges are saved and loaded.

## 2021-10-12
### Added
Shipment Entry - Charge Max/Min values are applied.

Shipment Entry - Charges don't depend upon zones and the TotalDeliveryCharge to be calculated.

## 2021-10-08
### Added
Shipment Entry - Charges are saved with the trip and they are updated with their values when the trip is loaded from the search screen.

## 2021-10-05
### Added
Shipment Entry - Basic pricing is working.  A trip with GST, a simple added value from a text box, and a fuel surcharge matches IDS1 with the same rate value.

## 2021-09-22
### Fixed
CD1-T103 - Linfox report - Original piece count still wasn't right.

## 2021-09-21
### Fixed
CD1-T103 - Linfox report - Original piece count wasn't including the original values in the trip packages.

## 2021-09-17
### Changed
Linfox Report - Added 3 new columns to the report for Eddy.

Shipment Entry - Working on trip pricing but have hidden it for the moment.

## 2021-09-10
### Changed
Shipment Entry - When it is a new trip, the zones are looked up from the Zone Wizard.

### Added
Shipment Entry - Rates are calculated (not fully tested) and updated when package type, weight, pieces, service level, pickup zone, and delivery zones are changed.

The rate total is also updated.

Shipment Entry - The Total section has been laid out and the Delivery Charge is updated from the Rate total.



## 2021-09-10
### Fixed
Shipment Entry - A specific trip (O631197262894) in Pmltest was causing a crash when loading from the search screen.

### Changed
Linfox Report - The program now tries to find the pickup zone if it is missing by looking up the postal code in the Rate Wizard.  If there is no match, the trips that can't be updated are listed in Notepad, along with their postal codes.

## 2021-09-09
### Changed
Advanced Shipments - Zones weren't assigned to the trips.

Zone Wizard - Took out some logging.

## 2021-09-08
### Changed
CD1-T100 - Maintain Staff - Now using a new call from Terry that loads staff and their roles in one call.

## 2021-09-07
### Changed
CD1-T100 - Shipment Entry - Turned off requiring a customer phone number for Eddy, but only when editing a trip loaded from the search screen.

CD1-T100 - Maintain Staff - Screen is excessively slow to load the staff and their roles.  Sped up as much as i can in the client.

## 2021-09-01
### Fixed
Search Shipments - Billing Notes wasn't included in the filter.
Rate Wizard - Fixed price and lane pricing load from the server.

### Changed
Drivers Board - Added TripId to adhoc filter.

## 2021-08-23
### Changed
Rate Matrix - Rates are now saved to and restored (sometimes) from the server.

## 2021-08-23
### Changed
Reports - Improved some messages and added a check to ensure that at least 1 group is selected.

## 2021-08-19
### Fixed
Address Book - Bug when importing addresses into extant group.

Address Book - Importing addresses didn't attach them to a group.

Address Book - Available Groups and the dropdown of groups in the import section weren't poptulated on loading the tab.

### Added
Address Book - MessageBox on successful import.

## 2021-08-13
### Added
Shipment Entry - Added labels to charges to display calculated value.  Not attached to data yet.

## 2021-08-13
### Added
Charges Wizard - Testing complex formulas works.

Charges Wizard - Added formula testing to Edit Formula window.  Testing simple formulas works.

## 2021-08-12
### Changed
Charges Wizard - Sort order is performed on the Charges tab.  Overrides are visible, but if one is edited, the override is loaded in the Overrides tab.

Shipment Entry - Cleaned up the display of the charges.

## 2021-08-11
### Added
Search Screen - Put in Billing Notes per Eddy's request.

## 2021-08-10
### Fixed
CD1-T27 - Large results in Search Shipments caused Out of Memory crashes and took a very long time to render if at all.

## 2021-07-30
### Added
Charge Wizard - Loading, editing, and saving a complex formula works.

## 2021-07-30
### Added
Charge Wizard - Loading, editing, and saving a simple formula works.

## 2021-07-29
### Fixed
Charge Wizard - Overrides weren't displayed on first loading.

### Added
Charge Wizard - Charges are now saved and loaded from the server.  The basics of the wizard are now functioning.


## 2021-07-28
### Changed
CD1-T52 - Put in check box to allow the user to update the addresses in the address book from the addresses in the import file.

## 2021-07-23
### Added
Charge Wizard - Charges are properly created, saved (locally), and loaded.

## 2021-07-22
### Changed
Charge Wizard - Changed Default and Formula in Charges to radio buttons.  Default only takes numbers.

Started to lay out Overrides.

## 2021-07-21
### Added
Charge Wizard - Saving charges works.  There is currently no mechanism to load the charges from the server.
Charge deletion and moving works.

## 2021-07-20
### Added
Charge Wizard - Basic layout and functionality of Charges tab is done.  No saving.

## 2021-07-16
### Added
Starting to build the ChargeWizard.

## 2021-07-15
### Added
TripEntry - The trip's ready time and due times are checked against the selected service level and errors are displayed if outside of the range.

### Fixed
TripEntry - Removed time selectors from the ReadyDate and DueDate pickers.


## 2021-07-14
### Added
Rate Wizard/Service Levels and TripEntry - The selected package types added to a service level are saved to the server and restored.  The list of package types for each TripPackage's combo box are set to the package types for a service level if it has any, or to all package types if it doesn't.

## 2021-07-09
### Fixed
CD1-T76 - Staff roles weren't added to the UpdateData object to be sent to the server.  In addition, the server wasn't saving the new roles.

## 2021-07-08
### Fixed
Rate Matrix - fixed bug when switching between package/service pairs that have multiple lanes.

### Added
CD1-T52 - Added border to Import Shipments window.

## 2021-07-07
### Fixed
RouteShipments - Trips are loaded into an extant Route Shipments tab.

## 2021-05-04
### Added
CD1-T68 - Search by CallTime, PickupTime, and DeliveredTime.  Note: the server code has been modified but not tested yet because of a problem publishing to my slot.
CD1-T69 - T-VER to search screen.

## 2021-05-03
### Added
CD1-T66 - History tab on maps works.

## 2021-04-28
### Fixed
#### CD1-T66
Both PU and Del trips are displayed and controlled by the checkboxes.
Turned off History for the moment.

## 2021-04-28
### Changed
#### CD1-T66
Moved Webview2 methods from MapBrowser.xaml.cs to MapHelper.
Have started to get the History to work again.

## 2021-04-27
### Added
#### CD1-T66
- New layout for group management.
- Update current group button that works with current group.

## 2021-04-26
### Added
#### CD1-T66
Delete group button.

## 2021-04-26
### Fixed
#### CD1-T25
- Layout was broken - everything was centred vertically.
- Account Details notes and Account Notes are saved.

## 2021-04-23
### Fixed
#### CD1-T25
- Shipping address notes are saved.
- Admin user details are saved.

## 2021-04-22
### Fixed
#### CD1-T25
- Shipping State is selected.
- Limited fields to 30 characters.
- Clearing fields when account loaded.
- Shifted button sizes and alignments.

## 2021-04-20
### Fixed
CD1-T52 - If DueTime or ReadyTime were set to the beginning of time the Dispatch Board wouldn't load.

### Added
CD1-T52 - 'Import Shipments' creates shipments that appear on the Dispatch Board.

## 2021-04-16
### Added
CD1-T52 - 'Import Shipments' item added to 'Shipments' menu.


## 2021-04-13
### Fixed
CD1-T66 - Not all drivers were removed when 'Remove All' button clicked.

## 2021-04-13
### Changed
#### CD1-T66
- Changed font for pins and infoboxes.
- Help button works.
- Loading driver group now turns off the map and displays a wait cursor while the selected drivers and trips are being loaded to stop the display from blinking.
- New icons.

## 2021-04-01
### Changed
CD1-T17 - Shipment ID is now displayed in the trips list.

### Added
CD1-T70 - Shipment verified is on the shipment tracking report.

## 2021-04-01
### Changed
#### Drivers Map
- In 'Selected', hiding a driver hides its trips as well.
- In 'Select', unchecking both 'Pickups' and 'Deliveries' unchecks 'Show Shipments'.
- Stopped the resizing of the map when toggling between 'Select' and 'Selected'.

## 2021-03-31
### Added
#### Drivers Map
- Have been working on speeding up the loading of trips and the geolocating of their addresses.

## 2021-03-30
### Added
#### Drivers Map
- Things are generally working for live map, but there are performance problems.  History hasn't been tested yet.

## 2021-03-26
### Added
#### Drivers Map
- Checkboxes to control whether PU or Del trips, or both, are displayed on the live map.  Not quite right yet.
- Changed the infobox for trips to match Gord's illustration.
- All infoboxes are removed when 'Show Trips' and 'Show Drivers' are unchecked.

## 2021-03-25
### Added
#### Drivers Map
- Info boxes are displayed beside trips and drivers.

## 2021-03-24
### Added
#### Drivers Map
- Can now draw drivers with lines on map.

## 2021-03-23
### Added
#### Drivers Map
- Truck, pickup trip, and delivery trip icons are now loaded.  Had to encode them as Base64 strings.

## 2021-03-19
### Added
#### Drivers Map
- Driver's pins can be added and removed, with checks for duplicates.  Only testing at the moment.
- Using an html file for editing the html that is loaded into the WebView2 component.

## 2021-03-18
### Added
#### Drivers Map
- Removed the old wpf map component.
- Starting to implement the control of the map by calling javascript methods embedded in the html.  Traffic works.

## 2021-03-17
### Changed
Drivers Map - Have started migrating to new map engine.

## 2021-03-10
### Fixed
CD1-T21 - The Save and Cancel buttons are enabled if a Text field is modified, as well as when a checkbox is changed.

CD1-T26 - Trips are made for all addresses in a group.

CD1-T26 - Group selection works after creating a shipment.  It only clears the details of the shipment, not the selected group or address, to handle the situation where multiple shipments are being created for a single address or group.
## 2021-03-09
### Fixed
CD1-T7 - Double-clicking a driver in the list brings those trips into sight.

CD1-T7 - Filters from settings work.

CD1-T7 - Remove ? from Hide.

CD1-T7 - Bounce works again.

CD1-T7 - Double-clicking trips open them in shipment entry.

## 2021-03-02
### Fixed
CD1-T7 - Sorting and hiding now work correctly.  The UI has been sped up as much as possible, for the moment.

## 2021-03-01
### Changed
CD1-T7 - Trips are sorted initially by tripid;

CD1-T7 - Tripid's field's background is transparent to match the rest of the row.

## 2021-02-25
### Changed
CD1-T7 - Collapse All & Expand All buttons work.
Start Page - Set alignment for labels and text fields.
### Added
CD1-T7 - Unicode arrows and tooltips for Hide? column.
CD1-T7 - Label above trips shows the currently sorted column and direction.

CD1-T7 - Sorting by date columns works.

## 2021-02-24
### Changed
CD1-T7 - Sorting trips for each driver works.  Driver's trips can be hidden or shown by double-clicking the + or - in the Hide? column.  The menu show/hide buttons don't work yet.

## 2021-02-10
### Changed
EditSelectedTripsModel - added test for MinValue when changing the status to picked up and verified.

## 2021-02-08
### Fixed
I2P-12 - Fixed the binding errors visible in the Output console.  Took logging out of the DriversBoard.xaml.cs.

## 2021-02-08
### Changed
I2P-12 - Took most of the logging statements out of the drivers board.

I2P-9 - Removed spinners from service level times.
## 2021-02-05
### Changed
I2P-1 - Address imports include Contact Name and Contact Phone.  Also improved the error detection and reporting of errors in the import.

## 2021-02-04
### Fixed
I2P-1 - Address import works on Speedia files.

## 2021-02-03
### Changed
Drivers Board - Turned off the extra fetching of trips.  Seems to be working.

Advanced Shipments - Set the BroadcastToDriverBoard flag for new trips.

I2P-2 - Service Level - Start and End time pickers have their spinners disabled.  The times are set by picking from the dropdown.

### Fixed
I2P-2 - Service Level - Cancel button works.
Rate Wizard - Fixed button spacing on Zones tab.

## 2021-02-02
### Fixed
I2P-4 - Time portion of CallTime wasn't set from the trip's CallTime.

Search Trips - ReadyTime and DueTime didn't respect the 'Show As Local Time' toggle.

## 2021-02-01
### Fixed
I2P-9 - Shipment Entry only loads enabled companies.

## 2021-01-29
### Changed
Accounts - Made zone comboboxes editable and sorted by name.

Shipment Entry - Pickup and Delivery zones are editable to make it easier to find the right one.
### Added
Shipment Entry - Pickup and Delivery zones are now comboboxes populated from the ZonesWizard.

## 2021-01-28
### Fixed
I2P-6 - Some company names had trailing spaces.

## 2021-01-27
### Added
Drivers Board - Added a tooltip to the drivers filter to inform the user about the effect of clearing the filter.

### Changed
Drivers Board - When the drivers list only shows drivers with trips, the other drivers' visibility is set to collapsed rather than hidden to remove the irregular spacing between them.

Drivers Board - When the drivers filter is cleared, the drivers list changes back to only showing drivers with trips.

## 2021-01-26
### Added
I2P-53 - Auto-dispatch works for Advanced Shipments.

### Fixed
Dispatch Board - There was a one-off addressing error with the columns in the settings.

Shipment Entry - Province lists weren't changing when the country changed.

I2P-53 - Zones are reloaded after saving.

## 2021-01-25
### Fixed
I2P-11 - All dispatch board columns can be hidden.  Also removed 'Shipment ID' from column list in settings.


## 2021-01-21
### Changed
I2P-8 - Final speed up of loading the staff.

I2P-8 - Moved the loading of the staff's roles into a separate method called when 'Select' role radio button is selected.

## 2021-01-20
### Changed
Shipment Entry - Improved the address loading process to handle situations where the address is missing or the region is an abbreviation.

## 2021-01-19
### Changed
I2P-4 - Missed a couple of fields when loading pickup and delivery addresses.

Per Gord: 'Shipment Entry' tab is opened automatically on successful login.

I2P-8 - Have sped up the loading of the staff members.

## 2021-01-18
### Fixed
I2P-8 - The staff list is now in a scroll view.

I2P-8 - Staff Roles list is now cleared when the staff member is saved or the edit is cancelled.  Had to modify the RoleListBoxModel to handle an empty StaffId, as Client.RequestStaffMemberRoles returns a role for an empty staffId.

## 2021-01-14
### Changed
I2P-10 - Increased delay before reloading search after bulk edit.

### Fixed
I2P-10 - Finalised shipments crashed when double-clicked or Edited.

I2P-4 - Audit dialog has been widened as has the Description column, so that most lines don't wrap.  The underlying report won't resize.

## 2021-01-13
### Fixed
I2P-4 - Find Shipment works.

I2P-4 - Waybill, tracking, and audit wasn't working for new trips.

I2P-4 - Cloning wasn't working for new trips.

## 2021-01-12
### Added
I2P-4 - Pickup and delivery location barcodes.
### Changed
I2P-53 - Routes are no longer unique, although postal codes are.

Maintain Roles - Put title at top of tab.

### Fixed
I2P-1 - Groups were being created but not loaded.  Needed to trim the descriptions.

## 2021-01-11
### Fixed
I2P-4 - ValidateTrip didn't catch Pickup or Delivery Prov/State.
### Changed
I2P-27 - The roles are now saved by clicking the 'Save' button, rather than being saved upon change.

### Added
I2P-27 - Removed underscores from checkboxes.

I2P-27 - Help button.


## 2020-12-18
### Fixed
Drivers Map - Unchecking a driver didn't remove it's trips from the map.

## 2020-12-17
### Fixed
LinfoxShipmentReport - This was broken because Terry changed from a '-' to '+' separating the operations and regions.  **Note:** Formulae aren't working.

## 2020-12-16
### Changed
Drivers Map - Map starts centred upon the city of the reseller.

### Fixed
Drivers Map - Wrong map label was initially displayed.

Drivers Map - Added tooltip to zoom buttons.

### Added
Drivers Map - Zoom buttons.

Drivers Map - Auto-opens a shipment tab.

## 2020-12-14
### Fixed
Drivers Map - Driver's toggle works again.

Drivers Map - Removing drivers left the user unable to check the drivers again.

## 2020-12-11
### Added
Drivers Map - Context menu for trip labels to open trip in Shipment Entry.

Drivers Map - Trips, haltingly, are starting to appear on map.

## 2020-12-10
Added accelerators to main menu.

## 2020-12-09
### Added
Drivers Map - Alt-L and Alt-H switch between the 2 views.

Drivers Map - Checkboxes to control whether drivers, drivers routes, and trips are displayed on the map.

Drivers Map - Pin icons for shipments on map.

## 2020-12-07
### Added
Drivers Map - Changed truck icon.

## 2020-12-04
### Fixed
Drivers Map - Trips from server weren't loading.

### Added
Drivers Map - Driver's label includes a truck icon.

Drivers Map - Driver's label now shows totals for trips, weight, and pieces.  The trip information is taken from a Drivers Board, if one is open, or from the server.  The label is updated when the trips for a driver changes.

## 2020-12-02
### Changed
Drivers Map - Simplified the process of removing drivers from the selected drivers list.

## 2020-11-27
### Added
Drivers Map - There are now Live and History tabs at the top.  When they are switched, the map switches which lines are displayed.  Note: there isn't a refresh thread in place yet.

## 2020-11-26
### Changed
Drivers Map - Display name includes the staff id.

Drivers Map - Show on map buttons: All and None work.

Drivers Map - Replaced Hide/Show checkbox with a ToggleButton.

## 2020-11-25
### Added
Drivers Map - drivers can be hidden and revealed from the map by clicking a checkbox beside each driver.

## 2020-11-24
### Added
Drivers map - selected drivers can be saved and loaded.

### Fixed
Drivers map - map takes all free space.

## 2020-11-23
### Added
Points are being mapped.

Drivers map has been added to the Dispatch menu.  Drivers are loaded and basic UI layout work has been done.  Gps points are loaded for the selected drivers when the Apply button is clicked, but not yet mapped.


## 2020-11-19
### Fixed
I2P-9 - Account Details Phone number wasn't saved.

## 2020-11-13
### Added
I2P-53 - A new Main menu item 'Zone Wizard' has been added.  It opens a tab that allows the zones to be added, modified, and deleted.  __Note:__ At the time of committing, the deletion isn't working because of a server-side exception.

There is also an import section that allows a csv file to be imported to populate the table.

The beginning of a maps tool has been added to the main menu: 'Map Browser'.  i have commented it out for the time being.


## 2020-11-06
### Fixed
I2P-7 - Stopped buttons from resizing and added scrollbars to Service Levels and Package Types.

## 2020-11-02
### Changed
ReportBrowser - CreateExcel now uses the CloseXML library (https://github.com/closedxml/closedxml) instead of EPPlus which has changed its licence. __NOTE__: The formulas don't work yet.

## 2020-11-02
### Added
MainWindow - File menu now has a sub-menu called 'Settings' that contains 'Delete Local Settings' which deletes the *Settings files in AppData\Local\Ids2Client.  There is also an 'Explore Settings Directory' that opens Explorer in that directory.

### Fixed
I2P-11 - All trips selected for 'Driver Allocation' receive the driver and the 'Dispatch' buttons in the 'Ok' column are set.

Dispatch Board - Adhoc filter was ignored when the trips were reloaded.

## 2020-10-28
### Fixed
RateManager - Service Levels wouldn't load if one had _no_ active days checked.

### Added
I2P-11 - Other Settings - Display notes on one line.


## 2020-10-27
### Changed
MaintainStaff - minor UI tweaks.

### Added
Accounts - Activated the 'Users' button.

I2P-4 - Signatures now appear in Shipment Entry.

## 2020-10-23
### Added
RateManager - Service Level - Package Types are associated with the service level.

RateManager - Service Level - Active days are all checked by default.

## 2020-10-22
### Changed
RateManager - Service Level - Put group boxes around the active times and days to make them a little more organised.

RateManager - Service Level - Expanded the text of some of the labels to make them more meaningful.

### Added
RateManager - Service Level - The detailed lines in the list of the service levels include the start time and the active days.

RateManager - Service Level - Active days are loaded and saved.

RateManager - Service Level - Start and End times are loaded and saved.

### Fixed
RateManager - Service Level - Editing a service level loads the total minutes into the warn, critical, and beyond boxes.

## 2020-10-21
### Changed
RateManager - Service Level - Delivery Hours is saved and restored, but not applied to the DueTime.

### Fixed
Shipment Entry - The waybill didn't have the correct phone number.

I2P-11 & 12 - There was a bug in the conditions when changing from Critical to Late that lead to nothing being displayed.

## 2020-10-20
### Changed
Dispatch & Drivers Boards - Settings - put alternating background colours behind the display filters.

I2P-11 & 12 - Took several columns out of the filters.

RateWizard - ServiceLevel - Changed 'Warn', 'Critical', and 'Beyond' to IntegerUpDowns with a minimum of 0.

### Added
I2P-12 - Drivers board has both ReadyTime and DueTime conditions columns.

I2P-11 - ReadyTime Condition works and is updated every minute.  Have also added a DueTime condition to DisplayTrip for the Drivers Board.

## 2020-10-19
### Fixed
RateWizard - Warn, critical, and beyond weren't populated when a service level was loaded.

Shipment Entry - Couldn't set the hours/minutes section of the Ready By and Due By dropdowns.

## 2020-10-16
### Added
Rate Wizard - Warn, critical, and beyond values are saved in the ServiceLevel.

## 2020-10-15
### Fixed
I2P-12 - If no trips matched the active Settings filters, all trips were displayed.

### Changed
I2P-12 - Took out 'Proof of Delivery'.

### Added
I2P-12 - Help link.


## 2020-10-13
### Fixed
Dispatch Board - Adhoc filter now works with all columns.  This required changing the Filter section of the DisplayTrip.

Dispatch Board - Removing an adhoc filter undid the filters set in the settings.

### Changed
Drivers Board - Removed some dead code.

## 2020-10-13
### Added
I2P-11 - Added an adhoc filter to the drivers tab.

## 2020-10-08
### Fixed
I2P-11 - All trips that are selected are assigned a driver when the 'Allocate Driver' menu item is clicked.  Note: this doesn't _dispatch_ the trips; the 'Ok' buttons or 'Assign Driver' must be clicked.

## 2020-10-07
### Changed
I2P-4 - New help url.

I2P-11 - Removed the DriversPopup display per Gord.

### Fixed
I2P-4 - Trips created with the 'New' button are saved.

## 2020-10-06
### Added
- Search Trips - Edit Selected Trips - If the selected status requires a driver and none of the trips have one and a new driver hasn't been selected in the dialog, the user is told to pick one.  If at least 1 of the trips has a driver already, those that don't are not updated and the user is given the trip ids that were skipped and told why.

## 2020-10-05
### Fixed
- Search Trips - Program crashed when there is only 1 match in the search results, and that trip's status is changed so that it no longer matches the search parameters.

I2P-10 - T-PU is set to _now_ when the status is changed to PICKED_UP. if it is at the beginning of time.  This also occurs when the trip is set to VERIFIED.  The T-DEL is also set if necessary.

- Search Trips - Entire line's background changes when selected.

- Address Book - Stopped text boxes from expanding.

## 2020-10-02
### Fixed
I2P-12 - Settings - both columns and trip filters - are now working.

## 2020-10-01
### Added
Drivers and Dispatch boards - settings - added Select All & Select None buttons to the Select Columns section.

I2P-12 - Now shows driver-read status and service level colour.  

### Changed
I2P-12 - Entire row is highlighted when selected.

Dispatch Board - Entire row is highlighted when selected.

## 2020-09-30

### Added
I2P-12 - Expanders now have the driver's name.

I2P-12 - The Expand/Collapse all work again.

I2P-12 - The driver selection list works.

### Changed
I2P-12 - Have abandoned Terry's DataGrid and brought the version over from the Dispatch Board.  i've managed to get the DataGrid grouping process working so that the sorting is done for each driver's group.  The column headers are not displayed for each driver.  Note: this has probably broken the Settings.
### Fixed 
- Dispatch Board - Buttons on settings tab were misplaced.

## 2020-09-28
### Fixed 
- Drivers Board name wasn't correct when displayed.

- Driver name wasn't displayed after filters were applied.

### Added
I2P-12 - Filters are working and the Drivers Board is updated.

## 2020-09-24
### Added
I2P-12 - Settings tab is working - that is, the settings are saved and restored, but the board isn't responding to the settings.

## 2020-09-15
### Changed
I2P-12 - Tab labels are hidden until settings are opened.

Dispatch Board - Tab labels are hidden until settings are opened.

### Added
Drivers Board - Put Close button on Settings tab.

Dispatch Board - Put Close button on Settings tab.

## 2020-09-14
### Changed
I2P-12 - Added Pickup Address Notes and Delivery Address Notes.

### Fixed
I2P-4 - Ready Time and Due By work in the same way that Call Time does.

I2P-10 - Fixed null-reference error.

I2P-4 - Text boxes no longer expand.

I2P-9 - Connected 'Enabled' checkbox as Terry has added it to 
the Company object.

### Added
I2P-9 - Added 'Edit' context menu.

## 2020-09-11
### Fixed
I2P-4 - Locked size of notes fields.  They will scroll rather than grow.

I2P-9 - Admin user's enabled flag works.

I2P-9 - TextBoxes no longer expand.

I2P-9 - Clearing works.

## 2020-09-10
### Added
Customer Accounts - Added 'Edit' button and menu.

### Fixed
I2P-10 - Changing status through the Edit Selected Trips dialog didn't work.

## 2020-09-08
### Changed
Search Trips tab - increased the size of the Search button.

### Fixed
I2P-8 - Rename staffId error displayed when the staffId wasn't being changed.  

## 2020-08-27
### Fixed
I2P-6 - Crashed when creating Uppack shipments.  It also didn't save the items inside the package.  This _used_ to work.

## 2020-08-26
### Fixed
I2P-4 - The date part of the call time was being modified when the hour/minutes was changed.

I2P-4 - Audit works in Shipment Entry.


## 2020-08-20
### Fixed
I2P-4 - Multiple package can be added without inventory.

I2P-4 - Staff in dropdown list of drivers are not lower case, and are restricted to staff with the role of driver, which 
wasn't the case before.

I2P-4 - Pickup and Delivery notes were not always saved.

## 2020-08-17
### Fixed
I2P-4 - Problem with creating trip with some addresses.

## 2020-08-13
### Added
I2P-11 - Added board name to top of right-hand details panel.
I2P-11 - Filters are now applied to boards.  As noted below, the filters, like the column selections, are specific to the each board.

### Changed
I2P-11 - Disabled the 'Display Notes on 1 line' setting.
I2P-11 - Commented out the Unmatched trips toolbar and menu sections for the moment.

I2P-11 - Took Driver out of filters.

## 2020-08-11
### Added
I2P-11 - Filters are saved and restored.  Trips aren't filtered yet.

## 2020-08-07
### Added
I2P-11 - Selected columns are saved with the internal dispatch board name to the local file system (<user>/AppData/Local/Ids2Client/), so that when that board is reopened, the selected columns are loaded.

## 2020-08-06
### Fixed
I2P-4 - Reference field redux.

## 2020-08-05
### Fixed
I2P-10 - Selecting 'PICKED UP' crashes the program.

### Added
I2P-11 - Dispatch board names are reused, so that if, for example, there are 3 boards open (db_0, db_1, and db_2) and db_0 is closed and then a new board is opened, it calls itself db_0 and will reload the settings for that board name.  This matches the behaviour of IDS Dispatch.  No persistence yet.

I2P-11 - Dispatch tabs are named, in order to keep settings for each one.

I2P-11 - While the dispatch tab is open, the settings tab's column list's checkboxes match the previously selected columns.  The collection is not yet persistent.

I2P-11 - All columns have been added and appear in settings.

## 2020-08-04
### Added
I2P-11 - The displayed columns are now controlled by the currently incomplete column list on the settings tab.

### Fixed
I2P-11 - Wrong icon for Unmatched trips.


## 2020-07-30
### Added
I2P-11 - Columns list appears in settings tab.

I2P-11 - Adding Settings and Unmatched trips tabs.  They are hidden until the appropriate buttons and menu items are clicked.  The filters list appears in the settings tab.

## 2020-07-29
### Changed
I2P-11 - Toolbar and menu have been changed to match the other tabs.


## 2020-07-28
### Fixed
Shipment Entry - Fixed Toolbar's Clone column layout.

I2P-11 - Pickup Address Notes, Pickup Notes, Delivery Address Notes, and Delivery Notes are displayed.

I2P-11 - Service Level is displayed.
### Added
I2P-11 - Pickup Address Notes, Pickup Notes, Delivery Address Notes, and Delivery Notes are columns.  Filter works with them.

I2P-11 - Added Help button.

## 2020-07-27
### Fixed
I2P-1 - Changed address import to use Terry's utility.

## 2020-07-23
### Added
I2P-10 - PickupNotes, PickupAddressNotes, DeliveryNotes, and DeliveryAddressNotes have been added as columns.  The filter works with them.

### Fixed
I2P-4 - Reference wasn't being saved.

I2P-4 - Pickup and Delivery address notes were not being displayed.

I2P-10 - Clear empties the company, pickup address, and delivery address combo boxes.

I2P-10 - Searching by Pickup and Delivery addresses weren't working.

## 2020-07-22
### Added
Accounts - Populated the Billing Period.

Accounts - Populated the Invoice Override.

Accounts - Populated the Billing and Shipping zones.

### Fixed
I2P-10 - Status can be set to PICKED UP in 'Edit Selected'.

I2P-10 - Drivers list is loaded in 'Edit Selected' in Coral.


## 2020-07-21
### Fixed
I2P-9 - Admin user details are saved and restored.  

I2P-9 - Admin user detail fields are cleared.
### Changed
Accounts - Vertically aligned contents all text fields.

I2P-4 - Trip ID to Shipment ID.

### Added
I2P-9 - Help to toolbar.

## 2020-07-20
### Fixed
I2P-9 - Drivers list populated

Accounts - Wrong labels for 'Invoice Override' and 'Invoice Email'.

Accounts - Toolbar 'Accounts' label was not aligned with the others.

I2P-5 - Product list wasn't reloading after deletion.

### Changed
I2P-9 - Hardcoded Invoice methods.
I2P-5 - Changed help uri.

I2P-5 - Changed label to “Product Name (Required)”.


## 2020-07-16
### Fixed
I2P-10 - Edit Selected Apply button wasn't enabled.

Address Book - import checks that the countries and regions in the file are valid before importing.  The errors are loaded into notepad.

I2P-1 - Addresses were sometimes duplicated when editing.

### Changed
I2P-10 - Edit Selected - removed blank line from Status list.

Address Book - imported address values are trimmed.
I2P-6 - Changed help url.

## 2020-07-16
### Fixed
I2P-6 - Advanced Inventory - Remove button wasn't enabled.

Advanced Inventory - When first loaded, shipments could be created without an address or group selected.

Search Shipments - odd case where editing a shipment from a different account could trigger an exception caused by the addresses not being loaded.
	
## 2020-07-15
### Changed
Advanced Inventory - Group members are sorted alphabetically.

### Fixed
I2P-1,6 - Non-empty groups are now attached to the account that the addresses belong to.  Empty groups are not visible in Advanced Shipments but are in the Address Book.


## 2020-07-14
### Changed
I2P-6 - Moved Inventory from old PML manu to new Shipment menu.

## 2020-07-13
### Changed
AddressBook - Imported addresses must be one of the valid countries, and their regions are translated to the full region name if they are abbreviations.

### Fixed
I2P-2,7 - Yet more loading and moving problems found by Gord.

## 2020-07-09
### Fixed
I2P-2,7 - There were a collection of problems that started with the sort orders.  Not sure if i've caught all of the cases yet.  Gord, please apply your testing skills and break it.

I2P-2,7 - Clicking the down button on service levels and package types when one wasn't selected caused a crash.


## 2020-07-08
### Fixed
I2P-8 - Both Save buttons rename the StaffId.

I2P-8 - Pre-selected the country using the most common country from the staff members.

## 2020-07-07
### Fixed
I2P-8 - File menu has an accelerator key.

I2P-4 - There were still problems with the population of the region and country of the pickup and delivery addresses.

I2P-4 - Customer Phone wasn't being populated when the selected account changed after clicking the new shipment button.

## 2020-07-06
### Fixed
I2P-2,7 - The service levels and package types seem to move and be saved successfully.


## 2020-07-03
### Fixed
I2P-4 - EditPackageInventory - the service level combo wasn't enabled until the items were unchecked and checked again.

Log files older than 30 days weren't being deleted.

I2P-4 - Shipment Entry no longer overwrites a shipment's status, driver, pickup time or delivery time.

## 2020-07-02
### Changed
I2P-4 - CallName and CallPhone are no longer required.

### Fixed
I2P-4 - i think i've fixed the problem with the regions and some other parts of the addresses not being loaded.

## 2020-06-30
### Fixed
Maintain Staff - changing the staffId works.  The original is deleted.

## 2020-06-26
### Fixed
Maintain Staff - discovered that there was no server method to delete a staff member.  Added to the server.

I2P-8 - Changed Trip menu to File.

I2P-8 - Changed Region to Prov/State.

I2P-8 - Crash when deleting staff member.

## 2020-06-24
### Added
Inventory/Inventory now has an account selector.
### Changed
PML menu item has been changed to 'Inventory'.

## 2020-06-24
### Changed
- I2P-8 - Maintain Staff - The staff notes section has been reworked to match the behaviour of other sections of the program.

## 2020-06-22
### Added
- Maintain Staff - Message is displayed when editing a staff member if the password doesn't match the password standard.
### Fixed
- I2P-8 - Maintain Staff - Phone, mobile, and email are not saved or displayed.
- I2P-8 - Maintain Staff - Password confirmation is only required if adding a new member or changing the password for an existing member.
- I2P-8 - Maintain Staff - The enabled checkbox is set to the value of the member's enabled status.
- I2P-8 - Maintain Staff - User list is updated when a new user is added.

### Changed
- I2P-18 - Preferences - Changed HelpUri to correct value.

## 2020-06-18
### Changed
Forgot to turn off log message in ShipmentsModel, SearchTripsModel, and TripEntryModel.
ShipmentsModel, SearchTripsModel, and TripEntryModel all have 15 second timers to reload their address list for the currently selected account.

### Fixed
- I2P-1 - AddressBook - Open trip search and entry tabs are updated when addresses are modified.

## 2020-06-15
### Fixed
- RateWizard - I2P-7 - Package types seemed to vanish after saving.

## 2020-06-12
### Fixed
- RateWizard - DependsUpon doesn't seem to be working any more, or at least unreliably, so have put in explicit code to control the readonly states that used to be automatic.
- AddressBook - Importing addresses into a new group imported them and created the group, but didn't put them into the group.
- AddressBook - New addresses weren't created unless the GPS fields were filled in and there was no error displayed.  They now default to 0.
- Accounts - New accounts couldn't be added as the Account field was locked.

## 2020-05-26
### Added
- AddressBook - added latitude and longitude to address.

### Changed
- Staff - tab is disabled until the staff list has been loaded, to match the other tabs.

### Fixed
- AddressBook - PML Shipments wasn't being updated with the new or updated or deleted address.
- AddressBook - Filter was inconsistent after saving.  It is now restored after saving.
- AddressBook - List of available groups wasn't loaded in the address section.
- AddressBook - saving a group didn't properly add and remove members.
- PML/Shipments - some addresses within a group weren't loading their address details.  Changed to a case-insensitive search. 
- AddressBook - Groups that an address belonged to weren't always loading in the 'Member of Groups' list.

## 2020-05-08
### Fixed
- AddressBook - Region filter wasn't finding addresses that used the abbreviation.
- TripEntry - Address's country and region weren't loading.
- I2P1 - AddressBook - The groups that an address belonged to wasn't always loaded.

## 2020-05-05
### Fixed
- I2P1 - AddressBook - Double-clicking an address only occasionally worked.
- I2P1 - AddressBook - Selecting a different value in 'Prov/State' in Filter didn't work unless the filter was 'Cleared' first.
- I2P1 - AddressBook - mouse wheel scrolling didn't work unless over the scrollbar.

## 2020-04-29
### Changed
- Staff - When a staff member is loaded, it gets the staff member record from the server that includes the address.
## 2020-04-16
### Added
- Staff - Activated 'Delete'.  It doesn't actually delete the user, just disables it.
## 2020-04-09
### Added
- Staff - Basic validation (staff id and passwords).
- Staff - Dialog is displayed when a staff member is added or updated.
### Changed
- Staff - Cancel now clears everything, including the roles.
- Program's name in ALT-TAB is now 'IDS Shipments' to match the window's title.
- Passwords are being loaded, and the 'reveal' button works.
## 2020-03-17
### Changed
- 'Staff' tab filtering is working.
## 2020-03-10

### Changed
- Program is identified as 'IDS Core' in alt-tab list, rather than MainWindow.
- Starting to rework 'Staff' tab to match the others.  Toolbar/menu is done.
### Changed - Shipments
- New 'Save' button is always enabled, like the toolbar button.

## 2020-03-09
### Added - Shipments
- Added 'Save' button to right-hand side.
### Changed - Shipments
- Region filter now sees addresses that have abbreviations instead of full names.
- Filters and selected address are cleared when a filter is changed.
### Fixed - Shipments
- Changing the region filter didn't reload the unfiltered list first.
- Location Barcode filter had duplicated results.

### Fixed - I2P-2
- Exception thrown when there are duplicate sort orders.

## 2020-03-06
### Changed
#### I2P-18
- Changed the Preferences tab to *not* save automatically, but to behave like the Service Level and Package Type sections in RateWizard.  A confirmation dialog is displayed on saving.
The help button works, but goes to the general IDS Core tutorials page.

### Fixed
#### I2P-4
- Audit trail isn't always loading.

## 2020-02-20
### Fixed
#### I2P-4
- Addresses are reloaded in open search and shipment entry tabs when an address is added or updated.
- DeliveryNotes was being saved in DeliveryAddressNotes.

### Fixed
#### Shipment Entry
- Delivery postal/zip wasn't loaded on selection.


## 2020-02-11
### Changed
- Dispatch and Drivers boards - on starting up, create a Trip Entry tab if one doesn't exist, like Search Trips.
- Dispatch and Drivers board - double-clicking a trip loads it into the Trip Entry tab.
### Fixed
- Trip Entry - The primary package type wasn't being saved.


## 2020-02-06
### Fixed
- Rate Wizard - The list of Package Types wasn't updated after saving.
- Rate Wizard - The list of Service Levels wasn't updated after saving.
### Added 
- Shipments (PML) - Added Startrack service level.
## 2020-01-29
### Added 
- Rate Wizard - Colour selection to service levels.  **Note** - not connected to dispatch boards yet.

### Fixed
- Rate Wizard -Service Levels weren't sorted properly.


## 2020-01-23
### Fixed
-- Trip Entry - New didn't clear all of the field.
-- Search Trips - Exception on cancelling EditSelectedTrips.

## 2020-01-14
### Fixed
- AddressBook - Exception on clearing address if there were no groups.
- Shipment Entry - Account Phone is loaded when the account is selected in the combo box.
## 2019-12-10
### Fixed
- Shipment Entry - Addresses were being duplicated.  Now, they are only added if the company name doesn't exist.  To modify an address, use the Address Book.
- Shipment Entry - status label at top is updated when a trip is saved. Eg. if the trip has been dispatched from here, the Status shows DISPATCHED.
- Shipment Entry - if the trip that has been loaded has the prov/state abbreviation, it is changed to the full name.
## 2019-12-09
### Added
- EditSelectedDialog has a status-changing combo box.
## 2019-12-05
- I2P-10 - Changed context menu.  'Open in Shipment Entry' loads the first selected trip into the Shipment Entry tab.  'Edit Selected' opens the trips in the EditSelectedTrips dialog.
- I2P-10 - New help uri for Search Trips.
- All Ratewizard help uris are set to the same page.
- I2P-26 - New Help uri.
- PML Report handles group selection.
## 2019-12-04
- ML-311 - Added Help button to login page.
- PML report is basically working.  Have yet to test all combinations of the settings.

## 2019-11-28
### Fixed
- Database/Model/Databases/Carrier/Zones.cs didn't delete zones.
- RateWizard's Service Levels failed to load if there were duplicate SortOrders.
### Added
- RateWizard's Zone wizard now works.  The db is updated, but they aren't connected to the rest of the Client.

## 2019-11-25
### Added
`PML Reports` 

- Dialog is displayed on errors and success.
- Put in Toolbar/Menu swapping.
### Fixed
Server-side changes on Friday broke the addresses.
Working save.

## 2019-11-22
`I2P-6` PML Shipments
### Fixed 
- PML Addresses weren't visible.
`I2P-4` Shipment Entry
### Fixed
- The Package Type for each line was defaulting to the first package type in the combobox.

`PML Reports` 
### Added
- Starting to build the generated Excel file.

## 2019-11-20
### Added
Starting to build the Report dialog.

`I2P-4` Shipment Entry
### Fixed
- Address names and Account list are no longer lower case.
### Removed
- Took out the Default Address checkboxes for the moment.

## 2019-11-19
### Added
- Reports tab with basic UI.  No backing report yet.

## 2019-11-15
`I2P-6` PML Shipments
### Fixed
- Pickup Location Barcode in the trip is set to the PickupAddress' LocationBarcode.  Delivery Location Barcode has also been fixed.

`Shipment Entry`
### Fixed
- CustomerPhone was uneditable.

`I2P-6`
### Added
- ReadyTime and DueTime are set to now.
- Double-clicking an address selects it.

### Changed
- Quantity has been changed to an IntegerUpDown with a minimum of 1.
- Uppack shipments have 9999 pieces.

`Address Book`
### Added

- Import - a message is displayed showing what headers are incorrect and what they should be.

## 2019-11-13
`I2P-4` Shipment Entry && `I2P-10` Search Shipments

- Customer Phone is now loaded.
- Fixed a periodic bug in the loading of shipments from the search tab that resulted in either some fields not being populated, or the application freezing.

## 2019-11-08
`I2P-4` Shipment Entry

### Fixed
- Cloned trips have a CallTime of now.
- Cloned trips are set to status `New`.
- Occasionally the pickup or delivery company names don't load.
- The ShipmentId field is _always_ readonly.
- When an account is selected, if there is no ShipmentId (`New Shipment` hasn't been clicked), a new ShipmentId is fetched.
- When `New Shipment` is clicked, if the current trip hasn't been saved, a dialog is displayed asking whether or not to save it first that includes a **Cancel** button.

### Changed
- Trimming all string fields when building trip.

## 2019-11-06
`I2P-4` Shipment Entry

### Fixed
- Package Weight has been changed to a DecimalUpDown with a minimum value of 0.1 to prevent it being set to 0.

## 2019-11-06
`I2P-4` Shipment Entry

### Fixed
- \#1 Call date and time changing to 01-01-01.
- Weight and pieces don't change to 0.
- Package items aren't duplicated on repetitive saves.
## 2019-11-06
### Changed
- `Address Book` - Adding address file import and update mechanism works.

## 2019-11-05
### Added
- `Address Book` - Adding address file import and update mechanism.  Not fully finished.

### Changed
- `Address Book` - Delete now handles multiple addresses.

## 2019-10-31
### Fixed
- `Shipment Entry` - fixed bug with New Trip (I2P-4).

## 2019-10-29
### Changed
- `Search Shipments` - Converted to use Terry's DataGrid with children.  

## 2019-10-24
### Fixed
**I2P-10**

- `Search Shipments` - List didn't display the driver.
` `Search Shipments` - Loading the latest version of a trip doesn't work if the trip is in storage.  Checks that there is a TripId in the returned copy and ignores it if there isn't.

## 2019-10-23
### Fixed
- `Trip Entry` - UI locked up when opening 1 trip after another in the Search Shipments tab. 

## 2019-10-23
### Changed
- `EditPackagesInventory` - When items are added to the selectged list, their checkboxes are checked.  This will cut down on the number of steps required by Linfox.
- `EditPackagesInventory` - If no items are checked, the service level combo and button are disabled.

## 2019-10-22
### Fixed
- `PML Shipments` - Addresses didn't have Location barcodes.

### Added
- `EditPackagesInventory` - UI now allows the user to set a service level for the items.
- `Trip Entry` - TripItems now have a service level in their ItemCode field.


## 2019-10-21
### Changed
- `PML Shipments` - Updated help url.

### Fixed
- `PML Shipments` - Regions list was getting extra blank lines.

## 2019-10-21
### Added
- `Address Book` - added Location Barcode column to address list.  Filter by Location Barcode works.

## 2019-10-21
### Changed
- `PML` Inventory removed from PML menu.
- `PML Products` - new URI for help.

### Added
- `PML Products` - context menu for Save and clear.

### Fixed
- Inventory.PML_AddUpdateInventory was missing BinX, BinY, and BinZ fields when creating an Inventory item from a PmlInventory object.

### Fixed
- Shipment was not putting the package types and items into the trip record.

## 2019-10-18
### Changed
- `Trip Entry` - When loading a trip's inventory, it looks them up by barcode instead of description.  In addition, the first one is selected.

### Fixed
- `Trip Entry` - New shipment didn't display a package row. 
- `Trip Entry` - Audit for a shipment loads.


## 2019-10-18
### Changed
- `PML Shipments` can now be loaded from the Search tab.  The items for uppc and uppack shipments are put into the trip's TripPackage.

## 2019-10-15
### Changed
- `Trip Entry` package handling has been rewritten to match Terry's new design.  Saving is still not completed.

## 2019-10-09
### Added
- Dialog for editing the contents of a selected TripItem is finished.  A listview displays the inventory of a selected TripItem, but doesn't allow changes to be made.  The selected inventory items _aren't_ saved yet.

## 2019-10-07
### Added
- Dialog for editing the contents of a selected TripItem. 
- basic UI works.

## 2019-10-07
### Added
- trw - Added filter to dispatch


## 2019-10-04
### Added
- trw - Added broadcasting to Trip Entry

## 2019-10-04
### Added
- Started to add Inventory grid to `Shipment Entry`.
- Edit button to Search Shipments.

### Changed
- Editing (either by button or right-click menu) a single trip loads it like double-clicking it.  Multiple trips are loaded into the Edit Trips dialog.

### Fixed
- Thread access bug in PML/Shipments.
- Loading the same trip in `Shipment Entry` sometimes hung my client.
- Cloning a trip sets the status to **NEW**;
- Fixed bug when cloning trips for pickup or delivery.


## 2019-10-03
### Added
- trw - Copy shipment id to clipboard in search trips
- trw - Added staff id to trip audit trail

`Shipment Entry` I2P-4

### Fixed
- CallDate and other date controls sometimes showed 01/01/0001.
- POP displayed, not POD.

### Changed
- Added missing fields newly added to db.

## 2019-09-30
`Address Book` I2P-1

### Fixed
- `Company Name` and `Location Barcode` *must* be unique ; user is warned if they aren't.
- Filter by Location Barcode works.

## 2019-09-27
`Address Book` I2P-1

### Fixed
- The filter's Prov/State dropdown only adds an empty line if there isn't one already.
- Clicking the `Edit` buttons or the `Edit` menu item without selecting an address no longer crashes.
- `Clear` didn't remove the error backgrounds from the required fields.
- Clicking `Modify Group` after cancelling a previous modify attempt didn't load the group until another group had been selected.
- Saving an address didn't necessarily save the address fields.
- Deleting an address now works, as long as one has been selected in the list.
- The `Prov/State` combo is populated with those of the country found in the first address.
- Deleting an address didn't delete it from any associated groups.  There was a bug in `CompanyAddressGroups.DeleteCompanyAddressGroupEntry` in `Azure Web Service`.
- The `Location Barcode` and `Contact Name` weren't being loaded, although they had been saved.

### Added
- Help button in toolbar.


## 2019-09-23

### Added
`Uplift Products`
- When any Products are added, updated, or deleted, any open Uplift Shipments tabs are updated.

## 2019-09-19
`Uplift Products`

### Added
- Finished the UI layout and buttons are largely wired up.
- PmlInventory items are loaded and can be edited.
- Filter (with multiple words) works.
- Saving and Deleting works.

## 2019-09-17
`Uplift Shipments`

### Added
- Added filter for `Package Type` combo box.  Works with multiple words.
- Added Help Toolbar item.
- Added shipment description to help users.

### Fixed 
- Problems when selecting a Product in ProductList.

## 2019-09-16
`Uplift Shipments`

### Added
- Shipments are created for single addresses and groups but are *not* saved.
- Addresses and groups can be selected.
- Company name and location barcode filtering works.
- Shipment Details fields are selectively enabled as the service level changes.
- Toolbar Clear button works.

## 2019-09-03
### Added
- Shipments page for uplifts.  Basic layout but no functionality.
- Products page for uplifts.  Basic layout but no functionality.

## 2019-08-30
### Changed
- Azure Web Services - Location barcode is saved when an address is updated.
- AddressBook - Location Barcodes are being saved and loaded.
- AddressBook - Addresses are saved with whatever groups are selected.  The list in the `Groups` is updated with the new count.


## 2019-08-29
### Added
- AddressBook Groups - Adding and removing addresses in a group works but no saving.
- AddressBook address list - Edit, Delete, Select All, and Select None buttons added and working.
- AddressBook Groups - Delete works.
- AddressBook Addresses - Member of Groups and Available Groups lists - Groups can be moved between the 2 lists but are not saved.


## 2019-08-23
### Changed
- Backed out the code i had written yesterday to get a list of groups as Terry rewrote this section.
- Groups are loaded and can be created.  No companies attached yet.

## 2019-08-21
`Address Book`

- Starting to modify to match the one in Uplift.  The new filters are in place.

## 2019-08-20
### Changed
`Search Trips `

- Redesigned the tools at the top and added Package Type, Service Level, Pickup Address, and Delivery Address.
- Reworked the enabling logic of the controls depending upon whether it is a date search or not.

## 2019-08-15
### Fixed
EditSelectedTrips - POD wasn't working and CallTime was always being set to Now.

### Changed
SearchTrips - when double-clicking a trip, the latest version is loaded from the db.  This is also done when using the context menu (Edit Selected Trips).
The backing list is also updated.

### Added
TripEntry - POP and POP signature.
EditSelectedTrips - POP name.

## 2019-08-13a
### Added
- New IsQuote, UndeliverableNotes fields to db.

## 2019-08-13
### Added
- New IsQuote, UndeliverableNotes fields to db.
- New CallerName field to db.
### Fixed
- Minor changes to Trip Entry and Account Details.
- Minor bugs in Search Trips.

## 2019-08-08

### Added
File menu has a `Logs` section which allow the user to view the current log file or open Explorer to the <MyDocuments>\Ids directory.
### Fixed
`Account Details`

For some reason, the method UpdateCompany in Company.cs was forcing the CompanyName to upper case.  


## 2019-08-07a
### Changed
`Contact Email` in `Address Book` is connected.

## 2019-08-07
### Added
`Address Book` - Notes field and context menu.
`Account Details` - Missing toolbar labels and partial context menu.

### Changed
`Contact Name` in `Address Book` is connected.  Had to modify ResellerCustomers.cs in Azure Web Service project.
	

## 2019-08-02
### Fixed
`Search Trips`

- Scrolling was broken for large results.
- `Clear` sets the from and to dates to today.

### Changes
`Trip Entry`

- Weight fields are restricted to floats ; pieces fields are restricted to integers.  

`Search Trips`

- `Clear` sets the from and to dates to today.
- `From` and `To` are set to the beginning and end of the selected days, respectively.

## 2019-08-01
### Changed
- Added `Totals` line to package types list.
- Only the last TripItem line has an 'Add' button.
- Totals are working in `Trip Entry`.


## 2019-07-30

### Fixed
`Trip Entry` 

- Call Time is now set properly.
- Notes fields accept 'Enter'.
- Notes fields wrap properly.
- Context menu for Save, New, and Clear.
- New addresses are created.

### Changed

- Added more space between toolbar sections and other tweaks.
- Toolbar buttons have been re-styled to minimise their Padding.  This appears in `Search Trips`, `Trip Entry`, `Accounts', 'Address Book`, and `Rate Wizard`.
- `Address Book` now starts as disabled until the accounts have been loaded.

## 2019-07-29
### Added
Help button to main menu.

### Changed
Moved `Rate Wizard` into the `File` menu.
`Rate Wizard` - Applying Gord's changes from **ML-269**.
`Main Menu` - Removed `Log Out` button, icons, and `Show Icons` context menu.

## 2019-07-26
### Changed
Added icon to `Select Waybill Printer` in `Shipment Entry`.

## 2019-07-24
### Changed
Login TextBoxes automatically select all text.
`Address Book`

Delete works.
Description is readonly and disabled when editing an address.
Updating works.

## 2019-07-19
### Changed
`Address Book` - moved the buttons into a ToolBar/Menu to match the other pages.

## 2019-07-18
### Fixed
The regions in `Account Details` are now loading correctly.


## 2019-07-17
### Changed
`Address Book`

Countries and Regions are loaded using case-insensitive methods in order to deal with dirty data.  Countries are also found by abbreviations.

New addresses are now saved and added to the list.

`Shipment Entry`

Problem - Nav is getting this message
2019-07-11 14:17:41.1753; Found 9 Entries; Method: GetAuditLines; File: TripEntryModel.cs; Line: 1891
Unhandled exception : Not enough quota is available to process this command
i've truncated `Entry.PartitionKey`, `Entry.Operation`, `Entry.Data` when constructing the AuditLine to 1024 characters each, as the error seems to suggest that Windows doesn't have enough room to handle the data.

## 2019-07-12
### Changed
`Export Addresses` in `Address Book` page works.
`Address Book` button and menu item in `Accounts` page works
`Address Book` validation is in place.

## 2019-07-11
### Changed
Removed `Select AddressBook` ; following Gord's design, there are only address books for individual accounts.

## 2019-07-10
### Changed
`Address Book`

The address list is now loaded with company names and addresses.
The address filter works.

## 2019-07-02
### Fixed
Pickup and Delivery company names are now loaded when a trip in Search is double-clicked.

## 2019-06-28
### Changed
`Account Details` is disabled until the account names have been loaded.  Followed Terry's technique in `TripEntry`.
Lots of minor changes.

`Trip Entry`  Lots of minor changes.


## 2019-06-21
### Fixed
`Account Details`

- New accounts are added to the list.
- The filter works.

- Open `Trip Entry` pages have their accounts list updated when an account is saved.

## 2019-06-20
### Fixed
New addresses in `Shipment Entry` are saved with the trip but aren't visible in the UI until a new page is loaded.
New Customers are saved in the `Account Details` page.


## 2019-06-14
### Fixed
Billing and Delivery company names weren't being selected when a trip was double-clicked in Search.

## 2019-06-13
###**ML-260**
### Changed
`Search Shipments`

- Converted top of window to toolbar, matching `Shipment Entry`.

### Added
- Help button that opens search entry url in a browser.

## 2019-06-12
### Changed
- Package Row fields auto-select text on focus.

###**ML-260**

### Changed
`Trip Entry`

- Tab title changed to `Shipment Entry`.
- New Trip icon changed.
- Find Trip icon and text changed.
- Swap icon changed.
- Address book icon changed.
- Trip changed to Shipment.

`Search Trips`

- Tab title changed to `Search Shipments`.

`Main Window` menu

- Changed `Trip` to `Shipment`.
- Changed `Boards` to `Dispatch`.

### Added
`Trip Entry`

- Help icon that opens shipment entry url in browser.

## 2019-06-11
### Added
- `Trip Entry` now creates and saves trips with trip items.  It also loads and displays trips with trip items.

## 2019-06-07
### Changed
- Regions and countries in Trip Entry are now combo boxes.
- Regions and countries in Account Details are now combo boxes.
- Saving an Account is beginning to work. 

### Added
- AccountId field is readonly if an account is loaded and editable if the page has been cleared.

## 2019-05-30
**Customer Account Details**

- Now loads PrimaryCompany for selected name.
- Clear works.


## 2019-05-29
### Added
- Customer `Account Details` page has been largely laid out.

## 2019-05-27
### Added
- Started on `Accounts` per Eddy's instructions.  Beginning page loads.
- Connected remaining menu items in `Trip Entry` to toolbar methods.
- `Accounts` menu is connected.


## 2019-05-24
### Added
- Started on `Multiple Package Types` in Trip Entry. 

## 2019-05-23
### Removed
- Per Gord - removed `Clipboard` toolbar section.

## 2019-05-22
### Added
- Beginnings of Address Book.

## 2019-05-21a
### Added
- `Reveal Password` button to login page.

## 2019-05-21
### Changed
- Clone, Clone Pickup, and Clone Return check for modified trips and offers to save them first.

## 2019-05-16
### Added
- Trip Entry's swap function works.
- There are tooltips for the buttons on the menubar.
- Clone, Clone Pickup, and Clone Return work.

## 2019-05-15a
### Added
- Trip Entry's `Find Trip` works.

## 2019-05-15
### Added
- Trip Audit is working, albeit with LOGIN data rather than TRIP history, because the latter doesn't exist yet.
## 2019-05-10a
### Fixed
- Double-clicking a trip in Search if there wasn't a Trip Entry tab open tried to open a new Search tab.
## 2019-05-10
### Added
- Basic tracking works.  Only pulls items from the trip record (trip creation, ready time, picked up, and delivered).

## 2019-05-07
### Added
- Weighbill loads for currently loaded trip.  The account is printed, but can't find the phone number yet.
### Fixed
- `Search Trips` date selection didn't work - don't know when it stopped.

## 2019-04-25
### Added
- `Rate Wizard` Service Levels and Package Types tabs are working.  The Model keeps track of pending changes and the UI is updated.

## 2019-04-23
- Finishing `Rate Wizard` Service Levels.  Saving isn't working yet.

## 2019-04-15
### Added
- `Rate Wizard` page opens.  Starting to build the interface.

## 2019-04-15
### Changed
- Connected GetServiceLevels to the server.  After populating the table, they are now returned and displayed.

## 2019-04-05
### Added
- There is a validation system in place for TripEntry.  The code is written up in **Notes_On_UI_Validation.md**.
- The call time widget shows 15 minute periods in the dropdown.
- There is a menu that replicates the toolbar buttons.  A context menu in the toolbar (or double-clicking it) hides the toolbar and shows the menu.  A `View` menu item shows the toolbar and hides the menu.
- Trips are created and updated.


## 2019-03-26a
### Added
- cjt Added a driver dropdown to TripEntry so that the trip can be dispatched immediately.

### Changed
- cjt Changed `Service` label in TripEntry to `Details`.


## 2019-03-26
### Added
- cjt PackageTypes are loaded either in their sort order or alphabetically.

## 2019-03-25
### Added
- cjt Toolbar to TripEntry using Gord's icons.
- cjt `New` button fetches a new tripid.  If the `Account` isn't empty, the user is asked if it just wants a new tripid.  This replicates IDS1 behaviour.
- cjt `Clear` works.


## 2019-02-22
### Added
- cjt Most of the basic fields in TripEntry are in place and being populated when a trip is loaded from the search screen.

## 2019-02-20
### Added
- cjt Trip details (status, driver, t-pu, t-del) at the top of the trip entry screen.

## 2019-02-19
### Added
- cjt Missed the Account column.
- cjt Address filter works.

## 2019-02-14
### Added
- cjt Nested a Filter for Addresses beneath the Company filter.  It is populated when the selected company changes.  The search doesn't make use of it yet.


## 2019-02-07
### Added
- cjt Trips that have one of the 4 (POD doesn't exist in the Trips table) fields modified are now updated in the ListView and saved to the database.  
- cjt Missed Calltime from the changeable fields.

## 2019-02-05a
### Added
- cjt Populated drivers dropdown with staff members that have a role of `Driver`.
- cjt Changing EditSearchTrips to have a 2-stage commit.
		1. First select the fields to change and `Select` them.
		2. Click `Ok` to apply the changes.

## 2019-02-05
### Added
- cjt Adding a dialog that allows the 4 editable (for the moment) fields in the search trips list to be modified.  It will be possible to modify multiple trips at once.

## 2019-02-01
### Added
- cjt When a SearchTrips tab is opened, it checks to see if there is a TripEntry tab open, and, if there isn't, it opens one.
- cjt TripId and AccountId are loaded in the Trip Entry tab.
- cjt Double-clicking a trip either opens a new Trip Entry tab if none are open or, goes to the first open one.  
### Changed
- cjt Rest of TextBoxes in TripEntry are attached to TextBoxesEditableEnable.  TripId is uneditable.

## 2019-01-31
### Added
- cjt New program `TripEntry` with menu item to create it.  Opens as a tab. 
- cjt Beginning of TripEntry layout according to `IDS2LiteJan52016.pdf`.

## 2019-01-30
### Changed
- cjt Reordering columns works.  Had to change headers from Buttons to GridViewColumnHeaders.

## 2019-01-29
### Added
- cjt Trip Search list has a context menu that allows the toggling of visible columns.  The list is automatically updated.

## 2019-01-24
### Changed
- cjt Reworking columns for trip search screen to match those in design document.  Some are deactivated because there are no matching columns in the Trips table.
- cjt Filtering only works with the visible columns.  The mechanism for selecting the columns hasn't been built yet.

## 2019-01-21
### Changed
- cjt Local logging is now flushed after every write.
- cjt Now 1 file per day is created.  **Caveat:** if the program is left running overnight, the previous day's log will still be used because the new log is created on startup. 
- cjt `Close` button on login screen is enabled so that the user can close the program without putting anything in the password field.

### Added
- cjt Utility method to Utils.Logging: WriteLogLine.  Replicates log line format from IdsDispatch.  It is improved, as it includes the source file name and the line number.
- cjt Added a `Filter` textbox to the search trips screen.  The filter is applied as typed.  It also keeps the current sort order.
- cjt Added `Clear` button to reset the screen.

## 2019-01-14
### Fixed
- cjt Bug in GetCompanyNames.
- cjt Added icon to project.

## 2019-01-08
### Changed
- cjt - Starting to modify Trip Search.  Added Start and End status combos.
