# UI Validation
i've created an validation process for TripEntry that can be used in other pages.  The design attempts to maintain the separation between the Xaml world and the associated Model, allowing the Model to do the validation without knowing anything about the widgets themselves.  

The other design goal is to provide a single report of the errors along with the problem fields to the user, rather than having them fix the errors one at a time.

It depends upon an Extension method `GetChildren` in `Widgets.cs` that recurses over the Page and returns each Control.


This is how it starts (from `TripEntry.xaml.cs`):

{noformat}

private Dictionary<string, Control> dictControls = new Dictionary<string, Control>();

private void LoadMappingDictionary()
{
  if (DataContext is TripEntryModel Model)
  {
    foreach (var ctrl in this.GetChildren())
    {
      Binding binding = null;
      if (ctrl is TextBox tb)
      {
        binding = BindingOperations.GetBinding(tb, TextBox.TextProperty);
      }
      else if (ctrl is ComboBox cb)
      {
        binding = BindingOperations.GetBinding(cb, ComboBox.ItemsSourceProperty);
      }
      else if (ctrl is DateTimePicker dtp)
      {
        binding = BindingOperations.GetBinding(dtp, DateTimePicker.ValueProperty);
      }
      if (binding != null)
      {
        string name = binding.Path.Path;
        if (!dictControls.ContainsKey(name))
        {
          dictControls.Add(name, (Control)ctrl);
        }
        else
        {
          Utils.Logging.WriteLogLine("ERROR " + name + " already exists");
        }
      }
    }
  }
}

{noformat}


It is called like this (from `TripEntry.xaml.cs`):

{noformat}
private void BtnSave_Click(object sender, RoutedEventArgs e)
{
  if( DataContext is TripEntryModel Model )
  {
    if (this.dictControls.Count == 0)
    {
      LoadMappingDictionary();
    }
    List<string> errors = Model.Execute_SaveTrip(dictControls);
    if (errors != null && errors.Count > 0)
    {
      string message = (string)Application.Current.TryFindResource("TripEntryValidationErrors")
        + System.Environment.NewLine;
      foreach (string line in errors)
      {
        message += "\t" + line + System.Environment.NewLine;
      }
      message += (string)Application.Current.TryFindResource("TripEntryValidationErrors1");
      string title = (string)Application.Current.TryFindResource("TripEntryValidationErrorsTitle");
      MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
    }
  }
}

{noformat}

The actual validation takes place in the Model (from `TripEntryModel.cs`):

{noformat}
public List<string> Execute_SaveTrip(Dictionary<string, Control> controls)
{
  List<string> errors = new List<string>();
  Trip trip = null;
  (errors, trip) = ValidateTrip(controls);

  if (errors.Count == 0)
  {
    // TODO Save the trip
  }

  return errors;
}

private (List<string>errors, Trip trip) ValidateTrip(Dictionary<string, Control> controls)
{
  List<string> errors = new List<string>();
  Trip trip = new Trip();            
  if (controls.Count > 0)
  {
    foreach (string fieldName in controls.Keys)
    {
      switch (fieldName)
      {
        case "TripId":
          string error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("TripEntryValidationErrorsTripId"));
          if (!string.IsNullOrEmpty(error))
          {
            errors.Add(error);
          }
          break;
        case "CompanyNames":
          error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("TripEntryValidationErrorsCompanyNames"));
          if (!string.IsNullOrEmpty(error))
          {                                
            errors.Add(error);
          }
          break;
        case "CallDate":
          error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("TripEntryValidationErrorsCallDate"));
          if (!string.IsNullOrEmpty(error))
          {
            errors.Add(error);
          }
          break;
      }
    }
  }

  return (errors, trip);
}

private string SetErrorState(Control ctrl, string message)
{
  string error = string.Empty;
  if (IsControlEmpty(ctrl))
  {
    ctrl.Background = Brushes.Yellow;
    error = message;
  }
  else
  {
    ctrl.Background = Brushes.White;
  }

  return error;
}

private bool IsControlEmpty(Control ctrl)
{
  bool isEmpty = true;
  if (ctrl is TextBox tb)
  {
    isEmpty = IsTextBoxEmpty(tb);
  }
  else if (ctrl is ComboBox cb)
  {
    isEmpty = IsComboBoxSelected(cb);
  }
  else if (ctrl is DateTimePicker dtp)
  {
    isEmpty = IsDateTimePickerEmpty(dtp);
  }

  return isEmpty;
}

private bool IsTextBoxEmpty(TextBox tb)
{
  bool isEmpty = true;

  if (!string.IsNullOrEmpty(tb.Text))
  {
    isEmpty = false;
  }

  return isEmpty;
}

private bool IsComboBoxSelected(ComboBox cb)
{
  bool isEmpty = true;

  if (cb.SelectedIndex > -1)
  {
    isEmpty = false;
  }

  return isEmpty;
}

private bool IsDateTimePickerEmpty(DateTimePicker dtp)
{
  bool isEmpty = true;

  if (!string.IsNullOrEmpty(dtp.Text))
  {
    isEmpty = false;
  }

  return isEmpty;
}


{noformat}